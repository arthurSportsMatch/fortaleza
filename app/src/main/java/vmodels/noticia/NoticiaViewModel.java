package vmodels.noticia;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.BannerData;
import data.PartidaData;
import repository.lastgames.LastGamesRepository;
import repository.noticia.NoticiaRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NoticiaViewModel extends MainViewModel {

    private NoticiaRepository mRepository;
    public void setmRepository(NoticiaRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<BannerData>> mNoticia;
    public LiveData<List<BannerData>> noticia (){
        if(mNoticia == null){
            mNoticia = new MutableLiveData<>();
            getNoticia(1);
        }else if(mNoticia.getValue() == null){
            getNoticia(1);
        }

        return mNoticia;
    }

    public void getNoticia (int home){
        mRepository.getNoticias(home, new NoticiaRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<BannerData> data) {
                mNoticia.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
