package vmodels.stories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.InstagramData;
import models.ErrorModel;
import repository.badges.BadgeRepository;
import repository.stories.StoriesRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class StoriesViewModel extends MainViewModel {

    private StoriesRepository mRepository;
    public void setmRepository(StoriesRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<InstagramData>> mStoriesList;
    public LiveData<List<InstagramData>> stories () {
        if (mStoriesList == null){
            mStoriesList = new MutableLiveData<>();
            loadStories();
        }
        return mStoriesList;
    }

    public void loadStories (){
        mRepository.getStories(new StoriesRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<InstagramData> data) {
                mStoriesList.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {
                mError.postValue(new ErrorModel(0, "Sem Stories"));
            }
        });
    }
}
