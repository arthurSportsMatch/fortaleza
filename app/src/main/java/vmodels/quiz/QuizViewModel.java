package vmodels.quiz;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import data.QuizData;
import repository.pesquisa.PesquisaRepository;
import repository.quiz.QuizRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class QuizViewModel extends MainViewModel {

    private QuizRepository mRepository;
    public void setmRepository(QuizRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<QuizData> mQuizHome;
    public LiveData<QuizData> quizHome () {
        if(mQuizHome == null){
            mQuizHome = new MutableLiveData<>();
            getQuizHome();
        }
        return mQuizHome;
    }

    public void getQuizHome (){
        mRepository.getQuizHome(new QuizRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(QuizData data) {
                mQuizHome.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
