package vmodels.twitter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.TweetResultData;
import models.ErrorModel;
import repository.server.ServerRepository;
import repository.twitter.TwitterRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class TwitterViewModel extends MainViewModel {

    private TwitterRepository mRepository;
    public void setmRepository(TwitterRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<TweetResultData>> mTweetList;
    public LiveData<List<TweetResultData>> tweets (){
        if(mTweetList == null){
            mTweetList = new MutableLiveData<>();
            getTweets();
        }

        return mTweetList;
    }

    public void getTweets (){
        mRepository.getTweets(new TwitterRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<TweetResultData> data) {
                mTweetList.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {
                mError.postValue(new ErrorModel(0, message));
            }
        });
    }

}
