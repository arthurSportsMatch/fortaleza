package vmodels.badges;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.BadgeData;
import models.MessageModel;
import repository.badges.BadgeRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class BadgeViewModel extends MainViewModel {

    private BadgeRepository mRepository;
    public void setmRepository(BadgeRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<BadgeData>> mBadges;

    public LiveData<List<BadgeData>> getBadges (){
        if(mBadges == null){
            mBadges = new MutableLiveData<>();
            getBadges(1);
        }else if(mBadges.getValue() == null){
            getBadges(1);
        }else if (mBadges.getValue().size() <= 0){
            getBadges(1);
        }


        return mBadges;
    }

    public void getBadges(int page){
        mRepository.getList(page, new BadgeRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<BadgeData> data) {
                mBadges.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel =  new MessageModel();
                messageModel.setMsg(message);
                messageModel.setId(1);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
