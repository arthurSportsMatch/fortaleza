package vmodels.fragment;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import data.FragmentData;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class FragmentHolderViewModel extends MainViewModel {


    MutableLiveData<FragmentData> mFragment;
    MutableLiveData<Boolean> mChange;

    public LiveData<FragmentData> getFragment (){
        if(mFragment == null){
            mFragment = new MutableLiveData<>();
        }
        return mFragment;
    }

    public LiveData<Boolean> canChange (){
        if(mChange == null){
            mChange = new MutableLiveData<>();
        }
        return mChange;
    }

    public void setHasToChange(boolean change){
        mChange.postValue(change);
    }

    public void changeFragment (Fragment fragment, String tag){
        mChange.postValue(true);
        mFragment.postValue(new FragmentData(fragment, tag));
    }
}
