package vmodels.nextgames;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.BadgeData;
import data.PartidaData;
import models.nextgames.NextGameModel;
import repository.badges.BadgeRepository;
import repository.nextgames.NextGamesRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NextGamesViewModel extends MainViewModel {

    private NextGamesRepository mRepository;
    public void setmRepository(NextGamesRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<NextGameModel> mNextGames;
    public LiveData<NextGameModel> nextGames () {
        if(mNextGames == null){
            mNextGames = new MutableLiveData<>();
            getNextGames(1);
        }else if(mNextGames.getValue() == null){
            getNextGames(1);
        }

        return mNextGames;
    }

    public void getNextGames (int home){
        mRepository.getList(home, new NextGamesRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(NextGameModel data) {
                mNextGames.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
