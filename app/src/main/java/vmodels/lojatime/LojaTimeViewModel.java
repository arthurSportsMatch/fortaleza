package vmodels.lojatime;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import data.ProdutoFData;
import repository.lojatime.LojaTimeRepository;
import repository.stories.StoriesRepository;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LojaTimeViewModel extends ViewModel {

    private LojaTimeRepository mRepository;
    public void setmRepository(LojaTimeRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<ProdutoFData>> mProdutos;
    public LiveData<List<ProdutoFData>> produtos (){
      if(mProdutos == null){
          mProdutos = new MutableLiveData<>();
          getProdutos();
      }

      return mProdutos;
    };

    public void getProdutos (){
        mRepository.getProdutosHome(new LojaTimeRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<ProdutoFData> data) {
                mProdutos.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
