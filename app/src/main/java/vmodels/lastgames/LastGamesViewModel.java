package vmodels.lastgames;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.PartidaData;
import repository.badges.BadgeRepository;
import repository.lastgames.LastGamesRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LastGamesViewModel extends MainViewModel {

    private LastGamesRepository mRepository;
    public void setmRepository(LastGamesRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<PartidaData>> mPartidas;
    public LiveData<List<PartidaData>> lastGames (){
        if(mPartidas == null){
            mPartidas = new MutableLiveData<>();
            getLastGames();
        }else if(mPartidas.getValue() == null){
            getLastGames();
        }else if (mPartidas.getValue().size() <= 0){
            getLastGames();
        }


        return mPartidas;
    }

    public void getLastGames (){
        mRepository.getList(new LastGamesRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<PartidaData> data) {
                mPartidas.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
