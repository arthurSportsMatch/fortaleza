package vmodels.audioteca;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.BannerData;
import data.PlaylistItemData;
import repository.audioteca.AudioTecaRepository;
import repository.badges.BadgeRepository;
import repository.noticia.NoticiaRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class AudioTecaViewModel extends MainViewModel {

    private AudioTecaRepository mRepository;
    public void setmRepository(AudioTecaRepository mRepository) {
        this.mRepository = mRepository;
    }


    MutableLiveData<PlaylistItemData> mAudio;
    MutableLiveData<List<PlaylistItemData>> mAudios;

    public LiveData<List<PlaylistItemData>> audios (){
        if(mAudios == null){
            mAudios = new MutableLiveData<>();
            getAudios();
        }else if(mAudios.getValue() == null){
            getAudios();
        }else if (mAudios.getValue().size() <= 0){
            getAudios();
        }

        return mAudios;
    }

    public LiveData<PlaylistItemData> audio () {
        if (mAudio == null) {
            mAudio = new MutableLiveData<>();
            getAudioHome();
        }

        return mAudio;
    }

    public void getAudioHome (){
        mRepository.getAudioHome(new AudioTecaRepository.AsyncResponseHome() {
            @Override
            public void onResponseSucces(PlaylistItemData data) {
                if(mAudio == null) {
                    mAudio = new MutableLiveData<>();
                }
                mAudio.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }

    public void getAudios (){
        mRepository.getAudios(new AudioTecaRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<PlaylistItemData> data) {
                if(mAudio == null) {
                    mAudio = new MutableLiveData<>();
                }
                mAudio.postValue(data.get(0));

                if(mAudios == null) {
                    mAudios = new MutableLiveData<>();
                }
                mAudios.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
