package vmodels.pesquisa;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.QuizData;
import models.ErrorModel;
import repository.lojatime.LojaTimeRepository;
import repository.pesquisa.PesquisaRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class PesquisaViewModel extends MainViewModel {

    private PesquisaRepository mRepository;
    public void setmRepository(PesquisaRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<List<QuizData>> mLastPesquisas;

    public LiveData<List<QuizData>> lastPesquisas (){
        if(mLastPesquisas == null){
            mLastPesquisas = new MutableLiveData<>();
            getLastPesquisas();
        }
        return mLastPesquisas;
    }

    public void getLastPesquisas (){
        mRepository.getPesquisaHome(new PesquisaRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(List<QuizData> data) {
                mLastPesquisas.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {
                mError.postValue(new ErrorModel(0, message));
            }
        });
    }
}
