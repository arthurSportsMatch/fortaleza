package vmodels.socio;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import data.ServerCustomData;
import models.MessageModel;
import models.user.UserModel;
import repository.server.ServerRepository;
import repository.socio.SocioRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class SocioViewModel extends MainViewModel {
    private SocioRepository mRepository;
    public void setmRepository(SocioRepository mRepository) {
        this.mRepository = mRepository;
    }

    public void getSocioAPI (){
        loadSocioAPI();
    }

    private void loadSocioAPI (){
        mRepository.getSocioAPI(new SocioRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(ServerCustomData data) {

            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setId(1);
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }


}
