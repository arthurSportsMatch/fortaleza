package vmodels.elenco;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import models.elenco.ElencoModel;
import models.nextgames.NextGameModel;
import repository.elenco.ElencoRepository;
import repository.nextgames.NextGamesRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class ElencoViewModel extends MainViewModel {

    private ElencoRepository mRepository;
    public void setmRepository(ElencoRepository mRepository) {
        this.mRepository = mRepository;
    }

    MutableLiveData<ElencoModel> mElenco;
    public LiveData<ElencoModel> elenco () {
        if(mElenco == null){
            mElenco = new MutableLiveData<>();
            getElenco();
        }else if(mElenco.getValue() == null){
            getElenco();
        }

        return mElenco;
    }

    public void getElenco (){
        mRepository.getElenco(new ElencoRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(ElencoModel data) {
                mElenco.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
