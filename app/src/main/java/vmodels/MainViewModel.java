package vmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import models.ErrorModel;
import models.MessageModel;
import models.user.UserModel;


/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class MainViewModel extends ViewModel {

    //Se holver algum erro
    protected MutableLiveData<ErrorModel> mError = new MutableLiveData<>();
    public LiveData<ErrorModel> getError (){
        return mError;
    }

    //Se tiver alguma mensagem
    protected MutableLiveData<MessageModel> mMessage = new MutableLiveData<>();
    public LiveData<MessageModel> getMessage (){
        return mMessage;
    }

    //Se estiver carregando
    protected MutableLiveData<Boolean> mLoading = new MutableLiveData<>();
    public LiveData<Boolean> isLoading (){
        return mLoading;
    }
}
