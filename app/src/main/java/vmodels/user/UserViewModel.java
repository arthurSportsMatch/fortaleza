package vmodels.user;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import models.ErrorModel;
import models.MessageModel;
import models.user.UserModel;
import repository.user.UserRepository;
import utils.UTILS;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class UserViewModel  extends MainViewModel {

    public static int NameError = 0;
    public static int EmailError = 1;
    public static int SenhaError = 2;
    public static int ConfirmError = 3;

    //Repositorio
    private UserRepository mRepository;
    public void setmRepository(UserRepository mRepository) {
        this.mRepository = mRepository;
    }

    //LiveData
    private MutableLiveData<UserModel> mUser;
    public LiveData<UserModel> getUserData (){
        if(mUser == null){
            mUser = new MutableLiveData<>();
        }
        return mUser;
    }


    //Public Getters
    public void getUser (){
        getUserAccount();
    }

    public void getUser (boolean hasRequested){
        getUserAccount(hasRequested);
    }

    public void singIn (String email, String senha, @Nullable String id){
        if(checkLoginInfos(email, senha)){
            loginUser(email, senha, id);
        }
    }

    public void singUp (String nome, String email, String senha, String confirmSenha, String id){
        if(checkCreateAccontForm(nome,email,senha,confirmSenha)){
            createUser(nome, email, senha, id);
        }
    }

    public void forgot(String email){
        if(checkEmail(email)) {
            forgotEmail(email);
        }
    }

    public void uploadUser (UserModel user){
        changeUser(user);
    }

    public void uploadPhoto (Uri photoUri, int cam, boolean isCam){
        changePhoto(photoUri, cam, isCam);
    }

    public void getSocio (){
        loadSocio();
    }

    public void getSocio (String cpf){
        loadSocio(cpf);
    }

    public void changeSenha (String senha1, String senha2){
        if(senha1.equals(senha2)){
            changeSenha(senha1);
        }else{
            ErrorModel errorModel = new ErrorModel();
            errorModel.setError(1);
            errorModel.setMsg("Senhas não coincidem");
            mError.postValue(errorModel);
        }

    }

    //region Checkers
    //Check login infos
    boolean checkLoginInfos (String email, String senha){
        //Check email
        if(email.isEmpty()){
            mError.postValue(new ErrorModel(EmailError, "Coloque seu e-mail aqui"));
            return false;
        }else{
            if(!UTILS.isValidEmail(email)){
                mError.postValue(new ErrorModel(EmailError, "Coloque um e-mail valido aqui"));
                return false;
            }
        }

        if(senha.isEmpty()){
            mError.postValue(new ErrorModel(SenhaError, "Coloque sua senha"));
            return false;
        }
        return true;
    }

    //Check create Accont info
    boolean checkCreateAccontForm (String name, String email, String senha, String senha2){

        if(name.isEmpty()){
            mError.postValue(new ErrorModel(NameError, "Coloque seu nome aqui"));
            return false;
        }

        //Check email
        if(email.isEmpty()){
            mError.postValue(new ErrorModel(EmailError, "Coloque seu e-mail aqui"));
            return false;
        }else{
            if(!UTILS.isValidEmail(email)){
                mError.postValue(new ErrorModel(EmailError, "Coloque um e-mail valido aqui"));
                return false;
            }
        }

        //Check senha
        if(senha.isEmpty()){
            mError.postValue(new ErrorModel(SenhaError, "Coloque sua senha"));
            return false;
        }else{
            if(senha.length() < 4){
                mError.postValue(new ErrorModel(SenhaError, "A senha tem que ter 4 ou mais caracteres"));
                return false;
            }
        }

        //Check Confirmation
        if(senha2.isEmpty()){
            mError.postValue(new ErrorModel(ConfirmError, "Confirme sua senha aqui"));
            return false;
        }else{
            if(!senha.equals(senha2)){
                mError.postValue(new ErrorModel(ConfirmError, "As senhas devem ser iguais!"));
                return false;
            }
        }

        return true;
    }

    boolean checkEmail (String email){
        //Check email
        if(email.isEmpty()){
            mError.postValue(new ErrorModel(EmailError, "Coloque seu e-mail aqui"));
            return false;
        }else{
            if(!UTILS.isValidEmail(email)){
                mError.postValue(new ErrorModel(EmailError, "Coloque um e-mail valido aqui"));
                return false;
            }
        }
        return true;
    }
    //endregion

    //region Private Getters
    //Pega o usuario pelo ID
    private void getUserAccount(){
        mRepository.getUser(new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    private void getUserAccount(final boolean hasRequested){
        mRepository.getSaved(new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                if(hasRequested){
                    MessageModel messageModel = new MessageModel();
                    messageModel.setMsg(message);
                    mMessage.postValue(messageModel);
                }
            }

            @Override
            public void onResponseError(String message) {
                if(hasRequested) {
                    ErrorModel errorModel = new ErrorModel();
                    errorModel.setError(0);
                    errorModel.setMsg(message);
                    mError.postValue(errorModel);
                }
            }
        });
    }

    //Muda as informações do usuario
    private void changeUser (UserModel user){
        mRepository.uploadUser(user, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    //Muda a foto do usuario
    private void changePhoto (Uri photoUri, int cam, boolean isCam){
        mRepository.uploadFoto(photoUri, cam, isCam, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    //Pega o usuario por login
    private void loginUser (String email, String senha, @Nullable String id){
        mRepository.loginUser(email, senha, id, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    //Pega o usuario por login
    private void createUser (String nome, String email, String senha, String id){
        mRepository.createUser(nome, email, senha, id, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    //Esqueceu a senha
    private void forgotEmail (String email){
        mRepository.forgot(email, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    //Recebe o socio torcedor
    private void loadSocio () {
        mRepository.getSocio(new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }

    //Recebe o socio torcedor
    private void loadSocio (String cpf) {
        mRepository.getSocio(cpf, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                messageModel.setId(12);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }

    private void changeSenha (String senha){
        mRepository.changePassword( senha, new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                mUser.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
    //endregion
}
