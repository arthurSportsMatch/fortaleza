package vmodels.utils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import data.FragmentData;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class MoedasViewModel extends MainViewModel {

    MutableLiveData<Integer> mMoedas;
    MutableLiveData<Boolean> mChange;

    public LiveData<Integer> getMoedas (){
        if(mMoedas == null){
            mMoedas = new MutableLiveData<>();
        }
        return mMoedas;
    }

    public LiveData<Boolean> canChange (){
        if(mChange == null){
            mChange = new MutableLiveData<>();
        }
        return mChange;
    }

    public void setHasToChange(boolean change){
        mChange.postValue(change);
    }

    public void giveMoedas (int moedas){
        mChange.postValue(true);
        mMoedas.postValue(moedas);
    }
}
