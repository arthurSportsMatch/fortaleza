package vmodels.utils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.PositonData;
import models.ErrorModel;
import models.MessageModel;
import models.utils.RegulamentoModel;
import repository.utils.RegulamentoRepository;
import vmodels.MainViewModel;

import static utils.LoadEmbed.loadHtmlEmbed;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class RegulamentoViewModel extends MainViewModel {

    private RegulamentoRepository mRepository;
    public void setmRepository(RegulamentoRepository mRepository) {
        this.mRepository = mRepository;
    }

    private MutableLiveData<RegulamentoModel> regulamento = new MutableLiveData<>();
    public LiveData<RegulamentoModel> getRegulamento (){
        return regulamento;
    }

    private MutableLiveData<List<PositonData>> docs;
    public LiveData<List<PositonData>> getDocs (){
        if(docs == null){
            docs = new MutableLiveData<>();
            getDocuments();
        }
        return docs;
    }

    public void getRegulamento (int id){
        mRepository.getRegulamento(id, new RegulamentoRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(RegulamentoModel data) {
                data.setDocumentacao(loadHtmlEmbed(data.getDocumentacao()));
                regulamento.postValue(data);
            }

            @Override
            public void onResponseSucces(List<PositonData> data) {

            }

            @Override
            public void onResponseMessage(String message) {
                MessageModel messageModel = new MessageModel();
                messageModel.setMsg(message);
                mMessage.postValue(messageModel);
            }

            @Override
            public void onResponseError(String message) {
                ErrorModel errorModel = new ErrorModel();
                errorModel.setError(0);
                errorModel.setMsg(message);
                mError.postValue(errorModel);
            }
        });
    }

    private void getDocuments (){
        mRepository.getDocuments(new RegulamentoRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(RegulamentoModel data) {

            }

            @Override
            public void onResponseSucces(List<PositonData> data) {
                docs.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }


}
