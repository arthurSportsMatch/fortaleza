package vmodels.server;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import data.ServerCustomData;
import models.utils.RegulamentoModel;
import repository.server.ServerRepository;
import repository.utils.RegulamentoRepository;
import vmodels.MainViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public  class ServerViewModel extends MainViewModel {

    private ServerRepository mRepository;
    public void setmRepository(ServerRepository mRepository) {
        this.mRepository = mRepository;
    }

    private MutableLiveData<ServerCustomData> servers;
    public LiveData<ServerCustomData> getServers (){
        if(servers == null){
            servers = new MutableLiveData<>();
            getServer(null);
        }
        return servers;
    }

    public void getServer (@Nullable Integer idUser){
        getServers(idUser);
    }

    private void getServers (@Nullable Integer idUser){
        mRepository.getServer(idUser, new ServerRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(ServerCustomData data) {
                servers.postValue(data);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }
}
