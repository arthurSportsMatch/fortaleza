package utils.fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import utils.CardType;
import utils.MaskEditUtil;

import static utils.CardType.forCardNumber;

public class MaskCard {


    Listener listener;

    public MaskCard(Listener listener) {
        this.listener = listener;
    }

    /**
     * Método que deve ser chamado para realizar a formatação
     *
     * @param ediTxt
     * @param mask
     * @return
     */

    public TextWatcher mask(final EditText ediTxt, final String mask) {
        return new TextWatcher() {
            boolean isUpdating;
            String old = "";

            @Override
            public void afterTextChanged(final Editable s) {
                String ccNum = s.toString();
                CardType card = forCardNumber(ccNum.replaceAll("-",""));

                listener.onCard(card);
            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {}

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                final String str = MaskEditUtil.unmask(s.toString());
                String mascara = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (final char m : mask.toCharArray()) {
                    if (m != '#' && str.length() > old.length()) {
                        mascara += m;
                        continue;
                    }
                    try {
                        mascara += str.charAt(i);
                    } catch (final Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                ediTxt.setText(mascara);
                ediTxt.setSelection(mascara.length());
            }
        };

    }

    public interface Listener {
        void onCard(CardType cardType);
    }

}
