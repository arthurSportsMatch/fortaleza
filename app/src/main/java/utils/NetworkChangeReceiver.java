package utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.sportm.fortaleza.MainActivity;

/**
 * Created by rafael.verginelli on 8/22/16.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    public static MainActivity currentActivity = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        switch(UTILS.getNetworkStatus(connectivityManager)){
            default: {
                if( (currentActivity != null) && (((IConnectionDialog)currentActivity) != null) ){
                    ((IConnectionDialog)currentActivity).CallConnectionDialog(currentActivity);
                }
            } break;

            case CONSTANTS.CONNECTION_TYPE_WIFI:
            case CONSTANTS.CONNECTION_TYPE_MOBILE: {

                if( (currentActivity != null) && (((IConnectionDialog)currentActivity) != null) ){
                    ((IConnectionDialog)currentActivity).DismissConnectionDialog();
                }

            } break;
        }
    }

    public interface IConnectionDialog {

        void CallConnectionDialog(Activity activity);
        void DismissConnectionDialog();

    }
}
