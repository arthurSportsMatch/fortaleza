package utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.sportm.fortaleza.R;

public class OnLoadListener {

    static public AlertDialog createLoadDialog (Activity context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View customLayout = context.getLayoutInflater().inflate(R.layout.dialog_loading, null);
        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
        return dialog;
    }

    static public View createLoadDialog (Activity activity, LinearLayout mainView){
        View view = null;
        //Get Views
        if(mainView.findViewById(R.id.dialog_loading) != null){
            view = mainView.findViewById(R.id.dialog_loading);
        }else{
            view = LayoutInflater.from(activity).inflate(R.layout.dialog_loading, null, false);
        }
        mainView.addView(view);
        return view;
    }
}
