package utils;

import android.view.View;
import android.widget.Button;

import com.sportm.fortaleza.R;

public class UILoadingController {

    View mainView;
    Listener listener;
    View loading;
    Button tryAgain;

    public UILoadingController(View mainView, Listener listener) {
        this.mainView = mainView;
        this.listener = listener;

        initView();
        initAction();
    }

    void initView (){
        loading = mainView.findViewById(R.id.view_carregando);
        tryAgain = mainView.findViewById(R.id.btn_tentar_novamente);
    }

    void initAction (){
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTryAgain();
                onLoad();
            }
        });
    }

    public void onLoad(){
        loading.setVisibility(View.VISIBLE);
        tryAgain.setVisibility(View.GONE);
    }

    public void finishLoader (boolean suceess){
        loading.setVisibility(View.GONE);
        if(!suceess){
            tryAgain.setVisibility(View.VISIBLE);
        }
    }

    public interface Listener {
        void onTryAgain ();
    }
}
