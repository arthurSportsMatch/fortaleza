package utils;

import android.os.Handler;

public class Timer {

    private Handler handler;
    private Listener listener;

    public Timer(Listener listener) {
        this.listener = listener;
    }

    public void start(int timeInMillis) {
        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.onTimeOut();
                handler.removeCallbacksAndMessages(null);
            }
        }, timeInMillis);
    }

    public void stop() {
        if ( handler != null ) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    public interface Listener {
        void onTimeOut();
    }
}
