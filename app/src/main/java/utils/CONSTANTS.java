package utils;

public class CONSTANTS {
    //YouTube
    public static final String YOUTUBE_EMBED_PREFIX = "https://www.youtube.com/embed/";
    public static final String YOUTUBE_MIN_EMBED_PREFIX = "https://youtu.be/";

    //Erro codes
    public static final int ERROR_CODE_MAINTENANCE      = 666;
    public static final int ERROR_CODE_UPDATE_SUGGESTED = 425;

    //Files
    public static String POST_ATTACHED_FILE_NAME = "attach_file_master_clube";
    public static String AVATAR_FILENAME = "avatar";
    public static String AVATAR_FILENAME_EXTENSION = ".jpeg";
//    public static String AVATAR_FILENAME_EXTENSION = ".jpg";

    //Chat
//    public static final String DB_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String DB_DATE_FORMAT = "yyyy-mm-dd HH:mm:ss";
    public static final String DB_HOUR_FORMAT_DISPLAY = "HH:mm";
    public static final String DB_DATE_FORMAT_DISPLAY = "dd/MM/yyyy";
//    public static final String DB_DATE_FORMAT_DISPLAY = "dd/MM/yyyy', %s' HH:mm";

    //End session
    public static final int END_SESSION_TYPE_ANDROID = 1;
    public static final int END_SESSION_TYPE_ANDROID_BACKGROUND = 2;
    public static final int END_SESSION_TYPE_ADNROID_KILL_APP = 3;

    //Random Match

    // Network Connection
    public static final int CONNECTION_TYPE_NONE = 0;
    public static final int CONNECTION_TYPE_WIFI = 1;
    public static final int CONNECTION_TYPE_MOBILE = 2;

    //API Socio torcedor
    public static final String WS_API_SOCIO_HEADER_KEY = "X-User-token";
    public static String apiServerURL = "http://mobile.sociofortaleza.com.br/v1/";
    public static String apiServer = "http://mobile.sociofortaleza.com.br";

    public static String apikey = "601f93f52aa24ca7acf8b16a49cd17cd"; //enviar no header da requisição

    //App
    public static final String APP_PACKAGE_NAME                         = "br.com.bizsys.masterbakers";
    public static final int SPLASH_SCREEN_DURATION = 3 * 1000;
    public static final int LOAD_SCREEN_DURATION = 0;
    public static final int CONNECTION_TIMEOUT = 20 * 1000;

    //Secure Prefs
    public static final String SHARED_PREFS_KEY_FILE                    = APP_PACKAGE_NAME + ".APP_PREFS";
    public static final String SHARED_PREFS_SECRET_KEY                  = "prts04112016";

    //Local Data
    public static final String SHARED_PREFS_KEY_TUTORIAL                       = "prts_show_tutorial";
    public static final String SHARED_PREFS_KEY_GCM_TOKEN                      = "prts_gcm_token";
    public static final String SHARED_PREFS_KEY_NEED_TO_UPDATE_APNS            = "prts_need_to_update_apns";
    public static final String SHARED_PREFS_KEY_AUTOLOGIN                      = "prts_autologin_data";
    public static final String SHARED_PREFS_KEY_PREVIOUS_EMAIL                 = "prts_previous_email";
    public static final String SHARED_PREFS_KEY_BEARER_TOKEN                   = "bearer_token";
    public static final String SHARED_PREFS_KEY_NEW_FEATURES                   = "prts_new_features";
    public static final String SHARED_PREFS_KEY_DENIED_WELCOME_MSGS            = "prts_denied_welcome_msgs";

    public static final String SHARED_PREFS_KEY_FIRST_ACCESS_SPORTS_LIST    = "prts_first_access_sports_list";
    public static final String SHARED_PREFS_KEY_FIRST_ACCESS_PLAYOFFS       = "prts_first_access_playoffs";
    public static final String SHARED_PREFS_KEY_FIRST_ACCESS_MAP            = "prts_first_access_map";
    public static final String SHARED_PREFS_KEY_FIRST_ACCESS_DILEMMAS       = "prts_first_access_dilemams";
    public static final String SHARED_PREFS_KEY_REG_VERSION                 = "prts_reg_ver";
    public static final String SHARED_PREFS_KEY_VIEW_TUTORIAL               = "prts_reg_tutorial";
    public static final String SHARED_PREFS_KEY_SERVER                      = "prts_rserver";
    public static final String SHARED_PREFS_KEY_ENDSESSION                  = "prts_endsession";

    public static final String LAST_CHAT_MSG_ID = "prts_lastChatMessageId";
    public static final String LAST_CHAT_MSGS = "prts_lastChatMessageS";

    //Twitter API
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    public static String TWITTER_KEY = "FhgOOhsBg5Qk0XnwIj7XEiYHD";
    public static String TWITTER_SECRET = "nZNBhUJs8b6rrang5F3EGNqAlkV0ACStwTIOnLtPSlRPfLDVnh";
    public static final String TWITTER_DEFAULT_HASHTAGS_IDOLS = "";
    public static final String TWITTER_DEFAULT_HASHTAGS_TEAMS = "";
    public static final String TWITTER_DEFAULT_HASHTAGS_SPORTS = "";
    public static final String TWITTER_DEFAULT_HASHTAGS_LIVE = "";

    //GCM
    public static final String GCM_PROJECT_NUMBER = "404442033767";

    //UTILS
    public static final float MAX_RADIANS = 6.28319f;
    public static final int OS_ID = 2; //Android is 2, IOS is 1

    //Server
    public static String serverVersion = "";
    public static String mainServer = "https://leao.app/";
    public static String serverAddress = "https://leao.app/";
    public static String getVersion = mainServer + "api/";
    public static final String WS_API_HEADER_KEY = "X-SMApiKey";
    public static String serverURL = serverAddress + "api/";
    public static final String serverContentImages = serverAddress + "Content/images/"; //Utils.getImageAt
    public static final String serverContentCatalogs = serverAddress +"Content/";
    public static final String serverContentPDFs = serverAddress + "Content/PDFs/";
    public static final String WALL_POST_SHARE_URL = "https://supermatch.group/MastersClube/post.php?id=%s";
    public static final String ICON_BROADCAST = "canais/icon_broadcast_chanel_";

    //WS
    public static final String WS_METHOD_SIGN_IN                            = "signin";
    public static final String WS_METHOD_SIGN_IN_TWITTER                    = "signinTwitter";
    public static final String WS_METHOD_SIGN_IN_DIGITS                     = "signinDigits";
    public static final String WS_METHOD_SIGN_UP                            = "signup";
    public static final String WS_METHOD_GET_RANK                           = "getRank";
    public static final String WS_METHOD_GET_SPORT_LIST                     = "sportList";
    public static final String WS_METHOD_GET_SPORT_LIST_COUNTRY             = "sportListCountry?idSport=%s";
    public static final String WS_METHOD_GET_SPORT_LIST_COUNTRY_EXPAND      = "sportListCountryExpand?idSport=%s&idCountry=%s";
    public static final String WS_METHOD_SET_USER_PLAYER                    = "setUserPlayer";
    public static final String WS_METHOD_SET_USER_CLUB                      = "setUserClub";
    public static final String WS_METHOD_GET_USER_SPORTS                    = "getUserSportList";
    public static final String WS_METHOD_FAV_USER_SPORT                     = "favSport";
    public static final String WS_METHOD_GET_NEWS_HOME                      = "getNoticiasHome?carrossel=%s";
    public static final String WS_METHOD_GET_NEWS_GROUP                     = "getNoticiaGrupos";
    public static final String WS_METHOD_GET_NEWS_GROUP_APNS                = "getNoticiaGrupos?apns=%s&os=%s";
    public static final String WS_METHOD_GET_NEWS_GROUP_EXPAND              = "getNoticias?idGrupo=%s";
    public static final String WS_METHOD_GET_NEWS                           = "getNoticia?idNoticia=%s";
    public static final String WS_METHOD_LIKE_NEWS                          = "curtirNoticia";
    public static final String WS_METHOD_GET_RANDOM_MATCH                   = "matchAleatorio";
    public static final String WS_METHOD_MATCH_PLAYERS                      = "match?p1=%s&p2=%s";
    public static final String WS_METHOD_MATCH_RANDOM_MATCH_PLAYERS         = "match?p1=%s&p2=%s&ps=%s";
    public static final String WS_METHOD_GET_PLAYERS                        = "player";
    public static final String WS_METHOD_GET_BONUS_ROUND                    = "getBonusRound";
    public static final String WS_METHOD_ANSWER_BONUS_ROUND                 = "respondeBonusRound";
    public static final String WS_METHOD_GET_PLAYOFFS                       = "getMataMata";
    public static final String WS_METHOD_ANSWER_PLAYOFFS                    = "respondeMataMata";
    public static final String WS_METHOD_GET_RESEARCH                       = "getPesquisa";
    public static final String WS_METHOD_ANSWER_RESEARCH                    = "respondePesquisa";
    public static final String WS_METHOD_GET_DILEMMA                        = "getDilema";
    public static final String WS_METHOD_ANSWER_DILEMMA                     = "respondeDilema";
    public static final String WS_METHOD_GET_CURIOSITIES                    = "getCuriosidades";
    public static final String WS_METHOD_GET_CITIZENSHIP                    = "getIdCitizenship";
    public static final String WS_METHOD_GET_PROFILE                        = "getPerfilUser";
    public static final String WS_METHOD_GET_RANK_PROFILE                   = "getPerfilUser?idUser=%s";
    public static final String WS_METHOD_UPLOAD_PICTURE                     = "uploadFotoUser";
    public static final String WS_METHOD_SEND_FORM_RP                       = "formRP";
    public static final String WS_METHOD_GET_USER_INFO                      = "getUserRank";
    public static final String WS_METHOD_UPDATE_USER_INFO                   = "atualizaUser";
    public static final String WS_METHOD_FORGOT_PASSWORD                    = "recuperaSenha";
    public static final String WS_METHOD_SAVE_TRACKING                      = "saveTracking";
    public static final String WS_METHOD_VERSION_CHECK                      = "getVersion";
    public static final String WS_METHOD_GET_HASHTAGS_MY_SPORTS             = "getUserFavList?t=1";
    public static final String WS_METHOD_GET_HASHTAGS_MY_TEAMS              = "getUserFavList?t=2";
    public static final String WS_METHOD_GET_HASHTAGS_MY_IDOLS              = "getUserFavList?t=3";
    public static final String WS_METHOD_GET_HASHTAGS_LIVE                  = "getUserFavList?t=4";
    public static final String WS_METHOD_SET_USER_APNS                      = "setUserApns";
    public static final String WS_METHOD_GET_DISTRIBUTORS                   = "getDistribuidores";
    public static final String WS_METHOD_GET_CARGOS                         = "getCargos";
    public static final String WS_METHOD_GET_VENDEDORES                     = "getVendedores?page=%s";
    public static final String WS_METHOD_GET_VENDEDORES_SEARCH              = "getVendedores?page=%s&search=%s";
    public static final String WS_METHOD_GET_VIDEO_HOME                     = "getVideoHome";

    public static final String WS_METHOD_GET_CATALOGS                       = "getCatalogos";
    public static final String WS_METHOD_GET_CATALOGS_SEARCH                = "getCatalogos?search=%s";
    public static final String WS_METHOD_ANSWER_QUIZ                        = "respondeQuiz";
    public static final String WS_METHOD_GET_INFORMATIVES                   = "getInformativos";
    public static final String WS_METHOD_GET_INFORMATIVES_SEARCH            = "getInformativos?search=%s";
    public static final String WS_METHOD_GET_INFORMATIVE_DETAILS            = "getInformativo?idInformativo=%s";
    public static final String WS_METHOD_GET_NEWS_LIST                      = "getNoticias?carrossel=%s";
    public static final String WS_METHOD_GET_NEWS_LIST_SEARCH               = "getNoticias?carrossel=%s&search=%s";
    public static final String WS_METHOD_SEND_MESSAGE                       = "setChat?texto=%s";
    public static final String WS_METHOD_GET_CHAT                           = "getChat?id=%s";
    public static final String WS_METHOD_GET_SEGMENTS                       = "getSegmentos";
    public static final String WS_METHOD_GET_TYPES                          = "getTipos";
    public static final String WS_METHOD_GET_TRAININGS                      = "getTreinamentos";
    public static final String WS_METHOD_GET_TRAININGS_SEARCH               = "getTreinamentos?search=%s";
    public static final String WS_METHOD_GET_RECIPES                        = "getReceitas";
    public static final String WS_METHOD_GET_RECIPES_CALC                   = "getCalcMenu";
    public static final String WS_METHOD_GET_RECIPES_HOME                   = "getReceitas?home=%s";
    public static final String WS_METHOD_GET_RECIPES_SEARCH                 = "getReceitas?search=%s";
    public static final String WS_METHOD_GET_TRAINING                       = "getTreinamento?id=%s";
    public static final String WS_METHOD_GET_RECIPE                         = "getReceita?id=%s";
    public static final String WS_METHOD_GET_PRODUCTS                       = "getProdutos";
    public static final String WS_METHOD_GET_PRODUCTS_SEARCH                = "getProdutos?search=%s";
    public static final String WS_METHOD_GET_PRODUCT                        = "getProduto?idProduto=%s";
    public static final String WS_METHOD_GET_AWARDS                         = "getPremios";
    public static final String WS_METHOD_GET_AWARDS_SEARCH                  = "getPremios?search=%s";
    public static final String WS_METHOD_REDEEM_AWARD                       = "resgataPremio";
    public static final String WS_METHOD_GET_PRODUCT_FILTER                 = "getProdutosFiltro";
    public static final String WS_METHOD_GET_WALL                           = "getMural";
    public static final String WS_METHOD_GET_WALL_SEARCH                    = "getMural?search=%s";
    public static final String WS_METHOD_GET_WALL_PARAM                     = "getMural?id=%s";
    public static final String WS_METHOD_GET_WALL_PARAM_SEARCH              = "getMural?id=%s&search=%s";
    public static final String WS_METHOD_GET_SPECIFIC_POST                  = "getMuralPost?id=%s";
    public static final String WS_METHOD_LIKE_WALL_POST                     = "curtirMural";
    public static final String WS_METHOD_POST_ON_WALL                       = "setMural?texto=%s";
    public static final String WS_METHOD_POST_ON_WALL_2                     = "setMural";
    public static final String WS_METHOD_COMMENT_WALL_POST                  = "setComentario";
    public static final String WS_METHOD_GET_WALL_USER                      = "getMuralUser";
    public static final String WS_METHOD_GET_WALL_USER_PARAM                = "getMuralUser?id=%s";
    public static final String WS_METHOD_GET_USER_ACHIEVEMENTS              = "getUserConquistas";
    public static final String WS_METHOD_DELETE_WALL_POST                   = "delMural";
    public static final String WS_METHOD_GET_CALCULATOR                     = "getCalculadora?id=%s&user=%s";
    public static final String WS_METHOD_SET_CALCULATOR_RECIPE              = "setCalcReceita";
    public static final String WS_METHOD_GET_RECIPES_WITH_CALC              = "getCalcMenu";
    public static final String WS_METHOD_GET_CATEGORIES                     = "getCategorias";
    public static final String WS_METHOD_GET_INGREDIENTS                    = "getIngredientes";
    public static final String WS_METHOD_GET_USER_RECIPES                   = "getUserReceitas";
    public static final String WS_METHOD_GET_USER_RECIPE_BY_ID              = "getUserReceita?id=%s";
    public static final String WS_METHOD_DEL_USER_RECIPE_BY_ID              = "delUserReceita";
    public static final String WS_METHOD_SET_END_SESSION                    = "setEndSession";
    public static final String WS_METHOD_GET_SEARCH                         = "buscaGeral?search=%s";
    public static final String WS_METHOD_GET_AGENDA                         = "getAgenda";
    public static final String WS_METHOD_GET_AGENDA_SEARCH                  = "getAgenda?search=%s";
    public static final String WS_METHOD_GET_AGENDA_TYPES                   = "getAgendaTipos?id=%s"; // Retorna os botoes do evento.
    public static final String WS_METHOD_GET_AGENDA_EVENT                   = "getAgendaEvento?idEvento=%s";
    public static final String WS_METHOD_SET_AGENDA_EVENT                   = "setAgendaUser";
    public static final String WS_METHOD_GET_NOTIFICATIONS                  = "getLogPushUser?page=%s";
    public static final String WS_METHOD_SET_NOTIFICATION                   = "setVisualizouPush?idPush=%s";
    public static final String WS_METHOD_GET_REGULAMENTO                    = "getRegulamento?t=%s";
    public static final String WS_METHOD_GET_REGULAMENTO_CARIBE             = "getRegulamento?t=2";
    public static final String WS_METHOD_SET_REGULAMENTO                    = "setRegulamento?rv=%s";
    public static final String WS_METHOD_GET_GIFS                           = "getGifs";
    public static final String WS_METHOD_SET_NEWS_COMMENT                   = "setComentarioNoticia";
    public static final String WS_METHOD_GET_PURATOS_CARDS                  = "getCardsCampanha";
    public static final String WS_METHOD_GET_CLIENT_LIST_ANAKIN             = "getClientesAnakin?page=%s&search=%s";
    public static final String WS_METHOD_GET_CLIENT_ANAKIN                  = "getClienteAnakin?id=%s";
    public static final String WS_METHOD_GET_CAMPANHA_ANAKIN                = "getCampanhasAnakin";
    public static final String WS_METHOD_GET_MURAL_ANAKIN                   = "getMuralAnakin?id=%s";
    public static final String WS_METHOD_GET_MURAL_ANAKIN_FIRST             = "getMuralAnakin";
    public static final String WS_METHOD_POST_DEL_MURAL_ANAKIN              = "delMuralAnakin";
    public static final String WS_METHOD_POST_SET_MURAL_ANAKIN              = "setMuralAnakin";
    public static final String WS_METHOD_GET_VOUCHERS_HOME                  = "getVouchersHome";
    public static final String WS_METHOD_GET_VOUCHERS_DETAILS               = "getVoucherDetails?id=%s";
    public static final String WS_METHOD_POST_VOUCHERS_CODE                 = "setVoucherCode";
    public static final String WS_METHOD_GET_VOUCHERS_PRIZES_LIST           = "getVoucherPrizesList?id=%s";
    public static final String WS_METHOD_POST_VOUCHERS_PRIZE_REDEEM         = "setVoucherPrizeRedeem";

    //SPFC=========
    //pesquisa
    public static final String WS_METHOD_SET_PESQUISA                       = "setPesquisa";
    public static final String WS_METHOD_GET_PESQUISA                       = "getPesquisa";
    public static final String WS_METHOD_GET_PESQUISA_ID                    = "getPesquisa?idPergunta=%s";
    public static final String WS_METHOD_GET_HOME_PESQUISA                  = "getPesquisasHome";
    //instagram
    public static final String WS_METHOD_GET_INSTAGRAM_STORIES              = "getInstagramStories";
    public static final String WS_METHOD_GET_INSTAGRAM_POSTS                = "getInstagramPosts";

    //quiz
    public static final String WS_METHOD_GET_QUIZES                         = "getQuizes";
    public static final String WS_METHOD_GET_QUIZES_ID                      = "getQuizes?id=%s";
    public static final String WS_METHOD_GET_QUIZES_PAGE                    = "getQuizes?page=%s";
    public static final String WS_METHOD_GET_QUIZ_HOME                      = "getQuizHome";
    public static final String WS_METHOD_GET_QUIZ                           = "getQuiz?id=%s";
    public static final String WS_METHOD_SET_QUIZ                           = "setQuiz";
    //noticias
    public static final String WS_METHOD_GET_NOTICIAS                       = "getNoticias";
    public static final String WS_METHOD_GET_NOTICIA                        = "getNoticia";
    public static final String WS_METHOD_GET_NOTICIA_ID                     = "getNoticia?id=%s";
    public static final String WS_METHOD_GET_NOTICIAS_ID                    = "getNoticias?home=%s";
    public static final String WS_METHOD_GET_NOTICIAS_BY_FILTER             = "getNoticias?filter=%s&page=%s";
    public static final String WS_METHOD_GET_NOTICIAS_PAGE                  = "getNoticias?page=%s";
    //videos
    public static final String WS_METHOD_GET_VIDEOS                         = "getVideos";
    public static final String WS_METHOD_GET_VIDEOS_ID                      = "getVideos?home=%s";
    //perfil
    public static final String WS_METHOD_GET_PERFIL                         = "getPerfil";
    public static final String WS_METHOD_GET_BADGES                         = "getBadges";
    public static final String WS_METHOD_GET_BADGES_PAGE                    = "getBadges?page=%s";
    //jogos
    public static final String WS_METHOD_GET_JOGOS                          = "getJogos";
    public static final String WS_METHOD_GET_JOGOS_HOME                     = "getJogos?home=%s";
    public static final String WS_METHOD_GET_LAST_RESULTADOS                = "getResultados";
    public static final String WS_METHOD_GET_LAST_RESULTADOS_HOME           = "getResultados?home=%s";
    public static final String WS_METHOD_GET_JOGO_DETALHES                  = "getJogoDetalhes?id=%s";
    //atletas
    public static final String WS_METHOD_GET_PLAYERS_TITULAR                = "getPlayersAtuais";
    public static final String WS_METHOD_GET_PLAYERS_PALPITES               = "getPlayersPalpites";
    public static final String WS_METHOD_GET_PLAYER                         = "getPlayer?id=%s";
    public static final String WS_METHOD_GET_PLAYER_CHART                   = "getPlayerChartDataAtual?id=%s&t=%s";
    //campo
    public static final String WS_METHOD_GET_ESCALACAO_HOME                 = "getEscalacaoHome";
    public static final String WS_METHOD_GET_ESCALACAO                      = "getEscalacao?id=%s";
    public static final String WS_METHOD_GET_PLAYERS_ESCALACAO              = "getPlayersEscalacao?id=%s";
    public static final String WS_METHOD_SET_PLAYERS_ESCALACAO              = "setEscalacao";
    //Loja
    public static final String WS_METHOD_GET_PRODUTOS                       = "getProdutos";
    public static final String WS_METHOD_GET_PRODUTOS_SEARCH                = "getProdutos?search=%s";
    public static final String WS_METHOD_GET_PRODUTO                        = "getProduto?id=%s";
    public static final String WS_METHOD_GET_PRODUTO_CUPOM                  = "getProduto?id=%s&idCupom=%s";
    public static final String WS_METHOD_GET_PRODUTO_REGULAMENTO            = "getRegulamentoProduto?id=%s";
    public static final String WS_METHOD_SET_COMPRA                         = "setCompra";
    //Ranking
    public static final String WS_METHOD_GET_RANK_GROUP                     = "getRankingsGrupos";
    public static final String WS_METHOD_GET_RANK_                          = "getRanking";
    public static final String WS_METHOD_GET_RANK_ID                        = "getRanking?id=%s";
    public static final String WS_METHOD_GET_RANK_ID_PAGE                   = "getRanking?id=%s&page=%s";
    //Get docs
    public static final String WS_METHOD_GET_DOCS                           = "getDocs";
    public static final String WS_METHOD_GET_PALPITES                       = "getPalpiteAtual";
    public static final String WS_METHOD_GET_PALPITES_RESPONDIDOS           = "getPalpitesRespondidos";
    public static final String WS_METHOD_GET_PALPITES_RESPONDIDOS_PAGE      = "getPalpitesRespondidos?page=%s";
    public static final String WS_METHOD_GET_PALPITES_DETAIS                = "getPalpiteResults?id=%s";
    public static final String WS_METHOD_SET_PALPITES                       = "setPalpite";
    //Socio torcedpr
    public static final String WS_METHOD_SET_SOCIO_TORCEDOR                 = "setSocioTorcedor";
    //Badges
    public static final String WS_METHOD_GET_NEW_BADGES                     = "getNewBadges";
    //Twitter
    public static final String WS_METHOD_GET_TWITTER_QUERY_INF              = "getTwitter";
    public static final String WS_METHOD_GET_TWITTER_QUERY_SUP              = "getTwitter?t=1";
    public static final String WS_METHOD_GET_BEARER_TOKEN                   = "https://api.twitter.com/oauth2/token";
    public static final String WS_METHOD_GET_TWEETS                         = "https://api.twitter.com/1.1/search/tweets.json?q=%s&result_type=mixed&count=%s&tweet_mode=extended";

    //PLAYER
    public static final String WS_METHOD_GET_PLAYLIST                         = "getPlaylists";
    public static final String WS_METHOD_GET_PLAYLIST_PAGE                    = "getPlaylists?page=%s";
    public static final String WS_METHOD_GET_AUDIO                            = "getAudio?id=%s";
    public static final String WS_METHOD_GET_AUDIO_HOME                       = "getAudiotecaHome";

    public static final String WS_METHOD_GET_NOTICIAS_FILTERS                 = "getNoticiasCategorias";

    public static final String WS_METHOD_GET_NOTIFICATION_HISTORY             = "getNotificationHistory?page=%s";
    public static final String WS_METHOD_GET_NOTIFICATION_MARK_ALL            = "setAllRead";

    public static final String WS_METHOD_GET_BANNER_HOME                      = "getBanner";
    public static final String WS_METHOD_GET_BANNER_MENU                      = "getMenu";

    public static final String WS_METHOD_GET_SOCIO_API                        = "getApiST";
    public static final String WS_METHOD_GET_SOCIO_LOGIN                      = "sessions";
    public static final String WS_METHOD_GET_CONSULTAR_SOCIO                  = "consultar?cpf=%s";
    public static final String WS_METHOD_GET_CONSULTAR_PROX_JOGOS             = "consultar_proximos_jogos";
    public static final String WS_METHOD_GET_CONSULTAR_BACK_JOGOS             = "consultar_jogos_anteriores";
    public static final String WS_METHOD_GET_SETORES                          = "consultar_setores?socio_id=%s&jogo_id=%s";
    public static final String WS_METHOD_GET_DIV_SETORES                      = "listar_divisoes?estadio_id=%s&estadio_setor_id=%s";
    public static final String WS_METHOD_GET_ASSENTOS_DIV                     = "listar_cadeiras?jogo_id=%s&estadio_setor_id=%s&estadio_setor_divisao_id=%s";
    public static final String WS_METHOD_GET_INGRESSOS                        = "listar_ingressos_setor?jogo_id=%s&estadio_setor_id=%s";
    public static final String WS_METHOD_GET_ACOMPANHANTES                    = "listar_pessoas_sem_checkin?socio_id=%s&jogo_id=%s";
    public static final String WS_METHOD_GET_CHECKINGS                        = "listar_checkins?socio_id=%s&jogo_id=%s";
    public static final String WS_METHOD_GET_FAZER_CHECKINGS                  = "fazer_checkin?socio_id=%s&jogo_id=%s&estadio_setor_id=%s&estadio_setor_divisao_id=%s&opcao=%s&valor=%s&ncc=%s&val=%s&cvv=%s&nomes=%s&estadio_cadeiras=%s";


    public static final String WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL            = "getLojaVirtualProdutos";
    public static final String WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_H          = "getLojaVirtualProdutos?home=%s";
    public static final String WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_F          = "getLojaVirtualProdutos?filter=%s";
    public static final String WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_S          = "getLojaVirtualProdutos?search=%s";
    public static final String WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_FS         = "getLojaVirtualProdutos?filter=%s&search=%s";
    public static final String WS_METHOD_GET_FILTERS_LOJA_VIRTUAL             = "getLojaVirtualProdutosCategorias";

    public static final String WS_METHOD_GET_MESSAGE_COVID                    = "getAviso?t=1";


    public static final String WS_METHOD_GET_SERVERS                          = "getServerList";
    public static final String WS_METHOD_GET_SERVERS_USER                     = "getServerList?idUser=%s";


    public static final String WS_METHOD_SET_SERVER_ERROR                     = "setTrackOnMainServer";

    public static final String WS_METHOD_GET_NOTIFICATION_COUNT               = "getNotificationCount";



    //Scores
    public static final int SCORES_PLAYOFFS_WIN = 1;
    public static final int SCORES_PLAYOFFS_LOSE = 1;

    public static final int SCORES_RESEARCHES = 1;

    //language ids
    public static final int LANG_ID_PORTUGUESE_BR = 1;
    public static final int LANG_ID_ENGLISH_USA = 2;

    //buttons ids
    public static final String BTN_ID_KEY = "btnIdKey";
    public static final String BTN_VAR_KEY = "btnVarKey";

    //Debug
    public static  final String ASYNCDEBUG = "Async-D";
    public static  final String APPDEBUG = "App-D";
    public static  final String SETDEBUG = "App-Set-D";

    //Push
    public static final String LOADING_SCREEN_KEY   = "screenKey";
    public static final int SCREEN_SEARCH           = 3;
    public static final int SCREEN_REDIRECT_PUSH    = 4;

}
