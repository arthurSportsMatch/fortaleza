package utils;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sportm.fortaleza.LoginActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.SearchActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.HttpGet;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sportm.fortaleza.WinBadgeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import data.UserData;
import informations.UserInformation;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import models.user.UserModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import repository.MainRepository;
import repository.user.UserRepository;

import static com.sportm.fortaleza.MainActivity.prefs;

/**
 * Created by rafael.verginelli on 7/12/16.
 */
public class AsyncOperation extends AsyncTask<Hashtable<String, Object>, Integer, Boolean> {

    private String TAG = "AsyncOperation";

    private Activity activity = null;
    private int TASK_ID = -1;
    private int OP_ID = -1;
    IAsyncOpCallback callback = null;

    private boolean taskCancelled = false;

    private int BTN_ID = -1;
    private int BTN_VAR = -1;
    private String BTN_VALUE = "";

    public void cancelRequest() {
        this.taskCancelled = true;
        UTILS.DebugLog(TAG, "Request Cancelled: [" + TASK_ID + "]");
    }

    public static final int TASK_ID_GET_GCM_TOKEN                   = 0;
    public static final int TASK_ID_SIGN_IN                         = 1;
    public static final int TASK_ID_SIGN_UP                         = 2;
    public static final int TASK_ID_GET_RANK                        = 3;
    public static final int TASK_ID_GET_SPORT_LIST                  = 4;
    public static final int TASK_ID_GET_SPORT_LIST_COUNTRY          = 5;
    public static final int TASK_ID_GET_SPORT_LIST_COUNTRY_EXPAND   = 6;
    public static final int TASK_ID_SET_USER_PLAYER                 = 7;
    public static final int TASK_ID_SET_USER_CLUB                   = 8;
    public static final int TASK_ID_GET_USER_SPORTS                 = 9;
    public static final int TASK_ID_FAV_USER_SPORT                  = 10;
    public static final int TASK_ID_GET_NEWS_HOME                   = 11;
    public static final int TASK_ID_GET_NEWS_GROUP                  = 12;
    public static final int TASK_ID_GET_NEWS_GROUP_EXPAND           = 13;
    public static final int TASK_ID_GET_NEWS                        = 14;
    public static final int TASK_ID_LIKE_NEWS                       = 15;
    public static final int TASK_ID_GET_RANDOM_MATCH                = 16;
    public static final int TASK_ID_MATCH_PLAYERS                   = 17;
    public static final int TASK_ID_GET_PLAYERS                     = 18;
    public static final int TASK_ID_GET_BONUS_ROUND                 = 19;
    public static final int TASK_ID_ANSWER_BONUS_ROUND              = 20;
    public static final int TASK_ID_GET_PLAYOFFS                    = 21;
    public static final int TASK_ID_ANSWER_PLAYOFFS                 = 22;
    public static final int TASK_ID_GET_RESEARCH                    = 23;
    public static final int TASK_ID_ANSWER_RESEARCH                 = 24;
    public static final int TASK_ID_GET_DILEMMA                     = 25;
    public static final int TASK_ID_ANSWER_DILEMMA                  = 26;
    public static final int TASK_ID_GET_CURIOSITIES                 = 27;
    public static final int TASK_ID_GET_CITIZENSHIP                 = 28;
    public static final int TASK_ID_GET_PROFILE                     = 32;
    public static final int TASK_ID_UPLOAD_PICTURE                  = 33;
    public static final int TASK_ID_SEND_FORM_RP                    = 34;
    public static final int TASK_ID_GET_USER_INFO                   = 36;
    public static final int TASK_ID_UPDATE_USER_INFO                = 37;
    public static final int TASK_ID_GET_RANK_PROFILE                = 38;
    public static final int TASK_ID_PASSWORD_FORGOTTEN              = 39;
    public static final int TASK_ID_SAVE_TRACKING                   = 40;
    public static final int TASK_ID_SEND_GOOGLE_PUSH                = 41;
    public static final int TASK_ID_RETRIVE_VIDEO_IMAGE             = 42;
    public static final int TASK_ID_CHECK_VERSION                   = 43;
    public static final int TASK_ID_SIGN_IN_TWITTER                 = 44;
    public static final int TASK_ID_SIGN_IN_DIGITS                  = 45;
    public static final int TASK_ID_GET_HASHTAGS_MY_IDOLS           = 46;
    public static final int TASK_ID_GET_HASHTAGS_MY_TEAMS           = 47;
    public static final int TASK_ID_GET_HASHTAGS_MY_SPORTS          = 48;
    public static final int TASK_ID_GET_HASHTAGS_LIVE               = 49;
    public static final int TASK_ID_SET_USER_APNS                   = 50;
    public static final int TASK_ID_GET_DISTRIBUTORS                = 51;
    public static final int TASK_ID_GET_CARGOS                      = 52;
    public static final int TASK_ID_GET_VIDEO_HOME                  = 53;
    public static final int TASK_ID_GET_QUIZ                        = 54;
    public static final int TASK_ID_GET_CATALOGS                    = 55;
    public static final int TASK_ID_ANSWER_QUIZ                     = 56;
    public static final int TASK_ID_GET_INFORMATIVES                = 57;
    public static final int TASK_ID_GET_INFORMATIVE_DETAILS         = 58;
    public static final int TASK_ID_GET_NEWS_LIST                   = 59;
    public static final int TASK_ID_SEND_MESSAGE                    = 60;
    public static final int TASK_ID_GET_CHAT                        = 61;
    public static final int TASK_ID_GET_SEGMENTS                    = 62;
    public static final int TASK_ID_GET_TYPES                       = 63;
    public static final int TASK_ID_GET_TRAININGS                   = 64;
    public static final int TASK_ID_GET_RECIPES                     = 65;
    public static final int TASK_ID_GET_PRODUCTS                    = 66;
    public static final int TASK_ID_GET_AWARDS                      = 67;
    public static final int TASK_ID_REDEEM_AWARD                    = 68;
    public static final int TASK_ID_GET_PRODUCTS_FILTER             = 69;
    public static final int TASK_ID_GET_WALL                        = 70;
    public static final int TASK_ID_LIKE_WALL_POST                  = 71;
    public static final int TASK_ID_POST_ON_WALL                    = 72;
    public static final int TASK_ID_COMMENT_WALL_POST               = 73;
    public static final int TASK_ID_GET_WALL_USER                   = 74;
    public static final int TASK_ID_GET_USER_ACHIEVEMENTS           = 75;
    public static final int TASK_ID_GET_TRAINING                    = 76;
    public static final int TASK_ID_GET_RECIPE                      = 77;
    public static final int TASK_ID_GET_SPECIFIC_POST               = 78;
    public static final int TASK_ID_DOWNLOAD_IMAGE_FROM_URL         = 79;
    public static final int TASK_ID_DELETE_WALL_POST                = 80;
    public static final int TASK_ID_GET_PRODUCT                     = 81;
    public static final int TASK_ID_GET_CALCULATOR                  = 82;
    public static final int TASK_ID_SET_CALCULATOR_RECIPE           = 83;
    public static final int TASK_ID_GET_RECIPES_WITH_CALC           = 84;
    public static final int TASK_ID_GET_INGREDIENTS                 = 85;
    public static final int TASK_ID_GET_CATEGORIES                  = 86;
    public static final int TASK_ID_GET_USER_RECIPES                = 87;
    public static final int TASK_ID_GET_USER_RECIPE_BY_ID           = 88;
    public static final int TASK_ID_DELT_USER_RECIPE_BY_ID          = 89;
    public static final int TASK_ID_SET_END_SESSION                 = 90;
    public static final int TASK_ID_GET_SEARCH                      = 91;
    public static final int TASK_ID_GET_NOTIFICATIONS               = 92;
    public static final int TASK_ID_SET_NOTIFICATION                = 93;
    public static final int TASK_ID_GET_AGENDA                      = 94;
    public static final int TASK_ID_GET_AGENDA_TYPES                = 95;
    public static final int TASK_ID_GET_AGENDA_EVENT                = 96;
    public static final int TASK_ID_SET_AGENDA_EVENT                = 97;
    public static final int TASK_ID_GET_REGULAMENTO                 = 98;
    public static final int TASK_ID_SET_REGULAMENTO                 = 99;
    public static final int TASK_ID_SET_NEWS_COMMENT                = 100;
    public static final int TASK_ID_GET_GIFS                        = 101;
    public static final int TASK_ID_GET_CARIBE_REGULATION           = 102;
    public static final int TASK_ID_GET_PURATOS_CARIBE_CARD         = 103;
    public static final int TASK_ID_GET_CLIENTS_ANAKIN              = 104;
    public static final int TASK_ID_GET_CLIENT_ANAKIN               = 105;
    public static final int TASK_ID_GET_CAMPANHA_ANAKIN             = 106;
    public static final int TASK_ID_GET_MURAL_ANAKIN                = 107;
    public static final int TASK_ID_POST_DEL_MURAL_ANAKIN           = 108;
    public static final int TASK_ID_POST_SET_MURAL_ANAKIN           = 109;
    public static final int TASK_ID_POST_PRIZE_REDEEM               = 110;
    public static final int TASK_ID_POST_VOUCHER_CODE               = 111;
    public static final int TASK_ID_GET_VOUCHER_HOME                = 112;
    public static final int TASK_ID_GET_VOUCHER_DETAILS             = 113;
    public static final int TASK_ID_GET_VOUCHER_PRIZES_LIST         = 114;
    public static final int TASK_ID_GET_VENDEDORES                  = 115;

    //region SPFC

    //Quiz
    public static final int TASK_ID_GET_QUIZES                      = 116;
    public static final int TASK_ID_SET_QUIZ                        = 117;
    //Pesquisa
    public static final int TASK_ID_GET_ULTIMAS_PESQUISAS           = 118;
    public static final int TASK_ID_GET_PESQUISAS                   = 119;
    public static final int TASK_ID_SET_PESQUISA                    = 120;
    //User
    public static final int TASK_ID_GET_USER                        = 121;
    public static final int TASK_ID_GET_BADGE                       = 122;
    //Home
    public static final int TASK_ID_GET_INSTAGRAM                   = 123;
    public static final int TASK_ID_GET_NOTICIAS                    = 124;
    public static final int TASK_ID_GET_NOTICIA                     = 152;
    //Partidas
    public static final int TASK_ID_GET_LAST_RESULTADOS             = 125;
    public static final int TASK_ID_GET_JOGOS                       = 126;
    public static final int TASK_ID_GET_PLAYERS_TITULAR             = 127;
    public static final int TASK_ID_GET_PLAYERS_PALPITES            = 154;
    public static final int TASK_ID_GET_PLAYER                      = 128;
    public static final int TASK_ID_GET_PLAYER_CHART                = 129;
    //Campo
    public static final int TASK_ID_GET_ESCALACAO_HOME              = 130;
    public static final int TASK_ID_GET_ESCALACAO                   = 131;
    public static final int TASK_ID_GET_PLAYERS_ESCALACAO           = 132;
    public static final int TASK_ID_SET_PLAYERS_ESCALACAO           = 133;
    //Loja
    public static final int TASK_ID_GET_PRODUTOS                    = 134;
    public static final int TASK_ID_GET_PRODUTO                     = 136;
    public static final int TASK_ID_GET_PRODUTO_REGULAMENTO         = 137;
    public static final int TASK_ID_SET_COMPRA                      = 141;
    //
    public static final int TASK_ID_GET_QUIZ_HOME                   = 135;
    //Rank
    public static final int TASK_ID_GET_RANK_GROUP                  = 138;
    public static final int TASK_ID_GET_RANK_                       = 139;
    public static final int TASK_ID_GET_DOCS                        = 140;
    //Palpites
    public static final int TASK_ID_GET_PALPITES                    = 142;

    public static final int TASK_ID_SET_PALPITES                    = 143;
    //Socio
    public static final int TASK_ID_GET_SOCIO                       = 144;
    public static final int TASK_ID_SET_SOCIO                       = 145;
    //Badges
    public static final int TASK_ID_GET_BADGES                      = 146;
    //Twitter
    public static final int TASK_ID_GET_HTML_PAGE_CODE              = 147;
    public static final int TASK_ID_GET_TWEETS                      = 148;
    public static final int TASK_ID_GET_BEARER_TOKEN                = 149;
    public static final int TASK_ID_GET_HASHTAGS_TWITTER_QUERY_SUP  = 150;
    public static final int TASK_ID_GET_HASHTAGS_TWITTER_QUERY_INF  = 151;

    public static final int TASK_ID_GET_PALPITES_LIST               = 155;
    public static final int TASK_ID_GET_PALPITES_DETAILS               = 156;


    //MatchDetails
    public static final int TASK_ID_GET_MATCH_DETAILS  = 153;

    public static final int TASK_ID_GET_PLAYLIST = 157;
    public static final int TASK_ID_GET_AUDIO = 158;
    public static final int TASK_ID_GET_AUDIO_HOME = 159;

    public static final int TASK_ID_GET_NOTICIAS_FILTERS = 160;
    public static final int TASK_ID_GET_NOTIFICATION_HISTORY = 161;
    public static final int TASK_ID_GET_NOTIFICATION_MARK_ALL = 162;


    public static final int TASK_ID_GET_BANNER_LOJA = 163;
    public static final int TASK_ID_GET_BANNER_MENU = 165;

    public static final int TASK_ID_GET_SOCIO_API = 164;

    public static final int TASK_ID_POST_SOCIO_LOGIN = 166;
    public static final int TASK_ID_GET_CONSULTAR_SOCIO = 167;
    public static final int TASK_ID_GET_CONSULTAR_PROX_JOGOS = 168;
    public static final int TASK_ID_GET_CONSULTAR_BACK_JOGOS = 169;
    public static final int TASK_ID_GET_CONSULTAR_SETORES = 170;
    public static final int TASK_ID_GET_CONSULTAR_DIV_SETORES = 171;
    public static final int TASK_ID_GET_ASSENTOSDIV = 172;
    public static final int TASK_ID_GET_ACOMPANHANTES = 173;
    public static final int TASK_ID_GET_INGRESSOS = 174;
    public static final int TASK_ID_GET_CHECKINS = 175;
    public static final int TASK_ID_GET_FAZER_CHECKIN = 176;

    public static final int TASK_ID_GET_PRODUTOS_LOJA_VIRTUAL = 177;
    public static final int TASK_ID_GET_FILTERS_LOJA_VIRTUAL = 178;

    public static final int TASK_ID_GET_SERVIDORES = 179;
    public static final int TASK_ID_SET_TRACK_ERROR = 180;
    public static final int TASK_ID_GET_MESSAGE = 181;
    public static final int TASK_ID_GET_NOTIFICATION_COUNT = 182;


    //endregion

    public AsyncOperation(Activity activity, int taskId, int opId, IAsyncOpCallback callback) {
        this.activity = activity;
        this.TASK_ID = taskId;
        this.OP_ID = opId;
        this.callback = callback;
    }

    public AsyncOperation(Activity activity, int taskId, int opId, IAsyncOpCallback callback, int btnId) {
        this.activity = activity;
        this.TASK_ID = taskId;
        this.OP_ID = opId;
        this.callback = callback;
        this.BTN_ID = btnId;

        Answers.getInstance().logCustom(new CustomEvent(TRACKING.getTrackingButtonName(btnId))
                .putCustomAttribute("id", btnId));
    }

    public AsyncOperation(Activity activity, int taskId, int opId, IAsyncOpCallback callback, int btnId, int btnVar) {
        this.activity = activity;
        this.TASK_ID = taskId;
        this.OP_ID = opId;
        this.callback = callback;
        this.BTN_ID = btnId;
        this.BTN_VALUE = String.valueOf(btnVar);
    }

    public AsyncOperation(Activity activity, int taskId, int opId, IAsyncOpCallback callback, int btnId, String btnVar) {
        this.activity = activity;
        this.TASK_ID = taskId;
        this.OP_ID = opId;
        this.callback = callback;
        this.BTN_ID = btnId;
        this.BTN_VALUE = btnVar;
    }

    void checkBadge (JsonObject response){
        if(response.has("newBadges")){
            Intent intent = new Intent(activity, WinBadgeActivity.class);
            activity.startActivity(intent);
        }
    }

    //Handles every task for an async operation with volley on this project.
    @Override
    protected Boolean doInBackground(Hashtable<String, Object>... params) {

        if (!(UTILS.isNetworkAvailable(activity))) {
            CallNoConnectionMessage();
            return false;
        }

        taskCancelled = false;

        try {
            switch (TASK_ID) {
                case TASK_ID_GET_VOUCHER_HOME: {
                    CallGetVoucherHome();
                }
                break;
                case TASK_ID_GET_VENDEDORES: {
                    CallTaskGetVendedores(params[0]);
                }
                break;
                case TASK_ID_GET_VOUCHER_DETAILS: {
                    CallGetVoucherDetails(params[0]);
                }
                break;
                case TASK_ID_GET_VOUCHER_PRIZES_LIST: {
                    CallGetVoucherPrizes(params[0]);
                }
                break;
                case TASK_ID_POST_VOUCHER_CODE: {
                    CallPostVoucherCode(params[0]);
                }
                break;
                case TASK_ID_POST_PRIZE_REDEEM: {
                    CallPostVoucherRedeem(params[0]);
                }
                break;
                case TASK_ID_GET_CLIENTS_ANAKIN:{
                    CallGetClientsAnakin(params[0]);
                }
                break;

                case TASK_ID_GET_CLIENT_ANAKIN:{
                    CallGetClientAnakin(params[0]);
                }
                break;

                case TASK_ID_GET_CAMPANHA_ANAKIN:{
                    CallGetCampanhaAnakin();
                }
                break;

                case TASK_ID_GET_MURAL_ANAKIN:{
                    CallGetMuralAnakin(params[0]);
                }
                break;

                case TASK_ID_POST_DEL_MURAL_ANAKIN:{
                    CallPostDelPostAnakin(params[0]);
                }
                break;

                case TASK_ID_POST_SET_MURAL_ANAKIN:{
                    CallPostSetPostAnakin(params[0]);
                }
                break;

                case TASK_ID_GET_PURATOS_CARIBE_CARD: {
                    callTaskGetPuratosCaribeCards();
                }
                case TASK_ID_GET_CARIBE_REGULATION: {
                    CallTaskGetCampaignRegulation(params[0]);
                }

                case TASK_ID_GET_GCM_TOKEN: {
                    CallTaskGetGcmToken();
                }
                break;

                case TASK_ID_SIGN_IN: {
                    CallTaskSignIn(params[0]);
                }
                break;

                case TASK_ID_SIGN_UP: {
                    CallTaskSignUp(params[0]);
                }
                break;

                case TASK_ID_GET_RANK: {
                    CallTaskGetRank();
                }
                break;

                case TASK_ID_GET_SPORT_LIST: {
                    CallTaskGetSportList();
                }
                break;

                case TASK_ID_GET_SPORT_LIST_COUNTRY: {
                    CallTaskGetSportListCountry(params[0]);
                }
                break;

                case TASK_ID_GET_SPORT_LIST_COUNTRY_EXPAND: {
                    CallTaskGetSportListCountryExpand(params[0]);
                }
                break;

                case TASK_ID_SET_USER_PLAYER:
                case TASK_ID_SET_USER_CLUB: {
                    CallTaskSetUserPlayerOrClub(params[0]);
                }
                break;

                case TASK_ID_GET_USER_SPORTS: {
                    CallTaskGetUserSports();
                }
                break;

                case TASK_ID_FAV_USER_SPORT: {
                    CallTaskFavUserSport(params[0]);
                }
                break;

                case TASK_ID_GET_NEWS_HOME: {
                    CallTaskGetNewsHome(params[0]);
                }
                break;

                case TASK_ID_GET_NEWS_GROUP: {
                    CallTaskGetNewsGroup();
                }
                break;

                case TASK_ID_GET_NEWS_GROUP_EXPAND: {
                    CallTaskGetNewsGroupExpand(params[0]);
                }
                break;

                case TASK_ID_GET_NEWS: {
                    CallTaskGetNews2(params[0]);
                }
                break;

                case TASK_ID_GET_NEWS_LIST: {
                    CallTaskGetNewsList(params[0]);
                }
                break;

                case TASK_ID_GET_RANDOM_MATCH: {
                    CallTaskGetRandomMatch();
                }
                break;

                case TASK_ID_MATCH_PLAYERS: {
                    CallTaskMatchPlayer(params[0]);
                }
                break;

                case TASK_ID_GET_PLAYERS: {
                    CallTaskGetPlayers();
                }
                break;

                case TASK_ID_GET_BONUS_ROUND: {
                    CallTaskGetBonusRound();
                }
                break;

                case TASK_ID_ANSWER_BONUS_ROUND: {
                    CallTaskAnswerBonusRound(params[0]);
                }
                break;

                case TASK_ID_GET_PLAYOFFS: {
                    CallTaskGetPlayoffs();
                }
                break;

                case TASK_ID_ANSWER_PLAYOFFS: {
                    CallTaskAnswerPlayoffs(params[0]);
                }
                break;

                case TASK_ID_LIKE_NEWS: {
                    CallTaskLikeNews(params[0]);
                }
                break;

                case TASK_ID_GET_RESEARCH: {
                    CallTaskGetResearch();
                }
                break;

                case TASK_ID_ANSWER_RESEARCH: {
                    CallTaskAnswerResearch(params[0]);
                }
                break;

                case TASK_ID_GET_DILEMMA: {
                    CallTaskGetDilemma();
                }
                break;

                case TASK_ID_ANSWER_DILEMMA: {
                    CallTaskAnswerDilemma(params[0]);
                }
                break;

                case TASK_ID_GET_CURIOSITIES: {
                    CallTaskGetCuriosities();
                }
                break;

                case TASK_ID_GET_CITIZENSHIP: {
                    CallTaskGetCitizenship();
                }
                break;

                case TASK_ID_GET_PROFILE: {
                    CallTaskGetProfile();
                }
                break;

                case TASK_ID_UPLOAD_PICTURE:{
                    CallTaskUploadPicture(params[0]);
                }
                break;

                case TASK_ID_SEND_FORM_RP:{
                    CallTaskSendFormRP(params[0]);
                }
                break;

                case TASK_ID_GET_USER_INFO: {
                    CallTaskGetUserInfo();
                }
                break;

                case TASK_ID_UPDATE_USER_INFO: {
                    CallTaskUpdateUserInfo(params[0]);
                }
                break;

                case TASK_ID_GET_RANK_PROFILE:{
                    CallTaskGetRankProfile(params[0]);
                }
                break;

                case TASK_ID_PASSWORD_FORGOTTEN:{
                    CallTaskForgotPassword(params[0]);
                }
                break;

                case TASK_ID_SAVE_TRACKING:{
                    CallTaskSaveTracking();
                }
                break;

                case TASK_ID_SEND_GOOGLE_PUSH: {
                    CallTaskSendGooglePush(params[0]);
                }
                break;

                case TASK_ID_RETRIVE_VIDEO_IMAGE:{
                    CallTaskRetrieveVideoBitmap(params[0]);
                }
                break;

                case TASK_ID_CHECK_VERSION:{
                    CallTaskVersionCheck();
                }
                break;

                case TASK_ID_SET_END_SESSION:{
                    CallTaskSetEndSession(params[0]);
                }
                break;

                case TASK_ID_SIGN_IN_TWITTER:{
                    CallTaskSignInTwitter(params[0]);
                }
                break;

                case TASK_ID_SIGN_IN_DIGITS:{
                    CallTaskSignInDigits(params[0]);
                }
                break;

                case TASK_ID_GET_HASHTAGS_MY_IDOLS:{
                    CallTaskGetHashtagsMyIdols();
                }
                break;

                case TASK_ID_GET_HASHTAGS_MY_TEAMS:{
                    CallTaskGetHashtagsMyTeams();
                }
                break;

                case TASK_ID_GET_HASHTAGS_MY_SPORTS:{
                    CallTaskGetHashtagsMySports();
                }
                break;

                case TASK_ID_GET_HASHTAGS_LIVE: {
                    CallTaskGetHashtagsLive();
                }
                break;

                case TASK_ID_SET_USER_APNS: {
                    CallTaskSetUserApns(params[0]);
                }
                break;

                case TASK_ID_GET_DISTRIBUTORS: {
                    CallTaskGetDistributors();
                }
                break;

                case TASK_ID_GET_CARGOS: {
                    CallTaskGetCargos();
                }
                break;

                case TASK_ID_GET_VIDEO_HOME: {
                    CallTaskGetVideoHome();
                }
                break;

                case TASK_ID_GET_QUIZ: {
                    CallTaskGetQuiz(params[0]);
                }
                break;

                case TASK_ID_GET_CATALOGS: {
                    CallTaskGetCatalogs();
                }
                break;

                case TASK_ID_ANSWER_QUIZ: {
                    CallTaskAnswerQuiz(params[0]);
                }
                break;

                case TASK_ID_GET_INFORMATIVES: {
                    CallTaskGetInformatives();
                }
                break;

                case TASK_ID_GET_INFORMATIVE_DETAILS: {
                    CallTaskGetInformative(params[0]);
                }
                break;

                case TASK_ID_SEND_MESSAGE: {
                    CallTaskSendMessage(params[0]);
                }
                break;

                case TASK_ID_GET_CHAT: {
                    CallTaskGetChat(params[0]);
                }
                break;

                case TASK_ID_GET_SEGMENTS: {
                    CallTaskGetSegments();
                }
                break;

                case TASK_ID_GET_TYPES: {
                    CallTaskGetTypes();
                }
                break;

                case TASK_ID_GET_TRAININGS: {
                    CallTaskGetTrainings();
                }
                break;

                case TASK_ID_GET_RECIPES: {
                    CallTaskGetRecipes(params[0]);
                }
                break;

                case TASK_ID_GET_PRODUCTS: {
                    CallTaskGetProducts();
                }
                break;

                case TASK_ID_GET_AWARDS: {
                    CallTaskGetAwards();
                }
                break;

                case TASK_ID_REDEEM_AWARD: {
                    CallTaskRedeemAward(params[0]);
                }
                break;

                case TASK_ID_GET_PRODUCTS_FILTER: {
                    CallTaskGetProductsFilter();
                }
                break;

                case TASK_ID_GET_WALL: {
                    CallTaskGetWall(params[0]);
                }
                break;

                case TASK_ID_LIKE_WALL_POST: {
                    CallTaskLikeWallPost(params[0]);
                }
                break;

                case TASK_ID_POST_ON_WALL: {
                    CallTaskPostOnWall2(params[0]);
                }
                break;

                case TASK_ID_COMMENT_WALL_POST: {
                    CallTaskCommentWallPost(params[0]);
                }
                break;

                case TASK_ID_GET_WALL_USER: {
                    CallTaskGetWallUser(params[0]);
                }
                break;

                case TASK_ID_GET_USER_ACHIEVEMENTS: {
                    CallTaskGetUserAchievements();
                }
                break;

                case TASK_ID_GET_TRAINING: {
                    CallTaskGetTraining(params[0]);
                }
                break;

                case TASK_ID_GET_RECIPE: {
                    CallTaskGetRecipe(params[0]);
                }
                break;

                case TASK_ID_GET_SPECIFIC_POST: {
                    CallTaskGetSpecificPost(params[0]);
                }
                break;

                case TASK_ID_DOWNLOAD_IMAGE_FROM_URL: {
                    CallTaskDownloadImageFromUrl(params[0]);
                }
                break;

                case TASK_ID_DELETE_WALL_POST: {
                    CallTaskDeleteWallPost(params[0]);
                }
                break;

                case TASK_ID_GET_PRODUCT: {
                    CallTaskGetProduct(params[0]);
                }
                break;

                case TASK_ID_GET_CALCULATOR: {
                    CallTaskGetCalculator(params[0]);
                }
                break;

                case TASK_ID_SET_CALCULATOR_RECIPE: {
                    CallTaskSetCalculatorRecipe(params[0]);
                }
                break;

                case TASK_ID_GET_RECIPES_WITH_CALC: {
                    CallTaskGetRecipesWithCalc();
                }
                break;

                case TASK_ID_GET_CATEGORIES: {
                    CallTaskGetCategories();
                }
                break;

                case TASK_ID_GET_INGREDIENTS: {
                    CallTaskGetIngredients();
                }
                break;

                case TASK_ID_GET_USER_RECIPES: {
                    CallTaskGetUserRecipes();
                }
                break;

                case TASK_ID_GET_USER_RECIPE_BY_ID: {
                    CallTaskGetUserRecipeById(params[0]);
                }
                break;

                case TASK_ID_DELT_USER_RECIPE_BY_ID: {
                    CallTaskDeleteUserRecipe(params[0]);
                }
                break;

                case TASK_ID_GET_SEARCH: {
                    CallTaskGetSearch(params[0]);
                }
                break;

                case TASK_ID_GET_NOTIFICATIONS: {
                    CallTaskGetNotifications(params[0]);
                }
                break;

                case TASK_ID_SET_NOTIFICATION: {
                    CallTaskSetNotification(params[0]);
                }
                break;

                case TASK_ID_GET_AGENDA: {
                    CallTaskGetAgenda();
                }
                break;

                case TASK_ID_GET_AGENDA_TYPES: {
                    CallTaskGetAgendaTypes(params[0]);
                }
                break;

                case TASK_ID_GET_AGENDA_EVENT: {
                    CallTaskGetAgendaEvent(params[0]);
                }
                break;

                case TASK_ID_SET_AGENDA_EVENT: {
                    CallTaskSetAgendaEvent(params[0]);
                }
                break;

                case TASK_ID_GET_REGULAMENTO: {
                    CallTaskGetRegulamento(params[0]);
                }
                break;

                case TASK_ID_SET_REGULAMENTO: {
                    CallTaskSetRegulamento(params[0]);
                }
                break;

                case TASK_ID_SET_NEWS_COMMENT: {
                    CallTaskSetNewsComment(params[0]);
                }
                break;

                case TASK_ID_GET_GIFS: {
                    CallTaskGetGifs();
                }
                break;
                case TASK_ID_GET_QUIZES: {
                    CallTaskGetQuizes(params[0]);
                }
                break;
                case TASK_ID_SET_QUIZ: {
                    CallTaskSetQuiz(params[0]);
                }
                break;
                case TASK_ID_GET_ULTIMAS_PESQUISAS: {
                    CallTaskGetUltimasPesquisas();
                }
                break;
                case TASK_ID_GET_PESQUISAS: {
                    CallTaskGetPesquisas(params[0]);
                }
                break;

                case TASK_ID_SET_PESQUISA: {
                    CallTaskSetPesquisa(params[0]);
                }
                break;
                case TASK_ID_GET_USER: {
                    CallTaskGetUser();
                }
                break;
                case TASK_ID_GET_BADGE: {
                    CallTaskGetBadge(params[0]);
                }
                break;
                case TASK_ID_GET_INSTAGRAM: {
                    CallTaskGetInstagram();
                }
                break;
                case TASK_ID_GET_NOTICIAS: {
                    CallTaskGetNoticias(params[0]);
                }
                break;
                case TASK_ID_GET_NOTICIA: {
                    CallTaskGetNoticia(params[0]);
                }
                break;

                case TASK_ID_GET_LAST_RESULTADOS: {
                    CallTaskGetLastResultados(params[0]);
                }
                break;

                case TASK_ID_GET_JOGOS: {
                    CallTaskGetLastJogos(params[0]);
                }
                break;

                case TASK_ID_GET_PLAYERS_TITULAR: {
                    CallTaskGetPlayersTitular();
                }
                break;
                case TASK_ID_GET_PLAYERS_PALPITES: {
                    CallTaskGetPlayersPalpites();
                }
                break;

                case TASK_ID_GET_PLAYER: {
                    CallTaskGetPlayer(params[0]);
                }
                break;

                case TASK_ID_GET_PLAYER_CHART: {
                    CallTaskGetPlayerChart(params[0]);
                }
                break;

                case TASK_ID_GET_ESCALACAO_HOME: {
                    CallTaskGetEscalacaoHome();
                }
                break;
                case TASK_ID_GET_ESCALACAO: {
                    CallTaskGetEscalacao(params[0]);
                }
                break;
                case TASK_ID_GET_PLAYERS_ESCALACAO: {
                    CallTaskGetPlayerEscalacao(params[0]);
                }
                break;

                case TASK_ID_SET_PLAYERS_ESCALACAO: {
                    CallTaskSetPlayerEscalacao(params[0]);
                }
                break;

                case TASK_ID_GET_PRODUTOS: {
                    CallTaskGetProdutos(params[0]);
                }
                break;

                case TASK_ID_GET_QUIZ_HOME: {
                    CallTaskGetQuizHome();
                }
                break;
                case TASK_ID_GET_PRODUTO: {
                    CallTaskGetProduto(params[0]);
                }
                break;
                case TASK_ID_GET_PRODUTO_REGULAMENTO: {
                    CallTaskGetProdutoRegulamento(params[0]);
                }
                break;
                case TASK_ID_GET_RANK_GROUP: {
                    CallTaskGetRankGroup();
                }
                break;
                case TASK_ID_GET_RANK_: {
                    CallTaskGetRanking(params[0]);
                }
                break;
                case TASK_ID_GET_DOCS: {
                    CallTaskGetDocs();
                }
                break;
                case TASK_ID_SET_COMPRA: {
                    CallTaskSetCompra(params[0]);
                }
                break;
                case TASK_ID_GET_PALPITES: {
                    CallTaskGetPalpite();
                }
                break;
                case TASK_ID_SET_PALPITES: {
                    CallTaskSetPalpite(params[0]);
                }
                break;
                case TASK_ID_GET_SOCIO: {
                    GetSocioTorcerdorAPI(params[0]);
                }
                break;
                case TASK_ID_SET_SOCIO: {
                    SetSocioTorcerdor(params[0]);
                }
                break;
                case TASK_ID_GET_BADGES: {
                    GetNewBadges();
                }
                break;
                case TASK_ID_GET_HTML_PAGE_CODE: {
                    CallTaskGetHtmlPageCode(params[0]);
                }
                break;
                case TASK_ID_GET_TWEETS: {
                    CallTaskGetTweets(params[0]);
                }
                break;
                case TASK_ID_GET_BEARER_TOKEN: {
                    CallTaskGetBearerToken();
                }
                break;
                case TASK_ID_GET_HASHTAGS_TWITTER_QUERY_SUP: {
                    CallTaskGetHashtagsTwitterQuerySup();
                }
                break;
                case TASK_ID_GET_HASHTAGS_TWITTER_QUERY_INF: {
                    CallTaskGetHashtagsTwitterQueryInf();
                }
                break;

                case TASK_ID_GET_MATCH_DETAILS: {
                    CallTaskGetMAtchDetails(params[0]);
                }
                break;

                case TASK_ID_GET_PALPITES_LIST: {
                    CallTaskGetPalpiteList(params[0]);
                }
                break;
                case TASK_ID_GET_PALPITES_DETAILS: {
                    CallTaskGetPalpiteDetails(params[0]);
                }
                break;
                case TASK_ID_GET_PLAYLIST: {
                    CallTaskGetPlaylist(params[0]);
                }
                break;
                case TASK_ID_GET_AUDIO: {
                    CallTaskGetAudio(params[0]);
                }
                break;
                case TASK_ID_GET_AUDIO_HOME: {
                    CallTaskGetAudioHome();
                }
                break;
                case TASK_ID_GET_NOTICIAS_FILTERS: {
                    CallTaskGetNoticiasFilters();
                }
                break;
                case TASK_ID_GET_NOTIFICATION_HISTORY: {
                    CallTaskGetNotificationHistory(params[0]);
                }
                break;
                case TASK_ID_GET_NOTIFICATION_MARK_ALL: {
                    CallTaskGetNotificationMarkAll();
                }
                break;

                case TASK_ID_GET_BANNER_LOJA: {
                    CallTaskGetBannerLoja();
                }
                break;
                case TASK_ID_GET_BANNER_MENU: {
                    CallTaskGetBannerMenu();
                }
                break;
                case TASK_ID_GET_SOCIO_API: {
                    CallTaskGetSocioApi();
                }
                break;
                case TASK_ID_POST_SOCIO_LOGIN: {
                    CallTaskSetSocioLogin(params[0]);
                }
                break;
                case TASK_ID_GET_CONSULTAR_SOCIO: {
                    CallTaskGetConsultarSocio(params[0]);
                }
                break;
                case TASK_ID_GET_CONSULTAR_PROX_JOGOS: {
                    CallTaskGetConsultarProxJogos(params[0]);
                }
                break;
                case TASK_ID_GET_CONSULTAR_BACK_JOGOS: {
                    CallTaskGetConsultarBackJogos(params[0]);
                }
                break;
                case TASK_ID_GET_CONSULTAR_SETORES: {
                    CallTaskGetConsultarSetores(params[0]);
                }
                break;
                case TASK_ID_GET_CONSULTAR_DIV_SETORES: {
                    CallTaskGetConsultarDivSetores(params[0]);
                }
                break;
                case TASK_ID_GET_ASSENTOSDIV: {
                    CallTaskGetAssentosDiv(params[0]);
                }
                break;
                case TASK_ID_GET_ACOMPANHANTES: {
                    CallTaskGetAcompanhantes(params[0]);
                }
                break;
                case TASK_ID_GET_INGRESSOS: {
                    CallTaskGetIngressos(params[0]);
                }
                break;

                case TASK_ID_GET_CHECKINS: {
                    CallTaskGetCheckings(params[0]);
                }
                break;
                case TASK_ID_GET_FAZER_CHECKIN: {
                    CallTaskGetFazerCheckin(params[0]);
                }
                break;
                case TASK_ID_GET_PRODUTOS_LOJA_VIRTUAL: {
                    CallTaskGetProdutosLojaVirtual(params[0]);
                }
                break;
                case TASK_ID_GET_FILTERS_LOJA_VIRTUAL: {
                    CallTaskGetFiltersLojaVirtual();
                }
                break;
                case TASK_ID_GET_MESSAGE: {
                    CallGetMessageCovid();
                }
                case TASK_ID_GET_NOTIFICATION_COUNT: {
                    CallTaskGetNotificationCount();
                }
                break;
                case TASK_ID_GET_SERVIDORES: {
                    CallTaskGetServers(params[0]);
                }
                break;
                case TASK_ID_SET_TRACK_ERROR: {
                    CallTaskErrorTracking();
                }
                break;

            }
        } catch (Exception e) {
            UTILS.DebugLog(TAG, e);
        } catch (Throwable throwable) {
            UTILS.DebugLog(TAG, throwable);
        }

        return false;
    }

    private void callTaskGetPuratosCaribeCards() throws JSONException {

        final String localTAG = "cards";

        //Look into CallTaskGetRegulamento() for a a
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //String finalMethod = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_REGULAMENTO);

        String url = "https://robertodeveloper.com/json_campanha.php";

        UTILS.DebugLog(localTAG, "Getting this: " + url);
        //heads up: this get method its not passing any params to the web api at the moment.
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(localTAG, "Response: " + response);
                Log.d(localTAG, "response " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(localTAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(3, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(localTAG + " (GetRegulamento)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(localTAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(localTAG, e);
                    }
                    callback.CallHandler(3, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(3, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
        Log.d(localTAG, "fetching data from web api..");
    }

    private void CallTaskGetCampaignRegulation(Hashtable<String, Object> _param) {

        final String localTAG = "campaign";

        //Look into CallTaskGetRegulamento() for a a
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final String paramT = String.valueOf(_param.get("t"));

        String finalMethod = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_REGULAMENTO, paramT);

        UTILS.DebugLog(localTAG, "Getting this: " + finalMethod);
        //heads up: this get method its not passing any params to the web api at the moment.
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, finalMethod, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(localTAG, "Response: " + response);
                Log.d(localTAG, "response " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(localTAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(localTAG + " (GetRegulamento)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(localTAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(localTAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

        Log.d(localTAG, "fetching data from web api..");
    }

    public interface IAsyncOpCallback {
        void CallHandler(int opId, JSONObject response, boolean success);

        void OnAsyncOperationSuccess(int opId, JSONObject response);

        void OnAsyncOperationError(int opId, JSONObject response);
    }

    Handler noConnectionMessageHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            Toast.makeText(activity, activity.getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show();
            return false;
        }
    });

    private String AddBtnIdToApiHeader(){

        String str = ":";
        if(BTN_ID != -1){
            str += String.valueOf(BTN_ID);
        }
        if(BTN_VAR != -1){
            str += "/" + String.valueOf(BTN_VAR);
        }

        return str;
    }

    private String AddTokenToApiHeader(){
        return (":" + UserInformation.getUserData().getToken());
    }

    public String getApiHeader(){
        if(!BTN_VALUE.isEmpty()){
            return TRACKING.buildHeader(BTN_ID, BTN_VALUE);
        }
        return TRACKING.buildHeader(BTN_ID, BTN_VAR);
    }

    private void CallNoConnectionMessage() {
        noConnectionMessageHandler.sendEmptyMessage(0);
    }

    private void CallTokenAuthError() {
        UTILS.ClearInformations(activity);
        Intent newIntent = new Intent(activity, LoginActivity.class);
        Bundle b = new Bundle();
        b.putBoolean(LoginActivity.BUNDLE_KEY_TOKEN_MESSAGE, true);
        newIntent.putExtras(b);
        activity.startActivity(newIntent);
        activity.finish();
    }

    private void CallTaskGetGcmToken() throws JSONException {

        boolean success = false;
        JSONObject response = new JSONObject();

        int maxTries = 6;
        int triesCount = 0;

        while ((!success) && (triesCount < maxTries)) {
            triesCount++;
            try {

                String gcmToken = FirebaseInstanceId.getInstance().getToken();
                UTILS.DebugLog(TAG, "Got this token: " + gcmToken);
                if( (gcmToken != null) && (gcmToken.length() > 0) ) {
                    response.put("gcmToken", gcmToken);
                    success = true;
                }
            } catch (JSONException e) {
                UTILS.DebugLog(TAG, e);
                if (response.has("gcmToken")) {
                    response.remove("gcmToken");
                }
                response.put("error", e.toString());
                success = false;
            }

            try {
                Thread.sleep(350);
            } catch (InterruptedException e) {
                UTILS.DebugLog(TAG, e);
            }
        }

        callback.CallHandler(OP_ID, response, success);
    }

    private void CallTaskSignIn(Hashtable<String, Object> _params) throws JSONException {

        //{"email":"raf@raf.com","senha":"123","os":2,"osv":22,"dev":"Motorola XT1033","apv":"1.0"}

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int os = CONSTANTS.OS_ID;  //(IOS envie 1 e Android envie 2)
        final int osv = Build.VERSION.SDK_INT;  //(versão do sistema operacional)
        final String dev = UTILS.getDeviceName();  //(modelo do device)
        String _apv = "";
        try{
            _apv = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            UTILS.DebugLog(TAG, e);
        }
        final String apv = _apv;

        JSONObject params = new JSONObject();
        try {
            params.put("email", _params.get("email"));
            params.put("senha", _params.get("senha"));
            if( ( _params.containsKey("apns") ) && (String.valueOf(_params.get("apns")).length() > 0) ) {
                params.put("apns", _params.get("apns"));
            }
            params.put("os", os);
            params.put("osv", osv);
            params.put("dev", dev);
            params.put("apv", apv);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        String metod = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SIGN_IN;

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " To: " + metod);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, metod, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_NOT_FOUND) {
                            success = false;
                            response.put("error", activity.getString(R.string.error_invalid_email_or_password));
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8"
                );
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSignInTwitter(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int os = CONSTANTS.OS_ID;  //(IOS envie 1 e Android envie 2)
        final int osv = Build.VERSION.SDK_INT;  //(versão do sistema operacional)
        final String dev = UTILS.getDeviceName();  //(modelo do device)
        String _apv = "";
        try{
            _apv = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            UTILS.DebugLog(TAG, e);
        }
        final String apv = _apv;

        JSONObject params = new JSONObject();
        try {
            params.put("TWuserID", String.valueOf(_params.get("TWuserID")));
            params.put("TWuserName", _params.get("TWuserName"));
            params.put("TWauthToken", _params.get("TWauthToken"));
            params.put("TWauthTokenSecret", _params.get("TWauthTokenSecret"));
            if( ( _params.containsKey("apns") ) && (String.valueOf(_params.get("apns")).length() > 0) ) {
                params.put("apns", _params.get("apns"));
            }
            params.put("os", os);
            params.put("osv", osv);
            params.put("dev", dev);
            params.put("apv", apv);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params.toString());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SIGN_IN_TWITTER, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_NOT_FOUND) {
                            success = false;
                            response.put("error", activity.getString(R.string.error_invalid_email_or_password));
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSignInDigits(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int os = CONSTANTS.OS_ID;  //(IOS envie 1 e Android envie 2)
        final int osv = Build.VERSION.SDK_INT;  //(versão do sistema operacional)
        final String dev = UTILS.getDeviceName();  //(modelo do device)
        String _apv = "";
        try{
            _apv = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            UTILS.DebugLog(TAG, e);
        }
        final String apv = _apv;

        JSONObject params = new JSONObject();
        try {
            params.put("DGphoneNumber", _params.get("DGphoneNumber"));
            params.put("DGauthToken", _params.get("DGauthToken"));
            params.put("DGauthTokenSecret", _params.get("DGauthTokenSecret"));
            params.put("DGemailAddress", _params.get("DGemailAddress"));
            params.put("DGemailAddressIsVerified", _params.get("DGemailAddressIsVerified"));
            params.put("DGuserID", String.valueOf(_params.get("DGuserID")));
            if( ( _params.containsKey("apns") ) && (String.valueOf(_params.get("apns")).length() > 0) ) {
                params.put("apns", _params.get("apns"));
            }
            params.put("os", os);
            params.put("osv", osv);
            params.put("dev", dev);
            params.put("apv", apv);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params.toString());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SIGN_IN_DIGITS, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_NOT_FOUND) {
                            success = false;
                            response.put("error", activity.getString(R.string.error_invalid_email_or_password));
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSignUp(final Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try {
            params.put("nome", _params.get("nome"));
            params.put("email", _params.get("email"));
            params.put("cpf", _params.get("cpf"));
//            if(_params.containsKey("dataNascimento")) {
//                params.put("dataNascimento", _params.get("dataNascimento"));
//            }
            params.put("empresa", _params.get("empresa"));
            params.put("idCargo", _params.get("idCargo"));
            params.put("idVendedor", _params.get("idVendedor"));
//            params.put("codigoVendedor", _params.get("codigoVendedor"));
            if(_params.containsKey("celular")) {
                params.put("celular", _params.get("celular"));
            }
            params.put("senha", _params.get("senha"));
            params.put("aceitaNews", _params.get("aceitaNews"));
            params.put("apns", _params.get("apns"));

            if(_params.containsKey("idUser")) {
                params.put("idUser", _params.get("idUser"));
            }
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SIGN_UP);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SIGN_UP, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            if ((response.has("Object")) && (response.get("Object") != JSONObject.NULL)){

                                JSONObject json = response.getJSONObject("Object");

                                json.put("nickname", _params.get("nome"));
                                json.put("email", _params.get("email"));
                                json.put("acceptNews", _params.get("news"));

                                success = true;
                            }
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRank() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.WS_METHOD_GET_RANK);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANK, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSportList() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.WS_METHOD_GET_SPORT_LIST);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_SPORT_LIST, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSportListCountry(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final String idSport = String.valueOf(_params.get("idSport"));
        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_SPORT_LIST_COUNTRY, idSport);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("idSport", idSport);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSportListCountryExpand(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final String idSport = String.valueOf(_params.get("idSport"));
        final String idCountry = String.valueOf(_params.get("idCountry"));

        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_SPORT_LIST_COUNTRY_EXPAND, idSport, idCountry);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("idSport", idSport);
                    response.put("idCountry", idCountry);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetUserPlayerOrClub(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final boolean isPlayer = _params.containsKey("idPlayer");

        final String idSport = String.valueOf(_params.get("idSport"));
        final String idCountry = String.valueOf(_params.get("idCountry"));
        final String idPlayerOrClub = String.valueOf(isPlayer ? _params.get("idPlayer") : _params.get("idClub"));

        JSONObject params = new JSONObject();
        try {
            params.put("idSport", idSport);
            params.put(isPlayer ? "idPlayer" : "idClub", idPlayerOrClub);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        String method = CONSTANTS.serverURL + (isPlayer ? CONSTANTS.WS_METHOD_SET_USER_PLAYER : CONSTANTS.WS_METHOD_SET_USER_CLUB);

        UTILS.DebugLog(TAG, "Sending this:\n" + params + "\nto: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("idSport", idSport);
                    response.put("idCountry", idCountry);
                    response.put(isPlayer ? "idPlayer" : "idClub", idPlayerOrClub);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ((response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED)) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetUserSports() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_SPORTS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_SPORTS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskFavUserSport(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final String idSport = String.valueOf(_params.get("idSport"));

        JSONObject params = new JSONObject();
        try {
            params.put("idSport", idSport);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_FAV_USER_SPORT;

        UTILS.DebugLog(TAG, "Sending this:\n" + params + "\nto: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("idSport", idSport);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ((response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED)) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNewsHome(Hashtable<String, Object> params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

//        UTILS.DebugLog(TAG, "ApiHeader Value: " + UTILS.getApiHeaderValue() + AddBtnIdToApiHeader() + AddTokenToApiHeader());

        int id = (int)params.get("id");
        String method = String.format(CONSTANTS.WS_METHOD_GET_NEWS_HOME, String.valueOf(id));

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                Log.d("newsDebug", "Response News() " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNewsGroup() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int os = CONSTANTS.OS_ID;  //(IOS envie 1 e Android envie 2)
        SecurePreferences prefs = new SecurePreferences(activity.getApplicationContext(), CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        String gcm = prefs.getString(CONSTANTS.SHARED_PREFS_KEY_GCM_TOKEN, UserInformation.getUserData().getGcm());

        String method = (gcm.length() > 0) ?
                (String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS_GROUP_APNS, gcm, os)) :
                (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS_GROUP);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNewsGroupExpand(final Hashtable<String, Object> params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS_GROUP_EXPAND, String.valueOf(params.get("idGrupo")));

        UTILS.DebugLog(TAG, method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("idGrupo", params.get("idGrupo"));
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNews(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int idNoticia = (int) _params.get("idNoticia");
        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS, String.valueOf(idNoticia));

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            if ((response.has("Object")) && (response.get("Object") != JSONObject.NULL)) {
                                JSONObject json = response.getJSONObject("Object");
                                json.put("id", idNoticia);
                                success = true;
                            }
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNews2(Hashtable<String, Object> _params) {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int idNoticia = (int) _params.get("idNoticia");
        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS, String.valueOf(idNoticia));

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            if ((response.has("Object")) && (response.get("Object") != JSONObject.NULL)) {
                                JSONObject json = response.getJSONObject("Object");
                                json.put("id", idNoticia);
                                success = true;
                            }
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNewsList(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int carrossel = (int) _params.get("carrossel");
        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS_LIST, String.valueOf(carrossel));


        if(!SearchActivity.getToSearch().isEmpty()){
            method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEWS_LIST_SEARCH, String.valueOf(carrossel), String.valueOf(SearchActivity.getToSearch()));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            if ((response.has("Object")) && (response.get("Object") != JSONObject.NULL)) {
                                success = true;
                            }
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRandomMatch() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANDOM_MATCH);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANDOM_MATCH, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskMatchPlayer(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int p1 = (int) _params.get("p1");
        final int p2 = (int) _params.get("p2");

        int tempPs = 0;

        boolean sendPs = (_params.containsKey("ps"));
        if(sendPs){
            tempPs = (int)_params.get("ps");
        }

        final int ps = tempPs;

        String method = sendPs ?
                String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_MATCH_RANDOM_MATCH_PLAYERS, p1, p2, ps) :
                String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_MATCH_PLAYERS, p1, p2);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetPlayers() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYERS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYERS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetBonusRound() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_BONUS_ROUND);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_BONUS_ROUND, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskAnswerBonusRound(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final boolean isRight = (boolean)_params.get("isRight");

        JSONObject params = new JSONObject();
        try{
            params.put("questionId", (int)_params.get("questionId"));
            params.put("choiceId", (int)_params.get("choiceId"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_BONUS_ROUND);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_BONUS_ROUND, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("isRight", isRight);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetPlayoffs() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYOFFS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYOFFS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskAnswerPlayoffs(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final boolean isRight = (boolean)_params.get("isRight");

        JSONObject params = new JSONObject();
        try{
            params.put("questionId", (int)_params.get("questionId"));
            params.put("choiceId", (int)_params.get("choiceId"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_PLAYOFFS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_PLAYOFFS, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    response.put("isRight", isRight);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskLikeNews(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int idNoticia = (int)_params.get("idNoticia");

        JSONObject params = new JSONObject();
        try{

            params.put("idNoticia", idNoticia);
            params.put("curtiu", (int)_params.get("curtiu"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_LIKE_NEWS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_LIKE_NEWS, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            response.put("idNoticia", idNoticia);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetResearch() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RESEARCH);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RESEARCH, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskAnswerResearch(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();

        boolean skipQuestion = false;

        try{
            int choiceId = (int)_params.get("choiceId");
            params.put("idPergunta", (int)_params.get("questionId"));
            params.put("idResposta", (choiceId <= 0) ? "null" : choiceId);
            if(_params.containsKey("skipQuestion")) {
                skipQuestion = (boolean) _params.get("skipQuestion");
                UTILS.DebugLog(TAG, "1");
            }
            if(_params.containsKey("aberta")) {
                params.put("aberta", String.valueOf(_params.get("aberta")));
            }
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        final boolean _skipQuestion = skipQuestion;
        UTILS.DebugLog(TAG, "skip: " + skipQuestion);


        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_RESEARCH);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_RESEARCH, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                            response.put("skipQuestion", _skipQuestion);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetDilemma() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_DILEMMA);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_DILEMMA, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskAnswerDilemma(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try{
            params.put("questionId", (int)_params.get("questionId"));
            params.put("choiceId", (int)_params.get("choiceId"));
            params.put("idDilema", (int)_params.get("idDilema"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_DILEMMA);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_DILEMMA, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCuriosities() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CURIOSITIES);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CURIOSITIES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCitizenship() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CITIZENSHIP);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CITIZENSHIP, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetProfile() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PROFILE);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PROFILE, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskUploadPicture(Hashtable<String, Object> _params) {

        String path = String.valueOf(_params.get("sourceFile"));

        //fix parameter
        if (path.startsWith("file:")) {
            path = path.replace("file:", "");
        }
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_UPLOAD_PICTURE;
        UTILS.DebugLog(TAG, "sending: " + path + " to: " + method);

        File newFile = new File(path);
        String fileName = newFile.getName();

        try {
            if (!fileName.contains(" ")) {

                Log.d("Async", "File size " + (newFile.length()/1024));
                //getting file size
                if((newFile.length()/1024) <= 2000){

                }else{
                    newFile = CompressFile.getCompressedImageFile(newFile, activity);
                }

            } else {

                byte[] data = new byte[(int) newFile.length()];

                try {
                    FileInputStream fileInputStream = new FileInputStream(newFile);
                    fileInputStream.read(data);
                    fileInputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String fullPath = newFile.getPath();
                String justPath = fullPath.replace(fileName, "");
                fileName = fileName.replaceAll("\\s", "");
                String newPath = justPath.concat(fileName);

                FileOutputStream fos = new FileOutputStream(newPath);
                fos.write(data);
                fos.close();

                File fileFixed = new File(newPath);

                Log.d("Async", "File size" + (fileFixed.length()/1024));
                //getting file size
                if((fileFixed.length()/1024) <= 2000){
                    newFile = fileFixed;
                }else{
                    newFile = CompressFile.getCompressedImageFile(fileFixed, activity);
                }
            }

            OkHttpClient client = new OkHttpClient();

            MediaType fileContentType = MediaType.parse("File/*");

            assert newFile != null;
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", fileName,
                            RequestBody.create(fileContentType, newFile))
                    .build();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(method)
                    .addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader())
                    .post(requestBody)
                    .build();


            try (okhttp3.Response res = client.newCall(request).execute()) {
                if (!res.isSuccessful()) {
                    UTILS.DebugLog(TAG, "Image Upload Failed: " + res);
                    if (!taskCancelled) {
                        try {
                            JSONObject response = new JSONObject(res.body().string());
                            callback.CallHandler(OP_ID, response, false);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }
                } else {
                    try {
                        JSONObject response = new JSONObject(res.body().string());
                        callback.CallHandler(OP_ID, response, true);
                        UTILS.DebugLog(TAG, "Image Upload Success:  " + response.toString());
                        UTILS.checkBadgeResponse(activity, response);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            UTILS.DebugLog(TAG, ex);
        } catch (IOException e) {
            UTILS.DebugLog(TAG, e);
        }

    }

    private void CallTaskSendFormRP(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try{
            params.put("nome", _params.get("nome"));
            params.put("email", _params.get("email"));
            params.put("msg", _params.get("msg"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this " + params + "\nto: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SEND_FORM_RP);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SEND_FORM_RP, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetUserInfo() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_INFO);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_INFO, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskUpdateUserInfo(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String nome = "";
        String email = "";
        String celular = "";
        String senha = "";
        String sexo = "";
        String cpf = "";
        String dn = "";

        JSONObject params = new JSONObject();
        try{
            if(_params.containsKey("nome")) {
                nome = String.valueOf(_params.get("nome"));
                params.put("nome", nome);
            }
            if(_params.containsKey("email")) {
                email = String.valueOf(_params.get("email"));
                params.put("email", email);
            }
            if(_params.containsKey("celular")) {
                celular = String.valueOf(_params.get("celular"));
                params.put("celular", celular);
            }
            if(_params.containsKey("cpf")) {
                cpf = String.valueOf(_params.get("cpf"));
                params.put("cpf", cpf);
            }
            if(_params.containsKey("senha")) {
                senha = String.valueOf(_params.get("senha"));
                params.put("senha", senha);
            }
            if(_params.containsKey("sexo")) {
                sexo = String.valueOf(_params.get("sexo"));
                sexo = sexo.toUpperCase();
                sexo = (sexo.length() > 0) ? ((sexo.startsWith("M") || sexo.startsWith("F") || sexo.startsWith("O")) ?  sexo.substring(0, 1) : "O") : "O";
                params.put("sexo", sexo);
            }
            if(_params.containsKey("dn")) {
                dn = String.valueOf(_params.get("dn"));
                if(dn.length() > 0) {
                    params.put("dn", dn);
                }
            }
            if(_params.containsKey("socioTorcedor")) {
                params.put("socioTorcedor", _params.get("socioTorcedor"));
            }

        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        final String finalNome = nome;
        final String finalEmail = email;
        final String finalCelular = celular;
        final String finalSexo = sexo;
        final String finalDn = dn;

        UTILS.DebugLog(TAG, "Sending this: " + params + " to: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_UPDATE_USER_INFO);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_UPDATE_USER_INFO, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                            response.put("nome", finalNome);
                            response.put("email", finalEmail);
                            response.put("phone", finalCelular);
                            response.put("sexo", finalSexo);
                            response.put("dn", finalDn);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.getMessage());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRankProfile(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int idUser = (int)_params.get("idUser");
        final String userName = String.valueOf(_params.get("userName"));
        final int userScore = (int)_params.get("userScore");
        final String userImageId = String.valueOf(_params.get("userImageId"));
        final int userPosition = (int)_params.get("userPosition");

        String method = String.format(CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANK_PROFILE, idUser);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {

                            response.put("userName", userName);
                            response.put("userScore", userScore);
                            response.put("userImageId", userImageId);
                            response.put("userPosition", userPosition);

                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskSaveTracking() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Saving tracking! " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SAVE_TRACKING + " sending this: " + getApiHeader());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SAVE_TRACKING, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
//                            CallTokenAuthError();
//                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskErrorTracking() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Saving tracking! " + CONSTANTS.getVersion + CONSTANTS.WS_METHOD_SET_SERVER_ERROR + " sending this: " + getApiHeader());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.getVersion + CONSTANTS.WS_METHOD_SET_SERVER_ERROR, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
//                            CallTokenAuthError();
//                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSendGooglePush(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final JSONObject params = new JSONObject();

        String GOOGLE_PUSH_SERVER_URL = "https://gcm-http.googleapis.com/gcm/send";

        String message = String.valueOf(_params.get("msg"));
//        String to = "/topics/global";
        String to = "APA91bEw5OD2uO479p9jkCBEPPXjLvy0-4wp8dA8EnaP7gd91kqMsJcyAtJkMEtqHaLweT6gWaAdPCMma9YaBMrHTJ3b3LfCaZXEv-Q8jnSF581bSTWMPdbmqRp6bRnoa3lhmIiZwXKP";

        if(_params.containsKey("to")){
            to = String.valueOf(_params.get("to"));
        }

        JSONObject data = new JSONObject();
        try {
            data.put("message", message);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        try {
//            params.put("to", to);
            JSONArray array = new JSONArray();
            array.put(to);
            params.put("registration_ids", array);
            params.put("data", data);
        } catch (JSONException e) {
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending Push " + params + " from " + GOOGLE_PUSH_SERVER_URL + " to: " + to);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, /*"https://android.googleapis.com/gcm/send"*/ GOOGLE_PUSH_SERVER_URL, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Volley Push OnResponse: " + response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error != null) {
                    UTILS.DebugLog(TAG, "Volley Push Error [" + error + "]");
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json;");
                headers.put("Authorization", "key=AIzaSyD882wcmLCOzQkAQ1XyHN2JrxKz6R9BfWU");
//                headers.put("Authorization", "key=AIzaSyDhVAw5e4bRicdJm4KMriOadePrXXOspJk");

                return headers;
            }
        };


        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskRetrieveVideoBitmap(Hashtable<String, Object> _params) throws Throwable {

        final int idNoticia = (int)_params.get("idNoticia");
        String videoPath = String.valueOf(_params.get("videoPath"));

        boolean success = false;
        JSONObject response = new JSONObject();

        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            bitmap = mediaMetadataRetriever.getFrameAtTime();

            success = true;
        }
        catch (Exception e)
        {
            UTILS.DebugLog(TAG, e);
            throw new Throwable( "Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }


        if( (success) && (bitmap != null) ){
            response.put("Status", HttpURLConnection.HTTP_OK);
            response.put("idNoticia", idNoticia);
            String str = UTILS.BitMapToString(bitmap);
            response.put("bitmap", str);
            callback.CallHandler(OP_ID, response, true);
        }
        else{
            response.put("Status", HttpURLConnection.HTTP_NOT_FOUND);
            response.put("error", activity.getString(R.string.error_could_not_retrieve_image_from_video));
            response.put("idNoticia", idNoticia);
            callback.CallHandler(OP_ID, response, false);
        }

    }

    private void CallTaskSetEndSession(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try{
            params.put("sId", _params.get("sId"));

            if(_params.containsKey("endType")){
                params.put("endType", _params.get("endType"));
            }else{
                params.put("endType", CONSTANTS.END_SESSION_TYPE_ANDROID);
            }

            if(_params.containsKey("time"))
                params.put("end", _params.get("time"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        String[] server = CONSTANTS.serverURL.split("/v");
        String method = server[0] + "/" + CONSTANTS.WS_METHOD_SET_END_SESSION;
        UTILS.DebugLog(TAG, "Sending this: " + params + "to:" + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_FORBIDDEN) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (SetEndSession)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskVersionCheck() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int osv = Build.VERSION.SDK_INT;  //(versão do sistema operacional)
        final String dev = UTILS.getDeviceName();  //(modelo do device)

        SecurePreferences prefs = new SecurePreferences(activity.getApplicationContext(), CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        int idUser = 0;
        if (prefs.containsKey(MainRepository.SHARED_KEY_USER)) {
            try {
                JSONObject autologinData = new JSONObject(prefs.getString(MainRepository.SHARED_KEY_USER, ""));
                idUser = ( ((autologinData.has("id")) && (autologinData.get("id") != JSONObject.NULL) ) ? autologinData.getInt("id") : 0 );
            } catch (JSONException e) {
                UTILS.DebugLog(TAG, e);
            }

        }

        JSONObject params = new JSONObject();
        try{
            String apv = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
            params.put("os", CONSTANTS.OS_ID);
            params.put("v", apv);
            params.put("osv", osv);
            params.put("dev", dev);

            if ((idUser > 0) && (UserInformation.getSessionId() <= 0)) {
                params.put("idUser", idUser);
            }

        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        } catch (PackageManager.NameNotFoundException e) {
            UTILS.DebugLog(TAG, e);
        }

        String[] server = CONSTANTS.serverURL.split("/v");
        String method = server[0] + "/" + CONSTANTS.WS_METHOD_VERSION_CHECK;

        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto:" + method );

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
//                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
////                            CallTokenAuthError();
//                            return;
//                        } else

                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_FORBIDDEN) ||
                                (response.getInt("Status") == CONSTANTS.ERROR_CODE_MAINTENANCE) ) { //server em manutencao ou qualquer outra mensagem dinamica
                            success = true;
                        }

//                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
//                            success = true;
//                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_FORBIDDEN) {
//                            success = true;
//                        } else {
//                            success = false;
//                        }
                    } else {
                        success = false;
                    }

                    if(response.has("blockcheckin")){
                        if(response.getInt("blockcheckin") == 1){
                            UserInformation.setBlockCheckIn(true);
                        }
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsMyIdols() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Requesting hashtags My Idols");

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_HASHTAGS_MY_IDOLS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsMyTeams() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Requesting hashtags my Teams");

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_HASHTAGS_MY_TEAMS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsMySports() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Requesting hashtags My Sports");

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_HASHTAGS_MY_SPORTS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsLive() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Requesting hashtags Live");

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_HASHTAGS_LIVE, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetUserApns(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try{
            params.put("apns", _params.get("apns"));
            params.put("allow", _params.get("allow"));
            params.put("os", CONSTANTS.OS_ID);
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        UTILS.DebugLog(TAG, "Sending this: " + params.toString());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_USER_APNS, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
//                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
//                            CallTokenAuthError();
//                            return;
//                        } else
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetDistributors() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.WS_METHOD_GET_DISTRIBUTORS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_DISTRIBUTORS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error);
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCargos() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.WS_METHOD_GET_CARGOS);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CARGOS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetVideoHome() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.WS_METHOD_GET_VIDEO_HOME);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_VIDEO_HOME, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetQuiz(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_QUIZ, _params.get("id"));

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCatalogs() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CATALOGS;


        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_CATALOGS_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskAnswerQuiz(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        try{
            params.put("idPergunta", _params.get("idPergunta"));
            params.put("idResposta", _params.get("idResposta"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        final boolean isRight = String.valueOf(_params.get("isRight")).toLowerCase().matches("true");

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " to: " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_QUIZ);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_ANSWER_QUIZ, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            response.put("isRight", isRight);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetInformatives() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_INFORMATIVES;


        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_INFORMATIVES_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetInformative(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String id = (_params.containsKey("id")) ? String.valueOf(_params.get("id")) : "0";

        String method = CONSTANTS.serverURL + (String.format(CONSTANTS.WS_METHOD_GET_INFORMATIVE_DETAILS, id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSendMessage(Hashtable<String, Object> _params) throws JSONException {
        String texto = ((_params.containsKey("texto")) ? String.valueOf(_params.get("texto")) : "");

        if(_params.containsKey("sourceFile")) {
            String path = String.valueOf(_params.get("sourceFile"));

            //fix parameter
            if (path.startsWith("file:")) {
                path = path.replace("file:", "");
            }

            UTILS.DebugLog(TAG, "path: " + path);

            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();
            File newFile = new File(path);
            String fileName = newFile.getName();

            try {
                if (!fileName.contains(" ")) {
                    params.put("file", newFile);
                } else {

                    byte[] data = new byte[(int) newFile.length()];

                    try {
                        FileInputStream fileInputStream = new FileInputStream(newFile);
                        fileInputStream.read(data);
                        fileInputStream.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String fullPath = newFile.getPath();
                    String justPath = fullPath.replace(fileName, "");
                    fileName = fileName.replaceAll("\\s", "");
                    String newPath = justPath.concat(fileName);

                    FileOutputStream fos = new FileOutputStream(newPath);
                    fos.write(data);
                    fos.close();

                    File fileFixed = new File(newPath);

                    params.put("file", fileFixed);
                }
//            client.addHeader("Content-Type", "multipart/form-data");//application/json; charset=utf-8");
                client.addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

                String method = CONSTANTS.serverURL;
                String encoded = "";
                try {
                    encoded = String.format(CONSTANTS.WS_METHOD_SEND_MESSAGE, URLEncoder.encode(texto, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    encoded = String.format(CONSTANTS.WS_METHOD_SEND_MESSAGE, "");
                    e.printStackTrace();
                }

                method += encoded;

                UTILS.DebugLog(TAG, "sending this: " + params + " to this: " + method);

                client.post(method, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        UTILS.DebugLog(TAG, "Image Upload Failed: " + responseString);
                        if (!taskCancelled) {
                            try {
                                JSONObject response = new JSONObject(responseString);
                                callback.CallHandler(OP_ID, response, false);
                            } catch (JSONException e) {
                                UTILS.DebugLog(TAG, e);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        UTILS.DebugLog(TAG, "Image Upload Success:  " + responseString);
                        try {
                            JSONObject response = new JSONObject(responseString);
                            callback.CallHandler(OP_ID, response, true);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }

                });

            } catch (FileNotFoundException ex) {
                UTILS.DebugLog(TAG, ex);
            } catch (IOException e) {
                UTILS.DebugLog(TAG, e);
            }
        }
        else {
            RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

            String method = CONSTANTS.serverURL;
            String encoded = "";
            try {
                encoded = String.format(CONSTANTS.WS_METHOD_SEND_MESSAGE, URLEncoder.encode(texto, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                encoded = String.format(CONSTANTS.WS_METHOD_SEND_MESSAGE, "");
                e.printStackTrace();
            }

            method += encoded;

            UTILS.DebugLog(TAG, "Getting this: " + method);

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    UTILS.DebugLog(TAG, "Response: " + response);
                    boolean success = false;
                    try {

                        if (response.has("Status")) {
                            if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                                success = true;
                            } else {
                                success = false;
                            }
                        } else {
                            success = false;
                        }

                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                        success = false;
                    }

                    if (!taskCancelled) {
                        callback.CallHandler(OP_ID, response, success);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UTILS.DebugLog(TAG, "Error! ");
                    if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                        String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                        UTILS.DebugLog(TAG, "Error: " + errorStr);
                        JSONObject jsonError = new JSONObject();
                        try {
                            jsonError.put("error", errorStr);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                        callback.CallHandler(OP_ID, jsonError, false);
                    }
                    else if (!taskCancelled) {
                        callback.CallHandler(OP_ID, new JSONObject(), false);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                    return headers;
                }
            };

            int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);

            rq.add(postReq);
        }

    }

    private void CallTaskGetChat(Hashtable<String, Object> _params) throws JSONException {

        int id = (_params.containsKey("id") ? ((int)_params.get("id")) : 0);

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + (String.format(CONSTANTS.WS_METHOD_GET_CHAT, id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSegments() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_SEGMENTS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetTypes() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_TYPES;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetTrainings() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_TRAININGS;

        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_TRAININGS_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRecipes(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RECIPES;

        if(_params.containsKey("home")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_RECIPES_HOME, String.valueOf((int)_params.get("home")));
        }

        if(_params.containsKey("calc")){
            method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RECIPES_CALC;
        }

        //toDo pegar esse search
        if(_params.containsKey("search")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_RECIPES_SEARCH, _params.get("search"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetProducts() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PRODUCTS;

        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUCTS_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetAwards() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_AWARDS;

        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_AWARDS_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskRedeemAward(Hashtable<String, Object> _params) throws JSONException {

        JSONObject params = new JSONObject();
        try{
            params.put("idPremio", (int)_params.get("idPremio"));
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
        }

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_REDEEM_AWARD;

        UTILS.DebugLog(TAG, "Sending this: " + params + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetProductsFilter() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PRODUCT_FILTER;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetWall(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method;

        if(SearchActivity.getToSearch().isEmpty()) {
            method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_WALL;
            if (_params.containsKey("id")) {
                method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_WALL_PARAM, String.valueOf((int) _params.get("id")));
            }
        }
        else {
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_WALL_SEARCH, SearchActivity.getToSearch());
            if (_params.containsKey("id")) {
                method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_WALL_PARAM_SEARCH, String.valueOf((int) _params.get("id")), SearchActivity.getToSearch());
            }
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error);
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskLikeWallPost(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_LIKE_WALL_POST;

        JSONObject params = new JSONObject();
        params.put("idMural", (int)_params.get("idPost"));

        UTILS.DebugLog(TAG, "Sending this " + params + " to:  " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskPostOnWall(Hashtable<String, Object> _params) throws JSONException {
        String texto = ((_params.containsKey("texto")) ? String.valueOf(_params.get("texto")) : "");

        if(_params.containsKey("sourceFile")) {
            String path = String.valueOf(_params.get("sourceFile"));

            //fix parameter
            if (path.startsWith("file:")) {
                path = path.replace("file:", "");
            }

            UTILS.DebugLog(TAG, "path: " + path);

            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();
            File newFile = new File(path);
            String fileName = newFile.getName();

            try {
                if (!fileName.contains(" ")) {
                    params.put("file", newFile);
                } else {

                    byte[] data = new byte[(int) newFile.length()];

                    try {
                        FileInputStream fileInputStream = new FileInputStream(newFile);
                        fileInputStream.read(data);
                        fileInputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String fullPath = newFile.getPath();
                    String justPath = fullPath.replace(fileName, "");
                    fileName = fileName.replaceAll("\\s", "");
                    String newPath = justPath.concat(fileName);

                    FileOutputStream fos = new FileOutputStream(newPath);
                    fos.write(data);
                    fos.close();

                    File fileFixed = new File(newPath);

                    params.put("file", fileFixed);
                }
//            client.addHeader("Content-Type", "multipart/form-data");//application/json; charset=utf-8");
                client.addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

                String method = CONSTANTS.serverURL;
                String encoded = "";
                try {
                    encoded = String.format(CONSTANTS.WS_METHOD_POST_ON_WALL, URLEncoder.encode(texto, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    encoded = String.format(CONSTANTS.WS_METHOD_POST_ON_WALL, "");
                    e.printStackTrace();
                }

                method += encoded;

                client.post(method, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        UTILS.DebugLog(TAG, "Image Upload Failed: " + responseString);
                        if (!taskCancelled) {
                            try {
                                JSONObject response = new JSONObject(responseString);
                                callback.CallHandler(OP_ID, response, false);
                            } catch (JSONException e) {
                                UTILS.DebugLog(TAG, e);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        UTILS.DebugLog(TAG, "Image Upload Success:  " + responseString);
                        try {
                            JSONObject response = new JSONObject(responseString);
                            callback.CallHandler(OP_ID, response, true);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }

                });

            } catch (FileNotFoundException ex) {
                UTILS.DebugLog(TAG, ex);
            } catch (IOException e) {
                UTILS.DebugLog(TAG, e);
            }
        }
        else {
            RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

            String method = CONSTANTS.serverURL;
            String encoded = "";
            try {
                encoded = String.format(CONSTANTS.WS_METHOD_POST_ON_WALL, URLEncoder.encode(texto, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                encoded = String.format(CONSTANTS.WS_METHOD_POST_ON_WALL, "");
                e.printStackTrace();
            }

            method += encoded;

            UTILS.DebugLog(TAG, "Getting this: " + method);

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    UTILS.DebugLog(TAG, "Response: " + response);
                    boolean success = false;
                    try {

                        if (response.has("Status")) {
                            if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                                success = true;
                            } else {
                                success = false;
                            }
                        } else {
                            success = false;
                        }

                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                        success = false;
                    }

                    if (!taskCancelled) {
                        callback.CallHandler(OP_ID, response, success);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UTILS.DebugLog(TAG, "Error! ");
                    if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                        String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                        UTILS.DebugLog(TAG, "Error: " + errorStr);
                        JSONObject jsonError = new JSONObject();
                        try {
                            jsonError.put("error", errorStr);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                        callback.CallHandler(OP_ID, jsonError, false);
                    }
                    else if (!taskCancelled) {
                        callback.CallHandler(OP_ID, new JSONObject(), false);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                    return headers;
                }
            };

            int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);

            rq.add(postReq);
        }

    }

    private void CallTaskPostOnWall2(Hashtable<String, Object> _params) throws JSONException {
        String texto = ((_params.containsKey("texto")) ? String.valueOf(_params.get("texto")) : "");

        if(_params.containsKey("sourceFile")) {
            String path = String.valueOf(_params.get("sourceFile"));

            //fix parameter
            if (path.startsWith("file:")) {
                path = path.replace("file:", "");
            }

            UTILS.DebugLog(TAG, "path: " + path);

            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();
            File newFile = new File(path);
            String fileName = newFile.getName();

            try {
                if (!fileName.contains(" ")) {
                    params.put("file", newFile);
                } else {

                    byte[] data = new byte[(int) newFile.length()];

                    try {
                        FileInputStream fileInputStream = new FileInputStream(newFile);
                        fileInputStream.read(data);
                        fileInputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String fullPath = newFile.getPath();
                    String justPath = fullPath.replace(fileName, "");
                    fileName = fileName.replaceAll("\\s", "");
                    String newPath = justPath.concat(fileName);

                    FileOutputStream fos = new FileOutputStream(newPath);
                    fos.write(data);
                    fos.close();

                    File fileFixed = new File(newPath);

                    params.put("file", fileFixed);
                }
//            client.addHeader("Content-Type", "multipart/form-data");//application/json; charset=utf-8");
//                client.addHeader("Content-Type", "application/x-www-form-urlencoded");
                client.addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

                String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_ON_WALL_2;
                params.put("texto", texto);
//                Uuncomment in case we can send video and image in the same post
//                if(_params.containsKey("video_url") &&
//                        !String.valueOf(_params.get("video_url")).isEmpty()){
//                    params.put("video_url", _params.get("video_url"));
//                } else
                if(_params.containsKey("gif") &&
                        !String.valueOf(_params.get("gif")).isEmpty()) {
                    params.put("pred_img", _params.get("gif"));
                }

                client.post(method, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                        UTILS.DebugLog(TAG, "Image Upload Failed: " + responseString);
                        if (!taskCancelled) {
                            try {
                                JSONObject response = new JSONObject(responseString);
                                callback.CallHandler(OP_ID, response, false);
                            } catch (JSONException e) {
                                UTILS.DebugLog(TAG, e);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                        UTILS.DebugLog(TAG, "Image Upload Success:  " + responseString);
                        try {
                            JSONObject response = new JSONObject(responseString);
                            callback.CallHandler(OP_ID, response, true);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }

                });

            } catch (FileNotFoundException ex) {
                UTILS.DebugLog(TAG, ex);
            } catch (IOException e) {
                UTILS.DebugLog(TAG, e);
            }
        }
        else {


            SyncHttpClient client = new SyncHttpClient();
            RequestParams params = new RequestParams();

            client.addHeader("Content-Type", "application/x-www-form-urlencoded");
            client.addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

            String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_ON_WALL_2;
            params.put("texto", texto);
            if(_params.containsKey("video_url") &&
                    !String.valueOf(_params.get("video_url")).isEmpty()){
                params.put("video_url", _params.get("video_url"));
            } else if(_params.containsKey("gif") &&
                    !String.valueOf(_params.get("gif")).isEmpty()) {
                params.put("pred_img", _params.get("gif"));
            }

            client.post(method, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                    UTILS.DebugLog(TAG, "Image Upload Failed: " + responseString);
                    if (!taskCancelled) {
                        try {
                            JSONObject response = new JSONObject(responseString);
                            callback.CallHandler(OP_ID, response, false);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                    UTILS.DebugLog(TAG, "Image Upload Success:  " + responseString);
                    try {
                        JSONObject response = new JSONObject(responseString);
                        callback.CallHandler(OP_ID, response, true);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                }

            });


//            RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
//
//            String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_ON_WALL_2;
//
//            JSONObject args = new JSONObject();
//            args.put("texto", texto);
//            if(_params.containsKey("gif")) {
//                String gif = String.valueOf(_params.get("gif"));
//                if(!gif.isEmpty()){ args.put("pred_img", gif); }
//            }
//
//            UTILS.DebugLog(TAG, "Posting this: " + args + " to " + method);
//
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, args, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    UTILS.DebugLog(TAG, "Response: " + response);
//                    boolean success = false;
//                    try {
//
//                        if (response.has("Status")) {
//                            if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
//                                success = true;
//                            } else {
//                                success = false;
//                            }
//                        } else {
//                            success = false;
//                        }
//
//                    } catch (JSONException e) {
//                        UTILS.DebugLog(TAG, e);
//                        success = false;
//                    }
//
//                    if (!taskCancelled) {
//                        callback.CallHandler(OP_ID, response, success);
//                    }
//                }
//
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    UTILS.DebugLog(TAG, "Error! ");
//                    if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {
//
//                        String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
//                        UTILS.DebugLog(TAG, "Error: " + errorStr);
//                        JSONObject jsonError = new JSONObject();
//                        try {
//                            jsonError.put("error", errorStr);
//                        } catch (JSONException e) {
//                            UTILS.DebugLog(TAG, e);
//                        }
//                        callback.CallHandler(OP_ID, jsonError, false);
//                    }
//                    else if (!taskCancelled) {
//                        callback.CallHandler(OP_ID, new JSONObject(), false);
//                    }
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//
//                    HashMap<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/json; charset=utf-8");
//                    headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
//                    return headers;
//                }
//            };
//
//            int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//
//            rq.add(postReq);
        }

    }

    private void CallTaskCommentWallPost(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_COMMENT_WALL_POST;

        JSONObject params = new JSONObject();
        params.put("idMural", (int)_params.get("idPost"));
        params.put("comentario", _params.get("comentario"));

        UTILS.DebugLog(TAG, "Sending this " + params + " to:  " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    //Task called to retrieve user posts, comments and photos for the commment section ( WallActivity class).
    private void CallTaskGetWallUser(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_WALL_USER;

        if(_params.containsKey("id")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_WALL_USER_PARAM, String.valueOf((int)_params.get("id")));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }


    private void CallTaskGetUserAchievements() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_ACHIEVEMENTS;
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error);
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);

                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);

                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

                UTILS.DebugLog(TAG, "Header: " + headers);
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetTraining(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_TRAINING, String.valueOf(id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRecipe(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_RECIPE, String.valueOf(id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSpecificPost(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_SPECIFIC_POST, String.valueOf(id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskDownloadImageFromUrl(Hashtable<String, Object> _params) throws JSONException{
        String _url = String.valueOf(_params.get("url"));

        UTILS.DebugLog(TAG, "Trying to download this: " + _url);

        boolean success = false;
        JSONObject json = new JSONObject();
        try {
            //DOWNLOAD IMAGE
            URL url = new URL(_url);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = in.read(buf);
            while (n != -1) {
                out.write(buf, 0, n);
                n = in.read(buf);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            //SAVE TO DOWNLOADS
            String fileName = "Puratos Img " + UserInformation.getUserData().getNickname() + UTILS.getCurrenteDateTime() + ".jpg";//CONSTANTS.AVATAR_FILENAME_EXTENSION;
            String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + fileName;
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(response);
            fos.close();

            json.put("filePath", filePath);
            UTILS.DebugLog(TAG, "Download filepath = " + filePath);

            success = true;
        } catch (IOException e) {
            e.printStackTrace();

            json.remove("filePath");
            UTILS.DebugLog(TAG, "Download filepath = \"\"");
        }


        callback.CallHandler(OP_ID, json, success);

    }

    private void CallTaskDeleteWallPost(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_DELETE_WALL_POST;

        JSONObject params = new JSONObject();
        params.put("idMural", id);

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            response.put("id", id);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error);
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetProduct(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUCT, String.valueOf(id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskForgotPassword(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String email = String.valueOf(_params.get("email"));

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_FORGOT_PASSWORD;

        JSONObject params = new JSONObject();
        params.put("email", email);

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_NOT_FOUND) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCalculator(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int id = (int)_params.get("id");
        int fromUser = 0;
        if(_params.containsKey("user")){
            fromUser = (int)_params.get("user");
        }

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_CALCULATOR, String.valueOf(id), String.valueOf(fromUser));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {
                    response.put("id", id);
                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetCalculatorRecipe(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params  = new JSONObject(String.valueOf(_params.get("param")));
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_CALCULATOR_RECIPE;
        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " to: "+ method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRecipesWithCalc() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RECIPES_WITH_CALC;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetCategories() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CATEGORIES;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetIngredients() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_INGREDIENTS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetUserRecipes() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_USER_RECIPES;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetUserRecipeById(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + (String.format(CONSTANTS.WS_METHOD_GET_USER_RECIPE_BY_ID, id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskDeleteUserRecipe(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_DEL_USER_RECIPE_BY_ID;

        JSONObject params = new JSONObject();
        params.put("id", id);

        UTILS.DebugLog(TAG, "Sending this: " + params.toString() + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            response.put("id", id);
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetSearch(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final String search = String.valueOf(_params.get("search"));

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_SEARCH, search);

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetNotifications(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        int page = 1;

        if(_params.containsKey("page")) {
            page = (int) _params.get("page");
        }

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTIFICATIONS, page);

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetNotifications)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetNotification(final Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        final int idPush = (int)_params.get("idPush");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_SET_NOTIFICATION, idPush);

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    try {
                        response.put("idPush", _params.get("idPush"));
                    }
                    catch (JSONException e){
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (SetNotification)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetAgenda() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_AGENDA;

        if(!SearchActivity.getToSearch().isEmpty()){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_AGENDA_SEARCH, SearchActivity.getToSearch());
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);

                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetAgenda)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    //get Agenda tipos.
    private void CallTaskGetAgendaTypes(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_AGENDA_TYPES;

        String method2 = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_AGENDA_TYPES, String.valueOf(_params.get("id")));
        UTILS.DebugLog(TAG, "Getting this: " + method2);

        Log.d("myagenda", "getAgenda Tipos method " + method2);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                Log.d("myagenda", "getAgenda Tipos " + response);

                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetAgendaTypes)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());

                Log.d("myagenda", "header: " + headers.toString());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetAgendaEvent(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_AGENDA_EVENT, String.valueOf(_params.get("id")));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        Log.d("myAgenda", "GET Agenda Event url: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                Log.d("myAgenda", "GET Agenda Event response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetAgendaEvent)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetAgendaEvent(final Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        params.put("idEvento", _params.get("idEvento"));
        params.put("idStatus", _params.get("idStatus"));

        Log.d("myagenda setAgenda: ", "idEvento: " + _params.get("idEvento"));
        Log.d("myagenda setAgenda: ", "idStatus: " + _params.get("idStatus"));

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_AGENDA_EVENT;

        UTILS.DebugLog(TAG, "Sending this: " + params + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    try {
                        response.put("idEvento", _params.get("idEvento"));
                        response.put("idStatus", _params.get("idStatus"));
                    }
                    catch (JSONException e){
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (SetAgendaEvent)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetRegulamento(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_REGULAMENTO, _params.get("t"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetRegulamento)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetRegulamento(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        double rv = (_params.containsKey("rv")) ? (double)_params.get("rv") : 0.0d;

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_SET_REGULAMENTO, String.valueOf(rv));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (SetRegulamento)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetNewsComment(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject();
        if(_params.containsKey("idNoticia")) {
            params.put("idNoticia", (int)_params.get("idNoticia"));
        }
        if(_params.containsKey("comentario")) {
            params.put("comentario", String.valueOf(_params.get("comentario")));
        }

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_NEWS_COMMENT;

        UTILS.DebugLog(TAG, "Setting this: " + params + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (SetNewsComment)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetGifs() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_GIFS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    //anakin
    private void CallGetClientsAnakin(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String search = "";
        if(_params.containsKey("search")){
            search = (String)_params.get("search");
        }

        final int page = (int)_params.get("page");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_CLIENT_LIST_ANAKIN, String.valueOf(page), String.valueOf(search));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetClientAnakin(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        final int id = (int)_params.get("id");

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_CLIENT_ANAKIN, String.valueOf(id));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetCampanhaAnakin() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_CAMPANHA_ANAKIN;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetMuralAnakin(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_MURAL_ANAKIN_FIRST;


        if(_params.containsKey("id")) {
            Log.d("Get-Post-async", "id: " + (int)_params.get("id"));
            final int id = (int)_params.get("id");
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_MURAL_ANAKIN, String.valueOf(id));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallPostDelPostAnakin(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject(_params);
        params.put("idMural", _params.get("id"));

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_DEL_MURAL_ANAKIN;
        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + " to this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallPostSetPostAnakin(Hashtable<String, Object> _params) {

        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        if(_params.containsKey("idCliente")){
            params.put("idCliente", _params.get("idCliente"));
        }else{
            params.put("nome", _params.get("nome"));
            params.put("address", _params.get("address"));
            params.put("bairro", _params.get("bairro"));
            params.put("cidade", _params.get("cidade"));
            params.put("estado", _params.get("estado"));
        }

        params.put("idCampanha", _params.get("idCampanha"));
        params.put("qtde", _params.get("qtde"));

        try {
            for(int i = 0; i < 30; i++){
                int index = i+1;
                if(_params.containsKey("file"+ index)){
                    String file = "file" + index;
                    File send = new File(String.valueOf(_params.get(file)));
                    params.put("file"+ index, send);
                }else{
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            Log.e("Check-Info", e.getMessage());
        }

        try {
            client.addHeader(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
            String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_SET_MURAL_ANAKIN;

            client.post(method, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                    UTILS.DebugLog(TAG, "Image Upload Failed: " + responseString);
                    if (!taskCancelled) {
                        try {
                            JSONObject response = new JSONObject(responseString);
                            callback.CallHandler(OP_ID, response, false);
                        } catch (JSONException e) {
                            UTILS.DebugLog(TAG, e);
                        }
                    }
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                    UTILS.DebugLog(TAG, "Image Upload Success:  " + responseString);
                    try {
                        JSONObject response = new JSONObject(responseString);
                        callback.CallHandler(OP_ID, response, true);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                }

            });

        } catch (Exception e) {
            Log.e("Check-Info-error", e.getMessage());
        }
    }

    private void CallPostVoucherCode(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject(_params);
        params.put("id", _params.get("id"));
        params.put("code", _params.get("code"));

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_VOUCHERS_CODE;
        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + " to this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallPostVoucherRedeem(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JSONObject params = new JSONObject(_params);
        params.put("id", _params.get("id"));

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_POST_VOUCHERS_PRIZE_REDEEM;
        UTILS.DebugLog(TAG, "Sending this: " + params + "\nto: " + " to this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetVoucherHome() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_VOUCHERS_HOME;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetVoucherDetails(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_VOUCHERS_DETAILS;

        if(_params.containsKey("id")) {
            Log.d("Get-Post-async", "id: " + (int)_params.get("id"));
            final int id = (int)_params.get("id");
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_VOUCHERS_DETAILS, String.valueOf(id));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallGetVoucherPrizes(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_VOUCHERS_PRIZES_LIST;

        if(_params.containsKey("id")) {
            Log.d("Get-Post-async", "id: " + (int)_params.get("id"));
            final int id = (int)_params.get("id");
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_VOUCHERS_PRIZES_LIST, String.valueOf(id));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG + " (GetGifs)", "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetVendedores(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_VENDEDORES, _params.get("page"));

        //Se tiver search
        if(_params.containsKey("search")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_VENDEDORES_SEARCH, _params.get("page"), _params.get("search"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetQuizes(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_QUIZES;

        //Se tiver search
        if(_params.containsKey("page")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_QUIZES_PAGE, _params.get("page"));
        }

        if(_params.containsKey("id")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_QUIZES_ID, _params.get("id"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetQuiz(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_QUIZ;

        JSONObject params = new JSONObject(_params);

        params.put("idPergunta" , _params.get("idPergunta"));

        if(_params.containsKey("idResposta")){
            params.put("idResposta" , _params.get("idResposta"));
        }

        if(_params.containsKey("timeout")){
            params.put("timeout" , _params.get("timeout"));
        }

        if(_params.containsKey("tempo")){
            params.put("tempo" , _params.get("tempo"));
        }

        UTILS.DebugLog(TAG, "Sending this:" + params.toString() + " to:" + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    //UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetUltimasPesquisas() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_HOME_PESQUISA;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetPesquisas(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PESQUISA;

        if(_params.containsKey("idPergunta")){
            method = CONSTANTS.serverURL +  String.format(CONSTANTS.WS_METHOD_GET_PESQUISA_ID, _params.get("idPergunta"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskSetPesquisa(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_PESQUISA;
        JSONObject params = new JSONObject(_params);

        params.put("idPergunta", _params.get("idPergunta"));

        if(_params.containsKey("idResposta"))
            params.put("idResposta", _params.get("idResposta"));

        if(_params.containsKey("aberta"))
            params.put("aberta", _params.get("aberta"));

        UTILS.DebugLog(TAG, "Sending this: " + params +" to:"+ method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetUser() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "GET " + CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PERFIL);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PERFIL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetBadge(Hashtable<String, Object> _params) throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_BADGES);

        if(_params.containsKey("page")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_BADGES_PAGE, _params.get("page"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetInstagram() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_INSTAGRAM_STORIES);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response insta: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetNoticias(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NOTICIAS);

        if(_params.containsKey("home")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTICIAS_ID, _params.get("home"));
        }

        if(_params.containsKey("filter")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTICIAS_BY_FILTER, _params.get("filter"), _params.get("page"));
        }else if (_params.containsKey("page")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTICIAS_PAGE, _params.get("page"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response banner: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error banner! " );
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error banner: " + errorStr);
                    UTILS.DebugLog(TAG, "Error banner: " +  error.networkResponse);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error banner", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetNoticia(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NOTICIA);

        if(_params.containsKey("id")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTICIA_ID, _params.get("id"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetLastResultados(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_LAST_RESULTADOS);

        if(_params.containsKey("home")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_LAST_RESULTADOS_HOME, _params.get("home"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetLastJogos(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_JOGOS);

        if(_params.containsKey("home")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_JOGOS_HOME, _params.get("home"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlayersTitular() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYERS_TITULAR);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlayersPalpites() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = (CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYERS_PALPITES);

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlayer(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PLAYER, _params.get("id"));
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlayerChart(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PLAYER_CHART, _params.get("id"), _params.get("t"));
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetEscalacaoHome() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_ESCALACAO_HOME;
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetEscalacao(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_ESCALACAO, _params.get("id"));
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlayerEscalacao(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PLAYERS_ESCALACAO, _params.get("id"));
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskSetPlayerEscalacao(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_PLAYERS_ESCALACAO;

        JSONObject params = new JSONObject(_params);
        UTILS.DebugLog(TAG, "Sending this: "+ params.toString() + " to: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetProdutos(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PRODUTOS;
        if(_params.containsKey("search")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTOS_SEARCH, _params.get("search"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetQuizHome() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_QUIZ_HOME;

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetProduto(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTO, _params.get("id"));
        if(_params.containsKey("idCupom")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTO_CUPOM, _params.get("id"), _params.get("idCupom"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetProdutoRegulamento(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTO_REGULAMENTO, _params.get("id"));
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetRankGroup() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANK_GROUP;
        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetRanking(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_RANK_;

        if(_params.containsKey("id")) {
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_RANK_ID, _params.get("id"));
        }

        if(_params.containsKey("page")) {
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_RANK_ID_PAGE, _params.get("id"), _params.get("page"));
        }

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }


    private void CallTaskGetDocs() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_DOCS;

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskSetCompra(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_COMPRA;

        JSONObject params = new JSONObject(_params);

        UTILS.DebugLog(TAG, "Sending this: "+params + " To: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPalpite() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PALPITES;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskSetPalpite(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_PALPITES;

        JSONObject params = new JSONObject(_params);

        UTILS.DebugLog(TAG, "Sending this: " + params + " To: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }



    private void GetSocioTorcerdorAPI(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.apiServerURL + _params.get("cpf");
        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                if (response!= null) {
                    success = true;
                }
                else {
                    CallTokenAuthError();
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.CallHandler(OP_ID, new JSONObject(), false);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put(CONSTANTS.WS_API_SOCIO_HEADER_KEY, CONSTANTS.apikey);
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void SetSocioTorcerdor(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_SET_SOCIO_TORCEDOR;

        JSONObject params = new JSONObject(_params);

        UTILS.DebugLog(TAG, "Sending this: " + params + " To: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, method, params,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    UTILS.checkBadgeResponse(activity, response);
                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void GetNewBadges() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());
        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NEW_BADGES;
        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetBearerToken() throws JSONException {

        String str = CONSTANTS.TWITTER_KEY + ":" + CONSTANTS.TWITTER_SECRET;
        String base64 = Base64.encodeToString(str.getBytes(), Base64.NO_WRAP);
        final String authHeader = "Basic " + base64;
        final String contentHeader = "application/x-www-form-urlencoded;charset=UTF-8";
        final String entity = "grant_type=client_credentials";

        UTILS.DebugLog("Api Key", CONSTANTS.TWITTER_KEY);
        UTILS.DebugLog("Api Secret", CONSTANTS.TWITTER_SECRET);
        UTILS.DebugLog("Concatenated", str);
        UTILS.DebugLog("Base64", base64);

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        UTILS.DebugLog(TAG, "Getting this: " +  CONSTANTS.WS_METHOD_GET_BEARER_TOKEN +"/ "  + authHeader );

        StringRequest postReq = new StringRequest(Request.Method.POST, CONSTANTS.WS_METHOD_GET_BEARER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject json = new JSONObject();
                try{
                    json.put("Status", 200);
                    JSONObject jsonResponse = new JSONObject(response);
                    json.put("Object", jsonResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.CallHandler(OP_ID, json, true);
                UTILS.DebugLog("Response_Twitter", json);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", contentHeader);
                headers.put("Authorization", authHeader);
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String someContent = entity;
                return someContent.getBytes();
            }
        };


        rq.add(postReq);
    }

    private void CallTaskGetTweets(Hashtable<String, Object> _params) throws JSONException {

        String query = String.valueOf(_params.get("query"));

        int count = 20;

        if(_params.containsKey("count"))
            count = Integer.parseInt(String.valueOf(_params.get("count")));


        String bearerToken = String.valueOf(_params.get("bearerToken"));
        final String authHeader = "Bearer " + bearerToken;

        String method = String.format(CONSTANTS.WS_METHOD_GET_TWEETS, query, count);
        final String contentHeader = "application/json;charset=UTF-8";

        UTILS.DebugLog(TAG, "Getting this: " + method + " Token: " + authHeader);

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject json = new JSONObject();
                try{
                    json.put("Status", 200);
                    json.put("Object", response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.CallHandler(OP_ID, json, true);
                UTILS.DebugLog("Response_Twitter", json);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null)) {
                    UTILS.DebugLog(TAG, "Error: " + error.toString());
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", error.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", contentHeader);
                headers.put("Authorization", authHeader);
                return headers;
            }

        };

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsTwitterQuerySup() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_TWITTER_QUERY_SUP;
        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                            success = response.getInt("Status") == HttpURLConnection.HTTP_OK;
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHashtagsTwitterQueryInf() throws JSONException {

        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_TWITTER_QUERY_INF;
        UTILS.DebugLog(TAG, method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else
                            success = response.getInt("Status") == HttpURLConnection.HTTP_OK;
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);

    }

    private void CallTaskGetHtmlPageCode(Hashtable<String, Object> _params) throws JSONException, IOException {

        String url = String.valueOf(_params.get("url"));
        long id = 0L;

        if(_params.containsKey("id")){
            id = (long)_params.get("id");
        }

        HttpClient httpclient = new DefaultHttpClient(); // Create HTTP Client
        HttpGet httpget = new HttpGet(url); // Set the action you want to do
        HttpResponse httpResponse = httpclient.execute(httpget); // Executeit
        HttpEntity entity = httpResponse.getEntity();
        InputStream is = entity.getContent(); // Create an InputStream with the response
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1), 8);
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) // Read line by line
            sb.append(line + "\n");

        String html = sb.toString(); // Result is here

        is.close(); // Close the stream

        JSONObject response = new JSONObject();
        response.put("url", url);
        response.put("page", html);
        if(id != 0L) {
            response.put("id", id);
        }
        UTILS.DebugLog(TAG, "Response:\n" + response.toString() );

        callback.CallHandler(OP_ID, response, true);

    }

    private void CallTaskGetMAtchDetails(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_JOGO_DETALHES, _params.get("id"));

        UTILS.DebugLog(TAG, "GET " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPalpiteList(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PALPITES_RESPONDIDOS;

        if(_params.containsKey("page")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PALPITES_RESPONDIDOS_PAGE, _params.get("page"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPalpiteDetails(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PALPITES_DETAIS, _params.get("id"));
        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetPlaylist(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PLAYLIST;

        if(_params.containsKey("page")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PLAYLIST_PAGE, _params.get("page"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetAudio(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_AUDIO, _params.get("id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetAudioHome() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_AUDIO_HOME;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetNoticiasFilters() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NOTICIAS_FILTERS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetNotificationHistory(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_NOTIFICATION_HISTORY, _params.get("page"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetNotificationMarkAll() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NOTIFICATION_MARK_ALL;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetBannerLoja() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_BANNER_HOME;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetBannerMenu() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_BANNER_MENU;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetSocioApi() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_SOCIO_API;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }


    private void CallTaskSetSocioLogin(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + CONSTANTS.WS_METHOD_GET_SOCIO_LOGIN;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        StringRequest  postReq = new StringRequest (Request.Method.POST, method,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String re) {
                        UTILS.DebugLog(TAG, "Response: " + re);
                        boolean success = false;
                        JSONObject response = null;
                        try {
                            response = new JSONObject(re);
                            if (response.has("data")) {
                                success = true;

                            }else{
                                success = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            success = false;
                        }

                        if (!taskCancelled) {
                            callback.CallHandler(OP_ID, response, success);
                        }
                    }},
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UTILS.DebugLog(TAG, "Error!" + error.toString());
                        if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                            String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                            UTILS.DebugLog(TAG, "Error: " + errorStr);
                            JSONObject jsonError = new JSONObject();
                            try {
                                jsonError.put("error", errorStr);
                            } catch (JSONException e) {
                                UTILS.DebugLog(TAG, e);
                            }
                            callback.CallHandler(OP_ID, jsonError, false);
                        }
                        else if (!taskCancelled) {
                            callback.CallHandler(OP_ID, new JSONObject(), false);
                        }
                    }})
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", "gigliani@swind.com.br");
                params.put("password", "12345678");
                return params;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetConsultarSocio(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + String.format(CONSTANTS.WS_METHOD_GET_CONSULTAR_SOCIO, _params.get("cpf"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("dados")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetConsultarProxJogos(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //URL provisória
        String method = "http://mobile.sociofortaleza.com.br/v1/" + CONSTANTS.WS_METHOD_GET_CONSULTAR_PROX_JOGOS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("status")) {
                    try {
                        if (response.getInt("status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetConsultarBackJogos(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + CONSTANTS.WS_METHOD_GET_CONSULTAR_BACK_JOGOS;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("data")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetConsultarSetores(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //URL provisória
        String method = "http://mobile.sociofortaleza.com.br/v1/" + String.format(CONSTANTS.WS_METHOD_GET_SETORES, _params.get("socio_id"), _params.get("jogo_id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("dados")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetConsultarDivSetores(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + String.format(CONSTANTS.WS_METHOD_GET_DIV_SETORES, _params.get("estadio_id"), _params.get("estadio_setor_id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("status")) {
                    try {
                        if (response.getInt("status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetAssentosDiv(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //URL provisória
        String method = "http://mobile.sociofortaleza.com.br/v1/" + String.format(CONSTANTS.WS_METHOD_GET_ASSENTOS_DIV, _params.get("jogo_id"),_params.get("estadio_setor_id"), _params.get("estadio_divisao_id"));
        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("status")) {
                    try {
                        if (response.getInt("status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetIngressos(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        //URL provisória
        String method = "http://mobile.sociofortaleza.com.br/v1/" + String.format(CONSTANTS.WS_METHOD_GET_INGRESSOS, _params.get("jogo_id"), _params.get("estadio_setor_id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("dados")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetAcompanhantes(Hashtable<String, Object> _params) {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + String.format(CONSTANTS.WS_METHOD_GET_ACOMPANHANTES, _params.get("socio_id"), _params.get("jogo_id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("dados")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetCheckings(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.apiServerURL + String.format(CONSTANTS.WS_METHOD_GET_CHECKINGS, _params.get("socio_id"), _params.get("jogo_id"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;

                if (response.has("dados")) {
                    success = true;
                }else{
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetFazerCheckin(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = "http://mobile.sociofortaleza.com.br/v1/" + String.format(CONSTANTS.WS_METHOD_GET_FAZER_CHECKINGS,
                _params.get("socio_id"),
                _params.get("jogo_id"),
                _params.get("estadio_setor_id"),
                _params.get("estadio_setor_divisao_id"),
                _params.get("opcao"),
                _params.get("valor"),
                _params.get("ncc"),
                _params.get("val"),
                _params.get("cvv"),
                _params.get("nomes"),
                _params.get("estadio_cadeiras"));

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                int status = 0;

                if (response.has("status")) {
                    try {
                        status = response.getInt("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    success = false;
                }

                if(status == 200){
                    success = true;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! " + error.toString());
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-User-email", "gigliani@swind.com.br");
                headers.put("X-User-token", "qqtw2SCcZyGYzcpDR7gn");
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetProdutosLojaVirtual(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL;

        if(_params.containsKey("filter") && _params.containsKey("search") ){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_FS, _params.get("filter"), _params.get("search"));
        }else
        if(_params.containsKey("search")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_S,  _params.get("search"));
        }else
        if(_params.containsKey("filter")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_F, _params.get("filter"));
        }else
        if(_params.containsKey("home")){
            method = CONSTANTS.serverURL + String.format(CONSTANTS.WS_METHOD_GET_PRODUTOS_LOJA_VIRTUAL_H, _params.get("home"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response Loja: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetFiltersLojaVirtual() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_FILTERS_LOJA_VIRTUAL;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallGetMessageCovid() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_MESSAGE_COVID;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ( (response.getInt("Status") == HttpURLConnection.HTTP_OK) ||
                                (response.getInt("Status") == HttpURLConnection.HTTP_ACCEPTED) ){
                            success = true;
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }


    private void CallTaskGetNotificationCount() throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.serverURL + CONSTANTS.WS_METHOD_GET_NOTIFICATION_COUNT;

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else if ( ( (response.has("Moedas")) && (response.get("Moedas") != JSONObject.NULL) ) &&
                            ( (response.has("Rank")) && (response.get("Rank") != JSONObject.NULL) ) &&
                            ( (response.has("Pontos")) && (response.get("Pontos") != JSONObject.NULL) ) ){
                        success = true;
                    }
                    else {
                        success = false;
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }

    private void CallTaskGetServers(Hashtable<String, Object> _params) throws JSONException {
        RequestQueue rq = Volley.newRequestQueue(activity.getApplicationContext());

        String method = CONSTANTS.getVersion + CONSTANTS.WS_METHOD_GET_SERVERS;

        if(_params.containsKey("idUser")){
            method = CONSTANTS.getVersion + String.format(CONSTANTS.WS_METHOD_GET_SERVERS_USER, _params.get("idUser"));
        }

        UTILS.DebugLog(TAG, "Getting this: " + method);

        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.GET, method, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UTILS.DebugLog(TAG, "Response: " + response);
                boolean success = false;
                try {

                    if (response.has("Status")) {
                        if ((response.getInt("Status") == HttpURLConnection.HTTP_NOT_ACCEPTABLE)) {
                            CallTokenAuthError();
                            return;
                        } else if (response.getInt("Status") == HttpURLConnection.HTTP_OK) {
                            success = true;
                        } else {
                            success = false;
                        }
                    }
                    else {
                        success = false;
                    }

                    if(response.has("Object")){
                        JSONObject obj = response.getJSONObject("Object");
                        if(obj.has("idUser")) {

                            int id = obj.getInt("idUser");
                            UserInformation.setUserId(id);
//                            UserModel data = new UserModel();
//                            data.setId(id);
//                            new UserRepository().saveUser(data, false);
                        }
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                    success = false;
                }

                if (!taskCancelled) {
                    callback.CallHandler(OP_ID, response, success);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UTILS.DebugLog(TAG, "Error! ");
                if ((error != null) && (!taskCancelled) && (error.networkResponse != null) && (error.networkResponse.data != null)) {

                    String errorStr = UTILS.getVolleyErrorDataString(error.networkResponse.data);
                    UTILS.DebugLog(TAG, "Error: " + errorStr);
                    JSONObject jsonError = new JSONObject();
                    try {
                        jsonError.put("error", errorStr);
                    } catch (JSONException e) {
                        UTILS.DebugLog(TAG, e);
                    }
                    callback.CallHandler(OP_ID, jsonError, false);
                }
                else if (!taskCancelled) {
                    callback.CallHandler(OP_ID, new JSONObject(), false);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(CONSTANTS.WS_API_HEADER_KEY, getApiHeader());
                return headers;
            }
        };

        int socketTimeout = CONSTANTS.CONNECTION_TIMEOUT;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);

        rq.add(postReq);
    }
}