package utils;

import informations.UserInformation;

/**
 * Created by rafael.verginelli on 1/16/17.
 */

public class TRACKING {

    //NENHUMA TELA
    public static final int TRACKING_NOSCREEN_PUSH = 1;
    public static final int TRACKING_NOSCREEN_SILENCEPUSH = 2;
    public static final int TRACKING_NOSCREEN_VOLTAR_PLAYER = 157;

    //SLASH
    public static final int TRACKING_SPLASH_TRYAGAIN = 3;

    //BLOCK
    public static final int TRACKING_BLOCK_TRYAGAIN = 4;
    public static final int TRACKING_BLOCK_DOWNLOAD = 5;
    public static final int TRACKING_BLOCK_PULARVERSION = 6;

    //CRIAR CONTA
    public static final int TRACKING_CREATEACCOUNT_TERMS = 7;
    public static final int TRACKING_CREATEACCOUNT_TERMS_POLITCS = 8;
    public static final int TRACKING_CREATEACCOUNT_CONFIRMAR = 9;
    public static final int TRACKING_CREATEACCOUNT_OPEN_LOGIN = 10;

    //LOGIN
    public static final int TRACKING_LOGIN_FORGOT = 11;
    public static final int TRACKING_LOGIN_ENTRAR = 12;
    public static final int TRACKING_LOGIN_CREATEACCONT = 13;

    //SOLICITAR NOTIFICAÇÕES
    public static final int TRACKING_SOLICITARNOTFICATION_CONTINUAR = 14;

    //HOME
    public static final int TRACKING_HOME_SELECTHOME = 15;
    public static final int TRACKING_HOME_RELOAD = 16;
    public static final int TRACKING_HOME_TWITTEREXPAND = 17;
    public static final int TRACKING_HOME_TWITTERVERMAIS = 18;
    public static final int TRACKING_HOME_TWITTERVERTODOS = 19;
    public static final int TRACKING_HOME_NOTICIASTRYAGAIN = 20;
    public static final int TRACKING_HOME_NOTICIAOPEN = 21;
    public static final int TRACKING_HOME_ULTIMOS_RESULTADOS = 22;
    public static final int TRACKING_HOME_QUIZ_PREVIEW = 23;
    public static final int TRACKING_HOME_QUIZ_PREVIEW_TRY_AGAIN = 24;
    public static final int TRACKING_HOME_ESCALACAO = 25;
    public static final int TRACKING_HOME_ESCALACAO_TRYAGAIN = 26;
    public static final int TRACKING_HOME_STORIES_EXPAND = 27;
    public static final int TRACKING_HOME_STORIES_TRYAGAIN = 28;
    public static final int TRACKING_HOME_PROXIMOSJOGOS_TRYAGAIN= 136;
    public static final int TRACKING_HOME_PROXIMOSJOGOS_COMPRAR= 137;
    public static final int TRACKING_HOME_PROXIMOSJOGOS_AGENDA= 138;
    public static final int TRACKING_HOME_ULTIMOSRESULTADOR_ABRIR= 141;
    public static final int TRACKING_HOME_CTA= 186;
    public static final int TRACKING_HOME_AUDIOTECA= 170;
    public static final int TRACKING_HOME_DESTAQUES_VERTODOS= 171;
    public static final int TRACKING_HOME_NOTIFICACAO= 180;

    //MURAL 36 --
    public static final int TRACKING_JOGOS_SELECT = 29;
    public static final int TRACKING_JOGOS_PESQUISAS = 30;
    public static final int TRACKING_JOGOS_PALPITES = 31;
    public static final int TRACKING_JOGOS_QUIZ = 32;

    //PESQUISA
    public static final int TRACKING_PESQUISAS_TRYAGAIN = 33;
    public static final int TRACKING_PESQUISAS_PULAR = 34;
    public static final int TRACKING_PESQUISAS_PROXIMA = 35;
    public static final int TRACKING_PESQUISAS_SELECIONAR = 36;
    public static final int TRACKING_PESQUISAS_ABERTA = 37;

    //GRUPOS QUIZ
    public static final int TRACKING_GRUPOQUIZ_TRYGAIN = 38;
    public static final int TRACKING_GRUPOQUIZ_SELECT = 39;
    public static final int TRACKING_GRUPOQUIZ_INICIAR = 40;
    public static final int TRACKING_GRUPOQUIZ_VOLTAR = 41;
    public static final int TRACKING_GRUPOQUIZ_RELOAD = 42;

    //QUIZ
    public static final int TRACKING_QUIZ_TEMPOESGOTADO = 43;
    public static final int TRACKING_QUIZ_RESPONDER = 44;
    public static final int TRACKING_QUIZ_PROXIMA_PERGUNTA_TIMER = 45;
    public static final int TRACKING_QUIZ_PROXIMA_PERGUNTA = 46;
    public static final int TRACKING_QUIZ_PROXIMA_SAIR = 47;

    //SELETOR DE ESCALAÇÂO
    public static final int TRACKING_SELETORESCALACAO_TRYGAIN = 48;
    public static final int TRACKING_SELETORESCALACAO_VOLTAR = 49;
    public static final int TRACKING_SELETORESCALACAO_SELECIONAR = 50;
    public static final int TRACKING_SELETORESCALACAO_ADICIONAR = 51;
    public static final int TRACKING_SELETORESCALACAO_REMOVER = 52;

    //SELETOR ESCALAÇÂO LISTA
    public static final int TRACKING_SELETORESCALACAO_LIST_TRYAGAIN = 53;
    public static final int TRACKING_SELETORESCALACAO_LIST_VOLTAR = 54;
    public static final int TRACKING_SELETORESCALACAO_LIST_SELECIONAR = 55;

    //TIME
    public static final int TRACKING_TIME_SELECIONAR = 56;
    public static final int TRACKING_TIME_RELOAD = 57;
    public static final int TRACKING_TIME_PROXIMOSJOGOS_TRYAGAIN = 58;
    public static final int TRACKING_TIME_PROXIMOSJOGOS_COMPRARINGRESSO = 59;
    public static final int TRACKING_TIME_PROXIMOSJOGOS_ABRIRAGENDA = 60;
    public static final int TRACKING_TIME_ULTIMOS_RESULTADOS_TRYAGAIN = 61;
    public static final int TRACKING_TIME_ELENCO_TRYAGAIN = 62;
    public static final int TRACKING_TIME_ELENCO_SELECIONAR = 63;
    public static final int TRACKING_TIME_ABRIRPARTIDA = 142;

    //AGENDA
    public static final int TRACKING_AGENDA_TRYAGAIN = 64;
    public static final int TRACKING_AGENDA_COMPRARINGRESSO = 65;

    //PERFIL JOGADOR
    public static final int TRACKING_PERFIL_JOGADOR_VOLTAR = 66;
    public static final int TRACKING_PERFIL_JOGADOR_TRYAGAIN = 67;
    public static final int TRACKING_PERFIL_JOGADOR_DADOS = 68;

    //PALPITES
    public static final int TRACKING_PALPITES_TRYAGAIN = 69;
    public static final int TRACKING_PALPITES_PARTICIPAR = 70;
    public static final int TRACKING_PALPITES_PROXIMO = 71;
    public static final int TRACKING_PALPITES_VOLTAR = 72;
    public static final int TRACKING_PALPITES_SELETOR = 73;
    public static final int TRACKING_PALPITES_SELECIONAR = 74;
    public static final int TRACKING_PALPITES_FINALIZAR = 75;
    public static final int TRACKING_PALPITES_VER_PALPITES = 76;
    public static final int TRACKING_PALPITES_BACK = 77;
    public static final int TRACKING_PALPITES_VER_ULTIMOSRESULTADOS = 143;

    //PALPITES_LISTA
    public static final int TRACKING_PALPITES_LISTA_TRYAGAIN = 78;
    public static final int TRACKING_PALPITES_LISTA_VOLTAR = 79;
    public static final int TRACKING_PALPITES_LISTA_SELECIONAR = 80;

    //RANKING
    public static final int TRACKING_RANKING_TRYAGAIN = 81;
    public static final int TRACKING_RANKING_VOLTAR = 82;
    public static final int TRACKING_RANKING_SELECIONAR = 83;

    //LOJA
    public static final int TRACKING_LOJA_VOLTAR = 84;
    public static final int TRACKING_LOJA_TRYAGAIN = 85;
    public static final int TRACKING_LOJA_SELECIONAR = 86;

    //LOJA-PRODUTO
    public static final int TRACKING_LOJA_PRODUTO_VOLTAR = 87;
    public static final int TRACKING_LOJA_PRODUTO_TRYAGAIN = 88;
    public static final int TRACKING_LOJA_PRODUTO_CONFIRMAR = 89;
    public static final int TRACKING_LOJA_PRODUTO_ABRIRREGULAMENTO = 90;

    //LOJA-RESGATAR PRODUTO
    public static final int TRACKING_LOJA_RESGATAR_FECHAR = 91;
    public static final int TRACKING_LOJA_RESGATAR_CONFIRMAR = 92;

    //MENU
    public static final int TRACKING_MENU_VERPERFIL_AVATAR = 93;
    public static final int TRACKING_MENU_SOCIOTORCEDOR = 94;
    public static final int TRACKING_MENU_VERPERFIL = 95;
    public static final int TRACKING_MENU_RANKING = 96;
    public static final int TRACKING_MENU_LOJA = 97;
    public static final int TRACKING_MENU_CONFIGURACAO = 98;
    public static final int TRACKING_MENU_SAIR = 99;
    public static final int TRACKING_MENU_SAIR_SIM = 100;
    public static final int TRACKING_MENU_SAIR_NAO = 101;
    public static final int TRACKING_MENU_SELECIONAR = 148;
    public static final int TRACKING_MENU_AUDIOTECA = 151;
    public static final int TRACKING_MENU_CTA = 187;

    //BADGE
    public static final int TRACKING_DETALHES_BADGE_VOLTAR = 102;

    //WEB
    public static final int TRACKING_WEB_VOLTAR = 103;
    public static final int TRACKING_WEB_TRYAGAIN = 104;

    //WEB_LOJA
    public static final int TRACKING_WEB_LOJA_VOLTAR = 105;
    public static final int TRACKING_WEB_LOJA_TRYAGAIN = 106;

    //SOCIO TORCEDOR
    public static final int TRACKING_SOCIO_VOLTAR = 107;
    public static final int TRACKING_SOCIO_CONFIRMAR = 108;

    //CONFIGURAÇÔES
    public static final int TRACKING_CONFIGURACAO_VOLTAR = 109;
    public static final int TRACKING_CONFIGURACAO_ALTERAR_SENHA = 110;
    public static final int TRACKING_CONFIGURACAO_ALTERAR_DADOS = 111;
    public static final int TRACKING_CONFIGURACAO_DOCS_TRYAGAIN = 112;
    public static final int TRACKING_CONFIGURACAO_DOCS_ABRIR = 113;

    //ALTERAR SENHA
    public static final int TRACKING_ALTERAR_SENHA_VOLTAR = 114;
    public static final int TRACKING_ALTERAR_SENHA_CONFIRMAR = 115;

    //EDITAR PERFIL
    public static final int TRACKING_EDITAR_PERFIL_VOLTAR = 116;
    public static final int TRACKING_EDITAR_PERFIL_CONFIRMAR = 117;
    public static final int TRACKING_EDITAR_PERFIL_ALTERAR_FOTO = 118;
    public static final int TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_CAMERA = 119;
    public static final int TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_BIBLIOTECA = 120;
    public static final int TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_CANCELAR = 121;

    //PERFIL USUARIO
    public static final int TRACKING_PERFIL_VOLTAR = 122;
    public static final int TRACKING_PERFIL_ABRIR_CONFIGURACAO = 123;
    public static final int TRACKING_PERFIL_ALTERAR_FOTO = 124;
    public static final int TRACKING_PERFIL_ALTERAR_FOTO_CAMERA = 125;
    public static final int TRACKING_PERFIL_ALTERAR_FOTO_BIBLIOTECA = 126;
    public static final int TRACKING_PERFIL_ALTERAR_FOTO_CANCELAR = 127;
    public static final int TRACKING_PERFIL_ALTERAR_BADGE_TRYAGAIN = 128;
    public static final int TRACKING_PERFIL_ALTERAR_BADGE_SELECIONAR = 129;
    public static final int TRACKING_PERFIL_ALTERAR_BADGE_RELOAD = 130;

    //NOVA CONQUISTA
    public static final int TRACKING_CONQUISTA_FECHAR = 131;
    public static final int TRACKING_CONQUISTA_PROXIMA = 132;

    //NOTICIA
    public static final int TRACKING_NOTICIA_FECHAR = 133;
    public static final int TRACKING_NOTICIA_COMPARTILHAR = 134;
    public static final int TRACKING_NOTICIA_TRYAGAIN = 135;

    //DETALHES PARTIDAS
    public static final int TRACKING_DETALHES_TREYAGAIN = 139;
    public static final int TRACKING_DETALHES_VOLTAR = 140;

    //MEUS PALPITES
    public static final int TRACKING_MEUSPALPITES_TRYAGAIN = 144;
    public static final int TRACKING_MEUSPALPITES_RELOAD = 145;
    public static final int TRACKING_MEUSPALPITES_VOLTAR = 146;
    public static final int TRACKING_MEUSPALPITES_SELECIONAR_PALPITE = 147;

    //MEU PALPITE
    public static final int TRACKING_MEUPALPITE_TRYAGAIN = 149;
    public static final int TRACKING_MEUPALPITE_FECHAR = 150;

    //AUDIOTECA
    public static final int TRACKING_AUDIOTECA_= 150;
    public static final int TRACKING_AUDIOTECA_TRYAGAIN = 152;
    public static final int TRACKING_AUDIOTECA_FECHAR = 153;
    public static final int TRACKING_AUDIOTECA_TOCAR_DESTAQUE = 154;
    public static final int TRACKING_AUDIOTECA_TOCAR_ITEM = 155;
    public static final int TRACKING_AUDIOTECA_RELOAD = 156;

    //PLAYER
    public static final int TRACKING_PLAYER_LETRAS = 158;
    public static final int TRACKING_PLAYER_REPETIR = 159;
    public static final int TRACKING_PLAYER_ALEATORIO = 160;
    public static final int TRACKING_PLAYER_MUDO = 161;
    public static final int TRACKING_PLAYER_RETROC15 = 162;
    public static final int TRACKING_PLAYER_AVAN15 = 163;
    public static final int TRACKING_PLAYER_ANTERIOR = 164;
    public static final int TRACKING_PLAYER_PROXIMA = 165;
    public static final int TRACKING_PLAYER_PLAY = 166;
    public static final int TRACKING_PLAYER_PAUSE = 167;
    public static final int TRACKING_PLAYER_BAIXAR = 168;
    public static final int TRACKING_PLAYER_FECHAR = 169;

    //DESTAQUES
    public static final int TRACKING_DESTAQUES_VOLTAR = 172;
    public static final int TRACKING_DESTAQUES_TRYAGAIN = 173;
    public static final int TRACKING_DESTAQUES_RELOAD = 174;
    public static final int TRACKING_DESTAQUES_SELECIONAR = 175;
    public static final int TRACKING_DESTAQUES_FILTROS_ABRIR = 176;
    public static final int TRACKING_DESTAQUES_FILTROS_FECHAR = 177;
    public static final int TRACKING_DESTAQUES_FILTROS_TRYAGAIN = 178;
    public static final int TRACKING_DESTAQUES_FILTROS_SELECIONAR = 179;


    //CENTRAL DE NOTIFICAÇÔES
    public static final int TRACKING_NOTIFICACAO_TRYAGAIN = 181;
    public static final int TRACKING_NOTIFICACAO_VOLTAR = 182;
    public static final int TRACKING_NOTIFICACAO_SELECIONAR = 183;
    public static final int TRACKING_NOTIFICACAO_MARCAR_TODAS = 184;
    public static final int TRACKING_NOTIFICACAO_RELOAD = 185;


    public static final int TRACKING_GETSERVER_ERROR = 225;










    //PERFIL
    public static final int TRACKING_PERFIL_SELECTPERFIL = 50;
    public static final int TRACKING_PERFIL_OPENNOTIFICATIONS = 51;
    public static final int TRACKING_PERFIL_CHANGEAVATAR = 52;
    public static final int TRACKING_PERFIL_CHANGEAVATARCAMERA = 53;
    public static final int TRACKING_PERFIL_CHANGEAVATARBIBLIOTECA = 54;
    public static final int TRACKING_PERFIL_CHANGEAVATARCANCEL = 55;
    public static final int TRACKING_PERFIL_EDITPERFIL = 56;
    public static final int TRACKING_PERFIL_PRENCHERNASCIMENTO = 57;
    public static final int TRACKING_PERFIL_PRENCHERGENERO = 58;
    public static final int TRACKING_PERFIL_PRENCHERPESQUISAS = 59;
    public static final int TRACKING_PERFIL_PRENCHERQUIZ = 60;
    public static final int TRACKING_PERFIL_SAIR = 61;
    public static final int TRACKING_PERFIL_VERRECEITA = 161;
    public static final int TRACKING_PERFIL_VERTREINAMENTOS = 162;

    //EDITAR PERFIL
    public static final int TRACKING_EDITARPERFIL_CANCELAR = 62;
    public static final int TRACKING_EDITARPERFIL_CONCLUIR = 63;

    //MENU
    public static final int TRACKING_MENU_SELECTMENU = 64;
    public static final int TRACKING_MENU_OPENNOTIFICATION = 65;
    public static final int TRACKING_MENU_BUSCA = 66;
    public static final int TRACKING_MENU_ABRIRAGENDA = 67;
    public static final int TRACKING_MENU_ABRIRCAMPANHA = 68;
    public static final int TRACKING_MENU_ABRIRCATALOGOS = 69;
    public static final int TRACKING_MENU_ABRIRFALECONOSCO = 70;
    public static final int TRACKING_MENU_ABRIRNOTICIAS = 71;
    public static final int TRACKING_MENU_ABRIRPESQUISAS = 72;
    public static final int TRACKING_MENU_ABRIRPRODUTOS = 73;
    public static final int TRACKING_MENU_ABRIRPREMIOS = 74;
    public static final int TRACKING_MENU_ABRIRQUIZ = 75;
    public static final int TRACKING_MENU_ABRIRRANKING = 76;
    public static final int TRACKING_MENU_ABRIRRECEITAS = 77;
    public static final int TRACKING_MENU_ABRIRTREINAMENTOS = 78;
    public static final int TRACKING_MENU_ABRIRCALCULADORA = 166;

    //NOTIFICAÇÕES
    public static final int TRACKING_NOTIFICATION_AGENDA = 79;
    public static final int TRACKING_NOTIFICATION_CAMPANHA = 80;
    public static final int TRACKING_NOTIFICATION_FALECONOSCO = 81;
    public static final int TRACKING_NOTIFICATION_NOTICIA = 82;
    public static final int TRACKING_NOTIFICATION_PESQUISA = 83;
    public static final int TRACKING_NOTIFICATION_PRODUTO = 84;
    public static final int TRACKING_NOTIFICATION_QUIZ = 85;
    public static final int TRACKING_NOTIFICATION_RECEITA = 86;
    public static final int TRACKING_NOTIFICATION_TREINAMENTO = 87;
    public static final int TRACKING_NOTIFICATION_MURAL = 88;
    public static final int TRACKING_NOTIFICATION_NORMAL = 89;
    //RECEITAS
    public static final int TRACKING_RECEITAS_TRYAGAIN = 99;
    public static final int TRACKING_RECEITAS_RELOAD = 100;
    public static final int TRACKING_RECEITAS_FILTER = 101;
    public static final int TRACKING_RECEITAS_OPENRECEITA = 102;

    //RECEITA
    public static final int TRACKING_RECEITA_OPENPDF = 109;
    public static final int TRACKING_RECEITA_OPENCALCULADORA = 110;

    //CALCULADORA
    public static final int TRACKING_CALCULADORA_REINICIAR= 167;
    public static final int TRACKING_CALCULADORA_REINICARSIM= 168;
    public static final int TRACKING_CALCULADORA_REINICARNAO= 169;
    public static final int TRACKING_CALCULADORA_RESGATARGRUPO= 170;
    public static final int TRACKING_CALCULADORA_RESGATARGRUPOSIM= 171;
    public static final int TRACKING_CALCULADORA_RESGATARGRUPONAO= 172;
    public static final int TRACKING_CALCULADORA_EDITARITEM= 173;
    public static final int TRACKING_CALCULADORA_EDITARITEMCONFIRMAR= 174;
    public static final int TRACKING_CALCULADORA_EDITARITEMCANCELAR= 175;
    public static final int TRACKING_CALCULADORA_AUMENTARMUTIPLICADOR= 176;
    public static final int TRACKING_CALCULADORA_DIMINUIRMUTIPLICADOR= 177;

    //VOUCHER
    public static final int TRACKING_VOUCHER_AJUDA= 180;
    public static final int TRACKING_VOUCHER_ABRIRLISTA= 181;
    public static final int TRACKING_VOUCHER_MOSTRARITEM= 182;
    public static final int TRACKING_VOUCHER_SUBMETERCODIGO= 183;

    //AJUDA VOUCHER
    public static final int TRACKING_AJUDAVOUCHER_FECHAR= 184;
    public static final int TRACKING_AJUDAVOUCHER_VERVIDEO= 185;
    public static final int TRACKING_AJUDAVOUCHER_CONFIRMAR= 186;

    //PREMIO VOUCHER
    public static final int TRACKING_PREMIOVOUCHER_FECHAR= 187;
    public static final int TRACKING_PREMIOVOUCHER_RESGATAR= 188;
    public static final int TRACKING_PREMIOVOUCHER_RESGATARNAO= 189;
    public static final int TRACKING_PREMIOVOUCHER_RESGATARSIM= 190;

    //PUSH
    public static final int TRACKING_PUSH_NOTICIA = 96;
    public static final int TRACKING_PUSH_PESQUISA = 97;
    public static final int TRACKING_PUSH_QUIZ = 98;
    public static final int TRACKING_PUSH_COMUM = 99;
    public static final int TRACKING_PUSH_FALE_CONOSCO = 100;
    public static final int TRACKING_PUSH_INFORMATIVO  = 101;
    public static final int TRACKING_PUSH_RECEITA      = 102;
    public static final int TRACKING_PUSH_TREINAMENTO  = 103;
    public static final int TRACKING_PUSH_MURAL  = 104;

    //BUSCA
    public static final int TRACKING_BUSCA_NOTICIA = 105;
    public static final int TRACKING_BUSCA_MURAL = 106;
    public static final int TRACKING_BUSCA_RECEITAS = 107;
    public static final int TRACKING_BUSCA_CATALOGO = 108;
    public static final int TRACKING_BUSCA_TREINAMENTO = 109;
    public static final int TRACKING_BUSCA_PRODUTOS = 110;
    public static final int TRACKING_BUSCA_INFORMATIVOS = 111;
    public static final int TRACKING_BUSCA_PREMIOS = 112;
    public static final int TRACKING_BUSCA_AGENDA = 114;

    //ERRORS
    public static final String ERROR_MC001 = "ERROR MC001";
    public static final String ERROR_MC002 = "ERROR MC002";
    public static final String ERROR_MC003 = "ERROR MC003";
    public static final String ERROR_MC004 = "ERROR MC004";

    public static final String ERROR_MG001 = "ERROR MG001";
    public static final String ERROR_MG002 = "ERROR MG002";
    public static final String ERROR_MG003 = "ERROR MG003";
    public static final String ERROR_MG004 = "ERROR MG004";
    public static final String ERROR_MG005 = "ERROR MG005";
    public static final String ERROR_MG006 = "ERROR MG006";
    public static final String ERROR_MG007 = "ERROR MG007";

    public static final String ERROR_PP001 = "ERROR PP001";
    public static final String ERROR_PP002 = "ERROR PP002";
    public static final String ERROR_PP003 = "ERROR PP003";
    public static final String ERROR_PP004 = "ERROR PP004";
    public static final String ERROR_PP005 = "ERROR PP005";
    public static final String ERROR_PP006 = "ERROR PP006";

    private static String AddBtnIdToApiHeader(int btnId, int varId){

        String str = ":";
        if(btnId != -1){
            str += String.valueOf(btnId);
        }
        if(varId != -1){
            str += "/" + String.valueOf(varId);
        }

        return str;
    }

    private static String AddBtnIdToApiHeader(int btnId, String  varId){

        String str = ":";
        if(btnId != -1){
            str += String.valueOf(btnId);
        }
        if(!varId.isEmpty()){
            str += "/" + varId;
        }

        return str;
    }

    private static String AddTokenToApiHeader(){
        return (":" + UserInformation.getUserData().getToken());
    }

    public static String buildHeader(int btnId, int varId){
        String header = (UTILS.getApiHeaderValue() + AddBtnIdToApiHeader(btnId, varId) + AddTokenToApiHeader()+ getSesionId());
        UTILS.DebugLog("Header", header);
        return header;
    }

    public static String buildHeader(int btnId, String varId){
        String header = (UTILS.getApiHeaderValue() + AddBtnIdToApiHeader(btnId, varId) + AddTokenToApiHeader() + getSesionId());
        UTILS.DebugLog("Header", header);
        return header;
    }

    public static String getSesionId (){
        return ":" + UserInformation.getSessionId();

    }

    public static String getTrackingButtonName(int id){
        String name = "TRACK";
            //        switch (id){
//            default:                                            name = "???";                                           break;
//            case TRACKING_HOME_VER_CONQUISTAS:                  name = "TRACKING_HOME_VER_CONQUISTAS";                  break;
//            case TRACKING_HOME_NOTICIA_CARROSSEL_SUPERIOR:      name = "TRACKING_HOME_NOTICIA_CARROSSEL_SUPERIOR";      break;
//            case TRACKING_HOME_MURAL:                           name = "TRACKING_HOME_MURAL";                           break;
//            case TRACKING_HOME_QUIZZ:                           name = "TRACKING_HOME_QUIZZ";                           break;
//            case TRACKING_HOME_CATALOGOS:                       name = "TRACKING_HOME_CATALOGOS";                       break;
//            case TRACKING_HOME_ESSENTIALS_RECEITAS:             name = "TRACKING_HOME_ESSENTIALS_RECEITAS";             break;
//            case TRACKING_HOME_ESSENTIALS_TREINAMENTOS:         name = "TRACKING_HOME_ESSENTIALS_TREINAMENTOS";         break;
//            case TRACKING_HOME_ESSENTIALS_PRODUTOS:             name = "TRACKING_HOME_ESSENTIALS_PRODUTOS";             break;
//            case TRACKING_HOME_INFORMATIVOS:                    name = "TRACKING_HOME_INFORMATIVOS";                    break;
//            case TRACKING_HOME_PREMIOS:                         name = "TRACKING_HOME_PREMIOS";                         break;
//            case TRACKING_HOME_PESQUISAS:                       name = "TRACKING_HOME_PESQUISAS";                       break;
//            case TRACKING_HOME_NOTICIA_CARROSSEL_INFERIOR:      name = "TRACKING_HOME_NOTICIA_CARROSSEL_INFERIOR";      break;
//            case TRACKING_NOTICIAS_ABRIR_NOTICIA:               name = "TRACKING_NOTICIAS_ABRIR_NOTICIA";               break;
//            case TRACKING_QUIZZ_RESPONDER_PERGUNTA:             name = "TRACKING_QUIZZ_RESPONDER_PERGUNTA";             break;
//            case TRACKING_RECEITAS_ABRIR_RECEITA:               name = "TRACKING_RECEITAS_ABRIR_RECEITA";               break;
//            case TRACKING_TREINAMENTOS_ABRIR_TREINAMENTO:       name = "TRACKING_TREINAMENTOS_ABRIR_TREINAMENTO";       break;
//            case TRACKING_PRODUTOS_ABRIR_PRODUTO:               name = "TRACKING_PRODUTOS_ABRIR_PRODUTO";               break;
//            case TRACKING_RECEITA_CALCULADORA:                  name = "TRACKING_RECEITA_CALCULADORA";                  break;
//            case TRACKING_HOME_CALCULADORA:                     name = "TRACKING_HOME_CALCULADORA";                     break;
//            case TRACKING_PUSH_NOTICIA:                         name = "TRACKING_PUSH_NOTICIA";                         break;
//            case TRACKING_PUSH_PESQUISA:                        name = "TRACKING_PUSH_PESQUISA";                        break;
//            case TRACKING_PUSH_QUIZ:                            name = "TRACKING_PUSH_QUIZ";                            break;
//            case TRACKING_PUSH_COMUM:                           name = "TRACKING_PUSH_COMUM";                           break;
//            case TRACKING_PUSH_FALE_CONOSCO:                    name = "TRACKING_PUSH_FALE_CONOSCO";                    break;
//            case TRACKING_PUSH_INFORMATIVO:                     name = "TRACKING_PUSH_INFORMATIVO";                     break;
//            case TRACKING_PUSH_RECEITA:                         name = "TRACKING_PUSH_RECEITA";                         break;
//            case TRACKING_PUSH_TREINAMENTO:                     name = "TRACKING_PUSH_TREINAMENTO";                     break;
//            case TRACKING_PUSH_MURAL:                           name = "TRACKING_PUSH_MURAL";                           break;
//            case TRACKING_BUSCA_NOTICIA:                        name = "TRACKING_BUSCA_NOTICIA";                        break;
//            case TRACKING_BUSCA_MURAL:                          name = "TRACKING_BUSCA_MURAL";                          break;
//            case TRACKING_BUSCA_RECEITAS:                       name = "TRACKING_BUSCA_RECEITAS";                       break;
//            case TRACKING_BUSCA_CATALOGO:                       name = "TRACKING_BUSCA_CATALOGO";                       break;
//            case TRACKING_BUSCA_TREINAMENTO:                    name = "TRACKING_BUSCA_TREINAMENTO";                    break;
//            case TRACKING_BUSCA_PRODUTOS:                       name = "TRACKING_BUSCA_PRODUTOS";                       break;
//            case TRACKING_BUSCA_INFORMATIVOS:                   name = "TRACKING_BUSCA_INFORMATIVOS";                   break;
//            case TRACKING_BUSCA_PREMIOS:                        name = "TRACKING_BUSCA_PREMIOS";                        break;
//            case TRACKING_HOME_BUSCA:                           name = "TRACKING_HOME_BUSCA";                           break;
//            case TRACKING_PUSH_EVENTO:                          name = "TRACKING_PUSH_EVENTO";                          break;
//            case TRACKING_HOME_NOTIFICACOES:                    name = "TRACKING_HOME_NOTIFICACOES";                    break;
//            case TRACKING_NOTIFICACOES_ABRIR_NOTIFICACAO:       name = "TRACKING_NOTIFICACOES_ABRIR_NOTIFICACAO";       break;
//            case TRACKING_HOME_AGENDA:                          name = "TRACKING_HOME_AGENDA";                          break;
//            case TRACKING_AGENDA_ABRIR_EVENTO:                  name = "TRACKING_AGENDA_ABRIR_EVENTO";                  break;
//            case TRACKING_AGENDA_ABRIR_COMPARTILHAR_PDF_EVENTO: name = "TRACKING_AGENDA_ABRIR_COMPARTILHAR_PDF_EVENTO"; break;
//            case TRACKING_AGENDA_ABRIR_PDF_EVENTO:              name = "TRACKING_AGENDA_ABRIR_PDF_EVENTO";              break;
//
//        }
        return name;
    }

}
