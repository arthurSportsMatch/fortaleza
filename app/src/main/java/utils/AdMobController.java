package utils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AdMobController {

    public static InterstitialAd mInterstitialAd;

    static InterstitialAd mInterstitialCampo;
    static InterstitialAd mInterstitialQuiz;
    static InterstitialAd mInterstitialPalpites;

    static public final String TEST_TOP_BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
    static public final String TEST_INTERSECTIAL_ID = "ca-app-pub-3940256099942544/1033173712";
    static public final String HOME_TOP_BANNER_ID = "ca-app-pub-1238536592599880/3743888137";
    static public final String HOME_INTERSECTIAL_CAMPO_ID = "ca-app-pub-1238536592599880/8393030841";
    static public final String HOME_INTERSECTIAL_NOTICIA_ID = "ca-app-pub-1238536592599880/1856091391";
    static public final String QUIZ_BOTTOM_ID = "ca-app-pub-1238536592599880/4809009057";
    static public final String QUIZ_INTERSECTIAL_ID = "ca-app-pub-1238536592599880/9795109406";
    static public final String PALPITES_INTERSECTIAL_ID = "ca-app-pub-1238536592599880/9678192354";
    static public final String TIME_TOP_BANNER_ID = "ca-app-pub-1238536592599880/5356352708";
    static public final String AGENDA_INTERSECTIAL_ID = "ca-app-pub-3940256099942544/1033173712";
    static public final String AGENDA_BOTTOM_BANNER_ID = "ca-app-pub-1238536592599880/6449942524";
    static public final String NOTICIA_BOTTOM_BANNER_ID = "ca-app-pub-1238536592599880/5767416245";

    static void AdMobController (Activity activity){

    }

    //Iniciar add e logo deppis iniciar a activity
    static public void starIntetentBeforeAd (final Activity activity, final Intent intent, String ID){
        mInterstitialAd = getmInterstitialAdType(ID);

        if(mInterstitialAd != null){
            //Caso não tenha o mesmo id da requisição
//            if(!mInterstitialAd.getAdUnitId().equals(ID)){
//                mInterstitialAd.setAdUnitId(ID);
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
//            }

            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
                        startActivityAd(activity, intent);
                    }
                });

            }else{
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                startActivityAd(activity, intent);
            }
        }else{
            mInterstitialAd = new InterstitialAd(activity);
            mInterstitialAd.setAdUnitId(ID);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            setmInterstitialAdType(mInterstitialAd, ID);
            starIntetentBeforeAd(activity,intent,ID);
        }
    }

    static InterstitialAd getmInterstitialAdType (String ID){
        switch (ID){
            case HOME_INTERSECTIAL_CAMPO_ID :
                return mInterstitialCampo;
            case PALPITES_INTERSECTIAL_ID :
                return mInterstitialPalpites;
            case QUIZ_INTERSECTIAL_ID :
                return mInterstitialQuiz;
            default:return null;
        }
    }

    static void setmInterstitialAdType (InterstitialAd interstitialAd, String ID){
        switch (ID){
            case HOME_INTERSECTIAL_CAMPO_ID :
                mInterstitialCampo = interstitialAd;
                break;
            case PALPITES_INTERSECTIAL_ID :
                mInterstitialPalpites = interstitialAd;
                break;
            case QUIZ_INTERSECTIAL_ID :
                mInterstitialQuiz = interstitialAd;
                break;
        }
    }

    //Iniciar add e logo depois iniciar listener
    static public void startBeforeAd (final Activity activity, AdListener listener, String ID){
        if(mInterstitialAd != null){

            if(mInterstitialAd.getAdUnitId() != null){
                //Caso não tenha o mesmo id da requisição
                if(!mInterstitialAd.getAdUnitId().equals(ID)){
                    mInterstitialAd.setAdUnitId(ID);
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            }else{
                mInterstitialAd.setAdUnitId(ID);
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
                mInterstitialAd.setAdListener(listener);
            }else{
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                listener.onAdClosed();
            }

        }else{
            mInterstitialAd = new InterstitialAd(activity);
            mInterstitialAd.setAdUnitId(ID);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            setmInterstitialAdType(mInterstitialAd, ID);
            startBeforeAd(activity,listener,ID);
        }
    }

    //Iniciar add e logo depois iniciar activity esperando resultados
    static public void starIntetentResultBeforeAd (final Activity activity, final  Intent intent, final int resultId, String ID){
        if(mInterstitialAd != null){
            //Caso não tenha o mesmo id da requisição
            if(!mInterstitialAd.getAdUnitId().equals(ID)){
                mInterstitialAd.setAdUnitId(ID);
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }


            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
                        startActivityResultAd(activity, intent, resultId);
                    }
                });

            }else{
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                startActivityResultAd(activity, intent, resultId);
            }
        }else{
            mInterstitialAd = new InterstitialAd(activity);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            starIntetentResultBeforeAd(activity,intent,resultId,ID);
        }
    }

    static public void createBannerAd (Activity activity, LinearLayout view, AdSize size, String ID){
        //Creating a view
        AdView adView = new AdView(activity);
        adView.setAdSize(size);
        adView.setAdUnitId(ID);

        //Adding this view to current layout
        view.addView(adView);

        //Requesting this ad
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    //Inicia atividade
    static void startActivityAd (Activity activity, Intent intent) {
        activity.startActivity(intent);
    }

    //Inicia atividade esperando resultados
    static void startActivityResultAd (Activity activity, Intent intent, int resultIntent){
        activity.startActivityForResult(intent,resultIntent);
    }
}

/*
320x50	Banner	                        Phones and Tablets	                    BANNER
320x100	Large Banner	                Phones and Tablets	                    LARGE_BANNER
300x250	IAB Medium Rectangle	        Phones and Tablets	                    MEDIUM_RECTANGLE
468x60	IAB Full-Size Banner	        Tablets	                                FULL_BANNER
728x90	IAB Leaderboard	                Tablets	                                LEADERBOARD
Provided width x Adaptive height	    Adaptive banner	Phones and Tablets	    N/A
Screen width x 32|50|90	Smart banner	Phones and Tablets	                    SMART_BANNER
 */