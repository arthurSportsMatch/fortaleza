package utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.sportm.fortaleza.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.sportm.fortaleza.WinBadgeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data.ServerCustomData;
import data.ServerData;
import data.SocioData;
import informations.UserInformation;

import static com.sportm.fortaleza.MainActivity.prefs;

/**
 * Created by rafael.verginelli on 10/03/2016.
 */
public class UTILS implements AsyncOperation.IAsyncOpCallback{

    private static final String TAG = "UTILS";

    //APP
    private static boolean isAppRunning = false;
    private static boolean isUserLoggedIn = false;

    //DOWNLOAD FILE
    private static final int BUFFER_SIZE = 4096 * 10;

    //DEBUG
    public final static boolean isDebugEnabled = true;

    //REQUEST
    private static final int OP_GET_VIDEO_IMAGE = 0;
    private boolean isDownloadingVideoImage = false;


    public static final int FLIP_VERTICAL = 1;
    public static final int FLIP_HORIZONTAL = 2;

    //There is no knowledge that is not power
    public static synchronized void DebugLog(String TAG, Object o) {
        Log.d(TAG, String.valueOf(o));
    }

    public static void setUserLoggedIn(boolean isLoggedIn){
        isUserLoggedIn = isLoggedIn;
    }

    public static boolean isUserLoggedIn(){
        return isUserLoggedIn;
    }

    public static void setAppRunning(boolean isRunning){
        isAppRunning = isRunning;
    }

    public static boolean isAppRunning(){
        return isAppRunning;
    }

    public static String getApiHeaderValue(){
        return (UserInformation.getUserId() + "");
    }

   public static  Hashtable<String, Object> SocioToHash (SocioData data){
        Hashtable<String, Object> params = new Hashtable<>();
        if(data.getBairro()!= null && !data.getBairro().isEmpty())
            params.put("bairro", data.getBairro());

       if(data.getUf()!= null && !data.getUf().isEmpty())
           params.put("uf", data.getUf());

       if(data.getCidade()!= null && !data.getCidade().isEmpty())
           params.put("cidade", data.getCidade());

       if(data.getNome()!= null && !data.getNome().isEmpty())
           params.put("nome", data.getNome());

       if(data.getCidade()!= null && !data.getCidade().isEmpty())
           params.put("cidade", data.getCidade());

       if(data.getCep()!= null && !data.getCep().isEmpty())
           params.put("cep", data.getCep());

       if(data.getNumero()!= null && !data.getNumero().isEmpty() )
           params.put("numero", data.getNumero());

       if(data.getNome()!= null && !data.getNome().isEmpty())
           params.put("nome", data.getNome());

       if(data.getCpf()!= null && !data.getCpf().isEmpty())
           params.put("cpf", data.getCpf());

       if(data.getEndereco()!= null && !data.getEndereco().isEmpty())
           params.put("endereco", data.getEndereco());

       if(data.getId()!= 0)
           params.put("idpessoa", data.getId());

       if(data.getStatus()!= null && !data.getStatus().isEmpty())
           params.put("status", data.getStatus());

       if(data.getIsActive()!= null)
           params.put("isActive", data.getIsActive());

        return params;
    }
//

    public static void checkBadgeResponse (Activity activity, JSONObject response){
        if(response.has("newBadges")){
            try {
                String badges = response.getString("newBadges");
                Intent intent = new Intent(activity, WinBadgeActivity.class);
                intent.putExtra("badges", badges);
                activity.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean checkSocioIsActive (String  status){
        switch (status){
            case "quitada":{
            }
            case "inadimplente":{
            }
            case "adimplente":{
            }
            case "a_iniciar":{
                return true;
            }
        }
        return false;
    }

    public static boolean checkSocioBeneficioIsActive (String  status){
        switch (status){
            case "quitada":{
                return true;
            }
            case "atrasada":{
                return true;
            }
            case "adimplente":{
                return true;
            }
        }
        return false;
    }

    public static String camelCase(String stringToConvert) {
        if (TextUtils.isEmpty(stringToConvert))
        {return "";}
        return Character.toUpperCase(stringToConvert.charAt(0)) +
                stringToConvert.substring(1).toLowerCase();
    }

    public static String getFormattedDate(String date){
        try {
            String[] datetime = date.split(" ");
            String _date = "";
            String[] dt = datetime[0].split("-");
            _date = dt[2] + "/" +dt[1] + "/" + dt[0];
            String time = "";
            String[] tm = datetime[1].split(":");
            time = tm[0] + ":" + tm[1];

            date = _date + " - " + time;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    public static String getNameFromFile(String filename){
        String str = "";

        if( (filename != null) && (!filename.isEmpty()) ){
            String[] parts = filename.split("/");
            if( (parts != null) && (parts.length > 0) ){
                str = parts[parts.length - 1];
            }
        }

        return str;
    }

    @SuppressLint("NewApi")
    public static String getPath(Context context, Uri uri) {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;

        if (needToCheckUri && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return "/storage/emulated/0/" + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "");
                }
                uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.parseLong(id));

            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                switch (type) {
                    case "image":
                        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "video":
                        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "audio":
                        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        break;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    return cursor.getString(columnIndex);
                }
            } catch (Exception e) {
                Log.e("on getPath", "Exception", e);
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getRealPathFromURI(final Context context, final Uri uri) {

        if (uri.getScheme().equals("file")) {
            return uri.toString();

        } else if (uri.getScheme().equals("content")) {
            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (DocumentsContract.isDocumentUri(context, uri)) {

                    // ExternalStorageProvider
                    if (isExternalStorageDocument(uri)) {
                        final String docId = DocumentsContract.getDocumentId(uri);
                        final String[] split = docId.split(":");
                        final String type = split[0];

                        if ("primary".equalsIgnoreCase(type)) {
                            return Environment.getExternalStorageDirectory() + "/" + split[1];
                        }

                        // to_do handle non-primary volumes
                    }
                    // DownloadsProvider
                    else if (isDownloadsDocument(uri)) {

                        String id = DocumentsContract.getDocumentId(uri);

                        long idl = -1l;
                        try{
                            idl = Long.valueOf(id);
                        }
                        catch (Exception e){
                            UTILS.DebugLog(TAG, e);
                        }

                        if(idl == -1){
                            String prefix = "raw:";
                            if(id.startsWith(prefix)){
                                id = id.substring(prefix.length(), id.length());
                            }
                            return id;
                        }

                        final Uri contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));


                        return getDataColumn(context, contentUri, null, null);
                    }
                    // MediaProvider
                    else if (isMediaDocument(uri)) {
                        final String docId = DocumentsContract.getDocumentId(uri);
                        final String[] split = docId.split(":");
                        final String type = split[0];

                        Uri contentUri = null;
                        if ("image".equals(type)) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(type)) {
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(type)) {
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        final String selection = "_id=?";
                        final String[] selectionArgs = new String[]{
                                split[1]
                        };

                        return getDataColumn(context, contentUri, selection, selectionArgs);
                    }
                }
                // MediaStore (and general)
                else if ("content".equalsIgnoreCase(uri.getScheme())) {

                    // Return the remote address
                    if (isGooglePhotosUri(uri)) {
                        return uri.getLastPathSegment();
                    }

                    return getDataColumn(context, uri, null, null);
                }
                // File
                else if ("file".equalsIgnoreCase(uri.getScheme())) {
                    return uri.getPath();
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }


    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String formatHourFromDBEvent(String dt){
        boolean success = false;
        String formatted = "";

        if(dt.contains(" ")){
            String[] hourDt = dt.split(" ");
            if(hourDt[1].contains(":")) {
                String[] hhmmss = hourDt[1].split(":");
                if(hhmmss.length == 3){
                    formatted = hhmmss[0] + ":" + hhmmss[1];
                }
                else {
                    formatted = hourDt[1];
                }
                success = true;
            }
        }

        return (success ? formatted : dt);
    }

    public static String formatDateToDB(String dt){
        boolean success = false;
        String formatted = "";

        if(dt.contains(" ")){
            String[] hourDt = dt.split(" ");

            String justDate = hourDt[0];
            String[] unformatted = justDate.split("/");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "-" +
                        unformatted[1] + "-" +
                        unformatted[0];

                success = true;
            }
        }
        else {
            String[] unformatted = dt.split("/");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "-" +
                        unformatted[1] + "-" +
                        unformatted[0];

                success = true;
            }
        }

        return (success ? formatted : dt);
    }

    private static boolean canFilter = true;
    public static void SetEditTextForDecimals(final EditText etxt, final boolean twoDecimals){
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if(!canFilter){
                    canFilter = true;
                    return null;
                }

                for (int i = start; i < end; i++) {
                    if ( (String.valueOf(source.charAt(i)).equals(",")) || (String.valueOf(source.charAt(i)).equals("."))){
                        if( (etxt.getText().toString().contains(",")) || ((etxt.getText().toString().contains("."))) ){
                            return "";
                        }
                    }
                }

                return null;
            }
        };

        etxt.setFilters(new InputFilter[] { filter });

        etxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    String editable = etxt.getText().toString().replace(".", ",");
                    if(editable.length() > 0) {
                        if(editable.contains(",")){

                            if (String.valueOf(editable.charAt(editable.length() - 1)).equals(",")) {
                                canFilter = false;
                                etxt.setText(editable + "00");
                            }

                            if(twoDecimals) {

                                editable = etxt.getText().toString();

                                String[] div = editable.split(",");

                                if ((div.length > 1) && (div[1] != null) && (div[1].length() > 2)) {
                                    editable = etxt.getText().toString().replace(",", "");
                                    String concat = "," + editable.substring(editable.length() - 2, editable.length());
                                    editable = editable.substring(0, editable.length() - 2) + concat;
                                    canFilter = false;
                                    etxt.setText(editable);
                                }
                            }
                        }
                        else if(editable.contains(".")){

                            if (String.valueOf(editable.charAt(editable.length() - 1)).equals(".")) {
                                canFilter = false;
                                etxt.setText(editable + "00");
                            }

                            if(twoDecimals) {

                                editable = etxt.getText().toString();

                                String[] div = editable.split(".");

                                if ((div.length > 1) && (div[1] != null) && (div[1].length() > 2)) {
                                    editable = etxt.getText().toString().replace(".", "");
                                    String concat = "." + editable.substring(editable.length() - 2, editable.length());
                                    editable = editable.substring(0, editable.length() - 2) + concat;
                                    canFilter = false;
                                    etxt.setText(editable);
                                }
                            }
                        }
                        else {
                            canFilter = false;
                            etxt.setText(etxt.getText() + ",00");
                        }

                        canFilter = false;
                        etxt.setText(etxt.getText().toString().replace(".", ","));
                    }
                }
            }
        });
    }

    public static String getRealPathFromURI2(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String dateZuluToGMT (String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());
        try {
            return getGMTHour(dateFormat.parse(date).toString());
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static String getGMTHour (String date){
        String[] chunks = date.split(" ");
        if(!chunks[3].isEmpty()){
            return chunks[3];
        }else{
            return date;
        }
    }

    public static String formatDateFromDB(String dt){
        boolean success = false;
        String formatted = "";

        if(dt.contains(" ")){
            String[] hourDt = dt.split(" ");

            String justDate = hourDt[0];
            String[] unformatted = justDate.split("-");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "/" +
                        unformatted[1] + "/" +
                        unformatted[0];

                success = true;
            }
        }
        else {
            String[] unformatted = dt.split("-");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "/" +
                        unformatted[1] + "/" +
                        unformatted[0];

                success = true;
            }
        }

        return (success ? formatted : dt);
    }

    public static boolean hasImage(String url){
        url = url.toUpperCase();

        return ((url.endsWith(".PNG"))     ||
                (url.endsWith(".JPG"))     ||
                (url.endsWith(".JPEG"))    ||
                (url.endsWith(".GIF"))     ||
                (url.endsWith(".BMP"))     ||
                (url.endsWith(".PSD"))     ||
                (url.endsWith(".ICO"))     ||
                (url.endsWith(".TGA"))     ||
                (url.endsWith(".TIF"))     ||
                (url.endsWith(".TIFF"))    ||
                (url.endsWith(".JIF"))     ||
                (url.endsWith(".JFIF"))    ||
                (url.endsWith(".JP2"))     ||
                (url.endsWith(".JPX"))     ||
                (url.endsWith(".J2K"))     ||
                (url.endsWith(".J2C"))     ||
                (url.endsWith(".FPX"))     ||
                (url.endsWith(".PCD"))     ||
                (url.endsWith(".EXIF"))    ||
                (url.endsWith(".BPG"))     ||
                (url.endsWith(".PPM"))     ||
                (url.endsWith(".PGM"))     ||
                (url.endsWith(".PBM"))     ||
                (url.endsWith(".PNM"))     );
    }

    public static String formatDateFromDBEvent(String dt){
        boolean success = false;
        String formatted = "";

        if(dt.contains(" ")){
            String[] hourDt = dt.split(" ");

            String justDate = hourDt[0];
            String[] unformatted = justDate.split("-");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "." +
                        unformatted[1] + "." +
                        unformatted[0];

                success = true;
            }
        }
        else {
            String[] unformatted = dt.split("-");
            if (unformatted.length >= 3) {
                formatted = unformatted[2] + "." +
                        unformatted[1] + "." +
                        unformatted[0];

                success = true;
            }
        }

        return (success ? formatted : dt);
    }

    public static String compressImage(Context context, String imageUri) {

        String filePath = UTILS.getRealPathFromURI(context, imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = UTILS.calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            UTILS.DebugLog("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                UTILS.DebugLog("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                UTILS.DebugLog("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                UTILS.DebugLog("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = UTILS.getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private static String getRealPathFromURI(Context context, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static String CropText(String content, int size){
        if(content.length() > size){
            if(size >= 4) {
                content = content.substring(0, (size - 3));
            }
            else {
                content = content.substring(0, size);
            }
            content += "...";
        }

        return content;
    }

    public static void CopyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static Bitmap resizeBitmapImage(String filePath, int targetWidth, int targetHeight) {
        Bitmap bitMapImage = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            double sampleSize = 0;
            Boolean scaleByHeight = Math.abs(options.outHeight - targetHeight) >= Math.abs(options.outWidth
                    - targetWidth);
            if (options.outHeight * options.outWidth * 2 >= 1638) {
                sampleSize = scaleByHeight ? options.outHeight / targetHeight : options.outWidth / targetWidth;
                sampleSize = (int) Math.pow(2d, Math.floor(Math.log(sampleSize) / Math.log(2d)));
            }
            options.inJustDecodeBounds = false;
            options.inTempStorage = new byte[128];
            while (true) {
                try {
                    options.inSampleSize = (int) sampleSize;
                    bitMapImage = BitmapFactory.decodeFile(filePath, options);
                    break;
                } catch (Exception ex) {
                    try {
                        sampleSize = sampleSize * 2;
                    } catch (Exception ex1) {

                    }
                }
            }
        } catch (Exception ex) {

        }
        return bitMapImage;
    }

    public static List<Integer> toIntegerList (int[] list){
        List<Integer> integerList = new ArrayList<>();
        if( (list != null) && (list.length > 0) ) {
            int size = list.length;
            for (int i = 0; i < size; i++) {
                integerList.add(list[i]);
            }
        }

        return integerList;
    }

    public static int[] toIntArray(List<Integer> list){
        if( (list != null) && (list.size() > 0) ) {
            int[] ret = new int[list.size()];
            for (int i = 0; i < ret.length; i++)
                ret[i] = list.get(i);
            return ret;
        }
        else {
            return (new int[0]);
        }
    }
    public static int parseColorByString (String colorString){

        if(!colorString.startsWith("#")){
            colorString = "#" + colorString;
        }

        return Color.parseColor(colorString);
    }
    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static List<String> getStringsBetweenStrings(String text, String pattern1, String pattern2){

        List<String> list = new ArrayList<>();

        Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
        Matcher m = p.matcher(text);
        while (m.find()) {
            list.add(m.group(1));
        }

        return list;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                DebugLog(TAG, e);
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }
        else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    public static Drawable bitmapToDrawable(Context context, Bitmap bmp){
        return (new BitmapDrawable(context.getResources(), bmp));
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static int getCurrentLangId(){
        int id;

        switch (Locale.getDefault().toString()){
            default:
            case "en_US":
                return CONSTANTS.LANG_ID_ENGLISH_USA;

            case "pt_BR":
                return CONSTANTS.LANG_ID_PORTUGUESE_BR;
        }
    }

    //toDo verificar isso depois
//    public static String getSportBestTitle(Context context, String icon){
//        String str = context.getString(R.string.the_best_of_category);
////        switch (icon){
////            case "icones/1.png" :
////                str = context.getString(R.string.the_best_of_soccer);
////                break;
////            case "icones/2.png" :
////                str = context.getString(R.string.the_best_of_basket);
////                break;
////            case "icones/3.png" :
////                str = context.getString(R.string.the_best_of_athletism);
////                break;
////            case "icones/4.png" :
////                str = context.getString(R.string.the_best_of_swimming);
////                break;
////            case "icones/5.png" :
////                str = context.getString(R.string.the_best_of_f1);
////                break;
////            case "icones/6.png" :
////                str = context.getString(R.string.the_best_of_boxing);
////                break;
////            case "icones/7.png" :
////                str = context.getString(R.string.the_best_of_tennis);
////                break;
////        }
//
//        return str;
//    }

    public static boolean isFirstAccessSportList(Context context, boolean trigger){
        SecurePreferences prefs = new SecurePreferences(context, CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        boolean isFirstAccess = ("true").matches(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_SPORTS_LIST, "true").toLowerCase());
        if(trigger) {
            prefs.put(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_SPORTS_LIST, "false");
        }
        return isFirstAccess;
    }

    public static boolean isFirstAccessPlayoffs(Context context, boolean trigger){
        SecurePreferences prefs = new SecurePreferences(context, CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        boolean isFirstAccess = ("true").matches(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_PLAYOFFS, "true").toLowerCase());
        if(trigger) {
            prefs.put(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_PLAYOFFS, "false");
        }
        return isFirstAccess;
    }

    public static boolean isFirstAccessMap(Context context, boolean trigger){
        SecurePreferences prefs = new SecurePreferences(context, CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        boolean isFirstAccess = ("true").matches(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_MAP, "true").toLowerCase());
        if(trigger) {
            prefs.put(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_MAP, "false");
        }
        return isFirstAccess;
    }

    public static boolean isFirstAccessDilemmas(Context context, boolean trigger){
        SecurePreferences prefs = new SecurePreferences(context, CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        boolean isFirstAccess = ("true").matches(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_DILEMMAS, "true").toLowerCase());
        if(trigger) {
            prefs.put(CONSTANTS.SHARED_PREFS_KEY_FIRST_ACCESS_DILEMMAS, "false");
        }
        return isFirstAccess;
    }

    public static String formatBirthDate(String birthDate){
        boolean success = false;
        String formatted = "";
        String[] unformatted = birthDate.split("/");
        if(unformatted.length == 3){
            if( (unformatted[0].length() == 2) &&
                    (unformatted[1].length() == 2) &&
                    (unformatted[2].length() == 4) ){

                formatted = unformatted[2] + "-" +
                        unformatted[1] + "-" +
                        unformatted[0];

                success = true;
            }
        }

        return (success ? formatted : "");
    }

    public static String formatDate (String date){
        SimpleDateFormat input  = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output  = new SimpleDateFormat("dd-MM-yyyy");

        Date datex = null;
        String mDate = date;

        try {
            datex = input.parse(date);
            mDate = output.format(datex);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }

    public static void BreakString(String TAG, String str, int division){

        DebugLog("", "==============================");
        String subStr = "";
        int length = str.length();
        for(int i = 0; i < division; i++){
            if(i == 0) {
                subStr = str.substring(i, (length / division));
            }
            else if(i == (division - 1)){
                subStr = str.substring((length / division) * (division - 1), length);
            }
            else{
                subStr = str.substring( (length / division) * i, (length / division) * (i + 1) );
            }
            DebugLog("", subStr);
        }
        DebugLog("", "==============================");
    }
    //---------------------

    /** Returns the consumer friendly device name */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            bitmap = mediaMetadataRetriever.getFrameAtTime(100000,MediaMetadataRetriever.OPTION_NEXT_SYNC);
        }
        catch (Exception e)
        {
            UTILS.DebugLog(TAG, e);
            throw new Throwable( "Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    //toDo verificar isto depois
    public static void ClearInformations(Context context){
        setUserLoggedIn(false);
//        CargoInformation.getCargos().clear();
//        DistribuidorInformation.getDistribuidores().clear();
//        FilterInformation.getProductsFilter().clear();
//        FilterInformation.getSegments().clear();
//        FilterInformation.getTypes().clear();
//        NewsInformation.getNewsHomeDataList().clear();
//        NewsInformation.getNewsDataList().clear();
//        NewsInformation.getNewsGroupDataList().clear();
//        RankInformation.ClearRankDataList();
        UserInformation.ClearUserData(context);
    }

    public static boolean isImage(String path){
        String extension = path.substring(path.indexOf('.')).toUpperCase();

        return (    (extension.contains("PNG"))     ||
                (extension.contains("JPG"))     ||
                (extension.contains("JPEG"))    ||
                (extension.contains("GIF"))     ||
                (extension.contains("BMP"))     ||
                (extension.contains("PSD"))     ||
                (extension.contains("ICO"))     ||
                (extension.contains("TGA"))     ||
                (extension.contains("TIF"))     ||
                (extension.contains("TIFF"))    ||
                (extension.contains("JIF"))     ||
                (extension.contains("JFIF"))    ||
                (extension.contains("JP2"))     ||
                (extension.contains("JPX"))     ||
                (extension.contains("J2K"))     ||
                (extension.contains("J2C"))     ||
                (extension.contains("FPX"))     ||
                (extension.contains("PCD"))     ||
                (extension.contains("EXIF"))    ||
                (extension.contains("BPG"))     ||
                (extension.contains("PPM"))     ||
                (extension.contains("PGM"))     ||
                (extension.contains("PBM"))     ||
                (extension.contains("PNM"))     );
    }

    public static boolean isValidAutologinJSON(JSONObject json){
        boolean isValid = false;

        try{
            isValid = ( ( (json.has("id")) && (json.get("id") != JSONObject.NULL) ) &&
                    ( (json.has("nome")) && (json.get("nome") != JSONObject.NULL) ) &&
                    ( (json.has("email")) && (json.get("email") != JSONObject.NULL) ) &&
                    ( (json.has("token")) && (json.get("token") != JSONObject.NULL) ) &&
//                    ( (json.has("phone")) && (json.get("phone") != JSONObject.NULL) ) &&
//                    ( (json.has("created_at")) && (json.get("created_at") != JSONObject.NULL) ) &&
                    ( (json.has("score")) && (json.get("score") != JSONObject.NULL) ) &&
//                    ( (json.has("acceptNews")) && (json.get("acceptNews") != JSONObject.NULL) ) &&
//                    ( (json.has("age")) && (json.get("age") != JSONObject.NULL) ) &&
                    ( (json.has("moedas")) && (json.get("moedas") != JSONObject.NULL) ) &&
                    ( (json.has("rank")) && (json.get("rank") != JSONObject.NULL) ) &&
                    ( (json.has("img")) && (json.get("img") != JSONObject.NULL) ) &&
//                    ( (json.has("clubId")) && (json.get("clubId") != JSONObject.NULL) ) &&
                    ( (json.has("birthDate")) && (json.get("birthDate") != JSONObject.NULL) ) &&
                    ( (json.has("idCitizenship")) && (json.get("idCitizenship") != JSONObject.NULL) ) &&
//                    ( (json.has("sexo")) && (json.get("sexo") != JSONObject.NULL) ) &&
                    ( (json.has("gcm")) && (json.get("gcm") != JSONObject.NULL) ) );
        }
        catch (JSONException e){
            UTILS.DebugLog(TAG, e);
            isValid = false;
        }

        return isValid;
    }

    public static int getScreenWidth(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getScreenHeight(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static float getWindowWidthPercentage(Activity activity, float percent) {
        WindowManager wm = activity.getWindowManager();
        return (percent * wm.getDefaultDisplay().getWidth());
    }

    public static String getFormattedUserName(String fullName){

        if(fullName.length() <= 0){
            UTILS.DebugLog(TAG, "Invalid name");
            return "";
        }

        String[] str = fullName.split(" ");

        if(str.length > 2){

            String middle = "";

            if( (str[1].matches("da")) || (str[1].matches("de")) || (str[1].matches("do")) ){
                middle = str[1];
            }
            else{
                middle += String.valueOf(str[1].charAt(0)).toUpperCase() + ". ";
            }

            return (str[0] + " " + middle + " " + str[str.length - 1]);
        }
        else{
            return fullName;
        }
    }

    public static String getCurrentAppVersionName(Context context){
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        try{
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            DebugLog(TAG, e);
        }

        return  ((packageInfo != null) ? packageInfo.versionName : "");
    }


    public static void copyFileFromAssets(Context context, String file, String dest) throws Exception
    {
        InputStream in = null;
        OutputStream fout = null;
        int count = 0;

        try
        {
            in = context.getAssets().open(file);
            fout = new FileOutputStream(new File(dest + "/" + file));

            byte data[] = new byte[1024];
            while ((count = in.read(data, 0, 1024)) != -1)
            {
                fout.write(data, 0, count);
            }
        }
        catch (Exception e)
        {
            UTILS.DebugLog(TAG, e);
        }
        finally
        {
            if (in != null)
            {
                try {
                    in.close();
                }
                catch (IOException e) {
                    UTILS.DebugLog(TAG, e);
                }
            }
            if (fout != null)
            {
                try {
                    fout.close();
                }
                catch (IOException e) {
                    UTILS.DebugLog(TAG, e);
                }
            }
        }
    }

    public static void setupUI(View view, final Activity activity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, activity);
            }
        }
    }

    public static String getHTTPResponseMessage(HttpURLConnection conn){
        String responseStr = "";

        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while (((line = br.readLine()) != null)) {
                responseStr += line;
            }
        } catch (IOException e) {
            DebugLog(TAG, e);
        }

        return responseStr;
    }

    public static String getVolleyErrorDataString(byte[] data){
        String str = Base64.encodeToString(data, Base64.DEFAULT);
        return str;
    }

    public static boolean toBool(int i){
        return (i == 1);
    }

    public static void hideSoftKeyboard(Activity activity) {

        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }


    public static boolean isValidNicknameCriteria(CharSequence nickname){
        return (nickname.length() >= 3);
    }

    public static boolean isValidBirthDate(CharSequence target) {
        boolean valid = true;

        if((target.length() != ("DD/MM/AAAA").length()) ||
                (target.toString().contains("D")) ||
                (target.toString().contains("M")) ||
                (target.toString().contains("Y")) ||
                (target.toString().contains("A")) ){
            valid = false;
        }
        return valid;
    }

    public static boolean isValidEmail(CharSequence target) {
        DebugLog(TAG, "Testing this: " + target.toString() + " Valid? " + ((!TextUtils.isEmpty(target)) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()));
        return ((!TextUtils.isEmpty(target)) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidCPF(CharSequence target) {
        String CPF = target.toString();
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
            return(false);

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            }
            else {
                dig10 = (char)(r + 48); // converte no respectivo caractere numerico
            }

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i = 0; i < 10; i++) {
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            }
            else {
                dig11 = (char)(r + 48);
            }

            // Verifica se os digitos calculados conferem com os digitos informados.
            return ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)));

        } catch (InputMismatchException erro) {
            return(false);
        }
    }

    public static boolean isValidSellerCode(CharSequence target) {
        return (true);
    }

    public static boolean isValidDDD(CharSequence target) {
        String str = target.toString();

        return ( str.matches("11") ||
                str.matches("12") ||
                str.matches("13") ||
                str.matches("14") ||
                str.matches("15") ||
                str.matches("16") ||
                str.matches("17") ||
                str.matches("18") ||
                str.matches("19") ||
                str.matches("22") ||
                str.matches("21") ||
                str.matches("24") ||
                str.matches("27") ||
                str.matches("28") ||
                str.matches("31") ||
                str.matches("32") ||
                str.matches("33") ||
                str.matches("34") ||
                str.matches("35") ||
                str.matches("37") ||
                str.matches("38") ||
                str.matches("41") ||
                str.matches("42") ||
                str.matches("43") ||
                str.matches("44") ||
                str.matches("45") ||
                str.matches("46") ||
                str.matches("47") ||
                str.matches("48") ||
                str.matches("49") ||
                str.matches("51") ||
                str.matches("53") ||
                str.matches("54") ||
                str.matches("55") ||
                str.matches("61") ||
                str.matches("62") ||
                str.matches("63") ||
                str.matches("64") ||
                str.matches("65") ||
                str.matches("66") ||
                str.matches("67") ||
                str.matches("68") ||
                str.matches("69") ||
                str.matches("71") ||
                str.matches("73") ||
                str.matches("74") ||
                str.matches("75") ||
                str.matches("77") ||
                str.matches("79") ||
                str.matches("81") ||
                str.matches("82") ||
                str.matches("83") ||
                str.matches("84") ||
                str.matches("85") ||
                str.matches("86") ||
                str.matches("87") ||
                str.matches("88") ||
                str.matches("89") ||
                str.matches("91") ||
                str.matches("92") ||
                str.matches("93") ||
                str.matches("94") ||
                str.matches("95") ||
                str.matches("96") ||
                str.matches("97") ||
                str.matches("98") ||
                str.matches("99") );
    }

    public static boolean isValidPhone(CharSequence target) {
        return (target.length() > 10);
//        String str = target.toString();
//        return ( (str.length() == 8) || ( (str.length() == 9) && (str.startsWith("9")) ) || ( ((str.length() == 11) || (str.length() == 10)) && (isValidDDD(str.substring(0, 2)))) );
    }

    public static boolean isValidPasswordCriteria(CharSequence pass){
        return ( (pass.length() >= 3) && (!pass.toString().contains(" ")) );
    }

    public static void setEditTextMaxLength(EditText et, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        et.setFilters(FilterArray);
    }

    public static int getProgressValue(float max, float target){
        return (int)((target * 100) / max);
    }

    public static boolean checkGooglePlayServiceAvailability(Context context) {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    public static String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        return String.valueOf(seconds);
    }

    public static String getCurrenteDateTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }

    public static File bitmapToFile(Context context, Bitmap bm){
        return bitmapToFile(context, bm, "");
    }

    public static File bitmapToFile(Context context, Bitmap bm, String filename){

        File f = new File(context.getCacheDir(), CONSTANTS.POST_ATTACHED_FILE_NAME + filename);
        try {
            if(f.exists()){
                f.delete();
            }
            f = new File(context.getCacheDir(), CONSTANTS.POST_ATTACHED_FILE_NAME + filename);
            f.createNewFile();
        } catch (IOException e) {
            DebugLog(TAG, e);
        }

        //Convert bitmap to byte array
        Bitmap bitmap = bm;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            return f;

        } catch (FileNotFoundException e) {
            DebugLog(TAG, e);
        } catch (IOException e) {
            DebugLog(TAG, e);
        }

        return null;
    }

    public static boolean isNetworkAvailable(Context context) {
        if(context == null){
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return ( (activeNetworkInfo != null) && (activeNetworkInfo.isConnected()) );
    }

    public static int getNetworkStatus(ConnectivityManager connectivityManager){
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return CONSTANTS.CONNECTION_TYPE_WIFI;
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return CONSTANTS.CONNECTION_TYPE_MOBILE;
        }
        return CONSTANTS.CONNECTION_TYPE_NONE;
    }

    public static int Clamp(int min, int max, int val){

        return ( (val < min) ? min : ((val > max) ? max : val) );
    }

    public static float Clamp(float min, float max, float val){

        return ( (val < min) ? min : ((val > max) ? max : val) );
    }

    public static double Clamp(double min, double max, double val){

        return ( (val < min) ? min : ((val > max) ? max : val) );
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static int getHeight(Context context, String text, int textSize, int deviceWidth) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public static String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos = new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static int getBirthByDate (String date){
        int birthDate = Integer.parseInt(date.replace("-", ""));
        final Date calendarDate = Calendar.getInstance().getTime();
        String full = new SimpleDateFormat("yyyyMMdd").format(calendarDate);

        int birth  = Integer.parseInt(full) - birthDate;

        return birth/10000;
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }
        else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static float getDPI(Context context){
        return context.getResources().getDisplayMetrics().density;
    }

    public static String getDPIName(Context context){
        String dpi = String.valueOf(context.getResources().getDisplayMetrics().density);

        switch (dpi) {

            case "0.75":
                return "ldpi";

            case "1.0":
                return "mdpi";

            case "1.5":
                return "hdpi";

            case "2.0":
                return "xhdpi";

            case "3.0":
                return "xxhdpi";

            case "4.0":
                return "xxxhdpi";

            default:
                return "???";
        }
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog(TAG, "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId) {
            case OP_GET_VIDEO_IMAGE: {
                isDownloadingVideoImage = false;
                boolean success = false;

                try {
                    if ((response.has("Object")) && (response.get("Object") != JSONObject.NULL)) {

                    }
                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                }

                if ((success)) {
                } else {
                }
            }
            break;
        }

    }


    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_VIDEO_IMAGE:{
                isDownloadingVideoImage = false;

            }
            break;
        }
    }

    public static String getPlayerFlag (int flag){
        return "🇦🇴";
    }


    public static int getChanelD (int id){
        switch (id){
            case 1 : return R.drawable.icon_broadcast_chanel_1; //Fox Sports 2
            case 2 : return R.drawable.icon_broadcast_chanel_2; //Fox Sports
            case 3 : return R.drawable.icon_broadcast_chanel_3; //Globo
            case 4 : return R.drawable.icon_broadcast_chanel_4; //Esporte Interativo
            case 5 : return R.drawable.icon_broadcast_chanel_5; //Esporte Interativo 2
            case 6 : return R.drawable.icon_broadcast_chanel_6; //Ei Plus
            case 7 : return R.drawable.icon_broadcast_chanel_7; //Espn Brasil
            case 8 : return R.drawable.icon_broadcast_chanel_8; //Espn 2
            case 9 : return R.drawable.icon_broadcast_chanel_9; //Espn
            case 10 : return R.drawable.icon_broadcast_chanel_10; //WatchEspn
            case 11 : return R.drawable.icon_broadcast_chanel_11; //Space
            case 12 : return R.drawable.icon_broadcast_chanel_12; //Sportv
            case 13 : return R.drawable.icon_broadcast_chanel_13; //Sportv 2
            case 14 : return R.drawable.icon_broadcast_chanel_14; //Sportv 3
            case 15 : return R.drawable.icon_broadcast_chanel_15; //Premiere
            case 16 : return R.drawable.icon_broadcast_chanel_16; //Globoesporte
            case 17 : return R.drawable.icon_broadcast_chanel_17; //Band
            case 18 : return R.drawable.icon_broadcast_chanel_18; //BandSports
            case 19 : return R.drawable.icon_broadcast_chanel_19; //RedeTV!
            case 20 : return R.drawable.icon_broadcast_chanel_20; //ESPN Extra
            case 21 : return R.drawable.icon_broadcast_chanel_21; //SBT
            case 22 : return R.drawable.icon_broadcast_chanel_22; //TNT
            case 23 : return R.drawable.icon_broadcast_chanel_23; //CBF TV
            case 24 : return R.drawable.icon_broadcast_chanel_24; //FPF TV
            case 25 : return R.drawable.icon_broadcast_chanel_25; //Rai
            case 26 : return R.drawable.icon_broadcast_chanel_26; //Rede Vida
            case 27 : return R.drawable.icon_broadcast_chanel_27; //FOX Premium
            case 28 : return R.drawable.icon_broadcast_chanel_28; //DAZN erro
            case 29 : return R.drawable.icon_broadcast_chanel_29; //SPFCTV
            case 30 : return R.drawable.icon_broadcast_chanel_30; //Facebook
            case 31 : return R.drawable.icon_broadcast_chanel_31; //Twitter
            case 32 : return R.drawable.icon_broadcast_chanel_33; //MyCujoo
            case 33 : return R.drawable.icon_broadcast_chanel_34;
        }
        return -1;
    }

    //[ServerData{id=12, prefix='https://containergeral.com.br/FEC/', selected=0}, ServerData{id=19, prefix='https://geralwarehouse.com.br/FEC/', selected=0}, ServerData{id=20, prefix='https://warehousegeral.com.br/FEC/', selected=1}]

    public static void changeDrawableColor (TextView view, Activity activity, int id){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setCompoundDrawableTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, id)));
        }else{
            Drawable drawable = view.getCompoundDrawables()[0];
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, activity.getResources().getColor(id));
        }
    }

    public static void getImageAt(Context activity, String link, ImageView imageView){
        try{
            if(activity != null){
                Glide.with(activity).load(UTILS.getHtmlImage(link))
                        .into(imageView);
            }
        }catch (Exception e){

        }
    }

    public static void getImageAt(Context activity, int link, ImageView imageView){
        try{
            if(activity != null){
                Glide.with(activity).load(link)
                        .into(imageView);
            }
        }catch (Exception e){

        }
    }

    public static void getImageAt(Context activity, String link, ImageView imageView, RequestOptions options){
        try{
            if(activity != null){
                Glide.with(activity).load(UTILS.getHtmlImage(link))
                        .apply(options)
                        .into(imageView);
            }
        }catch (Exception e){

        }
    }

    public static String getHtmlImage (String link){
        String finalLink = "";

        if(link != null){
            finalLink = getRandomURL() + "Content/images/" + link;

            if(!link.isEmpty()){
                if(link.contains("http") || link.contains("https")){
                    finalLink = link;
                }
            }
        }
        return finalLink;
    }


    public static String getHtmlContent (String link){
        String finalLink = "";

        if(link != null){
            finalLink = getRandomURL() + "Content/" + link;

            if(!link.isEmpty()){
                if(link.contains("http") || link.contains("https")){
                    finalLink = link;
                }
            }
        }

        return finalLink;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap flip(Bitmap src, int type) {
        // criar new matrix para transformacao
        Matrix matrix = new Matrix();
        // if vertical
        if(type == FLIP_VERTICAL) {
            // y = y * -1
            matrix.preScale(1.0f, -1.0f);
        }
        // if horizonal
        else if(type == FLIP_HORIZONTAL) {
            // x = x * -1
            matrix.preScale(-1.0f, 1.0f);
            // unknown type
        } else {
            return null;
        }

        // return image transformada
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public static String getDatePost (String date){
        date = formatDateFromDB(date);
        String[] dateParts = date.split("/");
        String finalValue = "";
        finalValue = dateParts[0] + " "+ getActualMouth(dateParts[1]) + " de " + dateParts[2];
        return finalValue;
    }
    public static String getActualMouth (String mouth){
        switch (mouth){
            case "01":
                return "Janeiro";
            case "02":
                return "Fevereiro";
            case "03":
                return "Março";
            case "04":
                return "Abril";
            case "05":
                return "Maio";
            case "06":
                return "Junho";
            case "07":
                return "Julho";
            case "08":
                return "Agosto";
            case "09":
                return "Setembro";
            case "10":
                return "Outubro";
            case "11":
                return "Novembro";
            case "12":
                return "Dezembro";
            default: return "";
        }
    }

    //Converte um valor float em valor monetário
    public static String convertMonetaryValue (float valor, String linguagem, String pais){

        Locale locale = new Locale(linguagem, pais);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        return currencyFormatter.format(valor);
    }

    public static String getRandomURL (){
        List<ServerData> list = UserInformation.getServerData().getMedia();
        Collections.shuffle(list);
        return list.get(0).getPrefix();
    }

    public static String getApiServer (int id){
        boolean hasServer = false;
        ServerData server = new ServerData();
        for(ServerData d : UserInformation.getServerData().getApi()){
            if(d.getId() == id){
                UserInformation.getServerData().setServerId(d.getId());
                salvarServidor(UserInformation.getServerData());
                hasServer = true;
                server = d;
                break;
            }
        }

        if (hasServer){
            return server.getPrefix();
        }

        return CONSTANTS.serverURL;
    }

    public static String getApiServerSelected (){
        boolean hasServer = false;
        ServerData server = new ServerData();
        for(ServerData d : UserInformation.getServerData().getApi()){
            if(d.getSelected() == 1){
                UserInformation.getServerData().setServerId(d.getId());
                salvarServidor(UserInformation.getServerData());
                hasServer = true;
                server = d;
                break;
            }
        }

        if (hasServer){
            return server.getPrefix();
        }else{
            UserInformation.getServerData().getApi().get(0).setSelected(1);
            UserInformation.getServerData().setServerId(UserInformation.getServerData().getApi().get(0).getId());
            salvarServidor(UserInformation.getServerData());
            return UserInformation.getServerData().getApi().get(0).getPrefix();
        }
    }

    public static void salvarServidor (ServerCustomData data){
        Gson gson = new Gson();
        prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_SERVER);
        prefs.put(CONSTANTS.SHARED_PREFS_KEY_SERVER, gson.toJson(data));
    }
}

