package utils;

import android.util.Log;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LoadEmbed {

    public static String loadHtmlEmbed(String embed) {
        String fullHtml = embed;
        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<!DOCTYPE html>");
            stringBuilder.append("<html>");
            stringBuilder.append("<head> <meta name=\"viewport\" content=\"width=100, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no\" /> </head>");
            stringBuilder.append("<body>");
            stringBuilder.append("<style> span { display: block; max-width: 100%; height: auto; } </style>");

            List<String> images = UTILS.getStringsBetweenStrings(embed, "<img src=\"", "\"");
            int size = images.size();
            String replacement = "%s\" onclick=\"AndroidFunction.expand('%s')";
            for(int i = 0; i < size; i++){
                String img = images.get(i).replace("../../Content/images/", UTILS.getRandomURL());
                embed = embed.replaceFirst(images.get(i), String.format(replacement, img, img));
            }

            stringBuilder.append(embed);
            stringBuilder.append("\n<script language=\"javascript\">\n" +
                    "function expand(str) {\n" +
                    "    AndroidFunction.expand(str);\n" +
                    "}\n" +
                    "</script>");


            stringBuilder.append("</body>");
            stringBuilder.append("</html>");


            fullHtml = stringBuilder.toString();
        }
        catch (Exception e){
            UTILS.DebugLog("Error", e);
        }

        return fullHtml;
    }
}
