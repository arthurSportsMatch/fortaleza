package utils;

import android.app.Activity;
import android.app.Dialog;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.sportm.fortaleza.R;

import java.util.Hashtable;

import abstractions.AbstractDialog;

/**
 * Created by rafael.verginelli on 6/20/16.
 */
public class DialogDisplayer extends AbstractDialog {

    @Override
    public void ShowDialog(Activity activity, int resource, @NonNull Hashtable<String, Object> params) {

        DismissDialog();

        if(activity.isFinishing()){
            return;
        }

        if( (params.containsKey(PARAMS_FULLSCREEN)) && (((boolean)params.get(PARAMS_FULLSCREEN))) ){
            dialog = new Dialog(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        }
        else{
            dialog = new Dialog(activity);

        }

        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), R.color.colorFullTransparent));
        dialog.setContentView(resource);

//        View root = dialog.findViewById(R.id.root);
//        if(root != null){
//            root.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    DismissDialog();
//                }
//            });
//        }

        //set parameters
        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(activity.getApplicationContext(), params.containsKey(PARAMS_BACKGROUND_COLOR) ? (int) params.get(PARAMS_BACKGROUND_COLOR) : R.color.colorFullTransparent));
//        dialog.setCancelable(params.containsKey(PARAMS_CANCELABLE) ? (boolean)params.get(PARAMS_CANCELABLE) : false);
        dialog.setCancelable(false);

        dialog.show();
    }

    @Override
    public void DismissDialog() {
        if(dialog != null){
            dialog.dismiss();
//            dialog = null;
        }
    }

    @Override
    public Dialog getDialog() {
        return dialog;
    }
}
