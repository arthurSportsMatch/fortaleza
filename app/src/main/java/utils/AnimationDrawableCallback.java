package utils;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;

public abstract class AnimationDrawableCallback implements AnimationDrawable.Callback {

    private Drawable mLastFrame;
    private AnimationDrawable.Callback mWrappedCallback;
    private boolean mIsCallbackTriggered = false;

    public AnimationDrawableCallback(AnimationDrawable animationDrawable, AnimationDrawable.Callback callback) {
        mLastFrame = animationDrawable.getFrame(animationDrawable.getNumberOfFrames() - 1);
        mWrappedCallback = callback;
    }

    @Override
    public void invalidateDrawable(Drawable who) {
        if (mWrappedCallback != null) {
            mWrappedCallback.invalidateDrawable(who);
        }

        if (!mIsCallbackTriggered && mLastFrame != null && mLastFrame.equals(who.getCurrent())) {
            mIsCallbackTriggered = true;
            onAnimationComplete();
        }
    }

    @Override
    public void scheduleDrawable(Drawable who, Runnable what, long when) {
        if (mWrappedCallback != null) {
            mWrappedCallback.scheduleDrawable(who, what, when);
        }
    }

    @Override
    public void unscheduleDrawable(Drawable who, Runnable what) {
        if (mWrappedCallback != null) {
            mWrappedCallback.unscheduleDrawable(who, what);
        }
    }
    public abstract void onAnimationComplete();
}
