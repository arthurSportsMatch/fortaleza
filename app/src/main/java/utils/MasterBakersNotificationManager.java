package utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.SplashActivity;
import com.sportm.fortaleza.StartMenuActivity;
import com.sportm.fortaleza.WinBadgeActivity;

import java.util.Hashtable;
import java.util.Random;

import informations.UserInformation;

import static com.sportm.fortaleza.MainActivity.prefs;

public class MasterBakersNotificationManager {

    public static final int PUSH_ID_DEFAUT                  = 0;
    public static final int PUSH_ID_CONTROLEVERSAO          = 1000;
    public static final int PUSH_ID_FORCECRASH              = 1001;
    public static final int PUSH_ID_FORCELOGOUT             = 1002;
    public static final int PUSH_ID_BADGE                   = 1003; //v
    public static final int PUSH_ID_NOTICIA                 = 1; //v
    public static final int PUSH_ID_QUIZ                    = 2; //v //ok
    public static final int PUSH_ID_PESQUISA                = 3;
    public static final int PUSH_ID_PALPITES                = 4;
    public static final int PUSH_ID_PLAYERPROFILE           = 5; //v
    public static final int PUSH_ID_MEMBERSHIP              = 6;
    public static final int PUSH_ID_AGENDA                  = 7;
    public static final int PUSH_ID_CAMPINHO                = 8;  //v(id Grupo)
    public static final int PUSH_ID_LOJA                    = 9;
    public static final int PUSH_ID_PESQUISA_ESPECIFICA     = 10; //v(id pesquisa)
    public static final int PUSH_ID_AUDIO                   = 11; //v(id audio)
    public static final int PUSH_ID_DETALHES_JOGO           = 12; //v(id jogo) // toDo
    public static final int PUSH_ID_PRODUTO                 = 13; //v(id produto) //toDO
    public static final int PUSH_ID_ALTERAR_DADOS           = 14; //ok
    public static final int PUSH_ID_RANK                    = 15; //ok
    public static final int PUSH_ID_AGENDA_JOGO             = 16; //v (id do jogo)
    public static final int PUSH_ID_ANIVERSARIO             = 17; //não faz nada

    public static final String NOTIFICATION_KEY_CONTENT_TEXT    = "contentText";
    public static final String NOTIFICATION_KEY_TITTLE_TEXT    = "contentTittle";

    int notificationId = 003;

    private Context context;

    private NotificationManager notificationManager;
    private Notification.Builder builder;

    private static final String TAG = "MBNotification";

    public static String gcm = "";

    //Contructor do manager
    public MasterBakersNotificationManager(Context context){
        this.context = context;

        builder = new Notification.Builder(this.context);
        notificationManager = (NotificationManager)this.context.getSystemService(Context.NOTIFICATION_SERVICE);
        SetNotificationChanel(notificationManager);
    }

    //Necessario para que seja encaminhado as notificações
    private void SetNotificationChanel (NotificationManager notific){
        String channelId = "SPFC";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel followerChannel = new NotificationChannel(channelId, "notification", NotificationManager.IMPORTANCE_DEFAULT);
            followerChannel.setLightColor(R.color.bg_gray);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notific.createNotificationChannel(followerChannel);
            builder.setChannelId(channelId);
        }
    }

    //Cria as notificações
    public void CreateNotification(int idPush, int pushType, String tittle, String message, Bundle b) {
        Hashtable<String, Object> alertParams = new Hashtable<>();
        if(!message.isEmpty())
            alertParams.put(NOTIFICATION_KEY_CONTENT_TEXT, message);
        if(!tittle.isEmpty())
                alertParams.put(NOTIFICATION_KEY_TITTLE_TEXT, tittle);

        if(pushType > 999){
            CallSilentNotification(idPush,pushType, b);
        }else{
            SetNotificationParams(idPush, pushType, alertParams, b);
        }

    }

    private void SetDefaultNotificationParams(Bundle b){
        builder.setSmallIcon(R.drawable.ic_logo_vazado);

        String tittle = !b.getString("nTittle").isEmpty()? b.getString("nTittle"): context.getString(R.string.app_name);
        String body = !b.getString("nBody").isEmpty() ? b.getString("nBody"): context.getString(R.string.push_description);

        builder.setAutoCancel(true);
        //builder.setLights(Color.WHITE, 2000, 2000);
        //builder.setVibrate(new long[] { 250, 250, 250, 250 });
        //builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setContentTitle(tittle);
        builder.setContentText(body);
        builder.setPriority(Notification.PRIORITY_MAX);

        Intent resultIntent = new Intent(context, MainMenuActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainMenuActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
    }

    private void SetNotificationParams(int idPush, int pushType, Hashtable<String, Object> alertParams, Bundle b){
        SetDefaultNotificationParams(b);

        if(alertParams.containsKey(NOTIFICATION_KEY_CONTENT_TEXT) && !String.valueOf(alertParams.get(NOTIFICATION_KEY_CONTENT_TEXT)).isEmpty()){
            builder.setContentText(String.valueOf(alertParams.get(NOTIFICATION_KEY_CONTENT_TEXT)));

            CharSequence cs = (CharSequence) String.valueOf(alertParams.get(NOTIFICATION_KEY_CONTENT_TEXT));
            Notification.BigTextStyle style = new Notification.BigTextStyle();
            style.bigText(cs);
            builder.setStyle(style);
        }

        if(alertParams.containsKey(NOTIFICATION_KEY_TITTLE_TEXT) && !String.valueOf(alertParams.get(NOTIFICATION_KEY_TITTLE_TEXT)).isEmpty()){
            builder.setContentTitle(String.valueOf(alertParams.get(NOTIFICATION_KEY_TITTLE_TEXT)));
        }

        if(pushType == PUSH_ID_DEFAUT){
            CallNotificationGeneric(idPush);
        }
        else {
            CallNotification(idPush, pushType, b);
        }
    }

    private void CallNotification(int idPush, int pushType, Bundle b){
        UTILS.DebugLog(TAG, "Creating news notification");

        Bundle pushBundle = new Bundle();

        pushBundle.putInt("t", pushType);
        pushBundle.putInt("idPush", idPush);

        switch (pushType){

            case PUSH_ID_NOTICIA:
                pushBundle.putInt("id", b.getInt("v", -1));
                break;

            case PUSH_ID_AUDIO:
                pushBundle.putInt("id", b.getInt("v", 0));
                break;

            case PUSH_ID_QUIZ:
                pushBundle.putInt("idQuiz", b.getInt("v", -1));
                break;

            case PUSH_ID_PESQUISA_ESPECIFICA:
                pushBundle.putInt("idPesquisa", b.getInt("v", -1));
                break;

            case PUSH_ID_PLAYERPROFILE:
                pushBundle.putInt("idPlayer", b.getInt("v", 0));
                break;

            case PUSH_ID_CAMPINHO:
                pushBundle.putInt("escalacao", b.getInt("v", -1));
                break;

            case PUSH_ID_DETALHES_JOGO:
                pushBundle.putInt("idJogo", b.getInt("v", -1));
                break;

            case PUSH_ID_PRODUTO:
                pushBundle.putInt("idProduto", b.getInt("v", -1));
                break;

            case PUSH_ID_AGENDA_JOGO:
                pushBundle.putInt("idAgenda", b.getInt("v", -1));
                break;
        }


        Intent resultIntent =  null;

        //Se esta logado
        if(UTILS.isUserLoggedIn()) {
            resultIntent = new Intent(context, StartMenuActivity.class);
                Bundle _b = new Bundle();
                _b.putBundle("pushBundle", pushBundle);
                _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
            resultIntent.putExtras(_b);
        }
        //Se não esta logado
        else{
            resultIntent = new Intent(context, SplashActivity.class);
            resultIntent.putExtras(pushBundle);
        }

        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(StartMenuActivity.class);
        stackBuilder.addNextIntentWithParentStack(resultIntent);

        PendingIntent resultPendingIntent =  stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);//PendingIntent.getActivity(context, (int) System.currentTimeMillis(), resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        Random rand = new Random();
        notificationId = rand.nextInt();
        notificationManager.notify(notificationId, builder.build());
    }

    public void CallSilentNotification(int idPush, int pushType, Bundle b){
        UTILS.DebugLog(TAG, "Creating news notification");

        Bundle pushBundle = new Bundle();
        pushBundle.putInt("t", pushType);
        pushBundle.putInt("idPush", idPush);
        Intent resultIntent =  null;

        switch (idPush){
            case PUSH_ID_CONTROLEVERSAO:{
                resultIntent = new Intent(context, SplashActivity.class);
            }
            break;
            case PUSH_ID_FORCECRASH:{
                Crashlytics.getInstance().crash();
            }
            break;
            case PUSH_ID_FORCELOGOUT:{
                if(UTILS.isUserLoggedIn()) {
                    UserInformation.setSessionId(0);
                    prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_SERVER);
                    UTILS.ClearInformations(context);
                    resultIntent = new Intent(context, SplashActivity.class);
                }
            }
            break;
            case PUSH_ID_BADGE :{
                //Se esta logado
                if(UTILS.isUserLoggedIn()) {
                    resultIntent = new Intent(context, WinBadgeActivity.class);
                    Bundle _b = new Bundle();
                    _b.putBundle("pushBundle", pushBundle);
                    resultIntent.putExtras(_b);
                }
            }
            break;
        }

        if(resultIntent != null){
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(resultIntent);
        }
    }

    private void CallNotificationGeneric(int idPush){
        UTILS.DebugLog(TAG, "Creating generic notification");

        Intent resultIntent =  null;

        Bundle pushBundle = new Bundle();
        pushBundle.putInt("t", PUSH_ID_DEFAUT);
        pushBundle.putInt("idPush", idPush);

        if( (UTILS.isUserLoggedIn()) ) {
            MainMenuActivity.setSaveTrackingOfCommonPush(true);
            resultIntent = new Intent(context, MainMenuActivity.class);
        }
        else{
            resultIntent = new Intent(context, SplashActivity.class);
        }

        resultIntent.putExtras(pushBundle);

        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainMenuActivity.class);
        stackBuilder.addNextIntentWithParentStack(resultIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        Random rand = new Random();
        notificationId = rand.nextInt();
        notificationManager.notify(notificationId, builder.build());
    }
}
