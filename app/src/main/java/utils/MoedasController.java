package utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import informations.UserInformation;
import models.user.UserModel;
import repository.user.UserRepository;

import static com.sportm.fortaleza.MainActivity.prefs;

/*
    Controlador de moedas ganhas
    Ele apresenta uma view quando o usuario ganhar uma moeda
 */
public class MoedasController {

    public MoedasController (){

    }

    static public void showNewMoedas (final Activity activity, final LinearLayout mainView, final int finalValue){
        final UserRepository repository = new UserRepository(activity, prefs);
        repository.getSaved(new UserRepository.AsyncResponse() {
            @Override
            public void onResponseSucces(UserModel data) {
                showCanvas(activity, data, mainView, finalValue);
                int coin = data.getMoedas() + finalValue;
                saveUserCoin(repository, data, coin);
            }

            @Override
            public void onResponseMessage(String message) {

            }

            @Override
            public void onResponseError(String message) {

            }
        });
    }

    private static void showCanvas (Activity activity, UserModel user, LinearLayout mainView, int finalValue){
        if(mainView.getChildCount() > 0){
            mainView.removeAllViews();
        }

        //Temp values
        int actualCoin = user.getMoedas();
        finalValue = finalValue + actualCoin;

        //Get Views
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_moedas_popup, null, false);
        ImageView img = view.findViewById(R.id.img_user);
        TextView nome = view.findViewById(R.id.txt_nome);
        final TextView moedas = view.findViewById(R.id.txt_moedas);
        View moedasView = view.findViewById(R.id.view_moedas);

        //Update data
        UTILS.getImageAt(activity, user.getImg(), img,new RequestOptions()
                .placeholder(R.drawable.ic_placeholderuser)
                .error(R.drawable.ic_placeholderuser)
                .circleCrop());

        nome.setText(user.getNome());
        moedas.setText(String.valueOf(actualCoin));

        //Animations
        final Animation goDown = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_down);
        final Animation popUp = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.pop_delayed);
        final Animation goUp = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_up);
        view.setAnimation(goDown);
        view.startAnimation(goDown);

        //Final temp values
        final View finalMoedas = moedasView;
        final View finalView = view;
        final int finalCoin = finalValue;

        //Animação de ir para baixo
        goDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finalMoedas.setAnimation(popUp);
                finalMoedas.startAnimation(popUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        //Animação de ir para cima
        popUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                moedas.setText(String.valueOf(finalCoin));
                goUp.setStartOffset(1000);
                finalView.setAnimation(goUp);
                finalView.startAnimation(goUp);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //Adiciono a view
        mainView.addView(view);
    }

    private static void saveUserCoin (UserRepository repository, UserModel model, int coint){
        model.setMoedas(coint);
        repository.saveUser(model, false);
    }
}
