package abstractions;

import android.app.Activity;
import android.app.Dialog;

import java.util.Hashtable;

/**
 * Created by rafael.verginelli on 6/20/16.
 */
public abstract class AbstractDialog {

    public static final String PARAMS_FULLSCREEN = "fullscreen";
    public static final String PARAMS_CANCELABLE = "cancelable";
    public static final String PARAMS_BACKGROUND_COLOR = "backgroundColor";

    protected Dialog dialog = null;

    public abstract void ShowDialog(Activity activity, int resource, Hashtable<String, Object> params);
    public abstract void DismissDialog();
    public abstract Dialog getDialog();

}