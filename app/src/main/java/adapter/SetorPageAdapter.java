package adapter;

import android.content.Context;

import com.sportm.fortaleza.R;

import java.util.List;

import data.CheckinSetoresData;

public class SetorPageAdapter extends EstadioPageAdapter {


    public SetorPageAdapter(Context context, List<CheckinSetoresData> data) {
        super(context, data);
    }

    @Override
    int getImage(String setor) {
        int color = R.drawable.checkin_section_group_101;
        switch (setor.toLowerCase()){
            case "101" : color = R.drawable.checkin_section_group_101;
                break;
            case "102" : color = R.drawable.checkin_section_group_102;
                break;
            case "103" : color = R.drawable.checkin_section_group_103;
                break;
            case "104" :color = R.drawable.checkin_section_group_104;
                break;
            case "105" : color = R.drawable.checkin_section_group_105;
                break;
            case "106" : color = R.drawable.checkin_section_group_106;
                break;
            case "107" : color = R.drawable.checkin_section_group_107;
                break;
        }
        return color;
    }
}
