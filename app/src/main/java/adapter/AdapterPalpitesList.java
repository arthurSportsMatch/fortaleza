package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PalpiteData;
import utils.CONSTANTS;
import utils.UTILS;

public class AdapterPalpitesList extends RecyclerView.Adapter<AdapterPalpitesList.ViewHolder> {

    Context context;
    List<PalpiteData> data;
    Listener listener;

    public AdapterPalpitesList(Context context, List<PalpiteData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    public void ChangeList (List<PalpiteData> nData){
        data.addAll(nData);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_palpites_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position <= data.size()-1){
            listener.OnLast();
        }

        final PalpiteData palpite = data.get(position);

        if(palpite.getCampeonato() != null){
            holder.tittle.setText(palpite.getCampeonato());
        }

        if(palpite.getRodada() != null){
            holder.rodada.setText(palpite.getRodada());
        }

        //Penalte
        if(palpite.getPenC1() != null){
            holder.versus.setVisibility(View.GONE);
            holder.empate.setVisibility(View.VISIBLE);
            String value = palpite.getPenC1();

            if(palpite.getPenC2() != null){
                value = value + "x" + palpite.getPenC2();
            }

            holder.empate.setText("(" + value + ")");
        }

        //Clube 1
        if(palpite.getTime1() != null){
            holder.time1.setText(palpite.getTime1());
        }

        if(palpite.getTime1img() != null){
            UTILS.getImageAt(context,palpite.getTime1img(), holder.imgTime1);
        }

        if(palpite.getGolsClube1() != null){
            holder.gols1.setText(palpite.getGolsClube1());
        }

        //Clube 2
        if(palpite.getTime2() != null){
            holder.time2.setText(palpite.getTime2());
        }

        if(palpite.getTime2img() != null){
            UTILS.getImageAt(context,palpite.getTime2img(), holder.imgTime2);
        }

        if(palpite.getGolsClube2() != null){
            holder.gols2.setText(palpite.getGolsClube2());
        }

        if(palpite.getAcertos() != null){
            holder.pontos.setText(palpite.getAcertos());
        }else{
            holder.pontos.setText("0");
        }

        if(palpite.getMoedas() != null){

            String moedas = "+" + palpite.getMoedas();
            if(moedas.equals("0")){
                moedas = "0";
            }

            holder.moedas.setText(moedas);
        }else{
            holder.moedas.setText("0");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(palpite);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener{
        void OnClick(PalpiteData data);
        void OnLast ();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tittle;
        TextView rodada;

        TextView pontos;
        TextView moedas;

        TextView empate;
        View versus;

        ImageView imgTime1;
        TextView time1;
        TextView gols1;

        ImageView imgTime2;
        TextView time2;
        TextView gols2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tittle = itemView.findViewById(R.id.txt_tittle);
            rodada = itemView.findViewById(R.id.txt_rodada);

            pontos = itemView.findViewById(R.id.txt_acertos);
            moedas = itemView.findViewById(R.id.txt_moedas);

            empate = itemView.findViewById(R.id.txt_empate);
            versus = itemView.findViewById(R.id.txt_versus);

            imgTime1 = itemView.findViewById(R.id.img_time_1);
            time1 = itemView.findViewById(R.id.txt_time_1);
            gols1 = itemView.findViewById(R.id.txt_result_1);

            imgTime2 = itemView.findViewById(R.id.img_time_2);
            time2 = itemView.findViewById(R.id.txt_time_2);
            gols2 = itemView.findViewById(R.id.txt_result_2);
        }
    }
}
