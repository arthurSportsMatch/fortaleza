package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.R;

import java.util.List;

import data.VoucherItemData;

public class ResgatadosAdapter extends RecyclerView.Adapter<ResgatadosAdapter.ViewHolder> {

    List<VoucherItemData> itens;
    Context context;

    public ResgatadosAdapter(List<VoucherItemData> itens, Context context) {
        this.itens = itens;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_itens_resgatados, parent, false);
        ViewHolder holder  = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VoucherItemData data = itens.get(position);

        if(data.getTitle() != null)
            holder.title.setText(data.getTitle());

        if(data.getSubtitle() != null)
            holder.subtitle.setText(data.getSubtitle());


        holder.qnt.setText(String.valueOf(data.getQtd()));

    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView title, subtitle, qnt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            qnt = itemView.findViewById(R.id.txt_resgatado_qnt);
            subtitle = itemView.findViewById(R.id.txt_resgatado_subtitle);
            title = itemView.findViewById(R.id.txt_resgatado_tittle);
        }
    }
}
