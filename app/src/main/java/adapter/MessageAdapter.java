package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.List;

import data.ChatMessageData;
import utils.CONSTANTS;
import utils.UTILS;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<ChatMessageData> messages;
    private Context context;

    public List<ChatMessageData> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatMessageData> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessages (List<ChatMessageData> messages){
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

    public void addMessages (ChatMessageData message){
        this.messages.add(message);
        notifyDataSetChanged();
    }

    public MessageAdapter(List<ChatMessageData> messages, Context context) {
        this.messages = new ArrayList<>(messages);
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_conversation, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatMessageData message = messages.get(position);

        if(message.getAdm() == 0){
            holder.myMessage.setVisibility(View.VISIBLE);

            //Pega a mensagem
            if(message.getTexto() != null){
                holder.content.setText(message.getTexto());
            }else{
                holder.content.setVisibility(View.GONE);
            }

            //Pega a data da mensagem
            holder.time.setText(UTILS.getFormattedDate(message.getData()));

            //Pega a imagem
            if(!message.getImg().isEmpty()){
                UTILS.getImageAt(context, message.getImg(), holder.image);
                holder.image.setVisibility(View.VISIBLE);
            }else{
                holder.image.setVisibility(View.GONE);
            }

        }else{
            holder.admMessage.setVisibility(View.VISIBLE);

            //Pega a mensagem
            if(message.getTexto() != null){
                holder.contentAdm.setText(message.getTexto());
            }else{
                holder.contentAdm.setVisibility(View.GONE);
            }

            //Pega a data da mensagem
            holder.timeAdm.setText(UTILS.getFormattedDate(message.getData()));

            //Pega a imagem
            if(!message.getImg().isEmpty()){
                holder.imageAdm.setVisibility(View.VISIBLE);

                UTILS.getImageAt(context, message.getImg(), holder.imageAdm);
            }else{
                holder.imageAdm.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        View myMessage;
        ImageView image;
        TextView content;
        TextView time;

        View admMessage;
        ImageView imageAdm;
        TextView contentAdm;
        TextView timeAdm;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            myMessage = itemView.findViewById(R.id.message_holder_my);
            image = itemView.findViewById(R.id.img_coment);
            content = itemView.findViewById(R.id.txt_coment);
            time = itemView.findViewById(R.id.txt_coment_time);

            admMessage = itemView.findViewById(R.id.message_holder_adm);
            imageAdm = itemView.findViewById(R.id.img_coment_adm);
            contentAdm = itemView.findViewById(R.id.txt_coment_adm);
            timeAdm = itemView.findViewById(R.id.txt_coment_time_adm);
        }
    }
}
