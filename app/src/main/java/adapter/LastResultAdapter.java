package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import utils.CONSTANTS;
import utils.UTILS;

public class LastResultAdapter extends RecyclerView.Adapter<LastResultAdapter.ViewHolder> {


    Context context;
    List<PartidaData> data;
    int resorce;
    Listener listener;

    public LastResultAdapter(Context context, List<PartidaData> data, int resorce, Listener listener) {
        this.context = context;
        this.data = data;
        this.resorce = resorce;
        this.listener = listener;
    }

    public LastResultAdapter(Context context, List<PartidaData> data, int resorce) {
        this.context = context;
        this.data = data;
        this.resorce = resorce;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(resorce, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        //Get position
        final PartidaData partida = data.get(position);

        //Imagem 1
        UTILS.getImageAt(context, partida.getClubeImg1(), holder.image1);

        //Imagem 2
        UTILS.getImageAt(context, partida.getClubeImg2(), holder.image2);

        holder.date.setText(UTILS.getFormattedDate(partida.getDataHora()));
        holder.local.setText(partida.getDesc());

        holder.result1.setText(partida.getGolsClube1());
        holder.result2.setText(partida.getGolsClube2());

        if(partida.getPenC1() != null){
            holder.versus.setVisibility(View.GONE);
            String penalts = "(" + partida.getPenC1() +" x " + partida.getPenC2() + ")";
            holder.penalts.setText(penalts);
            holder.penalts.setVisibility(View.VISIBLE);
        }

        if(partida.getCor1() != null && !partida.getCor1().isEmpty()){
            holder.winner1.setVisibility(View.VISIBLE);
            holder.winner1.setBackgroundColor(UTILS.parseColorByString(partida.getCor1()));
        }else {
            holder.winner1.setVisibility(View.INVISIBLE);
        }

        if(partida.getCor2() != null && !partida.getCor2().isEmpty()){
            holder.winner2.setVisibility(View.VISIBLE);
            holder.winner2.setBackgroundColor(UTILS.parseColorByString(partida.getCor2()));
        }else{
            holder.winner2.setVisibility(View.INVISIBLE);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(partida.getId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void onClick (int id);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image1;
        ImageView image2;

        TextView date;
        TextView local;

        TextView result1;
        TextView result2;

        TextView versus;
        TextView penalts;

        FrameLayout winner1;
        FrameLayout winner2;

        View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView;
            image1 = itemView.findViewById(R.id.img_time_1);
            image2 = itemView.findViewById(R.id.img_time_2);

            date = itemView.findViewById(R.id.txt_date_match);
            local = itemView.findViewById(R.id.txt_local_match);

            result1 = itemView.findViewById(R.id.txt_result_1);
            result2 = itemView.findViewById(R.id.txt_result_2);

            versus = itemView.findViewById(R.id.txt_versus);
            penalts = itemView.findViewById(R.id.txt_empate);

            winner1 = itemView.findViewById(R.id.frame_winner_1);
            winner2 = itemView.findViewById(R.id.frame_winner_2);

        }
    }
}
