package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.net.URL;
import java.util.List;

import data.CheckinPartidaData;
import data.PartidaData;
import utils.CONSTANTS;
import utils.UTILS;

public class ChekinJogosAdapter extends RecyclerView.Adapter<ChekinJogosAdapter.ViewHolder> {

    Context context;
    List<CheckinPartidaData> data;
    Listener listener;

    public ChekinJogosAdapter(Context context, List<CheckinPartidaData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_checkin_partida, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final CheckinPartidaData partida = data.get(position);

        Glide.with(context)
                .load(R.drawable.logo_fortaleza)
                .into(holder.img1);

        String linkAdversario[] = partida.getLogo_adversario().split("\\?");
        String link = CONSTANTS.apiServer + partida.getLogo_adversario();
        String finalLink = link.replace("http:", "https:");
        Log.d("Async", finalLink);
        Glide.with(context)
                .load(finalLink)
                .into(holder.img2);

        holder.title.setText(partida.getCampeonato());
        holder.date.setText(UTILS.formatDateFromDB(partida.getData()));
        holder.hour.setText(UTILS.dateZuluToGMT(partida.getHora()));
        holder.local.setText(partida.getEstadio());

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(partida);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void onClick (CheckinPartidaData data);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View item;
        ImageView img1;
        ImageView img2;
        TextView title;
        TextView date;
        TextView hour;
        TextView local;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            img1 = itemView.findViewById(R.id.img_time_1);
            img2 = itemView.findViewById(R.id.img_time_2);
            title = itemView.findViewById(R.id.txt_tipo_jogo);
            date = itemView.findViewById(R.id.txt_jogo_data);
            hour = itemView.findViewById(R.id.txt_jogo_time);
            local = itemView.findViewById(R.id.txt_jogo_local);
        }
    }
}
