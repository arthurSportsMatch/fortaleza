package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.R;

import java.util.List;

import data.VendedorData;

public class VendedorAdapter extends RecyclerView.Adapter<VendedorAdapter.ViewHolder> {

    Context context;
    List<VendedorData> data;
    Interaction listener;

    public VendedorAdapter(Context context, List<VendedorData> data, Interaction listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    public void addData (List<VendedorData> nData){
        data.addAll(nData);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_vendedores,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final VendedorData vendedor = data.get(position);

        holder.nome.setText(vendedor.getNome());
        holder.distribuidor.setText(vendedor.getDistribuidor());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(vendedor.getId(), vendedor.getNome());
            }
        });

        if(position >= data.size()-1){
            listener.Botton();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface Interaction {
        void onClick(int id, String nome);
        void Botton ();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView nome;
        TextView distribuidor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            nome = itemView.findViewById(R.id.txt_nome_vend);
            distribuidor = itemView.findViewById(R.id.txt_distribuidor_vend);
        }
    }
}
