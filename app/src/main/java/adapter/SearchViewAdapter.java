package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.SearchData;
import utils.TRACKING;

public class SearchViewAdapter extends ArrayAdapter<SearchData> {

    private static final String TAG = "SearchViewAdapter";

    Activity activity;
    Context context;
    int layoutResourceId;
    List<SearchData> data = null;
    ISearchSelectable callback = null;

    public interface ISearchSelectable {
        void OnSearchSelected(SearchData item);
    }

    public SearchViewAdapter(Activity activity, int layoutResourceId, List<SearchData> data, ISearchSelectable callback) {
        super(activity.getApplicationContext(), layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = activity.getApplicationContext();
        this.data = data;
        this.activity = activity;
        this.callback = callback;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SearchDataHolder holder = null;

        if (position >= data.size()) {
            return row;
        }

        if(row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new SearchDataHolder();
            holder.btnBg = (LinearLayout)row.findViewById(R.id.btnBg);
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            holder.txtResults = (TextView)row.findViewById(R.id.txtResults);

            row.setTag(holder);
        }
        else {
            holder = (SearchDataHolder)row.getTag();
        }

        final SearchData item = data.get(position);

        String nome;
        int img = -1;

        switch (item.getIdTipo()){
            default:
                nome = activity.getString(R.string.puratos_search);
                //img = R.drawable.lupa;
                break;
            case TRACKING.TRACKING_BUSCA_NOTICIA:
                nome = activity.getString(R.string.news);
                //img = R.drawable.ic_nocircle_receitas;
                break;
            case TRACKING.TRACKING_BUSCA_MURAL:
                nome = activity.getString(R.string.wall);
                //img = R.drawable.ic_nocircle_mural;
                break;
            case TRACKING.TRACKING_BUSCA_RECEITAS:
                nome = activity.getString(R.string.recipes);
                img = R.drawable.ic_nocircle_receitas;
                break;
            case TRACKING.TRACKING_BUSCA_CATALOGO:
                nome = activity.getString(R.string.catalog);
                //img = R.drawable.ic_nocircle_catalogo;
                break;
            case TRACKING.TRACKING_BUSCA_TREINAMENTO:
                nome = activity.getString(R.string.trainings);
                //img = R.drawable.ic_nocircle_treinamentos;
                break;
            case TRACKING.TRACKING_BUSCA_PRODUTOS:
                nome = activity.getString(R.string.products);
                //img = R.drawable.ic_nocircle_listadeprod;
                break;
            case TRACKING.TRACKING_BUSCA_INFORMATIVOS:
                nome = activity.getString(R.string.informatives);
                //img = R.drawable.ic_nocircle_campanhas;
                break;
            case TRACKING.TRACKING_BUSCA_PREMIOS:
                nome = activity.getString(R.string.awards);
                //img = R.drawable.ic_nocircle_premios;
                break;
            case TRACKING.TRACKING_BUSCA_AGENDA:
                nome = activity.getString(R.string.agenda);
                //img = R.drawable.ic_no_circle_agenda;
                break;
        }

        //puxar imagem
        if(img != -1){
            Glide.with(getContext()).
                    load(img).
                    into(holder.imgIcon);
        }

        holder.txtTitle.setText(nome);
        holder.txtResults.setText( String.format(activity.getString(item.getTot() == 1 ? R.string.x_results_s : R.string.x_results_p), item.getTot()) );

        if(callback != null){
            holder.btnBg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.OnSearchSelected(item);
                }
            });
        }

        return row;
    }

    static class SearchDataHolder {
        LinearLayout btnBg;
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtResults;
    }
}