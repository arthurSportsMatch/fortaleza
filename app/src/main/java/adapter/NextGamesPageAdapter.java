package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import data.PartidasData;
import utils.CONSTANTS;
import utils.UTILS;

public class NextGamesPageAdapter extends PagerAdapter {

    Context context;
    List<PartidasData> data;
    Listener listener;
    boolean hide;
    int gameFocusID = -1;

    public NextGamesPageAdapter(Context context, List<PartidasData> data) {
        this.context = context;
        this.data = data;
    }

    public NextGamesPageAdapter(Context context, List<PartidasData> data, boolean hide) {
        this.context = context;
        this.data = data;
        this.hide = hide;
    }

    public NextGamesPageAdapter(Context context, List<PartidasData> data, boolean hide, Listener listener) {
        this.context = context;
        this.data = data;
        this.hide = hide;
        this.listener = listener;
    }

    public NextGamesPageAdapter(Context context, List<PartidasData> data, boolean hide, int focus, Listener listener) {
        this.context = context;
        this.data = data;
        this.hide = hide;
        this.listener = listener;
        this.gameFocusID = focus;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View scrolView = LayoutInflater.from(context).inflate(R.layout.adapter_simple_linear_list, container, false);
        LinearLayout mainView = scrolView.findViewById(R.id.linear_childs);

        boolean haschilds = false;
        if(data.get(position).getPartidas().size() > 1){
            haschilds = true;
        }

        for(final PartidaData partida : data.get(position).getPartidas()){
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_agenda_jogos, container, false);
            ViewHolder holder = new ViewHolder(view);

            //Imagem primeiro time
            UTILS.getImageAt(context, partida.getClubeImg1(), holder.image1);

            //Imagem primeiro time
            UTILS.getImageAt(context, partida.getClubeImg2(), holder.image2);

            if(!hide)
                holder.timer.setText(UTILS.formatHourFromDBEvent(partida.getDataHora()));

            holder.local.setText(partida.getEstadio());

            if (partida.getTitle() != null)
                holder.txtCampeonato.setText(partida.getTitle());

            if (partida.getHeader() != null && !partida.getHeader().isEmpty()) {
                holder.tipoPartida.setVisibility(View.VISIBLE);
                holder.txtTipoPartida.setText(partida.getHeader());
            }

            final String link = partida.getLinkCompra();
            if (partida.getLinkCompra() != null) {
                holder.comprar.setVisibility(View.VISIBLE);
                holder.comprar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null){
                            listener.OnPurchase(partida);
                        }else {
                            String url = link;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            context.startActivity(i);

                        }
                    }
                });
            } else {
                holder.comprar.setVisibility(View.GONE);
            }

            if(partida.isPermiteCheckIn()){
                holder.checkIn.setVisibility(View.VISIBLE);
                holder.checkIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null){
                            listener.OnCheckIn();
                        }else {
                            Intent i = new Intent(context, LojaFortalezaActivity.class);
                            context.startActivity(i);
                        }
                    }
                });
            }else {
                holder.checkIn.setVisibility(View.GONE);
            }

            if(partida.getIdTransmissao() != null){
                String[] chanels = partida.getIdTransmissao().split(",");

                for(int i = 0; i < chanels.length; i ++){
                    addTransmistion(holder.tvs, chanels[i]);
                }
            }

            if(haschilds){
                holder.separador.setVisibility(View.VISIBLE);
            }

            mainView.addView(view);
        }

        container.addView(scrolView);

        return scrolView;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    void addTransmistion (LinearLayout view, String id){
        ImageView image = new ImageView(context);
        image.setPadding(10,0,0,0);
        image.setAdjustViewBounds(true);
        image.setImageResource(UTILS.getChanelD(Integer.parseInt(id)));
        view.addView(image);
    }

    public interface Listener {
        void OnPurchase (PartidaData data);
        void OnCheckIn ();
    }

    class ViewHolder {
        public View tipoPartida;
        public TextView txtTipoPartida;
        public ImageView imgTipoPartida;

        public TextView txtCampeonato;

        public ImageView image1;    //imagem do primeiro time
        public ImageView image2;    //imagem do segundo time

        public TextView timer;      //horario do jogo
        public TextView local;      //local do jogo

        public LinearLayout tvs;            //para colocar lista de imagens
        public Button comprar;
        public Button checkIn;
        public View separador;

        public ViewHolder(View itemView) {
            tipoPartida = itemView.findViewById(R.id.view_ag_tipo);
            txtTipoPartida = itemView.findViewById(R.id.txt_age_tipo);
            imgTipoPartida = itemView.findViewById(R.id.img_age_tipo);

            txtCampeonato = itemView.findViewById(R.id.txt_tipo_jogo);

            image1 = itemView.findViewById(R.id.img_time_1);
            image2 = itemView.findViewById(R.id.img_time_2);

            timer = itemView.findViewById(R.id.txt_jogo_time);
            local = itemView.findViewById(R.id.txt_jogo_local);

            tvs = itemView.findViewById(R.id.view_where_watch);
            comprar = itemView.findViewById(R.id.btn_comprar);
            checkIn = itemView.findViewById(R.id.btn_check_in);
            separador = itemView.findViewById(R.id.separador);
        }
    }
}
