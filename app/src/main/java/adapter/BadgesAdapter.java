package adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.BadgeData;
import utils.CONSTANTS;
import utils.UTILS;

public class BadgesAdapter extends RecyclerView.Adapter<BadgesAdapter.ViewHolder>{

    List<BadgeData> data;
    Context context;
    Listener listener;

    public BadgesAdapter(Context context, List<BadgeData> data, Listener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    public void addBadges (List<BadgeData> data){
        if(data == null){
            this.data.removeAll(this.data);
            notifyDataSetChanged();
        }else{
            this.data.addAll(data);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(context).inflate(R.layout.adapter_badge_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position >= data.size() -1){
            listener.OnEndOfList();
        }

        final BadgeData badge = data.get(position);

        UTILS.getImageAt(context,badge.getImg(), holder.image);

        ColorMatrix satured = new ColorMatrix();
        satured.setSaturation(0f);

        ColorMatrix color = new ColorMatrix();
        color.setSaturation(1f);

        if(badge.getOwned() == 1){
            holder.image.setColorFilter(new ColorMatrixColorFilter(color));
        }else{
            holder.image.setColorFilter(new ColorMatrixColorFilter(satured));
        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(badge);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public interface Listener {
        void OnClick (BadgeData data);
        void OnEndOfList ();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_image_gallery);
        }
    }
}
