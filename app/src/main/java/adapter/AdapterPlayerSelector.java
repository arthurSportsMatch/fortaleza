package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.EscalarTimeActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.AtletaData;
import data.PositonData;

public class AdapterPlayerSelector extends RecyclerView.Adapter<AdapterPlayerSelector.ViewHolder> {

    Context context;
    List<AtletaData> atletas;
    List<PositonData> pos;
    Listener listener;
    EscalarTimeActivity.AtletaEscalado[] atletasToAdd = new EscalarTimeActivity.AtletaEscalado[11];

    public AdapterPlayerSelector(Context context, List<AtletaData> atletas, List<PositonData> pos) {
        this.context = context;
        this.atletas = atletas;
        this.pos = pos;
    }

    public AdapterPlayerSelector(Context context, List<AtletaData> atletas, List<PositonData> pos, Listener listener) {
        this.context = context;
        this.atletas = atletas;
        this.pos = pos;
        this.listener = listener;
    }

    public void addAtleta (int id, EscalarTimeActivity.AtletaEscalado atleta){
        checkAtletaPlacements(atleta.idPlayer);
        atletasToAdd[id] = atleta;
        notifyDataSetChanged();
    }

    void checkAtletaPlacements (int atletaID){
        for(EscalarTimeActivity.AtletaEscalado atl : atletasToAdd){
            if(atl != null){
                if(atl.idPlayer == atletaID){
                    atletasToAdd[atl.idPosicao-1] = null;
                }
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_player_selector,parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AtletaData atleta = atletas.get(position);

        if(atleta.getCamisa() != null){
            String camisa = String.valueOf(atleta.getCamisa());
            holder.camisa.setText( camisa != null ? camisa : "");
        }

        if(atleta.getPos() != null){
            int pos = Integer.parseInt(atleta.getPos());
            holder.pos.setText( atleta.getPos() != null ? getPositions(pos): "");
        }


        String nome = atleta.getNome();
        holder.nome.setText( nome != null ? nome : "");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                listener.getAtleta(atleta);
            }
        });

        if(hasOnList(atleta.getId())){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorFundoAtletasSelect));
            holder.pos.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }else{
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorFundoAtletas));
            holder.pos.setTextColor(context.getResources().getColor(R.color.colorRedSPFC2));
        }
    }

    public boolean hasOnList (int atletaID){
        boolean result = false;
        for(EscalarTimeActivity.AtletaEscalado atl : atletasToAdd){
            if(atl != null){
                if(atl.idPlayer == atletaID){
                    result = true;
                }
            }
        }

        return result;
    }

    @Override
    public int getItemCount() {
        return atletas.size();
    }

    String getPositions (int id){
        for(PositonData p : pos){
            if(p.getId() == id){
                return  p.getNome();
            }
        }
        return  null;
    }

    public interface Listener {
        void getAtleta (AtletaData atleta);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView nome;
        TextView pos;
        TextView camisa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            nome = itemView.findViewById(R.id.txt_nome);
            pos = itemView.findViewById(R.id.txt_pos);
            camisa = itemView.findViewById(R.id.txt_camisa);
        }
    }
}
