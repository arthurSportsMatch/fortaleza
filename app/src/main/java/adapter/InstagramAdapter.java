package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.InstagramData;
import utils.UTILS;

public class InstagramAdapter extends RecyclerView.Adapter<InstagramAdapter.ViewHolder> {

    Context context;
    List<InstagramData> data;
    Listener listener;

    public InstagramAdapter(Context context, List<InstagramData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_stories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final InstagramData insta = data.get(position);

        UTILS.getImageAt(context, insta.getDisplay_url(), holder.content);

        if(insta.getVideo_src() != null){
            holder.icImage.setVisibility(View.GONE);
            holder.icVideo.setVisibility(View.VISIBLE);
        }

        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(insta);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void OnClick (InstagramData data);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView content;
        ImageView icVideo;
        ImageView icImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.img_content);
            icVideo = itemView.findViewById(R.id.img_video);
            icImage = itemView.findViewById(R.id.img_fotos);
        }
    }
}
