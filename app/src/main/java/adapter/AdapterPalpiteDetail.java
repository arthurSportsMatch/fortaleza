package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.R;

import java.util.List;

import data.PalpiteDetailData;

public class AdapterPalpiteDetail extends RecyclerView.Adapter<AdapterPalpiteDetail.ViewHolder> {

    Context context;
    List<PalpiteDetailData> data;

    public AdapterPalpiteDetail(Context context, List<PalpiteDetailData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_palpite_detail, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PalpiteDetailData palpite = data.get(position);
        boolean correto = false;

        String total = String.valueOf(data.size());
        String sPosition = (position + 1) + "/"+ total;
        holder.perguntaNum.setText(sPosition);

        //Coloca botão de correto
        if(palpite.getCerto() != null){
            if(palpite.getCerto() == 1){
                holder.correto.setVisibility(View.VISIBLE);
                holder.incorreto.setVisibility(View.GONE);
                correto = true;
            }else{
                holder.incorreto.setVisibility(View.VISIBLE);
                holder.correto.setVisibility(View.GONE);
                correto = false;
            }
        }

        //Coloca moedas ganhas
        if(palpite.getMoedas() != null){
            String moedas = "+" + palpite.getMoedas();

            if(palpite.getMoedas() <= 0){
                moedas = "0";
            }

            holder.moedas.setText(moedas);
        }else{
            holder.moedas.setText("0");
        }

        //Coloca pergunta
        if(palpite.getPergunta() != null){
            holder.pergunta.setText(palpite.getPergunta());
        }

        //Coloca respostas do usuario
        if(palpite.getRespostaUser1() != null){
            String resposta =  palpite.getRespostaUser1();
            if(palpite.getRespostaUser2() != null){
                resposta = resposta + "x" + palpite.getRespostaUser2();
            }

            holder.resposta.setText(resposta);
            holder.resposta.setTextColor(context.getResources().getColor(correto? R.color.colorRedSPFC6: R.color.colorGray));
        }

        //Caso incorreto coloca respostas corretas
        if(!correto){
            if(palpite.getRespostaCorreta1() != null){
                String resposta =  palpite.getRespostaCorreta1();
                if(palpite.getRespostaCorreta2() != null){
                    resposta = resposta + "x" + palpite.getRespostaCorreta2();
                }

                holder.respostaCorreta.setText("Resposta correta: " + resposta);
                holder.respostaCorreta.setVisibility(View.VISIBLE);
            }
        }else{
            holder.respostaCorreta.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView perguntaNum;
        TextView pergunta;
        TextView moedas;
        TextView resposta;
        TextView respostaCorreta;

        ImageView correto;
        ImageView incorreto;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            perguntaNum = itemView.findViewById(R.id.txt_pergunta_numero);
            pergunta = itemView.findViewById(R.id.txt_pergunta);
            resposta = itemView.findViewById(R.id.txt_resposta);
            respostaCorreta = itemView.findViewById(R.id.txt_resposta_correta);

            moedas = itemView.findViewById(R.id.txt_moedas);
            correto = itemView.findViewById(R.id.img_acerto);
            incorreto = itemView.findViewById(R.id.img_erro);
        }
    }
}
