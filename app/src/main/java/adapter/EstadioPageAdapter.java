package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.sportm.fortaleza.R;

import java.util.List;

import data.CheckinSetoresData;

public class EstadioPageAdapter extends PagerAdapter {

    Context context;
    List<CheckinSetoresData> data;

    public EstadioPageAdapter(Context context, List<CheckinSetoresData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_estadio_image,container,false);
        ImageView pagerImage = view.findViewById(R.id.pagerImage);
        pagerImage.setImageResource(getImage(data.get(position).getNome()));
        container.addView(view);
        return view;
    }


    int getImage (String setor){
        int color = R.drawable.e_bossa_nova;
        switch (setor.toLowerCase()){
            case "premium" : color = R.drawable.e_premium;
                break;
            case "especial" : color = R.drawable.e_especial;
                break;
            case "bossa nova" : color = R.drawable.e_bossa_nova;
                break;
            case "inferior sul" :color = R.drawable.e_inferior_sul;
                break;
            case "superior sul" : color = R.drawable.e_superior_sul;
                break;
            case "camarotes" : color = R.drawable.e_camarote;
                break;
            case "superior norte" : color = R.drawable.e_superior_norte;
                break;
            case "superior central" : color = R.drawable.e_superior_central;
                break;
            case "inferior norte" : color = R.drawable.e_inferior_nort;
                break;
        }
        return color;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).getNome();
    }
}
