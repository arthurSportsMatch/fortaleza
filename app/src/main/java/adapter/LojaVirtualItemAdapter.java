package adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;
import java.util.Locale;

import data.ProdutoFData;
import utils.CONSTANTS;
import utils.UTILS;

public class LojaVirtualItemAdapter extends RecyclerView.Adapter<LojaVirtualItemAdapter.ViewHolder> {

    public LojaVirtualItemAdapter(Context context, List<ProdutoFData> data) {
        this.context = context;
        this.data = data;
    }

    public LojaVirtualItemAdapter(Context context, List<ProdutoFData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }



    public LojaVirtualItemAdapter(Context context, int nweight, List<ProdutoFData> data, Listener listener) {
        this.context = context;
        this.nweight = nweight;
        this.data = data;
        this.listener = listener;
    }

    Context context;
    int nweight = 0;
    List<ProdutoFData> data;
    Listener listener;



    public void changeData (List<ProdutoFData> nData){
        data = nData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_loja_virtual_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ProdutoFData produto = data.get(position);

        UTILS.getImageAt(context, produto.getImg(), holder.image);

        holder.nome.setText(produto.getNome());

        String valorConcat = "";
        float valorST = produto.getValorST();
        if(valorST > 0){
            holder.precoST.setVisibility(View.VISIBLE);
            holder.txtSocio.setVisibility(View.VISIBLE);
            holder.precoST.setText(getValor( valorST));
            valorConcat = "ou ";
        }else{
            holder.precoST.setVisibility(View.GONE);
            holder.txtSocio.setVisibility(View.GONE);
        }

        float valor = produto.getValor();
        if(valor > 0){
            holder.preco.setVisibility(View.VISIBLE);
            holder.precoDetails.setVisibility(View.VISIBLE);
            holder.preco.setText(valorConcat + getValor( valor));
            holder.precoDetails.setText(createPrecoParcelado(produto.getNumeroParcelas(), produto.getValorParcelado()));
        }else{
            holder.preco.setVisibility(View.GONE);
            holder.precoDetails.setVisibility(View.GONE);
        }

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(produto);
            }
        });

        if(nweight > 0){
            holder.mainView.getLayoutParams().width = nweight;
            holder.mainView.requestLayout();
        }
    }

    String getValor (float valor){
        String result =  "R$ @";
        String s = String.format(Locale.getDefault(), "%.2f", valor);
        result = result.replace("@", s);
        return  result;
    }

    String createPrecoParcelado (int parcelas, float valorParcelado){
        String result = "Em até #x de @";
        result = result.replace("#", String.valueOf(parcelas));
        result = result.replace("@", getValor(valorParcelado));

        return  result;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void onClick (ProdutoFData produto);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View mainView;
        ImageView image;
        TextView nome;
        TextView precoST;
        TextView txtSocio;
        TextView preco;
        TextView precoDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mainView = itemView;
            image = itemView.findViewById(R.id.img_produto);
            nome = itemView.findViewById(R.id.txt_nome);
            precoST = itemView.findViewById(R.id.txt_preco_socio);
            txtSocio = itemView.findViewById(R.id.txt_socio);
            preco = itemView.findViewById(R.id.txt_preco);
            precoDetails = itemView.findViewById(R.id.txt_parcelado);
        }
    }
}
