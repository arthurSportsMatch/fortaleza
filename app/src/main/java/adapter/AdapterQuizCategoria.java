package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.QuizCategoriaData;
import utils.CONSTANTS;
import utils.UTILS;

public class AdapterQuizCategoria extends RecyclerView.Adapter<AdapterQuizCategoria.ViewHolder>{

    Context context;
    List<QuizCategoriaData> data;
    Listener listener;

    public void addCategoria (List<QuizCategoriaData> list){
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void changeCategoria (List<QuizCategoriaData> list){
        data.removeAll(data);
        data.addAll(list);
        notifyDataSetChanged();
    }

    public AdapterQuizCategoria(Context context, List<QuizCategoriaData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_quiz_categoria, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position >= data.size() -1){
            Log.d("Async", "Last Item");
            listener.OnLastItem();
        }

        final QuizCategoriaData quiz = data.get(position);

        holder.tittle.setText(quiz.getTitulo());


        UTILS.getImageAt(context,quiz.getImg(), holder.image);

        final int pos = position % 3;

        switch (pos){
            case 0 :{
                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonRedTransp2));
                holder.tittle.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.subTittle.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }
            break;
            case 1 :{
                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonWhiteTransp));
                holder.tittle.setTextColor(context.getResources().getColor(R.color.colorBlackLetter));
                holder.subTittle.setTextColor(context.getResources().getColor(R.color.colorBlackLetter));
            }
            break;
            case 2 :{
                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonBlackTransp));
                holder.tittle.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.subTittle.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }
            break;
        }

        final String quantidade = quiz.getQuantidade();
        final String[] value =  quantidade.split("/");
        int actual = Integer.parseInt(value[0]);

        if(quiz.getCorretas() != null){
            String quant = quiz.getCorretas() + "/" + actual;
            holder.subTittle.setText(quant);
        }else{
            String quant = "0/" + actual;
            holder.subTittle.setText(quant);
        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int actual = Integer.parseInt(value[0]);
                int max = Integer.parseInt(value[1]);

                boolean canUse = true;
                String quant = "0";
                if(quiz.getCorretas() != null) {
                    quant = quiz.getCorretas() + "/" + actual;
                }else{
                    quant = "0/" + actual;
                }

                if(actual >= max){
                    canUse = false;
                }

                listener.OnClick(quiz.getId(), quiz.getTitulo(), quiz.getDescricao(), quiz.getQuantidade(), quant, quiz.getImg(), pos, canUse);

            }
        });
    }

    public interface Listener{
        void OnClick (int id, String tittle, String description, String quant, String actualValue, String img, int pos, boolean canUse);
        void OnLastItem ();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView image;
        ImageView foreground;
        TextView tittle;
        TextView subTittle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.img_background);
            foreground = itemView.findViewById(R.id.color_background);
            tittle = itemView.findViewById(R.id.txt_titulo);
            subTittle = itemView.findViewById(R.id.txt_description);
        }
    }
}
