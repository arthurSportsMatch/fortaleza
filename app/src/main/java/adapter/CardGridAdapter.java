package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.sportm.fortaleza.R;

public class CardGridAdapter extends BaseAdapter {

    Context context;
    int quant;

    public CardGridAdapter(Context context, int quant) {
        this.context = context;
        this.quant = quant;
    }

    @Override
    public int getCount() {
        return quant;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView image = new ImageView(context);
        image.setImageResource(R.color.colorYellowCard);

//        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
//        params.width = 100;
//        params.height = 20;
//        params.setMargins(2,2,2,2);
//        image.setLayoutParams(params);


        //image.setAdjustViewBounds(true);

        return image;
    }
}
