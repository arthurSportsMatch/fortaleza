package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import java.util.List;

import data.ProdutoData;
import utils.CONSTANTS;
import utils.UTILS;

public class AdapterLojaItens extends RecyclerView.Adapter<AdapterLojaItens.ViewHolder> {

    Context context;
    List<ProdutoData> data;
    Listener listener;

    public AdapterLojaItens(Context context, List<ProdutoData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_item_loja, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ProdutoData produto = data.get(position);

        UTILS.getImageAt(context, produto.getImg(), holder.itemImg, new RequestOptions()
                .error(R.drawable.ic_bag_loja)
                .placeholder(R.drawable.ic_bag_loja));

        holder.description.setText(produto.getNome());

        if(produto.getValorST() != null){
            holder.socioCoinText.setText(produto.getValorST());
        }else{
            holder.socioCoin.setVisibility(View.GONE);
        }


        if(produto.getValor() != null){
            holder.normalCoinText.setText(produto.getValor());
        }else{
            holder.normalCoin.setVisibility(View.GONE);
            holder.socio.setVisibility(View.VISIBLE);
            holder.background.setBackgroundColor(context.getResources().getColor(R.color.colorBackSocioTorcedor));
        }

        if(produto.getAdquirido() != null){
            Log.d("Async", produto.toString());
            holder.socio.setVisibility(View.GONE);
            holder.adiquirido.setVisibility(View.VISIBLE);
            holder.background.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            String data = UTILS.formatDateFromDB(produto.getAdquirido()).replace("/2020", "");
            holder.adiquirido.setText("Adquirido em " + data);
        }

        holder.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(produto);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        public void OnClick(ProdutoData produtoData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View background;
        ImageView itemImg;
        TextView description;

        View normalCoin;
        TextView normalCoinText;

        View socioCoin;
        TextView socioCoinText;

        View socio;
        TextView adiquirido;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            background = itemView.findViewById(R.id.view_back);

            itemImg = itemView.findViewById(R.id.img_produto);
            description = itemView.findViewById(R.id.txt_description);

            normalCoin = itemView.findViewById(R.id.view_coin_normal);
            normalCoinText = itemView.findViewById(R.id.txt_coin_normal);

            socioCoin = itemView.findViewById(R.id.view_coin_socio);
            socioCoinText = itemView.findViewById(R.id.txt_coin_socio);

            socio = itemView.findViewById(R.id.view_socio_torcedor);
            adiquirido = itemView.findViewById(R.id.txt_adiquirido);
        }
    }
}
