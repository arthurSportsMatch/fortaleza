package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.R;

import java.util.List;

import data.NotificationData;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context context;
    List<NotificationData> data = null;
    Listener callback = null;

    public NotificationAdapter(Context context, List<NotificationData> data, Listener callback) {
        this.context = context;
        this.data = data;
        this.callback = callback;
    }

    public List<NotificationData> getData() {
        return data;
    }

    public void setData(List<NotificationData> data) {
        this.data.removeAll(this.data);
        this.data = data;
        notifyDataSetChanged();
    }

    public void addData (List<NotificationData> data){
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_notification,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if(position >= data.size() -1){
            callback.onBottom();
        }

        NotificationData notification = data.get(position);

        holder.text.setText(notification.getMsg());
        holder.tittle.setText(notification.getTitulo());

        holder.text.setTextColor(context.getResources().getColor(notification.isClicou()? R.color.colorGray: R.color.colorButtonRed));
        holder.tittle.setTextColor(context.getResources().getColor(notification.isClicou()? R.color.colorGray: R.color.colorButtonRed));

        holder.notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.isPressed(data.get(position));
            }
        });

        //holder.date.setText(UTILS.getFormattedDate(data.get(position).getData()));
        //holder.news.setVisibility(data.get(position).isClicou() ? View.INVISIBLE : View.VISIBLE);
        //holder.notification.setAlpha(data.get(position).isClicou() ? 0.5f : 1f);
//        switch (data.get(position).getTipo()){
//            case NotificationsFragment.NOTIFICATION_TIPO_FALE_CONOSCO: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_faleconosco)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_POST_MURAL: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_mural)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_NOTICIA: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_noticias)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_QUIZ: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_quizz)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_PESQUISA: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_pesquisas)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_INFORMATIVO: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_campanhas)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_RECEITA: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_receitas)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_TREINAMENTO: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_treinamentos)); break;
//            case NotificationsFragment.NOTIFICATION_TIPO_EVENTO: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_no_circle_agenda)); break;
//            default: holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_nocircle_notification));
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void isPressed(NotificationData data);
        void onBottom ();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        View notification;
        ImageView news;
        ImageView icon;
        TextView text;
        TextView tittle;
        TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            notification = itemView.findViewById(R.id.holder_notification);
            news = itemView.findViewById(R.id.img_notification_notsee);
            icon = itemView.findViewById(R.id.img_notification_icon);
            text = itemView.findViewById(R.id.txt_notification_text);
            tittle = itemView.findViewById(R.id.txt_notification_title);
            date = itemView.findViewById(R.id.txt_notification_date);
        }
    }
}
