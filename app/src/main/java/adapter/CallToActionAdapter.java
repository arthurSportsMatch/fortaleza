package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.CallToActionData;
import utils.CONSTANTS;
import utils.UTILS;

public class CallToActionAdapter extends RecyclerView.Adapter<CallToActionAdapter.ViewHolder> {

    List<CallToActionData> list;
    Interaction listener;
    Context context;

    public CallToActionAdapter(List<CallToActionData> list, Context context, Interaction listener) {
        this.list = list;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_image_galley, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        CallToActionData data = list.get(position);
        UTILS.getImageAt(context,data.getImg(), holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface Interaction{
        void OnClick (int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_image_gallery);
        }
    }
}
