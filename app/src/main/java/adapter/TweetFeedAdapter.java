package adapter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import data.TweetResultData;
import utils.AsyncOperation;
import utils.UTILS;

public class TweetFeedAdapter extends ArrayAdapter<TweetResultData> implements AsyncOperation.IAsyncOpCallback {

    private static final String TAG = "TweetFeedAdapter";

    private static final int OP_GET_HTML_CODE_VIDEO = 0;
    private static final int OP_GET_HTML_CODE_IMG = 1;

    private Activity activity;
    private int res;
    private List<TweetResultData> data;
    private List<Long> videoRequestIds = new ArrayList<>();
    private List<Long> videoRequestIdsTotal = new ArrayList<>();
    private List<Long> imgRequestIds = new ArrayList<>();
    private List<Long> imgRequestIdsTotal = new ArrayList<>();
    private int maxTries = 3;

    private int maxChars = 180;
    private int nWeight = 270;

    public interface ITweetSelectable{
        void OnSelect(TweetResultData item, WebView webView);
        void OnMediaClick(TweetResultData item);
        void OnLearnMoreClick(TweetResultData item);
        void OnLastCardClick();
    }

    ITweetSelectable callback = null;

    public TweetFeedAdapter(Activity activity, int layoutResourceId, List<TweetResultData> data, ITweetSelectable callback) {
        super(activity, layoutResourceId, data);
        this.res = layoutResourceId;
        this.activity = activity;
        this.data = data;
        this.callback = callback;
    }

    public TweetFeedAdapter(Activity activity, int layoutResourceId,int nWeight,  List<TweetResultData> data, ITweetSelectable callback) {
        super(activity, layoutResourceId, data);
        this.res = layoutResourceId;
        this.activity = activity;
        this.nWeight = nWeight;
        this.data = data;
        this.callback = callback;
    }

    public TweetFeedAdapter(Activity activity, int layoutResourceId, List<TweetResultData> data,  int maxChars, ITweetSelectable callback) {
        super(activity, layoutResourceId, data);
        this.res = layoutResourceId;
        this.activity = activity;
        this.data = data;
        this.callback = callback;
        this.maxChars = maxChars;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(res, parent, false);

            holder = new Holder();

            holder.bgTweet = row.findViewById(R.id.bgTweet);
            holder.rlNormalCard = row.findViewById(R.id.rlNormalCard);
            holder.rlLastCard = row.findViewById(R.id.rlLastCard);
            holder.imgPortrait = row.findViewById(R.id.imgPortrait);
            holder.txtUser = row.findViewById(R.id.txtUser);
            holder.txtTag = row.findViewById(R.id.txtTag);
            holder.txtMsg = row.findViewById(R.id.txtMsg);
            holder.txtDate = row.findViewById(R.id.txtDate);
            holder.txtMore = row.findViewById(R.id.txtMore);
            holder.imgContent = row.findViewById(R.id.imgContent);
            holder.webView = row.findViewById(R.id.webView);

            row.setTag(holder);
        }
        else {
            holder = (Holder)row.getTag();
        }

        //holder.webView.loadUrl("file:///android_asset/blank.html");

        holder.imgContent.setImageBitmap(null);
        holder.imgContent.setBackground(null);

        final TweetResultData tweet = data.get(position);

        //Quando for o ultimo item
        if(tweet.isLastItem()){

            holder.rlLastCard.setVisibility(View.VISIBLE);
            holder.rlNormalCard.setVisibility(View.INVISIBLE);

            if(callback != null) {
                holder.bgTweet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.OnLastCardClick();
                    }
                });
            }

            return row;
        }

        holder.rlLastCard.setVisibility(View.INVISIBLE);
        holder.rlNormalCard.setVisibility(View.VISIBLE);

        //Se tem imagem
        if(!tweet.getImgUser().isEmpty()) {
            String url = tweet.getImgUser();
            //url = url.replace("http", "https");
            Glide.with(activity).
                    load(url).
                    apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))).
                    into(holder.imgPortrait);
        }

        holder.txtUser.setText(tweet.getNameUser());
        holder.txtTag.setText(tweet.getTagUser());

        String txt = tweet.getMsgContent();

        if(txt.length() >= maxChars){
            txt = tweet.getMsgContent().substring(0, (maxChars - 1)) + "...";
        }

        holder.txtMsg.setText(txt);

        if(tweet.getDate() != null) {
            PrettyTime prettyTime = new PrettyTime(activity.getResources().getConfiguration().locale);
            holder.txtDate.setText(prettyTime.format(tweet.getDate()));
        }

        else {
            holder.txtDate.setText("");
        }

        //É imagem
        if(tweet.isImage()){

            holder.webView.setVisibility(View.GONE);
            holder.imgContent.setVisibility(View.VISIBLE);

            if(!tweet.getImgContent().isEmpty()) {
                String url = tweet.getImgContent();
                //url = url.replace("http", "https");
                Glide.with(activity).
                        load(url).
                        apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))
                                .error(R.drawable.tw__composer_logo_blue)).
                        into(holder.imgContent);
            }
            else {

                if(tweet.isTriedToLoadImg()) {

                    if(!tweet.getImgContent().isEmpty()) {
                        String url = tweet.getImgContent();
                        //url = url.replace("http", "https");
                        Glide.with(activity).
                                load(url).
                                apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))
                                        .error(R.drawable.tw__composer_logo_blue)).
                                into(holder.imgContent);
                    }
                    else {
                        Glide.with(activity).
                                load(R.drawable.tw__composer_logo_blue).
                                apply(new RequestOptions().centerInside()).
                                into(holder.imgContent);
                    }
                }
                else {

                    if(!imgRequestIds.contains(tweet.getId())) {

                        AddTweetIdToImageRequest(tweet.getId());

                        Hashtable<String, Object> params = new Hashtable<>();
                        params.put("url", tweet.getImageHtmlToExtract());
                        params.put("id", tweet.getId());

                        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_HTML_PAGE_CODE, OP_GET_HTML_CODE_IMG, this).execute(params);
                    }

                }

            }

        }

        //É video
        else {
            if(tweet.getVideoContent().isEmpty()){

                if(tweet.isTriedToLoadVideo()) {
                    holder.webView.setVisibility(View.GONE);
                    holder.imgContent.setVisibility(View.VISIBLE);

                    if(!tweet.getImgContent().isEmpty()) {
                        String url = tweet.getImgContent();
                        //url = url.replace("http", "https");
                        Glide.with(activity).
                                load(url).
                                apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))
                                        .error(R.drawable.tw__composer_logo_blue)).
                                into(holder.imgContent);
                    }
                    else {
                        Glide.with(activity).
                                load(R.drawable.tw__composer_logo_blue).
                                into(holder.imgContent);
                    }
                }
                else {

                    if(!videoRequestIds.contains(tweet.getId())) {

                        AddTweetIdToVideoRequest(tweet.getId());

                        Hashtable<String, Object> params = new Hashtable<>();
                        params.put("url", tweet.getVideoHtmlToExtract());
                        params.put("id", tweet.getId());

                        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_HTML_PAGE_CODE, OP_GET_HTML_CODE_VIDEO, this).execute(params);
                    }

                }
            }
            else {
                holder.webView.setVisibility(View.VISIBLE);
                holder.imgContent.setVisibility(View.INVISIBLE);

                holder.webView.getSettings().setJavaScriptEnabled(true);
                holder.webView.getSettings().setDefaultTextEncodingName("utf-8");
                holder.webView.setWebViewClient(new WebViewClient());
                holder.webView.setWebChromeClient(new WebChromeClient());

                holder.webView.getSettings().setDomStorageEnabled(true);
                holder.webView.getSettings().setAppCacheEnabled(true);
                holder.webView.getSettings().setAppCachePath(activity.getFilesDir().getAbsolutePath() + "/cache");
                holder.webView.getSettings().setDatabaseEnabled(true);
                holder.webView.getSettings().setDatabasePath(activity.getFilesDir().getAbsolutePath() + "/databases");

                holder.webView.loadUrl(tweet.getVideoContent());
            }
        }

        final Holder finalHolder = holder;
        if(callback != null){
            holder.imgContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.OnMediaClick(tweet);
                }
            });

            holder.txtMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.OnLearnMoreClick(tweet);
                }
            });

            holder.bgTweet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.OnSelect(tweet, finalHolder.webView);
                }
            });
        }

        row.getLayoutParams().width = nWeight;
        row.requestLayout();
        return row;
    }

    void AddTweetIdToVideoRequest(long id){
        int counter = 0;
        for(Long l : videoRequestIdsTotal){
            if(l == id){
                counter++;
            }
        }

        if(counter >= maxTries){
            for (TweetResultData t : data){
                if(t.getId() == id){
                    t.setTriedToLoadVideo(true);
                    return;
                }
            }
        }

        videoRequestIdsTotal.add(id);
        videoRequestIds.add(id);
    }

    void AddTweetIdToImageRequest(long id){
        int counter = 0;
        for(Long l : imgRequestIdsTotal){
            if(l == id){
                counter++;
            }
        }

        if(counter >= maxTries){
            for (TweetResultData t : data){
                if(t.getId() == id){
                    t.setTriedToLoadImg(true);
                    return;
                }
            }
        }

        imgRequestIdsTotal.add(id);
        imgRequestIds.add(id);
    }

    static class Holder {
        RelativeLayout bgTweet;
        RelativeLayout rlNormalCard;
        RelativeLayout rlLastCard;
        ImageView imgPortrait;
        TextView txtUser;
        TextView txtTag;
        TextView txtMsg;
        TextView txtDate;
        TextView txtMore;
        ImageView imgContent;
        WebView webView;
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        Message msg = new Message();
        msg.setData(b);
        handler.sendMessage(msg);
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Bundle b = msg.getData();
            int opId = b.getInt("opId");
            boolean success = b.getBoolean("success");
            JSONObject response = new JSONObject();

            try {
                response = new JSONObject(b.getString("response"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success){
                OnAsyncOperationSuccess(opId, response);
            }
            else {
                OnAsyncOperationError(opId, response);
            }

            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_HTML_CODE_VIDEO: {
                try {
                    long id = 0L;
                    if ((response.has("id")) && (response.get("id") != JSONObject.NULL)) {
                        id = response.getLong("id");

                        if ((response.has("page")) && (response.get("page") != JSONObject.NULL)) {

                            String pattern1 = "<meta  property=\"og:video:url\" content=\"";
                            String pattern2 = "\">";

                            String page = (response.getString("page"));

                            List<String> strings = UTILS.getStringsBetweenStrings(page, pattern1, pattern2);

                            for(TweetResultData t : data){
                                if(t.getId() == id){
                                    if(!strings.isEmpty()) {
                                        t.setVideoContent(strings.get(0));
                                    }
                                    t.setTriedToLoadVideo(true);

                                    videoRequestIds.remove(id);

                                    notifyDataSetChanged();
                                    return;
                                }
                            }
                        }

                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                }
            }

            case OP_GET_HTML_CODE_IMG: {
                try {
                    long id = 0L;
                    if ((response.has("id")) && (response.get("id") != JSONObject.NULL)) {
                        id = response.getLong("id");

                        if ((response.has("page")) && (response.get("page") != JSONObject.NULL)) {

                            String pattern1 = "<meta  property=\"og:image\" content=\"";
                            String pattern2 = "\">";

                            String page = (response.getString("page"));

                            List<String> strings = UTILS.getStringsBetweenStrings(page, pattern1, pattern2);

                            for(TweetResultData t : data){
                                if(t.getId() == id){
                                    if(!strings.isEmpty()) {
                                        t.setImgContent(strings.get(0));
                                    }
                                    t.setTriedToLoadImg(true);

                                    imgRequestIds.remove(id);

                                    notifyDataSetChanged();
                                    return;
                                }
                            }
                        }

                    }

                } catch (JSONException e) {
                    UTILS.DebugLog(TAG, e);
                }
            }
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_HTML_CODE_IMG:
            case OP_GET_HTML_CODE_VIDEO:
                UTILS.DebugLog("OP_GET_AUTH_TOKEN", response.toString());
                break;
        }
    }
}
