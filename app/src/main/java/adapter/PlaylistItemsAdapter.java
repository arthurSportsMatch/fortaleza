package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.MainActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.Audio;
import data.PlaylistItemData;
import utils.CONSTANTS;
import utils.UTILS;

public class PlaylistItemsAdapter extends RecyclerView.Adapter<PlaylistItemsAdapter.ViewHolder> {



    Context context;
    List<PlaylistItemData> data;
    Listener listener;

    public PlaylistItemsAdapter(Context context, List<PlaylistItemData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    public void addPlaylists (List<PlaylistItemData> nData){
        data.addAll(nData);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_playlist_item, null, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if(position >= data.size() -1){
            listener.LastItem();
        }

        final PlaylistItemData player = data.get(position);

        MainActivity.instance.addAudio(new Audio( player.getId(), UTILS.getHtmlContent(player.getAudio()), "São Paulo FC", player.getTitulo(), UTILS.formatDateFromDB(player.getDataHora())));

        holder.tittle.setText(player.getTitulo());

        holder.date.setText(UTILS.getDatePost(player.getDataHora()));

        holder.time.setText(player.getDuracao());

        UTILS.getImageAt(context, player.getImg(), holder.image,new RequestOptions()
                .transforms(new CenterCrop(), new RoundedCorners(20)));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //listener.onClick(player.getId());
                listener.onClick(position, player);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void LastItem();
        void onClick (int id);
        void onClick (int position, PlaylistItemData data);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView image;
        TextView tittle;
        TextView date;
        TextView time;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.img_content_image);
            tittle = itemView.findViewById(R.id.txt_tittle);
            date = itemView.findViewById(R.id.txt_date);
            time = itemView.findViewById(R.id.txt_time);
        }
    }
}
