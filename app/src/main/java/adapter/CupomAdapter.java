package adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.sportm.fortaleza.R;

import java.util.List;

import data.VoucherItemData;

public class CupomAdapter extends BaseAdapter {

    Context context;
    private List<Bitmap> list;
    private List<VoucherItemData> itens;
    private Handler myHandler = new Handler();
    private SpinRunnable mRunnable;
    Interaction listener;
    private int cupons;
    int cuponAdd = -1;

    long DURATION = 100;
    private boolean on_attach = true;

    Animation animSlideLeft, animSlideRight, animFadeIn;
    private String animState = "none";

    public int getCupons() {
        return cupons;
    }

    public void setCupons(int cupons) {
        this.cupons = cupons;
        this.animState = "none";
        notifyDataSetChanged();
    }

    public String getAnimState() {
        return animState;
    }

    public void setAnimState(String animState) {
        this.animState = animState;
        notifyDataSetChanged();
    }

    public int getCuponAdd() {
        return cuponAdd;
    }

    public void setCuponAdd(int cuponAdd) {
        this.cuponAdd = cuponAdd;
        //this.notifyDataSetChanged();
    }

    public CupomAdapter(Context context, List<Bitmap> list, List<VoucherItemData> itens, int cupons, Interaction listener) {
        this.context = context;
        this.list = list;
        this.itens = itens;
        this.cupons = cupons;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        initValues();

        View view = convertView;
        if(convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_cupon_list, null);
            FadeIN(view, position, 0.0f, 100);
        }

        //imagem cortada
        View holder = view.findViewById(R.id.holder_image_back);
        ImageView iconPresente = view.findViewById(R.id.img_icon_presente);

        ImageView imageBitmap = view.findViewById(R.id.image_view_back);
        imageBitmap.setImageBitmap(list.get(position));

        //parte de traz
        ImageView imageIcons = view.findViewById(R.id.image_view);
        imageIcons.setImageResource(R.drawable.icon_cupom);

        //Setagem dos icones
        if(itens != null && itens.size() > 0){
            for(int i = 0; i < itens.size(); i ++){
                if(position == (itens.get(i).getQtd()-1)){

                    imageIcons.setImageResource(R.drawable.icon_cupom_premio);
                    iconPresente.setVisibility(View.VISIBLE);

                    final int finalI = i;
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.OnClick(finalI);
                        }
                    });
                }
            }
        }

        //Setagem dos premios
        if(position <= (cupons -1)){
            if(position != cuponAdd-1){
                holder.setVisibility(View.VISIBLE);
                imageIcons.setVisibility(View.GONE);
            }
        }else{
            holder.setVisibility(View.GONE);
            imageIcons.setVisibility(View.VISIBLE);
        }

        //Animação do cupon que acabou de ser adicionado
        if(cuponAdd != -1){
            if(position == cuponAdd-1){
                Log.d("Anim-Adapter", "Spin");
                mRunnable = new SpinRunnable(holder, imageIcons, position);
                myHandler.postDelayed(mRunnable, 800);
            }else{
                FadeOUT(view, position, 20);
            }

        }else{
            if(animState == "lastAlpha"){
                if(position == cupons-1){
                    view.setAlpha(1f);
                }else{
                    FadeIN(view, position, 0.4f, 20);
                }
            }
        }

        //Ultimo a ser criado
        if(position >= list.size()-1){

            if(animState == "firtAlpha"){
                Log.d("Anim-Adapter", "FirtAlpha");
                listener.AnimAlpha();
            }

            if(animState == "lastAlpha"){
                Log.d("Anim-Adapter", "LastAlpha");
                listener.AnimLast();
            }
        }

        return view;
    }

    public interface Interaction {
        void OnClick (int position);
        void AnimAlpha ();
        void AnimSpin ();
        void AnimLast ();
    }

    void initValues (){
        animSlideLeft = AnimationUtils.loadAnimation(context, R.anim.slide_left);
        animSlideRight = AnimationUtils.loadAnimation(context, R.anim.slide_righ);
        animFadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
    }

    //Animation fadeIN
    private void FadeIN(View itemView, int i, float firtAlpha, long DURATION) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(firtAlpha);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", firtAlpha, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", firtAlpha).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 2 : (i * DURATION / 3));
        animator.setDuration(500);
        animatorSet.play(animator);
        animator.start();
    }

    //Animation fadeOut
    private void FadeOUT(View itemView, int i, long DURATION) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(1.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 1.f, 0.4f);
        ObjectAnimator.ofFloat(itemView, "alpha", 1.0f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 2 : (i * DURATION / 3));
        animator.setDuration(500);
        animatorSet.play(animator);
        animator.start();
    }

    //Usado para realizar a animação
    class SpinRunnable implements Runnable {
        View ivBack;
        ImageView ivFront;
        int position;

        public SpinRunnable(View back, ImageView front, int position) {
            this.ivBack = back;
            this.ivFront = front;
            this.position = position;
        }

        @Override
        public void run() {
            Animation slideRight = AnimationUtils.loadAnimation(context, R.anim.slide_spin_in);
            slideRight.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    ivFront.setVisibility(View.GONE);

                    ivBack.setVisibility(View.VISIBLE);
                    Animation slideLeft = AnimationUtils.loadAnimation(context, R.anim.slide_spin_off);
                    slideLeft.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            listener.AnimSpin();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    ivBack.startAnimation(slideLeft);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            ivFront.startAnimation(slideRight);
        }
    }
}
