package adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.List;

import data.IngredienteData;
import data.ReceitaIngredientesAdapter;
import data.RecipeCategoriasData;
import utils.UTILS;

public class ReceitaCategoriaAdapter extends ArrayAdapter<RecipeCategoriasData> {

    ViewHolder mHolder;
    Interaction listener;
    ReceitaIngredientesAdapter adapter;
    List<RecipeCategoriasData> objects;
    List<IngredienteData> backup;
    int iddBackup = -1;


    public ReceitaCategoriaAdapter(@NonNull Context context, int resource, @NonNull List<RecipeCategoriasData> objects, Interaction listener) {
        super(context, resource, objects);
        this.objects = objects;
        this.listener = listener;
    }

    public void setBackup (int id, List<IngredienteData> data){
        this.iddBackup = id;
        this.backup = data;
        notifyDataSetChanged();
    }

    List<IngredienteData> createCopy (List<IngredienteData> original){
        ArrayList list = new ArrayList(original.size());

        for(IngredienteData obj : original){
            list.add(new IngredienteData(obj.getId(), obj.getIng(), obj.getPrc(), obj.getQtd(), obj.getTotal()));
        }
        return list;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final RecipeCategoriasData recipe = getItem(position);
        final boolean islast = (position == (getCount() - 1));

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_receita_categoria, parent, false);
        }

        mHolder = new ViewHolder(convertView);
        mHolder.name.setText(recipe.getCat());

        mHolder.reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter != null){
                    listener.ResetValues(position);
                }else{

                }
            }
        });

        if(iddBackup == position){
            adapter = new ReceitaIngredientesAdapter(getContext(), R.layout.adapter_calculadora_valores, backup, new ReceitaIngredientesAdapter.Interaction() {
                @Override
                public void CreatedAll() {
                    if(islast){
                        listener.CreatedAll();
                        Log.d("Backup-created-1", "position: " + position + " size: " + getCount());
                    }
                }

                @Override
                public void UpdateValues(IngredienteData data, int pos) {
                    recipe.getIngs().set(pos, data);
                    listener.UpdateValue(recipe, position);
                    dataChange();
                }

                @Override
                public void OnClick(int pos) {

                }
            });
            mHolder.listView.setAdapter(adapter);
            UTILS.setListViewHeightBasedOnItems(mHolder.listView);

            float total = getTotal(backup);
            String pressUn = String.format("%.2f" , total);
            mHolder.result.setText("Total: R$ " + pressUn);

        }else{
            adapter = new ReceitaIngredientesAdapter(getContext(), R.layout.adapter_calculadora_valores, recipe.getIngs(), new ReceitaIngredientesAdapter.Interaction() {
                @Override
                public void CreatedAll() {
                    if(islast){
                        listener.CreatedAll();
                        Log.d("Backup-created-1", "position: " + position + " size: " + getCount());
                    }
                }

                @Override
                public void UpdateValues(IngredienteData data, int pos) {
                    recipe.getIngs().set(pos, data);
                    listener.UpdateValue(recipe, position);
                    dataChange();
                }

                @Override
                public void OnClick(int pos) {

                }
            });
            mHolder.listView.setAdapter(adapter);
            UTILS.setListViewHeightBasedOnItems(mHolder.listView);

            float total = getTotal(recipe.getIngs());
            String pressUn = String.format("%.2f" , total);
            mHolder.result.setText("Total: R$ " + pressUn);
        }

        //listener.CreatedAll();

        return convertView;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    boolean posting = false;
    Handler handler = new Handler();

    Runnable runnable = new Runnable(){
        public void run() {
            notifyDataSetChanged();
            Log.d("Handler-handle", "run");
            posting = false;
        }
    };

    void dataChange (){
        final int interval = 1000; // 1 Second
        if (!posting){
            posting = true;
            handler.postAtTime(runnable, System.currentTimeMillis()+interval);
            handler.postDelayed(runnable, interval);
        }else{
            handler.removeCallbacks(runnable);
            posting = false;
            dataChange();
        }
    }

    //
    float getTotal (List<IngredienteData> ings){
        float result = 0;
        for(int i = 0; i < ings.size(); i++){
            result += ings.get(i).getTotal();
        }
        return result;
    }

    public interface Interaction {
        void CreatedAll ();
        void UpdateValue (RecipeCategoriasData data, int position);
        void OnClick (int position);
        void ResetValues (int position);
    }

    class ViewHolder {
        TextView name;
        Button reset;
        ListView listView;
        TextView result;

        public ViewHolder(View view){
            name = view.findViewById(R.id.txt_receita_name);
            reset = view.findViewById(R.id.btn_reset);
            listView = view.findViewById(R.id.scroll_receita_values);
            result = view.findViewById(R.id.txt_receita_total_custo);
        }
    }
}
