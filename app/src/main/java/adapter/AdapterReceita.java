package adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.AdapterReceitaData;
import data.FragmentData;
import fragments.ReceitaFragment;
import utils.CONSTANTS;
import utils.UTILS;

public class AdapterReceita extends RecyclerView.Adapter<AdapterReceita.ViewHolder>{

    List<AdapterReceitaData> list;
    Context context;
    int resource;
    Activity activity;
    boolean isRecipe = true;

    long DURATION = 150;
    private boolean on_attach = true;

    boolean vertical;

    public AdapterReceita(List<AdapterReceitaData> list, Context context, int resource) {
        this.list = list;
        this.context = context;
        this.resource = resource;
    }

    public AdapterReceita(List<AdapterReceitaData> list, Context context, int resource, Activity activity) {
        this.list = list;
        this.context = context;
        this.resource = resource;
        this.activity = activity;
    }

    public AdapterReceita(List<AdapterReceitaData> list, Context context, int resource, boolean isVertical) {
        this.list = list;
        this.context = context;
        this.resource = resource;
        this.vertical = isVertical;
    }

    public AdapterReceita(List<AdapterReceitaData> list, Context context, int resource, boolean isVertical, Activity activity) {
        this.list = list;
        this.context = context;
        this.resource = resource;
        this.vertical = isVertical;
        this.activity = activity;
    }

    public AdapterReceita(List<AdapterReceitaData> list, Context context, int resource, boolean isVertical, Activity activity, boolean isRecipe) {
        this.list = list;
        this.context = context;
        this.resource = resource;
        this.vertical = isVertical;
        this.activity = activity;
        this.isRecipe = isRecipe;
    }

    public boolean isVertical() {
        return vertical;
    }

    public void setVertical(boolean vertical) {
        this.vertical = vertical;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(vertical){
            holder.view.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        }

        if(list.get(position).getImg() != null){
            UTILS.getImageAt(context,list.get(position).getImg(), holder.imagem);
        }else{
            UTILS.getImageAt(context,R.drawable.svgbakers, holder.imagem);
        }


        if(list.get(position).getTitulo() != null)
            holder.titulo.setText(list.get(position).getTitulo());

        if(list.get(position).getDescricao() != null)
            holder.descricao.setText(list.get(position).getDescricao());

        String t = "";

        if(list.get(position).getCalc() != null){
            t = "CALCULADORA";
        }

        if(list.get(position).getPdf() != null){
            if(t.isEmpty()){
                t = "PDF";
            }else{
                t += "/PDF";
            }
        }

        holder.type.setText(t);
        if(!isRecipe){
            holder.goTreinamento(list.get(position).getId());
        }else{
            holder.goRecipe(list.get(position).getId());
        }


        FromLeftToRight(holder.view, position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        super.onAttachedToRecyclerView(recyclerView);
    }

    //Animation fadeIN
    private void FadeIN(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 2 : (i * DURATION / 3));
        animator.setDuration(500);
        animatorSet.play(animator);
        animator.start();
    }

    private void FromRightToLeft(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean not_first_item = i == -1;
        i = i + 1;
        itemView.setTranslationX(itemView.getX() + 400);
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(itemView, "translationX", itemView.getX() + 400, 0);
        ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(itemView, "alpha", 1.f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animatorTranslateY.setStartDelay(not_first_item ? DURATION : (i * DURATION));
        animatorTranslateY.setDuration((not_first_item ? 2 : 1) * DURATION);
        animatorSet.playTogether(animatorTranslateY, animatorAlpha);
        animatorSet.start();
    }

    private void FromLeftToRight(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean not_first_item = i == -1;
        i = i + 1;
        itemView.setTranslationX(-400f);
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(itemView, "translationX", -400f, 0);
        ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(itemView, "alpha", 1.f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animatorTranslateY.setStartDelay(not_first_item ? DURATION : (i * DURATION));
        animatorTranslateY.setDuration((not_first_item ? 2 : 1) * DURATION);
        animatorSet.playTogether(animatorTranslateY, animatorAlpha);
        animatorSet.start();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        View view;
        ImageView imagem;
        TextView titulo;
        TextView descricao;
        TextView type;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            imagem = itemView.findViewById(R.id.adapter_receita_image);
            titulo = itemView.findViewById(R.id.adapter_receita_title);
            descricao = itemView.findViewById(R.id.adapter_receita_description);
            type = itemView.findViewById(R.id.txt_receita_type);
        }

        public void goRecipe (final int id){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitaFragment(id), "Treinamento"), -1);
                    //new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_HOME_RECEITAVER, id).execute();
                }
            });
        }

        public void goTreinamento (final int id){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitaFragment(id, false), "Treinamento"), -1);
                    //new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_HOME_RECEITAVER, id).execute();
                }
            });
        }
    }
}
