package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import java.util.List;

import data.BannerData;
import informations.UserInformation;
import utils.CONSTANTS;
import utils.UTILS;

public class NoticiaAdapter extends RecyclerView.Adapter<NoticiaAdapter.ViewHolder> {

    Context context;
    List<BannerData> data;
    Listener listener;

    public NoticiaAdapter(Context context, List<BannerData> data, Listener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    public void AddMoreData (List<BannerData> nData){
        data.addAll(nData);
        notifyDataSetChanged();
    }

    public void ChangeData (List<BannerData> nData){
        data.removeAll(data);
        data.addAll(nData);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_noticia, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position >= data.size()-1){
            listener.OnLast();
        }

        final BannerData banner = data.get(position);
        UTILS.getImageAt(context, banner.getImg(), holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnCick(banner);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void OnCick(BannerData data);
        void OnLast ();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_adapterBanner);
        }
    }
}
