package adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import java.util.List;

import data.RankData;
import utils.CONSTANTS;
import utils.UTILS;

public class AdapterRankItem extends RecyclerView.Adapter<AdapterRankItem.ViewHolder> {

    Context context;
    List<RankData> data;
    Listener listener;
    int userId = -1;

    public AdapterRankItem(Context context, List<RankData> data) {
        this.context = context;
        this.data = data;
    }

    public AdapterRankItem(Context context, List<RankData> data, int user, Listener listener) {
        this.context = context;
        this.data = data;
        this.userId = user;
        this.listener = listener;
    }

    public void newData (List<RankData> rank, int id){
        data.removeAll(data);

        if(rank != null)
            data.addAll(rank);

        userId = id;
        notifyDataSetChanged();
    }

    public void addData (List<RankData> rank, int id){
        if(rank != null)
            data.addAll(rank);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(context).inflate(R.layout.adapter_rank_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position >= data.size()-1){
            listener.onLastItem();
        }

        RankData user = data.get(position);

        UTILS.getImageAt(context,user.getImg(), holder.userImage,new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_placeholderuser)
                .error(R.drawable.ic_placeholderuser));

        holder.userPos.setText(String.valueOf(user.getRank()));
        holder.userNome.setText(user.getNome());

        if(user.getId() != userId){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.userPos.getBackground().setTint(context.getResources().getColor(R.color.colorGray));
            } else{
                holder.userPos.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGray),
                        PorterDuff.Mode.SRC_ATOP);
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.userPos.getBackground().setTint(context.getResources().getColor(R.color.colorRedCard));
            }else{
                holder.userPos.getBackground().setColorFilter(context.getResources().getColor(R.color.colorRedCard),
                        PorterDuff.Mode.SRC_ATOP);
            }
        }

        if(user.getSubtitle() != null){
            holder.userNivel.setText(user.getSubtitle());
        }else{
            holder.userNivel.setText("Torcedor");
        }

        holder.userPontos.setText(String.valueOf(user.getScore()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Listener {
        void onLastItem ();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView userPos;
        ImageView userImage;
        TextView userNome;
        TextView userNivel;
        TextView userPontos;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userPos = itemView.findViewById(R.id.txt_pos);
            userImage = itemView.findViewById(R.id.img_photo);
            userNome = itemView.findViewById(R.id.txt_nome);
            userNivel = itemView.findViewById(R.id.txt_nivel);
            userPontos = itemView.findViewById(R.id.txt_pontos);
        }
    }
}
