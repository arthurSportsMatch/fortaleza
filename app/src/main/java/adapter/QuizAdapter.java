package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.text.DecimalFormat;
import java.util.List;

import data.QuizData;
import data.RespostasData;
import utils.CONSTANTS;
import utils.UTILS;

public class QuizAdapter  extends RecyclerView.Adapter<QuizAdapter.ViewHolder> {

    Context context;
    List<QuizData> data;

    public QuizAdapter(Context context, List<QuizData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_card_pesquisa_home, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        QuizData quiz = data.get(position);

        holder.txtTitle.setText(quiz.getGrupo());
        holder.txtDescription.setText(quiz.getPergunta());


        UTILS.getImageAt(context, quiz.getImg(), holder.background);

        int pos = position % 3;
        int max = 0;

//        switch (pos){
//            case 0 :{
//                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonBlackTransp));
//                holder.txtTitle.setTextColor(context.getResources().getColor(R.color.colorWhite));
//                holder.txtDescription.setTextColor(context.getResources().getColor(R.color.colorWhite));
//
//            }
//            break;
//            case 1 :{
//                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonWhiteTransp));
//                holder.txtTitle.setTextColor(context.getResources().getColor(R.color.colorBlackLetter));
//                holder.txtDescription.setTextColor(context.getResources().getColor(R.color.colorBlackLetter));
//            }
//            break;
//            case 2 :{
//                holder.foreground.setBackgroundColor(context.getResources().getColor(R.color.colorButtonRedTransp2));
//                holder.txtTitle.setTextColor(context.getResources().getColor(R.color.colorWhite));
//                holder.txtDescription.setTextColor(context.getResources().getColor(R.color.colorWhite));
//            }
//            break;
//        }

        for(RespostasData r : quiz.getRespostas()){
            max += r.getQtd();
        }

        for(RespostasData r : quiz.getRespostas()){
            View v = LayoutInflater.from(context).inflate(R.layout.adapter_answer, null, false);

            ProgressBar progressBar = v.findViewById(R.id.progress_answer);
            TextView tittle = v.findViewById(R.id.txt_answer);
            TextView porc = v.findViewById(R.id.txt_porc);

            tittle.setText(r.getResposta());

            progressBar.setVisibility(View.VISIBLE);
            progressBar.setMax(max);
            progressBar.setProgress(r.getQtd());

            float porcentagem = (((float)r.getQtd() / max) * 100);
            DecimalFormat formater = new DecimalFormat("##.##");
            String porcenDecimal = formater.format(porcentagem);
            String finalPorcentagem = porcenDecimal + "%";//(Math.round(porcentagem)  + "%");

            Log.d("Home", "quant: " + r.getQtd() + " max: " + max + "porc" + Math.round(porcentagem)  + "%");
            porc.setVisibility(View.VISIBLE);
            porc.setText(finalPorcentagem);

            holder.viewResults.addView(v);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitle;
        TextView txtDescription;
        ImageView background;
        ImageView foreground;

        LinearLayout viewResults;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            background = itemView.findViewById(R.id.img_pesquisa_back);
            foreground = itemView.findViewById(R.id.img_foreground);
            txtTitle = itemView.findViewById(R.id.txt_nome_quiz);
            txtDescription = itemView.findViewById(R.id.txt_description_quiz);
            viewResults = itemView.findViewById(R.id.view_results_quiz);
        }
    }
}
