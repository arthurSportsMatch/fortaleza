package adapter;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import data.FragmentData;
import utils.FragmentHolder;
import view.DynamicHeightViewPager;

public class PageAdapter extends FragmentStatePagerAdapter {

    private Fragment mCurrentFragment;
    private FragmentManager mFragmentManager;
    private int mCurrentPosition = 0;
    private List<FragmentHolder> list;


    public List<FragmentHolder> getList() {
        return list;
    }

    public void setList(List<FragmentHolder> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setFragment (FragmentData fragment, int position){
        FragmentHolder mlist = new FragmentHolder(fragment);
        mFragmentManager.beginTransaction().remove(list.get(position).getFragment()).commitAllowingStateLoss();
        this.list.remove(position);
        this.list.add(position, mlist);
        notifyDataSetChanged();
    }

    public String getFragmentName (int position){
        return list.get(position).getFragmentData().getName();
    }

    public void addFragment (FragmentData fragment, int actualPosition){
        mFragmentManager.beginTransaction().remove(list.get(actualPosition).getFragment()).commitAllowingStateLoss();
        this.list.get(actualPosition).addFragment(fragment);
        notifyDataSetChanged();
    }

    public void removeFragment (int position){
        mFragmentManager.beginTransaction().remove(list.get(position).getFragment()).commitAllowingStateLoss();
        this.list.get(position).removeFragment(list.get(position).getFragmentData());
        notifyDataSetChanged();
    }

    public Fragment getmCurrentFragment() {
        return mCurrentFragment;
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public PageAdapter(FragmentManager fm, List<FragmentHolder> fragmentList) {
        super(fm);
        this.mFragmentManager = fm;
        this.list = fragmentList;
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public int getmCurrentPosition() {
        return mCurrentPosition;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position).getFragment();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;//super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }

        if (position != mCurrentPosition && container instanceof DynamicHeightViewPager) {
            Fragment fragment = (Fragment) object;
            DynamicHeightViewPager pager = (DynamicHeightViewPager) container;

            if (fragment.getView() != null) {
                mCurrentPosition = position;
                pager.measureCurrentView(fragment.getView());
            }
        }

        super.setPrimaryItem(container, position, object);
    }
}
