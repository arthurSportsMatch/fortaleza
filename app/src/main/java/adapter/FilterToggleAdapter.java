package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.List;

import data.FilterData;

public class FilterToggleAdapter extends RecyclerView.Adapter<FilterToggleAdapter.ViewHolder> {

    List<FilterData> filters;
    Context context;
    int resource;

    public FilterToggleAdapter(List<FilterData> filters, Context context, int resource) {
        this.filters = filters;
        this.context = context;
        this.resource = resource;
    }

    //Seleciona ou não os filtros
    public void setToggle (boolean value){
        if(filters.size() > 0){
            for(FilterData data : filters){
                data.setSelected(value);
            }
        }
        notifyDataSetChanged();
    }

    //Pega apenas os filtros que foram selecionados
    public List<FilterData> getToggled (){
        List<FilterData> toggled = new ArrayList<>();
        for(FilterData data : filters){
            if(data.isSelected()){
                toggled.add(data);
            }
        }
        return toggled;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(resource, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        FilterData data = filters.get(position);

        //Tem Produto
        if(data.getProduto() != null){
            holder.name.setText(data.getProduto());
        }
        //Tem Segmento
        else if(data.getSegmento() != null){
            holder.name.setText(data.getSegmento());
        }
        //Se não tiver nada
        else{
            holder.name.setVisibility(View.GONE);
        }

        holder.button.setChecked(data.isSelected());

        holder.button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filters.get(position).setSelected(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox button;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.ratio_toggle_check);
            name = itemView.findViewById(R.id.txt_toggle_name);
        }
    }
}
