package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import utils.CONSTANTS;
import utils.UTILS;

public class NextGameAdapter extends RecyclerView.Adapter<NextGameAdapter.ViewHolder> {

    Context context;
    List<PartidaData> data;
    int resorce;

    public NextGameAdapter(Context context, List<PartidaData> data, int resorce) {
        this.context = context;
        this.data = data;
        this.resorce = resorce;
    }

    @NonNull
    @Override
    public NextGameAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(resorce, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Get position
        PartidaData partida = data.get(position);

        //Imagem primeiro time
        UTILS.getImageAt(context, partida.getClubeImg1(), holder.image1);

        //Imagem primeiro time
        UTILS.getImageAt(context, partida.getClubeImg2(), holder.image2);

        holder.timer.setText(partida.getDataHora());

        holder.local.setText(partida.getEstadio());

//        if(partida.getType() != null && !partida.getType().isEmpty()){
//            holder.tipoPartida.setVisibility(View.VISIBLE);
//            holder.txtTipoPartida.setText(partida.getTitle());
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                holder.imgTipoPartida.getBackground().setTint(UTILS.parseColorByString(partida.getTypeColor()));
//            }
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public View tipoPartida;
        public TextView txtTipoPartida;
        public ImageView imgTipoPartida;

        public ImageView image1;    //imagem do primeiro time
        public ImageView image2;    //imagem do segundo time

        public TextView timer;      //horario do jogo
        public TextView local;      //local do jogo

        public View tvs;            //para colocar lista de imagens

        public ViewHolder(View itemView) {
            super(itemView);
            tipoPartida = itemView.findViewById(R.id.view_ag_tipo);
            txtTipoPartida = itemView.findViewById(R.id.txt_age_tipo);
            imgTipoPartida = itemView.findViewById(R.id.img_age_tipo);

            image1 = itemView.findViewById(R.id.img_time_1);
            image2 = itemView.findViewById(R.id.img_time_2);

            timer = itemView.findViewById(R.id.txt_jogo_time);
            local = itemView.findViewById(R.id.txt_jogo_local);

            tvs = itemView.findViewById(R.id.view_where_watch);
        }
    }
}
