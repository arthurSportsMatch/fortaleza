package fragments;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sportm.fortaleza.MainActivity;
import com.sportm.fortaleza.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import adapter.ReceitaCategoriaAdapter;
import data.CalculadoraData;
import data.IngredienteData;
import data.RecipeCategoriasData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;


public class CalculadoraFragment extends Fragment implements AsyncOperation.IAsyncOpCallback{

    ViewHolder mHolder;

    //para teste
    int recipeID = 383;     // ID da receita
    int recipeQuant = 1;    // Quantidade de porções



    float recipeUn;         // Valor da unidade da receita
    float recipeKG;         // Valor da receita por quilo
    float recipeTotal;      // Valor total de receita Total

    CalculadoraData calculadoraData = new CalculadoraData();
    List<RecipeCategoriasData> calculadoraBack = new ArrayList<>();

    ReceitaCategoriaAdapter adapter;


    public CalculadoraFragment() {
    }

    public CalculadoraFragment(int id) {
        this.recipeID = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculadora, container, false);

        mHolder = new ViewHolder(view);
        getRecipeByID(recipeID);

        initValues();
        initAction();

        //((MainMenuActivity) getActivity()).changeToolbarName("Calculadora");

        return view;
    }

    void initValues (){
        Log.d("Backup-creating", "initValues");
        recipeKG = getRecipeKg(calculadoraData.getCategorias());
        recipeUn = getRecipeUn(calculadoraData.getCategorias());
        recipeTotal = recipeUn * recipeQuant;
        int recipeRendimento = calculadoraData.getPorc() * recipeQuant;

        mHolder.recipeKg.setText("R$ " + String.format("%.2f",recipeKG));
        mHolder.recipeUn.setText("R$ " + String.format("%.2f",recipeUn));
        mHolder.recipeTotal.setText("R$ " + String.format("%.2f" , recipeTotal));
        mHolder.recipePorcao.setText(recipeQuant + " Receita");
        mHolder.recipeRendimento.setText("Rendimento " + recipeRendimento);
    }

    void initAction (){

        mHolder.recipePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_AUMENTARMUTIPLICADOR).execute();
                recipeQuant ++;
                initValues();
            }
        });

        mHolder.recipeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recipeQuant > 1){
                    new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_DIMINUIRMUTIPLICADOR).execute();
                    recipeQuant --;
                    initValues();
                }
            }
        });

        mHolder.recipeRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_REINICIAR).execute();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Realmente deseja restaurar a receita?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_REINICARSIM).execute();
                                getRecipeByID(recipeID);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_REINICARNAO).execute();
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });
    }

    void getRecipeByID (int id){
        Hashtable<String, Object> _params = new Hashtable<>();
        _params.put("id", id);

        //TASK_ID_GET_RECIPE para pegar receita
        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_CALCULATOR, 0,this).execute(_params);
    }

    float getRecipeKg (List<RecipeCategoriasData> list){
        float result = 0;
        if(list != null){
            for(int i = 0; i < list.size(); i++){
                for(int y = 0; y < list.get(i).getIngs().size(); y++ ){
                    result += list.get(i).getIngs().get(y).getPrc();
                }
            }
        }

        return result;
    }

    float getRecipeUn (List<RecipeCategoriasData> list){
        float result = 0;
        if(list != null) {
            for (int i = 0; i < list.size(); i++) {
                for (int y = 0; y < list.get(i).getIngs().size(); y++) {
                    result += list.get(i).getIngs().get(y).getTotal();
                }
            }
        }
        return result;
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });


    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        try {
            if(response.has("Object")){
                Gson gson = new Gson();
                calculadoraData = gson.fromJson(response.get("Object").toString(), CalculadoraData.class);
                calculadoraBack = gson.fromJson(response.get("Object").toString(), CalculadoraData.class).getCategorias();
                Log.d("Backup-creating", "Criando");
                adapter = new ReceitaCategoriaAdapter(getContext(), R.layout.adapter_receita_categoria, createCopy(calculadoraData.getCategorias()), new ReceitaCategoriaAdapter.Interaction() {
                    @Override
                    public void CreatedAll() {
                        Log.d("Backup-calculadora", "data: " + calculadoraData.toString());
                        initValues();
                    }

                    @Override
                    public void UpdateValue(RecipeCategoriasData data, int position) {
                        new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_EDITARITEM).execute();
                        calculadoraData.getCategorias().set(position, data);
                        //initValues();
                    }

                    @Override
                    public void OnClick(int position) {

                    }

                    @Override
                    public void ResetValues(final int position) {
                        new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_RESGATARGRUPO).execute();
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Realmente deseja restaurar grupo?")
                                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_RESGATARGRUPOSIM).execute();
                                        adapter.setBackup(position, createCopyIng(calculadoraBack.get(position).getIngs()));
                                        calculadoraData.getCategorias().get(position).setIngs(createCopyIng(calculadoraBack.get(position).getIngs()));
                                        //initValues();
                                    }
                                })
                                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_CALCULADORA_RESGATARGRUPONAO).execute();
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = alertDialog.create();
                        alert.show();
                    }
                });
                mHolder.categorias.setAdapter(adapter);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }

    List<RecipeCategoriasData> createCopy (List<RecipeCategoriasData> original){
        ArrayList list = new ArrayList(original.size());

        for(RecipeCategoriasData obj : original){
            list.add(new RecipeCategoriasData(obj.getIdcat(), obj.getCat(), obj.getIngs()));
        }
        return list;
    }

    List<IngredienteData> createCopyIng (List<IngredienteData> original){
        ArrayList list = new ArrayList(original.size());

        for(IngredienteData obj : original){
            list.add(new IngredienteData(obj.getId(), obj.getIng(), obj.getPrc(), obj.getQtd(), calculoUni(obj.getQtd(), obj.getPrc())));
        }
        return list;
    }

    float calculoUni (float quant, float prec){
        float realQuant = quant / 1000;
        float result = prec *    realQuant;
        return result;
    }

    class ViewHolder {
        ListView categorias;

        //Adicionar
        View recipeBottomMenu;
        Button recipePlus;
        Button recipeMinus;
        TextView recipePorcao;
        TextView recipeRendimento;

        ImageView recipeRestart;

        //Valores
        TextView recipeUn;
        TextView recipeKg;
        TextView recipeTotal;

        public ViewHolder (View view){

            categorias = view.findViewById(R.id.list_calculadora);

            recipeBottomMenu = view.findViewById(R.id.receitas_menu_inferior);
            recipePlus = view.findViewById(R.id.btn_receita_plus);
            recipeMinus = view.findViewById(R.id.btn_receita_minus);
            recipePorcao = view.findViewById(R.id.txt_receita_porcao);
            recipeRendimento = view.findViewById(R.id.txt_receita_rendimento);

            recipeRestart = view.findViewById(R.id.img_receita_refresh);

            recipeUn = view.findViewById(R.id.txt_receita_un);
            recipeKg = view.findViewById(R.id.txt_receita_kg);
            recipeTotal = view.findViewById(R.id.txt_receita_total);
        }
    }

}
