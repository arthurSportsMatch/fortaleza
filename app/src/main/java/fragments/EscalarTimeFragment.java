package fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

import com.sportm.fortaleza.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EscalarTimeFragment extends MainFragment {

    ViewHolder mHolder;
    CampoFragment campo;

    public EscalarTimeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_escalar_time, container, false);
        mHolder = new ViewHolder(view);
        initCampo();
        return view;
    }

    void initCampo (){
        campo = new CampoFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(mHolder.fragmentLayout.getId(), campo);
        transaction.commit();
    }

    void ChangeEscala (String id){
        campo.changeEsquema(id);
    }
    class ViewHolder {
        RadioGroup radioGroup;
        FrameLayout fragmentLayout;
        Button btnContinue;

        public ViewHolder (View view){
            radioGroup = view.findViewById(R.id.radio_escalas);
            fragmentLayout = view.findViewById(R.id.frame_campo);
            btnContinue = view.findViewById(R.id.btn_concluir);
        }
    }
}
