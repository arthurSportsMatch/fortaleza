package fragments;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import utils.UILoadingController;

public class MainFragment extends Fragment {

    protected Activity activity;
    protected String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private boolean mUserSeen = false;
    private boolean mViewCreated = false;
    boolean valuesInitialized = false;
    UILoadingController uiLoadingController;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!mUserSeen && isVisibleToUser) {
            mUserSeen = true;
            onUserFirstSight();
            tryViewCreatedFirstSight();
        }
        onUserVisibleChanged(isVisibleToUser);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity() != null){
            activity = getActivity();
        }
        setUserVisibleHint(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewCreated = true;
        tryViewCreatedFirstSight();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mViewCreated = false;
        mUserSeen = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
    }

    @Override
    public void onStop() {
        super.onStop();
        unbind();
    }

    private void tryViewCreatedFirstSight() {
        if (mUserSeen && mViewCreated) {
            onViewCreatedFirstSight(getView());
        }
    }

    public void onReload (){

    };

    public void onBackPressed() {

    }

    public void bind (){

    }

    public void unbind (){

    }

    void createLoadingController (View loadingView){
        uiLoadingController = new UILoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
            }
        });
    }

    void createLoadingController (View loadingView, UILoadingController.Listener listener){
        uiLoadingController = new UILoadingController(loadingView, listener);
    }

    public void errorToGetData (){
        uiLoadingController.finishLoader(false);
    }


    //vendo com a view já criada
    protected void onViewCreatedFirstSight(View view) {
        // handling here
    }

    //usuario viu, mas não se criou a view exatamente
    protected void onUserFirstSight() {
    }

    //não esta vendo maisMain
    protected void onUserVisibleChanged(boolean visible) {
    }

}
