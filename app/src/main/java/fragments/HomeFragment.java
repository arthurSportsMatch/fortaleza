package fragments;


import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.CentralNoticiasActivity;
import com.sportm.fortaleza.CheckInLoginActivity;
import com.sportm.fortaleza.CheckInMessageActivity;
import com.sportm.fortaleza.CheckinActivity;
import com.sportm.fortaleza.ConfigurationActivity;
import com.sportm.fortaleza.EditarPerfilActivity;
import com.sportm.fortaleza.EscalarTimeActivity;
import com.sportm.fortaleza.LojaActivity;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.MatchHistoryActivity;
import com.sportm.fortaleza.MediaPlayActivity;
import com.sportm.fortaleza.NavegadorActivity;
import com.sportm.fortaleza.NoticiaActivity;
import com.sportm.fortaleza.NotificationActivity;
import com.sportm.fortaleza.PerfilAtletaActivity;
import com.sportm.fortaleza.PlayerActivity;
import com.sportm.fortaleza.PlaylistActivity;
import com.sportm.fortaleza.QuizActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.RankingActivity;
import com.sportm.fortaleza.SocioTorcedorActivity;
import com.sportm.fortaleza.TimelineActivity;
import com.sportm.fortaleza.TwitterFeedCardExpandedActivity;
import com.google.android.gms.ads.AdSize;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import adapter.InstagramAdapter;
import adapter.LastResultAdapter;
import adapter.LojaVirtualItemAdapter;
import adapter.QuizAdapter;
import adapter.TweetFeedAdapter;
import data.BannerData;
import data.BannerLojaData;
import data.EscalacaoData;
import data.FragmentData;
import data.InstagramData;
import data.NotificationData;
import data.PartidaData;
import data.PlaylistItemData;
import data.ProdutoFData;
import data.QuizData;
import data.TweetResultData;
import data.TwitterFeedInformation;
import data.UserData;
import informations.UserInformation;
import models.ErrorModel;
import models.nextgames.NextGameModel;
import models.user.UserModel;
import pager.BannerAdapter;
import pager.NextGamePageAdapter;
import repository.audioteca.AudioTecaRepository;
import repository.lastgames.LastGamesRepository;
import repository.lojatime.LojaTimeRepository;
import repository.nextgames.NextGamesRepository;
import repository.noticia.NoticiaRepository;
import repository.pesquisa.PesquisaRepository;
import repository.quiz.QuizRepository;
import repository.stories.StoriesRepository;
import repository.twitter.TwitterRepository;
import repository.user.UserRepository;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MasterBakersNotificationManager;
import utils.TRACKING;
import utils.UTILS;
import view.DynamicTextView;
import viewpresenter.AudioHomePresenter;
import viewpresenter.CampoPresenter;
import viewpresenter.LastGamesPresenter;
import viewpresenter.LojaTimePresenter;
import viewpresenter.NextGamesPresenter;
import viewpresenter.NoticiaPresenter;
import viewpresenter.PesquisaPresenter;
import viewpresenter.QuizHomePresenter;
import viewpresenter.StoriesPresenter;
import viewpresenter.TwitterPresenter;
import viewpresenter.UserHeaderPresenter;
import vmodels.audioteca.AudioTecaViewModel;
import vmodels.fragment.FragmentHolderViewModel;
import vmodels.lastgames.LastGamesViewModel;
import vmodels.lojatime.LojaTimeViewModel;
import vmodels.nextgames.NextGamesViewModel;
import vmodels.noticia.NoticiaViewModel;
import vmodels.pesquisa.PesquisaViewModel;
import vmodels.quiz.QuizViewModel;
import vmodels.stories.StoriesViewModel;
import vmodels.twitter.TwitterViewModel;
import vmodels.user.UserViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;
import static com.sportm.fortaleza.MainActivity.prefs;

public class HomeFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    private ViewHolder mHolder; //ViewHolder que contém todas as views desse fragment
    private UserViewModel userViewModel;
    private NextGamesViewModel nextGamesViewModel;
    private LastGamesViewModel lastGamesViewModel;
    private NoticiaViewModel noticiaViewModel;
    private TwitterViewModel twitterViewModel;
    private StoriesViewModel storiesViewModel;
    private LojaTimeViewModel lojaTimeViewModel;
    private PesquisaViewModel pesquisaViewModel;
    private AudioTecaViewModel audioTecaViewModel;
    private QuizViewModel quizViewModel;
    private FragmentHolderViewModel mfragmentViewModel;

    private static final int OP_ID_CAMPO                = 4;
    private static final int OP_ID_BANNER               = 12;


    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mHolder = new ViewHolder(view);

        LinearLayout mAdView = view.findViewById(R.id.adView);
        AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.HOME_TOP_BANNER_ID);
        GoTop();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHolder.firtView.setVisibility(View.VISIBLE);
        initAsyncViews();
        GoTop();
    }

    @Override
    public void onReload() {
        super.onReload();
        initAsyncViews();
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_RELOAD).execute();
    }

    @Override
    public void bind() {
        super.bind();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, this::uploadUser);
        userViewModel.getUser();

        NextGamesRepository nextGamesRepository = new NextGamesRepository(activity);
        nextGamesViewModel = new ViewModelProvider(requireActivity()).get(NextGamesViewModel.class);
        nextGamesViewModel.setmRepository(nextGamesRepository);
        nextGamesViewModel.nextGames().observe(this, new Observer<NextGameModel>() {
            @Override
            public void onChanged(NextGameModel games) {
                initNextGames(games.getPartidaData(), games.getHide());
            }
        });

        LastGamesRepository lastGamesRepository = new LastGamesRepository(activity);
        lastGamesViewModel = new ViewModelProvider(requireActivity()).get(LastGamesViewModel.class);
        lastGamesViewModel.setmRepository(lastGamesRepository);
        lastGamesViewModel.lastGames().observe(this, this::initLastResults);

        //Noticias
        NoticiaRepository noticiaRepository = new NoticiaRepository(activity);
        noticiaViewModel = new ViewModelProvider(requireActivity()).get(NoticiaViewModel.class);
        noticiaViewModel.setmRepository(noticiaRepository);
        noticiaViewModel.noticia().observe(this, this::initBannerNews);

        //Audio Teca
        AudioTecaRepository audioTecaRepository = new AudioTecaRepository(activity);
        audioTecaViewModel = new ViewModelProvider(requireActivity()).get(AudioTecaViewModel.class);
        audioTecaViewModel.setmRepository(audioTecaRepository);
        audioTecaViewModel.audio().observe(this, this::initAudio);

        //Twitter
        TwitterRepository twitterRepository = new TwitterRepository(activity, prefs);
        twitterViewModel = new ViewModelProvider(requireActivity()).get(TwitterViewModel.class);
        twitterViewModel.setmRepository(twitterRepository);
        twitterViewModel.tweets().observe(this, this::configureTwitterFeedSup);

        //Stories
        StoriesRepository storiesRepository = new StoriesRepository(activity);
        storiesViewModel = new ViewModelProvider(requireActivity()).get(StoriesViewModel.class);
        storiesViewModel.setmRepository(storiesRepository);
        storiesViewModel.stories().observe(this, this::initStories);

        //Pesquisas
        PesquisaRepository pesquisaRepository = new PesquisaRepository(activity);
        pesquisaViewModel = new ViewModelProvider(requireActivity()).get(PesquisaViewModel.class);
        pesquisaViewModel.setmRepository(pesquisaRepository);
        pesquisaViewModel.lastPesquisas().observe(this, this::initLastSearch);

        //Loja
        LojaTimeRepository lojaTimeRepository = new LojaTimeRepository(activity);
        lojaTimeViewModel = new ViewModelProvider(requireActivity()).get(LojaTimeViewModel.class);
        lojaTimeViewModel.setmRepository(lojaTimeRepository);
        lojaTimeViewModel.produtos().observe(this, this::initLojaFortaleza);

        QuizRepository quizRepository = new QuizRepository(activity);
        quizViewModel = new ViewModelProvider(requireActivity()).get(QuizViewModel.class);
        quizViewModel.setmRepository(quizRepository);
        quizViewModel.quizHome().observe(this, this::initQuiz);
        quizViewModel.getError().observe(this, new Observer<ErrorModel>() {
            @Override
            public void onChanged(ErrorModel errorModel) {
                initNoQuiz();
            }
        });

        mfragmentViewModel = new ViewModelProvider(requireActivity()).get(FragmentHolderViewModel.class);
    }

    @Override
    public void unbind() {
        super.unbind();
        userViewModel.getUserData().removeObservers(this);
        lastGamesViewModel.lastGames().removeObservers(this);
        nextGamesViewModel.nextGames().removeObservers(this);
        audioTecaViewModel.audio().removeObservers(this);
        noticiaViewModel.noticia().removeObservers(this);
        twitterViewModel.tweets().removeObservers(this);
        lojaTimeViewModel.produtos().removeObservers(this);
        storiesViewModel.stories().removeObservers(this);
        storiesViewModel.getError().removeObservers(this);
        pesquisaViewModel.lastPesquisas().removeObservers(this);
        pesquisaViewModel.getError().removeObservers(this);
    }

    //Sobe a view inteira
    public void GoTop (){
        mHolder.scrollHome.fullScroll(ScrollView.FOCUS_UP);
    }

    void initAsyncViews (){
        getLoja();
        getCampo();
    }

    void getLoja (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_BANNER_LOJA, OP_ID_BANNER, this).execute();
    }
    void getCampo (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_ESCALACAO_HOME, OP_ID_CAMPO, this).execute();
    }

    //Inicia header home
    void uploadUser (UserModel user){
        new UserHeaderPresenter(activity, R.layout.include_home_user_header, (ViewGroup) mHolder.holderUserHeader, user, new UserHeaderPresenter.Listener() {
            @Override
            public void OnNotificationClick() {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_NOTIFICACAO).execute();
                Intent intent = new Intent(activity, NotificationActivity.class);
                activity.startActivityForResult(intent, 762);
            }
        });
    }

    //Inicia os Banners
    void initBannerNews (List<BannerData> data){
        if(data !=null && data.size() > 0)
            Log.d("Async", "has banner");
        if(UserInformation.getHide() <= 0) {
            new NoticiaPresenter(activity, R.layout.include_home_noticias, (ViewGroup) mHolder.holderNoticia, data);
        }
    }

    //Loja Time
    void initLojaFortaleza (List<ProdutoFData> data){
        if(UserInformation.getHide() <= 0) {
            new LojaTimePresenter(activity, R.layout.include_home_loja_time, (ViewGroup) mHolder.holderLojaTime, data);
        }
    }

    //Inicia Ultimos resultados
    void initLastResults (List<PartidaData> data){
        new LastGamesPresenter(activity, R.layout.include_home_lastgame, (ViewGroup) mHolder.holderLastGames, data);
    }

    //Inicia Proximos jogos
    void initNextGames (List<PartidaData> data, Integer hide){
        new NextGamesPresenter(activity,R.layout.include_home_nextgames, mfragmentViewModel, (ViewGroup) mHolder.holderNextGames, data, hide);
    }

    void initAudio (final PlaylistItemData data){
        new AudioHomePresenter(activity, R.layout.include_home_audio, (ViewGroup) mHolder.holderAudio, data);
    }

    //Configura a lista de twitts
    void configureTwitterFeedSup(List<TweetResultData> data){
        if(UserInformation.getHide() <= 0) {
            new TwitterPresenter(activity, R.layout.include_home_twitter, (ViewGroup) mHolder.holderTwitter, data);
        }
    }

    //Inicia o stories
    void initStories (List<InstagramData> data){
        if(UserInformation.getHide() <= 0) {
            new StoriesPresenter(activity, R.layout.include_home_stories, (ViewGroup) mHolder.holderStories, data);
        }
    }

    //Inicia as ultimas pesquisas
    void initLastSearch (List<QuizData> data){
        new PesquisaPresenter(activity, R.layout.include_home_pesquisa, mfragmentViewModel, (ViewGroup) mHolder.holderPesquisa, data);
    }

    //Inicia o Quiz
    void initQuiz (QuizData data){
        new QuizHomePresenter(activity, R.layout.include_home_quiz, (ViewGroup) mHolder.holderQuiz, data);
    }

    //Inicia quando não tem quiz
    void initNoQuiz (){
        new QuizHomePresenter(activity, R.layout.include_home_quiz, (ViewGroup) mHolder.holderQuiz);
    }

    //Inicia Campo
    void initCampo (final EscalacaoData data){
        if(data != null){

            CampoPresenter campoPresenter = new CampoPresenter(activity, R.layout.fragment_campo, (ViewGroup) mHolder.holderCampo, mHolder.holderCampo.getWidth(), mHolder.holderCampo.getHeight());
            mHolder.campoTittle.setText(data.getTitulo());

            int id = 1;
            int esquema = 1;

            if(data.getPlayers() != null && data.getPlayers().size() > 0){
                id = data.getId();
                esquema = data.getEsquema();
                campoPresenter.changeEsquema(data);
            }else{
                id = data.getId();
                esquema = data.getEsquema();
                campoPresenter.changeEsquema(esquema);
            }

            final int finalId = id;
            final int finalEsquema = esquema;
            campoPresenter.setClickListenet(new CampoPresenter.OnClickListener() {
                @Override
                public void OnClick(int id) {
                    Intent intent = new Intent(activity, EscalarTimeActivity.class);
                    intent.putExtra("id", finalId);
                    intent.putExtra("esquema", finalEsquema);
                    intent.putExtra("tittle", data.getTitulo());
                    AdMobController.starIntetentBeforeAd(activity, intent, AdMobController.HOME_INTERSECTIAL_CAMPO_ID);
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_ESCALACAO, finalId).execute();
                }
            });

            mHolder.btnCampo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, EscalarTimeActivity.class);
                    intent.putExtra("id", finalId);
                    intent.putExtra("esquema", finalEsquema);
                    intent.putExtra("tittle", data.getTitulo());
                    AdMobController.starIntetentBeforeAd(activity, intent, AdMobController.HOME_INTERSECTIAL_CAMPO_ID);
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_ESCALACAO, finalId).execute();
                }
            });
        }
    }

    void initBanner (final BannerLojaData data){
        mHolder.bannerLoja.setVisibility(View.VISIBLE);

        Log.d("HOME", data.toString());
        UTILS.getImageAt(activity,data.getImg(), mHolder.imgBanerLoja );

        mHolder.txtBanerLoja.setText(data.getTittle());

        if(!data.getColor().isEmpty()){
            String color = data.getColor().replace("#","");
            int parcedColor = Color.parseColor("#" + color);
            mHolder.bannerBackground.setBackgroundColor(parcedColor);
        }

        mHolder.bannerLoja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(data.getT() >= 0){
                    SetNotification(new NotificationData(data.getV(), data.getId(),data.getT()));
                }else{
                    if(data.getEmbed() != null){
                        Intent intent = new Intent(activity, NavegadorActivity.class);
                        intent.putExtra("link", data.getLink());
                        startActivity(intent);
                    }else{
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getLink()));
                        startActivity(browserIntent);
                    }
                }
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_CTA, data.getId()).execute();
            }
        });
    }


    void SetNotification (NotificationData data){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idPush", data.getIdPush());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_NOTIFICATION, 999, emptyAsync).execute(params);

        switch (data.getTipo()){
            case MasterBakersNotificationManager.PUSH_ID_DEFAUT: {
                //
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_QUIZ: {
                mfragmentViewModel.changeFragment(new QuizFragment(data.getId()), "QuizFragment");
            }
            break;


            case MasterBakersNotificationManager.PUSH_ID_PESQUISA:{
                mfragmentViewModel.changeFragment(new PesquisaFragment(), "PesquisaFragment");
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PALPITES: {
                mfragmentViewModel.changeFragment(new PalpitesFragment(), "PalpitesFragment");
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_AGENDA: {
                mfragmentViewModel.changeFragment(new AgendaFragment(), "AgendaFragment");
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PESQUISA_ESPECIFICA: {
                mfragmentViewModel.changeFragment(new PesquisaFragment(data.getId()), "PesquisaFragment");
            }
            break;


            case MasterBakersNotificationManager.PUSH_ID_NOTICIA: {
                Intent intent = new Intent(activity, NoticiaActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PLAYERPROFILE: {
                Intent intent = new Intent(activity, PerfilAtletaActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_MEMBERSHIP: {
                Intent intent = new Intent(activity, SocioTorcedorActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_CAMPINHO: {
                Intent intent = new Intent(activity, EscalarTimeActivity.class);
                intent.putExtra("esquema", data.getId());
                intent.putExtra("tittle", data.getTitulo());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_LOJA: {
                Intent intent = new Intent(activity, LojaActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_AUDIO: {
                Intent intent = new Intent(activity, PlayerActivity.class);
                intent.putExtra("push", true);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_ALTERAR_DADOS: {
                Intent intent = new Intent(activity, ConfigurationActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_RANK: {
                Intent intent = new Intent(activity, RankingActivity.class);
                startActivity(intent);
            }
            break;
        }
    }

    //region AsyncOperation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_BANNER:{

                int status = 0;
                boolean sucess = false;

                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            if(response.getJSONObject("Object").length() > 0){
                                sucess = true;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(sucess){
                    try {
                        Gson gson = new Gson();
                        BannerLojaData data = gson.fromJson(response.getString("Object"), BannerLojaData.class);

                        if(data != null){
                            initBanner(data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;

            case OP_ID_CAMPO:{
                //Campo
                int status = 0;
                boolean sucess = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            if(response.getJSONObject("Object").length() > 0){
                                sucess = true;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(sucess){
                    try {
                        Gson gson = new Gson();
                        EscalacaoData data = gson.fromJson(response.getString("Object"), EscalacaoData.class);

                        if(data != null){
                            initCampo(data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    mHolder.campo.setVisibility(View.GONE);
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        View firtView;
        View holderNoticia;
        View holderNextGames;
        View holderLastGames;
        View holderAudio;
        View holderTwitter;
        View holderStories;
        View holderPesquisa;
        View holderLojaTime;
        View holderQuiz;
        View holderUserHeader;
        View holderCampo;

        ScrollView scrollHome;

        RelativeLayout bannerLoja;
        ImageView imgBanerLoja;
        ImageView bannerBackground;
        TextView txtBanerLoja;

        View campo;
        TextView campoTittle;
        Button btnCampo;

        public ViewHolder (View view){
            firtView = view.findViewById(R.id.firt_view);
            holderNoticia = view.findViewById(R.id.noticia_holder);
            holderNextGames = view.findViewById(R.id.next_game_holder);
            holderLastGames = view.findViewById(R.id.last_game_holder);
            holderAudio = view.findViewById(R.id.audio_holder);
            holderTwitter = view.findViewById(R.id.twitter_holder);
            holderStories = view.findViewById(R.id.stories_holder);
            holderPesquisa = view.findViewById(R.id.pesquisa_holder);
            holderLojaTime = view.findViewById(R.id.loja_time_holder);
            holderUserHeader = view.findViewById(R.id.header_holder);
            holderQuiz = view.findViewById(R.id.quiz_holder);
            holderCampo = view.findViewById(R.id.campo_holder);

            bannerLoja = view.findViewById(R.id.view_banner_loja);
            bannerBackground = view.findViewById(R.id.img_banner_background);
            imgBanerLoja = view.findViewById(R.id.img_banner_loja);
            txtBanerLoja = view.findViewById(R.id.txt_banner_loja);

            scrollHome = view.findViewById(R.id.scroll_home);

            campo = view.findViewById(R.id.view_campinho);
            campoTittle = view.findViewById(R.id.txt_selecao);
            btnCampo = view.findViewById(R.id.btn_criar_campo);
        }
    }
}
