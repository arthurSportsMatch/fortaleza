package fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.BuildConfig;
import com.sportm.fortaleza.CheckInLoginActivity;
import com.sportm.fortaleza.CheckInMessageActivity;
import com.sportm.fortaleza.CheckinActivity;
import com.sportm.fortaleza.ConfigurationActivity;
import com.sportm.fortaleza.LoginActivity;
import com.sportm.fortaleza.LojaActivity;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.NavegadorActivity;
import com.sportm.fortaleza.PlaylistActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.RankingActivity;
import com.sportm.fortaleza.SocioTorcedorActivity;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import data.BannerLojaData;
import data.ChatMessageData;
import data.FragmentData;
import data.MessageData;
import data.ServerCustomData;
import informations.UserInformation;
import models.user.UserModel;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.fragment.FragmentHolderViewModel;
import vmodels.user.UserViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;
import static com.sportm.fortaleza.MainActivity.prefs;

public class MenuLateralFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback{

    private ViewHolder mHolder;
    private UserViewModel userViewModel;
    private FragmentHolderViewModel mfragmentViewModel;
    private boolean trigger_check = true;

    private static final int OP_ID_BANNER               = 0;
    private static final int OP_ID_GET_SERVER_LIST      = 1;
    private static final int OP_ID_SING_OUT             = 2;
    private static final int OP_ID_GET_MESSAGE          = 3;

    public MenuLateralFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_lateral, container, false);
        mHolder = new ViewHolder(view);
        initAction();
        getLoja();
        putVersion();

        return view;
    }



    @Override
    public void bind() {
        super.bind();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                uploadUser(user);
            }
        });
        userViewModel.getUser();

        mfragmentViewModel = new ViewModelProvider(requireActivity()).get(FragmentHolderViewModel.class);
    }

    @Override
    public void unbind() {
        super.unbind();
        userViewModel.getUserData().removeObservers(this);
    }

    void putVersion (){
        String versionName = BuildConfig.VERSION_NAME;
        String version = versionName + "(" + (UserInformation.getServerData() != null? UserInformation.getServerData().getServerId() : "") +")";
        mHolder.version.setText(version);
    }

    void getLoja (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_BANNER_MENU, OP_ID_BANNER, this).execute();
    }

    void uploadUser (UserModel user){
        //Imagem
        UTILS.getImageAt(activity, user.getImg(), mHolder.userImage, new RequestOptions()
                .circleCrop()
                .error(R.drawable.ic_placeholderuser)
                .placeholder(R.drawable.ic_placeholderuser));

        //Nome
        mHolder.userName.setText(user.getNome());

        mHolder.userNivel.setText(user.isSocioTorcedor()? "Sócio Torcedor" : "Torcedor");
        UserInformation.getUserData().setSocioBeneficio(user.getSocioBeneficio());
        UserInformation.getUserData().setSocioTorcedor(user.getSocioTorcedor());
        UserInformation.getUserData().setNickname(user.getNome());
        UserInformation.getUserData().setCelular(user.getCelular());
        UserInformation.getUserData().setCpf(user.getCpf());
        UserInformation.getUserData().setImg(user.getImg());
        UserInformation.getUserData().setDn(user.getDn());
        UserInformation.getUserData().setEmail(user.getEmail());
        UserInformation.getUserData().setMoedas(user.getMoedas());
        UserInformation.getUserData().setSocioStatus(user.getSocioStatus());
    }

    void initBanner (final List<BannerLojaData> data){
        Log.d("Banner", data.toString());
        for(final BannerLojaData banner : data){
            View view = LayoutInflater.from(activity).inflate(R.layout.adapter_menu_lateral_button, null, false);
            ImageView imageView = view.findViewById(R.id.img_banner_loja);
            TextView textView = view.findViewById(R.id.txt_banner_loja);

            UTILS.getImageAt(activity, banner.getIcon(), imageView);

            textView.setText(banner.getTittle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(banner.getEmbed() != null && banner.getEmbed().equals("1")){
                        Intent intent = new Intent(activity, NavegadorActivity.class);
                        intent.putExtra("link", banner.getLink());
                        startActivity(intent);
                    }else{
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                        startActivity(browserIntent);
                    }
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_CTA, banner.getId()).execute();
                }
            });

            mHolder.bannerView.addView(view);
        }
    }

    void initAction (){
        mHolder.configuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_CONFIGURACAO).execute();
                Intent intent = new Intent(getActivity(), ConfigurationActivity.class);
                startActivity(intent);
            }
        });

        mHolder.validarSocio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_SOCIOTORCEDOR).execute();
                Intent intent = new Intent(getActivity(), SocioTorcedorActivity.class);
                startActivity(intent);
            }
        });

        mHolder.checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!UserInformation.isBlockCheckIn()){
                    getMessageCovid();
                }else{
                    goSocioTorcedor();
                }
            }
        });

        mHolder.perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_VERPERFIL).execute();
                mfragmentViewModel.changeFragment(new PerfilFragment(), "PerfilFragment");
            }
        });

        mHolder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_VERPERFIL_AVATAR).execute();
                mfragmentViewModel.changeFragment(new PerfilFragment(), "PerfilFragment");
            }
        });

        mHolder.ranking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_RANKING).execute();
                Intent intent = new Intent(getActivity(), RankingActivity.class);
                startActivity(intent);
            }
        });

        mHolder.loja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_LOJA).execute();
                Intent intent = new Intent(getActivity(), LojaActivity.class);
                startActivity(intent);
            }
        });

        mHolder.socio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHolder.socioToggle.getVisibility() == View.VISIBLE){
                    mHolder.arrowTogle.setImageResource(R.drawable.ic_arrow_down);
                    mHolder.socioToggle.setVisibility(View.GONE);
                }else{
                    mHolder.arrowTogle.setImageResource(R.drawable.ic_arrow_up);
                    mHolder.socioToggle.setVisibility(View.VISIBLE);
                }
            }
        });

        mHolder.podcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_AUDIOTECA).execute();
                Intent intent = new Intent(getActivity(), PlaylistActivity.class);
                startActivity(intent);
            }
        });

        mHolder.lojaVirtual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_AUDIOTECA).execute();
                Intent intent = new Intent(getActivity(), LojaFortalezaActivity.class);
                startActivity(intent);
            }
        });

        mHolder.sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_SAIR).execute();
                wantLogout();
            }
        });
    }

    void goSocioTorcedor (){
        int socio = UserInformation.getUserData().getSocioTorcedor();
        int beneficio = UserInformation.getUserData().getSocioBeneficio();

        if(socio == 1){
            if(beneficio == 1){
                Intent intent = new Intent(activity, CheckinActivity.class);
                intent.putExtra("socio", socio);
                intent.putExtra("beneficio", beneficio);
                intent.putExtra("nome", UserInformation.getUserData().getNickname());
                intent.putExtra("cpf", UserInformation.getUserData().getCpf());
                startActivity(intent);
            }else{
                Intent intent = new Intent(activity, CheckInLoginActivity.class);
                startActivity(intent);
            }

        }else{
            Intent intent = new Intent(activity, CheckInLoginActivity.class);
            startActivity(intent);
        }
    }

    void wantLogout (){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        //builder.setTitle("Você ira se deslogar");
        builder.setMessage("Você realmente deseja sair?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_SAIR_SIM).execute();
                logout();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_MENU_SAIR_NAO).execute();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    //Logout do usuario
    private void logout(){
        endSession();
        getServerList();
    }

    //End sessions
    private void endSession(){
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("sId", UserInformation.getSessionId());
        params.put("endType", 3);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        params.put("time", currentDateandTime);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_END_SESSION, OP_ID_SING_OUT, this).execute(params);
    }

    void getServerList (){
        Hashtable<String,Object> params = new Hashtable<>();

        //        if(UserInformation.getUserData() != null){
//            if(UserInformation.getUserData().getId() != 0){
//                params.put("idUser", UserInformation.getUserData().getId());
//            }
//        }

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_SERVIDORES, OP_ID_GET_SERVER_LIST, this).execute(params);
    }

    void getMessageCovid (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_MESSAGE, OP_ID_GET_MESSAGE, this).execute();
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_BANNER :{
                int status = 0;
                boolean sucess = false;

                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            if(response.getJSONObject("Object").length() > 0){
                                sucess = true;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(sucess){
                    try {
                        Gson gson = new Gson();
                        JSONObject object = response.getJSONObject("Object");
                        BannerLojaData[] data = gson.fromJson(object.getString("itens"), BannerLojaData[].class);

                        if(data != null){
                            List<BannerLojaData> banners = new ArrayList<>(Arrays.asList(data));
                            initBanner(banners);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;

            case OP_ID_GET_MESSAGE : {
                    int status = 0;
                    boolean sucess = false;

                    if (response.has("Status")) {
                        try {
                            status = (int) response.get("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (status == 200) {
                        if (response.has("Object")) {
                            try {
                                if (response.getJSONObject("Object").length() > 0) {
                                    sucess = true;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (sucess) {
                        try {
                            Gson gson = new Gson();
                            MessageData message = gson.fromJson(response.getString("Object"), MessageData.class);
                            Intent intent = new Intent(getActivity(), CheckInMessageActivity.class);
                            if ( message.getTit() != null || message.getMsg() != null ){
                            intent.putExtra("tit", message.getTit());
                            intent.putExtra("msg", message.getMsg());
                            startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (UserInformation.getUserData().getSocioTorcedor() == 1) {
                            Intent intent = new Intent(getActivity(), CheckinActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getActivity(), CheckInLoginActivity.class);
                            startActivity(intent);
                        }
                    }
            }
            break;

            case OP_ID_GET_SERVER_LIST :{
                int status = 0;
                if(response.has("Status")){
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if (!response.toString().isEmpty()){
                        try {
                            Gson gson = new Gson();
                            ServerCustomData data = gson.fromJson(response.getString("Object"), ServerCustomData.class);

                            boolean suceess = true;

                            try{
                                if(data == null){
                                    suceess = false;
                                }else if (data.getMedia() == null && data.getMedia().size() <= 0){
                                    suceess = false;
                                } else if (data.getApi() == null && data.getApi().size() <= 0){
                                    suceess = false;
                                }
                            }catch (Exception e){
                                suceess = false;
                                Log.e("Async",  "Erro ao pegar servers" + e);
                            }

                            if(suceess){
                                UserInformation.setServerData(data);
                                UTILS.salvarServidor(data);

                                if(data.getIdUser() > 0){
                                    UserInformation.getServerData().setIdUser(data.getIdUser());
                                    UTILS.salvarServidor(UserInformation.getServerData());
                                }

                                CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServerSelected() + "api/");;
                                CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                            }else{
                                new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_TRACK_ERROR, 345, emptyAsync, TRACKING.TRACKING_GETSERVER_ERROR).execute();
                                if(UserInformation.getServerData() != null){
                                    UserInformation.getServerData().setServerId(0);
                                }
                            }

                            //new AsyncOperation(activity, AsyncOperation.TASK_ID_CHECK_VERSION, OP_CHECK_VERSION, this).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_ID_SING_OUT :{
                int status = 0;
                if(response.has("Status")){
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    UserInformation.setSessionId(0);
                    UTILS.ClearInformations(getContext());
                    Intent newIntent = new Intent(getContext(), LoginActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(newIntent);
                    activity.finish();
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_SING_OUT:{
                UserInformation.setSessionId(0);
                UTILS.ClearInformations(getContext());
                Intent newIntent = new Intent(getContext(), LoginActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(newIntent);
                activity.finish();
            }

            case OP_ID_GET_MESSAGE:{
                if(UserInformation.getUserData().getSocioTorcedor() == 1){
                    Intent intent = new Intent(getActivity(), CheckinActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), CheckInLoginActivity.class);
                    startActivity(intent);
                }
            }
        }
    }

    class ViewHolder {
        ImageView userImage;
        TextView userName;
        TextView userNivel;
        TextView version;
        LinearLayout bannerView;

        View socioToggle;
        ImageView arrowTogle;
        View validarSocio;
        View checkIn;
        LinearLayout bannerLoja;
        TextView txtBannerLoja;
        ImageView imgBannerLoja;
        View socio;
        View perfil;
        View ranking;
        View loja;
        View podcast;
        View lojaVirtual;
        //View rifas;
        View configuration;
        View sair;

        public ViewHolder (View view){


            userImage = view.findViewById(R.id.user_image_menu);
            userName = view.findViewById(R.id.menu_txt_user);
            userNivel = view.findViewById(R.id.menu_txt_nivel);
            bannerView = view.findViewById(R.id.banner_news);

            socioToggle = view.findViewById(R.id.toogle_menu);
            arrowTogle = view.findViewById(R.id.img_arrow);
            validarSocio = view.findViewById(R.id.menu_validar_socio);
            checkIn = view.findViewById(R.id.menu_checkin);

            bannerLoja = view.findViewById(R.id.banner_loja);
            txtBannerLoja = view.findViewById(R.id.txt_banner_loja);
            imgBannerLoja = view.findViewById(R.id.img_banner_loja);

            socio = view.findViewById(R.id.menu_socio);
            perfil = view.findViewById(R.id.menu_perfil);
            ranking = view.findViewById(R.id.menu_ranking);
            loja = view.findViewById(R.id.menu_loja);
            podcast = view.findViewById(R.id.menu_audio);
            lojaVirtual = view.findViewById(R.id.menu_loja_virtual);
            //rifas = view.findViewById(R.id.menu_rifas);
            configuration = view.findViewById(R.id.menu_configuration);
            sair = view.findViewById(R.id.menu_sair);
            version = view.findViewById(R.id.txt_version);
        }
    }
}
