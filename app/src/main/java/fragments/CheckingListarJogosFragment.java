package fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportm.fortaleza.MainActivity;
import com.sportm.fortaleza.R;

import org.json.JSONObject;

import java.util.List;

import adapter.ChekinJogosAdapter;
import data.CheckinPartidaData;
import data.PartidaData;
import utils.UILoadingController;

/**
 * Recebe a data e a exibe na tela
 * OnClick = Manda mensagem pra atividade que um item foi clicado
 * OnLoad = Manda mensagem pra atividade que esta carregando e espera de volta a data
 * Quando estiver carregando o layout deve ficar desativado
 * Aparecendo apenas quando tiver carregado
 */
public class CheckingListarJogosFragment extends MainFragment {

    Listener listener;
    ViewHolder mHolder;


    public CheckingListarJogosFragment(Listener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checking_listar_jogos, container, false);
        mHolder = new ViewHolder(view);

        View loadingView = view.findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.OnLoad();
            }
        });

        initLoading();
        return view;
    }

    void initLoading (){
        listener.OnLoad();
        mHolder.semJogos.setVisibility(View.GONE);
        uiLoadingController.onLoad();
    }

    //Recebe a data e atualiza o layout
    public void atualizarData(JSONObject data){

    }

    //Recebe a data e atualiza o layout
    public void atualizarData(@Nullable List<CheckinPartidaData> data){
        uiLoadingController.finishLoader(true);
        ChekinJogosAdapter adapter = new ChekinJogosAdapter(activity, data, new ChekinJogosAdapter.Listener() {
            @Override
            public void onClick(CheckinPartidaData partida) {
                listener.OnClickItem(partida);
            }
        });

        mHolder.listaDeJogos.setAdapter(adapter);
        mHolder.listaDeJogos.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
    }

    public void getNoData (){
        uiLoadingController.finishLoader(true);
        mHolder.semJogos.setVisibility(View.VISIBLE);

    }

    public void errorToGetData (){
        uiLoadingController.finishLoader(false);
    }

    public interface Listener {
        void OnClickItem (CheckinPartidaData partida);
        void OnLoad ();
    }

    class ViewHolder {
        RecyclerView listaDeJogos ;
        TextView semJogos;
        public ViewHolder (View view){
            listaDeJogos = view.findViewById(R.id.lista_jogos);
            semJogos = view.findViewById(R.id.txt_sem_jogos);
        }
    }
}
