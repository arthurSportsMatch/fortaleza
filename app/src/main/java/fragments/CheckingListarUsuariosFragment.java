package fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.sportm.fortaleza.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import data.CheckinData;
import data.CheckinSetoresData;
import informations.UserInformation;
import utils.UILoadingController;
import static utils.UTILS.convertMonetaryValue;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckingListarUsuariosFragment extends MainFragment {

    ViewHolder mHolder;
    CheckinData checking;
    Listener listener;
    TextView incluir_acompanhante;

    private static final int OP_DIALOG_ADD_TICKET = 1;
    private static final int OP_DIALOG_ALTER_NAME = 2;
    private static final int OP_DIALOG_ALTER_TICKET = 3;

    public CheckingListarUsuariosFragment() {
    }

    public CheckingListarUsuariosFragment(CheckinData data, Listener listener) {
        this.checking = data;
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checking_listar_usuarios, container, false);
        mHolder = new ViewHolder(view);
        incluir_acompanhante = view.findViewById(R.id.incluir_acompanhante);
        View loadingView = view.findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.onLoad();
            }
        });
        initLoading();
        return view;
    }


    //Atualiza a quantidade de ingressos
    public void atualizarData (List<CheckinSetoresData> response){
        uiLoadingController.finishLoader(true);

        if ( !checking.isHasTickets() ) {

            JSONArray tickets = new JSONArray();

            try {
                JSONObject ticket = new JSONObject();
                ticket.put("nome", UserInformation.getUserData().getNickname());
                ticket.put("ingresso", "Conselheiro");
                ticket.put("valor", 0);
                ticket.put("id", 0);
                ticket.put("conselheiro", false);
                ticket.put("primario", true);
                tickets.put(ticket);
            }catch (JSONException e){
                e.printStackTrace();
            }

            for (CheckinSetoresData data : response) {
                try {
                    JSONObject ticket = new JSONObject();
                    ticket.put("nome", data.getNome());
                    ticket.put("ingresso", "Conselheiro");
                    ticket.put("valor", 0);
                    ticket.put("id", data.getId());
                    ticket.put("conselheiro", true);
                    ticket.put("primario", false);
                    tickets.put(ticket);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            int i = 0;

            do {
                try {
                    JSONObject ticket = tickets.getJSONObject(i);
                    createCheckingItem(
                            ticket.getString("nome"),
                            ticket.getString("ingresso"),
                            String.valueOf(ticket.getDouble("valor")),
                            ticket.getInt("id"),
                            ticket.getBoolean("primario"),
                            ticket.getBoolean("conselheiro"),
                            true);
                    i++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }while (i < tickets.length());

        }else {
            JSONArray list = new JSONArray(checking.getTickets());

            listener.deleteAllTickets();

            JSONObject ticket;

            for (int i = 0; i < list.length(); i++) {
                try {
                    ticket = list.getJSONObject(i);
                    createCheckingItem(
                            ticket.getString("nome"),
                            ticket.getString("ingresso"),
                            String.valueOf(ticket.getDouble("valor")),
                            ticket.getInt("id"),
                            ticket.getBoolean("primario"),
                            ticket.getBoolean("conselheiro"),
                            true
                            );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        listener.OnUnlock();
    }

    void initLoading (){
        listener.onLoad();
        uiLoadingController.onLoad();
    }

    public void createCheckingItem (@Nullable String nome,  String ingresso, String valor, int id, boolean isPrimary, boolean isConselheiro, boolean add){
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_checkin_users,mHolder.viewListaChecking, false);
        TextView txtNome, txtIngresso, txtValor, txtAlterar, txtAlterarIngresso;
        View remover = view.findViewById(R.id.remove);
        txtNome = view.findViewById(R.id.txt_nome);
        txtIngresso = view.findViewById(R.id.txt_ingresso);
        txtValor = view.findViewById(R.id.txt_valor);
        txtAlterar = view.findViewById(R.id.btn_alterar);
        txtAlterarIngresso = view.findViewById(R.id.btn_alterar_ingresso);
        JSONObject ticket = new JSONObject();

        txtNome.setText(nome);
        txtIngresso.setText(ingresso);
        txtValor.setText(convertMonetaryValue(Float.parseFloat(valor), "pt", "BR"));

        try {
            ticket.put("nome", nome);
            ticket.put("ingresso", ingresso);
            ticket.put("valor", valor);
            ticket.put("id", id);
            ticket.put("conselheiro", isConselheiro);
            ticket.put("primario", isPrimary);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ( !isPrimary ) {
            remover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view2) {
                    mHolder.viewListaChecking.removeView(view);
                    checking.getNomes().remove(nome);
                    checking.getValores().remove(valor);
                    listener.removeTicket(ticket);
                }
            });
            if ( !isConselheiro ) {
                txtAlterar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view2) {
                        showDialogTicket(OP_DIALOG_ALTER_NAME, view, ticket);
                    }
                });
                txtAlterarIngresso.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view2) {
                        showDialogTicket(OP_DIALOG_ALTER_TICKET, view, ticket);
                    }
                });
            }else {
                txtAlterar.setVisibility(View.GONE);
                txtAlterarIngresso.setVisibility(View.GONE);
            }
        }else {
            remover.setVisibility(View.GONE);
            txtAlterar.setVisibility(View.GONE);
            txtAlterarIngresso.setVisibility(View.GONE);
        }

        incluir_acompanhante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                showDialogTicket(OP_DIALOG_ADD_TICKET, view, ticket);
            }
        });

        mHolder.viewListaChecking.addView(view);
        mHolder.scroll.post(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mHolder.scroll.fullScroll(View.FOCUS_DOWN);
                    }
                },500);
            }
        });

        if (add)
        listener.addTicket(ticket);
    }

    void showDialogTicket(int type, View viewTicket, JSONObject ticket) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawableResource(R.color.colorBlackTransp);
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_checkin_ticket, null, false);
        dialog.setContentView(view);

        EditText name = dialog.findViewById(R.id.edt_checkin_acompanhante);
        Spinner spnTicket = dialog.findViewById(R.id.spn_checkin_ingresso);
        Button addTicket = dialog.findViewById(R.id.btn_concluir);
        LinearLayout container = dialog.findViewById(R.id.ln_container);
        CardView cardView = view.findViewById(R.id.card);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        switch (type) {
            case OP_DIALOG_ADD_TICKET: {

                JSONArray ingressos = checking.getIngressos();
                final JSONObject[] ingresso = new JSONObject[1];
                ArrayList tickets = new ArrayList<>();

                tickets.add("Tipo de ingresso");

                for ( int i = 0; i<ingressos.length(); i++ ) {
                    try {
                        ingresso[0] = new JSONObject(ingressos.get(i).toString());
                        tickets.add(ingresso[0].getString("nome") + " - " + ingresso[0].getDouble("valor"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final String[] tSelect = {""};
                final float[] valor = {0};
                final int[] id = {0};

                ArrayAdapter<String> adp = new ArrayAdapter<String> (activity,android.R.layout.simple_spinner_dropdown_item,tickets);
                spnTicket.setAdapter(adp);

                spnTicket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long arg3)
                    {

                        if ( position != 0 ) {
                            tSelect[0] = parent.getItemAtPosition(position).toString();


                            for (int i = 0; i < ingressos.length(); i++) {
                                try {
                                    ingresso[0] = new JSONObject(ingressos.get(i).toString());
                                    if ((ingresso[0].getString("nome") + " - " + ingresso[0].getDouble("valor")).equals(tSelect[0])) {
                                        tSelect[0] = ingresso[0].getString("nome");
                                        valor[0] = (float) ingresso[0].getDouble("valor");
                                        id[0] = ingresso[0].getInt("id");
                                        break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    public void onNothingSelected(AdapterView<?> arg0)
                    {

                    }
                });


                addTicket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nome = name.getText().toString();
                        if (!nome.equals("")) {

                            if (!tSelect[0].equals("")) {
                                createCheckingItem(nome, tSelect[0], String.valueOf(valor[0]), id[0],false, false, true);
                                dialog.dismiss();
                                Toast.makeText(activity, "Acompanhante adicionado", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(activity, "Escolha um ingresso", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            name.setError("Insira o nome do acompanhante");
                        }
                    }
                });
            }
            break;
            case OP_DIALOG_ALTER_NAME: {
                spnTicket.setVisibility(View.GONE);
                TextView txtNome = viewTicket.findViewById(R.id.txt_nome);
                name.setHint(txtNome.getText().toString());

                addTicket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String nome = name.getText().toString();
                        if (!nome.equals("")) {
                            TextView name = viewTicket.findViewById(R.id.txt_nome);
                            listener.removeTicket(ticket);
                            try {
                                ticket.put("nome", nome);
                                listener.addTicket(ticket);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            name.setText(nome);
                            dialog.dismiss();
                        } else {
                            name.setError("Insira o nome do acompanhante");
                        }
                    }
                });
            }
            break;
            case OP_DIALOG_ALTER_TICKET: {
                name.setVisibility(View.GONE);

                JSONArray ingressos = checking.getIngressos();
                final JSONObject[] ingresso = new JSONObject[1];
                ArrayList tickets = new ArrayList<>();

                tickets.add("Tipo de ingresso");

                for ( int i = 0; i<ingressos.length(); i++ ) {
                    try {
                        ingresso[0] = new JSONObject(ingressos.get(i).toString());
                        tickets.add(ingresso[0].getString("nome") + " - " + ingresso[0].getDouble("valor"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final String[] tSelect = {""};
                final float[] valor = {0};
                final int[] id = {0};

                ArrayAdapter<String> adp = new ArrayAdapter<String> (activity,android.R.layout.simple_spinner_dropdown_item,tickets);
                spnTicket.setAdapter(adp);

                spnTicket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long arg3)
                    {
                        if ( position != 0 ) {
                            tSelect[0] = parent.getItemAtPosition(position).toString();

                            for (int i = 0; i < ingressos.length(); i++) {
                                try {
                                    ingresso[0] = new JSONObject(ingressos.get(i).toString());
                                    if ((ingresso[0].getString("nome") + " - " + ingresso[0].getDouble("valor")).equals(tSelect[0])) {
                                        tSelect[0] = ingresso[0].getString("nome");
                                        valor[0] = (float) ingresso[0].getDouble("valor");
                                        id[0] = ingresso[0].getInt("id");
                                        break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }

                    public void onNothingSelected(AdapterView<?> arg0)
                    {

                    }
                });

                addTicket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!tSelect[0].equals("")) {
                            TextView ingresso = viewTicket.findViewById(R.id.txt_ingresso);
                            TextView txtValor = viewTicket.findViewById(R.id.txt_valor);
                            listener.removeTicket(ticket);
                            try {
                                ticket.put("ingresso", tSelect[0]);
                                ticket.put("valor", String.valueOf(valor[0]));
                                ticket.put("id", id[0]);
                                listener.addTicket(ticket);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ingresso.setText(tSelect[0]);
                            txtValor.setText(convertMonetaryValue(valor[0], "pt", "BR"));

                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                        }
                    }
                });
            }
            break;
        }

        dialog.show();
    }

    public void errorToGetData (){
        uiLoadingController.finishLoader(false);
    }

    public interface Listener {
        void onLoad();
        void addTicket(JSONObject ticket);
        void removeTicket(JSONObject ticket);
        void deleteAllTickets();
        void OnUnlock();
    }

    class ViewHolder {
        LinearLayout viewListaChecking;
        ScrollView scroll;
        public ViewHolder(View view){
            viewListaChecking = view.findViewById(R.id.view_lista_checkin);
            scroll = view.findViewById(R.id.scroll_tickets);
        }
    }

}
