package fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.QuizActivity;
import com.sportm.fortaleza.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterQuizCategoria;
import data.QuizCategoriaData;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

import static android.app.Activity.RESULT_OK;
import static com.sportm.fortaleza.MainActivity.emptyAsync;

public class QuizFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    AdapterQuizCategoria adapter;
    ViewHolder mHolder;
    int categoryPage = 1;
    int categoryID = 0;
    int pushQuiz = -1;
    Dialog loading;

    boolean openingQuiz = false;
    boolean hasData = false;



    private static final int OP_GET_QUIZES            = 0;


    public QuizFragment() {
    }
    public QuizFragment(int id) {
        pushQuiz = id;
    }

    public void setPUSH (int pushValue){
        this.pushQuiz = pushValue;
        getQuizes(null, pushQuiz);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.include_card_quiz, container, false);
        mHolder = new ViewHolder(view);
        initAction();
        return view;
    }

    @Override
    public void onReload() {
        super.onReload();
        categoryPage = 1;
        if(pushQuiz > 0){
            getQuizes(null, pushQuiz);
        }else{
            getQuizes(null, null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if(data != null){
                if(data.hasExtra("quant")){
                    mHolder.txtCategoryButton.setText(data.getStringExtra("quant"));
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        categoryPage = 1;
        if(pushQuiz > 0){
            getQuizes(null, pushQuiz);
        }else{
            getQuizes(null, null);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(adapter == null){
            categoryPage = 1;
            getQuizes(null, null);
        }else{
            mHolder.categorias.setVisibility(View.VISIBLE);
        }
    }

    private void initAction (){
        //Botão de categorias
        mHolder.viewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_GRUPOQUIZ_VOLTAR).execute();
                if(adapter == null){
                    categoryPage = 1;
                    getQuizes(null, null);
                }else{
                    mHolder.categorias.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    //Pega a lista de quizes existentes
    void getQuizes (@Nullable Integer page, @Nullable Integer id){
        Hashtable<String, Object> params = new Hashtable<>();
        if(page != null){
            params.put("page" , page);
        }

        if(id != null){
            params.put("id", id);
        }
        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_QUIZES, OP_GET_QUIZES, this).execute(params);
        //loading = OnLoadListener.createLoadDialog(activity);
    }

    //Inicia o quiz
    private void initQuiz (final String title, String description, final String quant, final String finalValue, final String img, final int pos, boolean canUse){
        mHolder.categorias.setVisibility(View.GONE);

        UTILS.getImageAt(activity, img, mHolder.imgBackground);

//        switch (pos){
//            case 0:{
//                mHolder.imgForeground.setColorFilter(getActivity().getResources().getColor(R.color.colorButtonRedTransp2));
//                mHolder.txtTitle.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtCategoryButton.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtDescription.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtNext.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//            }
//            break;
//            case 1:{
//                mHolder.imgForeground.setColorFilter(getActivity().getResources().getColor(R.color.colorButtonWhiteTransp));
//                mHolder.txtTitle.setTextColor(getActivity().getResources().getColor(R.color.colorBlackLetter));
//                mHolder.txtCategoryButton.setTextColor(getActivity().getResources().getColor(R.color.colorBlackLetter));
//                mHolder.txtDescription.setTextColor(getActivity().getResources().getColor(R.color.colorBlackLetter));
//                mHolder.txtNext.setTextColor(getActivity().getResources().getColor(R.color.colorBlackLetter));
//            }
//            break;
//            case 2:{
//                mHolder.imgForeground.setColorFilter(getActivity().getResources().getColor(R.color.colorButtonBlackTransp));
//                mHolder.txtTitle.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtCategoryButton.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtDescription.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//                mHolder.txtNext.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//            }
//            break;
//        }

        //Mudar esqueleto
        mHolder.viewNext.setVisibility(View.VISIBLE);
        mHolder.viewAlignTop.setVisibility(View.VISIBLE);
        mHolder.viewAlign.setVisibility(View.VISIBLE);

        //Mudar valores de descrição
        mHolder.txtTitle.setText(title);
        mHolder.txtDescription.setText(description);
        mHolder.txtCategoryButton.setText(finalValue);

        //Botão para iniciar quiz
        mHolder.viewResults.setVisibility(View.GONE);
        mHolder.viewAction.setVisibility(View.VISIBLE);
        mHolder.txtCategoryButton.setVisibility(View.VISIBLE);

        if(canUse){
            mHolder.action.setActivated(true);
            mHolder.action.setAlpha(1);
            //Botão de play
            mHolder.action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!openingQuiz){
                        openingQuiz = true;
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_GRUPOQUIZ_INICIAR).execute();
                        String[] strings = finalValue.split("/");
                        int respostas = Integer.parseInt(strings[0]);
                        openQuiz(categoryID, title, pos, img, quant, respostas);
                    }
                }
            });
        }else{
            mHolder.action.setActivated(false);
            mHolder.action.setAlpha(0.5f);
            //Botão de play
            mHolder.action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

    }

    //Inicia Categorias
    private void initCategorias (final List<QuizCategoriaData> data){
        mHolder.categorias.setVisibility(View.VISIBLE);
        if(adapter == null){
            adapter = new AdapterQuizCategoria(getContext(), data, new AdapterQuizCategoria.Listener() {

                @Override
                public void OnClick(int id, String tittle, String descricao, String quant, String finalValue,  String img, int pos, boolean canUse) {
                    initQuiz(tittle, descricao, quant, finalValue, img, pos, canUse);
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_GRUPOQUIZ_SELECT, id).execute();
                    categoryID = id;
                }

                @Override
                public void OnLastItem() {
                    Onlast();
                }
            });
        }else{
            if(data != null){
                if(categoryPage > 1){
                    adapter.addCategoria(data);
                }else{
                    adapter.changeCategoria(data);
                }
            }
        }

        mHolder.categorias.setAdapter(adapter);
        mHolder.categorias.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    void Onlast (){
        if(hasData){
            categoryPage ++;
            getQuizes(categoryPage, null);
            hasData = false;
        }
    }

    private void initPushQuiz (QuizCategoriaData data){
        initQuiz(data.getTitulo(), data.getDescricao(), data.getQuantidade(), "0",data.getImg(), 0, true);
        pushQuiz = -1;
    }

    //Abre o Quiz
    private void openQuiz (int id, String category, int color, String img, String quant, int acertos){
        Log.d("Acertos", "acertos: " + acertos);
        Intent intent = new Intent(getContext(), QuizActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("tittle", category);
        intent.putExtra("color", color);
        intent.putExtra("img", img);
        intent.putExtra("quant", quant);
        intent.putExtra("acertos", acertos);
        AdMobController.starIntetentBeforeAd(activity, intent, AdMobController.QUIZ_INTERSECTIAL_ID);
        openingQuiz = false;
    }

    //region Async Operation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {

        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;


            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_QUIZES:{
                //loading.cancel();
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject object = response.getJSONObject("Object");
                            QuizCategoriaData[] data = gson.fromJson(object.getString("itens"), QuizCategoriaData[].class);

                            if(data != null && data.length > 0){
                                hasData = true;
                                List<QuizCategoriaData> quizes = new ArrayList<>(Arrays.asList(data));
                                if(pushQuiz > 0){
                                    initPushQuiz(quizes.get(0));
                                }else{
                                    initCategorias(quizes);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        TextView txtTitle;
        TextView txtDescription;

        //resultados
        LinearLayout viewResults;

        //alinhamento (ultra necessario)
        LinearLayout viewAlign;
        LinearLayout viewAlignTop;

        //botão de ação
        LinearLayout viewAction;
        TextView txtCategoryButton;

        //arrastar para cima
        LinearLayout viewNext;
        TextView txtNext;

        //Button
        Button action;
        RecyclerView categorias;

        ImageView imgBackground;
        ImageView imgForeground;

        public ViewHolder (View view){
            txtTitle = view.findViewById(R.id.txt_nome_quiz);
            txtDescription = view.findViewById(R.id.txt_description_quiz);
            viewResults = view.findViewById(R.id.view_results_quiz);

            viewAlign = view.findViewById(R.id.space_bottom_card_quiz);
            viewAlignTop = view.findViewById(R.id.space_bottom_card_quiz_0);

            viewNext = view.findViewById(R.id.view_next);
            txtNext = view.findViewById(R.id.txt_next);

            viewAction = view.findViewById(R.id.view_action_quiz);
            txtCategoryButton = view.findViewById(R.id.txt_categoria_2_quiz);

            action = view.findViewById(R.id.btn_quiz_play);
            categorias = view.findViewById(R.id.view_categorias);

            imgBackground = view.findViewById(R.id.img_background);
            imgForeground = view.findViewById(R.id.img_foreground);
        }
    }
}
