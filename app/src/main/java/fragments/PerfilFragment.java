package fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.CameraActivity;
import com.sportm.fortaleza.ConfigurationActivity;
import com.sportm.fortaleza.MainActivity;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import adapter.BadgesAdapter;
import data.BadgeData;
import data.UserData;
import informations.UserInformation;
import models.MessageModel;
import models.user.UserModel;
import picker.ImageContract;
import picker.ImagePresenter;
import repository.badges.BadgeRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.badges.BadgeViewModel;
import vmodels.user.UserViewModel;

import static android.app.Activity.RESULT_OK;
import static com.sportm.fortaleza.MainActivity.emptyAsync;
import static com.sportm.fortaleza.MainActivity.prefs;
import static fragments.FaleConoscoFragment.REQUEST_GALLERY_PHOTO;
import static fragments.FaleConoscoFragment.REQUEST_TAKE_PHOTO;

public class PerfilFragment extends MainFragment implements ImageContract.View {

    private ViewHolder mHolder;
    private UserViewModel userViewModel;
    private BadgeViewModel badgeViewModel;

    private BadgesAdapter adapter;
    private ImagePresenter mPresenter;

    private int page = 1;
    private Uri photoURI;
    private String type = "";
    private boolean lastData = false;

    public PerfilFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        mHolder = new ViewHolder(view);
        mPresenter = new ImagePresenter(this);

        initActions();
        Log.d("FragmentController", "onCreateView");
        return view;
    }

    @Override
    public void bind() {
        super.bind();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                uploadUser(user);
            }
        });
        userViewModel.getUser(false);

        BadgeRepository badgeRepository = new BadgeRepository(activity);
        badgeViewModel = new ViewModelProvider(requireActivity()).get(BadgeViewModel.class);
        badgeViewModel.setmRepository(badgeRepository);
        badgeViewModel.getBadges().observe(this, new Observer<List<BadgeData>>() {
            @Override
            public void onChanged(List<BadgeData> badges) {

                if(badges != null){
                    Log.d("Async", "Has Badges: " + badges.size());
                    initBadges(badges);
                }else{
                    lastData = true;
                }
            }
        });
    }

    @Override
    public void unbind() {
        super.unbind();
        userViewModel.getUserData().removeObservers(this);
        badgeViewModel.getBadges().removeObservers(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            uploadPhoto(requestCode, resultCode, data);
        }
    }

    void initActions (){
        mHolder.trocarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_PERFIL_ALTERAR_FOTO).execute();
                selectImage();
            }
        });

        mHolder.config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_PERFIL_ABRIR_CONFIGURACAO).execute();
                Intent intent = new Intent(activity, ConfigurationActivity.class);
                startActivity(intent);
            }
        });
    }

    void selectImage() {
        final CharSequence[] items = {"Tirar Foto", "Pegar da Galeria", getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Tirar Foto")) {
                    mPresenter.cameraClick();
                    type = "foto";
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PERFIL_ALTERAR_FOTO_CAMERA).execute();
                } else if (items[item].equals("Pegar da Galeria")) {
                    mPresenter.ChooseGalleryClick();
                    type = "galeria";
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PERFIL_ALTERAR_FOTO_BIBLIOTECA).execute();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PERFIL_ALTERAR_FOTO_CANCELAR).execute();
                }
            }
        });
        builder.show();
    }

    void uploadPhoto (int requestCode, int resultCode, Intent data){
        boolean resultOK = false;
        int cameraSide = 0;
        boolean isCam = false;

        if (resultCode == RESULT_OK) {
            //Request de tirar foto
            if (requestCode == REQUEST_TAKE_PHOTO) {
                isCam = true;
                if(photoURI == null){
                    photoURI = data.getData();
                    resultOK = true;
                }else{
                    resultOK = true;
                }

            }
            //Request de pegar foto da galeria
            else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = Objects.requireNonNull(data).getData();
                //String mPhotoPath = getRealPathFromUri(selectedImage);
                isCam = false;

                if(selectedImage != null){
                    try {
                        photoURI = Uri.parse(UTILS.getRealPathFromURI(activity, selectedImage));
                    }catch (Exception ignored) {
                        try {
                            photoURI = Uri.parse(UTILS.getPath(activity, selectedImage));
                        }catch (Exception e){
                            Log.d("FragmentController", e.getMessage()+"");
                            Toast.makeText(activity, "Erro ao carregar foto, tente outra!", Toast.LENGTH_LONG).show();
                        }
                    }
                    resultOK = true;
                }else{
                    Toast.makeText(activity, "Erro ao carregar foto, tente outra!", Toast.LENGTH_LONG).show();
                }
            }
        }

        if(data.getExtras() != null){
            try{
                cameraSide = data.getExtras().getInt("cam");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(userViewModel == null){
            Log.d("FragmentController", "userViewModel");
        }

        try{
            if(resultOK)
                userViewModel.uploadPhoto(photoURI, cameraSide, isCam);
        }
        catch (Exception e){
            Log.d("FragmentController", e.getMessage()+"");
        }

    }

    void uploadUser (UserModel user){
        //Imagem
        UTILS.getImageAt(activity, user.getImg(), mHolder.userImage, new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_placeholderuser)
                .error(R.drawable.ic_placeholderuser));

        //Nome
        mHolder.userName.setText(user.getNome());

        //Pontos
        mHolder.userPontos.setText(String.valueOf(user.getMoedas()));

        int moedas = Integer.parseInt(mHolder.userPontos.getText().toString());

        if ( moedas <= 1 ) {
            mHolder.userMoedasDesc.setText(activity.getResources().getString(R.string.moeda));
        }else {
            mHolder.userMoedasDesc.setText(activity.getResources().getString(R.string.moedas));
        }

        mHolder.userNivel.setText(user.isSocioTorcedor()? "Sócio Torcedor" : "Torcedor");
    }

    void initBadges (List<BadgeData> data){
        if(adapter == null || mHolder.medalsView.getAdapter() == null){
            lastData = false;
            page = 1;
            adapter = new BadgesAdapter(activity, data, new BadgesAdapter.Listener() {
                @Override
                public void OnClick(BadgeData data) {
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PERFIL_ALTERAR_BADGE_SELECIONAR, data.getId()).execute();
                    showBadge(data);
                }

                @Override
                public void OnEndOfList() {
                    if(!lastData){
                        page += 1;
                        badgeViewModel.getBadges(page);
                    }
                }
            });
            mHolder.medalsView.setAdapter(adapter);
            mHolder.medalsView.setLayoutManager(new GridLayoutManager(activity, 4));
        }else{
            if(data != null){
                adapter.addBadges(data);
            }
        }
    }

    void showBadge (BadgeData badgeData){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        //builder.setTitle("Name");

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_badge, null);
        ImageView img = customLayout.findViewById(R.id.img_badges);
        TextView tittle = customLayout.findViewById(R.id.txt_tittle);
        TextView description = customLayout.findViewById(R.id.txt_description);

        UTILS.getImageAt(activity, badgeData.getImg(), img);

        ColorMatrix color = new ColorMatrix();
        color.setSaturation(0f);

        if(badgeData.getOwned() == 1){

        }else{
            img.setColorFilter(new ColorMatrixColorFilter(color));
        }

        tittle.setText(badgeData.getNome());

        if(badgeData.getDesc() != null)
            description.setText(badgeData.getDesc());

        builder.setView(customLayout);

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    //region Item Picker
    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(activity, CameraActivity.class);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            if (file != null) {
                photoURI = Uri.fromFile(file);
                takePictureIntent.setData(photoURI);
                activity.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        activity.startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(activity, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(activity).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (type == "foto") {
                                mPresenter.cameraClick();
                            }else if(type == "galeria"){
                                mPresenter.ChooseGalleryClick();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                PerfilFragment.this.showErrorDialog();
            }
        })
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setTitle("Precisa de permissão");
        builder.setMessage("Dê permissão");
        builder.setPositiveButton("Escolha", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PerfilFragment.this.openSettings();
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void showNoSpaceDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setTitle("Erro");
        builder.setMessage("Sem espaço");
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);

    }

    @Override
    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTime().getTime();
        String mFileName = timeInMillis + ".jpeg";
        File mFilePath = getFilePath();

        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            return newFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(activity.getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        //Glide.with(getContext()).load(mFilePath).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        //Glide.with(getContext()).load(mFileUri).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    @Override
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = activity.getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    //endregion

    class ViewHolder {
        //Header
        ImageView userImage;
        TextView userName;
        TextView userPontos;
        TextView userMoedasDesc;
        TextView userNivel;
        ProgressBar progressBar;

        TextView trocarFoto;
        ImageView config;

        RecyclerView medalsView;

        public ViewHolder (View view){
            //Header
            userImage = view.findViewById(R.id.img_perfil);
            userName = view.findViewById(R.id.txt_perfil_nome);
            userPontos = view.findViewById(R.id.txt_club_points);
            userMoedasDesc = view.findViewById(R.id.txt_moedas_des);
            userNivel = view.findViewById(R.id.txt_perfil_nivel);
            progressBar = view.findViewById(R.id.progress_perfil_exp);

            trocarFoto = view.findViewById(R.id.view_trocar_foto);
            config = view.findViewById(R.id.btn_option);

            medalsView = view.findViewById(R.id.recycler_perfil_medals);
        }
    }
}
