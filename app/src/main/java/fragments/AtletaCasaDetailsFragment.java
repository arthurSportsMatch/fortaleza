package fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportm.fortaleza.R;

import data.AtletaData;
import utils.UTILS;

public class AtletaCasaDetailsFragment extends Fragment {

    ViewHolder mHolder;
    AtletaData atleta;

    public AtletaCasaDetailsFragment() {

    }

    public AtletaCasaDetailsFragment(AtletaData data) {
        this.atleta = data;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_atleta_casa_details, container, false);
        mHolder = new ViewHolder(view);
        //Colocar codigo aqui
        initAtletaData();
        return view;
    }

    void initAtletaData (){
        if(atleta != null){
            mHolder.golsCasa.setText(String.valueOf(atleta.getGols()));

            if(atleta.getEstreiaDesc()!= null)
                mHolder.jogoEstreia.setText(atleta.getEstreiaDesc() != null ? atleta.getEstreiaDesc(): "");

            if(atleta.getEstreia()!= null)
                mHolder.dataEstreia.setText(UTILS.formatDateFromDB(atleta.getEstreia()));
        }
    }

    class ViewHolder {
        TextView golsCasa;
        TextView dataEstreia;
        TextView jogoEstreia;

        public ViewHolder (View view){
            golsCasa = view.findViewById(R.id.txt_gols_casa);
            dataEstreia = view.findViewById(R.id.txt_estreia);
            jogoEstreia = view.findViewById(R.id.txt_jogo_estreia);
        }
    }
}
