package fragments;


import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.sportm.fortaleza.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.GraficValues;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;
import view.RadioChartView;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

public class AtletaChartFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    int idAtleta;
    boolean valuesAdjusted  = false;

    private static final int OP_ID_JOGADOR_CHART_0       = 0;
    private static final int OP_ID_JOGADOR_CHART_1       = 1;
    private static final int OP_ID_JOGADOR_CHART_2       = 2;
    private static final int OP_ID_JOGADOR_CHART_3       = 3;

    List<GraficValues> firt = new ArrayList<>();
    List<GraficValues> second = new ArrayList<>();
    List<GraficValues> tirth = new ArrayList<>();

    public AtletaChartFragment() {

    }

    public AtletaChartFragment(int id) {
        this.idAtleta = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_atleta_chart, container, false);
        mHolder = new ViewHolder(view);
        initRadio();
        return view;
    }

    void initRadio (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", idAtleta);
        params.put("t", 1);
        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_PLAYER_CHART, OP_ID_JOGADOR_CHART_1, this).execute(params);

        mHolder.radioGroup.check(mHolder.radioGroup.getChildAt(0).getId());

        mHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int position = group.getCheckedRadioButtonId();
                View radioButton = group.findViewById(position);
                int idx = group.indexOfChild(radioButton);

                switch (idx){
                    case 0 :{
                        //Geral
                        getFirtRadio();
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PERFIL_JOGADOR_DADOS, 1+"@"+idAtleta).execute();
                    }
                    break;
                    case 1 :{
                        //Titular
                        getSecondRadio();
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PERFIL_JOGADOR_DADOS, 2+"@"+idAtleta).execute();
                    }
                    break;
                    case 2 :{
                        //Reserva
                        getTirthRadio();
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PERFIL_JOGADOR_DADOS, 3+"@"+idAtleta).execute();
                    }
                    break;
                }
            }
        });
    }

    void getFirtRadio(){
        if(firt == null || firt.size() == 0){
            Hashtable<String, Object> params = new Hashtable<>();
            params.put("id", idAtleta);
            params.put("t", 1);
            new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_PLAYER_CHART, OP_ID_JOGADOR_CHART_1, this).execute(params);

        }else{
            changeRadioValues(firt);
        }
    }

    void getSecondRadio(){
        if(second == null || second.size() == 0){
            Hashtable<String, Object> params = new Hashtable<>();
            params.put("id", idAtleta);
            params.put("t", 2);
            new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_PLAYER_CHART, OP_ID_JOGADOR_CHART_2, this).execute(params);
        }else{
            changeRadioValues(second);
        }
    }

    void getTirthRadio(){
        if(tirth == null || tirth.size() == 0){
            Hashtable<String, Object> params = new Hashtable<>();
            params.put("id", idAtleta);
            params.put("t", 3);
            new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_PLAYER_CHART, OP_ID_JOGADOR_CHART_3, this).execute(params);
        }else{
            changeRadioValues(tirth);
        }
    }

    void changeRadioValues (List<GraficValues> values){
        mHolder.radio.changeValues(values);
    }

    List<GraficValues> adjustValues (List<GraficValues> values){
        for(int i = 0; i < 2; i++){
            values.add(values.get(0));
            values.remove(0);
        }
        return values;
    }

    //region AsyncOperation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_JOGADOR_CHART_1:{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            GraficValues[] data = gson.fromJson(obj.getString("itens"), GraficValues[].class);
                            List<GraficValues> values = new ArrayList<>(Arrays.asList(data));
                            if(data != null){
                                firt = adjustValues(values);
                                getFirtRadio();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
            case OP_ID_JOGADOR_CHART_2:{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            GraficValues[] data = gson.fromJson(obj.getString("itens"), GraficValues[].class);
                            List<GraficValues> values = new ArrayList<>(Arrays.asList(data));
                            if(data != null){
                                second = adjustValues(values);
                                getSecondRadio();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
            case OP_ID_JOGADOR_CHART_3:{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            GraficValues[] data = gson.fromJson(obj.getString("itens"), GraficValues[].class);
                            List<GraficValues> values = new ArrayList<>(Arrays.asList(data));
                            if(data != null){
                                tirth = adjustValues(values);
                                getTirthRadio();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        RadioChartView radio;
        RadioGroup radioGroup;

        public ViewHolder(View view){
            radio = view.findViewById(R.id.radio_dados_atleta);
            radioGroup = view.findViewById(R.id.reg_radio_atleta);
        }
    }
}
