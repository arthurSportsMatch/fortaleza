package fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;

import org.json.JSONObject;

import java.util.Hashtable;

import adapter.NotificationAdapter;
import data.FragmentData;
import data.NotificationData;
import utils.AsyncOperation;

public class NotificationsFragment extends Fragment {

    ViewHolder mHolder;
    NotificationAdapter adapter;
    boolean gethingNotification;
    int currentPage = 1;

    public static final int NOTIFICATION_TIPO_NONE             = 0;
    public static final int NOTIFICATION_TIPO_FALE_CONOSCO     = 1;
    public static final int NOTIFICATION_TIPO_POST_MURAL       = 2;
    public static final int NOTIFICATION_TIPO_NOTICIA          = 3;
    public static final int NOTIFICATION_TIPO_QUIZ             = 4;
    public static final int NOTIFICATION_TIPO_PESQUISA         = 5;
    public static final int NOTIFICATION_TIPO_INFORMATIVO      = 6;
    public static final int NOTIFICATION_TIPO_RECEITA          = 7;
    public static final int NOTIFICATION_TIPO_TREINAMENTO      = 8;
    public static final int NOTIFICATION_TIPO_EVENTO           = 10;

    public NotificationsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        mHolder = new ViewHolder(view);


        //((MainMenuActivity) getActivity()).changeToolbarName("Notificações");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            adapter = null;
        }
        getNotification(currentPage);
    }

    void getNotification (int page){
        gethingNotification = true;
        mHolder.load.setVisibility(View.VISIBLE);

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);


//        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_NOTIFICATIONS, 0, new AsyncOperation.IAsyncOpCallback() {
//            @Override
//            public void CallHandler(int opId, JSONObject response, boolean success) {
//                if(success){
//                    this.OnAsyncOperationSuccess(opId, response);
//                }else{
//                    this.OnAsyncOperationError(opId, response);
//                }
//            }
//
//            @Override
//            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
//                gethingNotification = false;
//                mHolder.load.setVisibility(View.GONE);
//
//                if(response.has("Object")){
//                    try {
//                        Gson gson = new Gson();
//                        NotificationData[] data = gson.fromJson(response.getString("Object"),NotificationData[].class);
//                        List<NotificationData> list = new ArrayList<>(Arrays.asList(data));
//
//                        if(adapter == null){
//                            adapter = new NotificationAdapter(getContext(),list, new NotificationAdapter.Interaction() {
//                                @Override
//                                public void isPressed(NotificationData data) {
//                                    RedirectNotification(data);
//                                }
//
//                                @Override
//                                public void onBottom() {
//                                    currentPage ++;
//                                    getNotification(currentPage);
//                                }
//                            });
//                            mHolder.load.setVisibility(View.GONE);
//                            mHolder.notificationList.setAdapter(adapter);
//                            mHolder.notificationList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
//                        }else{
//                            adapter.addData(list);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }else{
//                    mHolder.noNotification.setVisibility(View.VISIBLE);
//                }
//            }
//            @Override
//            public void OnAsyncOperationError(int opId, JSONObject response) {
//                mHolder.noNotification.setVisibility(View.VISIBLE);
//            }
//        }).execute(params);
    }

    void RedirectNotification(NotificationData data){

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idPush", data.getIdPush());


        switch (data.getTipo()){
            default:
            case NOTIFICATION_TIPO_RECEITA: {
                //new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_SET_NOTIFICATION, 999, emptyAsyncOpCallback, TRACKING.TRACKING_NOTIFICATION_RECEITA, data.getId()).execute(params);
                if((data.getId() <= 0) ){
                    Toast.makeText(getContext(), R.string.notification_cant_redirect, Toast.LENGTH_LONG).show();
                    return;
                }
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitaFragment(data.getId()), "Receitas"), -1);
            }
            break;

            case NOTIFICATION_TIPO_FALE_CONOSCO: {
               // new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_SET_NOTIFICATION, 999, emptyAsyncOpCallback, TRACKING.TRACKING_NOTIFICATION_FALECONOSCO, data.getId()).execute(params);
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new FaleConoscoFragment(), "Receitas"), -1);
            }
            break;

            case NOTIFICATION_TIPO_TREINAMENTO: {

                if( (data.getV() <= 0) ){
                    Toast.makeText(getContext(), R.string.notification_cant_redirect, Toast.LENGTH_LONG).show();
                    return;
                }

                if( (data.getV() <= 0) ){
                    Toast.makeText(getContext(), R.string.notification_cant_redirect, Toast.LENGTH_LONG).show();
                    return;
                }
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitaFragment(data.getV()), "Receitas"), -1);
            }
            break;
        }
    }

    AsyncOperation.IAsyncOpCallback emptyAsyncOpCallback = new AsyncOperation.IAsyncOpCallback() {
        @Override
        public void CallHandler(int opId, JSONObject response, boolean success) {

        }

        @Override
        public void OnAsyncOperationSuccess(int opId, JSONObject response) {

        }

        @Override
        public void OnAsyncOperationError(int opId, JSONObject response) {

        }
    };

    class ViewHolder {
        TextView noNotification;
        RecyclerView notificationList;
        ProgressBar load;

        public ViewHolder (View view){
            notificationList = view.findViewById(R.id.list_notification);
            load = view.findViewById(R.id.progress_notification);
            noNotification = view.findViewById(R.id.txt_no_notification);
        }
    }

}
