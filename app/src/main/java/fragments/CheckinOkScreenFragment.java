package fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportm.fortaleza.R;

public class CheckinOkScreenFragment extends Fragment {


    ViewHolder mHolder;
    String tittle, message;

    public CheckinOkScreenFragment() {
        // Required empty public constructor
    }

    public CheckinOkScreenFragment(String tittle, String message) {
        this.tittle = tittle;
        this.message = message;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkin_ok_screen, container, false);
        mHolder = new ViewHolder(view);

        mHolder.tittle.setText(tittle);
        mHolder.message.setText(message);

        return view;
    }

    class ViewHolder {
        TextView tittle, message;
        public ViewHolder (View view){
            tittle = view.findViewById(R.id.txt_tittle);
            message = view.findViewById(R.id.txt_messsage);
        }
    }

}
