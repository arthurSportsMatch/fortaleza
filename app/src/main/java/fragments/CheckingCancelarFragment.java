package fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.text.DecimalFormat;

import data.CheckinData;
import data.CheckinPartidaData;
import utils.CONSTANTS;
import utils.UTILS;


public class CheckingCancelarFragment extends MainFragment {

    ViewHolder mHolder;
    CheckinData checking;
    CheckinPartidaData partida;

    public CheckingCancelarFragment() {
        // Required empty public constructor
    }

    public CheckingCancelarFragment(CheckinData checking, CheckinPartidaData partida) {
        this.checking = checking;
        this.partida = partida;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checking_cancelar, container, false);
        mHolder = new ViewHolder(view);

        if(partida != null){
            uploadPartida();
        }


        if(checking != null) {
            atualizarData();
        }


        return view;
    }


    //Atualiza partidas
    void uploadPartida (){
        Glide.with(activity)
                .load(R.drawable.logo_fortaleza)
                .into(mHolder.img1);

        Glide.with(activity)
                .load(CONSTANTS.apiServer + partida.getLogo_adversario())
                .into(mHolder.img2);

        mHolder.title.setText(partida.getCampeonato());
        mHolder.date.setText(UTILS.formatDateFromDB(partida.getData()));
        mHolder.hour.setText(UTILS.dateZuluToGMT(partida.getHora()));
        mHolder.local.setText(partida.getEstadio());
    }

    //Atualiza a quantidade de ingressos
    public void atualizarData (){
        for(int i = 0; i < checking.getNomes().size(); i ++){
            createCheckingItem(checking.getCadeiras() != null? checking.getCadeiras().get(i): null, checking.getSetorName(), checking.getNomes().get(i), checking.getIngressosDefinido().get(i), Float.parseFloat(checking.getValores().get(i)));
        }
    }

    public void createCheckingItem (@Nullable String acento, String setor, String nome, String ingresso, float valor){
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_checkin_list_user_unit,mHolder.viewListaChecking, false);
        TextView txtAcento, txtSetor, txtNome, txtIngresso, txtValor;
        txtAcento = view.findViewById(R.id.txt_assento);
        txtSetor = view.findViewById(R.id.txt_setor_tittle);
        txtNome = view.findViewById(R.id.txt_nome);
        txtIngresso = view.findViewById(R.id.txt_ingresso);
        txtValor = view.findViewById(R.id.txt_valor);

        String setorConcat = "";
        if(acento != null){
            txtAcento.setText(acento);
            if(acento.length() >= 2){
                setorConcat = " - Fileira % Assento #";
                String fileira = acento.substring(0,1);
                String assento = acento.substring(1, acento.length());
                setorConcat = setorConcat.replace("%", fileira);
                setorConcat = setorConcat.replace("#", assento);
            }
        }else{
            txtAcento.setVisibility(View.GONE);
        }
        String finalSetor = setor + setorConcat;
        txtSetor.setText(finalSetor);
        txtNome.setText(nome);
        txtIngresso.setText(ingresso);
        txtValor.setText(getValor(valor));

        mHolder.viewListaChecking.addView(view);
    }

    String getValor (float valor){
        String result = "R$ @";

        DecimalFormat df = new DecimalFormat("###,##0.00");

        String formattedNumber = df.format(valor);
        result = result.replace("@", formattedNumber);
        return result;
    }

    class ViewHolder {
        View itemView;
        ImageView img1;
        ImageView img2;
        TextView title;
        TextView date;
        TextView hour;
        TextView local;

        LinearLayout viewListaChecking;

        public ViewHolder (View view){
            itemView = view.findViewById(R.id.view_jogo);
            img1 = itemView.findViewById(R.id.img_time_1);
            img2 = itemView.findViewById(R.id.img_time_2);
            title = itemView.findViewById(R.id.txt_tipo_jogo);
            date = itemView.findViewById(R.id.txt_jogo_data);
            hour = itemView.findViewById(R.id.txt_jogo_time);
            local = itemView.findViewById(R.id.txt_jogo_local);

            viewListaChecking = view.findViewById(R.id.view_lista_checkin);
        }
    }

}
