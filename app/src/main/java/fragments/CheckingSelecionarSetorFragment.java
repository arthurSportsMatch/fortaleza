package fragments;


import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.sportm.fortaleza.R;

import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import adapter.EstadioPageAdapter;
import data.CheckinSetoresData;
import pager.FadePageTransformer;
import utils.UILoadingController;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckingSelecionarSetorFragment extends MainFragment {

    ViewHolder mHolder;
    Listener listener;
    List<CheckinSetoresData> setores;
    String selectedSetorTabName;

    boolean selectiongAssento = false;
    String selectedAssentoTabName;

    public CheckingSelecionarSetorFragment() {
        // Required empty public constructor
    }

    public CheckingSelecionarSetorFragment(Listener listener) {
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_checking_selecionar_setor, container, false);

        mHolder = new ViewHolder(view);
        View loadingView = view.findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.OnLoad();
            }
        });

        initLoading();
        return view;
    }

    void initLoading (){
        listener.OnLoad();
        uiLoadingController.onLoad();
    }

    void initAction (){
        mHolder.tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedSetorTabName = Objects.requireNonNull(tab.getText()).toString();
                changeTabSelectionColor(getColor(selectedSetorTabName));
                changeImage(selectedSetorTabName);
                checkSelection();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void atualizarData(List<CheckinSetoresData> data){
        uiLoadingController.finishLoader(true);
        listener.OnUnlock();
        setores = data;
        EstadioPageAdapter estadioPageAdapter = new EstadioPageAdapter(activity, data);
        mHolder.pager.setAdapter(estadioPageAdapter);
        mHolder.pager.setPageTransformer(true, new FadePageTransformer());
        mHolder.tabLayout.setupWithViewPager(mHolder.pager);

        selectedSetorTabName = Objects.requireNonNull(mHolder.tabLayout.getTabAt(mHolder.pager.getCurrentItem()).getText()).toString();
        changeTabSelectionColor(getColor(selectedSetorTabName));
        changeImage(selectedSetorTabName);
        checkSelection();

        initAction();

    }

    void checkSelection(){
        CheckinSetoresData seletor = getSetor(selectedSetorTabName);
        if(seletor.getMarcar_assento().equals("1")){
            selectiongAssento = true;
            listener.OnFinished(selectedSetorTabName,seletor.getId(), 1);
        }else{
            listener.OnFinished(selectedSetorTabName,seletor.getId());
        }
    }

    void checkSelection(String name){
        CheckinSetoresData seletor = getSetor(name);
        if(seletor.getMarcar_assento().equals("1")){
            selectiongAssento = true;
            listener.OnFinished(selectedSetorTabName,seletor.getId(), 1);
        }else{
            listener.OnFinished(selectedSetorTabName,seletor.getId());
        }
    }

    CheckinSetoresData getSetor (String setorName){
        CheckinSetoresData setor = new CheckinSetoresData();
        for(CheckinSetoresData d : setores){
            if(d.getNome().equals(setorName)){
                setor = d;
                break;
            }
        }
        return setor;
    }

    void changeImage (String setor){

    }

    int getColor (String setor){
        int color = R.color.colorAccent;
        switch (setor.toLowerCase()){
            case "premium" : color = R.color.colorPremium;
                break;
            case "especial" : color = R.color.colorEspecial;
                break;
            case "bossa nova" : color = R.color.colorBossaNova;
                break;
            case "inferior sul" : color = R.color.colorInferiorSul;
                break;
            case "superior sul" : color = R.color.colorSuperiorSul;
                break;
            case "camarotes" : color = R.color.colorCamarotes;
                break;
            case "superior norte" : color = R.color.colorSuperiorNorte;
                break;
            case "superior central" : color = R.color.colorSuperiorCentro;
                break;
            case "inferior norte" : color = R.color.colorInferiorNorte;
                break;
        }
        return color;
    }

    //Muda a cor do texto selecionavel
    void changeTabSelectionColor (int color){
        mHolder.tabLayout.setTabTextColors (activity.getResources().getColor(R.color.colorGray),activity.getResources().getColor(color));
    }

    public interface Listener {
        void OnLoad ();
        void OnFinished(String setorName, int setorID);
        void OnFinished(String setorName, int setorID, int assentoID);
        void OnUnlock();
    }

    class ViewHolder {
        View mainView;
        ViewPager pager;
        TabLayout tabLayout;

        public ViewHolder(View view){
            mainView = view;
            tabLayout = view.findViewById(R.id.tab_setor);
            pager = view.findViewById(R.id.pager_estadio);
        }
    }
}
