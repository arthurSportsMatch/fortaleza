package fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import adapter.SearchViewAdapter;
import data.FragmentData;
import data.SearchData;
import utils.TRACKING;

public class MenuFragment extends MainFragment {

    private ViewHolder mHolder;
    private boolean isSearching = false;
    private List<SearchData> buscas = new ArrayList<>();
    private SearchViewAdapter adapter;

    public MenuFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        mHolder = new ViewHolder(view);
        //initActions();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notification, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:{
                //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_MENU_OPENNOTIFICATION).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new NotificationsFragment(), "Notificações"), -1);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initActions (){
        mHolder.serchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!isSearching){
                    search(String.valueOf(s));
                }
            }
        });

        //Abre mural de receitas que tenham calculadora
        mHolder.calculadora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_MENU_ABRIRCALCULADORA).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitasMuralFragment(true), "Calculadora"), 2);
            }
        });

        //Abre fale conosco
        mHolder.faleConosco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_MENU_ABRIRFALECONOSCO).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new FaleConoscoFragment(),"Fale Conosco"), 2);
            }
        });

        //Abre receitas padrão
        mHolder.receitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_MENU_ABRIRRECEITAS).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitasMuralFragment(),"Receitas"), 2);
            }
        });

        //Abre treinamentos
        mHolder.treinamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_MENU_ABRIRRECEITAS).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new TreinamentosMuralFragment(),"Treinamentos"), 2);
            }
        });
    }

    void search (String query){

        if(query.isEmpty()){
            //btnClear.setVisibility(View.INVISIBLE);
            //txtTotalResult.setVisibility(View.INVISIBLE);
            mHolder.listView.setAdapter(null );
            if(adapter != null) {
                adapter = null;
            }
            return;
        }
        isSearching = true;

        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("search", query);

//        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_SEARCH, 0, new AsyncOperation.IAsyncOpCallback() {
//            @Override
//            public void CallHandler(int opId, JSONObject response, boolean success) {
//
//                try {
//                    Log.d("Search-Result", String.valueOf(response.get("Object")));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                if(success){
//                    this.OnAsyncOperationSuccess(opId, response);
//                }else{
//                    this.OnAsyncOperationError(opId, response);
//                }
//            }
//
//            @Override
//            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
//                isSearching = false;
//                try {
//                    if (response.has("Object")) {
//                        Gson gson  = new Gson();
//                        SearchData[] data = gson.fromJson(response.get("Object").toString(), SearchData[].class);
//                        buscas = Arrays.asList(data);
//
//                        if ((buscas != null) && (buscas.size() > 0)) {
//                            ConfigureSearches(buscas);
//                        }
//                        else {
//                            //ConfigureNoResult();
//                        }
//                    }
//                } catch (JSONException e) {
//                    UTILS.DebugLog("Error", e);
//                }
//            }
//
//            @Override
//            public void OnAsyncOperationError(int opId, JSONObject response) {
//
//            }
//        }, TRACKING.TRACKING_MENU_BUSCA).execute(params);
    }

    void ConfigureSearches(List<SearchData> list){

        adapter = new SearchViewAdapter(getActivity(), R.layout.adapter_search_results, list, new SearchViewAdapter.ISearchSelectable() {
            @Override
            public void OnSearchSelected(SearchData item) {
                switch (item.getIdTipo()){
                    default:
                        break;
                    case TRACKING.TRACKING_BUSCA_NOTICIA:
                        //CallNews();
                        break;
                    case TRACKING.TRACKING_BUSCA_MURAL:
                        //CallWall();
                        break;
                    case TRACKING.TRACKING_BUSCA_RECEITAS:
                        CallRecipes();
                        break;
                    case TRACKING.TRACKING_BUSCA_CATALOGO:
                        //CallCatalog();
                        break;
                    case TRACKING.TRACKING_BUSCA_TREINAMENTO:
                        //CallTrainings();
                        break;
                    case TRACKING.TRACKING_BUSCA_PRODUTOS:
                        //CallProductList();
                        break;
                    case TRACKING.TRACKING_BUSCA_INFORMATIVOS:
                        //CallInformatives();
                        break;
                    case TRACKING.TRACKING_BUSCA_PREMIOS:
                        //CallAwards();
                        break;
                    case TRACKING.TRACKING_BUSCA_AGENDA:
                        //CallAgenda();
                        break;
                }
            }
        });

        mHolder.listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        int tot = 0;

        for(SearchData sd : list){
            tot += sd.getTot();
        }
    }

    void CallRecipes (){
        MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitasMuralFragment(mHolder.serchBar.getText().toString()), "Receitas"), -1);
    }

    class ViewHolder {
        EditText serchBar;

        ListView listView;

        View calculadora;
        View receitas;
        View faleConosco;
        View treinamentos;

        public ViewHolder (View view){
            serchBar = view.findViewById(R.id.edt_menu_search);

            listView = view.findViewById(R.id.list_results);

            calculadora = view.findViewById(R.id.calculadora);
            receitas = view.findViewById(R.id.receitas);
            faleConosco = view.findViewById(R.id.fale_conosco);
            treinamentos = view.findViewById(R.id.treinamentos);
        }
    }
}
