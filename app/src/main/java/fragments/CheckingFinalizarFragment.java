package fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import data.CheckinData;
import data.CheckinPartidaData;
import utils.CONSTANTS;
import utils.MaskEditUtil;
import utils.UTILS;

import static utils.UTILS.convertMonetaryValue;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckingFinalizarFragment extends MainFragment {

    ViewHolder mHolder;
    CheckinData checking;
    Listener listener;

    public CheckingFinalizarFragment() {
        // Required empty public constructor
    }

    public CheckingFinalizarFragment(CheckinData checking, Listener listener) {
        this.checking = checking;
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_checking_finalizar, container, false);
        mHolder = new ViewHolder(view);
        atualizarData();
        return view;
    }

    //Atualiza a quantidade de ingressos
    public void atualizarData (){

        for(int i = 0; i < checking.getNomes().size(); i ++){
            if ( !checking.isHasCadeiras() ) {
                checking.setCadeiras(null);
                checking.setCadeiraID(null);
            }
            createCheckingItem(checking.getCadeiras() != null? checking.getCadeiras().get(i): null, checking.getSetorName(), checking.getNomes().get(i), checking.getIngressosDefinido().get(i), Float.parseFloat(checking.getValores().get(i)));
        }
        listener.OnUnlock();
    }

    public void createCheckingItem (@Nullable String acento, String setor, String nome, String ingresso, float valor){
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_checkin_list_user_unit,mHolder.viewListaChecking, false);
        TextView txtAcento, txtSetor, txtNome, txtIngresso, txtValor;
        txtAcento = view.findViewById(R.id.txt_assento);
        txtSetor = view.findViewById(R.id.txt_setor_tittle);
        txtNome = view.findViewById(R.id.txt_nome);
        txtIngresso = view.findViewById(R.id.txt_ingresso);
        txtValor = view.findViewById(R.id.txt_valor);

        String setorConcat = "";
        if(acento != null){
            txtAcento.setText(acento);
            if(acento.length() >= 2){
                setorConcat = " - Fileira % Assento #";
                String fileira = acento.substring(0,1);
                String assento = acento.substring(1, acento.length());
                setorConcat = setorConcat.replace("%", fileira);
                setorConcat = setorConcat.replace("#", assento);
            }
        }else{
            txtAcento.setVisibility(View.GONE);
        }
        String finalSetor = setor + setorConcat;
        txtSetor.setText(finalSetor);
        txtNome.setText(nome);
        txtIngresso.setText(ingresso);
        txtValor.setText(convertMonetaryValue(valor, "pt", "BR"));

        mHolder.viewListaChecking.addView(view);
    }

    public interface Listener {
        void OnUnlock();
    }

    class ViewHolder {
        LinearLayout viewListaChecking;
        public ViewHolder(View view){
            viewListaChecking = view.findViewById(R.id.view_lista_checkin);
        }
    }

}
