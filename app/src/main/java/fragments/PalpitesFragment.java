package fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.PalpitesListActivity;
import com.sportm.fortaleza.R;
import com.google.android.gms.ads.AdListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterPlayerSelector;
import data.AtletaData;
import data.PalpiteData;
import data.PalpitePerguntaData;
import data.PositonData;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.OnLoadListener;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * A simple {@link Fragment} subclass.
 */
public class PalpitesFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    Hashtable<Integer, View> instantiatedViews = new Hashtable<>();
    Hashtable<Integer, String> respostas = new Hashtable<>(); //usado para cetar respostas
    Dialog loading;

    PalpiteData palpiteData;
    AdapterPlayerSelector adapter;
    View actualView;
    int idPalpite;
    int actualPage = 0;
    int maxPages;
    boolean hasResponded = false;
    boolean secondView = false;

    int posID,tipo;
    TextView atletaSelector;
    ViewHolder mHolder;

    private static final int OP_GET_PALPITE              = 0;
    private static final int OP_SET_PALPITE              = 1;
    private static final int OP_GET_ATLETAS              = 2;

    public PalpitesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_palpites, container, false);
        mHolder = new ViewHolder(view);

        getParpite();
        initActions();
        return view;
    }

    void initActions (){
        mHolder.left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_VOLTAR, palpiteData.getPerguntas().get(actualPage-1).getId()).execute();
                previusView();
                checkPosition();
            }
        });

        mHolder.right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_PROXIMO, palpiteData.getPerguntas().get(actualPage-1).getId()).execute();
                nextView();
                checkPosition();
            }
        });

        mHolder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasResponded){
                    if(actualPage == maxPages){
                        //Finalizar
                        if(checkAnswer()){
                            //hasResponded = true;
                            //createOkScren(palpiteData);
                            setAnswer();
                            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_FINALIZAR, idPalpite).execute();
                        }else{
                            Toast.makeText(activity, "Responda todos os palpites", Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }else

                    if(actualPage == 0){
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_PARTICIPAR, idPalpite).execute();
                        AdMobController.startBeforeAd(activity, new AdListener() {
                            @Override
                            public void onAdClosed() {
                                nextView();
                                mHolder.indicator.setVisibility(View.VISIBLE);
                                checkPosition();
                            }
                        }, AdMobController.PALPITES_INTERSECTIAL_ID);
                        return;

                    }else{
                        //Adicionar
                        nextView();
                        checkPosition();
                        return;
                    }
                }else{
                    if(actualPage == 0){
                        nextView();
                        mHolder.btn.setText("Voltar");
                        mHolder.indicator.setVisibility(View.VISIBLE);
                        checkPosition();
                        return;
                    }else if(actualPage == maxPages){
                        if(!secondView){
                            mHolder.frameLayout.removeAllViews();
                            mHolder.btn.setText("Ver Palpite");
                            actualPage = 0;
                            getParpite();
                            secondView = true;
                        }
                    }else{
                        //botão de voltar
                        actualPage = 0;
                        mHolder.btn.setText("Ver Palpite");
                        mHolder.indicator.setVisibility(View.INVISIBLE);
                        getInstantiatedView(actualPage);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_VER_PALPITES, idPalpite).execute();
                        return;
                    }

                }
            }
        });

        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_LISTA_VOLTAR, idPalpite).execute();
                mHolder.atletas.setVisibility(View.GONE);
            }
        });
    }

    void setAnswer (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idPalpite", idPalpite);
        params.put("palpites", parceAnswer());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_PALPITES, OP_SET_PALPITE, this).execute(params);
    }

    void getParpite (){
        actualPage = 0;
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PALPITES, OP_GET_PALPITE, this).execute();
        loading = OnLoadListener.createLoadDialog(activity);
    }

    //Checa a posição do menu
    void checkPosition (){
        String actualPosition = actualPage + "/" +maxPages;
        mHolder.quant.setText(actualPosition);


        //Left
        if(actualPage > 1){
            if(mHolder.left.getVisibility() == View.INVISIBLE){
                mHolder.left.setVisibility(View.VISIBLE);
                mHolder.btn.setText(!hasResponded? "Próximo": "Voltar");
            }
        }else{
            if(mHolder.left.getVisibility() == View.VISIBLE){
                mHolder.left.setVisibility(View.INVISIBLE);
                mHolder.btn.setText(!hasResponded? "Próximo": "Voltar");
            }
        }

        //Right
        if(actualPage < maxPages){
            if(mHolder.right.getVisibility() == View.INVISIBLE){
                mHolder.right.setVisibility(View.VISIBLE);
                mHolder.btn.setText(!hasResponded? "Próximo": "Voltar");
            }
        }else{
            if(mHolder.right.getVisibility() == View.VISIBLE){
                mHolder.right.setVisibility(View.INVISIBLE);
                mHolder.btn.setText(!hasResponded? "Finalizar": "Voltar");
            }
        }


    }

    //Adicionar view
    void addViews (PalpiteData data){
        maxPages = data.getPerguntas().size();
        idPalpite = data.getId();
        instantiateFirtView(data);

        for(int i = 0; i < data.getPerguntas().size(); i ++){
            int tipo = data.getPerguntas().get(i).getTipo();
            instantiateView (i+1, tipo, data.getPerguntas().get(i), data);
        }
    }

    //Next view
    void nextView (){
        actualPage ++;
        getInstantiatedView(actualPage);
    }

    //Voltar view
    void previusView (){
        actualPage --;
        getInstantiatedView(actualPage);
    }

    //Instancia a primeir view
    void instantiateFirtView(PalpiteData palpite){
        View view = LayoutInflater.from(activity).inflate(R.layout.include_palpite_tipo_0, mHolder.frameLayout, false);
        TextView campeonato = view.findViewById(R.id.txt_tittle);
        TextView rodada = view.findViewById(R.id.txt_rodada);
        TextView message = view.findViewById(R.id.txt_message);
        TextView expiration = view.findViewById(R.id.txt_expiration);

        ImageView clubImage1 = view.findViewById(R.id.img_club_1);
        TextView club1 = view.findViewById(R.id.txt_club_1);

        ImageView clubImage2 = view.findViewById(R.id.img_club_2);
        TextView club2 = view.findViewById(R.id.txt_club_2);
        Button verTodos = view.findViewById(R.id.btn_ver_todos);



        if(palpite.getMore() != null){
            verTodos.setVisibility(View.VISIBLE);
            verTodos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, PalpitesListActivity.class);
                    startActivity(intent);
                }
            });
        }

        //valores principais
        campeonato.setText(palpite.getCampeonato());

        String[] dataPart = palpite.getExpire().split(" ");
        String firtPart = "DISPONÍVEL ATÉ " + UTILS.formatDateFromDB(dataPart[0]);
        String rodadaText = palpite.getRodada() + " - " + UTILS.formatDateFromDB(dataPart[0]).substring(0,5);
        String secondPart = " ás " + UTILS.formatDateFromDBEvent(dataPart[1].substring(0,5));
        expiration.setText(firtPart + secondPart);
        rodada.setText(rodadaText);

        String messageText = message.getText().toString();
        messageText = messageText.replace("#", String.valueOf(palpite.getCoins()));
        message.setText(messageText);

        UTILS.getImageAt(activity, palpite.getImgClube1(), clubImage1);

        UTILS.getImageAt(activity, palpite.getImgClube2(), clubImage2);

        mHolder.frameLayout.addView(view);
        putView(0, view);
        actualView = view;


        if(!hasResponded){
            if(!checkExpired(palpite.getExpire())){
                mHolder.btn.setAlpha(0.5f);
                mHolder.btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
        }
    }

    boolean checkExpired (String expireTime){
        String expire = expireTime.replace("-", "");
        expire = expire.replace(":", "");
        expire = expire.replace(" ", "");
        expire = expire.substring(0,12);
        Double value1 = Double.valueOf(expire);//Integer.parseInt();

        final Date calendarDate = Calendar.getInstance().getTime();
        String full = new SimpleDateFormat("yyyyMMddHHmm").format(calendarDate);
        Double value2 = Double.valueOf(full);//Integer.parseInt(full);


        Double result =  value1 - value2;
        Log.d("Result-Expire", String.valueOf(result));

        if(result > 0){
            return true;
        }

        return false;
    }

    //Instancia a view
    void instantiateView (final int pos, final int tipo, final PalpitePerguntaData data, PalpiteData palpite){
        switch (tipo){
            case 1 : {
                //tipo 1
                View view = LayoutInflater.from(activity).inflate(R.layout.include_palpite_tipo_1, mHolder.frameLayout, false);
                TextView campeonato = view.findViewById(R.id.txt_tittle);
                TextView rodada = view.findViewById(R.id.txt_rodada);
                TextView pergunta = view.findViewById(R.id.txt_message);

                ImageView clubImage1 = view.findViewById(R.id.img_club_1);
                TextView club1 = view.findViewById(R.id.txt_club_1);
                final EditText editClub1 = view.findViewById(R.id.edt_club_1);

                ImageView clubImage2 = view.findViewById(R.id.img_club_2);
                TextView club2 = view.findViewById(R.id.txt_club_2);
                final EditText editClub2 = view.findViewById(R.id.edt_club_2);

                String[] dataPart = palpite.getExpire().split(" ");
                String rodadaText = palpite.getRodada() + " - " + UTILS.formatDateFromDB(dataPart[0]).substring(0,5);
                rodada.setText(rodadaText);

                //valores principais
                campeonato.setText(palpite.getCampeonato());
                pergunta.setText(data.getPergunta());

                club1.setText(palpite.getClube1());
                club2.setText(palpite.getClube2());

                UTILS.getImageAt(activity, palpite.getImgClube1(), clubImage1);

                UTILS.getImageAt(activity, palpite.getImgClube2(), clubImage2);


                if(data.getV1() != null){
                    editClub1.setText(data.getV1());
                    editClub1.setFocusable(false);
                }

                if(data.getV2() != null){
                    editClub2.setText(data.getV2());
                    editClub2.setFocusable(false);
                }

                final String[] value1 = {""};
                final String[] value2 = {""};

                editClub1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        value1[0] = editClub1.getText().toString();
                        String answer = data.getId() + ":" + value1[0]+"/"+value2[0];
                        setAnswer(pos, answer);
                    }
                });

                editClub2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        value2[0] = editClub2.getText().toString();
                        String answer = data.getId() + ":" + value1[0]+"/"+value2[0];
                        setAnswer(pos, answer);
                    }
                });

                mHolder.frameLayout.addView(view);
                putView(pos, view);
                view.setVisibility(View.GONE);
            }
            break;
            case 2 : {
                //tipo 2
                View view = LayoutInflater.from(activity).inflate(R.layout.include_palpite_tipo_2, mHolder.frameLayout, false);
                TextView campeonato = view.findViewById(R.id.txt_tittle);
                TextView rodada = view.findViewById(R.id.txt_rodada);
                TextView pergunta = view.findViewById(R.id.txt_message);
                RadioGroup radioGroup = view.findViewById(R.id.radio_palpites);


                //valores principais
                campeonato.setText(palpite.getCampeonato());
                rodada.setText(palpite.getRodada());
                pergunta.setText(data.getPergunta());

                if(data.getV1() != null){
                    int id = Integer.parseInt(data.getV1());
                    RadioButton button1 = (RadioButton)radioGroup.getChildAt(1);
                    RadioButton button2 = (RadioButton)radioGroup.getChildAt(0);

                    button1.setEnabled(false);
                    button1.setFocusable(false);
                    button2.setEnabled(false);
                    button2.setFocusable(false);

                    switch (id){
                        case 0:{
                            button1.toggle();
                        }
                        break;
                        case 1:{
                            button2.toggle();
                        }
                        break;
                    }

                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {

                        }
                    });
                }

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        int position = group.getCheckedRadioButtonId();
                        View radioButton = group.findViewById(position);
                        int idx = group.indexOfChild(radioButton);
                        String buttonID = idx + "@" + data.getId();
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_SELECIONAR, buttonID).execute();
                        switch (idx){
                            case 0: {

                                String answer = data.getId() + ":" + 1;
                                setAnswer(pos, answer);
                            }
                            break;
                            case 1:{
                                String answer = data.getId() + ":" + 0;
                                setAnswer(pos, answer);
                            }
                            break;
                        }
                    }
                });

                mHolder.frameLayout.addView(view);
                putView(pos, view);
                view.setVisibility(View.GONE);
            }
            break;
            case 3 : {
                //tipo 3
                View view = LayoutInflater.from(activity).inflate(R.layout.include_palpite_tipo_3, mHolder.frameLayout, false);
                TextView campeonato = view.findViewById(R.id.txt_tittle);
                TextView rodada = view.findViewById(R.id.txt_rodada);
                TextView pergunta = view.findViewById(R.id.txt_message);

                final TextView atleta = view.findViewById(R.id.txt_jogador);

                //valores principais
                campeonato.setText(palpite.getCampeonato());
                rodada.setText(palpite.getRodada());
                pergunta.setText(data.getPergunta());

                atleta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                if(data.getV1() != null){
                    atleta.setText(data.getV2());
                    atleta.setEnabled(false);

                    atleta.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }

                atleta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_SELETOR, idPalpite).execute();
                        atletaSelector = atleta;
                        getAtletas(pos, data.getId());
                    }
                });

                mHolder.frameLayout.addView(view);
                putView(pos, view);
                view.setVisibility(View.GONE);
            }
            break;
        }
    }

    //Seta a resposta
    void setAnswer (int pos, String value){
        if(checkIfExistAnswer(pos)){
            respostas.put(pos, value);
        }else{
            respostas.put(pos, value);
        }
    }

    //Verifica se a resposta existe
    boolean checkIfExistAnswer (int pos){
        if(respostas.containsKey(pos)){
            return true;
        }else{
            return false;
        }
    }

    //coloca a view numa lista
    void putView (int pos, View view){
        instantiatedViews.put(pos, view);
        //view.setVisibility(View.GONE);
    }

    //pega a view numa lista
    View getInstantiatedView (int pos){
        //Se a view atual existir
        if(instantiatedViews.containsKey(pos)){
            //Deixa a view antiga como gone
            if(actualView != null){
                if(actualView.getVisibility() == View.VISIBLE){
                    actualView.setVisibility(View.GONE);
                }
            }
            //muda a view e deixa ela visivel
            View view = instantiatedViews.get(pos);
            actualView = view;
            actualView.setVisibility(View.VISIBLE);
        }

        return actualView;
    }

    void getAtletas (int pos, int tipo){
        this.posID = pos;
        this.tipo = tipo;

        if(adapter == null){
            new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PLAYERS_PALPITES, OP_GET_ATLETAS, this).execute();
        }else{
            mHolder.atletas.setVisibility(View.VISIBLE);
        }

    }

    //Inicia os atletas
    void initAtletas (List<AtletaData> data, List<PositonData> pos){
        mHolder.atletas.setVisibility(View.VISIBLE);
        adapter = new AdapterPlayerSelector(activity, data, pos, new AdapterPlayerSelector.Listener() {
            @Override
            public void getAtleta(AtletaData atleta) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PALPITES_LISTA_SELECIONAR, atleta.getId()).execute();
                atletaSelector.setText(atleta.getNome());
                String answer = tipo + ":" + atleta.getId();
                setAnswer(posID, answer);
                mHolder.atletas.setVisibility(View.GONE);
            }
        });
        mHolder.recyclerView.setAdapter(adapter);
        mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
    }



    boolean checkAnswer (){
        if(respostas.size() >= maxPages){
            return true;
        }
        return false;
    }

    String parceAnswer (){
        String finalAnswer = "";
        for(String s : respostas.values()){
            finalAnswer = finalAnswer + s + ";";
        }
        return finalAnswer;
    }

    void createOkScren (PalpiteData palpite){
        if(hasResponded ){
            //tipo 2
            View view = LayoutInflater.from(activity).inflate(R.layout.include_palpite_result, mHolder.frameLayout, false);
            TextView campeonato = view.findViewById(R.id.txt_tittle);
            TextView rodada = view.findViewById(R.id.txt_rodada);

            if(palpite != null){

            //valores principais
            if(palpite.getCampeonato() != null)
                campeonato.setText(palpite.getCampeonato());

            if(palpite.getRodada() != null)
                rodada.setText(palpite.getRodada());

            }

            mHolder.left.setVisibility(View.INVISIBLE);
            actualView.setVisibility(View.GONE);

            mHolder.frameLayout.addView(view);
            putView(maxPages, view);
            view.setVisibility(View.VISIBLE);
        }
    }

    void createErrorScreen (){
        View view = LayoutInflater.from(activity).inflate(R.layout.inclcude_palpite_error, mHolder.frameLayout, false);
        mHolder.frameLayout.addView(view);
        putView(0, view);
        actualView = view;
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;
            loading.dismiss();

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PALPITE:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){

                        if(mHolder.frameLayout.getChildCount() > 0){
                            mHolder.frameLayout.removeAllViews();
                        }


                        try {
                            Gson gson = new Gson();
                            PalpiteData data = gson.fromJson(response.getString("Object"), PalpiteData.class);

                            if(data.getPerguntas() != null){

                                if(data.getPerguntas().get(0).getV1() != null || data.getPerguntas().get(0).getV2() != null){
                                    hasResponded = true;
                                    mHolder.btn.setText("Ver Palpite");
                                }

                                palpiteData = data;
                                addViews(data);

                            }else{
                                createErrorScreen();
                                if(data.getMore() != null){
                                    mHolder.btn.setText("Ver Últimos Resultados");
                                    mHolder.btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(activity, PalpitesListActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                }else{
                                    mHolder.btn.setText("Atualizar");
                                    mHolder.btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getParpite();
                                        }
                                    });
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }else{
                    createErrorScreen();
                    mHolder.btn.setText("Atualizar");
                    mHolder.btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getParpite();
                        }
                    });
                }
            }
            break;

            case  OP_SET_PALPITE:{
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(status == 200){
                    hasResponded = true;
                    createOkScren(palpiteData);
                }
            }
            break;
            case OP_GET_ATLETAS:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            AtletaData[] data = gson.fromJson(obj.getString("itens"), AtletaData[].class);
                            PositonData[] pos = gson.fromJson(obj.getString("pos"), PositonData[].class);
                            List<AtletaData> atletas = new ArrayList<>(Arrays.asList(data));
                            List<PositonData> positions = new ArrayList<>(Arrays.asList(pos));

                            if(data != null){
                                initAtletas(atletas, positions);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_SET_PALPITE:{
                Toast.makeText(activity, "Erro ao enviar palpite, tente novamente mais tarde", Toast.LENGTH_LONG).show();
            }
        }
    }
    //endregion

    class ViewHolder {
        FrameLayout frameLayout;

        View indicator;
        TextView left;
        TextView quant;
        TextView right;

        Button btn;

        View atletas;
        RecyclerView recyclerView;
        TextView voltar;

        public ViewHolder (View view){
            frameLayout = view.findViewById(R.id.view_content);

            indicator = view.findViewById(R.id.view_indicator);
            left = view.findViewById(R.id.txt_voltar);
            quant = view.findViewById(R.id.txt_quantidade);
            right = view.findViewById(R.id.txt_proxima);

            btn = view.findViewById(R.id.btn_next);

            atletas = view.findViewById(R.id.view_card_selecionar);
            recyclerView = view.findViewById(R.id.recycle_atletas);
            voltar = view.findViewById(R.id.txt_voltar_palpites);
        }
    }
}
