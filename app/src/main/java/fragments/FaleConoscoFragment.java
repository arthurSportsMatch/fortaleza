package fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.CameraActivity;
import com.sportm.fortaleza.MainActivity;
import com.sportm.fortaleza.R;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import adapter.MessageAdapter;
import data.ChatMessageData;
import picker.ImageContract;
import picker.ImagePresenter;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.UTILS;

import static android.app.Activity.RESULT_OK;


public class FaleConoscoFragment extends Fragment implements AsyncOperation.IAsyncOpCallback, ImageContract.View{

    ViewHolder mHolder;

    int chatID = 00;
    boolean isGettingChat = false, isSendingMessage = false;

    private final int OP_GET_CHAT = 0;
    private final int OP_POST_MESSAGE = 1;
    public static final int REQUEST_TAKE_PHOTO = 101;
    public static final int REQUEST_GALLERY_PHOTO = 102;
    static String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private ImagePresenter mPresenter;
    Uri photoURI;

    MessageAdapter adapter;

    public FaleConoscoFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fale_conosco, container, false);

        mPresenter = new ImagePresenter(this);
        mHolder = new ViewHolder(view);
        initActions();

        //((MainMenuActivity) getActivity()).changeToolbarName("Fale Conosco");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getMessage();
    }

    void initActions (){
        mHolder.getImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo colocar botão para cancelar imagem e resetar envio
                mHolder.holderPreview.setVisibility(View.GONE);
                //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_FALECONOSCO_MIDIA).execute();
                selectImage();
            }
        });

        mHolder.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mHolder.messagePost.getText().toString();
                if(!message.isEmpty() || !photoURI.getPath().isEmpty()){
                    mHolder.holderPreview.setVisibility(View.GONE);
                    sendMessage(message);
                }
            }
        });
    }

    //Envia todas mensagem
    void sendMessage (String message){
        if(isSendingMessage){
            return;
        }

        isSendingMessage = true;
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        if(!message.isEmpty())
            params.put("texto", message);

        if(photoURI != null && !photoURI.getPath().isEmpty())
            params.put("sourceFile", photoURI.getPath());

        Log.d("Async-Message", "Enviando mensagem");

        //new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_SEND_MESSAGE, OP_POST_MESSAGE, this, TRACKING.TRACKING_FALECONOSCO_ENVIAR).execute(params);
    }

    //Pega as mensagens
    void getMessage (){
        if(isGettingChat){
            return;
        }

        isGettingChat = true;
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        int lastMsgId = Integer.parseInt(MainActivity.prefs.getString(CONSTANTS.LAST_CHAT_MSG_ID, "0"));
        params.put("id", lastMsgId);

        Log.d("Async-Message", "Geting message: " + lastMsgId);

        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_CHAT, OP_GET_CHAT,this ).execute(params);
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try {
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            } catch (JSONException e) {
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if (success) {
                OnAsyncOperationSuccess(opId, response);
            } else {
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });


    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_POST_MESSAGE:{
                isSendingMessage = false;
                try{
                    if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) && (response.getInt("Status") == HttpURLConnection.HTTP_OK) ){
                        getMessage();
                        mHolder.messagePost.setText("");
                        photoURI = null;
                        Log.d("Async-Message", "Mensagem enviada");
                    }else{
                        Log.d("Async-Message", "Mensagem NÃO enviada");
                    }
                } catch (JSONException e) {
                    UTILS.DebugLog("Error", e);
                }
            }
            break;

            case OP_GET_CHAT:{
                isGettingChat = false;

                try{
                    if( (response.has("Object")) && (response.get("Object") != JSONObject.NULL) ){
                        Gson gson = new Gson();
                        ChatMessageData[] data = gson.fromJson(response.getString("Object"), ChatMessageData[].class);
                        List<ChatMessageData> chatMessages = new ArrayList<>(Arrays.asList(data));
                        ConfigureMessages(chatMessages);
                        Log.d("Async-Message", "Mensagem Recebida" + response.toString());
                    }else{
                        Log.d("Async-Message", "Mensagem NÃO Recebida");
                    }
                } catch (JSONException e) {
                    UTILS.DebugLog("Error", e);
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_POST_MESSAGE:{
                isSendingMessage = false;
                Log.d("Async-Message", "Erro Envio: " + response.toString());
            }
            break;

            case OP_GET_CHAT:{
                isGettingChat = false;
                Log.d("Async-Message", "Erro Recebimento: " + response.toString());
            }
            break;
        }
    }

    void ConfigureMessages (List<ChatMessageData> messages){

        //recupero as mensagens já enviadas
        List<ChatMessageData> previousMessage = new ArrayList<>();
        int lastMsgId = Integer.parseInt(MainActivity.prefs.getString(CONSTANTS.LAST_CHAT_MSG_ID, "0"));

        if(lastMsgId > 0){
            String savedMsgs = MainActivity.prefs.getString(CONSTANTS.LAST_CHAT_MSGS, "");
            if(savedMsgs.length() > 0){
                ChatMessageData[] previus = new Gson().fromJson(savedMsgs, ChatMessageData[].class);
                previousMessage = new ArrayList<>(Arrays.asList(previus));
            }
        }

        //adiciono as mensagens que recebi
        if(messages != null && messages.size() > 0) {
            previousMessage.addAll(messages);
        }

        //salvo essas mensagens
        if(previousMessage.size() > 0) {
            MainActivity.prefs.put(CONSTANTS.LAST_CHAT_MSG_ID, String.valueOf(previousMessage.get(previousMessage.size() - 1).getId()));
            MainActivity.prefs.put(CONSTANTS.LAST_CHAT_MSGS, new Gson().toJson(previousMessage));
        }
        else{
            MainActivity.prefs.removeValue(CONSTANTS.LAST_CHAT_MSG_ID);
            MainActivity.prefs.removeValue(CONSTANTS.LAST_CHAT_MSGS);
        }

        if(adapter == null){
            adapter = new MessageAdapter(previousMessage, getContext());
            mHolder.messages.setAdapter(adapter);
            mHolder.messages.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        }else{
            adapter.addMessages(messages);
        }

        scrollToBottom();
    }

    void scrollToBottom (){
        mHolder.messages.scrollToPosition(adapter.getItemCount() - 1);
    }

    //toDo transformar toda essas classes em uma atividade que retorna a url da foto que o usuario selecionar
    //region Item Picker
    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(getActivity(), CameraActivity.class);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            if (file != null) {
                photoURI = Uri.fromFile(file); //FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                takePictureIntent.setData(photoURI);
                getActivity().startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getActivity().startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                if(photoURI == null){
                    Uri selectedImage = data.getData();
                    mHolder.holderPreview.setVisibility(View.VISIBLE);
                    mPresenter.showPreview(selectedImage);
                }else{
                    mHolder.holderPreview.setVisibility(View.VISIBLE);
                    mPresenter.showPreview(photoURI);
                }
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                String mPhotoPath = getRealPathFromUri(selectedImage);
                photoURI = Uri.parse(mPhotoPath);
                mHolder.holderPreview.setVisibility(View.VISIBLE);
                mPresenter.showPreview(mPhotoPath);
            }
        }
    }

    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(getContext(), mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(getActivity()).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                FaleConoscoFragment.this.showErrorDialog();
            }
        })
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Precisa de permissão");
        builder.setMessage("Dê permissão");
        builder.setPositiveButton("Escolha", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FaleConoscoFragment.this.openSettings();
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void showNoSpaceDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Erro");
        builder.setMessage("Sem espaço");
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);

    }

    @Override
    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTime().getTime();
        String mFileName = timeInMillis + ".jpeg";
        File mFilePath = getFilePath();

        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            return newFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(getActivity().getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        Glide.with(getContext()).load(mFilePath).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        Glide.with(getContext()).load(mFileUri).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    @Override
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Tirar Foto", "Pegar da Galeria",
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Tirar Foto")) {
                    mPresenter.cameraClick();
                    //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_FALECONOSCO_MIDIACAMERA).execute();
                } else if (items[item].equals("Pegar da Galeria")) {
                    mPresenter.ChooseGalleryClick();
                    //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_FALECONOSCO_MIDIABIBLIOTECA).execute();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                    //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_FALECONOSCO_MIDIACANCELAR).execute();
                }
            }
        });
        builder.show();
    }
    //endregion

    class ViewHolder {
        View holderPreview;
        ImageView previewImage;
        ImageView getImage;
        EditText messagePost;
        ImageView sendButton;
        RecyclerView messages;

        public ViewHolder (View view){
            holderPreview = view.findViewById(R.id.holder_image_preview);
            previewImage = view.findViewById(R.id.img_image_preview);
            getImage = view.findViewById(R.id.img_imgem_button);
            messagePost = view.findViewById(R.id.edt_message);
            sendButton = view.findViewById(R.id.img_send_button);
            messages = view.findViewById(R.id.recycler_fale_conosco);
        }
    }
}
