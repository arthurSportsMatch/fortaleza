package fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sportm.fortaleza.R;
import com.sportm.fortaleza.SearchActivity;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterReceita;
import data.AdapterReceitaData;
import utils.AsyncOperation;

import static android.app.Activity.RESULT_OK;

public class ReceitasMuralFragment extends Fragment {

    ViewHolder mHolder;
    List<AdapterReceitaData> receitas;
    String search = "";
    int currentPage = 1;

    boolean calculator = false;
    boolean isSearching = false;
    boolean posting = false;

    Handler handler = new Handler();

    Runnable runnable = new Runnable(){
        public void run() {
            getReceitas(currentPage);
        }
    };

    void dataChange (){
        final int interval = 1000; // 1 Second
        if (!posting){
            posting = true;
            handler.postAtTime(runnable, System.currentTimeMillis()+interval);
            handler.postDelayed(runnable, interval);
            //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_RECEITAS_FILTER).execute();
        }else{
            handler.removeCallbacks(runnable);
            posting = false;
            dataChange();
        }
    }

    public ReceitasMuralFragment() {
    }

    public ReceitasMuralFragment(boolean calc) {
        this.calculator = calc;
    }

    public ReceitasMuralFragment(String search) {
        this.search = search;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receitas_mural, container, false);
        setHasOptionsMenu(true);

        mHolder = new ViewHolder(view);
        getReceitas(currentPage);
        initAction();

        return view;
    }

    void initAction (){
        mHolder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHolder.filter.setVisibility(View.GONE);
            }
        });

        mHolder.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!isSearching){
                    currentPage = 1;
                    search = String.valueOf(s);
                    dataChange();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:{
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivityForResult(intent, 234, null);
                //mHolder.filter.setVisibility(View.VISIBLE);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    void getReceitas (int page){
        Hashtable<String, Object> _params = new Hashtable<>();

        //calculator
        if(calculator){
            _params.put("calc", 1);
        }

        //search
        search = search.replace(" ", "");

        if(!search.isEmpty()){
            if(calculator){
                search += "&calc=1";
            }
            _params.put("search", search);
            isSearching = true;
        }

        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_RECIPES, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Object")){
                    try {
                        if(response.getString("Object") != null && response.getString("Object").length() > 0){
                            Gson gson = new Gson();
                            AdapterReceitaData[] data = gson.fromJson(response.get("Object").toString(), AdapterReceitaData[].class);
                            if(data != null){
                                receitas = new ArrayList<>(Arrays.asList(data));

                                AdapterReceita adapter = new AdapterReceita(receitas, getContext(), R.layout.adapter_retangule_image_description, true, getActivity());
                                mHolder.recyclerView.setAdapter(adapter);
                                mHolder.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                            }else{
                                currentPage = 1;
                                search = "";
                                isSearching = false;
                                getReceitas(currentPage);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(isSearching){
                    isSearching = false;
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {
                if(isSearching){
                    isSearching = false;
                }
            }
        }).execute(_params);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 234){
            if(resultCode == RESULT_OK){

                Bundle extras = data.getExtras();
                int[] segmentos = extras.getIntArray("segmentos");
                int[] produtos = extras.getIntArray("produtos");

                if(segmentos.length > 0 || produtos.length > 0){
                    setFilter(segmentos, produtos);
                }
            }
        }
    }

    //
    void setFilter (int[] seg, int[] prod){
        //filtros são feitos localmente
        List<AdapterReceitaData> filterReceitas = new ArrayList<>();

        for(AdapterReceitaData data : receitas){
            if(data.getProdutos() != null){
                if(!checkIfExist(data, filterReceitas)){
                    Log.d("Recipe_Filters_P", "produtos: " + data.getProdutos());
                    if(checkIfContainID(getId(data.getProdutos()), prod)){
                        filterReceitas.add(data);
                    }
                }
            }else if (data.getSegmentos() != null){
                if(!checkIfExist(data, filterReceitas)){
                    Log.d("Recipe_Filters_P", "segmentos: " + data.getSegmentos());
                    if(checkIfContainID(getId(data.getSegmentos()), seg)){
                        filterReceitas.add(data);
                    }
                }
            }
        }

        AdapterReceita adapter = new AdapterReceita(filterReceitas, getContext(), R.layout.adapter_retangule_image_description, true, getActivity());
        mHolder.recyclerView.setAdapter(adapter);
        mHolder.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    //Verifica se existe na lista
    boolean checkIfExist (AdapterReceitaData data, List<AdapterReceitaData> list){
        if(list.contains(data)){
            return true;
        }
        return false;
    }

    //Pega os id's de uma string separada com ,
    int[] getId(String toCut){

        String[] cuts = toCut.split(",");
        Log.d("Recipe_Filters_P", "cortes: " + Arrays.toString(cuts));
        int[] idds = new int[cuts.length];

        for(int i = 0; i< cuts.length; i++){
            idds[i] = Integer.parseInt(cuts[i]);
        }

        return idds;
    }

    //Verifica se tem o mesmo id
    boolean checkIfContainID (int[] id, int[] ids){
        for(int i = 0; i < id.length; i++){
            for(int y = 0; y < ids.length; y++){
                if(id[i] == ids[y]){
                    return true;
                }
            }
        }
        return false;
    }

    class ViewHolder{
        RecyclerView recyclerView;

        View filter;
        EditText search;
        TextView close;

        public ViewHolder (View view){
            recyclerView = view.findViewById(R.id.recycler_mural_receitas);
            filter = view.findViewById(R.id.receita_search);
            search = view.findViewById(R.id.edt_search);
            close = view.findViewById(R.id.txt_close);
        }
    }
}
