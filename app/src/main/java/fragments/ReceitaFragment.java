package fragments;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import data.FragmentData;
import data.ReceitaData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.FileDownloader;
import utils.UTILS;


public class ReceitaFragment extends MainFragment {

    private static final int RQ_PERMISSIONS_STORAGE = 0;

    ViewHolder mHolder;

    boolean isRecipe = true;
    int recipeID = 383;     // ID da receita
    String search = "";     // Search

    String pdf;
    Menu menu;

    public ReceitaFragment() {
    }

    public ReceitaFragment(int id) {
        this.recipeID = id;
    }

    public ReceitaFragment(int id, boolean isRecipe) {
        this.recipeID = id;
        this.isRecipe = isRecipe;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RQ_PERMISSIONS_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if ( (grantResults.length > 0) ){

                    int size = grantResults.length;
                    for(int i = 0; i < size; i++){
                        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(getContext(), (R.string.error_storage_permission_not_granted), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    //success
                    if(!pdf.isEmpty()){
                        DownloadPDF(pdf);
                    }

                } else {

                    Toast.makeText(getContext(), (R.string.error_storage_permission_not_granted), Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receita, container, false);

        mHolder = new ViewHolder(view);
        initVars();

        loadRecipe(recipeID);
        initAction();

        //((MainMenuActivity) getActivity()).changeToolbarName("Receita");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_pdf, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_pdf:{
                OpenPDF(pdf);
                //new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_RECEITA_OPENPDF).execute();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initVars (){
        if(getActivity() == null)
            return;

        mHolder.webViewImagem.getSettings().setJavaScriptEnabled(true);
        mHolder.webViewImagem.getSettings().setDefaultTextEncodingName("utf-8");

        ReceitaFragment.MyJavaScriptInterface myJavaScriptInterface = new ReceitaFragment.MyJavaScriptInterface(getActivity());
        mHolder.webviewContent.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");
        mHolder.webviewContent.getSettings().setDefaultTextEncodingName("utf-8");
        mHolder.webviewContent.getSettings().setJavaScriptEnabled(true);
    }

    void initAction (){
        if(getActivity() == null)
            return;

        mHolder.calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new AsyncOperation(getActivity(),AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_RECEITA_OPENCALCULADORA).execute();
                MainMenuActivity.instance.ChangeFragment(new FragmentData(new CalculadoraFragment(recipeID), "Receitas"), -1);
            }
        });
    }

    //----------------------------------------------

    //Pega os valores da receita do servidor
    private void loadRecipe (int recipeID){
        if(getActivity() == null)
            return;

        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("id", recipeID);
        if(isRecipe){
            new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_NOTICIA, 0, new AsyncOperation.IAsyncOpCallback() {
                @Override
                public void CallHandler(int opId, JSONObject response, boolean success) {
                    if(success){
                        this.OnAsyncOperationSuccess(opId, response);
                    }else{
                        this.OnAsyncOperationError(opId, response);
                    }
                }

                @Override
                public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                    Log.d("Recipe-Menu", response.toString());
                    try {
                        if (response.getInt("Status") == 200){

                            if(response.has("Object")){
                                Gson gson = new Gson();
                                ReceitaData recipe = gson.fromJson(response.get("Object").toString(), ReceitaData.class);

                                if(recipe != null)
                                    setRecipe(recipe);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnAsyncOperationError(int opId, JSONObject response) {

                }
            }).execute(params);
        }else{
            new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_TRAINING, 0, new AsyncOperation.IAsyncOpCallback() {
                @Override
                public void CallHandler(int opId, JSONObject response, boolean success) {
                    if(success){
                        this.OnAsyncOperationSuccess(opId, response);
                    }else{
                        this.OnAsyncOperationError(opId, response);
                    }
                }

                @Override
                public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                    Log.d("Recipe-Menu", response.toString());
                    try {
                        if (response.getInt("Status") == 200){

                            if(response.has("Object")){
                                Gson gson = new Gson();
                                ReceitaData recipe = gson.fromJson(response.get("Object").toString(), ReceitaData.class);

                                if(recipe != null)
                                    setRecipe(recipe);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnAsyncOperationError(int opId, JSONObject response) {

                }
            }).execute(params);
        }

    }

    //Seta receita
    void setRecipe (ReceitaData recipe){
        if(getActivity() == null)
            return;

        if(recipe.getPdf() == null && this.menu != null){
            MenuItem pdfMenu = menu.findItem(R.id.menu_pdf);
            pdfMenu.setVisible(false);
        }else{
            pdf = recipe.getPdf();
        }

        if(recipe.getCalc() == 1){
            mHolder.calc.setVisibility(View.VISIBLE);
        }

        if(recipe.getTitulo() != null)
            mHolder.nome.setText(recipe.getTitulo());

        //escreve html no embed
        if(recipe.getTexto() != null){
            loadHtmlEmbed(recipe.getTexto());
        }

        //pega video ou imagem
        if((recipe.getVideo() == null) || (recipe.getVideo().length() <= 0) ){
            mHolder.webViewImagem.setVisibility(View.GONE);
            if(recipe.getImg() != null){
                UTILS.getImageAt(activity, recipe.getImg(), mHolder.recipeImagem);
            }
        }
        else {
            String frameVideo = "<iframe width=\"100%\" height=\"315\" src=\""+recipe.getVideo()+"\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
            WebSettings webSettings = mHolder.webViewImagem.getSettings();
            webSettings.setJavaScriptEnabled(true);
            mHolder.webViewImagem.loadData(frameVideo,"text/html", "utf-8");
        }
    }

    //Faz o load dos valores da receita em html
    private void loadHtmlEmbed(String embed) {

        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<!DOCTYPE html>");
            stringBuilder.append("<html>");
            stringBuilder.append("<head> <meta name=\"viewport\" content=\"width=100, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no\" /> </head>");
            stringBuilder.append("<body>");
            stringBuilder.append("<style> span { display: block; max-width: 100%; height: auto; } </style>");


            List<String> list = new ArrayList<>();

            List<String> images = UTILS.getStringsBetweenStrings(embed, "<img src=\"", "\"");
            int size = images.size();
            String replacement = "%s\" onclick=\"AndroidFunction.expand('%s')";
            for(int i = 0; i < size; i++){
                embed = embed.replaceFirst(images.get(i), String.format(replacement, images.get(i), images.get(i)));
            }

            stringBuilder.append(embed);
            stringBuilder.append("\n<script language=\"javascript\">\n" +
                    "function expand(str) {\n" +
                    "    AndroidFunction.expand(str);\n" +
                    "}\n" +
                    "</script>");


            stringBuilder.append("</body>");
            stringBuilder.append("</html>");


            String fullHtml = stringBuilder.toString();

            mHolder.webviewContent.getSettings().setUseWideViewPort(true);
            mHolder.webviewContent.getSettings().setLoadWithOverviewMode(true);
            mHolder.webviewContent.loadData(fullHtml, "text/html; charset=utf-8", null);
            Log.d("Html-builder", fullHtml);
        }
        catch (Exception e){
            UTILS.DebugLog("Error", e);
        }
    }

    //Abre PDF recebido
    private void OpenPDF(String pdfUrl){
        if( (pdfUrl == null) || (pdfUrl.length() <= 0) ){
            Toast.makeText(getContext(), getString(R.string.no_pdf_available), Toast.LENGTH_SHORT).show();
            return;
        }

        final String path = CONSTANTS.serverContentPDFs + pdfUrl;//Uri.encode(essential.getPdfUrl(), "UTF-8");
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        startActivity(browserIntent);
    }

    public class MyJavaScriptInterface {
        Context mContext;

        MyJavaScriptInterface(Context c) {
            mContext = c;
        }

        @android.webkit.JavascriptInterface
        public void expand(String strl){
            Bundle b = new Bundle();
            b.putString("media", strl);
            Message msg = new Message();
            msg.setData(b);
            //htmlImageClickCallback.sendMessage(msg);
        }
    }

    //region Share PDF
    //Chama download do pdf pra poder dar share
    void CallDownloadPdf(String  pdfUrl){

        if(pdfUrl == null){
            return;
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions
            if ((ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) ||
                    (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED) ||
                    (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED)) {
                AskForMediaPermission();
                return;
            }
        }

        DownloadPDF(pdfUrl);
    }

    public void DownloadPDF(String pdfUrl) {
        Toast.makeText(getContext(), getString(R.string.downloading), Toast.LENGTH_SHORT).show();
        String url = CONSTANTS.serverContentPDFs + pdfUrl;

        String dec1 = null;
        try {
            dec1 = java.net.URLDecoder.decode(pdfUrl, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String dec2 = null;
        try {
            dec2 = java.net.URLDecoder.decode(dec1, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String filename = UTILS.getNameFromFile(dec2).replace("_", " ");
        new DownloadFile().execute(url, filename);
    }

    void AskForMediaPermission(){

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE}, RQ_PERMISSIONS_STORAGE);
    }

    void ShareDownloadedPDF(File pdf){

        Uri uri;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", pdf);
        }
        else {
            uri = Uri.fromFile(pdf);
        }

        Intent share = new Intent();
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, uri);


        try {
            startActivity(share);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), getString(R.string.error_no_pdf_reader), Toast.LENGTH_SHORT).show();
        }

    }
    //endregion

    //ViewHolder com valores da view
    class ViewHolder {
        TextView nome;
        Button calc;
        ImageView recipeImagem;
        WebView webViewImagem;
        WebView webviewContent;

        public ViewHolder (View view){
            nome = view.findViewById(R.id.recipe_nome);
            calc = view.findViewById(R.id.btn_calculadora);

            recipeImagem = view.findViewById(R.id.img_receita_imagem);
            webViewImagem = view.findViewById(R.id.web_receita_imagem);
            webviewContent = view.findViewById(R.id.web_receita);
        }
    }

    //Classe para fazer download
    private class DownloadFile extends AsyncTask<String, Void, Void> {


        private String pdfFileName = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            String fileUrl = strings[0];
            String fileName = strings[1];

            boolean folderSuccess = false;
            File SDCardRoot = new File(Environment.getExternalStorageDirectory().toString() + "/Puratos/PDFs/Essentials/");
            if (!SDCardRoot.exists()) {
                boolean outcome = SDCardRoot.mkdirs();
                folderSuccess = outcome;
            }
            else {
                folderSuccess = true;
            }

            if (folderSuccess) {
                String filePath = Environment.getExternalStorageDirectory().getPath() + "/Puratos/PDFs/Essentials/" + fileName;
                pdfFileName = filePath;
                File pdfFile = new File(filePath);

                try {
                    pdfFile.createNewFile();
                    FileDownloader.downloadFile(fileUrl, pdfFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if( (pdfFileName != null) && (!pdfFileName.isEmpty()) ){
                File pdf = new File(pdfFileName);
                if(pdf.exists()){
                    Toast.makeText(getContext(), getString(R.string.download_finished), Toast.LENGTH_SHORT).show();
                    ShareDownloadedPDF(pdf);
                    return;
                }
            }

            Toast.makeText(getContext(), getString(R.string.download_error), Toast.LENGTH_SHORT).show();
        }
    }
}
