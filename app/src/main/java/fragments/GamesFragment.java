package fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.sportm.fortaleza.R;
import com.google.android.material.tabs.TabLayout;
import com.sportm.fortaleza.StartMenuActivity;

import java.util.ArrayList;

import adapter.PageAdapter;
import data.FragmentData;
import utils.AsyncOperation;
import utils.FragmentHolder;
import utils.TRACKING;
import utils.fragment.FragmentsHolderController;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

public class GamesFragment extends MainFragment {

    private FragmentsHolderController mFragmentsC;
    private View mainView ;
    private ViewHolder mHolder;


    private ArrayList<FragmentHolder> pageList = new ArrayList<>();
    public static final String ON_PUSH_GAMES = "com.devarthur.fortaleza.ON_PUSH_GAMES";

    private String pushType = "";
    private int pushValue = -1;

    public GamesFragment (){

    }

    public GamesFragment (String type){
        this.pushType = type;
    }

    public GamesFragment (String type, int pushV){
        this.pushType = type;
        this.pushValue = pushV;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_games, container, false);
        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initValues(mainView);
        initActions();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterBroadcast();
    }

    void initValues (View view){
        mHolder = new ViewHolder(view);
        mFragmentsC = new FragmentsHolderController(getChildFragmentManager(), R.id.view_game_pager);
    }

    void initActions (){
        initPesquisaLayout();
        registerBroadcast();
    }

    void initPesquisaLayout (){
        Log.d("FragmentManagerC", pushType);
        if(!pushType.isEmpty()){
            switch (pushType){
                case "palpite":{
                    mHolder.gameTab.getTabAt(2).select();
                    mFragmentsC.addFragmentInFrameSimple(new PalpitesFragment(), "PalpitesFragment", "GameScreen");
                    pushType = "";
                }
                break;

                case "pesquisa":{
                    mHolder.gameTab.getTabAt(1).select();
                    mFragmentsC.addFragmentInFrameSimple(new PesquisaFragment(pushValue), "PesquisaFragment", "GameScreen");
                    pushType = "";
                }
                break;

                case "quiz":{
                    mHolder.gameTab.getTabAt(0).select();
                    mFragmentsC.addFragmentInFrameSimple(new QuizFragment(pushValue), "QuizFragment", "GameScreen");
                    pushType = "";
                }
                break;
            }
        }else{
            mHolder.gameTab.getTabAt(0).select();
            mFragmentsC.addFragmentInFrameSimple(new QuizFragment(pushValue), "QuizFragment", "GameScreen");
        }

        mHolder.gameTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0: {
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_JOGOS_PESQUISAS).execute();
                        mFragmentsC.addFragmentInFrameSimple(new QuizFragment(pushValue), "QuizFragment", "GameScreen");
                    }
                    break;
                    case 1: {
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_JOGOS_PALPITES).execute();
                        mFragmentsC.addFragmentInFrameSimple(new PesquisaFragment(pushValue), "PesquisaFragment", "GameScreen");
                    }
                    break;
                    case 2: {
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_JOGOS_QUIZ).execute();
                        mFragmentsC.addFragmentInFrameSimple(new PalpitesFragment(), "PalpitesFragment", "GameScreen");
                    }
                    break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    void setPager (Intent intent){
        switch (pushType){
            case "palpite":{
                //mHolder.gamePager.setCurrentItem(2);
                //mFragmentsC.addFragmentInFrameSimple(new QuizFragment(pushValue), "QuizFragment", "GameScreen");
            }
            break;

            case "pesquisa":{
                //mHolder.gamePager.setCurrentItem(1);
                PesquisaFragment pesquisa = (PesquisaFragment) pageList.get(1).getFragment();
                pesquisa.setPUSH(intent.getIntExtra("id", -1));
            }
            break;

            case "quiz":{
                //mHolder.gamePager.setCurrentItem(0);
                QuizFragment quiz = (QuizFragment) pageList.get(0).getFragment();
                quiz.setPUSH(intent.getIntExtra("id", -1));
            }
            break;
        }
    }

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onPush = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null){
//                pushType = intent.getStringExtra("tipo");
//                setPager(intent);
            }
        }
    };

    void registerBroadcast (){
        IntentFilter iOnPrepared = new IntentFilter(ON_PUSH_GAMES);
        activity.registerReceiver(onPush, iOnPrepared);
    }

    void unregisterBroadcast (){
        activity.unregisterReceiver(onPush);
    }

    private class ViewHolder {
        View firstView;
        TabLayout gameTab;
        FrameLayout gamePager;

        public ViewHolder (View view){
            firstView = view.findViewById(R.id.swipe_view);
            gameTab = view.findViewById(R.id.tab_games);
            gamePager = view.findViewById(R.id.view_game_pager);
        }
    }
}
