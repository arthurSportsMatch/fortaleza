package fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.sportm.fortaleza.R;

import java.util.List;
import java.util.Objects;

import adapter.EstadioPageAdapter;
import adapter.SetorPageAdapter;
import data.CheckinSetoresData;
import pager.FadePageTransformer;
import utils.UILoadingController;


public class CheckinSetorDivFragment extends MainFragment {

    ViewHolder mHolder;
    Listener listener;
    String selectedSetorTabName;
    List<CheckinSetoresData> setores;

    public CheckinSetorDivFragment() {
        // Required empty public constructor
    }

    public CheckinSetorDivFragment(Listener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkin_setor_div, container, false);
        mHolder = new ViewHolder(view);
        View loadingView = view.findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.OnLoad();
            }
        });

        initLoading();
        return view;
    }

    void initLoading (){
        listener.OnLoad();
        uiLoadingController.onLoad();
    }

    void initAction (){

        mHolder.tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedSetorTabName = Objects.requireNonNull(tab.getText()).toString();
                checkSelection();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void atualizarData(List<CheckinSetoresData> data){
        uiLoadingController.finishLoader(true);
        listener.OnUnlock();
        setores = data;
        SetorPageAdapter estadioPageAdapter = new SetorPageAdapter(activity, data);
        mHolder.pager.setAdapter(estadioPageAdapter);
        mHolder.pager.setPageTransformer(false, new FadePageTransformer());
        mHolder.tabLayout.setupWithViewPager(mHolder.pager);

        selectedSetorTabName = Objects.requireNonNull(mHolder.tabLayout.getTabAt(mHolder.pager.getCurrentItem()).getText()).toString();
        changeTabSelectionColor(getColor(selectedSetorTabName));
        changeImage(selectedSetorTabName);
        checkSelection();

        initAction();
    }

    CheckinSetoresData getSetor (String setorName){
        CheckinSetoresData setor = new CheckinSetoresData();
        for(CheckinSetoresData d : setores){
            if(d.getNome().equals(setorName)){
                setor = d;
                break;
            }
        }
        return setor;
    }

    void checkSelection(){
        CheckinSetoresData seletor = getSetor(selectedSetorTabName);
        listener.OnFinished(String.valueOf(seletor.getId()), seletor.getNome());
    }

    void checkSelection(String nome){
        CheckinSetoresData seletor = getSetor(nome);
        listener.OnFinished(String.valueOf(seletor.getId()), seletor.getNome());
    }

    void changeImage (String setor){

    }

    int getColor (String setor){
        int color = R.color.colorAccent;
        switch (setor.toLowerCase()){
            case "premium" : color = R.color.colorPremium;
                break;
            case "especial" : color = R.color.colorEspecial;
                break;
            case "bossa nova" : color = R.color.colorBossaNova;
                break;
            case "inferior sul" : color = R.color.colorInferiorSul;
                break;
            case "superior sul" : color = R.color.colorSuperiorSul;
                break;
            case "camarotes" : color = R.color.colorCamarotes;
                break;
            case "superior norte" : color = R.color.colorSuperiorNorte;
                break;
            case "superior central" : color = R.color.colorSuperiorCentro;
                break;
            case "inferior norte" : color = R.color.colorInferiorNorte;
                break;
        }
        return color;
    }

    public void errorToGetData (){
        uiLoadingController.finishLoader(false);
    }

    //Muda a cor do texto selecionavel
    void changeTabSelectionColor (int color){
        mHolder.tabLayout.setTabTextColors (activity.getResources().getColor(R.color.colorGray),activity.getResources().getColor(color));
    }

    public interface Listener {
        void OnLoad ();
        void OnFinished(String setorID);
        void OnFinished(String setorID, String setorName);
        void OnUnlock();
    }

    class ViewHolder {
        View mainView;
        ViewPager pager;
        TabLayout tabLayout;

        public ViewHolder(View view){
            mainView = view;
            tabLayout = view.findViewById(R.id.tab_setor);
            pager = view.findViewById(R.id.pager_estadio);
        }
    }

}
