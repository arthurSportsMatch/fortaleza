package fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.CheckInLoginActivity;
import com.sportm.fortaleza.CheckInMessageActivity;
import com.sportm.fortaleza.CheckinActivity;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.MatchHistoryActivity;
import com.sportm.fortaleza.PerfilAtletaActivity;
import com.sportm.fortaleza.R;
import com.google.android.gms.ads.AdSize;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.sportm.fortaleza.StartMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import adapter.LastResultAdapter;
import data.ElencoData;
import data.ElencoPosData;
import data.ElencosData;
import data.FragmentData;
import data.PartidaData;
import informations.UserInformation;
import models.elenco.ElencoModel;
import models.nextgames.NextGameModel;
import pager.NextGamePageAdapter;
import repository.elenco.ElencoRepository;
import repository.lastgames.LastGamesRepository;
import repository.nextgames.NextGamesRepository;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.elenco.ElencoViewModel;
import vmodels.fragment.FragmentHolderViewModel;
import vmodels.lastgames.LastGamesViewModel;
import vmodels.nextgames.NextGamesViewModel;
import vmodels.user.UserViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

public class TimeFragment extends MainFragment {

    private ViewHolder mHolder;
    private NextGamesViewModel nextGamesViewModel;
    private LastGamesViewModel lastGamesViewModel;
    private ElencoViewModel elencoViewModel;
    private FragmentHolderViewModel mfragmentViewModel;

    List<View> groups = new ArrayList<>();

    public TimeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time, container, false);
        mHolder = new ViewHolder(view);

        LinearLayout mAdView = view.findViewById(R.id.adView);
        AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.TIME_TOP_BANNER_ID);

        initActions();
        return view;
    }


    @Override
    public void bind() {
        super.bind();
        NextGamesRepository nextGamesRepository = new NextGamesRepository(activity);
        nextGamesViewModel = new ViewModelProvider(requireActivity()).get(NextGamesViewModel.class);
        nextGamesViewModel.setmRepository(nextGamesRepository);
        nextGamesViewModel.nextGames().observe(this, new Observer<NextGameModel>() {
            @Override
            public void onChanged(NextGameModel games) {
                initNextGames(games.getPartidaData(), games.getHide());
            }
        });

        LastGamesRepository lastGamesRepository = new LastGamesRepository(activity);
        lastGamesViewModel = new ViewModelProvider(requireActivity()).get(LastGamesViewModel.class);
        lastGamesViewModel.setmRepository(lastGamesRepository);
        lastGamesViewModel.lastGames().observe(this, new Observer<List<PartidaData>>() {
            @Override
            public void onChanged(List<PartidaData> games) {
                initLastResults(games);
            }
        });

        ElencoRepository elencoRepository = new ElencoRepository(activity);
        elencoViewModel = new ViewModelProvider(requireActivity()).get(ElencoViewModel.class);
        elencoViewModel.setmRepository(elencoRepository);
        elencoViewModel.elenco().observe(this, new Observer<ElencoModel>() {
            @Override
            public void onChanged(ElencoModel elenco) {
                if(mHolder.viewAtletas.getChildCount() <= 0){
                    adjustElencos(elenco.getElencoData(), elenco.getElencoPosDataList());
                }
            }
        });

        mfragmentViewModel = new ViewModelProvider(requireActivity()).get(FragmentHolderViewModel.class);

    }

    @Override
    public void unbind() {
        super.unbind();
        nextGamesViewModel.nextGames().removeObservers(this);
        lastGamesViewModel.lastGames().removeObservers(this);
        elencoViewModel.elenco().removeObservers(this);
    }

    @Override
    public void onReload() {
        super.onReload();
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_TIME_RELOAD).execute();
    }

    void initActions (){
        mHolder.agendaCompleta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            mfragmentViewModel.changeFragment(new AgendaFragment(), "AgendaFragment");
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_TIME_PROXIMOSJOGOS_ABRIRAGENDA).execute();
            }
        });
    }

    //Inicia Proximos jogos
    void initNextGames (List<PartidaData> data, Integer hide){
        boolean mHide = false;
        if(hide != null && hide == 1){
            mHide = true;
        }
        NextGamePageAdapter nextGameAdapter = new NextGamePageAdapter(activity, data, R.layout.adapter_agenda_jogos, mHide, new NextGamePageAdapter.Listener() {
            @Override
            public void OnPurchase(PartidaData data) {
                String url = data.getLinkCompra();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                activity.startActivity(i);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_TIME_PROXIMOSJOGOS_COMPRARINGRESSO, data.getId()).execute();
            }

            @Override
            public void OnCheckIn() {
                goSocioTorcedor ();
            }
        });

        mHolder.nextGamesTab.setupWithViewPager(mHolder.nextGamesPager);
        mHolder.nextGamesPager.setAdapter(nextGameAdapter);


        if(data!= null) {
            Objects.requireNonNull(mHolder.nextGamesTab.getTabAt(0)).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(0).getDataHora()).replace("/2020", ""));

            if(data.size() >= 2) {
                Objects.requireNonNull(mHolder.nextGamesTab.getTabAt(1)).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(1).getDataHora()).replace("/2020", ""));
            }

            if(data.size() >= 3) {
                Objects.requireNonNull(mHolder.nextGamesTab.getTabAt(2)).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(2).getDataHora()).replace("/2020", ""));
            }

            if(data.size() >= 4) {
                Objects.requireNonNull(mHolder.nextGamesTab.getTabAt(3)).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(3).getDataHora()).replace("/2020", ""));
            }
        }
    }

    void goSocioTorcedor (){
        int socio = UserInformation.getUserData().getSocioTorcedor();
        int beneficio = UserInformation.getUserData().getSocioBeneficio();

        if(socio == 1){
            if(beneficio == 1){
                Intent intent = new Intent(activity, CheckinActivity.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(activity, CheckInMessageActivity.class);
                intent.putExtra("tit", activity.getResources().getString(R.string.funcionalidade_exclusiva_n_para_s_cio_torcedor));
                intent.putExtra("msg", activity.getResources().getString(R.string.socio_sem_beneficio));
                startActivity(intent);
            }

        }else{
            Intent intent = new Intent(activity, CheckInLoginActivity.class);
            startActivity(intent);
        }
    }

    //Inicia Ultimos resultados
    void initLastResults (List<PartidaData> data){
        LastResultAdapter lastResultAdapter = new LastResultAdapter(activity, data, R.layout.adapter_last_results, new LastResultAdapter.Listener() {
            @Override
            public void onClick(int id) {
                Intent intent = new Intent(getContext(), MatchHistoryActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_TIME_ABRIRPARTIDA, id).execute();
            }
        });

        mHolder.ultimosResultados.setAdapter(lastResultAdapter);
        mHolder.ultimosResultados.setLayoutManager( new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
    }

    //Ajusta o elenco e o separa em listas de elencos
    void adjustElencos (List<ElencoData> data, List<ElencoPosData> pos){

        List<ElencosData> elencos = new ArrayList<>();

        for(int i = 0; i < pos.size(); i++){
            ElencoPosData elenco = pos.get(i);
            List<ElencoData> atletas = new ArrayList<>();

            for(ElencoData atleta : data){
                if(atleta.getPos() == elenco.getId()){
                    atletas.add(atleta);
                }
            }

            elencos.add(new ElencosData(pos.get(i).getId(),pos.get(i).getNome(), atletas));
        }

        initElenco(elencos);
    }

    //Inicia Elenco (Masculino feminino)
    void initElenco (List<ElencosData> data){
        List<ElencosData> elencos = data;
        Collections.sort(elencos);

        if(mHolder.viewAtletas.getChildCount() > 0){
            mHolder.viewAtletas.removeAllViews();
            groups.removeAll(groups);
        }

        //Pegar tamanho da tela parar criar view com 48 da tela
        DisplayMetrics dimension = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int weight = dimension.widthPixels;
        weight = (weight / 100) * 48;

        for(ElencosData elenco : elencos){
            LinearLayout horizontalLayout = new LinearLayout(activity);
            horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
            horizontalLayout.setGravity(Gravity.BOTTOM);
            horizontalLayout.setLayoutParams(mHolder.viewAtletas.getLayoutParams());

            for (ElencoData n : elenco.getElenco()) {
                View view = LayoutInflater.from(activity).inflate(R.layout.adapter_elenco_item_image, horizontalLayout, false);
                ImageView imageView = view.findViewById(R.id.atleta_img);
                View backView = view.findViewById(R.id.atleta_back);
                //imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                //imageView.setAdjustViewBounds(true);

                UTILS.getImageAt(activity, n.getImg(), imageView, new RequestOptions()
                        .placeholder(R.drawable.player_placeholder)
                        .error(R.drawable.player_placeholder)
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .transforms( new RoundedCorners(20)));

                final String id = n.getId();
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, PerfilAtletaActivity.class);
                        intent.putExtra("id", Integer.parseInt(id));
                        startActivity(intent);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_TIME_ELENCO_SELECIONAR, Integer.parseInt(id)).execute();
                    }
                });

                backView.getLayoutParams().width = weight;
                imageView.getLayoutParams().width = weight;
                backView.requestLayout();

                horizontalLayout.addView(view);
            }
            groups.add(horizontalLayout);
            mHolder.viewAtletas.addView(horizontalLayout);
        }

        mHolder.elencoTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0 :{
                        mHolder.scrollElenco.setScrollX(0);
                    }
                    break;
                    case 1 :{
                        int pos = groups.get(0).getWidth();
                        mHolder.scrollElenco.setScrollX(pos);
                    }
                    break;
                    case 2 :{
                        int pos = groups.get(0).getWidth() + groups.get(1).getWidth();
                        mHolder.scrollElenco.setScrollX(pos);
                    }
                    break;
                    case 3 :{
                        int pos = groups.get(0).getWidth() + groups.get(1).getWidth() + groups.get(2).getWidth();
                        mHolder.scrollElenco.setScrollX(pos);
                    }
                    break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    class ViewHolder {
        View firtView;
        TabLayout nextGamesTab;
        ViewPager nextGamesPager;
        View agendaCompleta;

        RecyclerView ultimosResultados;

        HorizontalScrollView scrollElenco;
        LinearLayout viewAtletas;
        TabLayout elencoTab;

        public ViewHolder (View view){
            firtView = view.findViewById(R.id.firt_view);
            nextGamesTab = view.findViewById(R.id.tab_next_games);
            nextGamesPager = view.findViewById(R.id.pager_next_games);
            agendaCompleta = view.findViewById(R.id.btn_agenda_completa);

            ultimosResultados = view.findViewById(R.id.recycler_last_results);

            scrollElenco = view.findViewById(R.id.scroll_elenco);
            viewAtletas = view.findViewById(R.id.view_atletas);
            elencoTab = view.findViewById(R.id.tab_elenco);
        }
    }
}
