package fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sportm.fortaleza.CheckInLoginActivity;
import com.sportm.fortaleza.CheckInMessageActivity;
import com.sportm.fortaleza.CheckinActivity;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.R;
import com.google.android.gms.ads.AdSize;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import adapter.NextGamesPageAdapter;
import data.PartidaData;
import data.PartidasData;
import informations.UserInformation;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    private ViewHolder mHolder;
    private List<PartidasData> partidas;

    int page = 0;
    int gameFocusID = -1;
    private static final int OP_ID_PROXIMOS_JOGOS       = 0;

    public AgendaFragment() {
        // Required empty public constructor
    }

    public AgendaFragment(int id) {
        this.gameFocusID = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agenda, container, false);
        mHolder = new ViewHolder(view);

        LinearLayout mAdView = view.findViewById(R.id.adView);
        AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.AGENDA_BOTTOM_BANNER_ID);

        getProxJogos();
        return view;
    }

    void getProxJogos (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(getActivity(), AsyncOperation.TASK_ID_GET_JOGOS, OP_ID_PROXIMOS_JOGOS, this).execute(params);
    }

    void configurePartidas (List<PartidaData> partida, Integer hide){
        boolean mHide = false;
        if(hide != null && hide == 1){
            mHide = true;
        }

        Hashtable<String, PartidasData> configure = new Hashtable<>();
        List<PartidasData> data = new ArrayList<>();

        for(PartidaData d : partida){
            //Para colocar o id da hastable eu corto a string deixando ela maior dependendo da data pra facilitar a ordem
            String part = d.getDataHora().substring(5);
            part = part.replace("-", "");
            String dataJOGO = part.substring(0,5);

            if(configure.containsKey(dataJOGO)){
                configure.get(dataJOGO).addPartida(d);
            }else{
                PartidasData partidas = new PartidasData();
                partidas.addPartida(d);
                configure.put(dataJOGO, partidas);
            }
        }

        List<String> v = new ArrayList<>(configure.keySet());
        Collections.sort(v);

        for (String str : v) {
            data.add(configure.get(str));
        }
        initNextGames(data, mHide);
    }

    //Inicia Proximos jogos
    void initNextGames (List<PartidasData> data, boolean hide){
        NextGamesPageAdapter nextGameAdapter = new NextGamesPageAdapter(getActivity(), data, hide, gameFocusID, new NextGamesPageAdapter.Listener() {
            @Override
            public void OnPurchase(PartidaData data) {
                String url = data.getLinkCompra();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                activity.startActivity(i);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_AGENDA_COMPRARINGRESSO, data.getId()).execute();
            }

            @Override
            public void OnCheckIn() {
                goSocioTorcedor ();
            }
        });

        mHolder.nextGamesTab.setupWithViewPager(mHolder.nextGamesPager);
        mHolder.nextGamesPager.setAdapter(nextGameAdapter);

        for(int i = 0; i < data.size(); i ++){
            PartidaData d = data.get(i).getPartidas().get(0);
            String dataJOGO = UTILS.formatDateFromDB(d.getDataHora()).substring(0,5);
            mHolder.nextGamesTab.getTabAt(i).setText(hide?"--/--" : dataJOGO);
        }

        checkGameWithId(data);
    }

    void checkGameWithId (List<PartidasData> data){
        for(int i = 0; i < data.size(); i ++){
            for(final PartidaData partida : data.get(i).getPartidas()){
                if(gameFocusID == partida.getId()){
                    goToPage(i);
                }
            }
        }
    }

    void goToPage (int id){
        mHolder.nextGamesTab.getTabAt(id).select();
    }

    void goSocioTorcedor (){
        int socio = UserInformation.getUserData().getSocioTorcedor();
        int beneficio = UserInformation.getUserData().getSocioBeneficio();

        if(socio == 1){
            if(beneficio == 1){
                Intent intent = new Intent(activity, CheckinActivity.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(activity, CheckInMessageActivity.class);
                intent.putExtra("tit", activity.getResources().getString(R.string.funcionalidade_exclusiva_n_para_s_cio_torcedor));
                intent.putExtra("msg", activity.getResources().getString(R.string.socio_sem_beneficio));
                startActivity(intent);
            }

        }else{
            Intent intent = new Intent(activity, CheckInLoginActivity.class);
            startActivity(intent);
        }
    }

    //region AsyncOperation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_PROXIMOS_JOGOS:{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            PartidaData[] data = gson.fromJson(obj.getString("itens"), PartidaData[].class);
                            Integer hide = obj.getInt("hide");
                            if(data != null){
                                List<PartidaData> partidaData = new ArrayList<>(Arrays.asList(data));
                                configurePartidas(partidaData, hide);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        TabLayout nextGamesTab;
        ViewPager nextGamesPager;

        public ViewHolder (View view){
            nextGamesTab = view.findViewById(R.id.tab_next_games);
            nextGamesPager = view.findViewById(R.id.pager_next_games);
        }
    }
}
