package fragments;

import android.os.Bundle;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.List;

import data.CheckinData;
import utils.CardType;
import utils.MaskEditUtil;
import utils.fragment.MaskCard;

import static utils.CardType.forCardNumber;


public class CheckingCadastrarCartaoFragment extends Fragment {

    ViewHolder mHolder;
    CheckinData checking;
    Listener listener;

    public CheckingCadastrarCartaoFragment() {
        // Required empty public constructor
    }

    public CheckingCadastrarCartaoFragment(CheckinData checking, CheckingCadastrarCartaoFragment.Listener listener) {
        this.checking = checking;
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checking_cadastrar_cartao, container, false);
        mHolder = new ViewHolder(view);

        initActions();

        return view;
    }


    void initActions() {

        mHolder.ncc.addTextChangedListener(new MaskCard(new MaskCard.Listener() {
            @Override
            public void onCard(CardType cardType) {
                mHolder.flag.setImageDrawable(getResources().getDrawable(cardType.getFrontResource()));
            }
        }).mask(mHolder.ncc, "####-####-####-####"));

        mHolder.vencimento.addTextChangedListener(MaskEditUtil.mask(mHolder.vencimento,"##/####"));

    }

    public boolean validarFrom() {
        String txtNcc = MaskEditUtil.unmask(mHolder.ncc.getText().toString());
        String txtNome = MaskEditUtil.unmask(mHolder.nome.getText().toString());
        String txtVenc = MaskEditUtil.unmask(mHolder.vencimento.getText().toString());
        String txtCvv = MaskEditUtil.unmask(mHolder.cvv.getText().toString());


        if( txtNcc.length() < 12) {
            mHolder.ncc.setError("Número de cartão inválido");
            return false;
        }

        if( txtNome.equals("")) {
            mHolder.nome.setError("");
            return false;
        }else if ( txtNome.length() < 3 ) {
            mHolder.nome.setError("Nome inválido");
            return false;
        }

        if ( txtVenc.length() < 6) {
            mHolder.vencimento.setError("");
            return false;
        }

        if ( txtCvv.length() < 3 ) {
            mHolder.cvv.setError("");
            return false;
        }

        return true;
    }

    public interface Listener {
        void OnUnlock();
    }

    class ViewHolder {
        EditText ncc, nome, vencimento, cvv;
        SwitchCompat salvar;
        ImageView flag;

        public ViewHolder(View view) {
            ncc = view.findViewById(R.id.edt_ncc);
            nome = view.findViewById(R.id.edt_nome);
            vencimento = view.findViewById(R.id.edt_vencimento);
            cvv = view.findViewById(R.id.edt_cvv);

            salvar = view.findViewById(R.id.swt_salvar);
            flag = view.findViewById(R.id.iv_flag_card);
        }
    }
}