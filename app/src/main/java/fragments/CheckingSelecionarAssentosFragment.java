package fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sportm.fortaleza.CheckInController;
import com.sportm.fortaleza.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import adapter.SetorPageAdapter;
import data.CheckinCadeiraData;
import data.CheckinData;
import data.CheckinSetoresData;
import pager.FadePageTransformer;
import utils.UILoadingController;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckingSelecionarAssentosFragment extends MainFragment {

    ViewHolder mHolder;
    Listener listener;
    Hashtable<String, LinearLayout> listViews;
    List<CadeiraView> cadeirasViews = new ArrayList<>();
    CheckinData data;

    public CheckingSelecionarAssentosFragment() {
        // Required empty public constructor
    }

    public CheckingSelecionarAssentosFragment(CheckinData data, Listener listener) {
        this.listener = listener;
        this.data = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checking_selecionar_assentos, container, false);
        mHolder = new ViewHolder(view);
        View loadingView = view.findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.OnLoad();
            }
        });
        initLoading();
        cadeirasViews.clear();
        createAssentoToAdd(data.getTickets().size());
        return view;
    }

    //cria os assentos que ficaram de demonstrativo
    void createAssentoToAdd (int quanty){
        mHolder.listaCadeiras.removeAllViews();

        for(int i = 0; i < quanty; i ++){
            TextView view = createAssentoView(mHolder.listaCadeiras);
            cadeirasViews.add(new CadeiraView(view, null));
            mHolder.listaCadeiras.addView(view);
        }
    }

    void initLoading (){
        listener.OnLoad();
        uiLoadingController.onLoad();
    }

    public void atualizarData(List<CheckinCadeiraData> data){
        uiLoadingController.finishLoader(true);
        mHolder.list.removeAllViews();
        listViews = new Hashtable<>();

        listener.romeverCadeiras();

        for (int i=0; i<this.data.getTickets().size(); i++) {
            listener.onChose("", "", -1);
        }

        for(int i = 0;i < data.size(); i ++){
            CheckinCadeiraData cadeira = data.get(i);
            createCadeira(cadeira);
        }

        mHolder.textCampo.requestLayout();
        listener.OnUnlock();
    }

    void createCadeira (CheckinCadeiraData cadeira){
        LinearLayout layout = null;
        String text = cadeira.getFila() + "" + cadeira.getCadeira();

        if(listViews.containsKey(cadeira.getFila())){
            layout = listViews.get(cadeira.getFila());
        }else{
            layout = new LinearLayout(activity);
            layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            layout.setOrientation(LinearLayout.HORIZONTAL);
            mHolder.list.addView(layout);
            listViews.put(cadeira.getFila(), layout);
        }

        TextView txtCadeira = createAssentoView(layout, cadeira);
        txtCadeira.setText(text);
        layout.addView(txtCadeira);
    }

    TextView  createAssentoView (LinearLayout mView, final CheckinCadeiraData cadeira){
        final boolean status = getStatus(cadeira.getStatus());
        final TextView view = (TextView) LayoutInflater.from(activity).inflate(R.layout.include_checkin_assento, mView,false);

        view.getBackground().setTint( activity.getResources().getColor(status ? R.color.colorLoginFirst : R.color.colorGray6));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status){
                    if(!hasCadeira(cadeira)){
                        addAssento(cadeira, view);
                    }else{
                        Toast.makeText(activity, "Selecione outra cadeira", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(activity, "Selecione outra cadeira", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    TextView  createAssentoView (LinearLayout mView){
        TextView view = (TextView) LayoutInflater.from(activity).inflate(R.layout.include_checkin_assento, mView,false);
        return view;
    }

    boolean getStatus (String status){
        status = status.toUpperCase();
        switch (status){
            case "LIB" : return true;
            case "BLO" : return false;
            default: return false;
        }
    }

    //Adiciona assento especifico
    void addAssento (CheckinCadeiraData cadeira){
        boolean hasAssentos = false;
        for(int i = 0 ; i < cadeirasViews.size();){
            CadeiraView cadeiraView = cadeirasViews.get(i);
            if(cadeiraView.cadeira == null){
                String text = cadeira.getFila() + "" + cadeira.getCadeira();
                uploadAssentos(cadeiraView.view, text);
                cadeiraView.cadeira = cadeira;
                hasAssentos = true;
                break;
            }else{
                i ++;
            }
        }

        if(!hasAssentos){
           Toast.makeText(activity, "Você selecionou todas as cadeiras", Toast.LENGTH_LONG).show();
        }

        //listener.onChose();
    }

    boolean hasCadeira (CheckinCadeiraData cadeira){
        boolean hasCadeira = false;
        for(int i = 0; i < cadeirasViews.size(); i ++){
            if(cadeirasViews.get(i).cadeira == cadeira){
                hasCadeira = true;
                break;
            }
        }
        return hasCadeira;
    }

    void addAssento (CheckinCadeiraData cadeira, TextView newView){
        boolean hasAssentos = false;
        CadeiraView cadeiraView = null;
        int index = 0;

        for(int i = 0 ; i < cadeirasViews.size();){
            cadeiraView = cadeirasViews.get(i);
            if(cadeiraView.cadeira == null){
                hasAssentos = true;
                index = i;
                break;
            }else{
                i ++;
            }
        }

        if(!hasAssentos){
            Toast.makeText(activity, "Você selecionou todas as cadeiras", Toast.LENGTH_LONG).show();
        }else{
            String text = cadeira.getFila() + "" + cadeira.getCadeira();
            JSONObject info = new JSONObject();
            try {
                info.put("id", String.valueOf(cadeira.getId()));
                info.put("nome", text);
                uploadAssentos(index, cadeiraView.view, text, info);
                cadeiraView.cadeira = cadeira;
                cadeiraView.selectedView = newView;
                newView.getBackground().setTint(activity.getResources().getColor(R.color.colorRedSPFC2));
                listener.onChose(String.valueOf(cadeira.getId()), text, index);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    //Faz upload do assento
    void uploadAssentos (TextView view, String text){
        view.getBackground().setTint(activity.getResources().getColor(R.color.colorRedSPFC2));
        view.setText(text);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    void uploadAssentos (final int index, TextView view, String text, JSONObject info){
        view.getBackground().setTint(activity.getResources().getColor(R.color.colorRedSPFC3));
        view.setText(text);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAssento(index, info);
            }
        });
    }

    void removeAssento (int index, JSONObject info){
        try {
            listener.romeverCadeira(info.getString("id"), info.getString("nome"));
            cadeirasViews.get(index).view.getBackground().setTint(activity.getResources().getColor(R.color.colorLoginFirst));
            cadeirasViews.get(index).selectedView.getBackground().setTint(activity.getResources().getColor(R.color.colorLoginFirst));
            cadeirasViews.get(index).view.setText("");
            cadeirasViews.get(index).cadeira = null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public interface Listener {
        void OnLoad();
        void onChose(String cadeira, String cadeiraNome, int index);
        void onChosseAll();
        void OnUnlock();
        void romeverCadeira(String cadeira, String cadeiraNome);
        void romeverCadeiras();
    }

    class ViewHolder {
        LinearLayout list;
        TextView textCampo;
        LinearLayout listaCadeiras;

        public ViewHolder (View view){
            list = view.findViewById(R.id.view_lists);
            textCampo = view.findViewById(R.id.txt_vista_campo);
            listaCadeiras = view.findViewById(R.id.view_cadeiras);
        }
    }

    class CadeiraView {
        private TextView view;
        private TextView selectedView;
        private CheckinCadeiraData cadeira;

        CadeiraView(TextView view, @Nullable CheckinCadeiraData cadeira) {
            this.view = view;
            this.cadeira = cadeira;
        }

        public CadeiraView(TextView view, TextView selectedView, CheckinCadeiraData cadeira) {
            this.view = view;
            this.selectedView = selectedView;
            this.cadeira = cadeira;
        }

        public TextView getView() {
            return view;
        }

        public void setView(TextView view) {
            this.view = view;
        }

        public CheckinCadeiraData getCadeira() {
            return cadeira;
        }

        public void setCadeira(CheckinCadeiraData cadeira) {
            this.cadeira = cadeira;
        }

        public TextView getSelectedView() {
            return selectedView;
        }

        public void setSelectedView(TextView selectedView) {
            this.selectedView = selectedView;
        }
    }
}
