package fragments;


import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sportm.fortaleza.R;

import java.text.DecimalFormat;
import java.util.Locale;

import data.AtletaData;

public class AtletaDetalsFragment extends Fragment {

    ViewHolder mHolder;
    private ClipDrawable mImageDrawable;
    AtletaData atleta;


    public AtletaDetalsFragment() {
        // Required empty public constructor
    }

    public AtletaDetalsFragment(AtletaData data) {
        this.atleta = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_atleta_detals, container, false);
        mHolder = new ViewHolder(view);
        initAtleta();
        return view;
    }

    void initAtleta(){
        mHolder.txtGols.setText(String.valueOf(atleta.getGols()));
        mHolder.txtAssistencias.setText(String.valueOf(atleta.getAssistencias()));
        mHolder.txtJogos.setText(String.valueOf(atleta.getJogos()));
        mHolder.txtCardAmarelo.setText(String.valueOf(atleta.getcAmarelos()));
        mHolder.txtCardVermelho.setText(String.valueOf(atleta.getcVermelhos()));

        initGols(atleta.getGols(), atleta.getAssistencias(), atleta.getJogos());
    }

    void initGols (float gols, float assistencias, int jogos ){
        if(gols > 0 && assistencias > 0 ){
            float porc = (gols + assistencias) / (float) jogos;
            String s = String.format(Locale.getDefault(), "%.2f", porc);
            mHolder.txtMediaGols.setText(s);
            mImageDrawable = (ClipDrawable) mHolder.progressGols.getDrawable();
            mImageDrawable.setLevel((int)(10000 * porc));
        }else{
            mHolder.txtMediaGols.setText("0,00");
        }
    }


    class ViewHolder {

        TextView txtMediaGols;
        ImageView progressGols;

        TextView txtCardAmarelo;
        TextView txtGols;
        TextView txtAssistencias;
        TextView txtJogos;

        TextView txtCardVermelho;

        public ViewHolder (View view){

            txtMediaGols = view.findViewById(R.id.txt_media_gols);
            progressGols = view.findViewById(R.id.progress_golls_balls);

            txtGols = view.findViewById(R.id.txt_gols_casa);
            txtAssistencias = view.findViewById(R.id.txt_assistencias);
            txtJogos = view.findViewById(R.id.txt_jogos);

            txtCardAmarelo = view.findViewById(R.id.txt_card_amarelo);
            txtCardVermelho = view.findViewById(R.id.txt_card_vermelho);
        }
    }

}
