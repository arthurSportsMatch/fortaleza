package fragments;


import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;
import com.google.android.gms.ads.AdSize;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Objects;

import data.QuizData;
import data.RespostasData;
import informations.UserInformation;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.OnLoadListener;
import utils.ProgressBarAnimation;
import utils.TRACKING;
import utils.UTILS;
import vmodels.utils.MoedasViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

public class PesquisaFragment extends MainFragment implements AsyncOperation.IAsyncOpCallback {

    private ViewHolder mHolder;
    private MoedasViewModel mMoedasViewModel;
    Dialog loading;

    private static final int OP_GET_PESQUISA            = 0;
    private static final int OP_SET_PESQUISA            = 1;
    private static final int OP_SET_JUMP_PESQUISA       = 2;

    QuizData pesquisa;
    boolean canAnswer = true;
    boolean respondeu;
    int pushID = -1;
    int max = 0;

    public PesquisaFragment() {
        // Required empty public constructor
    }

    public PesquisaFragment(int pushValue) {
        this.pushID = pushValue;
    }

    public void setPUSH (int pushValue){
        this.pushID = pushValue;
        if(loading!= null){
            loading.dismiss();
        }
        getPesquisa(pushID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.include_card_quiz, container, false);
        mHolder = new ViewHolder(view);

        if(activity.getResources().getDisplayMetrics().density > 1.0){
            LinearLayout mAdView = view.findViewById(R.id.adView);
            AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.HOME_TOP_BANNER_ID);
        }

        if(pushID > 0){
            getPesquisa(pushID);
        }else{
            getPesquisa();
        }
        return view;
    }

    @Override
    public void bind() {
        super.bind();
        mMoedasViewModel = new ViewModelProvider(requireActivity()).get(MoedasViewModel.class);
    }

    void getPesquisa (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PESQUISAS, OP_GET_PESQUISA,this).execute(params);
        loading = OnLoadListener.createLoadDialog(activity);
    }

    void getPesquisa (int ID){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idPergunta", ID);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PESQUISAS, OP_GET_PESQUISA,this).execute(params);
        loading = OnLoadListener.createLoadDialog(activity);
    }

    void initPesquisa (final QuizData quiz){
        pesquisa = quiz;
        canAnswer = true;
        respondeu = false;
        max = 0;

        //Mudar esqueleto
        mHolder.viewNext.setVisibility(View.VISIBLE);
        mHolder.viewAlignTop.setVisibility(View.VISIBLE);
        mHolder.viewAlign.setVisibility(View.VISIBLE);

        mHolder.scroll.fullScroll(ScrollView.FOCUS_UP);

        //Mudar valores de descrição
        mHolder.txtTitle.setText(quiz.getGrupo());
        mHolder.txtDescription.setText(quiz.getPergunta());

        if(mHolder.viewResults.getChildCount() > 0)
            mHolder.viewResults.removeAllViews();

        if(pesquisa.getImg() != null){
            UTILS.getImageAt(activity, pesquisa.getImg(), mHolder.background);
        }
//        else{
//            Glide.with(Objects.requireNonNull(activity))
//                    .load(R.drawable.splash_background)
//                    .into(mHolder.background);
//        }

        //Se é uma pesquisa com resposta aberta
        if(quiz.getRespAberta() == 1){
            View v = LayoutInflater.from(activity).inflate(R.layout.adapter_open_answer, mHolder.viewResults, false);
            final EditText answer = v.findViewById(R.id.edt_answer);
            final TextView enviada = v.findViewById(R.id.txt_resposta_enviada);
            final Button enviar = v.findViewById(R.id.btn_enviar);

            enviar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //retiro todos os espaços
                    if(canAnswer){
                        String resposta = answer.getText().toString();
                        resposta = resposta.replace(" ", "");

                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PESQUISAS_ABERTA, quiz.getId()).execute();
                        if(!resposta.isEmpty()){
                            respondeu = true;
                            canAnswer = false;
                            setPesquisa(quiz.getId(), null, answer.getText().toString(), false);
                            answer.setFocusable(false);
                            enviada.setVisibility(View.VISIBLE);
                            enviar.setVisibility(View.INVISIBLE);
                        }else{
                            answer.setError("Escreva sua resposta antes de enviar!");
                        }
                    }
                }
            });

            mHolder.viewResults.addView(v);
        }

        //Se é uma Pesquisa com respostas
        if(quiz.getRespostas() != null){

            //Perguntar respostas
            for(RespostasData r : quiz.getRespostas()){
                max += r.getQtd();
            }

            for(final RespostasData r : quiz.getRespostas()){
                View v = LayoutInflater.from(activity).inflate(R.layout.adapter_answer, null, false);

                final ProgressBar progressBar = v.findViewById(R.id.progress_answer);
                TextView tittle = v.findViewById(R.id.txt_answer);
                TextView porc = v.findViewById(R.id.txt_porc);

                tittle.setText(r.getResposta());

                porc.setVisibility(View.GONE);
                porc.setText(r.getQtd() + "%");

                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);

                Log.d("Resposta", "quant: " + r.getQtd() + " max: " + max);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(canAnswer){
                            respondeu = true;
                            canAnswer = false;

                            //pluss anwer
                            int rID = quiz.getRespostas().indexOf(r);
                            int quant = quiz.getRespostas().get(rID).getQtd();
                            quiz.getRespostas().get(rID).setQtd(quant+1);
                            max += 1;

                            if(pesquisa.getShowResults() != null && pesquisa.getShowResults() == 1){
                                setAnswer(quiz, r.getId());
                                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PESQUISAS_SELECIONAR, quiz.getId()).execute();
                            }else{
                                progressBar.setProgress(100);
                            }
                            setPesquisa(quiz.getId(), r.getId(), null, false);
                        }
                    }
                });
                mHolder.viewResults.addView(v);
            }
        }

        //Clicar em next
        mHolder.viewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quiz.getBtnNovoDesafio() == 1){
                    if(respondeu){
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PESQUISAS_PROXIMA, quiz.getId()).execute();
                        getPesquisa();
                    }else{
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PESQUISAS_PULAR, quiz.getId()).execute();
                        setPesquisa(quiz.getId(), null, null, true);
                    }
                }else{
                    initNonPesquisa("Todas as pesquisas foram respondidas");
                }
            }
        });

        mHolder.txtNext.setText("Próxima");
    }

    void initNonPesquisa (String msg){
        //Mudar esqueleto
        mHolder.viewNext.setVisibility(View.VISIBLE);
        mHolder.viewAlignTop.setVisibility(View.VISIBLE);
        mHolder.viewAlign.setVisibility(View.VISIBLE);

        //Mudar valores de descrição
        mHolder.txtTitle.setText(msg);
        mHolder.txtDescription.setText("");

        mHolder.txtNext.setText("Obrigado");

        if(mHolder.viewResults.getChildCount() > 0)
            mHolder.viewResults.removeAllViews();
    }

    void setAnswer (QuizData quiz, int id){
        mHolder.viewResults.removeAllViews();

        Log.d("Resposta", "max: " + max);

//        for(RespostasData r : quiz.getRespostas()){
//            if(r.getId() == id){
//                r.setQtd(r.getQtd() + 1);
//                max += 1;
//            }
//        }

        for(RespostasData r : quiz.getRespostas()){
            View v = LayoutInflater.from(activity).inflate(R.layout.adapter_answer, null, false);

            ProgressBar progressBar = v.findViewById(R.id.progress_answer);
            TextView tittle = v.findViewById(R.id.txt_answer);
            TextView porc = v.findViewById(R.id.txt_porc);

            tittle.setText(r.getResposta());

            Log.d("Resposta", "max: " + max);

            float porcentagem = (((float)r.getQtd() / max) * 100);
            DecimalFormat formater = new DecimalFormat("##.##");
            String porcenDecimal = formater.format(porcentagem);
            String finalPorcentagem = porcenDecimal + "%";//(Math.round(porcentagem)  + "%");

            Log.d("Resposta", "quant: " + r.getQtd() + " max: " + max + "porc" + Math.round(porcentagem)  + "%");

            porc.setVisibility(View.VISIBLE);
            porc.setText(finalPorcentagem);

            progressBar.setVisibility(View.VISIBLE);
            progressBar.setMax(max);

            //Animation
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(r.getQtd(), true);
            }else{
                ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, 0, r.getQtd());
                anim.setDuration(1000);
                progressBar.startAnimation(anim);
            }

            mHolder.viewResults.addView(v);
        }
    }

    void setPesquisa (int id, Integer resposta, String aberta, boolean jump){
        Hashtable<String, Object> params = new Hashtable<>();

        params.put("idPergunta", id);

        mHolder.txtNext.setText("Próxima");

        if(resposta != null)
            params.put("idResposta", resposta);

        if(aberta != null)
            params.put("aberta", aberta);
        if(jump){
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_PESQUISA, OP_SET_JUMP_PESQUISA,this).execute(params);
        }else{
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_PESQUISA, OP_SET_PESQUISA,this).execute(params);
        }

    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;
            loading.dismiss();

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PESQUISA :{
                int status = 0;
                Log.d("Async-PEsquisa",response.toString());
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(response.has("Object")){
                    if(status == 200){
                        try {
                            if(!response.has("msg")){
                                Gson gson = new Gson();
                                QuizData data = gson.fromJson(response.getString("Object"), QuizData.class);

                                if(data != null){
                                    initPesquisa(data);
                                }
                            }else{
                                String msg = response.getString("msg");

                                if(msg != null){
                                    initNonPesquisa(msg);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
            case OP_SET_PESQUISA :{
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");

                        if(status == 200){
                            mMoedasViewModel.giveMoedas(pesquisa.getMoedas());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;

            case OP_SET_JUMP_PESQUISA :{
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");

                        if(status == 200){
                            getPesquisa();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }

    class ViewHolder {
        TextView txtTitle;
        TextView txtDescription;
        NestedScrollView scroll;

        //resultados
        LinearLayout viewResults;

        //alinhamento (ultra necessario)
        LinearLayout viewAlign;
        LinearLayout viewAlignTop;

        //botão de ação
        LinearLayout viewAction;
        TextView txtCategoryButton;

        //arrastar para cima
        LinearLayout viewNext;
        TextView txtNext;

        ImageView background;

        public ViewHolder (View view){
            txtTitle = view.findViewById(R.id.txt_nome_quiz);
            txtDescription = view.findViewById(R.id.txt_description_quiz);
            viewResults = view.findViewById(R.id.view_results_quiz);
            scroll = view.findViewById(R.id.swipe_view);

            viewAlign = view.findViewById(R.id.space_bottom_card_quiz);
            viewAlignTop = view.findViewById(R.id.space_bottom_card_quiz_0);

            viewNext = view.findViewById(R.id.view_next);
            txtNext = view.findViewById(R.id.txt_next);

            viewAction = view.findViewById(R.id.view_action_quiz);
            txtCategoryButton = view.findViewById(R.id.txt_categoria_2_quiz);

            background = view.findViewById(R.id.img_background);
        }
    }
}
