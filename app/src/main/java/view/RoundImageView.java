package view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.sportm.fortaleza.R;

public class RoundImageView extends ImageView {

    private int roundImageViewColorStart;
    private int roundImageViewColorEnd;
    private int roundImageViewRadius;
    private int roundImageViewOrientation;
    private boolean roundTopLeft = true;
    private boolean roundTopRight = true;
    private boolean roundBottomRight = true;
    private boolean roundBottomLeft = true;

    public RoundImageView(Context context, AttributeSet attrs) {

        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RoundImageView,
                0, 0);

        try {
            roundImageViewColorStart = (a == null) ? ContextCompat.getColor(getContext(), R.color.colorWhite) : a.getColor(R.styleable.RoundImageView_roundImageViewColorStart, ContextCompat.getColor(getContext(), R.color.colorWhite));
            roundImageViewColorEnd = (a == null) ?  ContextCompat.getColor(getContext(), R.color.colorWhite) : a.getColor(R.styleable.RoundImageView_roundImageViewColorEnd, ContextCompat.getColor(getContext(), R.color.colorWhite));
            roundImageViewRadius = (a == null) ? 0 : a.getInteger(R.styleable.RoundImageView_roundImageViewRadius, 0);
            roundImageViewOrientation = (a == null) ? context.getResources().getInteger(0) : a.getInteger(R.styleable.RoundImageView_roundImageGradientOrientation, 0);
            roundTopLeft = (a == null) || a.getBoolean(R.styleable.RoundImageView_roundImageTopLeft, true);
            roundTopRight = (a == null) || a.getBoolean(R.styleable.RoundImageView_roundImageTopRight, true);
            roundBottomRight = (a == null) || a.getBoolean(R.styleable.RoundImageView_roundImageBottomRight, true);
            roundBottomLeft = (a == null) || a.getBoolean(R.styleable.RoundImageView_roundImageBottomLeft, true);
        } finally {
            a.recycle();
        }

//        if(isInEditMode()){
        InitDrawable();
//        }
    }

    public int getRoundImageViewColorStart() {
        return roundImageViewColorStart;
    }

    public void setRoundImageViewColorStart(int color) {
        roundImageViewColorStart = color;
        invalidate();
        requestLayout();
    }

    public int getRoundImageViewColorEnd() {
        return roundImageViewColorEnd;
    }

    public void setRoundImageViewColorEnd(int color) {
        roundImageViewColorEnd = color;
        invalidate();
        requestLayout();
    }

    public int getRoundImageViewRadius() {
        return roundImageViewRadius;
    }

    public void setRoundImageViewRadius(int radius) {
        roundImageViewRadius = radius;
        invalidate();
        requestLayout();
    }


    public int getRoundImageViewOrientation() {
        return roundImageViewOrientation;
    }

    public void setRoundImageViewOrientation(int roundImageViewOrientation) {
        this.roundImageViewOrientation = roundImageViewOrientation;
        invalidate();
        requestLayout();
    }

    public boolean isRoundTopLeft() {
        return roundTopLeft;
    }

    public void setRoundTopLeft(boolean roundTopLeft) {
        this.roundTopLeft = roundTopLeft;
        invalidate();
        requestLayout();
    }

    public boolean isRoundTopRight() {
        return roundTopRight;
    }

    public void setRoundTopRight(boolean roundTopRight) {
        this.roundTopRight = roundTopRight;
        invalidate();
        requestLayout();
    }

    public boolean isRoundBottomRight() {
        return roundBottomRight;
    }

    public void setRoundBottomRight(boolean roundBottomRight) {
        this.roundBottomRight = roundBottomRight;
        invalidate();
        requestLayout();
    }

    public boolean isRoundBottomLeft() {
        return roundBottomLeft;
    }

    public void setRoundBottomLeft(boolean roundBottomLeft) {
        this.roundBottomLeft = roundBottomLeft;
        invalidate();
        requestLayout();
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        InitDrawable();
    }

    void InitDrawable(){

        GradientDrawable drawable = new GradientDrawable();
        drawable.setDither(true);
        drawable.setColors(new int[]{getRoundImageViewColorStart(), getRoundImageViewColorEnd()});
//        drawable.setCornerRadius(getRoundImageViewRadius());
        //are ordered top-left, top-right, bottom-right, bottom-left
//        drawable.setCornerRadii(new float[]{100f, 100f, 100f, 100f, 0.0f, 0.0f, 0.0f, 0.0f});

        float[] radii = new float[]{0, 0, 0, 0, 0, 0, 0, 0};
        if(roundTopLeft){
            radii[0] = getRoundImageViewRadius();
            radii[1] = getRoundImageViewRadius();
        }

        if(roundTopRight){
            radii[2] = getRoundImageViewRadius();
            radii[3] = getRoundImageViewRadius();
        }

        if(roundBottomRight){
            radii[4] = getRoundImageViewRadius();
            radii[5] = getRoundImageViewRadius();
        }
        if(roundBottomLeft){
            radii[6] = getRoundImageViewRadius();
            radii[7] = getRoundImageViewRadius();
        }

        drawable.setCornerRadii(radii);

        drawable.setOrientation( ((getRoundImageViewOrientation() >= 0) || (getRoundImageViewOrientation() <= 7)) ? GradientDrawable.Orientation.values()[getRoundImageViewOrientation()] : GradientDrawable.Orientation.TOP_BOTTOM );

        setBackground(drawable.getConstantState().newDrawable());
    }

    @Override
    protected void drawableStateChanged(){
        super.drawableStateChanged();
    }


}
