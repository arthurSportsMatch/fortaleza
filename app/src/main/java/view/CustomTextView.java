package view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sportm.fortaleza.R;

import utils.UTILS;

/**
 * Created by rafael.verginelli on 11/03/2016.
 */
public class CustomTextView extends TextView {

    private boolean customTextViewHasBg;
    private Drawable customTextViewBgId;
    private int customTextViewBgColor;

    public void setCustomTextViewHasBg(boolean customTextViewHasBg){
        this.customTextViewHasBg = customTextViewHasBg;
        invalidate();
        requestLayout();
    }

    public void setCustomTextViewBgId(Drawable customTextViewBgId){
        this.customTextViewBgId = customTextViewBgId;
        invalidate();
        requestLayout();
    }

    public void setCustomTextViewBgColor(int customTextViewBgColor){
        this.customTextViewBgColor = customTextViewBgColor;
        invalidate();
        requestLayout();
    }


    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setCustomFont(context, attrs);

        initialize(context, attrs);
    }

    void initialize(Context context, AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        try {

            setCustomTextViewHasBg((a != null) && a.getBoolean(R.styleable.CustomTextView_customTextViewHasBg, false));
            if(customTextViewHasBg){
                setCustomTextViewBgId( ( (a == null) || (a.getDrawable(R.styleable.CustomTextView_customTextViewBgId) == null) )? ContextCompat.getDrawable(context, R.drawable.answer_button_bg).mutate().getConstantState().newDrawable() : a.getDrawable(R.styleable.CustomTextView_customTextViewBgId) );
                setCustomTextViewBgColor ( (a == null) ? ContextCompat.getColor(context, R.color.colorCorrect) : a.getColor(R.styleable.CustomTextView_customTextViewBgColor, ContextCompat.getColor(context, R.color.colorCorrect)) );
            }

        } finally {
            a.recycle();
        }

        initDrawable();
    }

    void initDrawable(){
        if(customTextViewHasBg){
            customTextViewBgId.mutate().setColorFilter(customTextViewBgColor, PorterDuff.Mode.SRC_IN);
            setBackground(customTextViewBgId);
        }
        else{
            setBackground(null);
        }
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String font = ( (a == null) || (a.getString(R.styleable.CustomTextView_customFont) == null) ) ? "fonts/Gotham-Bold.ttf" :a.getString(R.styleable.CustomTextView_customFont);
        String customFont = (font != null && font.length() > 0) ? font : "fonts/Gotham-Bold.ttf";
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {

        if( (asset == null) || (asset.length() <= 0) ){
            return false;
        }

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            UTILS.DebugLog("CustomTextView", "Could not get typeface: " + e.getMessage());
            return false;
        }

        setTypeface(tf);
        return true;
    }
}