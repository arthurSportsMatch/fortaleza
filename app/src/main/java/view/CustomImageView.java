package view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.sportm.fortaleza.R;

/**
 * Created by rafael.verginelli on 16/06/2016.
 */
public class CustomImageView extends ImageView {

    private int mDrawableColor;
    private Drawable mDrawableId;

    public void setMDrawableColor(int mDrawableColor){
        this.mDrawableColor = mDrawableColor;
        invalidate();
        requestLayout();
    }

    public void setMDrawableId(Drawable mDrawableId){
        this.mDrawableId = mDrawableId;
        invalidate();
        requestLayout();
    }


    public CustomImageView(Context context, AttributeSet attrs) {

        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomImageView,
                0, 0);

        try {
            setMDrawableColor( a.getColor(R.styleable.CustomImageView_drawableColor, -1) );
            setMDrawableId( a.getDrawable(R.styleable.CustomImageView_drawableId) );
        } finally {
            a.recycle();
        }

        if(isInEditMode()){
            InitDrawable();
        }
    }

    public int getDrawableColor() {
        return mDrawableColor;
    }

    public void setDrawableColor(int color) {
        mDrawableColor = color;
        invalidate();
        requestLayout();
    }

    public Drawable getDrawableId() {
        return mDrawableId;
    }

    public void setDrawableId(Drawable drawable) {
        mDrawableId = drawable;
        invalidate();
        requestLayout();
    }

    void InitDrawable(){
        Drawable drawable = null;
        drawable = getDrawableId();
        drawable = (drawable != null) ? drawable.getConstantState().newDrawable() : null;//ContextCompat.getDrawable(getContext(), R.drawable.icon_logo).getConstantState().newDrawable();

        if(drawable != null) {
            if (getDrawableColor() != -1) {
                drawable.mutate().setColorFilter(getDrawableColor(), PorterDuff.Mode.SRC_IN);
            }

            setImageDrawable(drawable);
        }
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        InitDrawable();
    }

    @Override
    protected void drawableStateChanged(){
        super.drawableStateChanged();
    }
}
