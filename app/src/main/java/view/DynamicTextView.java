package view;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.sportm.fortaleza.R;


public class DynamicTextView extends AppCompatTextView {

    private int refcode = 0;
    private String defaultText = "";
    Context context;
    AttributeSet attrs;
    private String extraString = "";

    public int getRefcode() {
        return refcode;
    }

    public void setRefcode(int refcode) {
        setRefcode(refcode, "");
    }

    public String getDefaultText() {
        return defaultText;
    }

    public void setDefaultText(String defaultText) {
        this.defaultText = defaultText;
    }

    public String getExtraString() {
        return extraString;
    }

    public void setExtraString(String extraString) {
        this.extraString = extraString;
    }

    public void setRefcode(int refcode, String extraString) {
        this.refcode = refcode;
        this.extraString = extraString;
        //Localize();
    }

    public DynamicTextView(Context context) {
        super(context);
        this.context = context;
        setWillNotDraw(false);
        Init();
    }

    public DynamicTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        setWillNotDraw(false);
        Init();
    }

    void Init(){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DynamicTextView,
                0, 0);

        try {
            if(isInEditMode()) {
                setRefcode ((a == null) ? 0 : a.getResourceId(R.styleable.DynamicTextView_refCode, 0));
            }
            else {
                setRefcode ((a == null) ? 0 : a.getInteger(R.styleable.DynamicTextView_refCode, 0));
            }

            setDefaultText((a == null) ? "" : a.getString(R.styleable.DynamicTextView_defaultText));

        } finally {
            a.recycle();
        }

        //Localize();
    }

    void Localize(){
//        if(isInEditMode()){
//            String txt = LocalizationHelper.internal_use_only_get_string_by_refcode(getRefcode());
//            txt = ( txt != null && txt.isEmpty() && getDefaultText() != null &&
//                    !getDefaultText().isEmpty()) ? getDefaultText() : txt;
//            txt = txt.concat(getExtraString());
//            setText(txt);
//        }
//        else {
//            String txt = LocalizationHelper.getString(getRefcode());
//            txt = ( txt != null && txt.isEmpty() && getDefaultText() != null &&
//                    !getDefaultText().isEmpty()) ? getDefaultText() : txt;
//            txt = txt.concat(getExtraString());
//            setText(txt);
//        }
    }

}
