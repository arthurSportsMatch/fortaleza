package view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import com.sportm.fortaleza.R;

/**
 * Created by rafael.verginelli on 6/15/16.
 */
public class GradientBGView extends FrameLayout {

    private FrameLayout gradientBG;
    private CustomImageView bgIcons;
    private int startColor;
    private int endColor;
    private boolean displayIcons;

    public boolean isDisplayIcons(){
        return  displayIcons;
    }

    public void setDisplayIcons(boolean displayIcons){
        this.displayIcons = displayIcons;
        invalidate();
        requestLayout();
        initDrawable();
    }

    public int getEndColor() {
        return endColor;
    }

    public void setEndColor(int endColor) {
        this.endColor = endColor;
        invalidate();
        requestLayout();
        initDrawable();
    }

    public int getStartColor() {
        return startColor;
    }

    public void setStartColor(int startColor) {
        this.startColor = startColor;
        invalidate();
        requestLayout();
        initDrawable();
    }

    public GradientBGView(Context context) {
        super(context);
        initialize(context, null);
    }

    public GradientBGView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    void initialize(Context context, AttributeSet attrs){

        inflate(context, R.layout.gradient_bg_view, this);
        TypedArray a = null;
        if(attrs != null) {
            a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.GradientBGView, 0, 0);
        }

        try {
            gradientBG = findViewById(R.id.gradientBG);
            bgIcons = findViewById(R.id.bgIcons);

            startColor = (a == null) ? ContextCompat.getColor(context, R.color.design_default_color_primary) : a.getColor(R.styleable.GradientBGView_startColor, ContextCompat.getColor(context, R.color.colorRedSPFC5));
            endColor = (a == null) ? ContextCompat.getColor(context, R.color.colorPrimaryDark) : a.getColor(R.styleable.GradientBGView_endColor, ContextCompat.getColor(context, R.color.colorPrimaryDark));
            displayIcons = (a == null) || a.getBoolean(R.styleable.GradientBGView_displayIcons, true);
        }
        finally {
            if(a != null){
                a.recycle();
            }
        }

//        if(!isInEditMode()) {
        initDrawable();
//        }

    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        initDrawable();
    }

    void initDrawable(){

        int[] colors = new int[2];
        colors[0] = startColor;
        colors[1] = endColor;

        if(bgIcons != null){
            bgIcons.setVisibility(displayIcons ? VISIBLE : GONE);
        }

        LayerDrawable layerDrawable = (LayerDrawable) ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg).mutate().getConstantState().newDrawable();
        if(layerDrawable != null) {
            GradientDrawable gradientDrawable = (GradientDrawable) (layerDrawable.findDrawableByLayerId(R.id.gd)).mutate().getConstantState().newDrawable();

            if (gradientDrawable != null) {
                gradientDrawable.setColors(colors);

                gradientBG.setBackground(gradientDrawable);
            }
        }

    }
}