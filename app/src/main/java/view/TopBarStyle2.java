package view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import com.sportm.fortaleza.R;

/**
 * Created by rafael.verginelli on 6/17/16.
 */
public class TopBarStyle2 extends FrameLayout {

    private GradientBGView frameTopBar2Bg;
    private CustomTextView txtTitle;
    private LinearLayout leftButton;
    private FrameLayout leftIconBtn;
    private CustomImageView imgLeftIcon;
    private CustomTextView txtLeftButtonText;

    private int topBar2BgColorStart;
    private int topBar2BgColorEnd;
    private String topBar2Title;
    private int topBar2TitleColor;
    private boolean topBar2UpperCase;
    private boolean topBar2HasLeftButton;
    private boolean topBar2HasLeftIcon;
    private int topBar2LeftButtonColor;
    private int topBar2LeftIconColor;
    private String topBar2LeftButtonText;
    private Drawable topBar2LeftIconId;

    public CustomTextView getTxtTitle(){
        return txtTitle;
    }

    public int getTopBar2BgColorEnd() {
        return topBar2BgColorEnd;
    }

    public void setTopBar2BgColorEnd(int topBar2BgColorEnd) {
        this.topBar2BgColorEnd = topBar2BgColorEnd;
        invalidate();
        requestLayout();
    }

    public int getTopBar2BgColorStart() {
        return topBar2BgColorStart;
    }

    public void setTopBar2BgColorStart(int topBar2BgColorStart) {
        this.topBar2BgColorStart = topBar2BgColorStart;
        invalidate();
        requestLayout();
    }

    public CustomTextView getTxtLeftButtonText() {
        return txtLeftButtonText;
    }

    public void setTxtLeftButtonText(CustomTextView txtLeftButtonText) {
        this.txtLeftButtonText = txtLeftButtonText;
        invalidate();
        requestLayout();
    }

    public CustomImageView getImgLeftIcon() {
        return imgLeftIcon;
    }

    public void setImgLeftIcon(CustomImageView imgLeftIcon) {
        this.imgLeftIcon = imgLeftIcon;
        invalidate();
        requestLayout();
    }

    public FrameLayout getLeftIconBtn() {
        return leftIconBtn;
    }

    public void setLeftIconBtn(FrameLayout leftIconBtn) {
        this.leftIconBtn = leftIconBtn;
        invalidate();
        requestLayout();
    }

    public LinearLayout getLeftButton() {
        return leftButton;
    }

    public void setLeftButton(LinearLayout leftButton) {
        this.leftButton = leftButton;
        invalidate();
        requestLayout();
    }

    public TopBarStyle2(Context context) {
        super(context);
        initialize(context, null);
    }

    public TopBarStyle2(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    void initialize(Context context, AttributeSet attrs){

        inflate(context, R.layout.top_bar_style_2_view, this);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TopBarStyle2,
                0, 0);

        try {
            txtTitle = findViewById(R.id.txtTitle);
            frameTopBar2Bg = findViewById(R.id.frameTopBarBg);

            txtLeftButtonText = findViewById(R.id.txtLeftButtonText);

            leftButton = findViewById(R.id.leftButton);
            leftIconBtn = findViewById(R.id.leftIconBtn);

            imgLeftIcon = findViewById(R.id.imgLeftIcon);

            topBar2Title = (a == null) ? "" : a.getString(R.styleable.TopBarStyle2_topBar2Title);
            topBar2TitleColor = (a == null) ? ContextCompat.getColor(context, R.color.colorLoginFirst) : a.getColor(R.styleable.TopBarStyle2_topBar2TitleColor, ContextCompat.getColor(context, R.color.colorLoginFirst));
            topBar2BgColorStart = (a == null) ? ContextCompat.getColor(context, R.color.colorGreenLight) : a.getColor(R.styleable.TopBarStyle2_topBar2BgColorStart, ContextCompat.getColor(context, R.color.colorGreenLight));
            topBar2BgColorEnd = (a == null) ? ContextCompat.getColor(context, R.color.colorGreenLight) : a.getColor(R.styleable.TopBarStyle2_topBar2BgColorEnd, ContextCompat.getColor(context, R.color.colorGreenLight));
            topBar2UpperCase = (a == null) || a.getBoolean(R.styleable.TopBarStyle2_topBar2UpperCase, true);
            topBar2HasLeftButton = (a != null) && a.getBoolean(R.styleable.TopBarStyle2_topBar2HasLeftButton, false);
            topBar2HasLeftIcon = (a != null) && a.getBoolean(R.styleable.TopBarStyle2_topBar2HasLeftIcon, false);
            topBar2LeftButtonColor = (a == null) ? ContextCompat.getColor(context, R.color.colorLoginFirst) : a.getColor(R.styleable.TopBarStyle2_topBar2LeftButtonColor, ContextCompat.getColor(context, R.color.colorLoginFirst));
            topBar2LeftIconColor = (a == null) ? ContextCompat.getColor(context, R.color.colorLoginFirst) : a.getColor(R.styleable.TopBarStyle2_topBar2LeftIconColor, ContextCompat.getColor(context, R.color.colorLoginFirst));
            topBar2LeftButtonText = (a == null) ? "" : a.getString(R.styleable.TopBarStyle2_topBar2LeftButtonText);
            topBar2LeftIconId = (a == null) ? null : a.getDrawable(R.styleable.TopBarStyle2_topBar2LeftIconId);

        } finally {
            if(a != null) {
                a.recycle();
            }
        }

        initDrawable();

    }

    void initDrawable(){
        if(txtTitle != null){
            txtTitle.setText(topBar2Title);
            txtTitle.setTextColor(topBar2TitleColor);
            txtTitle.setAllCaps(topBar2UpperCase);
        }

        if(frameTopBar2Bg != null) {
            frameTopBar2Bg.setStartColor(topBar2BgColorStart);
            frameTopBar2Bg.setEndColor(topBar2BgColorEnd);
        }

        if(leftButton != null) {
            if (topBar2HasLeftButton) {
                leftButton.setVisibility(VISIBLE);

                if(txtLeftButtonText != null){
                    txtLeftButtonText.setText(topBar2LeftButtonText);
                    txtLeftButtonText.setTextColor(topBar2TitleColor);
                }

                if(topBar2HasLeftIcon){
                    if(leftIconBtn != null) {
                        leftIconBtn.setVisibility(VISIBLE);
                    }

                    if(imgLeftIcon != null){
                        imgLeftIcon.setVisibility(VISIBLE);
                        imgLeftIcon.setImageDrawable(topBar2LeftIconId);
                        imgLeftIcon.setColorFilter(topBar2LeftIconColor, PorterDuff.Mode.SRC_IN);
                    }
                }
                else{
                    if(leftIconBtn != null) {
                        leftIconBtn.setVisibility(INVISIBLE);
                    }
                }

            } else {
                leftButton.setVisibility(GONE);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        initDrawable();
    }
}
