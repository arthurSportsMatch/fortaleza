package repository;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.sportm.fortaleza.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.SecurePreferences;
import utils.UTILS;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class MainRepository implements AsyncOperation.IAsyncOpCallback {

    protected Activity context;

    //Local Data
    protected SecurePreferences securePreferences;
    public static final String SHARED_KEY_USER                          = "prts:user:11FsERsC$*sdaQ!2";
    public static final String SHARED_KEY_REGULAMENTOS                  = "prts:regulamentos:11ERG3e&(;sdaQ!4";
    public static final String SHARED_KEY_SERVERS                       = "prts:server:11EjsdhdsJUU#$#*(SDF4";
    public static final String SHARED_KEY_SOCIO                         = "prts:socio:11Ejguys#$%IUHSDHDAˆˆ*ˆˆ@";
    public static final String SHARED_KEY_SOCIO_USER                    = "prts:socio:user:FJFQ!&*72easd8919d!@";
    public static final String SHARED_KEY_TWITTER_TOKEN                 = "prts:twitter:FIOHSFDAIPU@#&*#_)FD12@";

    @Override
    public  void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        int status = 0;
        boolean sucess = false;

        if(response.has("Status") || response.has("status")){
            try {

                if(response.has("Status"))
                    status = response.getInt("Status");

                if(response.has("status"))
                    status = response.getInt("status");

                if(status == 200){
                    sucess = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(response.has("idServidor")){
            Log.d("Server", "Has idServidor");
            try {
                int serverId = (int) response.get("idServidor");
                if(UserInformation.getServerData() != null){
                    UserInformation.getServerData().setServerId(serverId);
                }
                UTILS.salvarServidor(UserInformation.getServerData());
                CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServer(serverId) + "api/"+CONSTANTS.serverVersion);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(sucess){
            onSucces(opId, status, response);
        }else{
            onError(opId, status, response);
        }

    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        onError(opId, 0, response);
    }

    public void onSucces (int opId, int status, JSONObject response){
    }

    public void onError (int opId, int status, JSONObject response){
    }
}
