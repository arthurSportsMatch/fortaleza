package repository.campo;

import android.app.Activity;

import repository.MainRepository;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class CampoRepository extends MainRepository {

    public CampoRepository (Activity context){
        this.context = context;
    }
}
