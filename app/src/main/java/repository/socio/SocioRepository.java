package repository.socio;

import android.app.Activity;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import data.ServerCustomData;
import repository.MainRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.SecurePreferences;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class SocioRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private final int OP_GET_SOCIO_API = 0;

    public SocioRepository() {
    }

    public SocioRepository(Activity context) {
        this.context = context;
    }

    public SocioRepository(Activity context, SecurePreferences preferences) {
        this.context = context;
        this.securePreferences = preferences;
    }

    public void getSocioAPI (AsyncResponse response){
        this.asyncResponse = response;
        getSocioApi();
    }

    private void getSocioApi (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_SOCIO_API, OP_GET_SOCIO_API, this).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        switch (opId){
            case OP_GET_SOCIO_API: {
                if (response != null){
                    try {
                        JSONObject obj = response.getJSONObject("Object");
                        CONSTANTS.apiServerURL = obj.getString("endpoint");
                        CONSTANTS.apikey = obj.getString("key");
                        asyncResponse.onResponseMessage("SocioAPI Atualizada");
                        saveSocioApi();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    private void unSaveSocioApi (){

    }

    private void saveSocioApi (){
        Gson gson = new Gson();
        securePreferences.removeValue(SHARED_KEY_SOCIO);
        //securePreferences.put(SHARED_KEY_SERVERS, gson.toJson(server));
    }

    public interface AsyncResponse {
        void onResponseSucces (ServerCustomData data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
