package repository.nextgames;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.PartidaData;
import models.nextgames.NextGameModel;
import repository.MainRepository;
import repository.badges.BadgeRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NextGamesRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private final int OP_GET_JOGOS = 0;

    public NextGamesRepository() {}
    public NextGamesRepository(Activity context) {
        this.context = context;
    }


    public void getList(int home, AsyncResponse response){
        this.asyncResponse = response;
        getList(home);
    }

    private void getList (int home){
        Hashtable<String,Object> params = new Hashtable<>();
        params.put("home", home);
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_JOGOS, OP_GET_JOGOS, this).execute(params);
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        if(opId == OP_GET_JOGOS){
            if(response.has("Object")){
                try {
                    Gson gson = new Gson();
                    JSONObject obj = response.getJSONObject("Object");
                    PartidaData[] data = gson.fromJson(obj.getString("itens"), PartidaData[].class);

                    int hide = 0;
                    if(obj.has("hide"))
                        hide = obj.getInt("hide");

                    if(data != null && data.length > 0){
                        List<PartidaData> partidaData = new ArrayList<>(Arrays.asList(data));
                        asyncResponse.onResponseSucces(new NextGameModel(hide, partidaData));
                    }else{
                        asyncResponse.onResponseMessage("Sem Jogos");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (NextGameModel data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
