package repository.lastgames;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.PartidaData;
import models.nextgames.NextGameModel;
import repository.MainRepository;
import repository.nextgames.NextGamesRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LastGamesRepository  extends MainRepository {
    private AsyncResponse asyncResponse;
    private final int OP_GET_JOGOS = 0;

    public LastGamesRepository() {}
    public LastGamesRepository(Activity context) {
        this.context = context;
    }

    public void getList(AsyncResponse response){
        this.asyncResponse = response;
        getList();
    }

    private void getList (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_LAST_RESULTADOS, OP_GET_JOGOS, this).execute(params);
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        if(opId == OP_GET_JOGOS){
            if(response.has("Object")){
                try {
                    Gson gson = new Gson();
                    JSONObject obj = response.getJSONObject("Object");
                    PartidaData[] data = gson.fromJson(obj.getString("itens"), PartidaData[].class);
                    if(data != null){
                        List<PartidaData> partidaData = new ArrayList<>(Arrays.asList(data));
                        asyncResponse.onResponseSucces(partidaData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<PartidaData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
