package repository.user;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import data.SocioData;
import informations.UserInformation;
import models.user.UserModel;
import repository.MainRepository;
import utils.AsyncOperation;
import utils.SecurePreferences;
import utils.UTILS;

import static utils.UTILS.FLIP_HORIZONTAL;
import static utils.UTILS.FLIP_VERTICAL;
import static utils.UTILS.flip;
import static utils.UTILS.rotateBitmap;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class UserRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private UserModel savedUser;

    private final int OP_GET_USER = 0;
    private final int OP_UPLOAD_USER = 1;
    private final int OP_UPLOAD_FOTO = 2;
    private final int OP_POST_LOGIN = 3;
    private final int OP_POST_CREATE = 4;
    private final int OP_FORGOT = 5;
    private final int OP_CHANGE_PASSWORD = 8;

    private final int OP_GET_SOCIO = 6;
    private final int OP_SET_SOCIO = 7;

    public UserRepository() {}

    public UserRepository(Activity context) {
        this.context = context;
    }

    public UserRepository(Activity context, SecurePreferences preferences) {
        this.context = context;
        this.securePreferences = preferences;
        getSavedUser();
    }

    //region Get Methods
    //Pega o usuario
    public void getUser (AsyncResponse response){
        this.asyncResponse = response;
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_USER, OP_GET_USER, this).execute();
    }

    public void getSaved (AsyncResponse response){
        this.asyncResponse = response;
        if(hasUser()){
            if (savedUser == null) {
                UserModel model;
                Gson gson = new Gson();
                String nData = securePreferences.getString(SHARED_KEY_USER, "");
                model = gson.fromJson(nData, UserModel.class);
                savedUser = model;
            }
            asyncResponse.onResponseSucces(changeUserModelValues(savedUser));
        }else{
            asyncResponse.onResponseError("Sem Usuario Salvo");
        }
    }

    //Faz upload das informações do usuario
    public void uploadUser (UserModel user, AsyncResponse response){
        this.asyncResponse = response;
        uploadUser(user);
    }

    public void saveUser (UserModel user, boolean hasRequest){
        saveUser(changeUserModelValues(user));
    }

    //Faz upload da foto
    public void uploadFoto (Uri photoUri, int cam, boolean isCam, AsyncResponse response){
        this.asyncResponse = response;
        uploadPhoto(cam, photoUri, isCam);
    }

    //Faz login do usuario
    public void loginUser (String email, String password, @Nullable String id, AsyncResponse response){
        this.asyncResponse = response;
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("email", email);
        params.put("senha", password);

        //ID de retorno do servidor (opcional)
        if(id != null)
            params.put("idUser", id);

        new AsyncOperation(context, AsyncOperation.TASK_ID_SIGN_IN, OP_POST_LOGIN, this).execute(params);
    }

    //Cria novo usuario
    public void createUser (String name, String email, String password, String id, AsyncResponse response){
        this.asyncResponse = response;
        savedUser = new UserModel(name, email);

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("nome", name);
        params.put("email", email );
        params.put("senha", password);
        params.put("idUser", id); //ID de retorno do servidor (neste caso não é opcional)
        new AsyncOperation(context, AsyncOperation.TASK_ID_SIGN_UP, OP_POST_CREATE, this).execute(params);
    }

    //Esqueceu a senha
    public void forgot (String email, AsyncResponse response){
        this.asyncResponse = response;
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("email", email);
        new AsyncOperation(context, AsyncOperation.TASK_ID_PASSWORD_FORGOTTEN, OP_FORGOT, this).execute(params);
    }

    //Get socio
    public void getSocio (AsyncResponse response){
        this.asyncResponse  = response;
        if(savedUser != null){
            getSocio(savedUser.getCpf());
        }else{
            getSavedUser();
            getSocio(response);
        }
    }

    //Get socio
    public void getSocio (String cpf,AsyncResponse response){
        this.asyncResponse  = response;
        getSocio(cpf);
    }

    //Muda a senha do usuario
    public void changePassword (String senha, AsyncResponse response){
        this.asyncResponse = response;
        changePassword(senha);
    }
    //endregion

    private Hashtable<String, Object> passUserInfos (UserModel user){
        Hashtable<String, Object> params = new Hashtable<String, Object>();

        if(savedUser != null){
            getSavedUser();
        }

        if(!user.getEmail().isEmpty()) {
            if(!user.getEmail().equals(savedUser.getEmail())){
                params.put("email", user.getEmail());
                savedUser.setEmail(user.getEmail());
            }
        }

        if(!user.getNome().isEmpty()) {
            if(!user.getNome().equals(savedUser.getNome())){
                params.put("nome", user.getNome());
                savedUser.setNome(user.getNome());
            }
        }

        if(!user.getCpf().isEmpty()) {
            if(!user.getCpf().equals(savedUser.getCpf())){
                params.put("cpf", user.getCpf());
                savedUser.setCpf(user.getCpf());
            }
        }

        if(!user.getCelular().isEmpty()) {
            if(!user.getCelular().equals(savedUser.getCelular()) ){
                params.put("celular",user.getCelular());
                savedUser.setCelular(user.getCelular());
            }
        }


        if(!user.getDn().isEmpty()){
            if(!user.getDn().equals(savedUser.getDn())){
                params.put("dn", UTILS.formatBirthDate(user.getDn()));
                savedUser.setDn(user.getDn());
            }
        }

        if(!user.getSexo().isEmpty()){
            if(!user.getSexo().equals(savedUser.getSexo())){
                params.put("sexo", user.getSexo());
                savedUser.setSexo(user.getSexo());
            }
        }

        if(user.getSocioTorcedor() != savedUser.getSocioTorcedor()){
            params.put("socioTorcedor", user.getSocioTorcedor());
            savedUser.setSocioTorcedor(user.getSocioTorcedor());
        }

        if(user.getSocioBeneficio() != savedUser.getSocioBeneficio()){
            savedUser.setSocioBeneficio(user.getSocioBeneficio());
        }

        if(!user.getSocioStatus().isEmpty()){
            if(!user.getSocioStatus().equals(savedUser.getSocioStatus())){
                savedUser.setSocioStatus(user.getSocioStatus());
            }
        }

        return params;
    }

    private UserModel changeUserModelValues (UserModel nModel){
        UserModel model = new UserModel();

        if(hasUser()){
            Gson gson = new Gson();
            String nData = securePreferences.getString(SHARED_KEY_USER, "");
            model = gson.fromJson(nData, UserModel.class);
        }

        //Faz parce dos dados de forma segura
        model.secureParce(nModel);
        UserInformation.setUserId(model.getId());

        //Salvar usuario na memoria
        saveUser(model);
        return model;
    }

    public boolean hasUser (){
        return securePreferences.containsKey(SHARED_KEY_USER);
    }

    private void saveUser (UserModel user){
        Gson gson = new Gson();
        securePreferences.removeValue(SHARED_KEY_USER);
        securePreferences.put(SHARED_KEY_USER, gson.toJson(user));
    }

    private void uploadUser (UserModel user){
        Hashtable<String, Object> params = passUserInfos(user);
        new AsyncOperation(context, AsyncOperation.TASK_ID_UPDATE_USER_INFO, OP_UPLOAD_USER, this).execute(params);
    }

    private void changePassword (String senha){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("senha", senha);
        new AsyncOperation(context, AsyncOperation.TASK_ID_UPDATE_USER_INFO, OP_CHANGE_PASSWORD, this).execute(params);
    }

    private void uploadPhoto (int cam, Uri photoUri, boolean isCam){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("sourceFile", photoUri);

        if ( isCam ) {
            String path = String.valueOf(params.get("sourceFile"));

            //fix parameter
            if (path.startsWith("file:")) {
                path = path.replace("file:", "");
            }

            File file = new File(path);

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(file.getAbsolutePath());
            } catch (IOException e) {
                Log.e("UserRepository", e.getMessage());
                e.printStackTrace();
            }

            assert exif != null;
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());

            Bitmap bmRotated = rotateBitmap(bitmap, orientation);

            OutputStream out = null;
            try {
                out = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (cam == 1) {
                bmRotated = flip(bmRotated, FLIP_VERTICAL);
            }

            bmRotated.compress(Bitmap.CompressFormat.JPEG, 100, out);

            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        new AsyncOperation(context, AsyncOperation.TASK_ID_UPLOAD_PICTURE, OP_UPLOAD_FOTO, this).execute(params);
    }

    private void getSocio (String cpf){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cpf", cpf);
        savedUser.setCpf(cpf);
        uploadUser(savedUser);
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_CONSULTAR_SOCIO, OP_GET_SOCIO, this).execute(params);
    }

    private void setSocio (SocioData data){
        String socioStatus = data.getStatus();
        boolean situacao = UTILS.checkSocioIsActive(socioStatus);
        boolean benecifio = UTILS.checkSocioBeneficioIsActive(socioStatus);
        UserModel userModel = savedUser;

        if(situacao){
            data.setIsActive(1);
            userModel.setSocioTorcedor(1);
        }else{
            data.setIsActive(0);
            userModel.setSocioTorcedor(0);
        }

        if(benecifio){
            userModel.setSocioBeneficio(1);
        }else{
            userModel.setSocioBeneficio(0);
        }

        userModel.setSocioStatus(socioStatus);
        uploadUser(userModel);

        if(data.getIsActive() == 1){
            Hashtable<String, Object> params = UTILS.SocioToHash(data);
            new AsyncOperation(context, AsyncOperation.TASK_ID_SET_SOCIO, OP_SET_SOCIO, this).execute(params);
        }
    }

    private void getSavedUser() {
        if(securePreferences.containsKey(SHARED_KEY_USER)){
            Gson gson = new Gson();
            String nUser = securePreferences.getString(SHARED_KEY_USER, "");
            savedUser = gson.fromJson(nUser, UserModel.class);
        }
    }

    //region Async Response
    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);

        switch (opId){
            case OP_GET_USER:
            case OP_POST_LOGIN:{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        UserModel data = gson.fromJson(response.getString("Object"), UserModel.class);
                        if(data != null){
                            asyncResponse.onResponseSucces(changeUserModelValues(data));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_UPLOAD_USER:{
                asyncResponse.onResponseSucces(changeUserModelValues(savedUser));
                asyncResponse.onResponseMessage("Usuário atualizado com sucesso!");
            }
            break;
            case OP_UPLOAD_FOTO:{
                if(response.has("img")){
                    try {
                        UserModel data = savedUser;
                        data.setImg(response.getString("img"));
                        asyncResponse.onResponseSucces(changeUserModelValues(data));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_POST_CREATE:{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        UserModel data = gson.fromJson(response.getString("Object"), UserModel.class);
                        if(data != null){
                            savedUser.setId(data.getId());
                            asyncResponse.onResponseSucces(changeUserModelValues(savedUser));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_FORGOT:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_GET_SOCIO: {
                if(response != null){
                    try {
                        if(response.has("msg")){
                            asyncResponse.onResponseMessage(response.getString("msg"));
                        }

                        if(response.has("dados")){
                            Gson gson = new Gson();
                            SocioData data = gson.fromJson(response.getString("dados"), SocioData.class);
                            setSocio(data);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            break;
            case OP_SET_SOCIO :{
                //uploadUser(savedUser);
            }
            break;
            case OP_CHANGE_PASSWORD:{
                asyncResponse.onResponseMessage("Senha atualizada");
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
        switch (opId){
            case OP_GET_USER:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    if(savedUser != null){
                        asyncResponse.onResponseSucces(savedUser);
                    }else{
                        asyncResponse.onResponseError("Erro ao buscar usuario");
                    }

                }
            }
            break;
            case OP_UPLOAD_USER:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("OP_UPLOAD_USER");
                }
            }
            break;

            case OP_UPLOAD_FOTO:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("OP_UPLOAD_FOTO");
                }
            }
            break;
            case OP_POST_LOGIN:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("OP_POST_LOGIN");
                }
            }
            break;
            case OP_POST_CREATE:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("OP_POST_CREATE");
                }
            }
            break;
            case OP_FORGOT:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("OP_FORGOT");
                }
            }
            break;
            case OP_GET_SOCIO:{
                if(response.has("msg")){
                    try {
                        asyncResponse.onResponseMessage(response.getString("msg"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("Erro ao verificar Sócio Torcedor");
                }
            }
            break;
        }
    }
    //endregion

    public interface AsyncResponse {
        void onResponseSucces (UserModel data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
