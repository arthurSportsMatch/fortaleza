package repository.utils;

import android.app.Activity;
import android.text.style.AlignmentSpan;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import data.PositonData;
import models.user.UserModel;
import models.utils.RegulamentoModel;
import repository.MainRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.SecurePreferences;
import vmodels.utils.RegulamentoViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class RegulamentoRepository extends MainRepository {


    private AsyncResponse asyncResponse;
    private Hashtable<String, String> regulamentos = new Hashtable<>();
    String type = "";

    private final int OP_GET_REGULAMENTO = 0;
    private final int OP_GET_DOCUMENTS = 1;

    public RegulamentoRepository(Activity context, SecurePreferences preferences) {
        this.context = context;
        this.securePreferences = preferences;
    }

    public void getRegulamento (int t, AsyncResponse response){
        this.asyncResponse = response;
        loadRegulamento();

        type = String.valueOf(t);
        if(!regulamentos.containsKey(type)){
            Hashtable<String, Object> params = new Hashtable<>();
            params.put("t", type);
            new AsyncOperation(context, AsyncOperation.TASK_ID_GET_REGULAMENTO, OP_GET_REGULAMENTO, this).execute(params);
        }else{
            RegulamentoModel model = new RegulamentoModel();
            model.setVersion(t);
            model.setDocumentacao(regulamentos.get(type));
            asyncResponse.onResponseSucces(model);
        }
    }

    public void getDocuments (AsyncResponse response){
        this.asyncResponse = response;
        getDocuments();
    }

    private void getDocuments (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_DOCS, OP_GET_DOCUMENTS, this).execute();
    }

    private void loadRegulamento (){
        if(securePreferences.containsKey(SHARED_KEY_REGULAMENTOS)){
            Gson gson = new Gson();
            String _regulamentos = securePreferences.getString(SHARED_KEY_REGULAMENTOS, "");
            regulamentos = gson.fromJson(_regulamentos, new TypeToken<Hashtable<String, String>>() {}.getType());
        }
    }

    private void saveRegulamento (){
        JSONObject list = new JSONObject(regulamentos);
        securePreferences.put(SHARED_KEY_REGULAMENTOS, list.toString());
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_GET_REGULAMENTO:{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        RegulamentoModel data = gson.fromJson(response.getString("Object"), RegulamentoModel.class);
                        if(data != null){
                            if(!regulamentos.containsKey(type)){
                                regulamentos.put(type, data.getDocumentacao());
                                saveRegulamento();
                            }
                            asyncResponse.onResponseSucces(data);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_GET_DOCUMENTS:{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONObject obj = response.getJSONObject("Object");
                        PositonData[] data = gson.fromJson(obj.getString("itens"), PositonData[].class);
                        if(data != null){
                            List<PositonData> docs = new ArrayList<>(Arrays.asList(data));
                            asyncResponse.onResponseSucces(docs);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        asyncResponse.onResponseError("Erro ao obter regulamentos");
    }

    public interface AsyncResponse {
        void onResponseSucces (RegulamentoModel data);
        void onResponseSucces (List<PositonData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
