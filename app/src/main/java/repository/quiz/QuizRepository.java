package repository.quiz;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import data.BadgeData;
import data.QuizData;
import repository.MainRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class QuizRepository extends MainRepository {

    public QuizRepository(Activity context) {
        this.context = context;
    }

    private AsyncResponse asyncResponse;
    private static final int OP_ID_QUIZ = 2;

    public void getQuizHome (AsyncResponse response){
        this.asyncResponse = response;
        getQuizHome();
    }

    private void getQuizHome (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_QUIZ_HOME, OP_ID_QUIZ, this).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_ID_QUIZ : {
                if(response.has("Object")){
                    try {
                        if(response.getJSONObject("Object").length() > 0){
                            Gson gson = new Gson();
                            QuizData data = gson.fromJson(response.getString("Object"), QuizData.class);

                            if(data != null){
                                asyncResponse.onResponseSucces(data);
                            }else{
                                asyncResponse.onResponseError("Sem quiz");
                            }
                        }else {
                            asyncResponse.onResponseError("Sem quiz");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    asyncResponse.onResponseError("Sem quiz");
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (QuizData data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
