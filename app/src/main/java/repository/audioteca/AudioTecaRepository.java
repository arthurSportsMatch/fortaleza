package repository.audioteca;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.PartidaData;
import data.PlaylistItemData;
import repository.MainRepository;
import repository.lastgames.LastGamesRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */

public class AudioTecaRepository extends MainRepository {
    private AsyncResponse asyncResponse;
    private AsyncResponseHome asyncResponseHome;
    private final int OP_GET_AUDIOS = 0;
    private final int OP_GET_AUDIOHOME = 1;

    public AudioTecaRepository() {}
    public AudioTecaRepository(Activity context) {
        this.context = context;
    }

    public void getAudios(AsyncResponse response){
        this.asyncResponse = response;
        getAudios();
    }

    public void getAudioHome(AsyncResponseHome response){
        this.asyncResponseHome = response;
        getAudioHome();
    }

    private void getAudios (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_PLAYLIST, OP_GET_AUDIOS, this).execute(params);

    }
    private void getAudioHome (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_AUDIO_HOME, OP_GET_AUDIOHOME, this).execute(params);

    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_GET_AUDIOS :{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONObject obj = response.getJSONObject("Object");
                        PlaylistItemData[] data = gson.fromJson(obj.getString("itens"), PlaylistItemData[].class);
                        List<PlaylistItemData> list = new ArrayList<>(Arrays.asList(data));

                        if(data.length > 0){
                            asyncResponse.onResponseSucces(list);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;

            case OP_GET_AUDIOHOME :{
                if(response.has("Object")) {
                    try {
                    Gson gson = new Gson();

                    PlaylistItemData data = gson.fromJson(response.getString("Object"), PlaylistItemData.class);
                    asyncResponseHome.onResponseSucces(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<PlaylistItemData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }

    public interface AsyncResponseHome {
        void onResponseSucces (PlaylistItemData data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
