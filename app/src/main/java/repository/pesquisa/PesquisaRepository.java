package repository.pesquisa;

import android.app.Activity;
import android.view.View;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.ProdutoFData;
import data.QuizData;
import repository.MainRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class PesquisaRepository extends MainRepository {

    AsyncResponse asyncResponse;
    private final int OP_GET_PPESQUISA_HOME      = 0;

    public PesquisaRepository(Activity context){
        this.context = context;
    }

    public void getPesquisaHome (AsyncResponse response){
        this.asyncResponse = response;
        getPesquisaHome();
    }

    private void getPesquisaHome (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_ULTIMAS_PESQUISAS, OP_GET_PPESQUISA_HOME, this).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_GET_PPESQUISA_HOME :{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONObject object = response.getJSONObject("Object");
                        QuizData[] data = gson.fromJson(object.getString("itens"), QuizData[].class);
                        if(data != null){
                            List<QuizData> banners = new ArrayList<>(Arrays.asList(data));
                            asyncResponse.onResponseSucces(banners);
                        }else{
                            asyncResponse.onResponseError("Erro ao obter pesquisas");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<QuizData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
