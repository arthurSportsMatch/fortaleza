package repository.noticia;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.BannerData;
import data.PartidaData;
import repository.MainRepository;
import repository.lastgames.LastGamesRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NoticiaRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private final int OP_GET_NOTICIA = 0;

    public NoticiaRepository() {}
    public NoticiaRepository(Activity context) {
        this.context = context;
    }

    public void getNoticias(int home, AsyncResponse response){
        this.asyncResponse = response;
        getNoticias(home);
    }

    private void getNoticias (int home){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("home", home);
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_NOTICIAS, OP_GET_NOTICIA, this).execute(params);
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        if(opId == OP_GET_NOTICIA){
            if(response.has("Object")){
                try {
                    Gson gson = new Gson();
                    JSONObject obj = response.getJSONObject("Object");
                    BannerData[] data = gson.fromJson(obj.getString("itens"), BannerData[].class);
                    if(data != null){
                        List<BannerData> banner = new ArrayList<>(Arrays.asList(data));
                        asyncResponse.onResponseSucces(banner);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<BannerData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
