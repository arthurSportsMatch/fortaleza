package repository.lojatime;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.InstagramData;
import data.ProdutoFData;
import repository.MainRepository;
import repository.stories.StoriesRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LojaTimeRepository extends MainRepository {

    AsyncResponse asyncResponse;
    private final int OP_GET_PRODUTOS_HOME      = 0;
    private final int OP_GET_PRODUTOS           = 1;

    public LojaTimeRepository (Activity context){
        this.context = context;
    }

    public void getProdutosHome (AsyncResponse response){
        this.asyncResponse = response;
        getProdutosHome();
    }

    private void getProdutosHome (){
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("home", 1);
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_PRODUTOS_LOJA_VIRTUAL, OP_GET_PRODUTOS_HOME, this).execute(params);
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_GET_PRODUTOS_HOME:{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONObject json = response.getJSONObject("Object");
                        ProdutoFData[] data = gson.fromJson(json.getString("itens"), ProdutoFData[].class);
                        if(data != null){
                            List<ProdutoFData> produtos = new ArrayList<>(Arrays.asList(data));
                            asyncResponse.onResponseSucces(produtos);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<ProdutoFData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
