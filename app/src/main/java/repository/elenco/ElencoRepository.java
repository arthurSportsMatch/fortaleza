package repository.elenco;

import android.app.Activity;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.BadgeData;
import data.ElencoData;
import data.ElencoPosData;
import models.elenco.ElencoModel;
import repository.MainRepository;
import repository.badges.BadgeRepository;
import utils.AsyncOperation;
import utils.SecurePreferences;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class ElencoRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private final int OP_GET_ELENCO = 0;

    public ElencoRepository() {}

    public ElencoRepository(Activity context) {
        this.context = context;
    }

    public void getElenco (AsyncResponse asyncResponse){
        this.asyncResponse = asyncResponse;
        getElenco();
    }

    private void getElenco (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_PLAYERS_TITULAR, OP_GET_ELENCO, this).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        if(opId == OP_GET_ELENCO){
            if(response.has("Object")){
                try {
                    Gson gson = new Gson();
                    JSONObject obj = response.getJSONObject("Object");
                    ElencoData[] data = gson.fromJson(obj.getString("itens"), ElencoData[].class);
                    ElencoPosData[] pos = gson.fromJson(obj.getString("pos"), ElencoPosData[].class);
                    if(data != null){
                        List<ElencoData> elencoData = new ArrayList<>(Arrays.asList(data));
                        if(pos != null){
                            List<ElencoPosData> elencoPosData = new ArrayList<>(Arrays.asList(pos));
                            asyncResponse.onResponseSucces(new ElencoModel(elencoPosData, elencoData));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (ElencoModel data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
