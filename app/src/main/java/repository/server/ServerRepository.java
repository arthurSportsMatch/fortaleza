package repository.server;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

import data.ServerCustomData;
import informations.UserInformation;
import models.user.UserModel;
import repository.MainRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.SecurePreferences;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.prefs;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class ServerRepository extends MainRepository {

    private AsyncResponse asyncResponse;
    private final int OP_GET_SERVER = 0;
    private final int OP_SET_SERVER_ERROR = 1;

    private Integer idUser;

    public ServerRepository() {
    }

    public ServerRepository(Activity context) {
        this.context = context;
    }

    public ServerRepository(Activity context, SecurePreferences preferences) {
        this.context = context;
        this.securePreferences = preferences;
    }

    public void getServer (@Nullable Integer idUser, AsyncResponse response){
        this.asyncResponse = response;
        this.idUser = idUser;

        if(securePreferences.containsKey(SHARED_KEY_SERVERS)){
            getSavedServer();
        }else{
            getServer();
        }
    }

    public void getServer (){
        Hashtable<String,Object> params = new Hashtable<>();

        if(idUser != null)
            params.put("idUser", idUser);

        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_SERVIDORES, OP_GET_SERVER, this).execute(params);
    }

    private void getSavedServer (){
       loadServer();
    }

    private void setServerError(){
        new AsyncOperation(context, AsyncOperation.TASK_ID_SET_TRACK_ERROR, OP_SET_SERVER_ERROR, this, TRACKING.TRACKING_GETSERVER_ERROR).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        if(opId == OP_GET_SERVER){
            if(response.has("Object")){
                try {
                    Gson gson = new Gson();
                    ServerCustomData server = gson.fromJson(response.getString("Object"), ServerCustomData.class);

                    if (checkServerData(server)) {
                        salvarServidor(server);
                        changeServerURL(server);
                        asyncResponse.onResponseSucces(server);
                    }else{
                        setServerError();
                        if (UserInformation.getServerData() != null) {
                            UserInformation.getServerData().setServerId(0);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    private boolean checkServerData (ServerCustomData server){
        try {
            if (server == null) {
                return false;
            } else if (server.getMedia() == null && server.getMedia().size() <= 0) {
                return false;
            } else if (server.getApi() == null && server.getApi().size() <= 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    private void salvarServidor (ServerCustomData server){
        Gson gson = new Gson();
        securePreferences.removeValue(SHARED_KEY_SERVERS);
        securePreferences.put(SHARED_KEY_SERVERS, gson.toJson(server));
    }

    private void loadServer (){
        Gson gson = new Gson();
        String serverString = securePreferences.getString(SHARED_KEY_SERVERS, "");
        ServerCustomData server = gson.fromJson(serverString, ServerCustomData.class);
        if(checkServerData(server)){
            Log.d("Async", "Servidores salvos!");
            changeServerURL(server);
            asyncResponse.onResponseSucces(server);
        }else{
            Log.d("Async", "Sem servidores salvos");
            getServer();
        }
    }

    private void changeServerURL (ServerCustomData server){
        UserInformation.setServerData(server);
        CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServerSelected() + "api/");
        CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
    }

    public interface AsyncResponse {
        void onResponseSucces (ServerCustomData data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
