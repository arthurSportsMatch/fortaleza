package repository.twitter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.List;

import data.PartidaData;
import data.TweetResultData;
import data.TwitterFeedInformation;
import models.twitter.TwitterModel;
import repository.MainRepository;
import repository.lastgames.LastGamesRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.SecurePreferences;
import utils.UTILS;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class TwitterRepository extends MainRepository {


    private AsyncResponse asyncResponse;
    private String bearerToken;
    private int quanty = 20;

    public TwitterRepository() {}
    public TwitterRepository(Activity context) {
        this.context = context;
    }
    public TwitterRepository(Activity context, SecurePreferences prefs) {
        this.context = context;
        this.securePreferences = prefs;
    }

    public TwitterRepository(Activity context, int quanty, SecurePreferences prefs) {
        this.context = context;
        this.quanty = quanty;
        this.securePreferences = prefs;
    }

    private static final int OP_ID_GET_TWITTER_TOKEN    = 1; //pega o token para requisição
    private static final int OP_ID_GET_TWITEER_QUERRY   = 2; //pega querry do nosso servidor
    private static final int OP_ID_GET_TWEETS           = 3; //pega os twitts


    public void getTweets (AsyncResponse response){
        this.asyncResponse = response;
        getTwitterToken();
    }
    private void getTwitterToken (){
        loadToken();
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_BEARER_TOKEN, OP_ID_GET_TWITTER_TOKEN, this).execute();
    }

    private void loadToken (){
        if(securePreferences.containsKey(SHARED_KEY_TWITTER_TOKEN)){
            bearerToken = securePreferences.getString(SHARED_KEY_TWITTER_TOKEN, "");
        }
    }

    private void saveToken (String token){
        securePreferences.removeValue(SHARED_KEY_TWITTER_TOKEN);
        securePreferences.put(SHARED_KEY_TWITTER_TOKEN, token);
    }

    private void getTwitterQuerry (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_HASHTAGS_TWITTER_QUERY_SUP, OP_ID_GET_TWITEER_QUERRY, this).execute();
    }

    private void getTweets (String hastags, int quant){
        String query = "";
        try {
            query = URLEncoder.encode(hastags, "utf-8");
        } catch (UnsupportedEncodingException e) {
            UTILS.DebugLog("TwitterError", e);
        }

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("query", query);
        params.put("count", quant);
        params.put("bearerToken", bearerToken);

        Log.d("Async", "Gething" + params);
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_TWEETS, OP_ID_GET_TWEETS, this).execute(params);

    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);

        switch (opId){
            case OP_ID_GET_TWITTER_TOKEN:{
                try {
                    if(response.has("Object")) {
                        JSONObject obj = response.getJSONObject("Object");
                        if ((obj.has("access_token")) && (obj.get("access_token") != JSONObject.NULL)) {
                            bearerToken = obj.getString("access_token");
                            saveToken(bearerToken);
                            getTwitterQuerry();
                        }
                    }
                } catch (JSONException e) {
                    TwitterFeedInformation.setBearerToken("");
                    UTILS.DebugLog("TwitterError", e);
                }
            }
            break;

            case OP_ID_GET_TWITEER_QUERRY:{
                if(status == 200){
                    try{
                        if(response.has("Object")){
                            JSONObject obj = response.getJSONObject("Object");
                            if (obj.has("Hashtags")){
                                getTweets(obj.getString("Hashtags"), quanty);
                            }
                        }
                    } catch (JSONException e) {
                        UTILS.DebugLog("TwitterError", e);
                    }
                }
            }
            break;

            case OP_ID_GET_TWEETS:{
                try {
                    if(response.has("Object")) {
                        JSONObject obj = response.getJSONObject("Object");
                        if ((obj.has("statuses")) && (obj.get("statuses") != JSONObject.NULL)) {
                            List<TweetResultData> tweets = TweetResultData.parseTweetResultDataList(obj.getJSONArray("statuses"));
                            asyncResponse.onResponseSucces(tweets);
                        }
                    }
                } catch (JSONException e) {
                    asyncResponse.onResponseError("Erro ao obter twets");
                    UTILS.DebugLog("TwitterError", e);
                }
            }
            break;
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
        asyncResponse.onResponseError("Erro ao obter twets");
    }

    public interface AsyncResponse {
        void onResponseSucces (List<TweetResultData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
