package repository.stories;

import android.app.Activity;
import android.view.View;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.InstagramData;
import data.PlaylistItemData;
import repository.MainRepository;
import repository.badges.BadgeRepository;
import utils.AsyncOperation;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class StoriesRepository extends MainRepository {

    AsyncResponse asyncResponse;
    private final int OP_GET_STORIES = 0;

    public StoriesRepository (Activity context){
        this.context = context;
    }

    public void getStories (AsyncResponse response){
        this.asyncResponse = response;
        getStories();
    }

    private void getStories (){
        new AsyncOperation(context, AsyncOperation.TASK_ID_GET_INSTAGRAM, OP_GET_STORIES, this).execute();
    }

    @Override
    public void onSucces(int opId, int status, JSONObject response) {
        super.onSucces(opId, status, response);
        switch (opId){
            case OP_GET_STORIES :{
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONObject obj = response.getJSONObject("Object");
                        InstagramData[] data = gson.fromJson(obj.getString("itens"), InstagramData[].class);
                        if(data != null && data.length > 0){
                            List<InstagramData> banners = new ArrayList<>(Arrays.asList(data));
                            asyncResponse.onResponseSucces(banners);
                        }else{
                            asyncResponse.onResponseError("Não tem stories");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        asyncResponse.onResponseError("Não tem stories");
                    }
                }else{
                    asyncResponse.onResponseError("Não tem stories");
                }
            }
        }
    }

    @Override
    public void onError(int opId, int status, JSONObject response) {
        super.onError(opId, status, response);
    }

    public interface AsyncResponse {
        void onResponseSucces (List<InstagramData> data);
        void onResponseMessage (String message);
        void onResponseError (String message);
    }
}
