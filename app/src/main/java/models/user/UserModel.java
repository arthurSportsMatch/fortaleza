package models.user;

import android.util.Log;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class UserModel{

    private int id              = -1;
    private String nome         = "";
    private String email        = "";
    private String celular      = "";
    private String cpf          = "";
    private String img          = "";
    private String sexo         = "";
    private String dn           = "";
    private String senha        = "";

    private int notifications   = 0;
    private int moedas          = 0;

    private int socioTorcedor   = -1;
    private int socioBeneficio  = -1;
    private String socioStatus  = "";


    private int checkin         = -1;
    private double rv           = -1.0d; //ultimo regulamento

    private int badges          = -1;
    private String token        = "";
    private String gcm          = "";

    public UserModel() {}

    public UserModel(String nome, String email) {
        this.nome = nome;
        this.email = email;
    }

    public void secureParce (UserModel nUser){
        Log.d("SecureParce", nUser.toString());

        //ID
        if(nUser.getId() > 0 && nUser.getId() != getId()){
            setId(nUser.getId());
        }

        //Nome
        if(nUser.getNome() != null && !nUser.getNome().equals(getNome())){
            setNome(nUser.getNome());
        }

        //Email
        if(nUser.getEmail() != null && !nUser.getEmail().equals(getEmail())){
            setEmail(nUser.getEmail());
        }

        //Moedas
        if(nUser.getMoedas() > 0 && nUser.getMoedas() != getMoedas()){
            setMoedas(nUser.getMoedas());
        }

        //Celular
        if(nUser.getCelular() != null && !nUser.getCelular().equals(getCelular())){
            setCelular(nUser.getCelular());
        }

        //Cpf
        if(nUser.getCpf() != null && !nUser.getCpf().equals(getCpf())){
            setCpf(nUser.getCpf());
        }

        //Imagem
        if(nUser.getImg() != null && !nUser.getImg().equals(getImg())){
            setImg(nUser.getImg());
        }

        //Sexo
        if(nUser.getSexo() != null && !nUser.getSexo().equals(getSexo())){
            setSexo(nUser.getSexo());
        }

        //Data de nascimento
        if(nUser.getDn() != null && !nUser.getDn().equals(getDn())){
            setDn(nUser.getDn());
        }

        //Data de nascimento
        if(nUser.getSenha() != null && !nUser.getSenha().equals(getSenha())){
            setSenha(nUser.getSenha());
        }

        //Moedas
        if(nUser.getMoedas() != getMoedas()){
            setMoedas(nUser.getMoedas());
        }

        //Notificações
        if(nUser.getNotifications() != getNotifications()){
            setNotifications(nUser.getNotifications());
        }

        //Socio torcedor
        if(nUser.getSocioTorcedor() >= 0){
            if(nUser.getSocioTorcedor() != getSocioTorcedor()){
                setSocioTorcedor(nUser.getSocioTorcedor());
            }
        }


        //Socio beneficio
        if(nUser.getSocioBeneficio() >= 0){
            if(nUser.getSocioBeneficio() != getSocioBeneficio()){
                setSocioBeneficio(nUser.getSocioBeneficio());
            }
        }

        //Socio status
        if(nUser.getSocioStatus() != null && !nUser.getSocioStatus().isEmpty()){
            if(!nUser.getSocioStatus().equals(getSocioStatus())){
                setSocioStatus(nUser.getSocioStatus());
            }
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getMoedas() {
        return moedas;
    }

    public void setMoedas(int moedas) {
        this.moedas = moedas;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String image) {
        if (image.contains("http:")) {
            image = image.replace("http:", "https:");
        }
        this.img = image;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public int getSocioTorcedor() {
        return socioTorcedor;
    }

    public void setSocioTorcedor(int socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
    }

    public boolean isSocioTorcedor (){
        if(getSocioTorcedor() == 1){
            return true;
        }else{
            return false;
        }
    }

    public int getSocioBeneficio() {
        return socioBeneficio;
    }

    public void setSocioBeneficio(int socioBeneficio) {
        this.socioBeneficio = socioBeneficio;
    }

    public String getSocioStatus() {
        return socioStatus;
    }

    public void setSocioStatus(String socioStatus) {
        this.socioStatus = socioStatus;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getNotifications() {
        return notifications;
    }

    public void setNotifications(int notifications) {
        this.notifications = notifications;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", celular='" + celular + '\'' +
                ", cpf='" + cpf + '\'' +
                ", img='" + img + '\'' +
                ", sexo='" + sexo + '\'' +
                ", dn='" + dn + '\'' +
                ", senha='" + senha + '\'' +
                ", notifications=" + notifications +
                ", moedas=" + moedas +
                ", socioTorcedor=" + socioTorcedor +
                ", socioBeneficio=" + socioBeneficio +
                ", socioStatus='" + socioStatus + '\'' +
                ", checkin=" + checkin +
                '}';
    }
}
