package models.elenco;

import java.util.List;

import data.ElencoData;
import data.ElencoPosData;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class ElencoModel {
    private List<ElencoPosData> elencoPosDataList;
    private List<ElencoData> elencoData;

    public ElencoModel(List<ElencoPosData> elencoPosDataList, List<ElencoData> elencoData) {
        this.elencoPosDataList = elencoPosDataList;
        this.elencoData = elencoData;
    }

    public List<ElencoData> getElencoData() {
        return elencoData;
    }

    public void setElencoData(List<ElencoData> elencoData) {
        this.elencoData = elencoData;
    }

    public List<ElencoPosData> getElencoPosDataList() {
        return elencoPosDataList;
    }

    public void setElencoPosDataList(List<ElencoPosData> elencoPosDataList) {
        this.elencoPosDataList = elencoPosDataList;
    }
}
