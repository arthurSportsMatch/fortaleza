package models.nextgames;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.PartidaData;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NextGameModel {

    private int hide = 0;
    private List<PartidaData> partidaData;

    public NextGameModel(int hide, List<PartidaData> partidaData) {
        this.hide = hide;
        this.partidaData = partidaData;
    }

    public int getHide() {
        return hide;
    }

    public void setHide(int hide) {
        this.hide = hide;
    }

    public List<PartidaData> getPartidaData() {
        return partidaData;
    }

    public void setPartidaData(List<PartidaData> partidaData) {
        this.partidaData = partidaData;
    }
}
