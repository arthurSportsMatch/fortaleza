package viewpresenter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.math.MathUtils;
import com.sportm.fortaleza.R;

import java.util.ArrayList;
import java.util.List;

import data.EscalaAtleta;
import data.EscalacaoData;
import fragments.CampoFragment;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class CampoPresenter extends MainViewPresenter{

    RelativeLayout relativeLayout;
    int weight, height, esquema = 1;
    List<Seletor> seletores = new ArrayList<>();
    OnClickListener onClickListener;

    public CampoPresenter(Activity context, int viewID, ViewGroup viewHolder, int weight, int height) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.weight = weight;
        this.height = height;
        initView(viewID, viewHolder);
    }

    public void setClickListenet (OnClickListener listener)
    {
        this.onClickListener = listener;
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        relativeLayout = view.findViewById(R.id.relative_campo);
        createSelectors(11);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    public void passSize (int weight, int height){
        this.weight = weight;
        this.height = height;
        changeEsquema(esquema);
    }

    //Cria os seletores
    public void createSelectors (int number){
        relativeLayout.removeAllViews();
        if(seletores.size() < number){
            for(int i = 0; i < (number); i++){
                View view = LayoutInflater.from(context).inflate(R.layout.include_campo_selector, null, false);
                View b = view.findViewById(R.id.btn_selector);
                ImageView image = view.findViewById(R.id.img_atleta);

                final int id = i;
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(onClickListener != null){
                            onClickListener.OnClick(id);
                            //changeSelector(id);
                        }
                    }
                });

                view.setAlpha(0);
                relativeLayout.addView(view);
                seletores.add(new Seletor((i + 1), view, new Vector2(0,0)));
            }
        }
    }

    public void changeEsquema (String esquema){
        changeSelectorPositions(getCampoMethod(esquema));
    }

    public void changeEsquema (int esquema){
        this.esquema = esquema;
        changeSelectorPositions(getCampoMethod(esquema));
    }

    public void changeEsquema (EscalacaoData data){
        this.esquema = data.getEsquema();
        changeImageOptions(data);
        changeSelectorPositions(getCampoMethod(data.getEsquema()));
    }

    void changeImageOptions (EscalacaoData data){
        this.esquema = data.getEsquema();
        if(seletores == null || seletores.size()<= 0){
            createSelectors(11);
        }


        if(seletores.size() > 0){
            for(int i = 0; i < data.getPlayers().size(); i ++){
                Seletor seletor = seletores.get(i);

                View view = seletor.view;
                Button btn = view.findViewById(R.id.btn_selector);
                TextView text = view.findViewById(R.id.txt_selector);
                text.setVisibility(View.VISIBLE);

                EscalaAtleta atleta = data.getPlayers().get(i);

//                ImageView image = view.findViewById(R.id.img_atleta);
//                if(data.getPlayers().get(i).getImg() != null){
//                    UTILS.getImageAt(context, data.getPlayers().get(i).getImg(), image, new RequestOptions()
//                            .format(DecodeFormat.PREFER_ARGB_8888));
//                }
//                text.setText(atleta.getPerc() + "%");
//                String name = atleta.getName();
//                name = name.replace(" ", "\n");
//                text.setText(name);
                btn.setText(atleta.getPerc() + "%");
                String name = atleta.getName();
                name = name.replace(" ", "\n");
                text.setText(name);

                view.measure(0, 0);
                int xx = (view.getMeasuredWidth() / 2);
                int yy = (view.getMeasuredHeight() / 2);

                seletor.view.setX(seletor.pos.x - xx);
                seletor.view.setY(seletor.pos.y - yy);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //null
                    }
                });
            }
        }
    }

    //Muda as posições dos seletores
    void changeSelectorPositions (CampoInterface constructor){
        for(int i = 0; i < (seletores.size()); i++){
            Vector2 pos = constructor.GetPos(seletores.get(i).id);

            float xx = getMargin(weight, pos.x);
            float yy = getMargin(height, 0.95f - pos.y);
            seletores.get(i).setPos(new Vector2( xx, yy));

            putButton(seletores.get(i));
        }
    }

    //Pegar a margen correta (tamanho vertical ou horizontal, porcentagem que sera colocado)
    int getMargin (int size, float porc) {
        int valor = (int)(size * porc);
        return valor;
    }

    //Criar o botão
    void putButton (Seletor seletor){
        seletor.view.setAlpha(1);
        int xx = (seletor.view.getMeasuredWidth() / 2);
        int yy = (seletor.view.getMeasuredHeight() / 2);
        float lerpX = MathUtils.lerp(seletor.view.getX(), seletor.pos.x - xx, 1);
        float lerpY = MathUtils.lerp(seletor.view.getY(), seletor.pos.y - yy, 1);
        seletor.view.setX(lerpX);
        seletor.view.setY(lerpY);
    }

    public void changeButton (int id, String img){
        Log.d("Escalar", img);
        new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_ADICIONAR, id).execute();
        Seletor seletor = seletores.get(id);
        View view = seletor.view;
        View btn = view.findViewById(R.id.btn_selector);
        TextView text = view.findViewById(R.id.txt_selector);
        //TextView plus = view.findViewById(R.id.txt_plus);

        ImageView image = view.findViewById(R.id.img_atleta);
        UTILS.getImageAt(context, img, image, new RequestOptions()
                .format(DecodeFormat.PREFER_ARGB_8888));

        //btn.setText("-");
        btn.setAlpha(0.7f);
        //plus.setVisibility(View.GONE);
        view.measure(0, 0);
        int xx = (view.getMeasuredWidth() / 2);
        int yy = (view.getMeasuredHeight() / 2);

        seletor.view.setX(seletor.pos.x - xx);
        seletor.view.setY(seletor.pos.y - yy);
    }
    public void changeButtonNOME (int id, String name){
        //Log.d("Escalar", name);
        new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_ADICIONAR, id).execute();
        Seletor seletor = seletores.get(id);
        View view = seletor.view;
        View btn = view.findViewById(R.id.btn_selector);
        TextView text = view.findViewById(R.id.txt_selector);
        //TextView plus = view.findViewById(R.id.txt_plus);

        //        ImageView image = view.findViewById(R.id.img_atleta);
//        UTILS.getImageAt(context, R.drawable.adidas, image);

        //plus.setText("-");
        btn.setAlpha(0.7f);
        String nome = name;
        nome = nome.replace(" ", "\n");
        text.setText(nome);
        text.setVisibility(View.VISIBLE);

        view.measure(0, 0);
        int xx = (view.getMeasuredWidth() / 2);
        int yy = (view.getMeasuredHeight() / 2);

        seletor.view.setX(seletor.pos.x - xx);
        seletor.view.setY(seletor.pos.y - yy);
    }
    public void changeButtonImgNome (int id, String img, String name){
        //Log.d("Escalar", name);
        new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_ADICIONAR, id).execute();
        Seletor seletor = seletores.get(id);
        View view = seletor.view;
        View btn = view.findViewById(R.id.btn_selector);
        TextView text = view.findViewById(R.id.txt_selector);
        //TextView plus = view.findViewById(R.id.txt_plus);

        ImageView image = view.findViewById(R.id.img_atleta);
        UTILS.getImageAt(context, img, image);

       // plus.setText("-");
        btn.setAlpha(1f);
        String nome = name;
        nome = nome.replace(" ", "\n");
        text.setText(nome);
        text.setVisibility(View.VISIBLE);

        view.measure(0, 0);
        int xx = (view.getMeasuredWidth() / 2);
        int yy = (view.getMeasuredHeight() / 2 + 10);

        seletor.view.setX(seletor.pos.x - xx);
        seletor.view.setY(seletor.pos.y - yy);
    }

    public void removeAtleta (int id){
        new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_REMOVER, id).execute();
        Seletor seletor = seletores.get(id);
        View view = seletor.view;
        ImageView image = view.findViewById(R.id.img_atleta);
        image.setImageBitmap(null);

//        btn.setText("+");
//        btn.setAlpha(1f);
//        text.setText("Selecionar");
        view.measure(0, 0);
        int xx = (view.getMeasuredWidth() / 2);
        int yy = (view.getMeasuredHeight() / 2);

        seletor.view.setX(seletor.pos.x - xx);
        seletor.view.setY(seletor.pos.y - yy);
    }

    //Pega qual tipo de metodo de posição
    CampoInterface getCampoMethod (String type){
        switch (type){
            case "343" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get343(id);
                }
            };

            case "352" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get352(id);
                }
            };

            case "4131" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4132(id);
                }
            };

            case "41212" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get41212(id);
                }
            };

            case "4141" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4141(id);
                }
            };

            case "4222" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4222(id);
                }
            };

            case "4231" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4231(id);
                }
            };

            case "424" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get424(id);
                }
            };

            case "433" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get433(id);
                }
            };

            case "433falso9" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get433falso9(id);
                }
            };

            case "4411" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4411(id);
                }
            };

            case "442" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get442(id);
                }
            };

            case "451" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get451(id);
                }
            };

            case "5212" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get5212(id);
                }
            };

            case "523" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get523(id);
                }
            };

            case "532" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get532(id);
                }
            };

            case "541" : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get541(id);
                }
            };

        }

        return null;
    }
    //Pega qual tipo de metodo de posição
    CampoInterface getCampoMethod (int type){
        switch (type){
            case 1 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get343(id);
                }
            };

            case 2 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get352(id);
                }
            };

            case 3 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4132(id);
                }
            };

            case 4 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get41212(id);
                }
            };

            case 5 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4141(id);
                }
            };

            case 6 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4222(id);
                }
            };

            case 7 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4231(id);
                }
            };

            case 8 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get424(id);
                }
            };

            case 9 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get433(id);
                }
            };

            case 10 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get433falso9(id);
                }
            };

            case 11 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get4411(id);
                }
            };

            case 12 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get442(id);
                }
            };

            case 13 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get451(id);
                }
            };

            case 14 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get5212(id);
                }
            };

            case 15 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get523(id);
                }
            };

            case 16 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get532(id);
                }
            };

            case 17 : return new CampoInterface() {
                @Override
                public Vector2 GetPos(int id) {
                    return get541(id);
                }
            };

        }

        return null;
    }

    public interface OnClickListener {
        void OnClick(int id);
    }

    public interface CampoInterface {
        Vector2 GetPos(int id);
    }

    //Seletor
    class Seletor {
        public int id;
        public int atletaId;
        public View view;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public View getView() {
            return view;
        }

        public void setView(View view) {
            this.view = view;
        }

        public Vector2 getPos() {
            return pos;
        }

        public void setPos(Vector2 pos) {
            this.pos = pos;
        }

        public Vector2 pos;

        public int getAtletaId() {
            return atletaId;
        }

        public void setAtletaId(int atletaId) {
            this.atletaId = atletaId;
        }

        public Seletor(int id, View view, Vector2 pos) {
            this.id = id;
            this.view = view;
            this.pos = pos;
        }
    }

    class Vector2 {
        public float x;
        public float y;

        public Vector2(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }

    //region Position Values
    Vector2 get343 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.2f,0.25f);
            case 3 : return new Vector2(0.5f,0.25f);
            case 4 : return new Vector2(0.8f,0.25f);
            case 5 : return new Vector2(0.11f,0.5f);
            case 6 : return new Vector2(0.35f,0.5f);
            case 7 : return new Vector2(0.65f,0.5f);
            case 8 : return new Vector2(0.89f,0.5f);
            case 9 : return new Vector2(0.2f,0.75f);
            case 10 : return new Vector2(0.5f,0.8f);
            case 11 : return new Vector2(0.8f,0.75f);
        }
        return null;
    }

    Vector2 get352 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.2f,0.25f);
            case 3 : return new Vector2(0.5f,0.25f);
            case 4 : return new Vector2(0.8f,0.25f);
            case 5 : return new Vector2(0.11f,0.5f);
            case 6 : return new Vector2(0.35f,0.5f);
            case 7 : return new Vector2(0.65f,0.5f);
            case 8 : return new Vector2(0.89f,0.5f);
            case 9 : return new Vector2(0.32f,0.82f);
            case 10 : return new Vector2(0.5f,0.63f);
            case 11 : return new Vector2(0.68f,0.82f);
        }
        return null;
    }

    Vector2 get4132 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.5f,0.43f);
            case 7 : return new Vector2(0.2f,0.62f);
            case 8 : return new Vector2(0.5f,0.62f);
            case 9 : return new Vector2(0.8f,0.62f);
            case 10 : return new Vector2(0.32f,0.82f);
            case 11 : return new Vector2(0.68f,0.82f);
        }
        return null;
    }

    Vector2 get41212 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.5f,0.41f);
            case 7 : return new Vector2(0.25f,0.53f);
            case 8 : return new Vector2(0.5f,0.65f);
            case 9 : return new Vector2(0.75f,0.53f);
            case 10 : return new Vector2(0.32f,0.82f);
            case 11 : return new Vector2(0.68f,0.82f);
        }
        return null;
    }

    Vector2 get4141 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.5f,0.41f);
            case 7 : return new Vector2(0.11f,0.6f);
            case 8 : return new Vector2(0.35f,0.6f);
            case 9 : return new Vector2(0.65f,0.6f);
            case 10 : return new Vector2(0.89f,0.6f);
            case 11 : return new Vector2(0.5f,0.8f);
        }
        return null;
    }

    Vector2 get4222 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.33f,0.42f);
            case 7 : return new Vector2(0.67f,0.42f);
            case 8 : return new Vector2(0.22f,0.63f);
            case 9 : return new Vector2(0.78f,0.63f);
            case 10 : return new Vector2(0.33f,0.82f);
            case 11 : return new Vector2(0.67f,0.82f);
        }
        return null;
    }

    Vector2 get4231 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.33f,0.42f);
            case 7 : return new Vector2(0.67f,0.42f);
            case 8 : return new Vector2(0.22f,0.63f);
            case 9 : return new Vector2(0.5f,0.63f);
            case 10 : return new Vector2(0.78f,0.63f);
            case 11 : return new Vector2(0.5f,0.82f);
        }
        return null;
    }

    Vector2 get424 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.33f,0.5f);
            case 7 : return new Vector2(0.67f,0.5f);
            case 8 : return new Vector2(0.11f,0.78f);
            case 9 : return new Vector2(0.35f,0.82f);
            case 10 : return new Vector2(0.65f,0.82f);
            case 11 : return new Vector2(0.89f,0.78f);
        }
        return null;
    }

    Vector2 get433 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.31f,0.52f);
            case 7 : return new Vector2(0.5f,0.45f);
            case 8 : return new Vector2(0.69f,0.52f);
            case 9 : return new Vector2(0.2f,0.75f);
            case 10 : return new Vector2(0.5f,0.8f);
            case 11 : return new Vector2(0.8f,0.75f);
        }
        return null;
    }

    Vector2 get433falso9 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.31f,0.52f);
            case 7 : return new Vector2(0.5f,0.45f);
            case 8 : return new Vector2(0.69f,0.52f);
            case 9 : return new Vector2(0.2f,0.75f);
            case 10 : return new Vector2(0.5f,0.71f);
            case 11 : return new Vector2(0.8f,0.75f);
        }
        return null;
    }

    Vector2 get4411 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.11f,0.5f);
            case 7 : return new Vector2(0.35f,0.5f);
            case 8 : return new Vector2(0.65f,0.5f);
            case 9 : return new Vector2(0.89f,0.5f);
            case 10 : return new Vector2(0.5f,0.62f);
            case 11 : return new Vector2(0.5f,0.8f);
        }
        return null;
    }

    Vector2 get442 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.11f,0.5f);
            case 7 : return new Vector2(0.35f,0.5f);
            case 8 : return new Vector2(0.65f,0.5f);
            case 9 : return new Vector2(0.89f,0.5f);
            case 10 : return new Vector2(0.32f,0.78f);
            case 11 : return new Vector2(0.68f,0.78f);
        }
        return null;
    }

    Vector2 get451 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.35f,0.22f);
            case 4 : return new Vector2(0.65f,0.22f);
            case 5 : return new Vector2(0.89f,0.25f);
            case 6 : return new Vector2(0.11f,0.5f);
            case 7 : return new Vector2(0.33f,0.54f);
            case 8 : return new Vector2(0.5f,0.45f);
            case 9 : return new Vector2(0.67f,0.54f);
            case 10 : return new Vector2(0.89f,0.5f);
            case 11 : return new Vector2(0.5f,0.78f);
        }
        return null;
    }

    Vector2 get5212 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.32f,0.22f);
            case 4 : return new Vector2(0.5f,0.18f);
            case 5 : return new Vector2(0.68f,0.22f);
            case 6 : return new Vector2(0.89f,0.25f);
            case 7 : return new Vector2(0.33f,0.44f);
            case 8 : return new Vector2(0.67f,0.44f);
            case 9 : return new Vector2(0.5f,0.62f);
            case 10 : return new Vector2(0.32f,0.78f);
            case 11 : return new Vector2(0.68f,0.78f);
        }
        return null;
    }

    Vector2 get523 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.32f,0.22f);
            case 4 : return new Vector2(0.5f,0.18f);
            case 5 : return new Vector2(0.68f,0.22f);
            case 6 : return new Vector2(0.89f,0.25f);
            case 7 : return new Vector2(0.33f,0.44f);
            case 8 : return new Vector2(0.67f,0.44f);
            case 9 : return new Vector2(0.2f,0.73f);
            case 10 : return new Vector2(0.5f,0.78f);
            case 11 : return new Vector2(0.8f,0.73f);
        }
        return null;
    }

    Vector2 get532 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.32f,0.22f);
            case 4 : return new Vector2(0.5f,0.18f);
            case 5 : return new Vector2(0.68f,0.22f);
            case 6 : return new Vector2(0.89f,0.25f);
            case 7 : return new Vector2(0.2f,0.5f);
            case 8 : return new Vector2(0.5f,0.48f);
            case 9 : return new Vector2(0.8f,0.5f);
            case 10 : return new Vector2(0.33f,0.78f);
            case 11 : return new Vector2(0.67f,0.78f);
        }
        return null;
    }

    Vector2 get541 (int pos){
        switch (pos){
            case 1 : return new Vector2(0.5f,0.06f);
            case 2 : return new Vector2(0.11f,0.25f);
            case 3 : return new Vector2(0.32f,0.22f);
            case 4 : return new Vector2(0.5f,0.18f);
            case 5 : return new Vector2(0.68f,0.22f);
            case 6 : return new Vector2(0.89f,0.25f);
            case 7 : return new Vector2(0.11f,0.5f);
            case 8 : return new Vector2(0.35f,0.5f);
            case 9 : return new Vector2(0.65f,0.5f);
            case 10 : return new Vector2(0.89f,0.5f);
            case 11 : return new Vector2(0.5f,0.78f);
        }
        return null;
    }
    //endregion
}
