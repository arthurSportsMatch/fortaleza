package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sportm.fortaleza.CentralNoticiasActivity;
import com.sportm.fortaleza.NoticiaActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.BannerData;
import pager.BannerAdapter;
import utils.AsyncOperation;
import utils.TRACKING;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NoticiaPresenter extends MainViewPresenter{

    ViewHolder mHolder;
    List<BannerData> dataModel;

    public NoticiaPresenter(Activity context, int ViewID , ViewGroup viewHolder, List<BannerData> dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<BannerData> data){
        DisplayMetrics dimension = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int weight = dimension.widthPixels;//mHolder.bannerNews.getWidth();
        int height = (weight / 100) * 60;
        mHolder.bannerNews.getLayoutParams().height = height;
        mHolder.bannerNews.requestLayout();

        BannerAdapter bannerAdapter = new BannerAdapter(context, data, new BannerAdapter.Listener() {
            @Override
            public void Onclick(final BannerData data) {

                if(data.isHasLast()){
                    Intent intent = new Intent(context, CentralNoticiasActivity.class);
                    context.startActivity(intent);
                }else {
                    if(data.getUrl() != null){
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(data.getUrl()));
                        context.startActivity(i);
                    }else{
                        Intent intent = new Intent(context, NoticiaActivity.class);
                        intent.putExtra("id", data.getId());
                        context.startActivity(intent);
                        //AdMobController.starIntetentBeforeAd(activity, intent, AdMobController.HOME_INTERSECTIAL_NOTICIA_ID);
                    }
                    new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_NOTICIAOPEN, data.getId()).execute();
                }
            }
        });
        mHolder.bannerNews.setAdapter(bannerAdapter);
        mHolder.tabBanner.setupWithViewPager(mHolder.bannerNews, true);

        mHolder.seeAllNoticias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_DESTAQUES_VERTODOS).execute();
                Intent intent = new Intent(context, CentralNoticiasActivity.class);
                context.startActivity(intent);
            }
        });
    }

    class ViewHolder {
        TabLayout tabBanner;
        ViewPager bannerNews;
        View seeAllNoticias;

        public ViewHolder (View view){
            tabBanner = view.findViewById(R.id.tab_banner);
            bannerNews = view.findViewById(R.id.pager_banner_imgs);
            seeAllNoticias = view.findViewById(R.id.txt_ver_todos_noticia);
        }
    }
}
