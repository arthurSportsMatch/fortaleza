package viewpresenter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class MainViewPresenter {

    Activity context;
    ViewGroup viewHolder;

    protected View initView (int viewID, ViewGroup parentView){
        return LayoutInflater.from(context).inflate(viewID, parentView, false);
    }

}
