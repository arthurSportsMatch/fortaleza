package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sportm.fortaleza.CheckInLoginActivity;
import com.sportm.fortaleza.CheckInMessageActivity;
import com.sportm.fortaleza.CheckinActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.BannerData;
import data.PartidaData;
import fragments.AgendaFragment;
import informations.UserInformation;
import pager.NextGamePageAdapter;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.fragment.FragmentHolderViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;
import static com.sportm.fortaleza.MainActivity.prefs;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class NextGamesPresenter extends MainViewPresenter {

    ViewHolder mHolder;
    List<PartidaData> dataModel;
    Integer hide;
    FragmentHolderViewModel mfragmentViewModel;

    public NextGamesPresenter(Activity context, int ViewID , FragmentHolderViewModel mfragmentViewModel, ViewGroup viewHolder, List<PartidaData> dataModel, Integer hide) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        this.mfragmentViewModel = mfragmentViewModel;
        this.hide = hide;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel, hide);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<PartidaData> data, Integer hide){
        boolean mHide = false;
        if(hide != null && hide == 1){
            mHide = true;
        }

        NextGamePageAdapter nextGameAdapter = new NextGamePageAdapter(context, data, R.layout.adapter_agenda_jogos, mHide, new NextGamePageAdapter.Listener() {
            @Override
            public void OnPurchase(PartidaData data) {
                String url = data.getLinkCompra();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                context.startActivity(i);
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_PROXIMOSJOGOS_COMPRAR, data.getId()).execute();
            }

            @Override
            public void OnCheckIn() {
                goSocioTorcedor();
            }
        });

        mHolder.nextGamesTab.setupWithViewPager(mHolder.nextGamesPager);
        mHolder.nextGamesPager.setAdapter(nextGameAdapter);


        if(data!= null) {
            mHolder.nextGamesTab.getTabAt(0).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(0).getDataHora()).replace("/2020", ""));
        }
        if(data.size() >= 2) {
            mHolder.nextGamesTab.getTabAt(1).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(1).getDataHora()).replace("/2020", ""));
        }

        if(data.size() >= 3) {
            mHolder.nextGamesTab.getTabAt(2).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(2).getDataHora()).replace("/2020", ""));
        }

        if(data.size() >= 4) {
            mHolder.nextGamesTab.getTabAt(3).setText( mHide? "--/--" : UTILS.formatDateFromDB(data.get(3).getDataHora()).replace("/2020", ""));
        }

        mHolder.agendaCompleta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfragmentViewModel.changeFragment(new AgendaFragment(), "AgendaFragment");
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_PROXIMOSJOGOS_AGENDA).execute();
                prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_SERVER);
            }
        });
    }

    void goSocioTorcedor (){
        int socio = UserInformation.getUserData().getSocioTorcedor();
        int beneficio = UserInformation.getUserData().getSocioBeneficio();

        if(socio == 1){
            if(beneficio == 1){
                Intent intent = new Intent(context, CheckinActivity.class);
                context.startActivity(intent);
            }else{
                Intent intent = new Intent(context, CheckInMessageActivity.class);
                intent.putExtra("tit", context.getResources().getString(R.string.funcionalidade_exclusiva_n_para_s_cio_torcedor));
                intent.putExtra("msg", context.getResources().getString(R.string.socio_sem_beneficio));
                context.startActivity(intent);
            }

        }else{
            Intent intent = new Intent(context, CheckInLoginActivity.class);
            context.startActivity(intent);
        }
    }

    class ViewHolder {

        TabLayout nextGamesTab;
        ViewPager nextGamesPager;
        View agendaCompleta;

        public ViewHolder (View view){
            nextGamesTab = view.findViewById(R.id.tab_next_games);
            nextGamesPager = view.findViewById(R.id.pager_next_games);
            agendaCompleta = view.findViewById(R.id.btn_agenda_completa);
        }
    }
}
