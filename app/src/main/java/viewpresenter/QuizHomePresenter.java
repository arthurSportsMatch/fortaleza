package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sportm.fortaleza.QuizActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.QuizData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;
import vmodels.fragment.FragmentHolderViewModel;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class QuizHomePresenter extends MainViewPresenter{

    ViewHolder mHolder;
    QuizData dataModel;

    public QuizHomePresenter(Activity context, int ViewID, ViewGroup viewHolder, QuizData dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    public QuizHomePresenter(Activity context, int ViewID, ViewGroup viewHolder) {
        this.context = context;
        this.viewHolder = viewHolder;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        if(dataModel != null){
            initValues(dataModel);
        }else{
            initNoQuiz();
        }
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (final QuizData data){
        TextView txtTitle = mHolder.quiz.findViewById(R.id.txt_nome_quiz);
        TextView txtDescription = mHolder.quiz.findViewById(R.id.txt_description_quiz);
        TextView txtCategoria = mHolder.quiz.findViewById(R.id.txt_categoria_quiz);
        LinearLayout action = mHolder.quiz.findViewById(R.id.view_action_quiz);
        Button button = mHolder.quiz.findViewById(R.id.btn_quiz_play);
        ImageView view = mHolder.quiz.findViewById(R.id.img_background);

        UTILS.getImageAt(context, data.getImg(), view);

        txtCategoria.setVisibility(View.VISIBLE);

        txtTitle.setText(data.getTitulo());
        txtDescription.setText(data.getDescricao());
        String quantidade = data.getCorretas()+ "/" + data.getQuantidade().split("/")[0];
        txtCategoria.setText(quantidade);

        action.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openQuiz(data.getId(), data.getTitulo(), 0, data.getImg(), data.getQuantidade());
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_QUIZ_PREVIEW, data.getId()).execute();
            }
        });
    }

    void initNoQuiz (){
        TextView txtTitle = mHolder.quiz.findViewById(R.id.txt_nome_quiz);
        TextView txtDescription = mHolder.quiz.findViewById(R.id.txt_description_quiz);
        TextView txtCategoria = mHolder.quiz.findViewById(R.id.txt_categoria_quiz);

        LinearLayout action = mHolder.quiz.findViewById(R.id.view_action_quiz);
        Button button = mHolder.quiz.findViewById(R.id.btn_quiz_play);

        txtCategoria.setVisibility(View.VISIBLE);
        txtTitle.setText("Você respondeu todos os quizzes");

        action.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    private void openQuiz (int id, String category, int color, String img, String quant){
        Intent intent = new Intent(context, QuizActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("tittle", category);
        intent.putExtra("color", color);
        intent.putExtra("img", img);
        intent.putExtra("quant", quant);
        context.startActivity(intent);
    }

    class ViewHolder {
        View quiz;

        public ViewHolder (View view){
            quiz = view.findViewById(R.id.view_quiz);
        }
    }
}
