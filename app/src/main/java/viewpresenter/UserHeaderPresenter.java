package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.NotificationActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.ProdutoFData;
import models.user.UserModel;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class UserHeaderPresenter extends MainViewPresenter{

    ViewHolder mHolder;
    UserModel dataModel;
    Listener listener;

    public UserHeaderPresenter(Activity context, int ViewID, ViewGroup viewHolder, UserModel dataModel, Listener listener) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        this.listener = listener;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (UserModel user){
        //Foto
        UTILS.getImageAt(context, user.getImg(), mHolder.imagePerfil, new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_placeholderuser)
                .error(R.drawable.ic_placeholderuser));

        //Nome
        mHolder.nomePerfil.setText(user.getNome());

        //Pontos
        int coin = user.getMoedas();
        mHolder.moedas.setText(String.valueOf(coin));

        if (coin <= 1 ) {
            mHolder.moedasDes.setText(context.getResources().getString(R.string.moeda));
        }else {
            mHolder.moedasDes.setText(context.getResources().getString(R.string.moedas));
        }

        //Notifications
        if(user.getNotifications() > 0){
            mHolder.notificationCount.setVisibility(View.VISIBLE);
            mHolder.notificationCount.setText(String.valueOf(user.getNotifications()));
        }else{
            mHolder.notificationCount.setVisibility(View.GONE);
        }

        //Socio torcedor
        mHolder.nivel.setText(user.isSocioTorcedor()? "Sócio Torcedor" : "Torcedor");

        mHolder.notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnNotificationClick();
            }
        });
    }


    public interface Listener {
        void OnNotificationClick();
    }

    class ViewHolder {

        ImageView imagePerfil;
        TextView nomePerfil;
        TextView nivel;
        TextView moedas;
        TextView moedasDes;
        TextView notificationCount;
        ImageView notification;

        public ViewHolder (View view){
            imagePerfil = view.findViewById(R.id.user_image_menu);
            nomePerfil = view.findViewById(R.id.menu_txt_user);
            nivel = view.findViewById(R.id.menu_txt_nivel);
            moedas = view.findViewById(R.id.txt_moedas);
            moedasDes = view.findViewById(R.id.txt_moedas_des);
            notificationCount = view.findViewById(R.id.txt_notification);
            notification = view.findViewById(R.id.notification);
        }
    }
}
