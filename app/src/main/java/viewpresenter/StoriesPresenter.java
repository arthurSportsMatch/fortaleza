package viewpresenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import adapter.InstagramAdapter;
import data.InstagramData;
import data.TweetResultData;
import utils.AsyncOperation;
import utils.TRACKING;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class StoriesPresenter extends MainViewPresenter {

    ViewHolder mHolder;
    List<InstagramData> dataModel;

    public StoriesPresenter(Activity context, int ViewID, ViewGroup viewHolder, List<InstagramData>  dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<InstagramData> list){
        InstagramAdapter adapter = new InstagramAdapter(context, list,new InstagramAdapter.Listener() {
            @Override
            public void OnClick(InstagramData data) {
                showStories(data);
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_STORIES_EXPAND).execute();
            }
        });

        mHolder.instagram.setAdapter(adapter);
        mHolder.instagram.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    }

    void showStories (InstagramData instagramData){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        // set the custom layout
        final View customLayout = context.getLayoutInflater().inflate(R.layout.dialog_instagram, null);
        ImageView img = customLayout.findViewById(R.id.img_storie);
        final WebView video = customLayout.findViewById(R.id.img_storie_video);

        Glide.with(context)
                .load(instagramData.getDisplay_url())
                .into(img);

        if(instagramData.getVideo_src() != null){
            video.loadUrl(instagramData.getVideo_src());
            video.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    // do your stuff here
                    video.setVisibility(View.VISIBLE);
                }
            });
        }

        builder.setView(customLayout);

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    class ViewHolder {
        RecyclerView instagram;
        public ViewHolder (View view){
            instagram = view.findViewById(R.id.recycler_stories);
        }
    }
}
