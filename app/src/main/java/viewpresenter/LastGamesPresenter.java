package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.MatchHistoryActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import adapter.LastResultAdapter;
import data.PartidaData;
import utils.AsyncOperation;
import utils.TRACKING;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LastGamesPresenter extends MainViewPresenter{

    ViewHolder mHolder;
    List<PartidaData> dataModel;

    public LastGamesPresenter(Activity context, int ViewID , ViewGroup viewHolder, List<PartidaData> dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<PartidaData> data){
        LastResultAdapter lastResultAdapter = new LastResultAdapter(context, data, R.layout.adapter_last_results, new LastResultAdapter.Listener() {
            @Override
            public void onClick(int id) {
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_ULTIMOSRESULTADOR_ABRIR, id).execute();
                Intent intent = new Intent(context, MatchHistoryActivity.class);
                intent.putExtra("id", id);
                context.startActivity(intent);
            }
        });

        mHolder.lastResults.setAdapter(lastResultAdapter);
        mHolder.lastResults.setLayoutManager( new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    }

    class ViewHolder {
        RecyclerView lastResults;
        public ViewHolder(View view){
            lastResults = view.findViewById(R.id.recycle_last_results);
        }
    }
}
