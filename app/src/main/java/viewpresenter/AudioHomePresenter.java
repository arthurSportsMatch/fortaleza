package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.PlaylistActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import data.PlaylistItemData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class AudioHomePresenter extends MainViewPresenter{

    ViewHolder mHolder;
    PlaylistItemData dataModel;


    public AudioHomePresenter(Activity context, int ViewID, ViewGroup viewHolder, PlaylistItemData dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }
    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view =  super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValeus(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValeus (final PlaylistItemData data){
        Log.d("HOME", data.toString());
        UTILS.getImageAt(context, data.getImg(), mHolder.audioImage, new RequestOptions()
                .transforms(new RoundedCorners(20)));

        mHolder.audioTittle.setText(data.getTitulo());

        mHolder.audioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_AUDIOTECA, data.getId()).execute();
                Intent intent = new Intent(context, PlaylistActivity.class);
                intent.putExtra("initFirt", true);
                context.startActivity(intent);
            }
        });
    }

    class ViewHolder {
        ImageView audioImage;
        TextView audioTittle;
        Button audioPlay;
        public ViewHolder (View view){
            audioImage = view.findViewById(R.id.img_audio_image);
            audioTittle = view.findViewById(R.id.txt_audio_tittle);
            audioPlay = view.findViewById(R.id.btn_audio_play);
        }
    }

}
