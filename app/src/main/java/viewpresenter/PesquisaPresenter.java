package viewpresenter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.MainMenuActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.StartMenuActivity;

import java.util.List;

import adapter.QuizAdapter;
import data.QuizData;
import data.TweetResultData;
import fragments.GamesFragment;
import vmodels.fragment.FragmentHolderViewModel;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class PesquisaPresenter extends MainViewPresenter{

    ViewHolder mHolder;
    List<QuizData> dataModel;
    FragmentHolderViewModel mfragmentViewModel;

    public PesquisaPresenter(Activity context, int ViewID, FragmentHolderViewModel mfragmentViewModel, ViewGroup viewHolder, List<QuizData>  dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        this.mfragmentViewModel = mfragmentViewModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<QuizData> data){
        mHolder.searchResults.setVisibility(View.VISIBLE);
        mHolder.respondaPesquisas.setVisibility(View.GONE);
        mHolder.searchText.setVisibility(View.VISIBLE);

        QuizAdapter quizAdapter = new QuizAdapter(context, data);
        mHolder.searchResults.setAdapter(quizAdapter);
        mHolder.searchResults.setLayoutManager( new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        mHolder.respondaPesquisas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfragmentViewModel.changeFragment(new GamesFragment("pesquisa"), "GamesFragment");
            }
        });
    }

    class ViewHolder {
        TextView searchText;
        Button respondaPesquisas;
        RecyclerView searchResults;

        public ViewHolder (View view){
            searchText = view.findViewById(R.id.txt_last_pesquisas);
            respondaPesquisas = view.findViewById(R.id.btn_responda_pesquisas);
            searchResults = view.findViewById(R.id.last_pesquisa_pager);
        }
    }
}
