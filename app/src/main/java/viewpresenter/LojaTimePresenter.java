package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.NavegadorActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import adapter.LojaVirtualItemAdapter;
import data.ProdutoFData;
import data.TweetResultData;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class LojaTimePresenter extends MainViewPresenter{

    ViewHolder mHolder;
    List<ProdutoFData> dataModel;

    public LojaTimePresenter(Activity context, int ViewID, ViewGroup viewHolder, List<ProdutoFData>  dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<ProdutoFData> produtos){
        DisplayMetrics dimension = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int weight = dimension.widthPixels;
        int nWeigth = (weight / 100) * 60;

        LojaVirtualItemAdapter lojaFAdapter = new LojaVirtualItemAdapter(context, nWeigth, produtos, new LojaVirtualItemAdapter.Listener() {
            @Override
            public void onClick(ProdutoFData prod) {
                if(prod.getEmbed() == 1){
                    Intent intent = new Intent(context, NavegadorActivity.class);
                    intent.putExtra("link", prod.getUrl());
                    context.startActivity(intent);
                }else{
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(prod.getUrl()));
                    context.startActivity(i);
                }
            }
        });

        mHolder.lojaFortaleza.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        mHolder.lojaFortaleza.setHasFixedSize(true);
        mHolder.lojaFortaleza.setAdapter(lojaFAdapter);

        mHolder.verTodosLojaFortaleza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_DESTAQUES_VERTODOS).execute();
                Intent intent = new Intent(context, LojaFortalezaActivity.class);
                context.startActivity(intent);
            }
        });
    }

    class ViewHolder {
        RecyclerView lojaFortaleza;
        TextView verTodosLojaFortaleza;
        public ViewHolder (View view){
            lojaFortaleza = view.findViewById(R.id.recycler_loja_fortaleza);
            verTodosLojaFortaleza = view.findViewById(R.id.txt_ver_todos_loja_fortaleza);
        }
    }
}
