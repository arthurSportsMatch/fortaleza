package viewpresenter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import models.user.UserModel;
import utils.MaskEditUtil;
import utils.UTILS;


/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class CarteirinhaPresenter extends MainViewPresenter {

    ViewHolder mHolder;

    public CarteirinhaPresenter(Activity context, int ViewID , ViewGroup viewHolder) {
        this.context = context;
        this.viewHolder = viewHolder;
        initView(ViewID, viewHolder);
    }


    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    public void initValues (UserModel user){
//        Bitmap qrCode = new QRCodeController().generate("https://appdogalo.com.br/noticia/site/baixe-o-app-atltico-oficial/49");
//        mHolder.userQrCode.setImageBitmap(qrCode);
//
//        DisplayMetrics dimension = new DisplayMetrics();
//        context.getWindowManager().getDefaultDisplay().getMetrics(dimension);
//
//
//        int weight = dimension.widthPixels;
////        if(weight > 280){
////            weight = 480;
////        }
//
////        mHolder.bgGalo.getLayoutParams().width = weight;
////        mHolder.bgGalo.getLayoutParams().height = (weight / 100) * 75;
////        mHolder.bgGalo.requestLayout();
//
//        int nweight = (weight / 100) * 65;
//
//        mHolder.mainView.getLayoutParams().height = nweight;
//        mHolder.mainView.requestLayout();
//
//        mHolder.bgGalo.getLayoutParams().height = nweight;
//        mHolder.bgGalo.requestLayout();
////
////        mHolder.imagesHolder.getLayoutParams().width = (weight / 100) * 34;
////        mHolder.imagesHolder.requestLayout();
////
////        mHolder.galo.getLayoutParams().height = weight / 100 * 18;
////        mHolder.galo.requestLayout();
//
////        mHolder.userImage.getLayoutParams().width = nWeight - 30;
////        mHolder.userImage.requestLayout();
//
//        UTILS.getImageAt(context, user.getImg(), mHolder.userImage, new RequestOptions()
//                .placeholder(R.drawable.ic_placeholderuser)
//                .error(R.drawable.ic_placeholderuser));
//
//        mHolder.userName.setText(user.getNome());
//        if(user.getDn() == null || user.getDn().isEmpty()){
//            mHolder.userDate.setText("Sempre");
//        }else{
//            String nascimento = user.getDn();
//            nascimento = UTILS.formatDate(nascimento);
//            nascimento = nascimento.replace("-", "");
//            Log.d("Async", nascimento);
//            mHolder.userDate.setText(nascimento.substring(4));
//        }
//
//        String creted = user.getDtCreated();
//        creted = UTILS.formatDate(creted);
//        creted = creted.replace("-", "/");
//        mHolder.userCadastro.setText(creted.substring(3));
//        //String ID = user.getCardID().replaceAll("\\D+","").substring(0, 6);
//        mHolder.userID.setText(createId(user.getCardID()));
    }

    String createId (int ID){
        return ""; //MaskEditUtil.mask(String.valueOf(ID), MaskEditUtil.FORMAT_CARTEIRA, "0");
    }

    class ViewHolder {
        View mainView;
        View imagesHolder;
        ImageView galo;
        ImageView bgGalo;
        ImageView userImage;
        ImageView userQrCode;
        TextView userName;
        TextView userDate;
        TextView userCadastro;
        TextView userID;
        public ViewHolder (View view){
//            mainView = view.findViewById(R.id.main);
//            imagesHolder = view.findViewById(R.id.holder_images);
//            galo = view.findViewById(R.id.img_galo);
//            bgGalo = view.findViewById(R.id.bg_carteira);
//            userImage = view.findViewById(R.id.img_user);
//            userQrCode = view.findViewById(R.id.img_qrcode);
//            userName = view.findViewById(R.id.txt_nome);
//            userDate = view.findViewById(R.id.txt_idade);
//            userCadastro = view.findViewById(R.id.txt_expedido);
//            userID = view.findViewById(R.id.txt_number);
        }
    }
}
