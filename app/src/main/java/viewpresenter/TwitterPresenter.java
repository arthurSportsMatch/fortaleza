package viewpresenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sportm.fortaleza.MediaPlayActivity;
import com.sportm.fortaleza.R;
import com.sportm.fortaleza.TimelineActivity;
import com.sportm.fortaleza.TwitterFeedCardExpandedActivity;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import adapter.TweetFeedAdapter;
import data.PlaylistItemData;
import data.TweetResultData;
import utils.AsyncOperation;
import utils.TRACKING;
import view.DynamicTextView;

import static com.sportm.fortaleza.MainActivity.emptyAsync;

/**
 * Created by Carlos Vinicius at 23/06/20
 * Contact : carlos.vspaixao@gmail.com
 * Copyright : SportsMatch 2020
 */
public class TwitterPresenter extends MainViewPresenter{


    ViewHolder mHolder;
    List<TweetResultData> dataModel;
    TweetFeedAdapter tweetsSupAdapter;

    public TwitterPresenter(Activity context, int ViewID, ViewGroup viewHolder,  List<TweetResultData>  dataModel) {
        this.context = context;
        this.viewHolder = viewHolder;
        this.dataModel = dataModel;
        initView(ViewID, viewHolder);
    }

    @Override
    protected View initView(int viewID, ViewGroup parentView) {
        View view = super.initView(viewID, parentView);
        mHolder = new ViewHolder(view);
        initValues(dataModel);
        parentView.removeAllViews();
        parentView.addView(view);
        return view;
    }

    void initValues (List<TweetResultData> tweets){
        List<TweetResultData> list = tweets;
        list = removeDoblesTwitts(list);

        if(list == null || list.isEmpty()){
            configureNoTwitterFeedSup();
            return;
        }

        mHolder.twitterFeedSupView.setVisibility(View.VISIBLE);
        mHolder.txtNoTwitterFeedSup.setVisibility(View.GONE);
        mHolder.frameLoadingTwitterFeedSup.setVisibility(View.GONE);

        Collections.sort(list, new Comparator<TweetResultData>() {
            public int compare(TweetResultData o1, TweetResultData o2) {
                if (o1.getDate() == null || o2.getDate() == null) {
                    return 0;
                }
                return o2.getDate().compareTo(o1.getDate());
            }
        });

        if(!list.get(list.size() - 1).isLastItem()) {
            TweetResultData lastItem = new TweetResultData();
            lastItem.setLastItem(true);
            list.add(list.size(), lastItem);
        }

        DisplayMetrics dimension = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dimension);
        int weight = dimension.widthPixels;
        int nWeigth = (weight / 100) * 90;

        if (tweetsSupAdapter == null ) {
            tweetsSupAdapter = new TweetFeedAdapter(context, R.layout.list_item_tweet_carousel, nWeigth, list, new TweetFeedAdapter.ITweetSelectable() {
                @Override
                public void OnSelect(TweetResultData item, WebView webView) {

                    String twits = "@" + item.getNameUser() + "@" + item.getId();
                    new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_TWITTEREXPAND, twits).execute();

                    webView.loadUrl(webView.getUrl());

                    TwitterFeedCardExpandedActivity.setTweet(item);
                    Intent intent = new Intent(context, TwitterFeedCardExpandedActivity.class);
                    context.startActivityForResult(intent, 1);

                }

                @Override
                public void OnMediaClick(TweetResultData item) {
                    if (item.isImage()) {
                        Bundle b = new Bundle();
                        b.putInt("contentType", MediaPlayActivity.MEDIA_PLAY_CONTENT_TYPE_IMAGE);
                        b.putString("content", item.getImgContent());
                        Intent newIntent = new Intent(context, MediaPlayActivity.class);
                        newIntent.putExtras(b);
                        context.startActivity(newIntent);
                    }
                }

                @Override
                public void OnLearnMoreClick(TweetResultData item) {
                    new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_TWITTERVERMAIS, 1).execute();
                    TwitterFeedCardExpandedActivity.setTweet(item);
                    Intent intent = new Intent(context, TwitterFeedCardExpandedActivity.class);
                    context.startActivity(intent);
                }

                @Override
                public void OnLastCardClick() {
                    callAllTweets();
                    new AsyncOperation(context, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_TWITTERVERTODOS, 1).execute();
                }
            });
        }

        mHolder.twitterFeedSupView.setAdapter(tweetsSupAdapter);
        mHolder.seeAllTwitts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAllTweets();
            }
        });
    }

    //Remove tweets duplos
    List<TweetResultData> removeDoblesTwitts (List<TweetResultData> list){
        List<TweetResultData> finalTweets = new ArrayList<>();

        for(TweetResultData d : list){
            if(checkID(d.getId(), finalTweets)){
                finalTweets.add(d);
            }
        }

        return finalTweets;
    }

    //Verifica ID do tweet
    boolean checkID (long id, List<TweetResultData> list){
        for(TweetResultData d : list){
            if(d.getId() == id){
                return false;
            }
        }
        return true;
    }

    void callAllTweets(){
        Intent newIntent = new Intent(context, TimelineActivity.class);
        context.startActivity(newIntent);
    }

    //Configura quando não tem twitts
    void configureNoTwitterFeedSup(){
        mHolder.txtNoTwitterFeedSup.setVisibility(View.VISIBLE);
        mHolder.frameLoadingTwitterFeedSup.setVisibility(View.GONE);
        mHolder.twitterFeedSupView.setVisibility(View.INVISIBLE);
        mHolder.twitterFeedSupView.setAdapter(null);
    }

    class ViewHolder {
        TextView seeAllTwitts;
        public FrameLayout frameLoadingTwitterFeedSup;
        public DynamicTextView txtNoTwitterFeedSup;
        public TwoWayView twitterFeedSupView;

        public ViewHolder (View view){
            seeAllTwitts = view.findViewById(R.id.txt_ver_todos_twitter);

            twitterFeedSupView = (TwoWayView) view.findViewById(R.id.twitterFeedSupView);
            twitterFeedSupView.setVisibility(view.INVISIBLE);

            txtNoTwitterFeedSup = (DynamicTextView)view.findViewById(R.id.txtNoTwitterFeedSup);
            txtNoTwitterFeedSup.setVisibility(view.GONE);

            frameLoadingTwitterFeedSup = (FrameLayout)view.findViewById(R.id.frameLoadingTwitterFeedSup);
            frameLoadingTwitterFeedSup.setVisibility(view.VISIBLE);
        }
    }
}
