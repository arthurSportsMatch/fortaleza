package data;

import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.models.Tweet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TweetResultData {

    private long id = 0;
    private String imgUser = "";
    private String nameUser = "";
    private String tagUser = "";
    //    private TwitterDate date = new TwitterDate();
    private Date date = null;
    private String msgContent = "";
    private String imgContent = "";
    private String videoContent = "";
    private String videoHtmlToExtract = "";
    private String imageHtmlToExtract = "";
    private boolean triedToLoadVideo = false;
    private boolean triedToLoadImg = false;
    private boolean isImage = true;
    private boolean isLastItem = false;
    private String nextResults;

    public TweetResultData(){

    }

    public TweetResultData(Tweet result){
        if( (result != null) ){
            this.id = result.id;

            if(result.user != null) {
                this.imgUser = result.user.profileImageUrl;
                this.nameUser = result.user.name;
                this.tagUser = "@" + result.user.screenName;
            }

//            this.date = TwitterDate.parseDate(result.createdAt);
            setDate(result.createdAt);
            this.msgContent = result.text;

            if( (result.entities != null) && (result.entities.media != null) &&
                    (result.entities.media.size() > 0) && (result.entities.media.get(0) != null)
                    && (result.entities.media.get(0).mediaUrl != null) ){
                this.imgContent = result.entities.media.get(0).mediaUrl;
            }
        }
    }

    public TweetResultData(Result<Tweet> result){
        if( (result != null) && (result.data != null) ){
            this.id = result.data.id;

            if(result.data.user != null) {
                this.imgUser = result.data.user.profileImageUrl;
                this.nameUser = result.data.user.name;
                this.tagUser = "@" + result.data.user.screenName;
            }

//            this.date = TwitterDate.parseDate(result.data.createdAt);
            setDate(result.data.createdAt);
            this.msgContent = result.data.text;

            if( (result.data.entities != null) && (result.data.entities.media != null) &&
                    (result.data.entities.media.size() > 0) && (result.data.entities.media.get(0) != null)
                    && (result.data.entities.media.get(0).mediaUrl != null) ){
                this.imgContent = result.data.entities.media.get(0).mediaUrl;
            }
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUser() {
        return imgUser;
    }

    public void setImgUser(String imgUser) {
        this.imgUser = imgUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTagUser() {
        return "@" + tagUser;
    }

    public void setTagUser(String tagUser) {
        this.tagUser = tagUser;
    }

//    public TwitterDate getDate() {
//        return date;
//    }

    public Date getDate() {
        return date;
    }

    public void setDate(String string) {
        DateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy", Locale.ENGLISH);
        try {
            this.date = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
            this.date = null;
        }

    }

//    public void setDate(TwitterDate date) {
//        this.date = date;
//    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getImgContent() {
        return imgContent;
    }

    public void setImgContent(String imgContent) {
        this.imgContent = imgContent;
    }

    public String getVideoContent() {
        return videoContent;
    }

    public void setVideoContent(String videoContent) {
        this.videoContent = videoContent;
    }

    public String getVideoHtmlToExtract() {
        return videoHtmlToExtract;
    }

    public void setVideoHtmlToExtract(String videoHtmlToExtract) {
        this.videoHtmlToExtract = videoHtmlToExtract;
    }

    public String getImageHtmlToExtract() {
        return imageHtmlToExtract;
    }

    public void setImageHtmlToExtract(String imageHtmlToExtract) {
        this.imageHtmlToExtract = imageHtmlToExtract;
    }

    public boolean isTriedToLoadVideo() {
        return triedToLoadVideo;
    }

    public void setTriedToLoadVideo(boolean triedToLoadVideo) {
        this.triedToLoadVideo = triedToLoadVideo;
    }

    public boolean isTriedToLoadImg() {
        return triedToLoadImg;
    }

    public void setTriedToLoadImg(boolean triedToLoadImg) {
        this.triedToLoadImg = triedToLoadImg;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public boolean isLastItem() {
        return isLastItem;
    }

    public void setLastItem(boolean lastItem) {
        isLastItem = lastItem;
    }

    public static List<TweetResultData> parseTweetResultDataList(JSONArray array){
        List<TweetResultData> list = new ArrayList<>();

        boolean success = false;
        int size = array.length();

        try{
            for(int i = 0; i < size; i++){
                TweetResultData data = new TweetResultData();
                JSONObject json = array.getJSONObject(i);

                data.setId( (json.has("id") && (json.get("id") != null) ? json.getInt("id") : 0 ) );
                data.setMsgContent((json.has("full_text") && (json.get("full_text") != null) ? json.getString("full_text") : "" ));
                //data.setDate( TwitterDate.parseDate((json.has("created_at") && (json.get("created_at") != null) ? json.getString("created_at") : "" )) );
                data.setDate( (json.has("created_at") && (json.get("created_at") != null) ? json.getString("created_at") : "" ) );

                JSONObject user = (json.has("user") && (json.get("user") != JSONObject.NULL)) ? json.getJSONObject("user") : new JSONObject();

                data.setNameUser( (user.has("name") && (user.get("name") != JSONObject.NULL) ? user.getString("name") : "" ) );
                data.setTagUser( (user.has("screen_name") && (user.get("screen_name") != JSONObject.NULL) ? user.getString("screen_name") : "" ) );
                data.setImgUser( (user.has("profile_image_url") && (user.get("profile_image_url") != JSONObject.NULL) ? user.getString("profile_image_url") : "" ) );

                JSONObject entities = (json.has("extended_entities") && (json.get("extended_entities") != JSONObject.NULL)) ? json.getJSONObject("extended_entities") : (json.has("entities") && (json.get("entities") != JSONObject.NULL)) ? json.getJSONObject("entities") : new JSONObject();
                JSONArray media = (entities.has("media") && (entities.get("media") != JSONObject.NULL)) ? entities.getJSONArray("media") : new JSONArray();

                if( (!entities.has("media")) && (entities.has("urls")) ){
                    JSONArray urls = entities.getJSONArray("urls");
                    if(urls.length() > 0){
                        JSONObject firstUrl = urls.getJSONObject(0);
                        data.setImageHtmlToExtract( (firstUrl.has("url") && (firstUrl.get("url") != JSONObject.NULL) ? firstUrl.getString("url") : "" ) );
                    }

                }

                int mediaSize = media.length();

                if(mediaSize > 0){
                    JSONObject mediaJson = media.getJSONObject(0);

                    data.setImgContent (mediaJson.has("media_url") && (mediaJson.get("media_url") != JSONObject.NULL) ? mediaJson.getString("media_url") : "" );
                    data.setVideoHtmlToExtract (mediaJson.has("url") && (mediaJson.get("url") != JSONObject.NULL) ? mediaJson.getString("url") : "" );
                    data.setImage (!mediaJson.has("type") || (mediaJson.get("type") == JSONObject.NULL) || mediaJson.getString("type").equals("photo"));
                }

                list.add(data);
                success = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            success = false;
        }

        return (success ? list : new ArrayList<TweetResultData>());
    }

    public static String getNextQuerry (JSONObject obj){

        String querry = "";
        try {
            querry =  ((obj.has("next_results") && (obj.get("next_results") != null) ? obj.getString("next_results") : "" ) );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return querry;
    }

    public String getNextResults() {
        return nextResults;
    }

    public void setNextResults(String nextResults) {
        this.nextResults = nextResults;
    }
}

