package data;

import android.content.Intent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CheckinData {

    private String email = "gigliani@swind.com.br"; //sempre os mesmos
    private String token = "qqtw2SCcZyGYzcpDR7gn"; //sempre os mesmos
    private String socioID = "";
    private String pessoa_id;
    private String jogoID;
    private String setorID;
    private String ncc;
    private String setorName;   //para construção da tela
    private boolean hasCadeiras = false;
    private boolean hasTickets = false;
    private String setorDiv;
    private String setorDivName;
    private List<String> cadeiraID;
    private String cadeiraNome;
    private List<String> cadeiras;
    private List<JSONObject> tickets;
    private JSONArray ingressos;
    private String status;
    private String jogo_id;
    private String estadio_setor_id;
    private String nome;
    private CheckinPartidaData partida;


    public String getNcc() {
        return ncc;
    }

    public void setNcc(String ncc) {
        this.ncc = ncc;
    }

    public boolean isHasCard(){return ncc != null;}

    public boolean isHasTickets() {
        return hasTickets;
    }

    public void setHasTickets(boolean hasTickets) {
        this.hasTickets = hasTickets;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSocioID() {
        return socioID;
    }

    public void setSocioID(String socioID) {
        if(socioID != null){
            this.socioID = socioID;
        }
    }

    public String getJogoID() {
        return jogoID;
    }

    public void setJogoID(String jogoID) {
        if(jogoID != null){
            this.jogoID = jogoID;
        }
    }

    public String getSetorID() {
        return setorID;
    }

    public void setSetorID(String setorID) {
        if(setorID != null){
            this.setorID = setorID;
        }
    }

    public JSONArray getIngressos() {
        return ingressos;
    }

    public void setIngressos(JSONArray ingressos) {
        this.ingressos = ingressos;
    }

    public List<String> getCadeiraID() {
        return cadeiraID;
    }

    public void setCadeiraID(List<String> cadeiraID) {
        this.cadeiraID = cadeiraID;
    }

    public List<String> getNomes() {
        List<String> nomes = new ArrayList<>();
        try {
            for ( int i =0; i<tickets.size(); i++ ) {
                try {
                    nomes.add(tickets.get(i).getString("nome"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception ignored) {
            return new ArrayList<>();
        }
        return nomes;
    }

    public List<Integer> getIds() {
        List<Integer> ids = new ArrayList<>();
        for ( int i =0; i<tickets.size(); i++ ) {
            try {
                ids.add(tickets.get(i).getInt("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ids;
    }

    public List<Boolean> getIsPrimary() {
        List<Boolean> primary = new ArrayList<>();
        for ( int i =0; i<tickets.size(); i++ ) {
            try {
                primary.add(tickets.get(i).getBoolean("primario"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return primary;
    }

    public List<String> getIngressosDefinido() {
        List<String> ingressos = new ArrayList<>();
        for ( int i =0; i<tickets.size(); i++ ) {
            try {
                ingressos.add(tickets.get(i).getString("ingresso"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ingressos;
    }

    public void addCadeiraId(String id, int index) {
        if(this.cadeiraID == null){
            this.cadeiraID = new ArrayList<>();
        }
        if ( index >= 0 ) {
            this.cadeiraID.add(index, id);
        }else {
            this.cadeiraID.add("");
        }
    }

    public void addTicket(JSONObject ticket) {
        if ( this.tickets == null ) {
            this.tickets = new ArrayList<>();
        }
        this.tickets.add(ticket);

        if(this.cadeiraID == null){
            this.cadeiraID = new ArrayList<>();
        }
        if(this.cadeiras == null){
            this.cadeiras = new ArrayList<>();
        }

        this.cadeiras.add("");
        this.cadeiraID.add("");
    }

    public void removerCadeiras() {
        this.getCadeiras().clear();
        this.getCadeiraID().clear();
    }

    public void removeTicket(JSONObject ticket) {
        if (this.tickets == null) {
            return;
        }
        this.tickets.remove(ticket);
        this.getCadeiraID().remove(0);
        this.getCadeiras().remove(0);
    }

    public void removerCadeira(String id, String nome) {
        cadeiraID.remove(id);
        cadeiras.remove(nome);
    }

    public List<String> getCadeiras() {
        return cadeiras;
    }

    public List<JSONObject> getTickets() {
        return tickets;
    }

    public void setCadeiras(List<String> cadeiras) {
        this.cadeiras = cadeiras;
    }

    public void addCadeiras(String nome, int index) {
        if(this.cadeiras == null){
            this.cadeiras = new ArrayList<>();
        }
        if ( index >= 0 ) {
            this.cadeiras.add(index, nome);
        }else {
            this.cadeiras.add("");
        }
    }

    public String getSetorName() {
        return setorName;
    }



    public void setSetorName(String setorName) {
        this.setorName = setorName;
    }

    public List<String> getValores() {
        List<String> valores = new ArrayList<>();
        try {

            for ( int i =0; i<tickets.size(); i++ ) {
                try {
                    valores.add(tickets.get(i).getDouble("valor")+"");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception ignored) {
            return new ArrayList<>();
        }
        return valores;
    }

    public float getValorTotal() {
        List<String> valores = getValores();
        BigDecimal total;
        float soma = 0;

        for ( int i =0; i< valores.size(); i++ ) {
            soma = soma + Float.parseFloat(valores.get(i));
        }
        total = new BigDecimal(soma);
        return total.floatValue();
    }

    public void setTickets(List<JSONObject> tickets) {
        this.tickets = tickets;
    }

    public boolean isHasCadeiras() {
        return hasCadeiras;
    }

    public void setHasCadeiras(boolean hasCadeiras) {
        this.hasCadeiras = hasCadeiras;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNome() {

        return nome;
    }

    public CheckinPartidaData getPartida() {
        return partida;
    }

    public void setPartida(CheckinPartidaData partida) {
        this.partida = partida;
    }

    public String getEstadio_setor_id() {
        return estadio_setor_id;
    }

    public void setEstadio_setor_id(String estadio_setor_id) {
        this.estadio_setor_id = estadio_setor_id;
        this.setorID = estadio_setor_id;
    }

    public String getJogo_id() {
        return jogo_id;
    }

    public void setJogo_id(String jogo_id) {
        this.jogo_id = jogo_id;
        this.jogoID = jogo_id;
    }

    public String getPessoa_id() {
        return pessoa_id;
    }

    public void setPessoa_id(String pessoa_id) {
        this.pessoa_id = pessoa_id;
        this.socioID = pessoa_id;
    }

    public String getSetorDiv() {
        return setorDiv;
    }

    public void setSetorDiv(String setorDiv) {
        this.setorDiv = setorDiv;
    }

    public String getSetorDivName() {
        return setorDivName;
    }

    public void setSetorDivName(String setorDivName) {
        this.setorDivName = setorDivName;
    }

    public String getCadeiraNome() {
        return cadeiraNome;
    }

    public void setCadeiraNome(String cadeiraNome) {
        this.cadeiraNome = cadeiraNome;
    }
}
