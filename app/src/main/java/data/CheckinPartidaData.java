package data;

public class CheckinPartidaData {

    private int id;
    private String campeonato;
    private String data;
    private String hora;
    private String adversario;
    private String estadio;
    private int estadio_id;
    private int id_sistema_legado;
    private String logo_adversario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(String campeonato) {
        this.campeonato = campeonato;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getAdversario() {
        return adversario;
    }

    public void setAdversario(String adversario) {
        this.adversario = adversario;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public int getEstadio_id() {
        return estadio_id;
    }

    public void setEstadio_id(int estadio_id) {
        this.estadio_id = estadio_id;
    }

    public int getId_sistema_legado() {
        return id_sistema_legado;
    }

    public void setId_sistema_legado(int id_sistema_legado) {
        this.id_sistema_legado = id_sistema_legado;
    }

    public String getLogo_adversario() {
        return logo_adversario;
    }

    public void setLogo_adversario(String logo_adversario) {
        this.logo_adversario = logo_adversario;
    }

    @Override
    public String toString() {
        return "CheckinPartidaData{" +
                "id=" + id +
                ", campeonato='" + campeonato + '\'' +
                ", data='" + data + '\'' +
                ", hora='" + hora + '\'' +
                ", adversario='" + adversario + '\'' +
                ", estadio='" + estadio + '\'' +
                ", estadio_id=" + estadio_id +
                ", id_sistema_legado=" + id_sistema_legado +
                ", logo_adversario='" + logo_adversario + '\'' +
                '}';
    }
}
