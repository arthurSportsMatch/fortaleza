package data;

public class ProdutoData {
    private int id;
    private int idCupom;
    private int idGrupo;
    private String nome;
    private String descricao;
    private String adquirido;
    private String dataExpiracao;
    private String cupom;
    private String img;
    private String valor;
    private String valorST;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAdquirido() {
        return adquirido;
    }

    public void setAdquirido(String adquirido) {
        this.adquirido = adquirido;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValorST() {
        return valorST;
    }

    public void setValorST(String valorST) {
        this.valorST = valorST;
    }

    public int getIdCupom() {
        return idCupom;
    }

    public void setIdCupom(int idCupom) {
        this.idCupom = idCupom;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCupom() {
        return cupom;
    }

    public void setCupom(String cupom) {
        this.cupom = cupom;
    }

    public String getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(String dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    @Override
    public String toString() {
        return "ProdutoData{" +
                "id=" + id +
                ", idCupom=" + idCupom +
                ", idGrupo=" + idGrupo +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", adquirido='" + adquirido + '\'' +
                ", dataExpiracao='" + dataExpiracao + '\'' +
                ", cupom='" + cupom + '\'' +
                ", img='" + img + '\'' +
                ", valor='" + valor + '\'' +
                ", valorST='" + valorST + '\'' +
                '}';
    }
}
