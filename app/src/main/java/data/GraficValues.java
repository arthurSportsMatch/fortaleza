package data;

public class GraficValues {

    private String tit;
    private float max;
    private float v;
    private int p;

    private String name;
    private float value;

    public GraficValues(String name, float value, float max) {
        this.tit = name;
        this.v = value;
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public float getV() {
        return v;
    }

    public void setV(float v) {
        this.v = v;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    @Override
    public String toString() {
        return "GraficValues{" +
                "tit='" + tit + '\'' +
                ", max=" + max +
                ", v=" + v +
                ", p=" + p +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
