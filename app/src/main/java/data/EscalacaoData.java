package data;

import android.provider.ContactsContract;

import java.util.List;

public class EscalacaoData {
    private int id;
    private String titulo;
    private int esquema;

    private List<EscalaAtleta> players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getEsquema() {
        return esquema;
    }

    public void setEsquema(int esquema) {
        this.esquema = esquema;
    }

    public List<EscalaAtleta> getPlayers() {
        return players;
    }

    public void setPlayers(List<EscalaAtleta> players) {
        this.players = players;
    }

}
