package data;

public class RespostasData {

    private int id;
    private String resposta;
    private int qtd;
    private int correto;

    public RespostasData(String resposta, int qtd) {
        this.resposta = resposta;
        this.qtd = qtd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public int getCorreto() {
        return correto;
    }

    public void setCorreto(int correto) {
        this.correto = correto;
    }
}
