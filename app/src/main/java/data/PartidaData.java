package data;

public class PartidaData {

    private int id;
    private String dataHora;
    private String header;
    private String title;
    private String desc;
    private String rodada;
    private String genero;
    private String clube1;
    private String cor1;
    private String clubeImg1;
    private String golsClube1;
    private String penC1;
    private String cor2;
    private String clube2;
    private String clubeImg2;
    private String golsClube2;
    private String penC2;
    private String estadio;
    private String linkCompra;
    private int permiteCheckIn = 0;
    private String idTransmissao;
    private String hide;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRodada() {
        return rodada;
    }

    public void setRodada(String rodada) {
        this.rodada = rodada;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getClube1() {
        return clube1;
    }

    public void setClube1(String clube1) {
        this.clube1 = clube1;
    }

    public String getCor1() {
        return cor1;
    }

    public void setCor1(String cor1) {
        this.cor1 = cor1;
    }

    public String getClubeImg1() {
        return clubeImg1;
    }

    public void setClubeImg1(String clubeImg1) {
        this.clubeImg1 = clubeImg1;
    }

    public String getGolsClube1() {
        return golsClube1;
    }

    public void setGolsClube1(String golsClube1) {
        this.golsClube1 = golsClube1;
    }

    public String getPenC1() {
        return penC1;
    }

    public void setPenC1(String penC1) {
        this.penC1 = penC1;
    }

    public String getCor2() {
        return cor2;
    }

    public void setCor2(String cor2) {
        this.cor2 = cor2;
    }

    public String getClube2() {
        return clube2;
    }

    public void setClube2(String clube2) {
        this.clube2 = clube2;
    }

    public String getClubeImg2() {
        return clubeImg2;
    }

    public void setClubeImg2(String clubeImg2) {
        this.clubeImg2 = clubeImg2;
    }

    public String getGolsClube2() {
        return golsClube2;
    }

    public void setGolsClube2(String golsClube2) {
        this.golsClube2 = golsClube2;
    }

    public String getPenC2() {
        return penC2;
    }

    public void setPenC2(String penC2) {
        this.penC2 = penC2;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getLinkCompra() {
        return linkCompra;
    }

    public void setLinkCompra(String linkCompra) {
        this.linkCompra = linkCompra;
    }

    public String getIdTransmissao() {
        return idTransmissao;
    }

    public void setIdTransmissao(String idTransmissao) {
        this.idTransmissao = idTransmissao;
    }

    @Override
    public String toString() {
        return "PartidaData{" +
                "id=" + id +
                ", dataHora='" + dataHora + '\'' +
                ", header='" + header + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", rodada='" + rodada + '\'' +
                ", genero='" + genero + '\'' +
                ", clube1='" + clube1 + '\'' +
                ", cor1='" + cor1 + '\'' +
                ", clubeImg1='" + clubeImg1 + '\'' +
                ", golsClube1='" + golsClube1 + '\'' +
                ", penC1='" + penC1 + '\'' +
                ", cor2='" + cor2 + '\'' +
                ", clube2='" + clube2 + '\'' +
                ", clubeImg2='" + clubeImg2 + '\'' +
                ", golsClube2='" + golsClube2 + '\'' +
                ", penC2='" + penC2 + '\'' +
                ", estadio='" + estadio + '\'' +
                ", linkCompra='" + linkCompra + '\'' +
                ", idTransmissao='" + idTransmissao + '\'' +
                '}';
    }

    public String getHide() {
        return hide;
    }

    public void setHide(String hide) {
        this.hide = hide;
    }


    public int getPermiteCheckIn() {
        return permiteCheckIn;
    }

    public void setPermiteCheckIn(int permiteCheckIn) {
        this.permiteCheckIn = permiteCheckIn;
    }
    public boolean isPermiteCheckIn() {
        return this.permiteCheckIn == 1;
    }

}
