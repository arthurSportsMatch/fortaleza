package data;

public class ServerData {
    private int id;
    private String prefix;
    private int selected = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "ServerData{" +
                "id=" + id +
                ", prefix='" + prefix + '\'' +
                ", selected=" + selected +
                '}';
    }
}
