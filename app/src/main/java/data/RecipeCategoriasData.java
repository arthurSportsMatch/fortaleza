package data;

import java.util.List;

public class RecipeCategoriasData implements Cloneable{

    private int idcat;
    private String cat;
    private List<IngredienteData> ings;

    public RecipeCategoriasData(int idcat, String cat, List<IngredienteData> ings) {
        this.idcat = idcat;
        this.cat = cat;
        this.ings = ings;
    }

    public int getIdcat() {
        return idcat;
    }

    public void setIdcat(int idcat) {
        this.idcat = idcat;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public List<IngredienteData> getIngs() {
        return ings;
    }

    public void setIngs(List<IngredienteData> ings) {
        this.ings = ings;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "{" +
                "idcat=" + idcat +
                ", cat='" + cat + '\'' +
                ", ings=" + ings +
                '}';
    }
}
