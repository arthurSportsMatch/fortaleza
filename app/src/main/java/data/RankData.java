package data;

public class RankData {
    private int id = -1;
    private String nome;
    private int rank;
    private int score;
    private String img;
    private int socioTorcedor;
    private String subtitle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getSocioTorcedor() {
        return socioTorcedor;
    }

    public boolean isSocioTorcedor (){
        if(getSocioTorcedor() == 1){
            return true;
        }else{
            return false;
        }
    }

    public void setSocioTorcedor(int socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
    }
}
