package data;

import java.util.List;

public class QuizData {

    //pesquisa
    private int id;
    private String grupo;
    private String pergunta;
    private List<RespostasData> respostas;
    private int tempo;
    private int moedasVitoria;
    private int moedas;
    private int moedasTimeout;
    private int btnNovoDesafio;
    private int respAberta;
    private Integer showResults;

    //quiz
    private String titulo;
    private String descricao;
    private String quantidade;
    private int corretas = 0;
    private String img;

    public QuizData(String grupo, String pergunta, List<RespostasData> respostas) {
        this.pergunta = pergunta;
        this.respostas = respostas;
        this.grupo = grupo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public List<RespostasData> getRespostas() {
        return respostas;
    }

    public void setRespostas(List<RespostasData> respostas) {
        this.respostas = respostas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public int getMoedasVitoria() {
        return moedasVitoria;
    }

    public void setMoedasVitoria(int moedasVitoria) {
        this.moedasVitoria = moedasVitoria;
    }

    public int getMoedasTimeout() {
        return moedasTimeout;
    }

    public void setMoedasTimeout(int moedasTimeout) {
        this.moedasTimeout = moedasTimeout;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    @Override
    public String toString() {
        return "QuizData{" +
                "id=" + id +
                ", grupo='" + grupo + '\'' +
                ", pergunta='" + pergunta + '\'' +
                ", respostas=" + respostas +
                ", tempo=" + tempo +
                ", moedasVitoria=" + moedasVitoria +
                ", moedasTimeout=" + moedasTimeout +
                ", titulo='" + titulo + '\'' +
                ", descricao='" + descricao + '\'' +
                ", quantidade='" + quantidade + '\'' +
                ", img='" + img + '\'' +
                '}';
    }

    public int getBtnNovoDesafio() {
        return btnNovoDesafio;
    }

    public void setBtnNovoDesafio(int btnNovoDesafio) {
        this.btnNovoDesafio = btnNovoDesafio;
    }

    public int getRespAberta() {
        return respAberta;
    }

    public void setRespAberta(int respAberta) {
        this.respAberta = respAberta;
    }

    public int getMoedas() {
        return moedas;
    }

    public void setMoedas(int moedas) {
        this.moedas = moedas;
    }

    public Integer getShowResults() {
        return showResults;
    }

    public void setShowResults(Integer showResults) {
        this.showResults = showResults;
    }

    public int getCorretas() {
        return corretas;
    }

    public void setCorretas(int corretas) {
        this.corretas = corretas;
    }
}
