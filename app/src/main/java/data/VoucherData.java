package data;

import java.util.List;

public class VoucherData {
    private String title;
    private String url;
    private String terms;
    private int score;
    private int reset;
    private String img;
    private int cols;
    private List<VoucherItemData> items;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getReset() {
        return reset;
    }

    public void setReset(int reset) {
        this.reset = reset;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public List<VoucherItemData> getItems() {
        return items;
    }

    public void setItems(List<VoucherItemData> items) {
        this.items = items;
    }
}
