package data;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import utils.UTILS;

/**
 * Created by rafa on 10.01.18.
 */

public class SearchData {

    private static final String TAG  = "SearchData";
    private int idTipo = 0;
    private int tot = 0;

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getTot() {
        return tot;
    }

    public void setTot(int tot) {
        this.tot = tot;
    }

}