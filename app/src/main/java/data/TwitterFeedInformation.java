package data;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafa on 09.02.18.
 */

public class TwitterFeedInformation {

    private static String bearerToken = "";

    private static String querySup = "";
    private static String queryInf = "";
    private static String queryNext = "";

    private static List<TweetResultData> tweetsSup = new ArrayList<>();
    private static List<TweetResultData> tweetsInf = new ArrayList<>();

    public static String getBearerToken() {
        return bearerToken;
    }

    public static void setBearerToken(String bearerToken) {
        TwitterFeedInformation.bearerToken = bearerToken;
    }

    public static String getQuerySup() {
        return querySup;
    }

    public static void setQuerySup(String querySup) {
        TwitterFeedInformation.querySup = querySup;
    }

    public static String getQueryInf() {
        return queryInf;
    }

    public static void setQueryInf(String queryInf) {
        TwitterFeedInformation.queryInf = queryInf;
    }

    public static List<TweetResultData> getTweetsSup() {
        return tweetsSup;
    }

    public static void setTweetsSup(List<TweetResultData> data) {
        tweetsSup = data;
    }

    public static List<TweetResultData> getTweetsInf() {
        return tweetsInf;
    }

    public static void setTweetsInf(List<TweetResultData> data) {
        tweetsInf = data;
    }

    public static boolean HasFeedSup(){
        return (!getTweetsSup().isEmpty());
    }

    public static boolean HasFeedInf(){
        return (!getTweetsInf().isEmpty());
    }

    public static String getQueryNext() {
        return queryNext;
    }

    public static void setQueryNext(String queryNext) {
        TwitterFeedInformation.queryNext = queryNext;
    }
}