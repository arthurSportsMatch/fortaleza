package data;

import androidx.annotation.NonNull;

public class CargoData {
    private int id;
    private String cargo;

    public CargoData(int id, String cargo) {
        this.id = id;
        this.cargo = cargo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @NonNull
    @Override
    public String toString() {
        return getCargo();
    }
}
