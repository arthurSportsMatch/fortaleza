package data;

import java.util.List;

public class ServerCustomData {
    private int serverId;
    private int serverVersion;
    private int idUser;
    private List<ServerData> api;
    private List<ServerData> media;

    public int getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(int serverVersion) {
        this.serverVersion = serverVersion;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public List<ServerData> getApi() {
        return api;
    }

    public void setApi(List<ServerData> api) {
        this.api = api;
    }

    public List<ServerData> getMedia() {
        return media;
    }

    public void setMedia(List<ServerData> media) {
        this.media = media;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
        reselectServer(serverId);
    }

    void reselectServer (int id){
        for(ServerData server : this.api){
            if(server.getId() == id){
                server.setSelected(1);
            }else{
                server.setSelected(0);
            }
        }
    }

    @Override
    public String toString() {
        return "{" +
                "serverId=" + serverId +
                ", serverVersion=" + serverVersion +
                ", idUser=" + idUser +
                ", api=" + api +
                ", media=" + media +
                '}';
    }
}
