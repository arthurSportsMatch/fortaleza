package data;

public class IngredienteData implements Cloneable{
    private int id;
    private String ing;
    private float prc;
    private float qtd;
    private float total;

    public IngredienteData(int id, String ing, float prc, float qtd, float total) {
        this.id = id;
        this.ing = ing;
        this.prc = prc;
        this.qtd = qtd;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIng() {
        return ing;
    }

    public void setIng(String ing) {
        this.ing = ing;
    }

    public float getPrc() {
        return prc;
    }

    public void setPrc(float prc) {
        this.prc = prc;
    }

    public float getQtd() {
        return qtd;
    }

    public void setQtd(float qtd) {
        this.qtd = qtd;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", ing='" + ing + '\'' +
                ", prc=" + prc +
                ", qtd=" + qtd +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
