package data;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import utils.UTILS;

/**
 * Created by rafael.verginelli on 7/12/16.
 */
public class UserData {

    private int id = 0;
    private String nome = "";
    private String email = "";
    private int moedas;
    private int pontos;
    private int sessionId = 0;
    private String token = "";
    private String celular = "";
    private String cpf = "";
    private String img = "";
    private String sexo = "M";
    private int notifications = 0;
    private int badges = 0;
    private double rv = 0.0d;
    private String dn = "";
    private int socioTorcedor = 0;
    private int socioBeneficio = 0;
    private String socioStatus = "";
    private String Suser = "";
    private String Stoken = "";
    private String Spass = "";
    private String gcm = "";
    private String phone = "";


    public String getDn() {
        return dn;
    }

    public void setDn(String birthDate) {
        String formatted = birthDate;
        if (birthDate.contains("-")) {
            String[] unformatted = birthDate.split("-");
            if (unformatted.length == 3) {
                formatted = unformatted[2] + "/" + unformatted[1] + "/" + unformatted[0];
            }
        }

        this.dn = formatted;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return this.nome;
    }

    public void setNickname(String nickname) {
        this.nome = nickname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String phone) {
        this.celular = phone;
    }

    public String getImg() {
        return this.img;
    }

    public void setImg(String image) {
        if (image.contains("http:")) {
            image = image.replace("http:", "https:");
        }
        this.img = image;
    }

    public String getGcm() {
        return gcm;
    }
    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getBadges() {
        return badges;
    }

    public void setBadges(int badges) {
        this.badges = badges;
    }

    public double getRv() {
        return rv;
    }

    public void setRv(double rv) {
        this.rv = rv;
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        try {
            json.put("id", getId());
            json.put("nome", getNickname());
            json.put("cpf", getCpf());
            json.put("email", getEmail());
            json.put("pontos", getPontos());
            json.put("moedas", getMoedas());
            json.put("celular", getCelular());
            json.put("img", getImg());
            json.put("dn", getDn());
            json.put("notifications", getNotifications());
            json.put("sexo", getSexo());
            json.put("sessionId", getSessionId());
            json.put("badges", getBadges());
            json.put("Suser", getSuser());
            json.put("Stoken", getStoken());
            json.put("Spass", getSpass());
            json.put("socioTorcedor", getSocioTorcedor());
            json.put("socioBeneficio", getSocioBeneficio());
            json.put("socioStatus", getSocioStatus());
            json.put("phone", getPhone());
            json.put("rv", getRv());
        } catch (JSONException e) {
            UTILS.DebugLog("UserData", e);
        }

        return json;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "id=" + id +
                ", nickname='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", token='" + token + '\'' +
                ", phone='" + celular + '\'' +
                ", Moedas=" + moedas +
                ", image='" + img + '\'' +
                ", gcm='" + gcm + '\'' +
                ", birthDate='" + dn + '\'' +
                ", sexo='" + sexo + '\'' +
                ", badges=" + badges +
                ", rv=" + rv +
                '}';
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getMoedas() {
        return moedas;
    }

    public void setMoedas(int moedas) {
        this.moedas = moedas;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public int getSocioTorcedor() {
        return socioTorcedor;
    }

    public boolean isSocioTorcedor (){
        if(getSocioTorcedor() == 1){
            return true;
        }else{
            return false;
        }
    }

    public void setSocioTorcedor(int socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
    }

    public int getNotifications() {
        return notifications;
    }

    public void setNotifications(int notifications) {
        this.notifications = notifications;
    }

    public String getSuser() {
        return Suser;
    }

    public void setSuser(String suser) {
        Suser = suser;
    }

    public String getStoken() {
        return Stoken;
    }

    public void setStoken(String stoken) {
        Stoken = stoken;
    }

    public String getSpass() {
        return Spass;
    }

    public void setSpass(String spass) {
        Spass = spass;
    }

    public int getSocioBeneficio() {
        return socioBeneficio;
    }

    public void setSocioBeneficio(int socioBeneficio) {
        this.socioBeneficio = socioBeneficio;
    }

    public boolean isSocioBeneficio (){
        if(getSocioBeneficio() == 1){
            return true;
        }else{
            return false;
        }
    }

    public String getSocioStatus() {
        return socioStatus;
    }

    public void setSocioStatus(String socioStatus) {
        this.socioStatus = socioStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
