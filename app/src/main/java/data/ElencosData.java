package data;

import java.util.List;

public class ElencosData implements Comparable<ElencosData>{

    private int pos;
    private String type;
    private List<ElencoData> elenco;

    public ElencosData(int pos, String type, List<ElencoData> elenco) {
        this.pos = pos;
        this.type = type;
        this.elenco = elenco;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ElencoData> getElenco() {
        return elenco;
    }

    public void setElenco(List<ElencoData> elenco) {
        this.elenco = elenco;
    }

    @Override
    public String toString() {
        return "ElencosData{" +
                "type='" + type + '\'' +
                ", elenco=" + elenco +
                '}';
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public int compareTo(ElencosData o) {
        return this.getPos() - o.getPos();
    }
}
