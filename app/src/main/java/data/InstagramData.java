package data;

public class InstagramData {

    private String id;
    private String display_url;
    private String story_cta_url;
    private String video_src;

    public InstagramData(int id, String display_url, String story_cta_url, String video_src) {
        //this.id = id;
        this.display_url = display_url;
        this.story_cta_url = story_cta_url;
        this.video_src = video_src;
    }

    public InstagramData(int id, String display_url, String story_cta_url) {
        //this.id = id;
        this.display_url = display_url;
        this.story_cta_url = story_cta_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplay_url() {
        return display_url;
    }

    public void setDisplay_url(String display_url) {
        this.display_url = display_url;
    }

    public String getStory_cta_url() {
        return story_cta_url;
    }

    public void setStory_cta_url(String story_cta_url) {
        this.story_cta_url = story_cta_url;
    }

    public String getVideo_src() {
        return video_src;
    }

    public void setVideo_src(String video_src) {
        this.video_src = video_src;
    }
}
