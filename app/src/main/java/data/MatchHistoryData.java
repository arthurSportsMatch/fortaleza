package data;

import java.util.List;

public class MatchHistoryData {

    private int id;
    private String subtit;
    private String title;

    //Club 1
    private String clube1;
    private String clubeImg1;
    private String gols1;
    private String penC1;

    //Clube 2
    private String clube2;
    private String clubeImg2;
    private String gols2;
    private String penC2;

    private ListGolsData listGols;
    private List<MatchHistoryDetailsData> stats;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubtit() {
        return subtit;
    }

    public void setSubtit(String subtit) {
        this.subtit = subtit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClube1() {
        return clube1;
    }

    public void setClube1(String clube1) {
        this.clube1 = clube1;
    }

    public String getClubeImg1() {
        return clubeImg1;
    }

    public void setClubeImg1(String clubeImg1) {
        this.clubeImg1 = clubeImg1;
    }

    public String getGols1() {
        return gols1;
    }

    public void setGols1(String gols1) {
        this.gols1 = gols1;
    }

    public String getPenC1() {
        return penC1;
    }

    public void setPenC1(String penC1) {
        this.penC1 = penC1;
    }

    public String getClube2() {
        return clube2;
    }

    public void setClube2(String clube2) {
        this.clube2 = clube2;
    }

    public String getClubeImg2() {
        return clubeImg2;
    }

    public void setClubeImg2(String clubeImg2) {
        this.clubeImg2 = clubeImg2;
    }

    public String getGols2() {
        return gols2;
    }

    public void setGols2(String gols2) {
        this.gols2 = gols2;
    }

    public String getPenC2() {
        return penC2;
    }

    public void setPenC2(String penC2) {
        this.penC2 = penC2;
    }

    public ListGolsData getListGols() {
        return listGols;
    }

    public void setListGols(ListGolsData listGols) {
        this.listGols = listGols;
    }

    public List<MatchHistoryDetailsData> getStats() {
        return stats;
    }

    public void setStats(List<MatchHistoryDetailsData> stats) {
        this.stats = stats;
    }
}
