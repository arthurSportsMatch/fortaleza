package data;

import java.util.List;

public class CalculadoraData implements Cloneable{
    private int porc;
    private String tit;
    private List<RecipeCategoriasData> categorias;

    public CalculadoraData(int porc, String tit, List<RecipeCategoriasData> categorias) {
        this.porc = porc;
        this.tit = tit;
        this.categorias = categorias;
    }

    public CalculadoraData() {
    }


    public int getPorc() {
        return porc;
    }

    public void setPorc(int porc) {
        this.porc = porc;
    }

    public String getTit() {
        return tit;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public List<RecipeCategoriasData> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<RecipeCategoriasData> categorias) {
        this.categorias = categorias;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "{" +
                "porc=" + porc +
                ", tit='" + tit + '\'' +
                ", categorias=" + categorias +
                '}';
    }
}
