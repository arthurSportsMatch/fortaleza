package data;

public class CheckinSetoresData {
    private int id;
    private String nome;
    private float valor;
    private String marcar_assento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getMarcar_assento() {
        return marcar_assento;
    }

    public void setMarcar_assento(String marcar_assento) {
        this.marcar_assento = marcar_assento;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", marcar_assento='" + marcar_assento + '\'' +
                '}';
    }

}
