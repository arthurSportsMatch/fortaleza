package data;

public class LastCheckingData {

    private int id;
    private CheckingDadosData dados;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CheckingDadosData getDados() {
        return dados;
    }

    public void setDados(CheckingDadosData dados) {
        this.dados = dados;
    }
}
