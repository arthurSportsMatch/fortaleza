package data;

import java.util.ArrayList;
import java.util.List;

public class PartidasData {

    private List<PartidaData> partidas;

    public List<PartidaData> getPartidas() {
        return partidas;
    }

    public void setPartidas(List<PartidaData> partidas) {
        this.partidas = partidas;
    }

    public void addPartida (PartidaData partida){
        if(partidas != null){
            partidas.add(partida);
        }else{
            partidas = new ArrayList<>();
            partidas.add(partida);
        }
    }
}
