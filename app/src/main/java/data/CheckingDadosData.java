package data;

import java.util.List;

public class CheckingDadosData {

    private String setor;
    private String local;
    private List<CheckinData> checkins;

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public List<CheckinData> getCheckins() {
        return checkins;
    }

    public void setCheckins(List<CheckinData> checkins) {
        this.checkins = checkins;
    }
}
