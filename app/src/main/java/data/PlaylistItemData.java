package data;

public class PlaylistItemData {
    private int id;
    private String titulo;
    private String dataHora;
    private String audio;
    private String img;
    private String duracao;
    private String lyrics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDuracao() {
        return duracao;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    @Override
    public String toString() {
        return "PlaylistItemData{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", dataHora='" + dataHora + '\'' +
                ", audio='" + audio + '\'' +
                ", img='" + img + '\'' +
                ", duracao='" + duracao + '\'' +
                ", lyrics='" + lyrics + '\'' +
                '}';
    }
}
