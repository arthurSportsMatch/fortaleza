package data;

import java.util.Date;

public class NotificationData {

    private static final String TAG = "NotificationData";

    private int id = -1;
    private int idPush;
    private int tipo = -1;
    private int v = -1;
    private String msg = "";
    private String titulo = "";
    private String data = "";
    private int clicou;
    private int visualizou;
    private Date dt = new Date();

    public NotificationData(int id, int idPush, int tipo) {
        this.id = id;
        this.idPush = idPush;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public int getIdPush() {
        return idPush;
    }

    public void setIdPush(int idPush) {
        this.idPush = idPush;
    }

    public int getClicou() {
        return clicou;
    }

    public void setClicou(int clicou) {
        this.clicou = clicou;
    }

    public boolean isClicou (){
        if(this.visualizou == 1){
            return true;
        }else{
            return false;
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getVisualizou() {
        return visualizou;
    }

    public void setVisualizou(int visualizou) {
        this.visualizou = visualizou;
    }
}
