package data;

public class ProdutoFData {

    private int id;
    private int idCategoria;
    private String nome;
    private String img;
    private String descricao;
    private String url;
    private int embed = 0;
    private float valor = 0;
    private float valorST = 0;
    private float valorParcelado = 0;
    private int numeroParcelas = 0;

    public ProdutoFData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getValorST() {
        return valorST;
    }

    public void setValorST(float valorST) {
        this.valorST = valorST;
    }

    public float getValorParcelado() {
        return valorParcelado;
    }

    public void setValorParcelado(float valorParcelado) {
        this.valorParcelado = valorParcelado;
    }

    public int getNumeroParcelas() {
        return numeroParcelas;
    }

    public void setNumeroParcelas(int numeroParcelas) {
        this.numeroParcelas = numeroParcelas;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getEmbed() {
        return embed;
    }

    public void setEmbed(int embed) {
        this.embed = embed;
    }
}
