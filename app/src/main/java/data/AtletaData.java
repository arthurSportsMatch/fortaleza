package data;

public class AtletaData {
    private int id;
    private String nome;
    private String nomeCompleto;
    private String pos;
    private Integer camisa;
    private String dataNascimento;
    private String local;
    private int gols = 0;
    private String img;
    private String estreia;
    private int flag;
    private int madeCotia;
    private int assistencias = 0;
    private int golsProduzidos = 0;
    private int cVermelhos = 0;
    private int cAmarelos = 0;
    private int jogos = 0;
    private String estreiaDesc;





    private Integer num;
    private String name;
    private String completedName;
    private String peso;
    private String date;
    private String naturalidade;

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompletedName() {
        return completedName;
    }

    public void setCompletedName(String completedName) {
        this.completedName = completedName;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getNaturalidade() {
        return naturalidade;
    }

    public void setNaturalidade(String naturalidade) {
        this.naturalidade = naturalidade;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public Integer getCamisa() {
        return camisa;
    }

    public void setCamisa(Integer camisa) {
        this.camisa = camisa;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public int getGols() {
        return gols;
    }

    public void setGols(int gols) {
        this.gols = gols;
    }

    public String getEstreia() {
        return estreia;
    }

    public void setEstreia(String estreia) {
        this.estreia = estreia;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getMadeCotia() {
        return madeCotia;
    }

    public void setMadeCotia(int madeCotia) {
        this.madeCotia = madeCotia;
    }

    public String getEstreiaDesc() {
        return estreiaDesc;
    }

    public void setEstreiaDesc(String estreiaDesc) {
        this.estreiaDesc = estreiaDesc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AtletaData{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }

    public int getAssistencias() {
        return assistencias;
    }

    public void setAssistencias(int assistencias) {
        this.assistencias = assistencias;
    }

    public int getGolsProduzidos() {
        return golsProduzidos;
    }

    public void setGolsProduzidos(int golsProduzidos) {
        this.golsProduzidos = golsProduzidos;
    }

    public int getcVermelhos() {
        return cVermelhos;
    }

    public void setcVermelhos(int cVermelhos) {
        this.cVermelhos = cVermelhos;
    }

    public int getcAmarelos() {
        return cAmarelos;
    }

    public void setcAmarelos(int cAmarelos) {
        this.cAmarelos = cAmarelos;
    }

    public int getJogos() {
        return jogos;
    }

    public void setJogos(int jogos) {
        this.jogos = jogos;
    }
}
