package data;

public class ElencoData {

    private String id;
    private String player;
    private String img;
    private String nome;
    private int pos;
    private int camisa;

    public ElencoData(String id, String player) {
        this.id = id;
        this.player = player;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getCamisa() {
        return camisa;
    }

    public void setCamisa(int camisa) {
        this.camisa = camisa;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "ElencoData{" +
                "id='" + id + '\'' +
                ", player='" + player + '\'' +
                ", img='" + img + '\'' +
                ", nome='" + nome + '\'' +
                ", pos=" + pos +
                ", camisa=" + camisa +
                '}';
    }
}
