package data;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sportm.fortaleza.R;

import java.util.List;

public class ReceitaIngredientesAdapter extends ArrayAdapter<IngredienteData> {

    List<IngredienteData> objects;
    List<IngredienteData> backup;
    Interaction listener;
    ViewHolder mHolder;

    public ReceitaIngredientesAdapter(@NonNull Context context, int resource, @NonNull List<IngredienteData> objects, Interaction listener) {
        super(context, resource, objects);
        this.objects = objects;
        this.listener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_calculadora_valores, parent, false);
        }

        final IngredienteData item = objects.get(position);

        //Atualizar
        mHolder = new ViewHolder(convertView);
        mHolder.ingrediente.setText(item.getIng());
        mHolder.preco.setText("Preço por Kg: R$ " + item.getPrc());
        mHolder.peso.setText(item.getQtd() + " g");

        //Preco Un
        item.setTotal(calculoUni(item.getQtd(), item.getPrc()));
        String pressUn = String.format("%.2f" , item.getTotal());
        mHolder.precomin.setText("R$ " +  pressUn);

        //Listener do peso
        mHolder.peso.addTextChangedListener(new TextWatcher() {
            float beforeNumber;
            boolean textChanged = false;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //mHolder.peso.setText("");
                if(!s.toString().isEmpty()){
                    beforeNumber = Float.valueOf(s.toString().replaceAll("[^0-9]", ""));
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(textChanged){
                    String value = s.toString();
                    value = value.replace("g", "");
                    value = value.replace(" ", "");
                    value = value.replaceAll("^[0-9][,.][0-9]?", "");

                    if(!value.isEmpty()) {
                        float number = Float.valueOf(value);
                        //Retorna o ingredientData
                        item.setQtd(number);
                        listener.UpdateValues(item, position);
                    }
                }
            }
        });

        //Sou o ultimo a ser criado
        if(position >= (getCount() - 1)){
            listener.CreatedAll();
            Log.d("Backup-created-0", "position: " + position + " size: " + getCount());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    Handler handler = new Handler();

    Runnable runnable = new Runnable(){
        public void run() {
            notifyDataSetChanged();
        }
    };

    public interface Interaction {
        void CreatedAll ();
        void UpdateValues (IngredienteData data, int position);
        void OnClick (int position);
    }

    float calculoUni (float quant, float prec){
        float realQuant = quant / 1000;
        float result = prec *    realQuant;
        return result;
    }

    class ViewHolder {
        TextView ingrediente;
        TextView preco;

        EditText peso;
        TextView precomin;

        public ViewHolder(View view){
            ingrediente = view.findViewById(R.id.txt_ingrediente);
            preco = view.findViewById(R.id.txt_preco_holder);

            peso = view.findViewById(R.id.txt_peso);
            precomin = view.findViewById(R.id.txt_preco_min);
        }
    }
}
