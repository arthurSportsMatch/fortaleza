package data;

public class PalpiteDetailData {

    private String pergunta;
    private Integer moedas;
    private Integer certo;
    private String respostaUser1;
    private String respostaUser2;
    private String respostaCorreta1;
    private String respostaCorreta2;

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public Integer getMoedas() {
        return moedas;
    }

    public void setMoedas(Integer moedas) {
        this.moedas = moedas;
    }

    public Integer getCerto() {
        return certo;
    }

    public void setCerto(Integer certo) {
        this.certo = certo;
    }

    public String getRespostaUser1() {
        return respostaUser1;
    }

    public void setRespostaUser1(String respostaUser1) {
        this.respostaUser1 = respostaUser1;
    }

    public String getRespostaUser2() {
        return respostaUser2;
    }

    public void setRespostaUser2(String respostaUser2) {
        this.respostaUser2 = respostaUser2;
    }

    public String getRespostaCorreta1() {
        return respostaCorreta1;
    }

    public void setRespostaCorreta1(String respostaCorreta1) {
        this.respostaCorreta1 = respostaCorreta1;
    }

    public String getRespostaCorreta2() {
        return respostaCorreta2;
    }

    public void setRespostaCorreta2(String respostaCorreta2) {
        this.respostaCorreta2 = respostaCorreta2;
    }
}
