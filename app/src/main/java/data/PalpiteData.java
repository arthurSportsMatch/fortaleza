package data;

import java.util.List;

public class PalpiteData {
    private int id;
    private String expire;
    private String dataHoraJogo;
    private String campeonato;
    private String rodada;
    private String imgClube1;
    private String clube1;
    private String imgClube2;
    private String clube2;
    private int coins;
    private String more;
    private List<PalpitePerguntaData> perguntas;

    private String time1;
    private String time1img;
    private String penC1;
    private String golsClube1;

    private String time2;
    private String time2img;
    private String penC2;
    private String golsClube2;

    private String acertos;
    private String moedas;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getDataHoraJogo() {
        return dataHoraJogo;
    }

    public void setDataHoraJogo(String dataHoraJogo) {
        this.dataHoraJogo = dataHoraJogo;
    }

    public String getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(String campeonato) {
        this.campeonato = campeonato;
    }

    public String getRodada() {
        return rodada;
    }

    public void setRodada(String rodada) {
        this.rodada = rodada;
    }

    public String getImgClube1() {
        return imgClube1;
    }

    public void setImgClube1(String imgClube1) {
        this.imgClube1 = imgClube1;
    }

    public String getClube1() {
        return clube1;
    }

    public void setClube1(String clube1) {
        this.clube1 = clube1;
    }

    public String getImgClube2() {
        return imgClube2;
    }

    public void setImgClube2(String imgClube2) {
        this.imgClube2 = imgClube2;
    }

    public String getClube2() {
        return clube2;
    }

    public void setClube2(String clube2) {
        this.clube2 = clube2;
    }

    public List<PalpitePerguntaData> getPerguntas() {
        return perguntas;
    }

    public void setPerguntas(List<PalpitePerguntaData> perguntas) {
        this.perguntas = perguntas;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime1img() {
        return time1img;
    }

    public void setTime1img(String time1img) {
        this.time1img = time1img;
    }

    public String getPenC1() {
        return penC1;
    }

    public void setPenC1(String penC1) {
        this.penC1 = penC1;
    }

    public String getGolsClube1() {
        return golsClube1;
    }

    public void setGolsClube1(String golsClube1) {
        this.golsClube1 = golsClube1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime2img() {
        return time2img;
    }

    public void setTime2img(String time2img) {
        this.time2img = time2img;
    }

    public String getPenC2() {
        return penC2;
    }

    public void setPenC2(String penC2) {
        this.penC2 = penC2;
    }

    public String getGolsClube2() {
        return golsClube2;
    }

    public void setGolsClube2(String golsClube2) {
        this.golsClube2 = golsClube2;
    }

    public String getAcertos() {
        return acertos;
    }

    public void setAcertos(String acertos) {
        this.acertos = acertos;
    }

    public String getMoedas() {
        return moedas;
    }

    public void setMoedas(String moedas) {
        this.moedas = moedas;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
