package informations;


import android.content.Context;

import data.ServerCustomData;
import data.UserData;
import utils.CONSTANTS;
import utils.SecurePreferences;
import utils.UTILS;

import static repository.MainRepository.SHARED_KEY_USER;

/**
 * Created by rafael.verginelli on 7/12/16.
 */
public class UserInformation {

    private static final String TAG = "UserInformation";
    public static void setUserData(UserData userData) {
        UserInformation.userData = userData;
    }

    private static UserData userData;
    public static UserData getUserData() {
        if(userData == null){
            userData = new UserData();
        }
        return userData;
    }

    private static boolean blockCheckIn = false;
    private static ServerCustomData serverData;

    private static int userId;
    private static int sessionId;
    private static int hide = -1;

    public static int getSessionId() {
        return sessionId;
    }

    public static void setSessionId(int sessionId) {
        UserInformation.sessionId = sessionId;
    }

    public static int getUserId() {
        return userId;
    }

    public static void setUserId(int userId) {
        UserInformation.userId = userId;
    }

    public static void ClearUserData(Context context){
        userData = new UserData();
        userId = -1;
        hide = -1;
        SecurePreferences prefs = new SecurePreferences(context, CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        prefs.clear();
    }

    public static ServerCustomData getServerData() {
        if(serverData == null){
            serverData = new ServerCustomData();
        }
        return serverData;
    }

    public static void setServerData(ServerCustomData serverData) {
        UserInformation.serverData = serverData;
    }

    public static boolean isBlockCheckIn() {
        return blockCheckIn;
    }

    public static void setBlockCheckIn(boolean blockCheckIn) {
        UserInformation.blockCheckIn = blockCheckIn;
    }

    public static int getHide() {
        return hide;
    }

    public static void setHide(int hide) {
        UserInformation.hide = hide;
    }


}