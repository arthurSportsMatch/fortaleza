package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.NotificationAdapter;
import data.NotificationData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MasterBakersNotificationManager;
import utils.TRACKING;
import utils.UTILS;

public class NotificationActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    NotificationAdapter adapter;
    boolean hasData = false;
    boolean seeAll = false;
    int page = 1;

    private static final int OP_ID_NOTIFICATION                 = 0;
    private static final int OP_ID_SET_ALL                      = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mHolder = new ViewHolder();

        initAction();
        getNotification();
    }

    void initAction (){
        mHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setViewVisibility();
            }
        });

        mHolder.marcarTodas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMarkAll();
                setViewVisibility();
            }
        });
    }

    void setViewVisibility (){
        if(mHolder.marcarTodas.getVisibility() == View.VISIBLE){
            mHolder.marcarTodas.setVisibility(View.GONE);
            return;
        }else{
            mHolder.marcarTodas.setVisibility(View.VISIBLE);
            return;
        }
    }

    void setMarkAll (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTIFICATION_MARK_ALL, OP_ID_SET_ALL, this,TRACKING.TRACKING_NOTIFICACAO_SELECIONAR ).execute();
    }

    void getNotification (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTIFICATION_HISTORY, OP_ID_NOTIFICATION, this, TRACKING.TRACKING_NOTIFICACAO_RELOAD).execute(params);
    }

    void initNotification (List<NotificationData> data){
        if(adapter == null){
            adapter = new NotificationAdapter(activity, data, new NotificationAdapter.Listener() {
                @Override
                public void isPressed(NotificationData data) {
                    SetNotification(data);
                    return;
                }

                @Override
                public void onBottom() {
                    GetByPagination();
                }
            });

            mHolder.recyclerView.setAdapter(adapter);
            mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        }else{
            if(data != null){
                if(seeAll){
                    adapter.setData(data);
                    seeAll = false;
                }else{
                    adapter.addData(data);
                }

            }
        }
    }

    void GetByPagination (){
        if(hasData){
            page ++;
            getNotification();
            hasData = false;
        }
    }

    void SetNotification (NotificationData data){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idPush", data.getIdPush());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_NOTIFICATION, 999, emptyAsync, TRACKING.TRACKING_NOTIFICACAO_SELECIONAR, data.getIdPush()).execute(params);

        switch (data.getTipo()){
            case MasterBakersNotificationManager.PUSH_ID_DEFAUT: {
                //
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_QUIZ: {
                Intent broadcastIntent = new Intent(MainMenuActivity.ON_PUSH);
                Bundle pushBundle = new Bundle();
                pushBundle.putInt("t", data.getTipo());
                pushBundle.putInt("idPush", data.getIdPush());
                pushBundle.putInt("idQuiz", data.getId());
                Bundle _b = new Bundle();
                _b.putBundle("pushBundle", pushBundle);
                _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
                setResult(_b);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PESQUISA_ESPECIFICA: {
                Intent broadcastIntent = new Intent(MainMenuActivity.ON_PUSH);
                Bundle pushBundle = new Bundle();
                pushBundle.putInt("t", data.getTipo());
                pushBundle.putInt("idPush", data.getIdPush());
                pushBundle.putInt("idPesquisa", data.getId());
                Bundle _b = new Bundle();
                _b.putBundle("pushBundle", pushBundle);
                _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
                setResult(_b);
                //broadcastIntent.putExtras(_b);
                //sendBroadcast(broadcastIntent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PESQUISA:
            case MasterBakersNotificationManager.PUSH_ID_PALPITES:
            case MasterBakersNotificationManager.PUSH_ID_AGENDA: {
                Intent broadcastIntent = new Intent(MainMenuActivity.ON_PUSH);
                Bundle pushBundle = new Bundle();
                pushBundle.putInt("t", data.getTipo());
                pushBundle.putInt("idPush", data.getIdPush());

                Bundle _b = new Bundle();
                _b.putBundle("pushBundle", pushBundle);
                _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
                setResult(_b);
                //broadcastIntent.putExtras(_b);
                //sendBroadcast(broadcastIntent);
                //finish();
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_NOTICIA: {
                Intent intent = new Intent(activity, NoticiaActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PLAYERPROFILE: {
                Intent intent = new Intent(activity, PerfilAtletaActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_MEMBERSHIP: {
                Intent intent = new Intent(activity, SocioTorcedorActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_CAMPINHO: {
                Intent intent = new Intent(activity, EscalarTimeActivity.class);
                intent.putExtra("esquema", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_LOJA: {
                Intent intent = new Intent(activity, LojaActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_AUDIO: {
                Intent intent = new Intent(activity, PlayerActivity.class);
                intent.putExtra("push", true);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_ALTERAR_DADOS: {
                Intent intent = new Intent(activity, ConfigurationActivity.class);
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_RANK: {
                Intent intent = new Intent(activity, RankingActivity.class);
                startActivity(intent);
            }
            break;
            case MasterBakersNotificationManager.PUSH_ID_DETALHES_JOGO:{
                Intent intent = new Intent(this, MatchHistoryActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_PRODUTO:{
                Intent intent = new Intent(this, ItemLojaActivity.class);
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
            break;

            case MasterBakersNotificationManager.PUSH_ID_AGENDA_JOGO:{
                Bundle pushBundle = new Bundle();
                pushBundle.putInt("t", data.getTipo());
                pushBundle.putInt("idPush", data.getIdPush());
                pushBundle.putInt("idAgenda", data.getId());
                Bundle _b = new Bundle();
                _b.putBundle("pushBundle", pushBundle);
                _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
                setResult(_b);
            }
            break;
        }
    }

    void setResult (Bundle result){
        Intent intent = getIntent();
        intent.putExtras(result);
        activity.setResult(RESULT_OK, intent);
        activity.finish();
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });


    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {

        switch (opId){
            case OP_ID_NOTIFICATION :{
                //Noticia
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            NotificationData[] data = gson.fromJson(obj.getString("itens"), NotificationData[].class);

                            if(data != null && data.length > 0){
                                List<NotificationData> notification = new ArrayList<>(Arrays.asList(data));
                                initNotification(notification);
                                hasData = true;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_ID_SET_ALL :{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    seeAll = true;
                    page = 1;
                    Toast.makeText(activity, "Todas notificações marcadas como lidas com sucesso", Toast.LENGTH_LONG).show();
                    getNotification();
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }

    class ViewHolder {
        RecyclerView recyclerView;
        ImageView more;
        TextView marcarTodas;

        public ViewHolder (){
            recyclerView = findViewById(R.id.notification_recycler);
            more = findViewById(R.id.img_marcar_todos);
            marcarTodas = findViewById(R.id.text_marcar_todas);
        }
    }
}
