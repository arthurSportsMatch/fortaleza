package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.VendedorAdapter;
import data.VendedorData;
import utils.AsyncOperation;

public class GetVendedorActivity extends MainActivity {

    VendedorAdapter adapter;
    ViewHolder mHolder;
    int currentPage = 1;
    String search = "";
    boolean isSearching = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_vendedor);

        mHolder = new ViewHolder();
        getVendedores(currentPage);

        mHolder.text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!isSearching){
                    currentPage = 1;
                    search = (String.valueOf(s));
                    adapter = null;
                    mHolder.view.setAdapter(null);
                    getVendedores(currentPage);
                    isSearching = true;
                }
            }
        });
    }

    void getVendedores (int page){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);

        if(!search.isEmpty()){
            params.put("search", search);
        }


        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_VENDEDORES, 1, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId,response);
                }else{
                    this.OnAsyncOperationError(opId,response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        JSONArray obj = response.getJSONArray("Object");
                        VendedorData[] data = gson.fromJson(String.valueOf(obj), VendedorData[].class);
                        List<VendedorData> list = new ArrayList<>(Arrays.asList(data));

                        if(list.size() > 0){
                            if(isSearching){
                                isSearching = false;
                            }

                            if(adapter == null){
                                adapter = new VendedorAdapter(activity, list, new VendedorAdapter.Interaction() {
                                    @Override
                                    public void onClick(int id, String nome) {
                                        passVendedor(id, nome);
                                    }

                                    @Override
                                    public void Botton() {
                                        currentPage ++;
                                        getVendedores(currentPage);
                                    }
                                });
                                mHolder.view.setAdapter(adapter);
                                mHolder.view.setLayoutManager(new LinearLayoutManager(activity,RecyclerView.VERTICAL, false));
                            }else{
                                adapter.addData(list);
                            }

                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute(params);
    }

    void passVendedor (int id, String nome){
        Intent intent = getIntent();
        intent.putExtra("idVendedor", id);
        intent.putExtra("nomeVendedor", nome);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    class ViewHolder {
        EditText text;
        RecyclerView view;

        public ViewHolder (){
            text = findViewById(R.id.edt_vend_search);
            view = findViewById(R.id.list_results_vend);
        }
    }
}
