package com.sportm.fortaleza;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import data.BadgeData;
import data.UserData;
import informations.UserInformation;
import models.MessageModel;
import models.user.UserModel;
import picker.ImageContract;
import picker.ImagePresenter;
import repository.badges.BadgeRepository;
import repository.socio.SocioRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MaskEditUtil;
import utils.TRACKING;
import utils.UTILS;
import vmodels.badges.BadgeViewModel;
import vmodels.socio.SocioViewModel;
import vmodels.user.UserViewModel;


import static fragments.FaleConoscoFragment.REQUEST_GALLERY_PHOTO;
import static fragments.FaleConoscoFragment.REQUEST_TAKE_PHOTO;

public class EditarPerfilActivity extends MainActivity implements ImageContract.View{

    private ViewHolder mHolder;
    private UserViewModel userViewModel;
    private SocioViewModel socioViewModel;
    private ImagePresenter mPresenter;
    private Uri photoURI;

    UserModel newUser;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        mHolder = new ViewHolder();
        mPresenter = new ImagePresenter(this);

        initAction();
    }

    @Override
    public void bindViewModel() {
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = new ViewModelProvider(EditarPerfilActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                uploadUser(user);
            }
        });
        userViewModel.getMessage().observe(this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel message) {
                if(message.getId()!= 12) {
                    showMessage(message.getMsg());
                }
            }
        });
        userViewModel.getUser(false);
        userViewModel.getUser();


        SocioRepository socioRepository = new SocioRepository(activity, prefs);
        socioViewModel = new ViewModelProvider(EditarPerfilActivity.this).get(SocioViewModel.class);
        socioViewModel.setmRepository(socioRepository);
        socioViewModel.getMessage().observe(this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel message) {
                if(message.getId() == 1){
                    String cpf = MaskEditUtil.unmask(mHolder.cpf.getText().toString());
                    userViewModel.getSocio(cpf);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(checkInfos()){
                                changeUser();
                            }
                        }
                    },1500);
                }
            }
        });
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        userViewModel.getUserData().removeObservers(this);
        userViewModel.getMessage().removeObservers(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uploadPhoto(requestCode, resultCode, data);
    }

    void initAction (){
        mHolder.nascimento.addTextChangedListener(MaskEditUtil.mask(mHolder.nascimento, MaskEditUtil.FORMAT_DATE));
        mHolder.telefone.addTextChangedListener(MaskEditUtil.mask(mHolder.telefone, MaskEditUtil.FORMAT_FONE));
        mHolder.cpf.addTextChangedListener(MaskEditUtil.mask(mHolder.cpf, MaskEditUtil.FORMAT_CPF));

        mHolder.changePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_EDITAR_PERFIL_ALTERAR_FOTO).execute();
                selectImage();
            }
        });

        mHolder.upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkCpf()){
                    socioViewModel.getSocioAPI();
                }
            }
        });
    }

    //Mudar usuario
    void changeUser (){
        userViewModel.uploadUser(passUserInfos());
    }

    //Faz update do usuario salvo
    void uploadUser (UserModel user){
        this.newUser = user;

        //Imagem
        UTILS.getImageAt(activity, user.getImg(), mHolder.userImage, new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.ic_placeholderuser)
                .error(R.drawable.ic_placeholderuser));

        if(user.getSexo() != null){
            String sex = user.getSexo();

            if(sex.startsWith("F")){
                mHolder.genero.setSelection(1);
            }else if(sex.startsWith("M")){
                mHolder.genero.setSelection(2);
            }else if(sex.startsWith("O")){
                mHolder.genero.setSelection(3);
            }else{
                mHolder.genero.setSelection(0);
            }
        }

        //Nome;
        if(user.getNome() != null && !user.getNome().isEmpty()) {
            mHolder.nome.setHint(user.getNome());
            mHolder.nomeTOP.setText(user.getNome());
        }

        //Email
        if(user.getEmail() != null && !user.getEmail().isEmpty())
            mHolder.email.setHint(user.getEmail());

        //Data de nascimento
        if(user.getDn() != null && !user.getDn().isEmpty()){
            String nascimento = user.getDn();
            nascimento = UTILS.formatDate(nascimento);
            nascimento = nascimento.replace("-", "/");
            mHolder.nascimento.setHint(nascimento);
        }


        if(user.getCelular() != null && !user.getCelular().isEmpty()){
            String celular = user.getCelular();
            String celularMask = MaskEditUtil.mask(celular, MaskEditUtil.FORMAT_FONE);
            mHolder.telefone.setHint(celularMask);
        }


        if(user.getCpf() != null && !user.getCpf().isEmpty()){
            String cpf = MaskEditUtil.unmask(user.getCpf());
            String finalCpf = cpf.substring(0, 3) + ".***.***-**";
            mHolder.cpf.setHint(finalCpf);
        }
    }

    void uploadPhoto (int requestCode, int resultCode, Intent data){
        boolean resultOK = false;
        boolean isCam = false;
        int cam = 0;
        try {
            cam = data.getExtras().getInt("cam");
        }catch (Exception ignored){}

        if (resultCode == RESULT_OK) {
            //Request de tirar foto
            isCam = true;
            if (requestCode == REQUEST_TAKE_PHOTO) {
                if(photoURI == null){
                    photoURI = data.getData();
                    resultOK = true;
                }else{
                    resultOK = true;
                }

            }
            //Request de pegar foto da galeria
            else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = Objects.requireNonNull(data).getData();
                //String mPhotoPath = getRealPathFromUri(selectedImage);
                isCam = false;

                if(selectedImage != null){
                    try {
                        photoURI = Uri.parse(UTILS.getRealPathFromURI(activity, selectedImage));
                        resultOK = true;
                    }catch (Exception ignored) {
                        try {
                            photoURI = Uri.parse(UTILS.getPath(activity, selectedImage));
                            resultOK = true;
                        }catch (Exception e){
                            Log.d("FragmentController", e.getMessage()+"");
                            Toast.makeText(activity, "Erro ao carregar foto, tente outra!", Toast.LENGTH_LONG).show();
                        }
                    }
                }else{
                    Toast.makeText(activity, "Erro ao carregar foto, tente outra!", Toast.LENGTH_LONG).show();
                }
            }
        }

        if(resultOK)
            userViewModel.uploadPhoto(photoURI, cam, isCam);
    }

    //Verificar se algum item foi preenchido
    boolean checkInfos (){

        if(!mHolder.email.getText().toString().isEmpty()){
            if(UTILS.isValidEmail(mHolder.email.getText().toString())){
                return true;
            }else{
                mHolder.email.setError("Coloque um email valido");
            }
        }

        if(!mHolder.cpf.getText().toString().isEmpty()){
            return true;
        }

        if(!mHolder.nome.getText().toString().isEmpty()){
            return true;
        }

        if(!mHolder.telefone.getText().toString().isEmpty()){
            return true;
        }

        if(!mHolder.nascimento.getText().toString().isEmpty()){
            return true;
        }

//        if(!mHolder.cpf.getText().toString().isEmpty()){
//            return true;
//        }

//        if(mHolder.genero.getSelectedItemPosition() > 0){
//            if(UserInformation.getUserData().getSexo() != null){
//                String sex = UserInformation.getUserData().getSexo();
//                int id = 0;
//                if(sex.startsWith("F")){
//                    id = 1;
//                }else if(sex.startsWith("M")){
//                    id = 2;
//                }else if(sex.startsWith("O")){
//                    id = 3;
//                }
//
//                if(id != mHolder.genero.getSelectedItemPosition()){
//                    return true;
//                }
//            }else{
//                return true;
//            }
//        }

//        if(mHolder.cidade.getSelectedItemPosition() != 0){
//            return true;
//        }

        Toast.makeText(this, "Edite algum item antes", Toast.LENGTH_LONG).show();
        return false;
    }

    boolean checkCpf (){
        if(!mHolder.cpf.getText().toString().isEmpty()){
            return true;
        }

        return false;
    }

    //Pegar o user infos
    UserModel passUserInfos (){

        UserModel user = new UserModel();

        if(!mHolder.email.getText().toString().isEmpty()) {
            if(!mHolder.email.getText().toString().equals(newUser.getEmail())){
                String email = mHolder.email.getText().toString();
                user.setEmail(email);
            }
        }

        if(!mHolder.nome.getText().toString().isEmpty()) {
            if(!mHolder.nome.getText().toString().equals(newUser.getNome())){
                user.setNome(mHolder.nome.getText().toString());
            }
        }

        if(!mHolder.cpf.getText().toString().isEmpty()) {
            if(!mHolder.cpf.getText().toString().equals(newUser.getCpf())){
                user.setCpf(MaskEditUtil.unmask(mHolder.cpf.getText().toString()));
            }
        }

        if(!mHolder.telefone.getText().toString().isEmpty()) {
            if(!mHolder.telefone.getText().toString().equals(newUser.getCelular()) ){
                String celular = mHolder.telefone.getText().toString();
                String celularUnmask = MaskEditUtil.unmask(celular);
                user.setCelular(celularUnmask);
            }
        }

        if(!mHolder.nascimento.getText().toString().isEmpty()){
            if(!mHolder.nascimento.getText().toString().equals(newUser.getDn())){
                user.setDn(UTILS.formatBirthDate(mHolder.nascimento.getText().toString()));
            }
        }

        if(mHolder.genero.getSelectedItemPosition() != 0){
            int id =  mHolder.genero.getSelectedItemPosition();
            String genero = "";
            if(id == 1){
                genero = "F";
            }else if (id == 2){
                genero = "M";
            }else if (id == 3){
                genero = "O";
            }

            if(!newUser.getSexo().startsWith(genero))
                user.setSexo(genero);
        }

        return user;
    }

    private void selectImage() {
        final CharSequence[] items = {"Tirar Foto", "Pegar da Galeria", getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Tirar Foto")) {
                    mPresenter.cameraClick();
                    type = "foto";
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_CAMERA).execute();
                } else if (items[item].equals("Pegar da Galeria")) {
                    mPresenter.ChooseGalleryClick();
                    type = "galeria";
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_BIBLIOTECA).execute();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_EDITAR_PERFIL_ALTERAR_FOTO_CANCELAR).execute();
                }
            }
        });
        builder.show();
    }

    //region Item Picker
    @Override
    public void startCamera(File file) {
        Intent takePictureIntent = new Intent(this, CameraActivity.class);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (file != null) {
                photoURI = Uri.fromFile(file); //FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", file);
                takePictureIntent.setData(photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void chooseGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        activity.startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    @Override
    public boolean checkPermission() {
        for (String mPermission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, mPermission);
            if (result == PackageManager.PERMISSION_DENIED) return false;
        }
        return true;
    }

    @Override
    public void showPermissionDialog() {
        Dexter.withActivity(this).withPermissions(permissions)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (type == "foto") {
                                mPresenter.cameraClick();
                            }else if(type == "galeria"){
                                mPresenter.ChooseGalleryClick();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                showErrorDialog();
            }
        })
                .onSameThread()
                .check();
    }

    @Override
    public File getFilePath() {
        return getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Precisa de permissão");
        builder.setMessage("Dê permissão");
        builder.setPositiveButton("Escolha", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openSettings();
                dialog.cancel();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void showNoSpaceDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Erro");
        builder.setMessage("Sem espaço");
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public int availableDisk() {
        File mFilePath = getFilePath();
        long freeSpace = mFilePath.getFreeSpace();
        return Math.round(freeSpace / 1048576);

    }

    @Override
    public File newFile() {
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTime().getTime();
        String mFileName = timeInMillis + ".jpeg";
        File mFilePath = getFilePath();

        try {
            File newFile = new File(mFilePath.getAbsolutePath(), mFileName);
            newFile.createNewFile();
            return newFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void showErrorDialog() {
        Toast.makeText(getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayImagePreview(String mFilePath) {
        //Glide.with(getContext()).load(mFilePath).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    @Override
    public void displayImagePreview(Uri mFileUri) {
        //Glide.with(getContext()).load(mFileUri).apply(new RequestOptions().centerCrop().placeholder(R.drawable.logo_master_baker)).into(mHolder.previewImage);
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    @Override
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    //endregion

    void createOKScren (){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage("Atualizado com sucesso");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });
        builder.show();
    }

    class ViewHolder {
        TextView changePhoto;
        ImageView userImage;
        TextView nomeTOP;

        EditText nome;
        EditText email;
        EditText nascimento;
        EditText telefone;
        EditText cpf;

        Spinner genero;
        Spinner cidade;

        Button upload;


        public ViewHolder (){
            changePhoto = findViewById(R.id.view_trocar_foto);
            userImage = findViewById(R.id.img_perfil);

            nome = findViewById(R.id.edt_nome);
            nomeTOP = findViewById(R.id.txt_perfil_nome);
            email = findViewById(R.id.edt_email);
            nascimento = findViewById(R.id.edt_nascimento);
            telefone = findViewById(R.id.edt_telefone);
            cpf = findViewById(R.id.edt_cpf);

            genero = findViewById(R.id.edt_genero);
            cidade = findViewById(R.id.edt_cidade);

            upload = findViewById(R.id.btn_concluir);
        }
    }
}
