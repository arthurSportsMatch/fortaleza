package com.sportm.fortaleza;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import adapter.TweetFeedAdapter;
import data.TweetResultData;
import data.TwitterFeedInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import view.DynamicTextView;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class TimelineActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback{

    private static final int OP_ID_TWITEER                      = 1;
    private static final int OP_ID_TWITEER_TOKEN                = 2;
    private static final int OP_ID_TWITEER_QUERRY               = 3;

    TweetFeedAdapter tweetsSupAdapter;

    private boolean isGettingBearerToken = false;
    private boolean isGettingTwitterFeedSup = false;
    private boolean isGettingQuerySup = false;

    ViewHolder mHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        mHolder = new ViewHolder();
        initTwitter();
    }

    //region Twitter


    //Pega o token
    void GetBearerToken(){
        //Se to token for vazio
//        if(!TwitterFeedInformation.getBearerToken().isEmpty()){
//            GetTwitterQuerry();
//            return;
//        }
        if(isGettingBearerToken){
            return;
        }
        isGettingBearerToken = true;

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_BEARER_TOKEN, OP_ID_TWITEER_TOKEN, this).execute();
    }

    //Pega a hashTags
    void GetTwitterQuerry(){
        if(isGettingQuerySup){
            return;
        }
        isGettingQuerySup = true;
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_HASHTAGS_TWITTER_QUERY_SUP, OP_ID_TWITEER_QUERRY, this).execute();
    }

    //Pega os twitts existentes
    void GetTwitterFeed(){
        if(TwitterFeedInformation.getBearerToken().isEmpty()){
            GetBearerToken();
            return;
        }

        if(TwitterFeedInformation.getQuerySup().isEmpty()){
            GetTwitterQuerry();
            return;
        }


        if(isGettingTwitterFeedSup){
            return;
        }
        isGettingTwitterFeedSup = true;

        String query = "";

        try {
//            if(TwitterFeedInformation.getQueryNext() != null){
//                query = TwitterFeedInformation.getQueryNext();
//            }else{
//                query = URLEncoder.encode(TwitterFeedInformation.getQuerySup(), "utf-8");
//            }

            query = URLEncoder.encode(TwitterFeedInformation.getQuerySup(), "utf-8");

        } catch (UnsupportedEncodingException e) {
            UTILS.DebugLog("TwitterError", e);
        }

        Hashtable<String, Object> params = new Hashtable<>();

        params.put("query", query);
        params.put("bearerToken", TwitterFeedInformation.getBearerToken());
        params.put("count", 200);

        Log.d("Async", "Gething" + params);

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_TWEETS, OP_ID_TWITEER, this).execute(params);

        //ToDo use pagination whith next_results
    }

    void initTwitter (){
        TwitterFeedInformation.setBearerToken(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_BEARER_TOKEN,
                ""));

        //Se já tiver as tags
        GetTwitterFeed();
//        if(TwitterFeedInformation.HasFeedSup()) {
//            ConfigureTwitterFeedSup();
//        }
//        else{
//
//        }
    }

    //Configura a lista de twitts
    void ConfigureTwitterFeedSup(){

        List<TweetResultData> list = TwitterFeedInformation.getTweetsSup();
        list = removeDoblesTwitts(list);

        if(list == null || list.isEmpty()){
            ConfigureNoTwitterFeedSup();
            return;
        }

        mHolder.twitterFeedSupView.setVisibility(View.VISIBLE);
        mHolder.txtNoTwitterFeedSup.setVisibility(View.GONE);
        mHolder.frameLoadingTwitterFeedSup.setVisibility(View.GONE);

        Collections.sort(list, new Comparator<TweetResultData>() {
            public int compare(TweetResultData o1, TweetResultData o2) {
                if (o1.getDate() == null || o2.getDate() == null) {
                    return 0;
                }
                return o2.getDate().compareTo(o1.getDate());
            }
        });

        if(!list.get(list.size() - 1).isLastItem()) {
            TweetResultData lastItem = new TweetResultData();
            lastItem.setLastItem(true);
            list.add(list.size(), lastItem);
        }

        tweetsSupAdapter = new TweetFeedAdapter(activity, R.layout.list_item_tweet_carousel, list,290, new TweetFeedAdapter.ITweetSelectable() {
            @Override
            public void OnSelect(TweetResultData item, WebView webView) {

                //String twits = "@" + item.getNameUser() + "@" + item.getId();
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_TWITTEREXPAND).execute();

                webView.loadUrl(webView.getUrl());

                TwitterFeedCardExpandedActivity.setTweet(item);
                Intent intent = new Intent(activity, TwitterFeedCardExpandedActivity.class);
                startActivityForResult(intent, 1);

            }

            @Override
            public void OnMediaClick(TweetResultData item) {
                if(item.isImage()) {
                    Bundle b = new Bundle();
                    b.putInt("contentType", MediaPlayActivity.MEDIA_PLAY_CONTENT_TYPE_IMAGE);
                    b.putString("content", item.getImgContent());
                    Intent newIntent = new Intent(activity, MediaPlayActivity.class);
                    newIntent.putExtras(b);
                    startActivity(newIntent);
                }
            }

            @Override
            public void OnLearnMoreClick(TweetResultData item) {

//                new AsyncOperation(MainMenuActivity.this,
//                        AsyncOperation.TASK_ID_SAVE_TRACKING, OP_SAVE_TRACKING,
//                        MainMenuActivity.this, TRACKING.BTN_ID_HOME_EXPANDIR_TWEET,
//                        "@" + item.getNameUser() + "@" + item.getId()).execute();

                TwitterFeedCardExpandedActivity.setTweet(item);
                Intent intent = new Intent(activity, TwitterFeedCardExpandedActivity.class);
                startActivity(intent);
            }

            @Override
            public void OnLastCardClick() {
            }
        });

        mHolder.twitterFeedSupView.setAdapter(tweetsSupAdapter);
    }

    //Remove tweets duplos
    List<TweetResultData> removeDoblesTwitts (List<TweetResultData> list){
        List<TweetResultData> finalTweets = new ArrayList<>();

        for(TweetResultData d : list){
            if(checkID(d.getId(), finalTweets)){
                finalTweets.add(d);
            }
        }

        return finalTweets;
    }

    //Verifica ID do tweet
    boolean checkID (long id, List<TweetResultData> list){
        for(TweetResultData d : list){
            if(d.getId() == id){
                return false;
            }
        }
        return true;
    }


    //Configura quando não tem twitts
    void ConfigureNoTwitterFeedSup(){
        mHolder.txtNoTwitterFeedSup.setVisibility(View.VISIBLE);
        mHolder.frameLoadingTwitterFeedSup.setVisibility(View.GONE);
        mHolder.twitterFeedSupView.setVisibility(View.INVISIBLE);
        mHolder.twitterFeedSupView.setAdapter(null);
    }
    //endregion

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_TWITEER:{
                try {
                    if ((response.has("statuses")) && (response.get("statuses") != JSONObject.NULL)) {
                        TwitterFeedInformation.setTweetsSup(TweetResultData.parseTweetResultDataList(response.getJSONArray("statuses")));
                        TwitterFeedInformation.setQueryNext(TweetResultData.getNextQuerry(response.getJSONObject("search_metadata")));
                        ConfigureTwitterFeedSup();
                    }
                } catch (JSONException e) {
                    UTILS.DebugLog("TwitterError", e);
                }
            }
            break;

            case OP_ID_TWITEER_QUERRY:{
                int status = 0;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    isGettingQuerySup = false;
                    try{
                        if(response.has("Object")){
                            JSONObject obj = response.getJSONObject("Object");
                            if (obj.has("Hashtags")){
                                TwitterFeedInformation.setQuerySup(obj.getString("Hashtags"));
                                GetTwitterFeed();
                                return;
                            }
                            TwitterFeedInformation.setQuerySup("");
                        }
                    } catch (JSONException e) {
                        UTILS.DebugLog("TwitterError", e);
                    }
                    TwitterFeedInformation.setQuerySup("");
                }

            }
            break;

            case OP_ID_TWITEER_TOKEN:{
                try {
                    if ((response.has("access_token")) && (response.get("access_token") != JSONObject.NULL)) {
                        String bearerToken = response.getString("access_token");

                        prefs.put(CONSTANTS.SHARED_PREFS_KEY_BEARER_TOKEN, bearerToken);

                        TwitterFeedInformation.setBearerToken(bearerToken);
                        GetTwitterQuerry();
                    }
                } catch (JSONException e) {
                    TwitterFeedInformation.setBearerToken("");
                    UTILS.DebugLog("TwitterError", e);
                }

            }
            break;
        }

    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }

    class ViewHolder {

        public FrameLayout frameLoadingTwitterFeedSup;
        public DynamicTextView txtNoTwitterFeedSup;
        public TwoWayView twitterFeedSupView;

        public ViewHolder (){
            twitterFeedSupView = (TwoWayView) findViewById(R.id.twitterFeedSupView);
            twitterFeedSupView.setVisibility(INVISIBLE);

            txtNoTwitterFeedSup = (DynamicTextView)findViewById(R.id.txtNoTwitterFeedSup);
            txtNoTwitterFeedSup.setVisibility(GONE);

            frameLoadingTwitterFeedSup = (FrameLayout)findViewById(R.id.frameLoadingTwitterFeedSup);
            frameLoadingTwitterFeedSup.setVisibility(VISIBLE);
        }
    }
}
