package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterRankItem;
import data.PositonData;
import data.RankData;
import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class RankingActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    AdapterRankItem adapterRankItem;
    int page = 1;
    int userID = 1;
    int actualID;
    boolean hasCreatedGrups = false;

    private static final int OP_GET_RANK_GROUPS              = 0;
    private static final int OP_GET_RANK                     = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        mHolder = new ViewHolder();
        getRankingGrupos();
    }

    void getRankingGrupos (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_RANK_GROUP, OP_GET_RANK_GROUPS, this).execute();
    }

    void getRanking (Integer id, Integer page){
        Toast.makeText(activity, "Carregando..." ,Toast.LENGTH_SHORT).show();

        Hashtable<String, Object> params = new Hashtable<>();

        if(id != null)
            params.put("id", id);

        if(page != null)
            params.put("page", page);

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_RANK_, OP_GET_RANK, this).execute(params);
    }

    void initRankGroups (List<PositonData> data){
        for(PositonData pos : data){
            RadioButton button = (RadioButton) LayoutInflater.from(activity).inflate(R.layout.include_radio_button, null, false);
            DisplayMetrics displayMetrics = new DisplayMetrics();

            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            int finalwidth = (width/3) + ((width/3)/8);

            RadioGroup.LayoutParams param = new RadioGroup.LayoutParams(finalwidth, RadioGroup.LayoutParams.MATCH_PARENT, 1.0f);
            param.setMargins(10,0,0,0);

            button.setText(pos.getNome());
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP , 15);

            final int ID = pos.getId();
            button.setLayoutParams(param);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_RANKING_SELECIONAR, ID).execute();
                    mHolder.radioGroup.check(v.getId());
                    page = 1;
                    getRanking(ID, page);
                    actualID = ID;
                }
            });

            mHolder.radioGroup.addView(button);
        }


        if(!hasCreatedGrups){
            RadioButton button = (RadioButton) mHolder.radioGroup.getChildAt(0);
            actualID = data.get(0).getId();
            page = 1;
            getRanking(actualID, page);
            button.toggle();
            hasCreatedGrups = true;
        }
    }

    void initRank (List<RankData> rank, RankData user){
        if(user != null){
            userID = user.getId();
        }

        if(rank != null){
            if(adapterRankItem == null){
                adapterRankItem = new AdapterRankItem(activity, rank, userID, new AdapterRankItem.Listener() {
                    @Override
                    public void onLastItem() {
                        getRanking(actualID, page);
                    }
                });

                mHolder.recyclerRanking.setAdapter(adapterRankItem);
                mHolder.recyclerRanking.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
            }else{
                if(page == 1){
                    adapterRankItem.newData(rank, userID);
                }else{
                    adapterRankItem.addData(rank, userID);
                }
            }
        }

        //Mostrar seu personagem
        if(page == 1){
            if(user != null){
                mHolder.userTop.setVisibility(View.VISIBLE);

                UTILS.getImageAt(activity, user.getImg(), mHolder.userImage,new RequestOptions()
                        .circleCrop()
                        .placeholder(R.drawable.ic_placeholderuser)
                        .error(R.drawable.ic_placeholderuser));

                mHolder.userPos.setText(String.valueOf(user.getRank()));
                mHolder.userNome.setText(user.getNome());

                mHolder.userNivel.setText(user.isSocioTorcedor()? "Sócio Torcedor" : "Torcedor");

                mHolder.userPontos.setText(String.valueOf(user.getScore()));
            }else{
                mHolder.userTop.setVisibility(View.GONE);
            }
        }
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_RANK_GROUPS:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();

                            JSONObject obj = response.getJSONObject("Object");
                            PositonData[] data = gson.fromJson(obj.getString("itens"), PositonData[].class);
                            List<PositonData> grupos = new ArrayList<>(Arrays.asList(data));

                            if(data != null){
                                initRankGroups(grupos);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_GET_RANK:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            RankData pos = null;
                            JSONObject obj = response.getJSONObject("Object");

                            if(obj.has("itens")){
                                RankData[] data = gson.fromJson(obj.getString("itens"), RankData[].class);

                                if(obj.has("user"))
                                    pos = gson.fromJson(obj.getString("user"), RankData.class);

                                if(data != null && data.length > 0){
                                    List<RankData> rank = new ArrayList<>(Arrays.asList(data));
                                    initRank(rank, pos);
                                    page ++;
                                }
                            }else{
                                if(adapterRankItem != null){
                                    if(page == 1){
                                        adapterRankItem.newData(null, -1);
                                    }
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {

        RadioGroup radioGroup;

        View userTop;
        TextView userPos;
        ImageView userImage;
        TextView userNome;
        TextView userNivel;
        TextView userPontos;

        RecyclerView recyclerRanking;
        public ViewHolder (){
            radioGroup = findViewById(R.id.radioGroup_filters);

            userTop = findViewById(R.id.view_userTop);
            userPos = findViewById(R.id.txt_pos);
            userImage = findViewById(R.id.img_photo);
            userNome = findViewById(R.id.txt_nome);
            userNivel = findViewById(R.id.txt_nivel);
            userPontos = findViewById(R.id.txt_pontos);

            recyclerRanking = findViewById(R.id.recycle_ranking);

        }
    }
}
