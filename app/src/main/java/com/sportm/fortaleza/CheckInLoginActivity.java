package com.sportm.fortaleza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.URI;

public class CheckInLoginActivity extends MainActivity {

    ViewHolder mHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_login);
        mHolder = new ViewHolder();

        initActions();
    }

    void initActions (){
        mHolder.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SocioTorcedorActivity.class);
                startActivity(intent);
            }
        });

        mHolder.btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://sociofortaleza.com.br/"));
                startActivity(intent);

            }
        });
    }

    class ViewHolder {
        TextView tittle, message;

        Button btnLogin;
        Button btnPurchase;

        public ViewHolder (){
            tittle = findViewById(R.id.txt_tittle);
            message = findViewById(R.id.txt_message);

            btnLogin = findViewById(R.id.btn_is_socio);
            btnPurchase = findViewById(R.id.btn_whant_socio);
        }
    }
}
