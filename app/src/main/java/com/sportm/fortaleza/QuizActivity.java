package com.sportm.fortaleza;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdSize;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import data.QuizData;
import data.RespostasData;
import informations.UserInformation;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MoedasController;
import utils.TRACKING;
import utils.UTILS;

public class QuizActivity extends MainActivity  implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    CountDownTimer timer;
    CountDownTimer nextTimer;
    QuizData quiz;
    List<RespostasData> options;
    boolean hasExited = false;
    boolean hasFinished = false;
    private int actualTimer;

    int id;             // ID do grupo de Quiz
    int color;          // 0 vermelho  / 1 branco / 2 preto
    String tittle;      // Titulo
    String img;         // Imagem de fundo
    String quant;       // Quantidade deste quiz

    int maxQuiz;
    int actualQuiz;
    int acertos = 0;
    boolean canAnwer = true;

    private static final int OP_GET_QUIZ              = 0;
    private static final int OP_SET_QUIZ              = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        LinearLayout mAdView = findViewById(R.id.adView);
        AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.QUIZ_BOTTOM_ID);

        Intent intent= getIntent();
        id = intent.getIntExtra("id", 0);
        color = intent.getIntExtra("color", 0);
        tittle = intent.getStringExtra("tittle");
        img = intent.getStringExtra("img");
        quant = intent.getStringExtra("quant");
        acertos = intent.getIntExtra("acertos", 0);

        Log.d("Acertos", "Quiz: " + acertos);


        final String[] value =  quant.split("/");
        actualQuiz = Integer.parseInt(value[0]);
        maxQuiz = Integer.parseInt(value[1]);

        nextTimer = new CountDownTimer((6000), 1000) {

            public void onTick(long millisUntilFinished) {
                int value = (int)millisUntilFinished / 1000;
                if(actualQuiz >= maxQuiz){
                    mHolder.btnNext.setText("Saindo em ("+ value +")");
                }else{
                    mHolder.btnNext.setText("Próxima Pergunta("+ value +")");
                }
            }

            public void onFinish() {
                if(actualQuiz >= maxQuiz){
                    finish();
                }else{
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_PROXIMA_PERGUNTA_TIMER, quiz.getId()).execute();
                    getQuiz(id);
                }
            }
        };
        mHolder = new ViewHolder();

        getQuiz(id);
        initAction();
    }

    @Override
    protected void onResume() {
        if(hasExited){
            mHolder.progressTime.setProgress(0);
            mHolder.time.setText(String.valueOf(0));
            timer.onFinish();
            hasExited = false;
        }

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setIntentResult();
    }


    void setIntentResult (){
//        Intent resultIntent = this.getIntent();
//        resultIntent.putExtra("quant", mHolder.quizQuant.getText().toString());
//        this.setResult(RESULT_OK, resultIntent);
    }

    @Override
    protected void onStop() {
        hasExited = true;

        if(timer != null)
            timer.cancel();

        if(nextTimer != null)
            nextTimer.cancel();

        super.onStop();
    }

    void initAction (){
        mHolder.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_PROXIMA_PERGUNTA, quiz.getId()).execute();
                if(actualQuiz < maxQuiz){
                    getQuiz(id);
                }
                nextTimer.cancel();
            }
        });

        mHolder.txtSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_PROXIMA_SAIR, quiz.getId()).execute();
                finish();
                setIntentResult();
            }
        });
    }

    //Pega o quiz pelo id
    void getQuiz (int id) {
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_QUIZ, OP_GET_QUIZ,this).execute(params);
    }

    //Inicia o Quiz
    void initQuiz (final QuizData data){
        quiz = data;
        options = data.getRespostas();

        canAnwer = true;
        actualQuiz += 1;

        mHolder.menu_bottom.setVisibility(View.INVISIBLE);
        mHolder.menuGameStatus.setVisibility(View.INVISIBLE);
        mHolder.scrollView.scrollTo(0,0);

        switch (color){
            case 0:{
                mHolder.foreground.setColorFilter(activity.getResources().getColor(R.color.colorButtonRedTransp2));
                mHolder.tittle.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                //mHolder.description.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            }
            break;
            case 1:{
                mHolder.foreground.setColorFilter(activity.getResources().getColor(R.color.colorButtonWhiteTransp));
                mHolder.tittle.setTextColor(activity.getResources().getColor(R.color.colorBlackLetter));
                //mHolder.description.setTextColor(activity.getResources().getColor(R.color.colorBlackLetter));
            }
            break;
            case 2:{
                mHolder.foreground.setColorFilter(activity.getResources().getColor(R.color.colorButtonBlackTransp));
                mHolder.tittle.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                //mHolder.description.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            }
            break;
        }

        if(img != null){
            UTILS.getImageAt(activity, img, mHolder.background);
        }

        mHolder.tittle.setText(tittle);
        mHolder.description.setText(data.getPergunta());

        mHolder.progressTime.setVisibility(View.VISIBLE);
        mHolder.progressTime.setMax(data.getTempo());
        mHolder.progressTime.setProgress(data.getTempo());

        String quantidade = actualQuiz + "/" + maxQuiz;
        mHolder.quizQuant.setText(quantidade);
        String mAcerto = String.valueOf(acertos);
        mAcerto = mAcerto + (acertos > 1? " Acertos": " Acerto");
        mHolder.acertos.setText(mAcerto);
        mHolder.time.setText(String.valueOf(data.getTempo()));

        Collections.shuffle(options);
        initOptions(options);

        if(timer != null){
            timer.cancel();
            timer.start();
        }else{
            actualTimer = data.getTempo();
            timer = new CountDownTimer(((actualTimer + 1) * 1000), 1000) {

                public void onTick(long millisUntilFinished) {
                    int value = (int)millisUntilFinished / 1000;
                    mHolder.progressTime.setProgress(value);
                    mHolder.time.setText(String.valueOf(value));
                }

                public void onFinish() {
                    canAnwer = false;
                    initOptions(options, -1);
                    mHolder.txtGameStatus.setText("Tempo acabou!");
                    mHolder.txtGameCoin.setText("-" + quiz.getMoedasTimeout());
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_TEMPOESGOTADO, quiz.getId()).execute();
                }
            };
            timer.start();
        }

        if(actualQuiz >= maxQuiz){
            mHolder.btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_PROXIMA_SAIR, quiz.getId()).execute();
                    finish();
                    setIntentResult();
                }
            });
        }

    }

    //Inicia opções
    void initOptions (final List<RespostasData> data){

        if(mHolder.options.getChildCount() > 0){
            mHolder.options.removeAllViews();
        }

        for(final RespostasData r : data){
            View v = LayoutInflater.from(activity).inflate(R.layout.adapter_answer, null, false);
            final TextView tittle = v.findViewById(R.id.txt_answer);
            TextView porc = v.findViewById(R.id.txt_porc);
            ProgressBar bar = v.findViewById(R.id.progress_answer);
            final ImageView background = v.findViewById(R.id.img_background);

            tittle.setText(r.getResposta());
            tittle.setTextColor(getResources().getColor(R.color.colorBlackLetter));

            porc.setVisibility(View.GONE);
            bar.setVisibility(View.GONE);
            background.setVisibility(View.VISIBLE);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(canAnwer){
                        //tittle.setTextColor(getResources().getColor(R.color.colorWhite));
//
//                        if(r.getCorreto() == 1){
//                            //background.setColorFilter(getResources().getColor(R.color.colorCorrect));
//                            tittle.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_correct), null);
//                            setResposta(true, r.getId());
//                        }else{
//                            //background.setColorFilter(getResources().getColor(R.color.colorInCorrect));
//                            tittle.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_incorrect), null);
//                            setResposta(false, r.getId());
//                        }
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_QUIZ_RESPONDER, quiz.getId()).execute();
                        initOptions(data, r.getId());
                    }
                }
            });

            mHolder.options.addView(v);
        }
    }

    //Inicia opções
    void initOptions (List<RespostasData> data, int selected){

        if(mHolder.options.getChildCount() > 0){
            mHolder.options.removeAllViews();
        }

        if(timer != null) {
            timer.cancel();
        }
        boolean acertou = false;
        canAnwer = false;

        //Construção das respostas
        for(final RespostasData r : data){
            View v = LayoutInflater.from(activity).inflate(R.layout.adapter_answer, null, false);
            final TextView tittle = v.findViewById(R.id.txt_answer);
            ImageView status = v.findViewById(R.id.img_status);
            TextView porc = v.findViewById(R.id.txt_porc);
            ProgressBar bar = v.findViewById(R.id.progress_answer);
            final ImageView background = v.findViewById(R.id.img_background);

            tittle.setText(r.getResposta());
            tittle.setTextColor(getResources().getColor(R.color.colorBlackLetter));
            porc.setVisibility(View.GONE);
            bar.setVisibility(View.GONE);
            background.setVisibility(View.VISIBLE);


            //Verifica se esta é a resposta correta
            if(r.getCorreto() == 1){
                status.setVisibility(View.VISIBLE);
                status.setImageResource(R.drawable.ic_correct);
                //tittle.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_correct), null);
            }

            if(selected == r.getId()) {
                if (r.getCorreto() == 1) {
                    acertou = true;
                }else{
                    acertou = false;
                }
            }

            //verifica se é a resosta clicada
            if(r.getId() != selected){
                v.setAlpha(0.3f);
            }else{
                if(r.getCorreto() == 1){
                    //background.setColorFilter(getResources().getColor(R.color.colorCorrect));
                    status.setVisibility(View.VISIBLE);
                    status.setImageResource(R.drawable.ic_correct);
                    //tittle.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_correct), null);
                }else{
                    //background.setColorFilter(getResources().getColor(R.color.colorInCorrect));
                    status.setVisibility(View.VISIBLE);
                    status.setImageResource(R.drawable.ic_incorrect);
                    //tittle.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_incorrect), null);
                }
            }
            mHolder.options.addView(v);
        }
        mHolder.scrollView.scrollTo(0,mHolder.firtView.getHeight());

        if(acertou){
            acertos ++;
            String mAcerto = String.valueOf(acertos);
            mAcerto = mAcerto + (acertos > 1? " Acertos": " Acerto");
            mHolder.acertos.setText(mAcerto);
            mHolder.txtGameStatus.setText("Você acertou");
            mHolder.txtGameCoin.setText("+" + quiz.getMoedasVitoria());
            mHolder.txtGameCoin.setVisibility(View.VISIBLE);
            MoedasController.showNewMoedas(activity, mHolder.showMoedas, quiz.getMoedasVitoria());
        }else{
            mHolder.txtGameStatus.setText("Você errou");
            mHolder.txtGameCoin.setVisibility(View.GONE);
        }


        if(actualQuiz >= maxQuiz){
            hasFinished = true;
        }else{
            nextTimer.start();
        }
        mHolder.menu_bottom.setVisibility(View.VISIBLE);
        mHolder.menuGameStatus.setVisibility(View.VISIBLE);

        if(selected > 0){
            float timer = actualTimer - mHolder.progressTime.getProgress();
            Log.d("Async", String.valueOf(timer));
            Hashtable<String, Object> params  = setQuizParams(quiz.getId(), selected, -1, timer);
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_QUIZ, OP_SET_QUIZ, this).execute(params);
        }else{
            setTimeOut();
        }
    }

    void showExitPopup (){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage("Parabéns! Você respondeu todas as perguntas deste Quiz!" + "\n"+
                "Continue participando");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    Hashtable<String, Object> setQuizParams (int id, int resposta, int timeOut, float tempo){
        Hashtable<String, Object> params = new Hashtable<>();

        params.put("idPergunta", id);

        if(resposta >= 0)
            params.put("idResposta", resposta);

        if(timeOut >= 0)
            params.put("timeout", timeOut);

        if(tempo > 0)
            params.put("tempo", tempo);

        return params;
    }

    void setTimeOut (){
        Hashtable<String, Object> params = setQuizParams(quiz.getId(),-1, 1, -1);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_QUIZ, OP_SET_QUIZ, this).execute(params);
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_QUIZ:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            QuizData data = gson.fromJson(response.getString("Object"), QuizData.class);
                            if(data != null && data.getPergunta() != null){
                                initQuiz(data);
                            }else{
                                //finish();
                                setIntentResult();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else if (status == 202){
                    if(response.has("msg")){
                        try {
                            showMessageFinish(response.getString("msg"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        showMessageFinish("Parabéns você respondeu todas as perguntas deste Quiz");
                    }
                }
            }
            break;

            case  OP_SET_QUIZ:{
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200) {

                    if (response.has("newBadges")) {
                        try {
                            String badges = response.getString("newBadges");
                            Intent intent = new Intent(activity, WinBadgeActivity.class);
                            intent.putExtra("badges", badges);
                            activity.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if(hasFinished){
                        showExitPopup();
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId) {
            case OP_GET_QUIZ: {
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                if (status == 202){
                    if(response.has("msg")){
                        try {
                            showMessageFinish(response.getString("msg"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        showMessageFinish("Parabéns você respondeu todas as perguntas deste Quiz");
                    }
                }

            }
        }
    }
    //endregion

    class ViewHolder {
        ScrollView scrollView;
        LinearLayout firtView;

        TextView tittle;
        LinearLayout ads;

        ProgressBar progressTime;
        TextView time;
        TextView quizQuant;
        TextView acertos;

        TextView description;
        LinearLayout options;

        LinearLayout menu_bottom;
        Button btnNext;
        TextView txtSair;

        LinearLayout menuGameStatus;
        TextView txtGameStatus;
        TextView txtGameCoin;

        ImageView background;
        ImageView foreground;
        LinearLayout showMoedas;

        public ViewHolder (){
            scrollView = findViewById(R.id.scroll_quiz);
            firtView = findViewById(R.id.view_firtView);
            showMoedas = findViewById(R.id.show_moedas);

            tittle = findViewById(R.id.txt_quiz_tittle);
            ads = findViewById(R.id.view_ads);

            progressTime = findViewById(R.id.progress_quiz_timer);
            time = findViewById(R.id.txt_quiz_timer);
            quizQuant = findViewById(R.id.txt_quiz_number);
            acertos = findViewById(R.id.txt_quiz_number_acertos);

            description = findViewById(R.id.txt_description_quiz);
            options = findViewById(R.id.view_quiz_options);

            menu_bottom = findViewById(R.id.menu_bottom);
            btnNext = findViewById(R.id.btn_next_quiz);
            txtSair = findViewById(R.id.txt_sair);

            menuGameStatus = findViewById(R.id.menu_game_status);
            txtGameStatus = findViewById(R.id.txt_game_status);
            txtGameCoin = findViewById(R.id.txt_game_coin);

            background = findViewById(R.id.img_background);
            foreground = findViewById(R.id.img_foreground);
        }
    }
}
