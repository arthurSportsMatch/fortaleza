package com.sportm.fortaleza;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

public class MyAplication extends Application implements LifecycleObserver {

    private MainActivity activity;
    public static MyAplication intance;

    @Override
    public void onCreate() {
        super.onCreate();
        if (intance == null) {
            intance = this;
        }

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        Log.d("Session2", "ON_STOP");
        activity.saveSessionValues();//Salvando para quando o app for fechado
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        Log.d("Session2", "ON_START");
        activity.setSession();
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }
}
