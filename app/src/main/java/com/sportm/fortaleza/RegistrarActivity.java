package com.sportm.fortaleza;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

import data.UserData;
import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class RegistrarActivity extends MainActivity {
//    ViewHolder mHolder;
//    Animation slideDown;
//    private enum Screen{
//        emailForm,
//        nameForm,
//        dateForm,
//        genderForm,
//        socioForm;
//    }
//
//    private static final int OP_ID_SIGN_UP = 0;
//    private static final int OP_ID_SIGN_IN = 1;
//
//    Screen screenStates = Screen.emailForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
//        mHolder = new ViewHolder();
//        slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
//        initActions();
    }

//    //Inicia Ações
//    void initActions (){
//
//        mHolder.create.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Tela de email e senha
//                if(screenStates == Screen.emailForm){
//                    if(checkEmailForm()){
//                        ChangeView(mHolder.emailForm, mHolder.nameForm);
//                        screenStates = Screen.nameForm;
//                        return;
//                    }
//                }
//                //Tela de nome
//                if(screenStates == Screen.nameForm){
//                    if(checkNameForm()){
//                        ChangeView(mHolder.nameForm, mHolder.dateForm);
//                        screenStates = Screen.dateForm;
//                        return;
//                    }
//                }
//                //Tela de aniversario
//                if(screenStates == Screen.dateForm){
//                    if(checkDateForm()){
//                        ChangeView(mHolder.dateForm, mHolder.genderForm);
//                        screenStates = Screen.genderForm;
//                        return;
//                    }
//                }
//                //Tela de genero
//                if(screenStates == Screen.genderForm){
//                    if(checkGenderForm()){
//                        ChangeView(mHolder.genderForm, mHolder.socioForm);
//                        screenStates = Screen.socioForm;
//                        return;
//                    }
//                }
//                //Tela de socio
//                if(screenStates == Screen.socioForm){
//                    if(checkSocioForm()){
//                        Hashtable<String, Object> params = passUserInfos();
//                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SIGN_UP, OP_ID_SIGN_UP, RegistrarActivity.this, TRACKING.TRACKING_CREATEACCOUNT_CONFIRMAR).execute(params);
//                    }
//                }
//            }
//        });
//
//        mHolder.hasAccount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//                //new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_CREATEACCOUNT_FECHAR).execute();
//            }
//        });
//
//        //        SpannableString ss = new SpannableString(getResources().getString(R.string.terms));
////        ClickableSpan terms = new ClickableSpan() {
////            @Override
////            public void onClick(@NonNull View widget) {
////                showReg(MainActivity.regulamento);
////            }
////
////            @Override
////            public void updateDrawState(@NonNull TextPaint ds) {
////                super.updateDrawState(ds);
////                ds.setColor(getResources().getColor(R.color.colorButtonRed));
////                ds.setUnderlineText(false);
////            }
////        };
////
////        ss.setSpan(terms,21,61, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
////        mHolder.terms.setText(ss);
////        mHolder.terms.setMovementMethod(LinkMovementMethod.getInstance());
//
//        mHolder.closeTerms.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mHolder.holderTerms.setVisibility(View.GONE);
//            }
//        });
//    }
//
//    void showReg (String link){
//        Log.d("Regulamento-Reg", link);
//        mHolder.holderTerms.setVisibility(View.VISIBLE);
//        //mHolder.htmlTerms.loadData(link,"text/html", "utf-8");
//        mHolder.htmlTerms.getSettings().setJavaScriptEnabled(true); // enable javascript
//        mHolder.htmlTerms.setWebViewClient(new WebViewClient());
//        mHolder.htmlTerms.loadUrl(link);
//        //setContentView(mHolder.htmlTerms);
//    }
//
//    //Mudar view
//    void ChangeView (View lastView, View newView){
//        lastView.setVisibility(View.GONE);
//        newView.setVisibility(View.VISIBLE);
//    }
//
//    boolean checkEmailForm (){
//
//        //Check email
//        if(mHolder.email.getText().toString().isEmpty()){
//            mHolder.email.setError("Coloque seu email aqui");
//            return false;
//        }else{
//            if(!UTILS.isValidEmail(mHolder.email.getText().toString())){
//                mHolder.email.setError("Coloque um email valido aqui");
//                return false;
//            }
//        }
//
//        //Check senha
//        if(mHolder.senha.getText().toString().isEmpty()){
//            mHolder.senha.setError("Coloque sua senha aqui");
//            return false;
//        }else{
//            if(mHolder.senha.getText().toString().length() < 6){
//                mHolder.senha.setError("A senha tem que ter 6 ou mais caracteres");
//                return false;
//            }
//        }
//
//        if(mHolder.confirmSenha.getText().toString().isEmpty()){
//            mHolder.confirmSenha.setError("Confirme sua senha aqui");
//            return false;
//        }else{
//            String passord = mHolder.senha.getText().toString();
//            String passord2 = mHolder.confirmSenha.getText().toString();
//            Log.d("Register", passord + " " + passord2);
//            if( !passord2.contains(passord)){
//                mHolder.confirmSenha.setError("As senhas devem ser iguais");
//                return false;
//            }
//        }
//
//        return true;
//    }
//
//    boolean checkNameForm (){
//        if(mHolder.nome.getText().toString().isEmpty()){
//            mHolder.nome.setError("Coloque seu nome aqui");
//            return false;
//        }
//
//        return true;
//    }
//
//    boolean checkDateForm (){
//        if(mHolder.day.getText().toString().isEmpty()){
//            mHolder.day.setError("Coloque o dia aqui");
//            return false;
//        }
//
//        if(mHolder.month.getText().toString().isEmpty()){
//            mHolder.month.setError("Coloque o mês aqui");
//            return false;
//        }
//
//        if(mHolder.year.getText().toString().isEmpty()){
//            mHolder.year.setError("Coloque o ano aqui");
//            return false;
//        }
//        return true;
//    }
//
//    boolean checkGenderForm (){
//        if(mHolder.genderGroup.getCheckedRadioButtonId() == -1){
//            return false;
//        }
//        return true;
//    }
//
//    boolean checkSocioForm (){
//        if(mHolder.socio.getCheckedRadioButtonId() == -1){
//            return false;
//        }
//        return true;
//    }
//
//    //Cria user infos
//    Hashtable<String, Object> passUserInfos (){
//        Hashtable<String, Object> params = new Hashtable<String, Object>();
//        params.put("nome", mHolder.nome.getText().toString());
//        params.put("email", mHolder.email.getText().toString());
//        params.put("senha", mHolder.senha.getText().toString());
//        int aceptNotification = 1;
//        params.put("aceitaNews",aceptNotification);
//        params.put("apns","0000000000");
//        return params;
//    }
//
//    //Faz login do usuario
//    void login (){
//        Hashtable<String, Object> params = passUserInfos();
//        new AsyncOperation(activity,AsyncOperation.TASK_ID_SIGN_IN, OP_ID_SIGN_IN, RegistrarActivity.this ).execute(params);
//    }
//
//    //Call main menu
//    void callMainMenu(){
//        finish();
//        Intent intent = new Intent(activity, MainMenuActivity.class);
//        startActivity(intent);
//    }
//
//    //region Async Operation
//    @Override
//    public void CallHandler(int opId, JSONObject response, boolean success) {
//        Message message = new Message();
//        Bundle b = new Bundle();
//        b.putInt("opId", opId);
//        b.putString("response", response.toString());
//        b.putBoolean("success", success);
//        message.setData(b);
//        handlerOp.sendMessage(message);
//    }
//
//    private Handler handlerOp = new Handler(new Handler.Callback() {
//        @Override
//        public boolean handleMessage(Message message) {
//            int opId = -1;
//            JSONObject response = null;
//            boolean success = false;
//
//            try{
//                opId = message.getData().getInt("opId");
//                response = new JSONObject(message.getData().getString("response"));
//                success = message.getData().getBoolean("success");
//            }
//            catch (JSONException e){
//                UTILS.DebugLog("Error", "Error getting handlers params.");
//                return false;
//            }
//
//            if(success) {
//                OnAsyncOperationSuccess(opId, response);
//            }
//            else{
//                OnAsyncOperationError(opId, response);
//            }
//
//            return false;
//        }
//    });
//
//    @Override
//    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
//        switch (opId){
//            case OP_ID_SIGN_UP:{
//                int status = 0;
//                if(response.has("Status")) {
//                    try {
//                        status = (int) response.get("Status");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if(response.has("Object")){
//                    if(status == 200){
//                        try {
//                            if(response.has("id")){
//                                UserInformation.setSessionId((int) response.get("id"));
//                            }
//                        }catch (JSONException e){
//                            e.printStackTrace();
//                        }
//
//                        login();
//                    }
//                }
//            }
//            break;
//
//            case OP_ID_SIGN_IN:{
//                int status = 0;
//                boolean success = false;
//                if(response.has("Status")) {
//                    try {
//                        status = (int) response.get("Status");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if(status == 200){
//                    if(response.has("Object")){
//                        try {
//                            Log.d(CONSTANTS.ASYNCDEBUG,"Pegando Usuario");
//                            Gson gson = new Gson();
//                            UserData data = gson.fromJson(String.valueOf(response.get("Object")), UserData.class);
//                            data.setSessionId(UserInformation.getUserData().getSessionId());
//                            data.setSocioTorcedor(UserInformation.getUserData().getSocioTorcedor());
//                            data.setSocioBeneficio(UserInformation.getUserData().getSocioBeneficio());
//                            data.setSocioStatus(UserInformation.getUserData().getSocioStatus());
//                            UserInformation.setUserData(data);
//
//                            prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_AUTOLOGIN);
//                            prefs.put(CONSTANTS.SHARED_PREFS_KEY_AUTOLOGIN, UserInformation.getUserData().toJSON().toString());
//                            prefs.put(CONSTANTS.SHARED_PREFS_KEY_REG_VERSION, String.valueOf(UserInformation.getUserData().getRv()));
//                            success = true;
//                        }catch (JSONException e){
//                            e.printStackTrace();
//                        }
//                    }
//                }else{
//                    success = false;
//                }
//
//                if(success){
//                    prefs.put(CONSTANTS.SHARED_PREFS_KEY_PREVIOUS_EMAIL, UserInformation.getUserData().getEmail());
//                    UTILS.setUserLoggedIn(true);
//                    callMainMenu();
//                }else{
//                    try {
//                        Log.d(CONSTANTS.ASYNCDEBUG,"Logado");
//                        Toast.makeText(activity, response.getString("Message"), Toast.LENGTH_LONG).show();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            break;
//        }
//    }
//
//    @Override
//    public void OnAsyncOperationError(int opId, JSONObject response) {
//        switch (opId){
//            case OP_ID_SIGN_UP:{
//            }
//            case OP_ID_SIGN_IN:{
//                try {
//                    Toast.makeText(getApplicationContext(),response.getString("Message"), Toast.LENGTH_LONG).show();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            break;
//        }
//    }
//   //endregion
//
//    class ViewHolder {
//        TextView create;
//        TextView hasAccount;
//
//        EditText nome;
//        EditText email;
//        EditText senha;
//        EditText confirmSenha;
//
//        EditText day;
//        EditText month;
//        EditText year;
//
//        RadioGroup genderGroup;
//        EditText gendertype;
//
//        RadioGroup socio;
//
//        FrameLayout holderTerms;
//        WebView htmlTerms;
//        TextView closeTerms;
//
//        View emailForm;
//        View nameForm;
//        View dateForm;
//        TextView nameDateName;
//        View genderForm;
//        TextView nameGenderName;
//        View socioForm;
//
//        public ViewHolder (){
//            create = findViewById(R.id.finish_create_account);
//            hasAccount = findViewById(R.id.has_account);
//
//            nome = findViewById(R.id.reg_name);
//            email = findViewById(R.id.reg_email);
//            senha = findViewById(R.id.reg_password);
//            confirmSenha = findViewById(R.id.reg_confirm_password);
//
//            htmlTerms = findViewById(R.id.show_reg);
//            holderTerms = findViewById(R.id.holder_reg);
//            closeTerms = findViewById(R.id.txt_reg_close);
//
//            emailForm = findViewById(R.id.view_reg_email);
//            nameForm = findViewById(R.id.view_reg_name);
//            dateForm = findViewById(R.id.view_reg_date);
//            genderForm = findViewById(R.id.view_reg_gender);
//            socioForm = findViewById(R.id.view_reg_finish);
//
//            nameDateName = findViewById(R.id.reg_txt_name_date);
//            nameGenderName = findViewById(R.id.reg_txt_name_gender);
//
//            day = findViewById(R.id.reg_day);
//            month = findViewById(R.id.reg_month);
//            year = findViewById(R.id.reg_year);
//
//            genderGroup = findViewById(R.id.reg_radio_genero);
//            gendertype = findViewById(R.id.reg_edt_genero);
//
//            socio = findViewById(R.id.reg_radio_torcedor);
//
//        }
//    }

}
