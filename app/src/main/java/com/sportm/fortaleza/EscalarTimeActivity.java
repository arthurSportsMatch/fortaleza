package com.sportm.fortaleza;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterPlayerSelector;
import data.AtletaData;
import data.PositonData;
import fragments.CampoFragment;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

public class EscalarTimeActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    CampoFragment campo;
    AdapterPlayerSelector adapter;
    int id = 1; //id desta pergunta
    int idEsquema = -1; //escalação mais setada atualmente
    String tittle = "Monte sua escalação";


    private static final int OP_GET_ESCALACAO            = 0;
    private static final int OP_GET_ATLETAS              = 1;
    private static final int OP_SET_ESCALA               = 2;

    AtletaEscalado[] atletasToAdd = new AtletaEscalado[11];
    int buttonID;
    boolean canChange = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escalar_time);

        mHolder = new ViewHolder();

        Intent intent = getIntent();
        if(intent.hasExtra("esquema")){
            idEsquema = intent.getIntExtra("esquema", 1);
        }

        if(intent.hasExtra("tittle")){
            tittle = intent.getStringExtra("tittle");
            mHolder.tittle.setText(tittle);
        }

        if(idEsquema < 1){
            idEsquema = 1;
        }

        if(intent.hasExtra("id")){
            id = intent.getIntExtra("id", -1);
        }

        //inicia o campo
        initCampo();
        //inicia as ações
        initAction();

        if(campo != null){
            campo.changeEsquema(idEsquema);
        }

        if(id < 0){
            campo.changeEsquema(1);
        }else{
            getEscalacao();
        }

        RadioButton button = (RadioButton) mHolder.radioGroup.getChildAt(idEsquema-1);
        button.toggle();

        getAtletas();
    }

    void getEscalacao (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_ESCALACAO, OP_GET_ESCALACAO, this).execute(params);
    }

    void getAtletas (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PLAYERS_ESCALACAO, OP_GET_ATLETAS, this).execute(params);
    }

    void initAction (){
        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHolder.positions.setVisibility(View.VISIBLE);
                mHolder.atletas.setVisibility(View.GONE);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_LIST_VOLTAR, idEsquema).execute();
            }
        });

        mHolder.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAtletas(atletasToAdd);
            }
        });
    }

    void initCampo (){
        campo = new CampoFragment(new CampoFragment.OnClickListener() {
            @Override
            public void OnClick(int id) {
                changeButton(id);
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(mHolder.fragmentLayout.getId(), campo);
        transaction.commit();

        initRadio();
    }

    void initRadio (){
        if(idEsquema < 1){
            idEsquema = 1;
        }

        mHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int position = group.getCheckedRadioButtonId();
                View radioButton = group.findViewById(position);
                int idx = group.indexOfChild(radioButton);
                idEsquema = (idx + 1);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_SELECIONAR, idEsquema).execute();
                campo.changeEsquema(idEsquema);
            }
        });
    }

    void changeButton (int id){
        if(canChange){
            mHolder.atletas.setVisibility(View.VISIBLE);
            mHolder.positions.setVisibility(View.GONE);
            buttonID = id;
        }
    }

    void initEsquema (List<Players> players, int id){
        RadioButton button = (RadioButton) mHolder.radioGroup.getChildAt(id-1);
        button.toggle();
        campo.changeEsquema(id);

        for(Players pl : players){
            campo.changeButton(pl.pos-1, pl.getNome());
        }

        //mHolder.btnContinue.getBackground().setTint(getResources().getColor(R.color.colorGray));
        mHolder.btnContinue.setAlpha(0.5f);
        mHolder.btnContinue.setEnabled(false);
        mHolder.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    void initAtletas (List<AtletaData> data, List<PositonData> pos){
      adapter = new AdapterPlayerSelector(this, data, pos, new AdapterPlayerSelector.Listener() {
            @Override
            public void getAtleta(AtletaData atleta) {
                addAtleta(atleta);
            }
        });
        mHolder.recyclerView.setAdapter(adapter);
        mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }

    void addAtleta (AtletaData atleta){
        atletasToAdd[buttonID] = new AtletaEscalado(buttonID+1, atleta.getId());
        adapter.addAtleta(buttonID, new AtletaEscalado(buttonID+1, atleta.getId()));
        checkAtletaPlacements(atleta.getId());
        campo.changeButton(buttonID, atleta.getNome());
        mHolder.atletas.setVisibility(View.GONE);
        mHolder.positions.setVisibility(View.VISIBLE);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SELETORESCALACAO_LIST_SELECIONAR, atleta.getId()).execute();
    }

    void checkAtletaPlacements (int atletaID){
        for(AtletaEscalado atl : atletasToAdd){
            if(atl != null){
                if(atl.idPlayer == atletaID){
                    campo.removeAtleta(atl.idPosicao -1);
                }
            }
        }
    }

    void sendAtletas (AtletaEscalado[] escalacao){

        if(!checkArray(escalacao)){
            Toast.makeText(this, "Selecione 11 jogadores", Toast.LENGTH_SHORT).show();
            return;
        }

        if(idEsquema < 1){
            idEsquema = 1;
        }

        String header = "";
        for(AtletaEscalado atl: escalacao){
            header = header.concat(atl.idPosicao + ":" + atl.idPlayer +";");
        }

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("idEscalacao", id);
        params.put("idEsquema", idEsquema);
        params.put("players", header);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_PLAYERS_ESCALACAO, OP_SET_ESCALA, this).execute(params);
    }

    boolean checkArray (AtletaEscalado[] escalacao){
        for(AtletaEscalado atl: escalacao){
            if(atl == null){
                return false;
            }
        }
        return true;
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_ESCALACAO:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            Players[] data = gson.fromJson(obj.getString("players"), Players[].class);
                            int idEsq = obj.getInt("idEsquema");
                            Log.d("Async", "Baixei o esquema");

                            if(data != null && data.length > 0){
                                List<Players> atletas = new ArrayList<>(Arrays.asList(data));
                                Log.d("Async", "Data: " + data.toString());

                                if(idEsq < 1){
                                    idEsq = 1;
                                }

                                canChange = false;
                                initEsquema(atletas, idEsq);
                            }else{
                                canChange = true;
                                campo.changeEsquema(idEsquema);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_GET_ATLETAS:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            AtletaData[] data = gson.fromJson(obj.getString("itens"), AtletaData[].class);
                            PositonData[] pos = gson.fromJson(obj.getString("pos"), PositonData[].class);
                            List<AtletaData> atletas = new ArrayList<>(Arrays.asList(data));
                            List<PositonData> positions = new ArrayList<>(Arrays.asList(pos));

                            if(data != null){
                                initAtletas(atletas, positions);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case  OP_SET_ESCALA:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    Toast.makeText(this,"Escalação cadastrada com sucesso", Toast.LENGTH_LONG).show();
                    //mHolder.btnContinue.getBackground().setTint(getResources().getColor(R.color.colorGray));
                    mHolder.btnContinue.setAlpha(0.5f);
                    mHolder.btnContinue.setEnabled(false);
                    mHolder.btnContinue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //
                        }
                    });
                }else if (status == 202){
                    try {
                        Toast.makeText(this,response.getString("msg"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        View positions;
        RadioGroup radioGroup;
        FrameLayout fragmentLayout;
        Button btnContinue;
        TextView tittle;

        View atletas;
        RecyclerView recyclerView;
        TextView voltar;

        public ViewHolder (){

            tittle = findViewById(R.id.txt_tittle);
            positions = findViewById(R.id.view_card_quiz);
            radioGroup = findViewById(R.id.radio_escalas);
            fragmentLayout = findViewById(R.id.frame_campo);
            btnContinue = findViewById(R.id.btn_concluir);

            atletas = findViewById(R.id.view_card_selecionar);
            recyclerView = findViewById(R.id.recycle_atletas);
            voltar = findViewById(R.id.txt_voltar);
        }
    }

    public class AtletaEscalado {
        public Integer idPosicao;
        public int idPlayer;

        public AtletaEscalado(int idPosicao, int idPlayer) {
            this.idPosicao = idPosicao;
            this.idPlayer = idPlayer;
        }

        @Override
        public String toString() {
            return "AtletaEscalado{" +
                    "idPosicao=" + idPosicao +
                    ", idPlayer=" + idPlayer +
                    '}';
        }
    }

    class Players {
        private int pos;
        private String nome;

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }
    }
}
