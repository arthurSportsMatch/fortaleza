package com.sportm.fortaleza;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.LojaVirtualItemAdapter;
import data.FilterData;
import data.ProdutoFData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

public class LojaFortalezaActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    LojaVirtualItemAdapter adapter;
    String filter = "";
    String search = "";

    private static final int OP_ID_GET_PRODUTOS         = 0;
    private static final int OP_ID_GET_FILTERS          = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loja_fortaleza);

        mHolder = new ViewHolder();
        getProdutos(filter,search);
        getFilters();
        initActions();
    }

    void initActions (){

        mHolder.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHolder.filtros.getVisibility() == View.VISIBLE){
                    mHolder.filtros.setVisibility(View.GONE);
                }else{
                    mHolder.filtros.setVisibility(View.VISIBLE);
                }
            }
        });

        mHolder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHolder.filtros.getVisibility() == View.VISIBLE){
                    mHolder.filtros.setVisibility(View.GONE);
                }
            }
        });

        mHolder.searchBar.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    search = mHolder.searchBar.getText().toString();
                    getProdutos(filter, search);
                }
                return (keyCode == KeyEvent.KEYCODE_ENTER);
            }
        });

        mHolder.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search = mHolder.searchBar.getText().toString();
                getProdutos(filter, search);
            }
        });
    }

    void getFilters (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_FILTERS_LOJA_VIRTUAL, OP_ID_GET_FILTERS, this).execute();
    }

    void getProdutos (String filters, String search){
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        if(!filters.isEmpty())
            params.put("filter", filters);
        if(!search.isEmpty())
            params.put("search", search);

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PRODUTOS_LOJA_VIRTUAL, OP_ID_GET_PRODUTOS, this).execute(params);
    }

    void initProdutos (List<ProdutoFData> produtos){
        int colums = 2;

        if(adapter == null){
            adapter = new LojaVirtualItemAdapter(activity, produtos, new LojaVirtualItemAdapter.Listener() {
                @Override
                public void onClick(ProdutoFData prod) {
                    if(prod.getEmbed() == 1){
                        Intent intent = new Intent(activity, NavegadorActivity.class);
                        intent.putExtra("link", prod.getUrl());
                        startActivity(intent);
                    }else{
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(prod.getUrl()));
                        startActivity(i);
                    }
                }
            });
            mHolder.recyclerView.setAdapter(adapter);
            mHolder.recyclerView.setLayoutManager(new GridLayoutManager(activity, colums, RecyclerView.VERTICAL, false));
        }else{
            adapter.changeData(produtos);
        }
    }

    void initFilters (List<FilterData> data){
        data.add(0, new FilterData(0,"Todos"));

        for(FilterData pos : data){

            RadioButton button = (RadioButton) LayoutInflater.from(activity).inflate(R.layout.include_radio_button, null, false);
            float margin = getResources().getDimension(R.dimen.margin_10);
            float pading = getResources().getDimension(R.dimen.margin_15);

            RadioGroup.LayoutParams param = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            button.setPadding((int)pading,(int)margin,(int)pading,(int)margin);

            button.setText(pos.getTitulo());
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP , 14);
            button.setMaxLines(1);

            button.setBackground(getResources().getDrawable(R.drawable.radio_flat_selector_rec));
            button.setTextColor(getResources().getColorStateList(R.color.radio_white_black));

            final String ID = String.valueOf(pos.getId());
            button.setLayoutParams(param);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_FILTROS_SELECIONAR, ID).execute();
                    int id = v.getId();
                    mHolder.radioGroup.check(id);
                    if(ID.equals("0")){
                        filter = "";
                        getProdutos(filter,search);
                        //resetData = true;
                    }else{
                        filter = ID;
                        getProdutos(filter,search);
                        //resetData = true;
                    }
                    mHolder.filtros.setVisibility(View.GONE);
                }
            });

            mHolder.radioGroup.addView(button);
        }
    }

    //region AsyncOperation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_GET_PRODUTOS : {
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject json = response.getJSONObject("Object");
                            ProdutoFData[] data = gson.fromJson(json.getString("itens"), ProdutoFData[].class);
                            if(data != null){
                                List<ProdutoFData> produtos = new ArrayList<>(Arrays.asList(data));
                                initProdutos(produtos);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
            case OP_ID_GET_FILTERS : {
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject json = response.getJSONObject("Object");
                            FilterData[] data = gson.fromJson(json.getString("itens"), FilterData[].class);
                            if(data != null){
                                List<FilterData> filters = new ArrayList<>(Arrays.asList(data));
                                initFilters(filters);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_GET_PRODUTOS : {

            }
            break;
        }
    }

    class ViewHolder {
        EditText searchBar;
        ImageView search;
        ImageView filter;
        RecyclerView recyclerView;
        RadioGroup radioGroup;
        View filtros;
        View close;

        public ViewHolder(){
            searchBar = findViewById(R.id.edt_search);
            search = findViewById(R.id.img_search);
            filter = findViewById(R.id.img_filter);
            recyclerView = findViewById(R.id.recycler_produtos);
            radioGroup = findViewById(R.id.radio_group);
            filtros = findViewById(R.id.filtros);
            close = findViewById(R.id.close);
        }
    }

}
