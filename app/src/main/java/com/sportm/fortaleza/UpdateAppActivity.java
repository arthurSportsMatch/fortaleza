package com.sportm.fortaleza;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class UpdateAppActivity extends MainActivity {
    ViewHolder mHolder;
    String message = "";
    String url = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_app);

        Intent intent = getIntent();
        if(intent.hasExtra("msg")){
            message = intent.getStringExtra("msg");
        }

        if(intent.hasExtra("url")){
            url = intent.getStringExtra("url");
        }

        mHolder = new ViewHolder();
        initVars();
        initAction();
        forceLogout();
    }

    void forceLogout (){
        UserInformation.setSessionId(0);
        prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_SERVER);
        UTILS.ClearInformations(activity);
    }

    void initVars (){
        if(!message.isEmpty())
            mHolder.message.setText(message);

        if(!url.isEmpty()){
            mHolder.tryAgain.setText("Baixar");
        }
    }

    void initAction (){
        mHolder.tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!url.isEmpty()){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }else{
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING,0,emptyAsync, TRACKING.TRACKING_BLOCK_TRYAGAIN).execute();
                    getVersion();
                }
            }
        });
    }

    public void getVersion (){
        mHolder.progress.setVisibility(View.VISIBLE);
        mHolder.tryAgain.setVisibility(View.INVISIBLE);
        new AsyncOperation(this, AsyncOperation.TASK_ID_CHECK_VERSION, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                mHolder.progress.setVisibility(View.GONE);
                mHolder.tryAgain.setVisibility(View.VISIBLE);
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                boolean success = false;
                String msg = "";
                int status = 0;
                try{
                    if( (response.has("Message")) && (response.get("Message") != JSONObject.NULL) ){
                        msg = response.getString("Message");
                    }
                    if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) ){
                        status = response.getInt("Status");
                    }

                    success = ( (status == HttpURLConnection.HTTP_OK) || (status == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) );

                    if( (response.has("sId")) && (response.get("sId") != JSONObject.NULL) ){
                        //UserInformation.getUserData().setSessionId(response.getInt("sId"));
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog("Error", e);
                }

                if(success){

                    if(status == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) {
                        Toast.makeText(UpdateAppActivity.this, msg, Toast.LENGTH_LONG).show();
                    }
                    else {
                        finish();
                        Intent intent = new Intent(UpdateAppActivity.this, SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute();
    }

    class ViewHolder {
        TextView message;
        TextView tryAgain;
        View progress;

        public ViewHolder (){
            message = findViewById(R.id.splash_update);
            tryAgain = findViewById(R.id.splash_try_again);
            progress = findViewById(R.id.progress);
        }
    }
}
