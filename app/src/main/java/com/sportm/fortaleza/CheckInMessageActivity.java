package com.sportm.fortaleza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class CheckInMessageActivity extends AppCompatActivity {
    ViewHolder mHolder;
    String tittle, message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_message);

        mHolder = new ViewHolder();


        Intent intent = getIntent();
        if(intent.hasExtra("tit")){
            tittle = intent.getStringExtra("tit");
            message = intent.getStringExtra("msg");
            initValues();
        }
    }

    void initValues (){
        mHolder.tittle.setText(tittle);
        mHolder.message.setText(message);
    }

    class ViewHolder {
        TextView tittle, message;

        public ViewHolder(){
            tittle = findViewById(R.id.txt_tittle);
            message = findViewById(R.id.txt_message);
        }
    }
}
