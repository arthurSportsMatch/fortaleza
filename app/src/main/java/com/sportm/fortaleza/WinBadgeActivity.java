package com.sportm.fortaleza;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.BadgeData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class WinBadgeActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    private static final int OP_GET_BADGES              = 0;
    List<BadgeData> badges = new ArrayList<>();
    int listItemValue = 0;

    ViewHolder mHolder;

    Animation animPop, fadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win_badge);

        mHolder = new ViewHolder();
        initValues();
        initFirtAnimation();

        Intent intent = getIntent();
        if(intent.hasExtra("badges")){
            Gson gson = new Gson();
            BadgeData[] data = gson.fromJson(intent.getStringExtra("badges"), BadgeData[].class);
            if(data != null && data.length > 0) {
                badges = new ArrayList<>(Arrays.asList(data));
                showBadges();
            }
        }else{
            getNewBadges();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Toast.makeText(activity, "Receba seus badges!", Toast.LENGTH_SHORT).show();
    }

    void initValues (){
        animPop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pop);
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
    }

    void initFirtAnimation (){

    }

    void getNewBadges (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_BADGES, OP_GET_BADGES, this).execute();
    }

    void showBadges (){
        if(badges.size() > 0) {
            final BadgeData badge = badges.get(listItemValue);

            UTILS.getImageAt(activity, badge.getImg(), mHolder.badgeImage);

            mHolder.badgeImage.startAnimation(animPop);
            mHolder.badgeTitle.startAnimation(fadeIn);
            mHolder.badgeDescription.startAnimation(fadeIn);


            mHolder.badgeTitle.setText(badge.getNome());
            mHolder.badgeDescription.setText(badge.getDescricao());

            if(listItemValue < badges.size()-1){
                mHolder.nextBadge.setText("Proximo");
                mHolder.nextBadge.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_CONQUISTA_PROXIMA, badge.getId()).execute();
                        listItemValue ++;
                        showBadges();
                    }
                });

            }else
                {
                mHolder.nextBadge.setText("Sair");
                mHolder.nextBadge.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_CONQUISTA_FECHAR, badge.getId()).execute();
                        finish();
                    }
                });
            }
        }
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_BADGES:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            BadgeData[] data = gson.fromJson(obj.getString("itens"), BadgeData[].class);
                            if(data != null && data.length > 0){
                                badges = new ArrayList<>(Arrays.asList(data));
                                showBadges();
                            }else{
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        ImageView badgeImage;
        TextView badgeTitle;
        TextView badgeDescription;
        Button nextBadge;

        public ViewHolder (){
            badgeImage = findViewById(R.id.img_badges);
            badgeTitle = findViewById(R.id.txt_tittle);
            badgeDescription = findViewById(R.id.txt_description);

            nextBadge = findViewById(R.id.btn_next);
        }
    }
}
