package com.sportm.fortaleza;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import data.ProdutoData;
import data.UserData;
import informations.UserInformation;
import models.user.UserModel;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.user.UserViewModel;

public class ItemLojaActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    private UserViewModel userViewModel;

    int id, idCupom;
    String valor, valorST;
    String regulamento;
    boolean isOpeningRegulamenro = false;
    boolean isSocio = false;

    private static final int OP_GET_PRODUTO                 = 0;
    private static final int OP_GET_PRODUTO_REGULAMENTO     = 1;
    private static final int OP_SET_COMPRA                  = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_loja);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        idCupom = intent.getIntExtra("idCupom", -1);
        valor = intent.getStringExtra("valor");
        valorST = intent.getStringExtra("valorST");
        mHolder = new ViewHolder();

        initAction();
        getProduto();
        //getRegulamento();
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = new ViewModelProvider(ItemLojaActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, this::updateUser);
        userViewModel.getUser(false);
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
    }

    void updateUser(UserModel user){
        isSocio = user.getSocioTorcedor() == 1;
    }

    void initAction (){
        mHolder.regulamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isOpeningRegulamenro){
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_LOJA_PRODUTO_ABRIRREGULAMENTO, id).execute();
                    getRegulamento();
                }
            }
        });

        mHolder.txtCupom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setClipboard(ItemLojaActivity.this, mHolder.txtCupom.getText().toString());
            }
        });
    }

    private void setClipboard(Context context, String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(activity,"Cupom Copiado!", Toast.LENGTH_SHORT).show();
    }

    void getProduto (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        if(idCupom > 0)
            params.put("idCupom", idCupom);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PRODUTO, OP_GET_PRODUTO, this).execute(params);
    }

    void getRegulamento (){
        isOpeningRegulamenro = true;
        if(regulamento == null){
            Hashtable<String, Object> params = new Hashtable<>();
            params.put("id", id);
            new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PRODUTO_REGULAMENTO, OP_GET_PRODUTO_REGULAMENTO, this).execute(params);
        }
    }

    void getUser (){
        userViewModel.getUser();
    }

    void initProduto(final ProdutoData data){
        Log.d("Loja", data.toString());

        UTILS.getImageAt(activity,data.getImg(), mHolder.itemImg, new RequestOptions()
                .error(R.drawable.ic_bag_loja)
                .placeholder(R.drawable.ic_bag_loja));

        mHolder.description.setText(data.getNome());
        mHolder.regulamentoDescription.setText(data.getDescricao());

        if(valorST != null){
            mHolder.socioCoin.setVisibility(View.VISIBLE);
            mHolder.socioCoinText.setText(valorST + "*");
        }else{
            mHolder.socioCoin.setVisibility(View.GONE);
        }

        if(valor != null){
            mHolder.normalCoinText.setVisibility(View.VISIBLE);
            mHolder.normalCoinText.setText(valor);
        }else{
            mHolder.normalCoin.setVisibility(View.GONE);
        }

        if(data.getCupom()!= null){
            mHolder.confirmar.setVisibility(View.GONE);
            mHolder.socio.setVisibility(View.GONE);
            mHolder.txtExclusivo.setVisibility(View.GONE);
            mHolder.cupom.setVisibility(View.VISIBLE);
            mHolder.txtCupom.setText(data.getCupom());

            if(data.getDataExpiracao() != null){
                String expiration = UTILS.formatDateFromDB(data.getDataExpiracao());
                mHolder.txtExpiraCupom.setText("Expira em: " + expiration);
            }
        }


        if(valor == null && !isSocio){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mHolder.confirmar.getBackground().setTint(getResources().getColor(R.color.colorGray3));
            }
            mHolder.confirmar.setEnabled(false);
        }

        mHolder.confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_LOJA_PRODUTO_CONFIRMAR, data.getId()).execute();
                initPopupCompra(data);
            }
        });
    }

    void showRegulamento (String data){
        isOpeningRegulamenro = false;
        if(regulamento != null){
            regulamento = data;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_instagram, null);
        ImageView img = customLayout.findViewById(R.id.img_storie);
        WebView web = customLayout.findViewById(R.id.img_storie_video);

        img.setVisibility(View.GONE);
        web.setVisibility(View.VISIBLE);

        loadHtmlEmbed(web, data);

        builder.setView(customLayout);

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    void initPopupCompra(final ProdutoData data){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // set the custom layout
        View customLayout = getLayoutInflater().inflate(R.layout.dialog_item_compra, null);
        ImageView imgProduto = customLayout.findViewById(R.id.img_produto);
        TextView txtNome = customLayout.findViewById(R.id.txt_description);
        LinearLayout viewPreco = customLayout.findViewById(R.id.view_coin_normal);
        TextView txtPreco = customLayout.findViewById(R.id.txt_coin_normal);
        LinearLayout viewPrecoSocio = customLayout.findViewById(R.id.view_coin_socio);
        TextView txtPrecoSocio = customLayout.findViewById(R.id.txt_coin_socio);
        Button buttonCancel = customLayout.findViewById(R.id.btn_cancelar);
        Button buttonConfirm = customLayout.findViewById(R.id.btn_confirmar);

        UTILS.getImageAt(activity, data.getImg(), imgProduto, new RequestOptions()
                .error(R.drawable.ic_bag)
                .placeholder(R.drawable.ic_bag));

        txtNome.setText(data.getNome());

        if(valorST != null){
            txtPrecoSocio.setText(valorST + "*");
        }else{
            viewPrecoSocio.setVisibility(View.GONE);
        }

        if(valor != null){
            txtPreco.setText(valor);
        }else{
            viewPreco.setVisibility(View.GONE);
        }

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        final AlertDialog finalHolder = dialog;
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_LOJA_RESGATAR_FECHAR, id).execute();
                finalHolder.dismiss();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PurchaseItem(data.getId());
                finalHolder.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    void PurchaseItem (int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_COMPRA, OP_SET_COMPRA, this,TRACKING.TRACKING_LOJA_RESGATAR_CONFIRMAR, id).execute(params);
    }

    //Faz o load dos valores da receita em html
    private void loadHtmlEmbed(WebView web, String embed) {

        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<!DOCTYPE html>");
            stringBuilder.append("<html>");
            stringBuilder.append("<head> <meta name=\"viewport\" content=\"width=100, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no\" /> </head>");
            stringBuilder.append("<body>");
            stringBuilder.append("<style> span { display: block; max-width: 100%; height: auto; } </style>");


            List<String> list = new ArrayList<>();

            List<String> images = UTILS.getStringsBetweenStrings(embed, "<img src=\"", "\"");
            int size = images.size();
            String replacement = "%s\" onclick=\"AndroidFunction.expand('%s')";
            for(int i = 0; i < size; i++){
                String img = images.get(i).replace("../../Content/images/", UTILS.getRandomURL());
                embed = embed.replaceFirst(images.get(i), String.format(replacement, img, img));
            }

            stringBuilder.append(embed);
            stringBuilder.append("\n<script language=\"javascript\">\n" +
                    "function expand(str) {\n" +
                    "    AndroidFunction.expand(str);\n" +
                    "}\n" +
                    "</script>");


            stringBuilder.append("</body>");
            stringBuilder.append("</html>");


            String fullHtml = stringBuilder.toString();

            web.getSettings().setUseWideViewPort(true);
            web.getSettings().setLoadWithOverviewMode(true);
            web.loadData(fullHtml, "text/html; charset=utf-8", null);
            Log.d("Html-builder", fullHtml);
        }
        catch (Exception e){
            UTILS.DebugLog("Error", e);
        }
    }


    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){

            case OP_GET_PRODUTO:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            ProdutoData data = gson.fromJson(response.getString("Object"), ProdutoData.class);

                            if(data != null){
                                initProduto(data);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_GET_PRODUTO_REGULAMENTO :{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            String data = obj.getString("documentacao");

                            if(data != null){
                                showRegulamento(data);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case  OP_SET_COMPRA :{
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    try {
                        Toast.makeText(this, "Compra Realizada com sucesso", Toast.LENGTH_LONG).show();
                        JSONObject obj = response.getJSONObject("Object");
                        idCupom = obj.getInt("idCupom");
                        getProduto();
                        getUser();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    if(response.has("msg")){
                        try {
                            Toast.makeText(this, response.getString("msg"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case  OP_SET_COMPRA :{
                int status = 0;
                //pegar categoria de quiz

                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status != 200){
                    if(response.has("msg")){
                        try {
                            Toast.makeText(this, response.getString("msg"), Toast.LENGTH_LONG).show();
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }
    //endregion

    class  ViewHolder {
        View background;
        ImageView itemImg;
        TextView description;

        View normalCoin;
        TextView normalCoinText;

        View socioCoin;
        TextView socioCoinText;

        View socio;

        TextView regulamentoDescription;
        Button regulamento;
        Button confirmar;

        View cupom;
        TextView txtCupom;
        TextView txtExpiraCupom;

        TextView txtExclusivo;

        public ViewHolder(){
            background = findViewById(R.id.view_back);

            itemImg = findViewById(R.id.img_produto);
            description = findViewById(R.id.txt_description);

            normalCoin = findViewById(R.id.view_coin_normal);
            normalCoinText = findViewById(R.id.txt_coin_normal);

            socioCoin = findViewById(R.id.view_coin_socio);
            socioCoinText = findViewById(R.id.txt_coin_socio);

            socio = findViewById(R.id.view_socio_torcedor);

            regulamentoDescription = findViewById(R.id.web_view);
            regulamento = findViewById(R.id.btn_regulamento);
            confirmar = findViewById(R.id.btn_confirmar);

            cupom = findViewById(R.id.view_cupom);
            txtCupom = findViewById(R.id.txt_cupom);
            txtExpiraCupom = findViewById(R.id.txt_expiration);

            txtExclusivo = findViewById(R.id.view_txt_exclusivo);
        }
    }
}
