package com.sportm.fortaleza;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Hashtable;

import data.ServerCustomData;
import data.SocioData;
import data.UserData;
import informations.UserInformation;
import models.ErrorModel;
import models.MessageModel;
import models.user.UserModel;
import repository.server.ServerRepository;
import repository.socio.SocioRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.DialogDisplayer;
import utils.MasterBakersNotificationManager;
import utils.NetworkChangeReceiver;
import utils.TRACKING;
import utils.UTILS;
import vmodels.server.ServerViewModel;
import vmodels.socio.SocioViewModel;
import vmodels.user.UserViewModel;

import static repository.MainRepository.SHARED_KEY_USER;
import static utils.MasterBakersNotificationManager.PUSH_ID_AUDIO;
import static utils.MasterBakersNotificationManager.PUSH_ID_CAMPINHO;
import static utils.MasterBakersNotificationManager.PUSH_ID_NOTICIA;
import static utils.MasterBakersNotificationManager.PUSH_ID_PESQUISA_ESPECIFICA;
import static utils.MasterBakersNotificationManager.PUSH_ID_PLAYERPROFILE;
import static utils.MasterBakersNotificationManager.PUSH_ID_QUIZ;


public class SplashActivity extends MainActivity  implements AsyncOperation.IAsyncOpCallback {

    private ViewHolder mHolder;
    private UserViewModel userViewModel;
    private ServerViewModel serverViewModel;
    private SocioViewModel socioViewModel;

    Animation animFadeIn, animFadeOut , animSlideUp , animSlideDown;

    private String gcm_token = "";
    boolean getVersionOk = false, animationOK = true;

    private static final int RQ_UPDATE_SUGGESTED = 0;
    private static final int OP_GET_GCM_TOKEN = 0;
    private static final int OP_CHECK_VERSION = 1;
    private boolean trigger = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mHolder = new ViewHolder();
        initValues();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getVersionOk = false;
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        //Server
        ServerRepository serverRepository = new ServerRepository(activity, prefs);
        serverViewModel = ViewModelProviders.of(SplashActivity.this).get(ServerViewModel.class);
        serverViewModel.setmRepository(serverRepository);
        serverViewModel.getServers().observe(this, new Observer<ServerCustomData>() {
            @Override
            public void onChanged(ServerCustomData server) {
                //Pega o servidores (salvos ou chama o getVersion novamente)
                int id = server.getIdUser();
                UserInformation.setUserId(id);
                serverOK();
            }
        });


        //User
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = ViewModelProviders.of(SplashActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                initUser(user);
            }
        });

        userViewModel.getError().observe(this, new Observer<ErrorModel>() {
            @Override
            public void onChanged(ErrorModel error) {
                Log.d("Async", error.getMsg());
                initNoUser();
            }
        });

        //Socio
        SocioRepository socioRepository = new SocioRepository(activity, prefs);
        socioViewModel = ViewModelProviders.of(SplashActivity.this).get(SocioViewModel.class);
        socioViewModel.setmRepository(socioRepository);
        socioViewModel.getMessage().observe(this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel message) {
                if(message.getId() == 1){
                    userViewModel.getSocio();
                }
            }
        });
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        userViewModel.getUserData().removeObservers(this);
        userViewModel.getError().removeObservers(this);

        serverViewModel.getServers().removeObservers(this);

        socioViewModel.getMessage().removeObservers(this);
    }

    //Inicia valores
    private void initValues (){
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
    }

    //Pega o usuario do celular
    private void getUser(){
        //Caso não tenha servidor busca um novo passando uma id de usuario (que pode ser nula)
        if(UserInformation.getServerData() == null){
            serverViewModel.getServer(UserInformation.getUserId());
            return;
        }

        //Carrega Usuario Salvo
        userViewModel.getUser(true);

        gcm_token = prefs.getString(CONSTANTS.SHARED_PREFS_KEY_GCM_TOKEN, "");
        ConfigurePushNotification();
    }

    //Pega Api de socio torcedor
    void getSocioAPI (){
        socioViewModel.getSocioAPI();
    }

    //Inicia o usuario (prepara o app para caso não tenha um usuario)
    private void initUser (UserModel user){
        if(user.getCpf() != null && !user.getCpf().isEmpty()){
            getSocioAPI();
        }

        Bundle b = getIntent().getExtras();
        boolean hasBundle = b != null;

        UTILS.setUserLoggedIn(true);
        if(hasBundle){
            initMenuByPush(b);
        }else{
            goMainMenu();
        }
    }

    //Inicia sem Usuario
    private void initNoUser (){
        UTILS.setUserLoggedIn(false);
        goLoginMenu();
    }

    //Quando receber push background
    void initMenuByPush (Bundle b){
        int idPush = b.getInt("idPush", 0);
        int pushType = b.getInt("t", 0);

        Bundle pushBundle = new Bundle();

        pushBundle.putInt("t", pushType);
        pushBundle.putInt("idPush", idPush);

        switch (pushType){

            case PUSH_ID_NOTICIA:
                pushBundle.putInt("id", b.getInt("id", -1));
                break;

            case PUSH_ID_AUDIO:
                pushBundle.putInt("id", b.getInt("id", -1));
                break;

            case PUSH_ID_QUIZ:
                pushBundle.putInt("idQuiz", b.getInt("idQuiz", -1));
                break;

            case PUSH_ID_PESQUISA_ESPECIFICA:
                pushBundle.putInt("idPesquisa", b.getInt("idPesquisa", -1));
                break;

            case PUSH_ID_PLAYERPROFILE:
                pushBundle.putInt("idPlayer", b.getInt("idPlayer", 0));
                break;

            case PUSH_ID_CAMPINHO:
                pushBundle.putInt("escalacao", b.getInt("escalacao", -1));
                break;
        }

        Intent resultIntent =  null;

        //Se esta logado
        if(UTILS.isUserLoggedIn()) {
            resultIntent = new Intent(this, StartMenuActivity.class);
            Bundle _b = new Bundle();
            _b.putBundle("pushBundle", pushBundle);
            _b.putInt(CONSTANTS.LOADING_SCREEN_KEY, CONSTANTS.SCREEN_REDIRECT_PUSH);
            resultIntent.putExtras(_b);

            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(resultIntent);
            finish();
        }
    }

    //Se os Servidores tiverem salvos
    private void serverOK (){
        if(UTILS.isNetworkAvailable(this)) {
            new AsyncOperation(activity, AsyncOperation.TASK_ID_CHECK_VERSION, OP_CHECK_VERSION, this).execute();
        }else{
            NetworkChangeReceiver.currentActivity = this;
            CallNoConnectionWarning();
        }
    }

    //Se o getVersion teve um resultado ok
    private void goLoginMenu (){
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //Tudo OK Vai para o menu do app
    private void goMainMenu (){
        finish();
        Intent intent = new Intent(activity, StartMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //region AsyncOperations
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_GCM_TOKEN:{
            }
            case OP_CHECK_VERSION:{
                boolean success = false;
                String msg = "";
                String url = "";
                int status = 0;
                try{
                    if((response.has("serverVersion")) && (response.get("serverVersion") != JSONObject.NULL) ) {
                        int serverVersion = response.getInt("serverVersion");
                        if(serverVersion > UserInformation.getServerData().getServerVersion()){
                            serverViewModel.getServer(UserInformation.getUserId());
                            return;
                        }
                    }

                    if((response.has("Message")) && (response.get("Message") != JSONObject.NULL) ){
                        msg = response.getString("Message");
                    }
                    if((response.has("msg")) && (response.get("msg") != JSONObject.NULL) ){
                        msg = response.getString("msg");
                    }
                    if( (response.has("url")) && (response.get("url") != JSONObject.NULL) ){
                        url = response.getString("url");
                    }
                    if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) ){
                        status = response.getInt("Status");
                    }
                    if( (response.has("rv")) && (response.get("rv") != JSONObject.NULL) ) {
                        double latestRegVer = response.getDouble("rv");
                        MainActivity.latestRegVer = latestRegVer;
                    }

                    if( (response.has("idServidor")) && (response.get("idServidor") != JSONObject.NULL) ) {
                        int idServidor = response.getInt("idServidor");

                        if(UserInformation.getServerData().getServerId() != idServidor){
                            UserInformation.getServerData().setServerId(idServidor);
                            UTILS.salvarServidor(UserInformation.getServerData());
                            CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServer(idServidor) + "api/");
                        }
                    }

                    if( (response.has("api")) && (response.get("api") != JSONObject.NULL) ) {
                        String version = response.getString("api");
                        if(!CONSTANTS.serverURL.contains(version)) {
                            CONSTANTS.serverVersion = version + "/";
                            CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                        }
                    }

                    success = ( (status == HttpURLConnection.HTTP_OK) || (status == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) );

                    if((response.has("sId")) && (response.getInt("sId") > 0)){
                        UserInformation.setSessionId((int) response.get("sId"));
                    }

                    if(response.has("regulamento")){
                        MainActivity.regulamento = response.getString("regulamento");
                        Log.d("Regulamento", regulamento);
                    }

                } catch (JSONException e) {
                    UTILS.DebugLog("Error", e);
                }

                if(success){
                    if(status == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) {
                        CallUpdateSuggestion(msg, url);
                    }else if(status == CONSTANTS.ERROR_CODE_MAINTENANCE){
                        CallUpdateSuggestion(msg);
                    } else {
                        getVersionOk = true;
                        if(animationOK){
                            getUser();
                        }

                    }
                } else{
                    CallUpdateSuggestion(msg, url);
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_GCM_TOKEN: {
                UTILS.DebugLog(CONSTANTS.ASYNCDEBUG, "Error with GCM.");
                Proceed();
            }
            break;

            case OP_CHECK_VERSION: {
                UTILS.DebugLog(CONSTANTS.ASYNCDEBUG, "Error with Version Check.");
                Proceed();
            }
            break;
        }
    }
    //endregion

    void CallUpdateSuggestion(String message){
        finish();
        Intent intent = new Intent(SplashActivity.this, UpdateAppActivity.class);
        intent.putExtra("msg", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //Configura a push notifcation
    public void ConfigurePushNotification(){
        if(UTILS.checkGooglePlayServiceAvailability(getApplicationContext())){
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    UserInformation.getUserData().setGcm(instanceIdResult.getToken());
                    MasterBakersNotificationManager.gcm = instanceIdResult.getToken();
                    MainMenuActivity.setAPNSToUpdate(instanceIdResult.getToken(), true);
                    Proceed();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Proceed();
                }
            });
        }
        else{
            AskForGooglePlayServicesUpdate();
        }
    }

    // coloca valores do gcm token
    void Proceed (){
        UserInformation.getUserData().setGcm(gcm_token);
        prefs.put(CONSTANTS.SHARED_PREFS_KEY_GCM_TOKEN, gcm_token);
    }

    void AskForGooglePlayServicesUpdate(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);

        alertDialogBuilder.setTitle("");
        alertDialogBuilder
                .setMessage("error")
                .setCancelable(false)
                .setPositiveButton("Sim",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(SplashActivity.this);
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        Proceed();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //Warning de nenhuma conexão com a internet
    private void CallNoConnectionWarning(){
        mHolder.carregando.setText("Sem internet");
        mHolder.carregando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();
            }
        });
    }

    //Classe que guarda todos os valores do view
    class ViewHolder {
        LinearLayout oferecimentos;
        ImageView logo;
        TextView carregando;

        public ViewHolder (){

            oferecimentos = findViewById(R.id.grid_oferecimento_imgs);
            logo = findViewById(R.id.splash_logo);
            carregando = findViewById(R.id.txt_carregando);
        }
    }
}
