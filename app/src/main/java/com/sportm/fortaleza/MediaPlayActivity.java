package com.sportm.fortaleza;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import uk.co.senab.photoview.PhotoViewAttacher;

public class MediaPlayActivity extends AppCompatActivity {

    public static final int MEDIA_PLAY_CONTENT_TYPE_IMAGE = 0;
    public static final int MEDIA_PLAY_CONTENT_TYPE_VIDEO = 1;
    public static final int MEDIA_PLAY_CONTENT_TYPE_URL_VID = 2;

    private int contentType = -1;
    private String content = "";
    private int seekBar = 0;

    ImageView imgMediaPlay;
    VideoView videoMediaPlay;
    WebView webView;

    PhotoViewAttacher photoViewAttacher;

    FrameLayout frameBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_play);

        frameBase = (FrameLayout)findViewById(R.id.frameBase);

        imgMediaPlay = (ImageView)findViewById(R.id.imgMediaPlay);
        videoMediaPlay = (VideoView)findViewById(R.id.videoMediaPlay);
        webView = (WebView)findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(this.getFilesDir().getAbsolutePath() + "/cache");
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDatabasePath(this.getFilesDir().getAbsolutePath() + "/databases");

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(frameBase);
        videoMediaPlay.setMediaController(mediaController);

        Bundle b = getIntent().getExtras();

        if(b != null){
            contentType = b.getInt("contentType", -1);
            content = b.getString("content", "");
            seekBar = b.getInt("seekBar", 0);

            if( (contentType >= 0) && (content.length() > 0) ){
                ConfigureMedia();
            }
            else{
                CallError("Erro ao carregar");
            }
        }
        else{
            CallError("Erro ao carregar");
        }
    }

    void ConfigureMedia(){
        switch (contentType){
            case MEDIA_PLAY_CONTENT_TYPE_IMAGE:
                imgMediaPlay.setVisibility(View.VISIBLE);
                videoMediaPlay.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);

                Glide.with(this)
                        .load(content)
                        .apply(new RequestOptions().transforms(new RoundedCorners(20)))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                if (photoViewAttacher != null) {
                                    photoViewAttacher.update();
                                } else {
                                    photoViewAttacher = new PhotoViewAttacher(imgMediaPlay);
                                }
                                return false;
                            }
                        })
                        .into(imgMediaPlay);

                break;

            case MEDIA_PLAY_CONTENT_TYPE_VIDEO:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                imgMediaPlay.setVisibility(View.GONE);
                videoMediaPlay.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);

                Uri uri = Uri.parse(content);
                videoMediaPlay.setVideoURI(uri);

                videoMediaPlay.seekTo(seekBar);

                videoMediaPlay.start();
                break;

            case MEDIA_PLAY_CONTENT_TYPE_URL_VID:

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                imgMediaPlay.setVisibility(View.GONE);
                videoMediaPlay.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);

                webView.loadUrl(content);// + "?rel=0&amp;autoplay=1");
                break;

            default:
                CallError("Erro ao carregar");
                break;
        }
    }

    void CallError(){
        CallError("Erro ao carregar");
    }

    void CallError(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
