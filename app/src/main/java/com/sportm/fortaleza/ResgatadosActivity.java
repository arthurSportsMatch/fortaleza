package com.sportm.fortaleza;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.ResgatadosAdapter;
import data.VoucherItemData;
import utils.AsyncOperation;

public class ResgatadosActivity extends MainActivity {

    ViewHolder mHolder;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resgatados);

        Intent intent = getIntent();
        if(intent.hasExtra("id"))
            id = intent.getIntExtra("id", 0);

        mHolder = new ViewHolder();
        getVoucherList(id);

        setSupportActionBar(mHolder.tolbar);
        setTitle("Resgatados");
    }

    void getVoucherList(int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);

        mHolder.load.setVisibility(View.VISIBLE);
        new AsyncOperation(this, AsyncOperation.TASK_ID_GET_VOUCHER_PRIZES_LIST, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                mHolder.load.setVisibility(View.GONE);
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Object")){

                    try {
                        Gson gson = new Gson();
                        VoucherItemData[] data = gson.fromJson(response.getString("Object"), VoucherItemData[].class);
                        List<VoucherItemData> list = new ArrayList<>(Arrays.asList(data));

                        ResgatadosAdapter adapter = new ResgatadosAdapter(list, getApplicationContext());
                        mHolder.view.setAdapter(adapter);
                        mHolder.view.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute(params);
    }

    class ViewHolder {
        Toolbar tolbar;
        RecyclerView view;
        ProgressBar load;

        public ViewHolder() {
            tolbar = findViewById(R.id.topbar);
            view = findViewById(R.id.recycler_premios_resgatados);
            load = findViewById(R.id.progress_loading);
        }
    }

}
