package com.sportm.fortaleza;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Hashtable;

import data.FragmentData;
import data.ServerCustomData;
import fragments.AgendaFragment;
import fragments.GamesFragment;
import fragments.HomeFragment;
import fragments.MenuLateralFragment;
import fragments.TimeFragment;
import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.FragmentHolder;
import utils.MasterBakersNotificationManager;
import utils.MoedasController;
import utils.TRACKING;
import utils.UTILS;
import utils.fragment.FragmentsHolderController;
import vmodels.fragment.FragmentHolderViewModel;
import vmodels.server.ServerViewModel;
import vmodels.utils.MoedasViewModel;

import static utils.CONSTANTS.LOADING_SCREEN_KEY;
import static utils.CONSTANTS.SCREEN_REDIRECT_PUSH;
import static utils.CONSTANTS.SCREEN_SEARCH;

public class StartMenuActivity extends MainActivity {

    private ViewHolder mHolder;
    private FragmentsHolderController mFragmentsC;
    private FragmentHolderViewModel mfragmentViewModel;
    private MoedasViewModel mMoedasViewModel;

    private boolean canChangeFragment;
    private boolean canGiveCoin;
    private boolean getVersionOK = false;
    private boolean hasPushToCall = false;

    private Screen currentScreen;
    private enum Screen{
        Home,
        Jogos,
        Time,
        Menu
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_menu);

        initValues();
        callGetVersion();
        checkBundle(getIntent());
        checkIfCameFromLogin(getIntent());
        updateApns(1, MasterBakersNotificationManager.gcm);
    }

    @Override
    public void onBackPressed() {
        if(!mFragmentsC.onBackPressed()){
            initCloseAppMessage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFragmentsC.getCurrentFragment().onActivityResult(requestCode,resultCode,data);
        if (resultCode == RESULT_OK) {
            checkBundle(data);
        }
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        mfragmentViewModel = new ViewModelProvider(StartMenuActivity.this).get(FragmentHolderViewModel.class);
        mfragmentViewModel.canChange().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean change) {
                canChangeFragment = change;
            }
        });
        mfragmentViewModel.getFragment().observe(this, new Observer<FragmentData>() {
            @Override
            public void onChanged(FragmentData fragment) {
                if(canChangeFragment){
                    mFragmentsC.addFragmentInFrameSimple(fragment.getFragment(), fragment.getName(), null);
                    mfragmentViewModel.setHasToChange(false);
                }
            }
        });

        mMoedasViewModel = new ViewModelProvider(StartMenuActivity.this).get(MoedasViewModel.class);
        mMoedasViewModel.canChange().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean change) {
                canGiveCoin = change;
            }
        });

        mMoedasViewModel.getMoedas().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer moedas) {
                if(canGiveCoin){
                    setPontos(moedas);
                    mMoedasViewModel.setHasToChange(false);
                }
            }
        });
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        mfragmentViewModel.getFragment().removeObservers(this);
        mMoedasViewModel.getMoedas().removeObservers(this);
    }

    @Override
    public void bindBroadcast() {
        super.bindBroadcast();
    }

    @Override
    public void unBindBroadcast() {
        super.unBindBroadcast();
    }

    void initValues (){
        mHolder = new ViewHolder();
        mFragmentsC = new FragmentsHolderController(getSupportFragmentManager(), R.id.framelayout);
    }

    void initActions (){
        initBottonView();

        if(hasPushToCall){
            //Toast.makeText(activity, "Tem um erro Aqui", Toast.LENGTH_LONG).show();
            checkBundle(getIntent());
        }else{
            //Toast.makeText(activity, "Abrindo Home", Toast.LENGTH_LONG).show();
            mFragmentsC.changeToFragmentContext(new HomeFragment(), "HomeFragment", "Home", false);
        }
    }

    //Faz update da Apns
    public void updateApns(final int allow, String gcmToken){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("apns", gcmToken);
        params.put("allow", allow);
        new AsyncOperation(this, AsyncOperation.TASK_ID_SET_USER_APNS, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                prefs.put(CONSTANTS.SHARED_PREFS_KEY_NEED_TO_UPDATE_APNS, String.valueOf(allow));
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {
            }
        }).execute(params);
    }

    void initBottonView (){
        mHolder.loading.setVisibility(View.GONE);
        mHolder.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()){
                    case R.id.home:
                        goHome();
                        return true;

                    case R.id.functions:
                        goJogos();
                        return true;

                    case R.id.time:
                        goTime();
                        return true;

                    case R.id.perfil:
                        goMenu();
                        return true;
                }
                return false;
            }
        });
    }

    void goHome (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_SELECTHOME).execute();
        mFragmentsC.changeToFragmentContext(new HomeFragment(), "HomeFragment", "Home", currentScreen == Screen.Home);
        currentScreen = Screen.Home;
    }

    void goJogos (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_JOGOS_SELECT).execute();
        mFragmentsC.changeToFragmentContext(new GamesFragment(), "GamesFragment", "Jogos", currentScreen == Screen.Jogos);
        currentScreen = Screen.Jogos;
    }

    void goTime (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_TIME_SELECIONAR).execute();
        mFragmentsC.changeToFragmentContext(new TimeFragment(), "TimeFragment", "Time", currentScreen == Screen.Time);
        currentScreen = Screen.Time;
    }

    void goMenu (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_MENU_SELECIONAR).execute();
        mFragmentsC.changeToFragmentContext(new MenuLateralFragment(), "MenuLateralFragment", "Menu", currentScreen == Screen.Menu);
        currentScreen = Screen.Menu;
    }

    void checkIfCameFromLogin (Intent _intent){
        if(getIntent().hasExtra("login")){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setMessage("Deseja receber as notificações")
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateApns(1, MasterBakersNotificationManager.gcm);
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateApns(0, MasterBakersNotificationManager.gcm);
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }

    //Verifica qual tipo de bundle deve colocar
    void checkBundle (Intent _intent){
        Bundle b = _intent.getExtras();
        Bundle pushBundle = b;
        int screenToLoad = -1;

        if((b != null) && (b.containsKey(LOADING_SCREEN_KEY)) ){
            screenToLoad = b.getInt(LOADING_SCREEN_KEY);

            switch (screenToLoad) {
                //Quando o push leva para uma tela nova
                case SCREEN_REDIRECT_PUSH:
                    pushBundle = b.getBundle("pushBundle");
                    break;

                //Quando o push é de tipo diferente
                case SCREEN_SEARCH:
                    pushBundle = b;
                    break;
            }


            if(getVersionOK){
                callPush(pushBundle);
            }else{
                hasPushToCall = true;
            }
        }else{
            Log.d("Push","Não foi chamado");
        }
    }

    //Faz chamada do push
    void callPush(Bundle pushBundle){

        if(getIntent() != null && getIntent().getExtras() != null){
            getIntent().getExtras().clear();
        }

        if((pushBundle != null) && (pushBundle.containsKey("idPush") ) ){
            int type = pushBundle.getInt("t", -1);

            Hashtable<String, Object> params = new Hashtable<>();
            params.put("idPush", pushBundle.getInt("idPush"));
            new AsyncOperation(this, AsyncOperation.TASK_ID_SET_NOTIFICATION, 999, emptyAsync).execute(params);

            switch (type){
                case MasterBakersNotificationManager.PUSH_ID_DEFAUT: {
                    //
                }
                case MasterBakersNotificationManager.PUSH_ID_ANIVERSARIO:{

                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_NOTICIA: {
                    Intent intent = new Intent(activity, NoticiaActivity.class);
                    intent.putExtra("id", pushBundle.getInt("id"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_QUIZ: {
                    mFragmentsC.addFragmentInFrameSimple(new GamesFragment("quiz", pushBundle.getInt("idQuiz")), "GamesFragment", "Jogos");
                    mHolder.bottomNavigation.getMenu().findItem(R.id.functions).setChecked(true);
                }
                break;


                case MasterBakersNotificationManager.PUSH_ID_PESQUISA: {
                    mFragmentsC.addFragmentInFrameSimple(new GamesFragment("pesquisa"), "GamesFragment", "Jogos");
                    mHolder.bottomNavigation.getMenu().findItem(R.id.functions).setChecked(true);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PESQUISA_ESPECIFICA: {
                    mFragmentsC.addFragmentInFrameSimple(new GamesFragment("pesquisa", pushBundle.getInt("idPesquisa")), "GamesFragment", "Jogos");
                    mHolder.bottomNavigation.getMenu().findItem(R.id.functions).setChecked(true);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PALPITES: {
                    mFragmentsC.addFragmentInFrameSimple(new GamesFragment("palpite"), "GamesFragment", "Jogos");
                    mHolder.bottomNavigation.getMenu().findItem(R.id.functions).setChecked(true);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PLAYERPROFILE: {
                    Intent intent = new Intent(this, PerfilAtletaActivity.class);
                    intent.putExtra("id", pushBundle.getInt("idPlayer"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_MEMBERSHIP: {
                    Intent intent = new Intent(this, SocioTorcedorActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_AUDIO: {
                    Intent intent = new Intent(activity, PlayerActivity.class);
                    intent.putExtra("push", true);
                    intent.putExtra("id", pushBundle.getInt("id"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_AGENDA: {
                    mFragmentsC.addFragmentInFrameSimple(new AgendaFragment(), "AgendaFragment", null);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_CAMPINHO: {
                    Intent intent = new Intent(this, EscalarTimeActivity.class);
                    intent.putExtra("esquema", pushBundle.getInt("escalacao"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_LOJA: {
                    Intent intent = new Intent(this, LojaActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_ALTERAR_DADOS: {
                    Intent intent = new Intent(this, ConfigurationActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_RANK: {
                    Intent intent = new Intent(this, RankingActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_DETALHES_JOGO:{
                    Intent intent = new Intent(this, MatchHistoryActivity.class);
                    intent.putExtra("id", pushBundle.getInt("idJogo"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PRODUTO:{
                    Intent intent = new Intent(this, ItemLojaActivity.class);
                    intent.putExtra("id", pushBundle.getInt("idProduto"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_AGENDA_JOGO:{
                    mFragmentsC.addFragmentInFrameSimple(new AgendaFragment(pushBundle.getInt("idAgenda")), "AgendaFragment", null);
                }
                break;

                default: finish();
            }
        }
    }

    void setPontos (int plus){
        MoedasController.showNewMoedas(activity, mHolder.showMoedas, plus);
    }

    void callGetVersion (){
        new AsyncOperation(this, AsyncOperation.TASK_ID_CHECK_VERSION, 999, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                boolean success = false;
                String msg = "";
                int status = 0;
                try{
                    if( (response.has("Message")) && (response.get("Message") != JSONObject.NULL) ){
                        msg = response.getString("Message");
                    }

                    if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) ){
                        success = (response.getInt("Status") == HttpURLConnection.HTTP_OK);
                    }

                    if( (response.has("sId")) && (response.getInt("sId") > 0)){
                        UserInformation.setSessionId((int) response.get("sId"));
                    }

//                    if( (response.has("idServidor")) && (response.get("idServidor") != JSONObject.NULL) ) {
//                        int idServidor = response.getInt("idServidor");
//
//                        if(UserInformation.getServerData().getServerId() != idServidor){
//                            UserInformation.getServerData().setServerId(idServidor);
//                            UTILS.salvarServidor(UserInformation.getServerData());
//                            CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServer(idServidor) + "api/");
//                            CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
//                        }
//                    }
                } catch (JSONException e) {
                    UTILS.DebugLog(CONSTANTS.ASYNCDEBUG, e);
                }

                if(success){
                    getVersionOK = true;
                    initActions();
                }else{
                    getVersionOK = false;
                    callGetVersion();
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute();
    }

    private class ViewHolder {
        LinearLayout showMoedas;
        FrameLayout frameLayout;
        FrameLayout loading;
        BottomNavigationView bottomNavigation;

        public ViewHolder (){
            frameLayout = findViewById(R.id.framelayout);
            loading = findViewById(R.id.loading_card);
            bottomNavigation = findViewById(R.id.navgation);
            showMoedas = findViewById(R.id.show_moedas);
        }
    }
}