package com.sportm.fortaleza;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Locale;

import data.Audio;
import data.PlaylistItemData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class PlayerActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;

    boolean synquing    = false;
    boolean audioLoaded = false;
    boolean isPlaying   = false;            // Se esta tocando

    int duracao         = 1000;             // Tamanho do audio
    int currentTime     = 0;                // Current time
    int playlistAudioID = 0;

    String textDuracao;

    boolean fromPush = false;
    boolean mRepeat = true;
    boolean mShufle = false;
    boolean mSuperRepeat = false;
    boolean mLyric = false;
    boolean mAudio = true;
    boolean hasLyrics = false;

    Handler handler = null;
    Runnable runnable;

    private static final int OP_GET_AUDIO              = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        Intent intent = getIntent();
        fromPush = intent.getBooleanExtra("push", false);

        if(!fromPush) {
            playlistAudioID = intent.getIntExtra("position", 0);
            textDuracao = intent.getStringExtra("duracao");
        }

        int id = intent.getIntExtra("id",0);
        Log.d("onPrepared", "Audio id:" + id + "Audio list id: " + playlistAudioID);

        mHolder = new ViewHolder();
        mregisterBroadCasts();
        getAudio(id);
        initValues();
        initAction();


        Intent broadcastIntent = new Intent(MediaPlayerService.HAS_PLAYING);
        broadcastIntent.putExtra("audioID", playlistAudioID);
        sendBroadcast(broadcastIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        munRegisterBroadCasts();

        if(handler != null){
            handler.removeCallbacks(runnable);
        }
    }

    void initValues (){
        mHolder.actual.setText("00:00");
        mHolder.max.setText(textDuracao);
    }

    void initAction (){
        mHolder.bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            boolean hasTouched = false;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //currentTime = progress;
                //atualizarCurrentTime();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                hasTouched = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //atualizarCurrentTime();
                if(hasTouched){
                    seekTo(seekBar.getProgress());
                    hasTouched = false;
                }
            }
        });

        mHolder.back15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_RETROC15, playlistAudioID).execute();
                backTo15();
            }
        });

        mHolder.previews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_ANTERIOR, playlistAudioID).execute();
                goBack();
            }
        });

        mHolder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });

        mHolder.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_PROXIMA, playlistAudioID).execute();
                goNext();
            }
        });

        mHolder.go15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_AVAN15, playlistAudioID).execute();
                goTo15();
            }
        });

        mHolder.shufle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_ALEATORIO, playlistAudioID).execute();
                mShufle = !mShufle;
                sendConfiguration(mShufle,mRepeat,mSuperRepeat);
                actualizarShufle();
            }
        });
        mHolder.audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_MUDO, playlistAudioID).execute();
                mAudio = !mAudio;
                sendConfiguration(mShufle,mRepeat,mSuperRepeat);
                actualizarAudio();
            }
        });

        mHolder.repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_REPETIR, playlistAudioID).execute();
                mSuperRepeat = !mSuperRepeat;
                sendConfiguration(mShufle, mRepeat, mSuperRepeat);
                actualizarRepeat();
            }
        });

        mHolder.lyric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hasLyrics){
                    mLyric = !mLyric;
                    actualizarLyric();
                }
            }
        });
    }

    void getAudio (int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_AUDIO, OP_GET_AUDIO,this).execute(params);
    }

    //region Player methods
    //Da play na musica
    void play (){
        if(!audioLoaded){
            synquing = true;
            actualizarSync();
            MainActivity.instance.playAudioFromPlaylist(playlistAudioID);
        }else{
            if(!isPlaying){
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_PLAY, playlistAudioID).execute();
                Intent broadcastIntent = new Intent(MediaPlayerService.ACTION_PLAY);
                sendBroadcast(broadcastIntent);
            }else{
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_PLAYER_PAUSE, playlistAudioID).execute();
                Intent broadcastIntent = new Intent(MediaPlayerService.ACTION_PAUSE);
                sendBroadcast(broadcastIntent);
            }
        }
    }

    //Envia notificação
    void sendConfiguration ( boolean shufle, boolean repeat, boolean superRepeat){
        Intent broadcastIntent = new Intent(MediaPlayerService.CHANGE_CONFIGURATION);
        broadcastIntent.putExtra("shufle",shufle);
        broadcastIntent.putExtra("repeat",repeat);
        broadcastIntent.putExtra("superRepeat",superRepeat);
        broadcastIntent.putExtra("audio",mAudio);
        sendBroadcast(broadcastIntent);
    }

    //Ir 15 segundos
    void goTo15 (){
        synquing = true;
        actualizarSync();
        int timerStanp = 15 * 1000;
        if(currentTime + timerStanp < duracao){
            int t = currentTime + timerStanp;
            seekTo(t);
        }
    }

    //Voltar 15 segundos
    void backTo15 (){
        synquing = true;
        actualizarSync();
        int timerStanp = 15 * 1000;
        if(currentTime - timerStanp > 0){
            int t = currentTime - timerStanp;
            seekTo(t);
        }
    }

    //Proxima musica
    void goNext (){
        if(!audioLoaded){
            synquing = true;
            MainActivity.instance.playAudioFromPlaylist(++playlistAudioID);
            return;
        }
        synquing = true;
        actualizarSync();
        Intent broadcastIntent = new Intent(MediaPlayerService.ACTION_NEXT);
        sendBroadcast(broadcastIntent);
    }

    //Musica anterior
    void goBack (){
        if(!audioLoaded){
            synquing = true;
            MainActivity.instance.playAudioFromPlaylist(--playlistAudioID);
            return;
        }
        synquing = true;
        actualizarSync();
        Intent broadcastIntent = new Intent(MediaPlayerService.ACTION_PREVIOUS);
        sendBroadcast(broadcastIntent);
    }

    //Ir para local
    void seekTo (int position){
        synquing = true;
        actualizarSync();
        Intent broadcastIntent = new Intent(MediaPlayerService.SEEK_TO);
        broadcastIntent.putExtra("position", position);
        sendBroadcast(broadcastIntent);
    }

    //endregion

    //region Layout changes
    void initAudio (PlaylistItemData data){

        if(data.getImg() != null){
            UTILS.getImageAt(activity, data.getImg(), mHolder.audioImage,new RequestOptions()
                    .transforms( new RoundedCorners(20)));

            UTILS.getImageAt(activity, data.getImg(), mHolder.background);
        }

        if(data.getTitulo() != null)
            mHolder.audioTittle.setText(data.getTitulo());

        if(fromPush){
            MainActivity.instance.addAudio(new Audio( data.getId(), UTILS.getHtmlContent(data.getAudio()), "São Paulo FC", data.getTitulo(), UTILS.formatDateFromDB(data.getDataHora())));
            play();
            fromPush = false;
        }

        mSuperRepeat = false;
        actualizarLyric();

        if(data.getLyrics() != null){
            hasLyrics = true;
            mHolder.lyrics.setText(data.getLyrics());
        }
    }

    void atualizarCurrentTime (int time){
        currentTime = time;
        mHolder.bar.setProgress(currentTime);
        mHolder.actual.setText(milisecondsToString(currentTime));
    }

    void atualizarCurrentTime (){
        mHolder.bar.setProgress(currentTime);
        mHolder.actual.setText(milisecondsToString(currentTime));
    }

    void actualizarMaxTime (){
        mHolder.bar.setMax(duracao);
        mHolder.max.setText(milisecondsToString(duracao));
    }

    void actualizarSync (){
        if(synquing){
            mHolder.playerStatus.setText("Carregando");
            mHolder.playerStatus.setCompoundDrawablesWithIntrinsicBounds(null , null, null, null);
            mHolder.playerStatus.getBackground().setTint(getResources().getColor(R.color.color_player_button));
            mHolder.playerStatus.setTextColor(getResources().getColor(R.color.colorBlackLetter));
        }
    }

    void actualizarPLayButton (){

        if(isPlaying){
            if(!synquing){
                mHolder.playerStatus.setText(getResources().getString(R.string.tocando));
                mHolder.playerStatus.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_play_minor), null, null, null);
                mHolder.playerStatus.getBackground().setTint(getResources().getColor(R.color.colorRedSPFCTransp));
                mHolder.playerStatus.setTextColor(getResources().getColor(R.color.colorPrimary));
                UTILS.changeDrawableColor(mHolder.playerStatus, this, R.color.colorPrimary);
            }
            mHolder.action.setImageResource(R.drawable.ic_pause_minor);
        }else{
            if(!synquing){
                mHolder.playerStatus.setText(getResources().getString(R.string.pausado));
                mHolder.playerStatus.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pause_minor), null, null, null);
                mHolder.playerStatus.getBackground().setTint(getResources().getColor(R.color.color_player_button));
                mHolder.playerStatus.setTextColor(getResources().getColor(R.color.colorBlackLetter));
                UTILS.changeDrawableColor(mHolder.playerStatus, this, R.color.colorBlackLetter);
            }
            mHolder.action.setImageResource(R.drawable.ic_play_minor);
        }
    }

    void actualizarShufle (){
        if(mShufle){
            mHolder.shufle.setColorFilter(getResources().getColor(R.color.colorRedCard));
        }else{
            mHolder.shufle.setColorFilter(getResources().getColor(R.color.colorBlackLetter));
        }
    }

    void actualizarAudio (){
        if(!mAudio){
            mHolder.audio.setColorFilter(getResources().getColor(R.color.colorRedCard));
            mHolder.audio.setImageDrawable(getResources().getDrawable(R.drawable.ic_mute_off));
        }else{
            mHolder.audio.setColorFilter(getResources().getColor(R.color.colorBlackLetter));
            mHolder.audio.setImageDrawable(getResources().getDrawable(R.drawable.ic_mute));
        }
    }

    void actualizarRepeat (){
        if(mSuperRepeat){
            mHolder.repeat.setColorFilter(getResources().getColor(R.color.colorRedCard));
        }else{
            mHolder.repeat.setColorFilter(getResources().getColor(R.color.colorBlackLetter));
        }
    }

    void actualizarLyric (){
        if(mLyric){
            mHolder.viewLyrics.setVisibility(View.VISIBLE);
            mHolder.lyric.setColorFilter(getResources().getColor(R.color.colorRedCard));
        }else{
            mHolder.viewLyrics.setVisibility(View.GONE);
            mHolder.lyric.setColorFilter(getResources().getColor(R.color.colorBlackLetter));
        }
    }

    void updateSeekBar (){
        currentTime += 1000;
        if(!synquing)
            atualizarCurrentTime();

        if(isPlaying){
            runnable = new Runnable() {
                @Override
                public void run() {
                    //Log.d("onPrepared", "Audio ID: " + playlistAudioID + " Duracao: " + duracao + " CurrentTime: " + currentTime);
                    updateSeekBar();
                }
            };

            handler.postDelayed(runnable, 1000);
        }
    }

    String milisecondsToString (int time){
        String finalString = "";
        int seconds = (int) (time / 1000) % 60 ;
        int minutes = (int) ((time / (1000*60)) % 60);
        int hours   = (int) ((time / (1000*60*60)) % 24);

        String mSeconds = String.valueOf(seconds);
        String mMinutes = String.valueOf(minutes);
        String mHours = String.valueOf(hours);

        if(hours > 0){
            finalString = String.format(Locale.getDefault(), "%02d:%02d:%02d",hours, minutes, seconds);
        }else{
            finalString = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        }

        return finalString;
    }

    //endregion

    //region BroadCasts (Receivers)
    //Quando o audio tiver sido preparado
    private BroadcastReceiver onPrepared = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            synquing = false;
            audioLoaded = true;
            isPlaying = intent.getBooleanExtra("playing", true);
            playlistAudioID = intent.getIntExtra("audioID", playlistAudioID);
            duracao = intent.getIntExtra("duracao", 0);
            currentTime = intent.getIntExtra("position", 0);

            int id = intent.getIntExtra("id", 0);
            getAudio(id);

            actualizarMaxTime();
            atualizarCurrentTime(currentTime);
            actualizarPLayButton();
            sendConfiguration(mShufle,mRepeat,mSuperRepeat);

            if(handler != null){
                handler.removeCallbacks(runnable);
            }else{
                handler = new Handler();
            }

            updateSeekBar();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onSeekTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            synquing = false;
            currentTime = intent.getIntExtra("position", 0);
            atualizarCurrentTime();
            actualizarPLayButton();
        }
    };

    //Pegar configuração
    private BroadcastReceiver getConfiguration = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mRepeat = intent.getBooleanExtra("repeat", false);
            mSuperRepeat = intent.getBooleanExtra("superRepeat", false);
            mShufle = intent.getBooleanExtra("shufle", false);
            mAudio = intent.getBooleanExtra("audio", true);

            actualizarShufle();
            actualizarRepeat();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onStateChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isPlaying = intent.getBooleanExtra("state", false);
            actualizarPLayButton();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onStop = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isPlaying = false;
            audioLoaded = false;
            actualizarPLayButton();
        }
    };

    //Quando o audio tiver sido preparado e tocando
    private BroadcastReceiver getPlayer = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra("audioID", playlistAudioID);
            Log.d("onPrepared", "Audio id:" + id + "Audio list id: " + playlistAudioID);

            if(id == playlistAudioID){
                Log.d("onPrepared", "Mesmo audio");
                audioLoaded = true;
                playlistAudioID = id;
                isPlaying = intent.getBooleanExtra("playing", true);
                duracao = intent.getIntExtra("duracao", 0);
                currentTime = intent.getIntExtra("position", 0);

                int mID = intent.getIntExtra("id", 0);
                getAudio(mID);

                actualizarMaxTime();
                atualizarCurrentTime();
            }else{
                Log.d("onPrepared", "Audio diferente");
                audioLoaded = false;
                isPlaying = false;
                synquing = true;

                Intent broadcastIntent = new Intent(MediaPlayerService.ACTION_START);
                broadcastIntent.putExtra("audioID", playlistAudioID);
                sendBroadcast(broadcastIntent);
                actualizarPLayButton();
                actualizarSync();
            }
        }
    };



    void mregisterBroadCasts (){
        //OnPrepared
        IntentFilter iOnPrepared = new IntentFilter(MediaPlayerService.ON_PREPARED);
        registerReceiver(onPrepared, iOnPrepared);

        //OnSeekTo
        IntentFilter iOnSeek = new IntentFilter(MediaPlayerService.ON_SEEK_TO);
        registerReceiver(onSeekTo, iOnSeek);

        //OnStateChange
        IntentFilter iOnStateChange = new IntentFilter(MediaPlayerService.ON_STATE_CHANGE);
        registerReceiver(onStateChange, iOnStateChange);

        //OnStop
        IntentFilter iOnStop = new IntentFilter(MediaPlayerService.ACTION_STOP);
        registerReceiver(onStop, iOnStop);

        //Pegar configuração
        IntentFilter iGetConfiguration = new IntentFilter(MediaPlayerService.CHANGE_CONFIGURATION);
        registerReceiver(getConfiguration, iGetConfiguration);

        //Pegar player se estiver tocando
        IntentFilter iGetPlayer = new IntentFilter(MediaPlayerService.GET_PLAYER);
        registerReceiver(getPlayer, iGetPlayer);
    }

    void munRegisterBroadCasts(){
        //OnPrepared
        try {
            unregisterReceiver(onPrepared);

            //OnSeekTo
            unregisterReceiver(onSeekTo);

            //OnStateChange
            unregisterReceiver(onStateChange);

            //OnStop
            unregisterReceiver(onStop);

            //Pegar configuração atual
            unregisterReceiver(getConfiguration);

            //Pegar player se stiver tocando
            unregisterReceiver(getPlayer);
        } catch(IllegalArgumentException e) {

            e.printStackTrace();
        }
    }
    //endregion

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_AUDIO:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            PlaylistItemData data = gson.fromJson(response.getString("Object"), PlaylistItemData.class);

                            if(data != null){
                                initAudio(data);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        ImageView background;
        ImageView audioImage;
        TextView audioTittle;
        TextView lyrics;

        SeekBar bar;
        TextView actual;
        TextView max;
        TextView playerStatus;

        ImageView messages;
        ImageView repeat;
        ImageView shufle;
        ImageView audio;
        ImageView lyric;

        View back15;
        ImageView previews;
        ImageView action;
        ImageView next;
        View go15;
        View viewLyrics;

        public ViewHolder() {
            background = findViewById(R.id.img_background);
            audioImage = findViewById(R.id.img_audio);
            audioTittle = findViewById(R.id.text_tittle);

            bar = findViewById(R.id.seekbar);
            actual = findViewById(R.id.txt_actual_temp);
            max = findViewById(R.id.txt_total_temp);
            playerStatus = findViewById(R.id.text_player_status);

            //messages = findViewById(R.id.img_previews);
            repeat = findViewById(R.id.img_repeat);
            shufle = findViewById(R.id.img_shufle);
            audio = findViewById(R.id.img_mute);

            back15 = findViewById(R.id.back15);
            previews = findViewById(R.id.img_previews);
            action = findViewById(R.id.img_action);
            next = findViewById(R.id.img_next);
            go15 = findViewById(R.id.go15);
            lyrics = findViewById(R.id.txt_audio_lyrics);
            lyric = findViewById(R.id.img_lyrics);
            viewLyrics = findViewById(R.id.view_lyrics);
        }
    }
}
