package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterPalpitesList;
import data.PalpiteData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

public class PalpitesListActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    AdapterPalpitesList adapter;
    private static final int OP_GET_PALPITE              = 0;
    int pager = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palpites_list);
        mHolder = new ViewHolder();
        getPalpites(pager);
    }

    void getPalpites(int page){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PALPITES_LIST, OP_GET_PALPITE, this).execute(params);
    }

    void initPalpites (List<PalpiteData> palpites){
        if(adapter == null){
            adapter = new AdapterPalpitesList(activity, palpites, new AdapterPalpitesList.Listener() {
                @Override
                public void OnClick(PalpiteData data) {
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_MEUSPALPITES_SELECIONAR_PALPITE, data.getId()).execute();
                    Gson gson = new Gson();
                    String obj = gson.toJson(data);
                    Intent intent = new Intent(activity, PalpiteDetailActivity.class);
                    intent.putExtra("data", obj);
                    startActivity(intent);
                }

                @Override
                public void OnLast() {
                    getPalpites(pager);
                }
            });

            mHolder.recyclerView.setAdapter(adapter);
            mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        }else{
            adapter.ChangeList(palpites);
        }
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PALPITE:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){

                        if(response.has("Object")){
                            try {
                                Gson gson = new Gson();
                                JSONObject obj = response.getJSONObject("Object");
                                PalpiteData[] data = gson.fromJson(obj.getString("itens"), PalpiteData[].class);

                                if(data != null){
                                    List<PalpiteData> palpites = new ArrayList<>(Arrays.asList(data));
                                    initPalpites(palpites);
                                    pager ++;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        RecyclerView recyclerView;
        public ViewHolder (){
            recyclerView = findViewById(R.id.recycker_palpites);
        }
    }
}
