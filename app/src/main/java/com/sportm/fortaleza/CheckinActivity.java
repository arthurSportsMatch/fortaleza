package com.sportm.fortaleza;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import data.CheckinAcompanhanteData;
import data.CheckinCadeiraData;
import data.CheckinData;
import data.CheckinPartidaData;
import data.CheckinSetoresData;
import fragments.CheckinOkScreenFragment;
import fragments.CheckinSetorDivFragment;
import fragments.CheckingCadastrarCartaoFragment;
import fragments.CheckingCancelarFragment;
import fragments.CheckingFinalizarFragment;
import fragments.CheckingListarJogosFragment;
import fragments.CheckingListarUsuariosFragment;
import fragments.CheckingSelecionarAssentosFragment;
import fragments.CheckingSelecionarSetorFragment;
import informations.UserInformation;
import utils.UILoadingController;

import static utils.UTILS.convertMonetaryValue;
/*
    - Controlar os fragmentos existentes
    - Controlar o estado atual do checkin
    - Fazer a alteração de fragmentos passando o controlador
 */

public class CheckinActivity extends MainActivity {

    UILoadingController uiLoadingController;
    CheckInController controller;
    ViewHolder mHolder;
    Listener listener;


    int maxSteps = 5;
    int steps = 1;

    enum State{
        Jogos,
        Setor,
        SetorDiv,
        Acompanhantes,
        Cadeiras,
        Finalizar,
        Cartao,
        Cancelar,
        Ok
    }

    private final int  OP_ID_VALIDAR_USER = 0;
    private final int  OP_ID_CONSULTAR_SOCIO = 1;
    private final int  OP_ID_CONSULTAR_PROX_JOGOS = 2;
    private final int  OP_ID_CONSULTAR_BACK_JOGOS = 3;
    private final int  OP_ID_CONSULTAR_SETORES = 4;
    private final int  OP_ID_CONSULTAR_DIV_SETORES = 5;
    private final int  OP_ID_ASSENTOS_DIV = 6;
    private final int  OP_ID_INGRESSOS = 7;
    private final int  OP_ID_CHECKINGS = 8;
    private final int  OP_ID_CONSULTAR_CHECKINGS = 9;
    private final int  OP_ID_CHECKOUT = 10;
    private final int  OP_ID_ACOMPANHANTES = 13;

    final String FRAGMENT_LISTA_JOGOS = "lista_de_jogos";
    final String FRAGMENT_SELECIONAR_SETOR = "selecionar_setor";
    final String FRAGMENT_SELECIONAR_ACOMPANHANTE = "selecionar_acompanhante";
    final String FRAGMENT_SELECIONAR_SETOR_DIV = "selecionar_setor_div";
    final String FRAGMENT_SELECIONAR_ASSENTOS = "selecionar_user";
    final String FRAGMENT_FINALIZAR = "finalizar";
    final String FRAGMENT_CADASTRAR_CARTAO = "cadastrar_cartao";
    final String FRAGMENT_CANCELAR = "cancelar";
    final String FRAGMENT_OK = "okScreen";

    State checkInState = State.Jogos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);
        mHolder = new ViewHolder();
        UserInformation.getUserData().setSocioTorcedor(getIntent().getExtras().getInt("socio"));
        UserInformation.getUserData().setSocioBeneficio(getIntent().getExtras().getInt("beneficio"));
        UserInformation.getUserData().setNickname(getIntent().getExtras().getString("nome"));
        UserInformation.getUserData().setCpf(getIntent().getExtras().getString("cpf"));
        controller = createController();

        View loadingView = findViewById(R.id.include_loading_screen);
        createLoadingController(loadingView, new UILoadingController.Listener() {
            @Override
            public void onTryAgain() {
                listener.OnLoad();
            }
        });

        initAction();

        addFragmentInFrame(createFragmentListaDeJogos(), FRAGMENT_LISTA_JOGOS);
        uiLoadingController.finishLoader(true);
    }

    @Override
    public void onBackPressed() {
        mHolder.list.removeAllViews();

        if(checkInState != State.Ok){
            super.onBackPressed();
        }

        switch (checkInState){
            case Jogos:{
                checkStep();
            }
            break;
            case Setor: {

            }
            case Cancelar:{
                checkInState = State.Jogos;
                checkStep();
            }
            break;
            case SetorDiv: {
                checkInState = State.Setor;
                checkStep();
            }
            break;
            case Acompanhantes: {
                if(controller.haveSetorCadeiras()){
                    checkInState = State.SetorDiv;
                }else{
                    checkInState = State.Setor;
                }
                checkStep();
            }
            break;
            case Cadeiras: {
                checkInState = State.Acompanhantes;
                checkStep();
            }
            break;
            case Finalizar: {
                if(controller.haveSetorCadeiras()){
                    checkInState = State.Cadeiras;
                }else{
                    checkInState = State.Acompanhantes;
                }
                checkStep();
            }
            break;
        }
    }

    void initAction (){
        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHolder.voltar.setClickable(false);
                mHolder.confirm.setClickable(false);
                onBackPressed();
            }
        });

        mHolder.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHolder.confirm.setClickable(false);
                mHolder.voltar.setClickable(false);

                switch (checkInState){
                    //Esta na tela de setores
                    case Setor: {
                        mHolder.list.removeAllViews();
                        if(controller.haveSetor()){
                            if(controller.haveSetorCadeiras()){
                                addFragmentInFrame(createFragmentSelecionarSetorDiv(), FRAGMENT_SELECIONAR_SETOR_DIV);
                            }else{
                                addFragmentInFrame(createFragmentUserController(), FRAGMENT_SELECIONAR_ACOMPANHANTE);
                            }
                        }
                    }
                    break;

                    //Esta na tela de divisão de setores
                    case SetorDiv: {
                        mHolder.list.removeAllViews();
                        addFragmentInFrame(createFragmentUserController(), FRAGMENT_SELECIONAR_ACOMPANHANTE);
                    }
                    break;

                    //Esta na tela de Acompanhantes
                    case Acompanhantes: {
                        mHolder.list.removeAllViews();
                        //Se tiver que selecionar cadeiras
                        if (controller.haveSetorCadeiras()) {
                            addFragmentInFrame(createFragmentSelecionarAssentosController(), FRAGMENT_SELECIONAR_ASSENTOS);
                        }else {
                            addFragmentInFrame(createFragmentFinalizarCheckIn(), FRAGMENT_FINALIZAR);
                        }
                    }
                    break;

                    case Cadeiras:{
                        mHolder.list.removeAllViews();
                        if(controller.hasChairsSelectecteds()){
                            addFragmentInFrame(createFragmentFinalizarCheckIn(), FRAGMENT_FINALIZAR);
                        }else {
                            Toast.makeText(activity, "Selecione todas as cadeiras", Toast.LENGTH_SHORT).show();
                            unlockbuttons();
                        }
                    }
                    break;

                    case Finalizar:{
                        if ( controller.checkinData.isHasCard() ) {
                            controller.fazerChecking();
                            uiLoadingController.onLoad();
                        }else {
                            mHolder.list.removeAllViews();
                            unlockbuttons();
                            addFragmentInFrame(createFragmentCadastrarCartao(), FRAGMENT_CADASTRAR_CARTAO);
                        }
                    }
                    break;

                    case Cartao:{
                        CheckingCadastrarCartaoFragment frag = (CheckingCadastrarCartaoFragment) getFragmentByTag(FRAGMENT_CADASTRAR_CARTAO);

                        if ( frag.validarFrom() ) {
                            controller.fazerChecking();
                            uiLoadingController.onLoad();
                        }else {
                            unlockbuttons();
                        }
                    }
                    break;

                    case Cancelar:{
                        addPopUpMessage();

                    }
                    break;

                    case Ok :{
                        mHolder.list.removeAllViews();
                        uiLoadingController.finishLoader(true);
                        finish();
                    }
                }
            }
        });
    }

    void checkStep (){
        switch (checkInState){
            case Setor:{
                steps = 1;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Continuar");
                mHolder.textProressContext.setText("Selecione o Setor");
                controller.removerSetores();
                controller.checkinData.setTickets(null);
                controller.checkinData.setHasTickets(false);

            }
            break;

            case SetorDiv:{
                steps = 2;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Continuar");
                mHolder.textProressContext.setText("Selecione o Setor");

            }
            break;

            case Acompanhantes: {
                steps = 3;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Continuar");
                mHolder.textProressContext.setText("Selecione o Número de Pessoas");
                controller.removerCadeiras();
            }
            break;

            case Cadeiras:{
                steps = 4;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Continuar");
                mHolder.textProressContext.setText("Selecione o(s) Assento(s)");

            }
            break;

            case Finalizar:{
                steps = 5;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Finalizar");
                mHolder.textProressContext.setText("Confirme os Dados do Check-in");
            }
            break;

            case Cartao:{
                steps = 5;
                mHolder.viewProgressContext.setVisibility(View.VISIBLE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Finalizar");
                mHolder.textProressContext.setText("Para finalizar, preencha os campos abaixo");
            }
            break;

            case Jogos:{
                steps = 0;
                mHolder.viewProgressContext.setVisibility(View.GONE);
                mHolder.confirm.setVisibility(View.GONE);
            }
            break;
            case Cancelar:{
                steps = 0;
                mHolder.viewProgressContext.setVisibility(View.GONE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Cancelar Check-in");
            }
            break;

            case Ok:{
                steps = 0;
                mHolder.viewProgressContext.setVisibility(View.GONE);
                mHolder.confirm.setVisibility(View.VISIBLE);
                mHolder.confirm.setText("Voltar");
            }
            break;
        }

        mHolder.progressContext.setProgress(steps);
    }

    void addPopUpMessage (){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // set the custom layout
        View customLayout = getLayoutInflater().inflate(R.layout.dialog_checkin_message, null);
        Button button = customLayout.findViewById(R.id.btn_trocar_senha);
        TextView msg = customLayout.findViewById(R.id.txt_senha);

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uiLoadingController.onLoad();
                controller.fazerCheckout();
                dialog.dismiss();
            }
        });

        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    void createLoadingController (View loadingView, UILoadingController.Listener listener){
        uiLoadingController = new UILoadingController(loadingView, listener);
    }

    public void setListener (Listener listener){
        this.listener = listener;
    }

    //Cria o controlador
    CheckInController createController (){
        return new CheckInController(activity, new CheckInController.Listener() {
            @Override
            public void onSuccess(int opId, @Nullable List<?> response) {
                uiLoadingController.finishLoader(true);

                switch (opId) {
                    case OP_ID_CONSULTAR_PROX_JOGOS: {
                        //Significa que teve sucesso no carregamento
                        if(response != null){
                            CheckingListarJogosFragment frag = (CheckingListarJogosFragment) getFragmentByTag(FRAGMENT_LISTA_JOGOS);
                            frag.atualizarData((List<CheckinPartidaData>) response);
                        }else{
                            CheckingListarJogosFragment frag = (CheckingListarJogosFragment) getFragmentByTag(FRAGMENT_LISTA_JOGOS);
                            frag.getNoData();
                        }

                        controller.consultarSocio(UserInformation.getUserData().getCpf());
                    }
                    break;

                    case OP_ID_CONSULTAR_CHECKINGS:{
                        if(response == null){
                            addFragmentInFrame(createFragmentSelecionarSetor(), FRAGMENT_SELECIONAR_SETOR);
                        }
                    }
                    break;

                    case OP_ID_CONSULTAR_SETORES :{
                        CheckingSelecionarSetorFragment frag = (CheckingSelecionarSetorFragment) getFragmentByTag(FRAGMENT_SELECIONAR_SETOR);
                        frag.atualizarData((List<CheckinSetoresData>) response);
                    }
                    break;

                    case OP_ID_ACOMPANHANTES :{
                        CheckingListarUsuariosFragment frag = (CheckingListarUsuariosFragment) getFragmentByTag(FRAGMENT_SELECIONAR_ACOMPANHANTE);
                        if ( !controller.checkinData.isHasTickets() ) {
                            frag.atualizarData((List<CheckinSetoresData>) response);
                            controller.checkinData.setHasTickets(true);
                        }else {
                            List<CheckinSetoresData> list = new ArrayList<>();
                            for ( int i = 0; i < controller.checkinData.getNomes().size(); i++ ) {
                                CheckinSetoresData data = new CheckinSetoresData();
                                data.setNome(controller.checkinData.getNomes().get(i));
                                data.setValor(Float.parseFloat(controller.checkinData.getValores().get(i)));
                                list.add(data);
                            }
                            frag.atualizarData(list);
                        }
                    }
                    break;

                    case OP_ID_CONSULTAR_DIV_SETORES:{
                        CheckinSetorDivFragment frag = (CheckinSetorDivFragment) getFragmentByTag(FRAGMENT_SELECIONAR_SETOR_DIV);
                        frag.atualizarData((List<CheckinSetoresData>) response);
                    }
                    break;

                    case OP_ID_ASSENTOS_DIV:{
                        CheckingSelecionarAssentosFragment frag = (CheckingSelecionarAssentosFragment) getFragmentByTag(FRAGMENT_SELECIONAR_ASSENTOS);
                        frag.atualizarData((List<CheckinCadeiraData>) response);
                    }
                    break;

                    case OP_ID_CHECKINGS: {
                        if(response == null){
                            addFragmentInFrame(createFragmentOKScreen("Check-in Realizado com Sucesso.", "Você pode checar o status do seu check-in na área de sócio torcedor no menu lateral."), FRAGMENT_OK);
                        }
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_CHECKOUT: {
                        if(response == null){
                            addFragmentInFrame(createFragmentOKScreen("Check-in Cancelado com Sucesso.", "Você pode refazer o check-in se desejar, entrando novamente na área de Check-In."), FRAGMENT_OK);
                        }
                        unlockbuttons();
                    }
                    break;
                }
            }

            @Override
            public void onSuccess(int opId, @Nullable Object response) {
                uiLoadingController.finishLoader(true);
                switch (opId){
                    case OP_ID_CONSULTAR_CHECKINGS :{
                        CheckinData chekings  = (CheckinData)response;
                        addFragmentInFrame(createFragmentCancelarChecking(chekings, chekings.getPartida()), FRAGMENT_CANCELAR);
                        unlockbuttons();
                    }
                    break;
                }
            }

            @Override
            public void onSuccess(int opId, @Nullable JSONObject response) {
                uiLoadingController.finishLoader(true);
                //Significa que teve sucesso no carregamento
                switch (opId){
                    case OP_ID_CONSULTAR_PROX_JOGOS :{
                        CheckingListarJogosFragment frag = (CheckingListarJogosFragment) getFragmentByTag(FRAGMENT_LISTA_JOGOS);
                        frag.atualizarData(response);
                        unlockbuttons();
                    }

                    case OP_ID_CONSULTAR_SETORES :{
//                        CheckingSelecionarSetorFragment frag = (CheckingSelecionarSetorFragment) getFragmentByTag(FRAGMENT_SELECIONAR_SETOR);
//                        frag.atualizarData(response);
                        unlockbuttons();
                    }

                    case OP_ID_CONSULTAR_DIV_SETORES:{
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_INGRESSOS:{
                        try {
                            if ( response.getInt("status") == 200 ) {
                                if ( (response.has("dados") && (response.getJSONArray("dados").length() > 0)) ) {
                                    controller.checkinData.setIngressos(response.getJSONArray("dados"));
                                    controller.listarAcompanhantes();
                                }else {
                                    unlockbuttons();
                                    onBackPressed();
                                }
                            }else {
                                unlockbuttons();
                                onBackPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            unlockbuttons();
                            onBackPressed();
                        }
                    }
                    break;
                    case OP_ID_CONSULTAR_SOCIO: {
                        if ( response != null ) {
                            try {
                                if ( response.getInt("status") == 200 ) {

                                    if ( response.has("dados") ) {
                                        JSONObject data = response.getJSONObject("dados");

                                        controller.checkinData.setSocioID(data.getInt("id")+"");
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                }
            }

            @Override
            public void onError(int opId, JSONObject response) {
                //Significa que teve erro no carregamento
                switch (opId){
                    case OP_ID_CONSULTAR_PROX_JOGOS :{
                        CheckingListarJogosFragment frag = (CheckingListarJogosFragment) getFragmentByTag(FRAGMENT_LISTA_JOGOS);
                        frag.errorToGetData();
                        unlockbuttons();
                    }
                    break;
                    case OP_ID_CONSULTAR_SETORES:{
                        CheckingSelecionarSetorFragment frag = (CheckingSelecionarSetorFragment) getFragmentByTag(FRAGMENT_SELECIONAR_SETOR);
                        frag.errorToGetData();
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_CONSULTAR_DIV_SETORES:{
                        CheckinSetorDivFragment frag = (CheckinSetorDivFragment) getFragmentByTag(FRAGMENT_SELECIONAR_SETOR_DIV);
                        frag.errorToGetData();
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_ACOMPANHANTES:{
                        CheckinSetorDivFragment frag = (CheckinSetorDivFragment) getFragmentByTag(FRAGMENT_SELECIONAR_ACOMPANHANTE);
                        frag.errorToGetData();
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_ASSENTOS_DIV:{
                        CheckingSelecionarAssentosFragment frag = (CheckingSelecionarAssentosFragment) getFragmentByTag(FRAGMENT_SELECIONAR_ASSENTOS);
                        frag.errorToGetData();
                        unlockbuttons();
                    }
                    break;

                    case OP_ID_CONSULTAR_CHECKINGS:{
                        uiLoadingController.finishLoader(false);
                    }
                    break;
                    case OP_ID_CHECKINGS:{
                        uiLoadingController.finishLoader(true);
                        Toast.makeText(activity, "Falha ao realizar o check-in", Toast.LENGTH_SHORT).show();
                        unlockbuttons();
                    }
                    break;
                    case OP_ID_CHECKOUT:{
                        uiLoadingController.finishLoader(true);
                        Toast.makeText(activity, "Falha ao cancelar o check-in", Toast.LENGTH_SHORT).show();
                        unlockbuttons();
                    }
                    break;
                }
            }

            @Override
            public void onExpired() {
                uiLoadingController.finishLoader(true);
                mHolder.voltar.setClickable(true);
                Fragment fragment = getActiveFragment();

                if (fragment == null) {
                    return;
                }

                String tag = "";

                try {
                    tag = fragment.getArguments().getString("tag");
                }catch (Exception e) {
                    e.printStackTrace();
                }

                Toast.makeText(activity, "Tempo de conexão expirado", Toast.LENGTH_SHORT).show();

                switch (tag) {
                    case FRAGMENT_LISTA_JOGOS: {
                        CheckingListarJogosFragment frag = (CheckingListarJogosFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    case FRAGMENT_SELECIONAR_SETOR: {
                        CheckingSelecionarSetorFragment frag = (CheckingSelecionarSetorFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    case FRAGMENT_SELECIONAR_SETOR_DIV: {
                        CheckinSetorDivFragment frag = (CheckinSetorDivFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    case FRAGMENT_SELECIONAR_ACOMPANHANTE: {
                        CheckingListarUsuariosFragment frag = (CheckingListarUsuariosFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    case FRAGMENT_SELECIONAR_ASSENTOS: {
                        CheckingSelecionarAssentosFragment frag = (CheckingSelecionarAssentosFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    case FRAGMENT_FINALIZAR: {
                        CheckingFinalizarFragment frag = (CheckingFinalizarFragment) fragment;
                        frag.errorToGetData();
                    }
                    break;
                    default: {
                        unlockbuttons();
                    }
                }

            }
        });
    }
    //Cria o fragmento de lista de jogos
    Fragment createFragmentListaDeJogos (){
        checkInState = State.Jogos;
        checkStep();
        Bundle bundle = new Bundle();
        bundle.putString("tag", FRAGMENT_LISTA_JOGOS);

        Fragment fragment = new CheckingListarJogosFragment(new CheckingListarJogosFragment.Listener() {
            @Override
            public void OnClickItem(CheckinPartidaData partida) {
                controller.addJogoID(partida);
                uiLoadingController.onLoad();
            }

            @Override
            public void OnLoad() {
                controller.proximosJogos();
                unlockbuttons();
            }
        });

        fragment.setArguments(bundle);

        return fragment;
    }
    //Cria o fragmento de selecionar setor
    Fragment createFragmentSelecionarSetor (){
        checkInState = State.Setor;
        checkStep();
        Bundle bundle = new Bundle();
        bundle.putString("tag", FRAGMENT_SELECIONAR_SETOR);

        Fragment fragment = new CheckingSelecionarSetorFragment(new CheckingSelecionarSetorFragment.Listener() {

            @Override
            public void OnLoad() {
                controller.getSetores();
            }

            @Override
            public void OnFinished(String setorName, int setorID) {
                controller.addSetor(setorName, String.valueOf(setorID), false);
            }

            @Override
            public void OnFinished(String setorName, int setorID, int assentoID) {
                controller.addSetor(setorName, String.valueOf(setorID), true);
            }

            @Override
            public void OnUnlock() {
                unlockbuttons();
            }
        });

        fragment.setArguments(bundle);

        return fragment;
    }
    //Cria o fragmento de selecionar div
    Fragment createFragmentSelecionarSetorDiv (){
        checkInState = State.SetorDiv;
        checkStep();
        Bundle bundle = new Bundle();
        bundle.putString("tag", FRAGMENT_SELECIONAR_SETOR_DIV);

        Fragment fragment =  new CheckinSetorDivFragment(new CheckinSetorDivFragment.Listener() {
            @Override
            public void OnLoad() {
                controller.listarDivSetor();
            }

            @Override
            public void OnFinished(String setorID) {
                controller.addAssentoID(setorID);
            }

            @Override
            public void OnFinished(String setorID, String setorName) {
                controller.addAssentoID(setorID, setorName);
            }

            @Override
            public void OnUnlock() {
                unlockbuttons();
            }
        });

        fragment.setArguments(bundle);

        return fragment;
    }

    //Cria o fragmento de acompanhantes
    Fragment createFragmentUserController (){
        checkInState = State.Acompanhantes;
        checkStep();
        Bundle bundle = new Bundle();
        bundle.putString("tag", FRAGMENT_SELECIONAR_ACOMPANHANTE);

        Fragment fragment = new CheckingListarUsuariosFragment(controller.checkinData, new CheckingListarUsuariosFragment.Listener() {
            @Override
            public void onLoad() {
                controller.listarIngressos();
            }

            @Override
            public void addTicket(JSONObject ticket) {
                controller.checkinData.addTicket(ticket);
            }

            @Override
            public void removeTicket(JSONObject ticket) {
                controller.checkinData.removeTicket(ticket);
            }

            @Override
            public void deleteAllTickets() {
                controller.checkinData.setTickets(null);
            }

            @Override
            public void OnUnlock() {
                unlockbuttons();
            }
        });

        fragment.setArguments(bundle);

        return fragment;
    }

    //Cria o fragmento de selecionar o assento
    Fragment createFragmentSelecionarAssentosController (){
        checkInState = State.Cadeiras;
        checkStep();
        Bundle bundle = new Bundle();
        bundle.putString("tag", FRAGMENT_SELECIONAR_ASSENTOS);

        Fragment fragment = new CheckingSelecionarAssentosFragment(controller.checkinData, new CheckingSelecionarAssentosFragment.Listener() {
            @Override
            public void OnLoad() {
                controller.listarAssentos();
            }

            @Override
            public void onChose(String cadeira, String cadeiraNome, int index) {
                controller.addCadeiraID(cadeira, cadeiraNome, index);
            }

            @Override
            public void onChosseAll() {

            }

            @Override
            public void OnUnlock() {
                unlockbuttons();
            }

            @Override
            public void romeverCadeira(String cadeira, String cadeiraNome) {
                controller.checkinData.removerCadeira(cadeira, cadeiraNome);
            }

            @Override
            public void romeverCadeiras() {
                controller.checkinData.removerCadeiras();
            }
        });

        fragment.setArguments(bundle);

        return fragment;
    }

    //Cria o Finalizar o checkin
    Fragment createFragmentFinalizarCheckIn (){
        checkInState = State.Finalizar;
        checkStep();
        return  new CheckingFinalizarFragment(controller.checkinData, new CheckingFinalizarFragment.Listener() {
            @Override
            public void OnUnlock() {
                unlockbuttons();

                float total = 0;

                List<String> listN = controller.checkinData.getNomes();
                List<String> listV = controller.checkinData.getValores();

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMarginStart(60);
                lp.setMarginEnd(60);

                for ( int i = 1; i < listN.size(); i++ ) {
                    View view = LayoutInflater.from(activity).inflate(R.layout.include_checkin_values, mHolder.list, false);
                    TextView txt1 = view.findViewById(R.id.acompanhante);
                    TextView txt2 = view.findViewById(R.id.valor);
                    txt1.setText("Acompanhante " +i);
                    txt2.setText(convertMonetaryValue(Float.parseFloat(listV.get(i)), "pt", "BR"));

                    view.setLayoutParams(lp);

                    total = total+(Float.parseFloat(listV.get(i)));

                    mHolder.list.addView(view);
                }

                LinearLayout linearLayout = new LinearLayout(activity);

                LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
                lp2.setMargins(40,15,40,15);
                linearLayout.setLayoutParams(lp2);
                linearLayout.setBackgroundColor(Color.BLACK);

                mHolder.list.addView(linearLayout);

                View view = LayoutInflater.from(activity).inflate(R.layout.include_checkin_values, mHolder.list, false);
                TextView txt1 = view.findViewById(R.id.acompanhante);
                TextView txt2 = view.findViewById(R.id.valor);
                txt1.setTextSize(18);
                txt2.setTextSize(18);
                txt1.setText("TOTAL");
                txt2.setText(convertMonetaryValue(total, "pt", "BR"));

                view.setLayoutParams(lp);

                mHolder.list.addView(view);

            }
        });
    }
    Fragment createFragmentCadastrarCartao() {
        checkInState = State.Cartao;
        checkStep();

        return new CheckingCadastrarCartaoFragment(controller.checkinData, new CheckingCadastrarCartaoFragment.Listener(){
            @Override
            public void OnUnlock() {

            }
        });
    }
    //Cria fragmento de cancelar checking
    Fragment createFragmentCancelarChecking (CheckinData data, CheckinPartidaData partida){
        checkInState = State.Cancelar;
        checkStep();
        return new CheckingCancelarFragment(data, partida);
    }
    //Cria o OKScrenn do check- in/out
    Fragment createFragmentOKScreen (String tittle, String message){
        checkInState = State.Ok;
        checkStep();
        return new CheckinOkScreenFragment(tittle, message);
    }

    //region AddFragment Controller
    //Adiciona um fragmento ao container
    void addFragmentInFrame (Fragment fragment, String tag){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment oldFragment = getFragmentByTag(tag);

        if(getSupportFragmentManager().getFragments().size() > 0){
            if (!checkIfFragmentExists(tag)) {
                transaction.replace(R.id.frame_check_in, fragment, tag);
                transaction.addToBackStack(null);
            } else {
                transaction.replace(R.id.frame_check_in, oldFragment, tag);
            }
        }else{
            transaction.add(R.id.frame_check_in, fragment, tag);
        }
        transaction.commit();
    }

    //Verficica se esse fragmento existe na lista de fragmentos
    boolean checkIfFragmentExists (String tag){
        return getSupportFragmentManager().findFragmentByTag(tag) != null;
    }

    Fragment getFragmentByTag (String tag){
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    Fragment getActiveFragment() {
        for (Fragment fragment: getSupportFragmentManager().getFragments()) {
            if ( fragment.isVisible() )
                return fragment;
        }
        return null;
    }
    //endregion

    public interface Listener {
        void OnLoad ();
    }

    void unlockbuttons() {
        mHolder.confirm.setClickable(true);
        mHolder.voltar.setClickable(true);
    }

    class ViewHolder {
        View viewProgressContext;
        TextView textProressContext;
        ProgressBar progressContext;

        FrameLayout frame;
        Button confirm;
        TextView voltar;

        LinearLayout list;

        public ViewHolder (){
            viewProgressContext = findViewById(R.id.view_progress_context);
            textProressContext = findViewById(R.id.txt_progress_context);
            progressContext = findViewById(R.id.progress_context);
            frame = findViewById(R.id.frame_check_in);
            list = findViewById(R.id.view_list);

            confirm = findViewById(R.id.btn_confirm);
            voltar = findViewById(R.id.txt_voltar);
        }
    }
}
