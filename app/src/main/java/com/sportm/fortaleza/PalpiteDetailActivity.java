package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterPalpiteDetail;
import data.PalpiteData;
import data.PalpiteDetailData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.UTILS;

public class PalpiteDetailActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    AdapterPalpiteDetail adapter;
    private static final int OP_GET_PALPITE  = 0;

    int acertos;
    int erros;
    int total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palpite_detail);

        mHolder = new ViewHolder();

        Intent intent = getIntent();
        if(intent.hasExtra("data")){
            Gson gson = new Gson();
            PalpiteData data = gson.fromJson(intent.getStringExtra("data"), PalpiteData.class);
            getPalpites(data.getId());
            ajustHeader(data);
        }else{
            finish();
            Toast.makeText(activity, "Erro ao carregar", Toast.LENGTH_SHORT).show();
        }

        initAction();
    }

    void initAction (){
        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void getPalpites(int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PALPITES_DETAILS, OP_GET_PALPITE, this).execute(params);
    }

    void ajustHeader (PalpiteData palpite){
        //Penalte
        if(palpite.getPenC1() != null){
            mHolder.versus.setVisibility(View.GONE);
            mHolder.empate.setVisibility(View.VISIBLE);
            String value = palpite.getPenC1();

            if(palpite.getPenC2() != null){
                value = value + "x" + palpite.getPenC2();
            }

            mHolder.empate.setText("(" + value + ")");
        }

        //Clube 1
        if(palpite.getTime1img() != null){
            UTILS.getImageAt(activity, palpite.getTime1img(), mHolder.imgTime1);
        }

        if(palpite.getGolsClube1() != null){
            mHolder.gols1.setText(palpite.getGolsClube1());
        }

        //Clube 2
        if(palpite.getTime2img() != null){
            UTILS.getImageAt(activity, palpite.getTime2img(), mHolder.imgTime2);
        }

        if(palpite.getGolsClube2() != null){
            mHolder.gols2.setText(palpite.getGolsClube2());
        }

        if(palpite.getMoedas() != null){
            mHolder.moedas.setText("+" + palpite.getMoedas());
        }

        if(palpite.getAcertos() != null){
            acertos = Integer.parseInt(palpite.getAcertos());
        }
    }

    void initPalpites(List<PalpiteDetailData> palpites){
        total = palpites.size();
        erros = total - acertos;

        mHolder.acertos.setText(String.valueOf(acertos));
        mHolder.erros.setText(String.valueOf(erros));
        mHolder.total.setText(String.valueOf(total));

        if(adapter == null){
            adapter = new AdapterPalpiteDetail(activity, palpites);

            mHolder.recyclerView.setAdapter(adapter);
            mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        }
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PALPITE:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){

                        if(response.has("Object")){
                            try {
                                Gson gson = new Gson();
                                JSONObject obj = response.getJSONObject("Object");
                                PalpiteDetailData[] data = gson.fromJson(obj.getString("itens"), PalpiteDetailData[].class);

                                if(data != null){
                                    List<PalpiteDetailData> palpites = new ArrayList<>(Arrays.asList(data));
                                    initPalpites(palpites);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        ImageView imgTime1;
        ImageView imgTime2;
        TextView gols1;
        TextView gols2;
        TextView empate;
        View versus;
        TextView moedas;

        TextView acertos;
        TextView erros;
        TextView total;

        TextView voltar;

        RecyclerView recyclerView;

        public ViewHolder (){
            imgTime1 = findViewById(R.id.img_time_1);
            imgTime2 = findViewById(R.id.img_time_2);
            gols1 = findViewById(R.id.txt_result_1);
            gols2 = findViewById(R.id.txt_result_2);
            empate = findViewById(R.id.txt_empate);
            versus = findViewById(R.id.txt_versus);
            moedas = findViewById(R.id.txt_moedas);

            acertos = findViewById(R.id.txt_acertos);
            erros = findViewById(R.id.txt_erros);
            total = findViewById(R.id.txt_total);

            voltar = findViewById(R.id.txt_voltar);

            recyclerView = findViewById(R.id.view_respostas);
        }
    }
}
