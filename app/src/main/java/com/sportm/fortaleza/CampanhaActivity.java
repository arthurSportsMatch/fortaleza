package com.sportm.fortaleza;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import adapter.CupomAdapter;
import data.VoucherData;
import data.VoucherItemData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class CampanhaActivity extends MainActivity {

    AlertDialog dialog;
    ViewHolder mHolder;
    CupomAdapter adapter;
    VoucherData data;
    int id;
    int coluns = 0;
    int rows = 0;
    int cupons = 0;

    boolean hasVisualized;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campanha);


        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);

        mHolder = new ViewHolder();
        initCampanha();

        setSupportActionBar(mHolder.tolbar);
        setTitle("Campanha");
    }

    void initActions (){
        if(!hasVisualized && !prefs.containsKey(CONSTANTS.SHARED_PREFS_KEY_VIEW_TUTORIAL)){
            showHowWork(data.getUrl(),"");
            prefs.put(CONSTANTS.SHARED_PREFS_KEY_VIEW_TUTORIAL, "view");
        }

        mHolder.resgatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkEditCode()){
                    useCupom();
                    //makeMessage(true, "teste", true);
                }
            }
        });

        mHolder.how.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_VOUCHER_AJUDA, id).execute();
                showHowWork(null, data.getTerms());
            }
        });

        mHolder.premio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_VOUCHER_ABRIRLISTA, id).execute();
                Intent intent = new Intent(CampanhaActivity.this, ResgatadosActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    //region Cartela
    void initCampanha (){
        Hashtable<String,Object> params = new Hashtable<>();
        params.put("id", id);

        new AsyncOperation(this, AsyncOperation.TASK_ID_GET_VOUCHER_DETAILS, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Object")){
                    try {
                        Gson gson = new Gson();
                        data = gson.fromJson(response.getString("Object"), VoucherData.class);

                        if(adapter != null){
                            refreshCampanha();
                        }else{
                            setCampanha();
                        }
                        initActions();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {
                initCampanha();
            }
        }).execute(params);
    }

    //Faz refresh da Cartela
    void refreshCampanha (){
        cupons = data.getScore();
        adapter.setCupons(cupons);
    }

    //Cria a Cartela
    void setCampanha (){
        cupons = data.getScore();
        coluns = data.getCols();

        if(data.getItems() != null && data.getItems().size() > 0){
            int qtn = 0;
            for(int i = 0; i < data.getItems().size(); i ++){
                if(data.getItems().get(i).getQtd() > qtn){
                    qtn = data.getItems().get(i).getQtd();
                }
            }
            rows = qtn/coluns;
        }

        Glide.with(this)
                .asBitmap()
                .load(CONSTANTS.serverContentImages + data.getImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        Bitmap bitmap = resource;
//                        int dimension = getSquareCropDimensionForBitmap(bitmap);
//                        bitmap = ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension);
                        createCropedImageViews(cropedImages(bitmap, coluns, rows), data.getItems());
                    }
                });

        String method = data.getTerms();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, method,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data.setTerms(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

        if(data.getReset() > 0)
            mHolder.reset.setVisibility(View.VISIBLE);
    }

    //Pega menor tamanho entre largura e altura
    public int getSquareCropDimensionForBitmap(Bitmap bitmap) {


        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public int getSquareCropDimensionForBitmap(Bitmap bitmap, int rows, int coluns) {


        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    //Pega lista de imagens cortadas
    List<Bitmap> cropedImages (Bitmap image, final int coluns, final int rows){

        int paddingX = 0, paddingY = 0, itemWidth = 0, itemHeight = 0;
        List<Bitmap> imgs = new ArrayList<>();

        float imageRatio = (float)image.getWidth() / (float)image.getHeight();
        float cropRatio = (float)coluns / (float)rows;

        //É mais largo que a imagem
        boolean isWidderThanCrop = imageRatio > cropRatio;

        Log.d("Bitmap-Images", "imageW " + image.getWidth() + " imageH " + image.getHeight());

        if(isWidderThanCrop){
            itemWidth = Math.round(image.getHeight() * cropRatio);
            itemHeight = image.getHeight();
            paddingX = Math.round((float)(image.getWidth() - itemWidth) / 2f);
        }else{
            itemHeight = Math.round(image.getWidth() / cropRatio);
            itemWidth = image.getWidth();
            paddingY = Math.round((float)(image.getHeight() - itemHeight) / 2f);
        }

        Log.d("Bitmap-Images", "Widder " + isWidderThanCrop + " width " + itemWidth + " height " + itemHeight + " cropRatio " + cropRatio + " padingX " + paddingX + " paddingY " + paddingY);

        itemWidth = itemWidth / coluns;
        itemHeight = itemHeight / rows;

        for(int y = 0; y < rows; y ++){
            for(int x = 0; x < coluns; x ++){
                int xx = paddingX + (itemWidth * x);
                int yy = paddingY + (itemHeight * y);
                imgs.add(Bitmap.createBitmap(image, xx, yy, itemWidth, itemHeight));
            }
        }


        return imgs;
    }

    //Cria o adapter
    void createCropedImageViews (List<Bitmap> list , List<VoucherItemData> itens){
        adapter = new CupomAdapter(this, list, itens, cupons, new CupomAdapter.Interaction() {
            @Override
            public void OnClick(int position) {
                new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_VOUCHER_MOSTRARITEM, data.getItems().get(position).getId()).execute();
                resgatarCupom(position);
            }

            @Override
            public void AnimAlpha() {

            }

            @Override
            public void AnimSpin() {
                adapter.setCuponAdd(-1);
                adapter.setAnimState("lastAlpha");
            }

            @Override
            public void AnimLast() {
                Log.d("Anim-Adapter-C", "finished");
            }
        });
        mHolder.grid.setAdapter(adapter);
        mHolder.grid.setNumColumns(coluns);
    }

    //Pega o tamanho da imagem
    int getImageSize (Bitmap image){
        return image.getWidth();
    }

    //Pega o tamanho do cortes da imagem
    int getCutSize (int size, int cuts){
        return size/cuts;
    }
    //endregion

    //Verifica se o EditText tem algum valor escrito
    boolean checkEditCode (){
        if(mHolder.codigo.getText().toString().isEmpty()){
            mHolder.codigo.setError("Coloque o codigo antes");
            return false;
        }

        return true;
    }

    //Usar o cupom após todas validações
    void useCupom (){
        Hashtable<String, Object> params = new Hashtable<>();

        params.put("id", id);
        String codigo = mHolder.codigo.getText().toString();
        codigo = codigo.replace(" ", "");

        params.put("code", codigo);

        new AsyncOperation(this, AsyncOperation.TASK_ID_POST_VOUCHER_CODE, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Message")){
                    int status = 0;
                    String message = "";
                    try {
                        status = response.getInt("Status");

                        if(status == 200){
                            message = response.getString("Message");
                            makeMessage(true, message, true);
                        }

                        if(status == 202){
                            message = response.getString("Message");
                            makeMessage(false, message, true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }, TRACKING.TRACKING_VOUCHER_SUBMETERCODIGO, id).execute(params);
    }

    //Cria mensagem com botão de ação
    void makeMessage (final boolean acertou, String message){
        new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PREMIOVOUCHER_RESGATAR, id).execute();
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(acertou){
                            new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PREMIOVOUCHER_RESGATARSIM, id).execute();
                            initCampanha();
                        }
                        new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_PREMIOVOUCHER_RESGATARNAO, id).execute();
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    void makeMessage (final boolean acertou, String message, final boolean resgate){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(acertou && resgate){
                            //initCampanha();
                            startCupomAnimation();
                        }
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    void startCupomAnimation (){
        if(adapter == null){
            return;
        }

        cupons ++;
        adapter.setCupons(cupons);
        adapter.setCuponAdd(cupons);
        adapter.notifyDataSetChanged();

        //        Hashtable<String,Object> params = new Hashtable<>();
//        params.put("id", id);
//
//        new AsyncOperation(this, AsyncOperation.TASK_ID_GET_VOUCHER_DETAILS, 0, new AsyncOperation.IAsyncOpCallback() {
//            @Override
//            public void CallHandler(int opId, JSONObject response, boolean success) {
//                if(success){
//                    this.OnAsyncOperationSuccess(opId, response);
//                }else{
//                    this.OnAsyncOperationError(opId, response);
//                }
//            }
//
//            @Override
//            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
//                if(response.has("Object")){
//                    try {
//                        Gson gson = new Gson();
//                        data = gson.fromJson(response.getString("Object"), VoucherData.class);
//                        //cupons = data.getScore();
//                        cupons ++;
//                        adapter.setCupons(cupons);
//                        adapter.setCuponAdd(cupons);
//                        adapter.notifyDataSetChanged();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void OnAsyncOperationError(int opId, JSONObject response) {
//                initCampanha();
//            }
//        }).execute(params);
    }

    //Resgatar ao clicar em um cupom virado
    void resgatarCupom (final int idd){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_ver_premio, null);

        TextView nome = mView.findViewById(R.id.txt_premio_name);
        TextView qnt = mView.findViewById(R.id.txt_cupom_qnt);
        TextView aviso = mView.findViewById(R.id.txt_aviso);
        Button resgatar = mView.findViewById(R.id.btn_resgatar);

        if(data.getReset() > 0){
            aviso.setVisibility(View.VISIBLE);
        }

        //Valores do dialog
        qnt.setText(String.valueOf(data.getItems().get(idd).getQtd()));
        if(!data.getItems().get(idd).getTitle().isEmpty())
            nome.setText(data.getItems().get(idd).getTitle());

        if(cupons < data.getItems().get(idd).getQtd()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                resgatar.setBackgroundColor(getColor(R.color.colorButtonRedTransp));
            }
        }

        //Button
        resgatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CampanhaActivity.this);
                alertDialog.setMessage("Realmente deseja resgatar este prêmio?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        redeemPrize(data.getItems().get(idd).getId());
                    }
                }).setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });

        mBuilder.setView(mView);
        dialog = mBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    //Mostrar Como funciona
    void showHowWork (String video, final String html){
        hasVisualized = true;
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_como_funciona, null);

        TextView name = mView.findViewById(R.id.txt_card_name);
        View holderVideo =  mView.findViewById(R.id.holder_video);
        View holderHtml = mView.findViewById(R.id.holder_html);

        WebView videoHtml = mView.findViewById(R.id.web_dialog_video);
        final WebView textHtml = mView.findViewById(R.id.web_dialog_html);
        Button action = mView.findViewById(R.id.btn_howto_action);
        Button action2 = mView.findViewById(R.id.btn_howto_action2);

        if(video != null){
            holderVideo.setVisibility(View.VISIBLE);
            holderHtml.setVisibility(View.GONE);

            String frameVideo = "<iframe width=\"100%\" height=\"315\" src=\""+video+"\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

            WebSettings webSettings = videoHtml.getSettings();
            webSettings.setJavaScriptEnabled(true);
            videoHtml.loadData(frameVideo,"text/html", "utf-8");
            name.setText("Campanha");
            action2.setText("ENTENDI");
        }
        else {
            holderVideo.setVisibility(View.GONE);
            holderHtml.setVisibility(View.VISIBLE);

            textHtml.getSettings().setUseWideViewPort(true);
            textHtml.getSettings().setLoadWithOverviewMode(true);
            String embed = loadHtmlEmbed(html);
            textHtml.loadData(embed, "text/html; charset=utf-8", null);

            action.setText("VER VÍDEO DA CAMPANHA");
        }

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        if(video != null){
            new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_AJUDAVOUCHER_CONFIRMAR, id).execute();
            action2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }else{
            new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, MainActivity.emptyAsync, TRACKING.TRACKING_AJUDAVOUCHER_VERVIDEO, id).execute();
            action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHowWork(data.getUrl(),"");
                    dialog.dismiss();
                }
            });
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    //Faz o load dos valores da receita em html
    private String loadHtmlEmbed(String embed) {

        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<!DOCTYPE html>");
            stringBuilder.append("<html>");
            stringBuilder.append("<head> <meta name=\"viewport\" content=\"width=100, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no\" /> </head>");
            stringBuilder.append("<body>");
            stringBuilder.append("<style> span { display: block; max-width: 100%; height: auto; } </style>");


            List<String> list = new ArrayList<>();

            List<String> images = UTILS.getStringsBetweenStrings(embed, "<img src=\"", "\"");
            int size = images.size();
            String replacement = "%s\" onclick=\"AndroidFunction.expand('%s')";
            for(int i = 0; i < size; i++){
                embed = embed.replaceFirst(images.get(i), String.format(replacement, images.get(i), images.get(i)));
            }

            stringBuilder.append(embed);
            stringBuilder.append("\n<script language=\"javascript\">\n" +
                    "function expand(str) {\n" +
                    "    AndroidFunction.expand(str);\n" +
                    "}\n" +
                    "</script>");


            stringBuilder.append("</body>");
            stringBuilder.append("</html>");


            String fullHtml = stringBuilder.toString();
            return fullHtml;
        }
        catch (Exception e){
            UTILS.DebugLog("Error", e);
            return "";
        }
    }

    public class MyJavaScriptInterface {
        Context mContext;

        MyJavaScriptInterface(Context c) {
            mContext = c;
        }

        @android.webkit.JavascriptInterface
        public void expand(String strl){
            Bundle b = new Bundle();
            b.putString("media", strl);
            Message msg = new Message();
            msg.setData(b);
            //htmlImageClickCallback.sendMessage(msg);
        }
    }

    //Resgatar premios
    void redeemPrize (int idd){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", idd);

        new AsyncOperation(this, AsyncOperation.TASK_ID_POST_PRIZE_REDEEM, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                if(response.has("Message")){
                    int status = 0;
                    String message = "";
                    try {
                        status = response.getInt("Status");

                        if(status == 200){
                            message = response.getString("Message");
                            makeMessage(true, message);
                            dialog.dismiss();
                        }

                        if(status == 202){
                            message = response.getString("Message");
                            makeMessage(false, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute(params);
    }

    class ViewHolder {
        Toolbar tolbar;
        GridView grid;
        Button how;
        ImageView premio;

        EditText codigo;
        TextView resgatar;
        TextView reset;

        public ViewHolder (){
            tolbar = findViewById(R.id.topbar);
            grid = findViewById(R.id.grid_cupons);
            how = findViewById(R.id.btn_como_funciona);
            premio = findViewById(R.id.btn_presente);

            codigo = findViewById(R.id.edt_codigo);
            resgatar = findViewById(R.id.txt_resgatar);
            reset = findViewById(R.id.txt_reset);
        }
    }
}
