 package com.sportm.fortaleza;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Hashtable;

import data.SocioData;
import data.UserData;
import informations.UserInformation;
import models.MessageModel;
import models.user.UserModel;
import repository.socio.SocioRepository;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MaskEditUtil;
import utils.TRACKING;
import utils.UTILS;
import vmodels.socio.SocioViewModel;
import vmodels.user.UserViewModel;

 public class SocioTorcedorActivity extends MainActivity{

     private ViewHolder mHolder;
     private UserViewModel userViewModel;
     private SocioViewModel socioViewModel;

     int socio = 0;
     int beneficio = 0;
     String socioStatus = "";
     String phone = "55 (85) 99194-3513";

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_socio_torcedor);

         mHolder = new ViewHolder();
         initActions();
     }

     @Override
     public void bindViewModel() {
         super.bindViewModel();
         UserRepository repository = new UserRepository(activity, prefs);
         userViewModel = new ViewModelProvider(SocioTorcedorActivity.this).get(UserViewModel.class);
         userViewModel.setmRepository(repository);
         userViewModel.getUserData().observe(this, new Observer<UserModel>() {
             @Override
             public void onChanged(UserModel user) {
                 uploadUser(user);
             }
         });
         userViewModel.getMessage().observe(this, new Observer<MessageModel>() {
             @Override
             public void onChanged(MessageModel message) {
                 showMessage(message.getMsg());
             }
         });
         userViewModel.getUser(false);

         SocioRepository socioRepository = new SocioRepository(activity, prefs);
         socioViewModel = new ViewModelProvider(SocioTorcedorActivity.this).get(SocioViewModel.class);
         socioViewModel.setmRepository(socioRepository);
         socioViewModel.getMessage().observe(this, new Observer<MessageModel>() {
             @Override
             public void onChanged(MessageModel message) {
                 if(message.getId() == 1){
                     String cpf = MaskEditUtil.unmask(mHolder.cpf.getText().toString());
                     userViewModel.getSocio(cpf);
                 }
             }
         });
     }

     @Override
     public void unBindViewModel() {
         super.unBindViewModel();
         socioViewModel.getMessage().removeObservers(this);
         userViewModel.getUserData().removeObservers(this);
     }

     //Inicia ações
     void initActions (){
         mHolder.cpf.addTextChangedListener(MaskEditUtil.mask(mHolder.cpf, MaskEditUtil.FORMAT_CPF));
         mHolder.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(socio <= 0){
                    if(checkCPF()){
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SOCIO_CONFIRMAR).execute();
                        getSocioAPI();
                    }
                }else if (socio == 1){
                    String cpf = MaskEditUtil.unmask(mHolder.cpf.getText().toString());
                    if(!cpf.isEmpty()){
                        initPopUp(true,"Você tem certeza que deseja alterar o plano sócio torcedor vinculado a sua conta do app?");
                    }
                    else{
                        initPopUp(false,"Você tem certeza que deseja desvincular seu plano sócio torcedor do app? Você perderá acesso as funcionalidades exclusivas");
                    }
                }
            }
        });
     }

     //Faz update do usuario
     void uploadUser (UserModel user){

         socio = user.getSocioTorcedor();
         beneficio = user.getSocioBeneficio();

         if(!user.getSocioStatus().isEmpty())
             socioStatus = user.getSocioStatus();

         if(!user.getCelular().isEmpty())
             phone = user.getCelular();


         if(!user.getCpf().isEmpty()) {
             String cpf = user.getCpf();
             String finalCpf = cpf.substring(0, 3) + ".***.***-**";

             mHolder.cpf.setHint(finalCpf);
         }else{
             mHolder.cpf.setHint(getResources().getString(R.string.cpf));
         }

         changeStatusMessage(user);
     }

     //Muda header mensage
     void changeStatusMessage (UserModel user){
         if(socio == 1){
             //Atualiza mensagem de CPF
             if(!user.getCpf().isEmpty()) {
                 mHolder.message.setText(getResources().getString(R.string.socio_cpf));
                 mHolder.confirm.setText(getResources().getString(R.string.socio_atualizar));
             }

             if(!socioStatus.isEmpty()){
                 mHolder.viewStatusSocio.setVisibility(View.VISIBLE);

                 String text = mHolder.socioStatus.getText().toString();
                 text = text.replace("#", UTILS.camelCase(getTextShowMessage(socioStatus)));
                 SpannableString ss = new SpannableString(text);
                 ss.setSpan(new ForegroundColorSpan(getResources().getColor(getColor(socioStatus))), 8, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                 mHolder.socioStatus.setText(ss);

                 String statusMessage = getResources().getString(getTextMessage(socioStatus));


                 if(socioStatus.equals("inadimplente")){
                     if(!phone.isEmpty()){
                         statusMessage = statusMessage + "por este telefone " + phone;
                         SpannableString ssText = new SpannableString(statusMessage);
                         ClickableSpan clickableSpan1 = new ClickableSpan() {
                             @Override
                             public void onClick(View widget) {
                                 phone = MaskEditUtil.unmask(phone);
                                 Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                                 startActivity(intent);
                             }

                             @Override
                             public void updateDrawState(TextPaint ds) {
                                 super.updateDrawState(ds);
                                 ds.setColor(getResources().getColor(R.color.colorRedSPFC7));
                                 ds.setUnderlineText(false);
                             }
                         };

                         ssText.setSpan(clickableSpan1, (statusMessage.length() - phone.length()), statusMessage.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                         mHolder.statusMessage.setText(ssText);
                     }else{
                         mHolder.statusMessage.setText(statusMessage);
                     }

                 }else {
                     mHolder.statusMessage.setText(statusMessage);
                 }

             }else{
                 mHolder.viewStatusSocio.setVisibility(View.GONE);
             }

         }else{
             mHolder.viewStatusSocio.setVisibility(View.GONE);
             mHolder.message.setText(getResources().getString(R.string.socio_no_cpf));
             mHolder.confirm.setText(getResources().getString(R.string.socio_confirmar));
         }
     }

     //Inicia popUp de mudar cpf
     void initPopUp (final boolean hasCpf, String message){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_SOCIO_CONFIRMAR).execute();
            if(hasCpf){
                getSocioAPI();
            }else{
                socio = 0;
                beneficio = 0;
                socioStatus = "";

                UserModel _user = new UserModel();
                _user.setSocioTorcedor(0);
                _user.setSocioBeneficio(0);
                _user.setSocioStatus("");
                _user.setCpf("");
                userViewModel.uploadUser(_user);
            }
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

     //Verifica se é um cpf valido
     boolean checkCPF (){
         String cpf = MaskEditUtil.unmask(mHolder.cpf.getText().toString());
         if(!cpf.isEmpty()){
             if(cpf.length() == 11){
                 return true;
             }else{
                 mHolder.cpf.setError("Coloque um CPF valido");
             }
         }else{
             mHolder.cpf.setError("Coloque seu CPF aqui");
             return false;
         }

        return false;
     }

     //Pega o socio Api do servidor
     void getSocioAPI (){
         socioViewModel.getSocioAPI();
     }

     //Texto de status
     int getTextMessage (String status){
         status = status.toUpperCase();
         switch (status){
             case "ATIVO":
             case "QUITADA":
             case "ATRASADO":
             case "ADIMPLENTE": return R.string.socio_message_ativo;
             case "INADIMPLENTE": return R.string.socio_message_inadimplente;
             case "A_INICIAR": return R.string.socio_message_ainiciar;
         }
         return R.string.socio_message_ativo;
     }

     //Pega o texto de situação
     String getTextShowMessage (String status){
         status = status.toUpperCase();
         switch (status){
             case "ATIVO":
             case "QUITADA":
             case "ATRASADO":
             case "ADIMPLENTE": return "ATIVO";
             case "INADIMPLENTE": return "INADIMPLENTE";
             case "A_INICIAR": return "A INICIAR";
         }
         return "A INICIAR";
     }

     //Pega a cor que o testo vai passar
     int getColor (String status){
         status = status.toUpperCase();
         switch (status){
             case "ATIVO":
             case "QUITADA":
             case "ATRASADO":
             case "ADIMPLENTE":
             case "A_INICIAR": return R.color.colorRedSPFC2;
             case "INADIMPLENTE" : return R.color.colorGold;
         }
         return R.color.colorRedSPFC2;
     }

     class ViewHolder {
        EditText cpf;
        Button confirm;
        TextView verificando;

        TextView message;

        View viewStatusSocio;
        TextView socioStatus;
        TextView statusMessage;

        public ViewHolder (){
            cpf = findViewById(R.id.edt_socio_cpf);
            confirm = findViewById(R.id.btn_socio_confirm);
            verificando = findViewById(R.id.txt_verificando);

            message = findViewById(R.id.txt_message);
            viewStatusSocio = findViewById(R.id.view_status_socio);
            socioStatus = findViewById(R.id.txt_status);
            statusMessage = findViewById(R.id.txt_status_message);
        }
    }
}
