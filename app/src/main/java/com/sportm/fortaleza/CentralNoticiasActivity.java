package com.sportm.fortaleza;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.NoticiaAdapter;
import data.BannerData;
import data.FilterData;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

public class CentralNoticiasActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback{

    ViewHolder mHolder;
    NoticiaAdapter adapter;

    boolean hasData = false;
    boolean resetData = false;
    String filters = "";
    int page = 1;

    private static final int OP_ID_NOTICIA              = 0;
    private static final int OP_ID_FILTERS              = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central_noticias);

        mHolder = new ViewHolder();
        getFilters();
        getNoticias();
        initAction();
    }


    void initAction (){
        mHolder.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setViewVisibility();
            }
        });

        mHolder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_FILTROS_FECHAR).execute();
                removeView();
            }
        });
    }

    void setViewVisibility (){
        if(mHolder.filtros.getVisibility() == View.VISIBLE){
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_FILTROS_FECHAR).execute();
            mHolder.filtros.setVisibility(View.GONE);
            return;
        }else{
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_FILTROS_ABRIR).execute();
            mHolder.filtros.setVisibility(View.VISIBLE);
            return;
        }
    }

    void removeView (){
        if(mHolder.filtros.getVisibility() == View.VISIBLE){
            mHolder.filtros.setVisibility(View.GONE);
            return;
        }
    }

    void getNoticias (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTICIAS, OP_ID_NOTICIA, this).execute(params);
    }

    void getFilters (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTICIAS_FILTERS, OP_ID_FILTERS, this).execute();
    }

    void getNoticias (String filters){
        this.filters = filters;
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("filter", filters);
        params.put("page", page);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTICIAS, OP_ID_NOTICIA, this).execute(params);
    }

    void initNoticias (List<BannerData> data){
        if(adapter == null){
            adapter = new NoticiaAdapter(activity, data, new NoticiaAdapter.Listener() {
                @Override
                public void OnCick(BannerData data) {
                    if(data.getUrl() != null){
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_SELECIONAR, data.getId()).execute();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(data.getUrl()));
                        startActivity(i);
                    }else{
                        Intent intent = new Intent(activity, NoticiaActivity.class);
                        intent.putExtra("id", data.getId());
                        startActivity(intent);
                        //AdMobController.starIntetentBeforeAd(activity, intent, AdMobController.HOME_INTERSECTIAL_NOTICIA_ID);
                    }
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_HOME_NOTICIAOPEN, data.getId()).execute();
                }

                @Override
                public void OnLast() {
                    GetByPagination();
                }
            });
            mHolder.recyclerView.setAdapter(adapter);
            mHolder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        }else{
            if(data != null){
                if(resetData){
                    adapter.ChangeData(data);
                    resetData = false;
                }else{
                    adapter.AddMoreData(data);
                }

            }
        }
    }

    void initFilters (List<FilterData> data){
        data.add(0, new FilterData(0,"Todos"));

        for(FilterData pos : data){

            RadioButton button = (RadioButton) LayoutInflater.from(activity).inflate(R.layout.include_radio_button, null, false);
            float margin = getResources().getDimension(R.dimen.margin_10);
            float pading = getResources().getDimension(R.dimen.margin_15);

            RadioGroup.LayoutParams param = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            button.setPadding((int)pading,(int)margin,(int)pading,(int)margin);

            button.setText(pos.getTitulo());
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP , 14);
            button.setMaxLines(1);

            button.setBackground(getResources().getDrawable(R.drawable.radio_flat_selector_rec));
            button.setTextColor(getResources().getColorStateList(R.color.radio_white_black));

            final String ID = String.valueOf(pos.getId());
            button.setLayoutParams(param);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_DESTAQUES_FILTROS_SELECIONAR, ID).execute();
                    int id = v.getId();
                    mHolder.radioGroup.check(id);
                    if(ID.equals("0")){
                        page = 1;
                        getNoticias();
                        resetData = true;
                    }else{
                        page = 1;
                        getNoticias(ID);
                        resetData = true;
                    }
                    mHolder.filtros.setVisibility(View.GONE);
                }
            });

            mHolder.radioGroup.addView(button);
        }
    }

    void GetByPagination (){
        if(hasData){
            page ++;
            if(filters.isEmpty()){
                getNoticias();
            }else {
                getNoticias(filters);
            }
            hasData = false;
        }
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_NOTICIA :{
                //Noticia
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {

                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            BannerData[] data = gson.fromJson(obj.getString("itens"), BannerData[].class);

                            if(data != null && data.length > 0){
                                List<BannerData> banner = new ArrayList<>(Arrays.asList(data));
                                initNoticias(banner);
                                hasData = true;
                            }else{
                                Toast.makeText(activity, "Sem destaques para este filtro", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_ID_FILTERS :{
                //Noticia
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            FilterData[] data = gson.fromJson(obj.getString("itens"), FilterData[].class);
                            if(data != null){
                                List<FilterData> filterData = new ArrayList<>(Arrays.asList(data));
                                initFilters(filterData);
                                hasData = true;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){

        }
    }

    class ViewHolder {
        RecyclerView recyclerView;
        RadioGroup radioGroup;
        View filtros;
        TextView close;
        ImageView filter;

        public ViewHolder (){

            filter = findViewById(R.id.img_filter);
            filtros = findViewById(R.id.filtros);
            close = findViewById(R.id.close);
            recyclerView = findViewById(R.id.noticias_recycler);
            radioGroup = findViewById(R.id.radio_group);
        }
    }
}
