package com.sportm.fortaleza;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import data.Audio;
import data.EndSessionData;
import data.ServerCustomData;
import data.UserData;
import informations.UserInformation;
import repository.MainRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.SecurePreferences;
import utils.TRACKING;
import utils.UTILS;

//classe que contém todos os valores iniciais necessarios das classes
public class MainActivity extends AppCompatActivity {

    protected Activity activity;
    protected String[] permissions = new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static SecurePreferences prefs = null;
    public static MainActivity instance;
    boolean registredStop = false;

    protected static double latestRegVer;
    protected static String regulamento =  "";

    private static final int OP_CHECK_VERSION = 1242;
    private static final int OP_ID_GET_SERVER_LIST = 2123;
    private static final int OP_ID_SING_OUT = 3234;


    public static final String Broadcast_PLAY_NEW_AUDIO = "com.devarthur.fortaleza.PlayNewAudio";
    public static MediaPlayerService player;
    boolean serviceBound = false;
    List<Audio> audioList;
    int actualAudio = -1;

    public static AsyncOperation.IAsyncOpCallback emptyAsync = new AsyncOperation.IAsyncOpCallback() {
        @Override
        public void CallHandler(int opId, JSONObject response, boolean success) {

        }

        @Override
        public void OnAsyncOperationSuccess(int opId, JSONObject response) {

        }

        @Override
        public void OnAsyncOperationError(int opId, JSONObject response) {

        }
    };

    private AsyncOperation.IAsyncOpCallback mainCallback = new AsyncOperation.IAsyncOpCallback() {
        @Override
        public void CallHandler(int opId, JSONObject response, boolean success) {
            if(success) {
                this.OnAsyncOperationSuccess(opId, response);
            }
            else{
                this.OnAsyncOperationError(opId, response);
            }
        }

        @Override
        public void OnAsyncOperationSuccess(int opId, JSONObject response) {

            switch (opId){
                case OP_CHECK_VERSION:{
                    Log.d("Header", "OP_CHECK_VERSION");
                    String msg = "";
                    String url = "";
                    int status = 0;

                    try{
                        if((response.has("sId")) && (response.getInt("sId") > 0)){
                            UserInformation.setSessionId((int) response.get("sId"));
                        }

                        if( (response.has("serverVersion")) && (response.get("serverVersion") != JSONObject.NULL) ) {
                            int serverVersion = response.getInt("serverVersion");
                            if(serverVersion > UserInformation.getServerData().getServerVersion()){
                                getServerList();
                                return;
                            }
                        }

                        if( (response.has("Message")) && (response.get("Message") != JSONObject.NULL) ){
                            msg = response.getString("Message");
                        }

                        if( (response.has("url")) && (response.get("url") != JSONObject.NULL) ){
                            url = response.getString("url");
                        }

                        if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) ){
                            status = response.getInt("Status");
                        }

                        if( (response.has("idServidor")) && (response.get("idServidor") != JSONObject.NULL) ) {
                            int idServidor = response.getInt("idServidor");

                            if(UserInformation.getServerData().getServerId() != idServidor){
                                UserInformation.getServerData().setServerId(idServidor);
                                UTILS.salvarServidor(UserInformation.getServerData());
                                CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServer(idServidor) + "api/");
                                CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                            }
                        }

                        if( (response.has("api")) && (response.get("api") != JSONObject.NULL) ) {
                            String version = response.getString("api");
                            if(!CONSTANTS.serverURL.contains(version)) {
                                CONSTANTS.serverVersion = version + "/";
                                CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                            }
                        }


//
                        if(response.has("regulamento")){
                            MainActivity.regulamento = response.getString("regulamento");
                        }

                    } catch (JSONException e) {
                        UTILS.DebugLog("Error", e);
                    }

                    if(status == CONSTANTS.ERROR_CODE_UPDATE_SUGGESTED) {
                        CallUpdateSuggestion(msg, url);
                    }else if(status == CONSTANTS.ERROR_CODE_MAINTENANCE){
                        CallUpdateSuggestion(msg);
                    }
                }
                break;

                case OP_ID_GET_SERVER_LIST:{
                    Log.d("Header", "OP_ID_GET_SERVER_LIST");
                    int status = 0;
                    if(response.has("Status")){
                        try {
                            status = response.getInt("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if(status == 200){
                        if (!response.toString().isEmpty()){
                            try {
                                Gson gson = new Gson();
                                ServerCustomData data = gson.fromJson(response.getString("Object"), ServerCustomData.class);

                                boolean suceess = true;

                                try{
                                    if(data == null){
                                        suceess = false;
                                    }else if (data.getMedia() == null && data.getMedia().size() <= 0){
                                        suceess = false;
                                    } else if (data.getApi() == null && data.getApi().size() <= 0){
                                        suceess = false;
                                    }
                                }catch (Exception e){
                                    suceess = false;
                                    Log.e("Async",  "Erro ao pegar servers" + e);
                                }

                                if(suceess){
                                    UserInformation.setServerData(data);
                                    UTILS.salvarServidor(data);

                                    if(data.getIdUser() > 0){
                                        UserInformation.getServerData().setIdUser(data.getIdUser());
                                        UTILS.salvarServidor(UserInformation.getServerData());
                                    }

                                    CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServerSelected() + "api/");
                                    CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                                    mainGetVersion();
                                }else{
                                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_TRACK_ERROR, 345, emptyAsync, TRACKING.TRACKING_GETSERVER_ERROR).execute();
                                    if(UserInformation.getServerData() != null){
                                        UserInformation.getServerData().setServerId(0);
                                    }
                                    CallUpdateSuggestion("Servidores em Manutenção \n Codigo: 225");
                                    //getServerList();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;

                case OP_ID_SING_OUT:{
                    Log.d("Header", "OP_ID_SING_OUT");
                    int status = 0;
                    if(response.has("Status")){
                        try {
                            status = response.getInt("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if(status == 200){
                        mainGetVersion();
                    }
                }
                break;
            }
        }

        @Override
        public void OnAsyncOperationError(int opId, JSONObject response) {
            switch (opId){
                case OP_ID_SING_OUT:{

                    int status = 0;
                    if(response.has("Status")){
                        try {
                            status = response.getInt("Status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if(status == 200){
                        mainGetVersion();
                    }
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        prefs = new SecurePreferences(getApplicationContext(), CONSTANTS.SHARED_PREFS_KEY_FILE, CONSTANTS.SHARED_PREFS_SECRET_KEY, true);
        activity = this;

        if(instance == null){
            instance = this;
            registerBroadCasts();
            MyAplication.intance.setActivity(instance);
        }

        //Fazer load e criação do UserData
        Gson gson = new Gson();
        UserData user = gson.fromJson(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_AUTOLOGIN, ""), UserData.class);//gson.fromJson(String.valueOf(autologinData), UserData.class);
        //user.setSocioTorcedor(UserInformation.getUserData().getSocioTorcedor());
        UserInformation.setUserData(user);

        //Para usar ads como device de teste
//        RequestConfiguration requestConfiguration = new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("BBBE64DE0CAD6B315337FBD9F887AC9A")).build();
//        MobileAds.setRequestConfiguration(requestConfiguration);

        //Ads
        MobileAds.initialize(activity, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
    }

    @Override
    protected void onResume() {
        if(!UTILS.isAppRunning()){
            setSession();
        }

        UTILS.setAppRunning(true);
        bindViewModel();
        bindBroadcast();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unBindViewModel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            unbindService(serviceConnection);
            unRegisterBroadCasts();
        }
        unBindBroadcast();
    }

    public void bindViewModel (){

    }

    public void unBindViewModel (){

    }
    public void bindBroadcast (){

    }

    public void unBindBroadcast (){

    }

    //Public methods
    public void showMessage (String message){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setPositiveButton("Certo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showMessageFinish (String message){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setPositiveButton("Certo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void initCloseAppMessage (){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setMessage("Você deseja fechar o app?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showRegulamento (int v, String data){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_instagram, null);
        ImageView img = customLayout.findViewById(R.id.img_storie);
        WebView web = customLayout.findViewById(R.id.img_storie_video);

        //Visibility
        img.setVisibility(View.GONE);
        web.setVisibility(View.VISIBLE);

        //Load string on embed
        web.getSettings().setUseWideViewPort(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.loadData(data, "text/html; charset=utf-8", null);

        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }


    public void saveSessionValues (){
        Gson gson = new Gson();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        prefs.put(CONSTANTS.SHARED_PREFS_KEY_ENDSESSION, gson.toJson(new EndSessionData(currentDateandTime, UserInformation.getSessionId())));
    }

    public void setSession (){
        if(prefs.containsKey(CONSTANTS.SHARED_PREFS_KEY_ENDSESSION)){
            Gson gson = new Gson();
            endSessionKill(gson.fromJson(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_ENDSESSION, ""), EndSessionData.class));
            prefs.removeValue(CONSTANTS.SHARED_PREFS_KEY_ENDSESSION);
        }
    }

    public void endSessionBackground(){
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("sId", UserInformation.getSessionId());
        params.put("endType", 2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        params.put("time", currentDateandTime);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_END_SESSION, OP_ID_SING_OUT, mainCallback).execute(params);
    }

    public void endSessionKill(EndSessionData data){
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("sId", data.getsId());
        params.put("endType", 1);
        params.put("time", data.getTime());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_SET_END_SESSION, OP_ID_SING_OUT, mainCallback).execute(params);
    }

    public void mainGetVersion (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_CHECK_VERSION, OP_CHECK_VERSION, mainCallback).execute();
    }

    void getServerList (){
        Hashtable<String,Object> params = new Hashtable<>();
        if(UserInformation.getUserId() != 0){
            params.put("idUser", UserInformation.getUserId());
        }

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_SERVIDORES, OP_ID_GET_SERVER_LIST, mainCallback).execute(params);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    public void getInstanceID (){
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
            if (!task.isSuccessful()) {
                Log.w("Token", "getInstanceId failed", task.getException());
                return;
            }

            // Get new Instance ID token
            String token = task.getResult().getToken();

            // Log and toast
            String msg = "Token";//getString(R.string.msg_token_fmt, token);
            Log.d("Token", msg);
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //region Audio
    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;

        }
    };


    //Add Audio on playlist
    public void addAudio (Audio audio){
        if(this.audioList == null){
            this.audioList = new ArrayList<>();
        }
        if(!checkIfAudioExist(audio)){
            this.audioList.add(audio);
            setPlaylist();
        }
    }

    public boolean checkIfAudioExist (Audio audio){
        boolean result = false;
        for(Audio a : this.audioList){
            if(a.getId() == audio.getId()){
                result = true;
            }
        }
        return result;
    }

    void setPlaylist (){
        StorageUtil storage = new StorageUtil(getApplicationContext());
        storage.storeAudio(this.audioList);
    }

    //Play audio from playlist
    public void playAudioFromPlaylist (int id){
        this.actualAudio = id;
        this.playAudio(id);
    }

    //Da play no audio
    private void playAudio(int audioIndex) {
        //Check is service is active
        if (!this.serviceBound) {
            //Store Serializable audioList to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudioIndex(audioIndex);

            Intent playerIntent = new Intent(MainActivity.this, MediaPlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, this.serviceConnection, Context.BIND_AUTO_CREATE);
        }
        else {
            //Store the new audioIndex to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudioIndex(audioIndex);

            //Service is active
            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            sendBroadcast(broadcastIntent);
        }
    }

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onStop = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (serviceBound) {
                unbindService(serviceConnection);
            }
        }
    };

    void registerBroadCasts (){
        //OnStop
        if(instance.registredStop){
            IntentFilter iOnStop = new IntentFilter(MediaPlayerService.ACTION_STOP);
            registerReceiver(onStop, iOnStop);
            instance.registredStop = true;
        }
    }

    void unRegisterBroadCasts(){
        //OnStop
        if(instance.registredStop){
            unregisterReceiver(onStop);
        }
    }

    private void loadAudio() {
        ContentResolver contentResolver = getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            //audioList = new ArrayList<>();
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                audioList.add(new Audio(data, title, album, artist));
            }
        }
        cursor.close();
    }

    //endregion

    void CallUpdateSuggestion(String message){
        finish();
        Intent intent = new Intent(MainActivity.this, UpdateAppActivity.class);
        intent.putExtra("msg", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    void CallUpdateSuggestion(String message, String url){
        finish();
        Intent intent = new Intent(MainActivity.this, UpdateAppActivity.class);
        intent.putExtra("msg", message);
        intent.putExtra("url", url);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
