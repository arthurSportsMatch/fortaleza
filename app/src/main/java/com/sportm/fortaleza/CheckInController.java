package com.sportm.fortaleza;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.CheckinCadeiraData;
import data.CheckinData;
import data.CheckinPartidaData;
import data.CheckinSetoresData;
import data.CheckingDadosData;
import data.LastCheckingData;
import data.PartidaData;
import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MaskEditUtil;
import utils.Timer;
import utils.UTILS;

/*
    - Toda regra de negocio do checkin
    - Todos os dados necessarios pra fazer o checkin {get, set}
    - Toda comunicação com o Model
 */
public class CheckInController implements AsyncOperation.IAsyncOpCallback {

    Activity activity;
    Listener listener;
    CheckinData checkinData;
    Timer timer;
    boolean getSocioTorcedorURL = false;

    private final int  OP_ID_GET_SOCIO_TORCEDOR_URL = 12;
    private final int  OP_ID_VALIDAR_USER = 0;
    private final int  OP_ID_CONSULTAR_SOCIO = 1;
    private final int  OP_ID_CONSULTAR_PROX_JOGOS = 2;
    private final int  OP_ID_CONSULTAR_BACK_JOGOS = 3;
    private final int  OP_ID_CONSULTAR_SETORES = 4;
    private final int  OP_ID_CONSULTAR_DIV_SETORES = 5;
    private final int  OP_ID_ACOMPANHANTES = 13;
    private final int  OP_ID_ASSENTOS_DIV = 6;
    private final int  OP_ID_INGRESSOS = 7;
    private final int  OP_ID_CHECKINGS = 8;
    private final int  OP_ID_CONSULTAR_CHECKINGS = 9;
    private final int  OP_ID_CHECKOUT = 10;

    private final int  TIMER_DEFAULT = 8000;

    public CheckInController(Activity activity) {
        this.activity = activity;
    }

    public CheckInController(Activity activity, Listener listener) {
        this.activity = activity;
        this.listener = listener;
        checkinData = initChecking();
        timer = new Timer(new Timer.Listener() {
            @Override
            public void onTimeOut() {
                listener.onExpired();
            }
        });
    }

    //Inicia os valores principais do checking
    CheckinData initChecking (){
        getSocioAPI();
        CheckinData checkingD = new CheckinData();
        return checkingD;
    }



    //region Jogo
    public void addJogoID (String id){
        checkinData.setJogoID(id);
        listarChecking();
    }

    public void addJogoID (CheckinPartidaData partida){
        checkinData.setPartida(partida);
        checkinData.setJogoID(String.valueOf(partida.getId()));
        CheckinActivity ac = (CheckinActivity) activity;
        ac.setListener(new CheckinActivity.Listener() {
            @Override
            public void OnLoad() {
                listarChecking();
            }
        });
        listarChecking();
    }
    //endregion

    //region Setor
    //Adiciona setor com seu nome
    public void addSetor (String setorName, String setorId, boolean hasCadeiras){
        checkinData.setSetorName(setorName);
        checkinData.setSetorID(setorId);
        checkinData.setHasCadeiras(hasCadeiras);
    }
    //Adiciona id do setor
    void addSetorID (String id){
        checkinData.setSetorID(id);
    }
    //Verifica se existe um setor selecionado
    public boolean haveSetor (){
        return checkinData.getSetorName() != null && checkinData.getSetorID() != null;
    }
    //Verifica se o setor tem cadeiras
    public boolean haveSetorCadeiras (){
        return checkinData.isHasCadeiras();
    }
    //endregion

    public void addAssentoID (String id){
        checkinData.setSetorDiv(id);
    }

    public void addAssentoID (String id, String setorDivName){
        checkinData.setSetorDiv(id);
        checkinData.setSetorDivName(setorDivName);
    }

    public void addCadeiraID (String id, String nome, int index){
        checkinData.addCadeiraId(id, index);
        checkinData.addCadeiras(nome, index);
    }

    public void removerCadeiras() {
        checkinData.setCadeiraID(null);
        checkinData.setCadeiraNome(null);
        checkinData.setCadeiras(null);
    }

    public void removerSetores() {
        checkinData.setSetorName(null);
        checkinData.setEstadio_setor_id(null);
        checkinData.setSetorDiv(null);
    }

    public boolean hasChairsSelectecteds (){

        int size = 0;

        for ( String d: checkinData.getCadeiras() ) {
            if ( !d.equals("") ) {
                size++;
            }
        }

        return size == checkinData.getTickets().size();
    }

    //region Async

    void getSocioAPI (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_SOCIO_API, OP_ID_GET_SOCIO_TORCEDOR_URL, this).execute();
    }

    void validarUsuario (String email, String password){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("email", email);
        params.put("password", password);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_POST_SOCIO_LOGIN, OP_ID_VALIDAR_USER, this).execute(params);
    }

    void consultarSocio (String cpf){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("cpf", cpf);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CONSULTAR_SOCIO, OP_ID_CONSULTAR_SOCIO, this).execute(params);
    }


    void proximosJogos (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CONSULTAR_PROX_JOGOS, OP_ID_CONSULTAR_PROX_JOGOS, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void jogoAnteriores (){
        Hashtable<String, Object> params = new Hashtable<>();
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CONSULTAR_BACK_JOGOS, OP_ID_CONSULTAR_BACK_JOGOS, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void getSetores (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("socio_id", checkinData.getSocioID());
        params.put("jogo_id", checkinData.getJogoID());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CONSULTAR_SETORES, OP_ID_CONSULTAR_SETORES, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void listarDivSetor (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("estadio_id", 1);
        params.put("estadio_setor_id", checkinData.getSetorID());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CONSULTAR_DIV_SETORES, OP_ID_CONSULTAR_DIV_SETORES, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void listarAssentos (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("jogo_id", checkinData.getJogoID());
        params.put("estadio_setor_id", checkinData.getSetorID());
        params.put("estadio_divisao_id", checkinData.getSetorDiv());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_ASSENTOSDIV, OP_ID_ASSENTOS_DIV, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void listarAcompanhantes (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("socio_id", checkinData.getSocioID());
        params.put("jogo_id", checkinData.getJogoID());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_ACOMPANHANTES, OP_ID_ACOMPANHANTES, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void listarIngressos (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("jogo_id", checkinData.getJogoID());
        params.put("estadio_setor_id", checkinData.getSetorID());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_INGRESSOS, OP_ID_INGRESSOS, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void listarChecking (){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("socio_id", checkinData.getSocioID());
        params.put("jogo_id", checkinData.getJogoID());
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_CHECKINS, OP_ID_CONSULTAR_CHECKINGS, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void fazerChecking (){
        CheckinActivity ac = (CheckinActivity) activity;
        ac.setListener(new CheckinActivity.Listener() {
            @Override
            public void OnLoad() {
                fazerChecking();
            }
        });
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("socio_id", checkinData.getSocioID());
        params.put("jogo_id", checkinData.getJogoID());
        params.put("estadio_setor_id", "");
        params.put("opcao", 0); // 1 fazer checkin - 2 fazer checkout
        params.put("valor", checkinData.getValorTotal());
        params.put("ncc", "");
        params.put("cvv", "");
        params.put("val", "");
        params.put("nomes", convertListInString(clearList(checkinData.getNomes())));

        if(checkinData.isHasCadeiras()){
            params.put("estadio_setor_divisao_id", checkinData.getSetorDiv());
            params.put("estadio_cadeiras", checkinData.getCadeiraID() != null? convertListInString(clearList(checkinData.getCadeiraID())): "[]");
        }else{
            params.put("estadio_cadeiras", checkinData.getCadeiraID() != null? convertListInString(clearList(checkinData.getCadeiraID())): "[]");
        }

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_FAZER_CHECKIN, OP_ID_CHECKINGS, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    void fazerCheckout (){
        CheckinActivity ac = (CheckinActivity) activity;
        ac.setListener(new CheckinActivity.Listener() {
            @Override
            public void OnLoad() {
                fazerCheckout();
            }
        });

        Log.d("Async", checkinData.toString());
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("socio_id", checkinData.getSocioID());
        params.put("jogo_id", checkinData.getJogoID());
        params.put("estadio_setor_id", checkinData.getEstadio_setor_id());
        params.put("opcao", 2); // 1 fazer checkin - 2 fazer checkout
        params.put("valor", checkinData.getValorTotal());
        params.put("ncc", "");
        params.put("cvv", "");
        params.put("val", "");
        params.put("nomes", checkinData.getNomes() != null? convertListInString(clearList(checkinData.getNomes()))+"]" : "[]");

        if(checkinData.isHasCadeiras()){
            params.put("estadio_setor_divisao_id", checkinData.getSetorDiv());
            params.put("estadio_cadeira_id", checkinData.getCadeiraID());
            params.put("estadio_cadeiras", checkinData.getCadeiras() != null? convertListInString(clearList(checkinData.getCadeiras())): "[]");
        }else{
            params.put("estadio_setor_divisao_id", "nil");
            params.put("estadio_cadeira_id", 0);
            params.put("estadio_cadeiras", checkinData.getCadeiras() != null? convertListInString(clearList(checkinData.getCadeiras())): "[]");
        }

        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_FAZER_CHECKIN, OP_ID_CHECKOUT, this).execute(params);
        timer.start(TIMER_DEFAULT);
    }

    String convertListInString (List<String> list){
        String finalString = "[";
        for(int i = 0; i < list.size(); i ++){
            finalString = finalString.concat(list.get(i));
            if(i != list.size()-1){
                finalString = finalString.concat(",");
            }else{
                finalString = finalString.concat("]");
            }
        }
        return finalString;
    }

    List<String> clearList(List<String> list) {
        if ( list == null ) {
            return list;
        }
        while (list.remove("")){}

        return list;
    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        timer.stop();
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {

        switch (opId){
            case OP_ID_VALIDAR_USER:{
                listener.onSuccess(OP_ID_VALIDAR_USER, response);
            }
            break;
            case OP_ID_GET_SOCIO_TORCEDOR_URL:{
                if (response != null){
                    try {
                        JSONObject obj = response.getJSONObject("Object");
                        CONSTANTS.apiServerURL = obj.getString("endpoint");
                        CONSTANTS.apikey = obj.getString("key");
                        getSocioTorcedorURL = true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_ID_CONSULTAR_SOCIO:{
                listener.onSuccess(OP_ID_CONSULTAR_SOCIO, response);
            }
            break;
            case OP_ID_CONSULTAR_PROX_JOGOS:{
                if(response.has("dados")){
                    try {
                        if(!response.getString("dados").isEmpty()){
                            Gson gson = new Gson();
                            CheckinPartidaData[] partidas = gson.fromJson(response.getString("dados"), CheckinPartidaData[].class);

                            if(partidas.length > 0){
                                List<CheckinPartidaData> data = new ArrayList<>(Arrays.asList(partidas));
                                listener.onSuccess(OP_ID_CONSULTAR_PROX_JOGOS, data);
                            }
                        }else{
                            listener.onSuccess(OP_ID_CONSULTAR_PROX_JOGOS, (List<?>) null);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    listener.onSuccess(OP_ID_CONSULTAR_PROX_JOGOS, (List<?>) null);
                }
            }
            break;

            case OP_ID_CONSULTAR_SETORES:{
                if(response.has("dados")){
                    try {
                        Gson gson = new Gson();
                        CheckinSetoresData[] partidas = gson.fromJson(response.getString("dados"), CheckinSetoresData[].class);

                        try {
                            if(partidas.length > 0){
                                List<CheckinSetoresData> data = new ArrayList<>(Arrays.asList(partidas));
                                listener.onSuccess(OP_ID_CONSULTAR_SETORES, data);
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                            activity.finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case OP_ID_CONSULTAR_DIV_SETORES:{
                if(response.has("dados")){
                    try {
                        Gson gson = new Gson();
                        CheckinSetoresData[] partidas = gson.fromJson(response.getString("dados"), CheckinSetoresData[].class);

                        if(partidas.length > 0){
                            List<CheckinSetoresData> data = new ArrayList<>(Arrays.asList(partidas));
                            listener.onSuccess(OP_ID_CONSULTAR_DIV_SETORES, data);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;

            case OP_ID_INGRESSOS:{
                    listener.onSuccess(OP_ID_INGRESSOS, response);
            }
            break;

            case OP_ID_ACOMPANHANTES:{
                int status = 0;

                if(response.has("status")){
                    try {
                        status = response.getInt("status");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200) {
                    if (response.has("dados")) {
                        try {
                            Gson gson = new Gson();
                            CheckinSetoresData[] acompanhantes = gson.fromJson(response.getString("dados"), CheckinSetoresData[].class);

                            if(acompanhantes.length > 0){
                                List<CheckinSetoresData> data = new ArrayList<>(Arrays.asList(acompanhantes));
                                listener.onSuccess(OP_ID_ACOMPANHANTES, data);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_ID_CONSULTAR_CHECKINGS:{
                int status = 0;
                boolean hascheking = false;
                CheckinData checkin;

                if(response.has("status")){
                    try {
                        status = response.getInt("status");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("dados")){
                        try {
                            Gson gson = new Gson();

                            LastCheckingData data[] = gson.fromJson(response.getString("dados"), LastCheckingData[].class);
                            List<LastCheckingData> dados = new ArrayList<>(Arrays.asList(data));

                            CheckingDadosData chedtas = dados.get(dados.size()-1).getDados();
                            List<CheckinData> checkings =  chedtas.getCheckins();
                            checkin = checkings.get(checkings.size()-1);
                            checkin.setPartida(checkinData.getPartida());

                            String socioID = checkin.getSocioID();
                            String state = checkin.getStatus();
                            checkin.setSetorName(chedtas.getSetor());

                            if(checkin.getSocioID().equals(socioID)){
                                if(state.equals("ok")){
                                    hascheking = true;
                                    checkinData = checkin;
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(hascheking){
                    listener.onSuccess(OP_ID_CONSULTAR_CHECKINGS, checkinData);
                }else{
                    listener.onSuccess(OP_ID_CONSULTAR_CHECKINGS, (List<?>) null);
                }

            }
            break;

            case OP_ID_ASSENTOS_DIV:{
                int status = 0;
                if(response.has("status")){
                    try {
                        status = response.getInt("status");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("dados")){
                        try {
                            Gson gson = new Gson();

                            CheckinCadeiraData data[] = gson.fromJson(response.getString("dados"), CheckinCadeiraData[].class);
                            List<CheckinCadeiraData> dados = new ArrayList<>(Arrays.asList(data));
                            listener.onSuccess(OP_ID_ASSENTOS_DIV, dados);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;

            case OP_ID_CHECKINGS: {
                Log.d("Async", "check-in");
                listener.onSuccess(OP_ID_CHECKINGS, (List<?>) null);
            }
            break;

            case OP_ID_CHECKOUT: {
                Log.d("Async", "check-out");
                listener.onSuccess(OP_ID_CHECKOUT, (List<?>) null);
            }
            break;

        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        Log.d("Async" , "Error: " + opId);

        listener.onError(opId, response);
    }
    //endregion

    public interface Listener {
        void onSuccess(int opId, @Nullable List<?> response);
        void onSuccess(int opId, @Nullable Object response);
        void onSuccess(int opId, @Nullable JSONObject response);
        void onError(int opId, @Nullable JSONObject response);
        void onExpired();
    }
}
