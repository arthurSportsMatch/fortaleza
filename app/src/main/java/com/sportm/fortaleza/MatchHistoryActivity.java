package com.sportm.fortaleza;

import androidx.core.graphics.drawable.DrawableCompat;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import data.MatchHistoryData;
import data.MatchHistoryDetailsData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.UTILS;

public class MatchHistoryActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback{

    ViewHolder mHolder;

    private static final int OP_GET_MATCH              = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_history);
        mHolder = new ViewHolder();

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 22);

        getMatch(id);
    }

    void getMatch (int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_MATCH_DETAILS, OP_GET_MATCH,this).execute(params);
    }

    //Inicia os valores de match
    void initMatchHistory (MatchHistoryData data){
        mHolder.partidaType.setText(data.getTitle() != null? data.getTitle(): "");
        mHolder.partidaLocal.setText(data.getSubtit() != null? data.getSubtit(): "");

        //Clube 1
        mHolder.nomeTime1.setText(data.getClube1() != null? data.getClube1(): "");
        mHolder.golsTime1.setText(data.getGols1() != null? data.getGols1(): "");
        UTILS.getImageAt(activity, data.getClubeImg1(), mHolder.imgTime1);

        //Clube 2
        mHolder.nomeTime2.setText(data.getClube2() != null? data.getClube2(): "");
        mHolder.golsTime2.setText(data.getGols2() != null? data.getGols2(): "");

        UTILS.getImageAt(activity, data.getClubeImg2(), mHolder.imgTime2);

        //Lista de gols time 1
        if(data.getListGols() != null){
            if(data.getListGols().getT1() != null){
                List<String> gols = new ArrayList<>(Arrays.asList(data.getListGols().getT1()));
                initClubGols(gols, mHolder.viewGolsTime1, 0);
            }
//            if (hasServer){
//                return server.getPrefix();
//            }else{
//                UserInformation.getServerData().getApi().get(0).setSelected(1);
//                UserInformation.getServerData().setServerId(UserInformation.getServerData().getApi().get(0).getId());
//                salvarServidor(UserInformation.getServerData());
//            }
            if(data.getListGols().getT2() != null){
                List<String> gols = new ArrayList<>(Arrays.asList(data.getListGols().getT2()));
                initClubGols(gols, mHolder.viewGolsTime2, 1);
            }
        }

        //Detalhes da partida
        if(data.getStats() != null){
            mHolder.matchDetailHolder.setVisibility(View.VISIBLE);
            initMatchDetails(data.getStats());
        }
    }

    //Inicia os valores dos gols do clube
    void initClubGols (List<String> list, LinearLayout view, int side){
        for(String s: list){
            int i = (int) getResources().getDimension(R.dimen.margin_5);

            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            p.bottomMargin = i;
            p.topMargin = i;

            TextView textView = new TextView(activity);
            textView.setText(s);
            textView.setLayoutParams(p);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP , 12);

            textView.setCompoundDrawablesWithIntrinsicBounds(
                    side == 0? getResources().getDrawable(R.drawable.ic_ball): null,
                    null,
                    side == 1? getResources().getDrawable(R.drawable.ic_ball): null,
                    null);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setCompoundDrawableTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorBlack)));
            }else{
                Drawable drawable = textView.getCompoundDrawables()[0];
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorBlack));
            }

            textView.setCompoundDrawablePadding(i);
            textView.setTextAlignment(side == 1? View.TEXT_ALIGNMENT_TEXT_END: View.TEXT_ALIGNMENT_TEXT_START);

            textView.setTextColor(getResources().getColor(R.color.colorBlack));
            view.addView(textView);
        }
    }

    //Inicia os detalhes da partida
    void initMatchDetails (List<MatchHistoryDetailsData> list){
        for(MatchHistoryDetailsData mDetail: list){
            View view = LayoutInflater.from(activity).inflate(R.layout.adapter_match_details, mHolder.matchDetails, false);
            TextView club1 = view.findViewById(R.id.txt_club1);
            TextView tit = view.findViewById(R.id.txt_tit);
            TextView club2 = view.findViewById(R.id.txt_club2);


            club1.setText(mDetail.getV1() != null ? mDetail.getV1() : "");
            tit.setText(mDetail.getTipo() != null ? mDetail.getTipo() : "");
            club2.setText(mDetail.getV2() != null ? mDetail.getV2() : "");

            mHolder.matchDetails.addView(view);
        }
    }


    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_MATCH:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            MatchHistoryData data = gson.fromJson(response.getString("Object"), MatchHistoryData.class);
                            if(data != null){
                                initMatchHistory(data);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        //Header
        TextView partidaType;
        TextView partidaLocal;

        //Time 1
        ImageView imgTime1;
        TextView nomeTime1;
        TextView golsTime1;

        //Time 2
        ImageView imgTime2;
        TextView nomeTime2;
        TextView golsTime2;

        //details
        LinearLayout viewGolsTime1;
        LinearLayout viewGolsTime2;

        LinearLayout matchDetails;
        View matchDetailHolder;

        public ViewHolder (){
            partidaType = findViewById(R.id.txt_age_tipo);
            partidaLocal = findViewById(R.id.txt_local_match);

            imgTime1 = findViewById(R.id.img_time_1);
            nomeTime1 = findViewById(R.id.txt_club_1);
            golsTime1 = findViewById(R.id.txt_result_1);

            imgTime2 = findViewById(R.id.img_time_2);
            nomeTime2 = findViewById(R.id.txt_club_2);
            golsTime2 = findViewById(R.id.txt_result_2);

            viewGolsTime1 = findViewById(R.id.view_gols_club_1);
            viewGolsTime2 = findViewById(R.id.view_gols_club_2);

            matchDetails = findViewById(R.id.view_match_details);
            matchDetailHolder = findViewById(R.id.view_match_details_holder);

        }
    }
}
