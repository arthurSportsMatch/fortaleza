package com.sportm.fortaleza;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Objects;

import data.FragmentData;
import fragments.AgendaFragment;
import fragments.FaleConoscoFragment;
import fragments.GamesFragment;
import fragments.HomeFragment;
import fragments.MainFragment;
import fragments.MenuLateralFragment;
import fragments.ReceitaFragment;
import fragments.TimeFragment;
import informations.UserInformation;
import utils.AsyncOperation;
import utils.CONSTANTS;
import adapter.PageAdapter;
import utils.FragmentHolder;
import utils.MasterBakersNotificationManager;
import utils.MoedasController;
import utils.TRACKING;
import utils.UTILS;
import view.MultiSwipeRefreshLayout;

import static utils.CONSTANTS.LOADING_SCREEN_KEY;
import static utils.CONSTANTS.SCREEN_REDIRECT_PUSH;
import static utils.CONSTANTS.SCREEN_SEARCH;

public class MainMenuActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback, LifecycleObserver {

    //Operações
    private static int BTN_ID = -1;
    private static int BTN_VAR = -1;


    public static final String ON_PUSH = "com.devarthur.fortaleza.ON_PUSH";

    private static final int OP_CHECK_VERSION       = 1;
    private static final int OP_SAVE_TRACKING       = 2;


    private static boolean cameFromPush = false;
    private static boolean saveTrackingOfCommonPush = false;

    private static int idPush = -1;
    private static int screenToLoad = -1;
    private static int pushTrackingId = -1;
    private static int pushTrackingVariableId = -1;

    int refrashed = 0;
    int lastRefreshTime = 0;

    boolean hasFromLogin = false;
    public static void setSaveTrackingOfCommonPush(boolean saveTrackingOfCommonPush) {
        MainMenuActivity.saveTrackingOfCommonPush = saveTrackingOfCommonPush;
    }

    //Instancia statica
    public static MainMenuActivity instance;

    //Valores para pageView
    private ArrayList<FragmentHolder> pageList = new ArrayList<>();
    PageAdapter adapter;
    ViewHolder mHolder;

    private static boolean needToUpdateAPNS = false;
    private static String newApns = "";
    private boolean isUpdatingApns = false;


    private final int MAIN_MENU_PUSH_BUNDLE = 762;
    Bundle pushBundle;
    boolean broadcastOnPush = false;
    boolean hasPushToCall = false;
    boolean GetVersionOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        mHolder = new ViewHolder();

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = getIntent();

        if(intent.hasExtra("login"))
            hasFromLogin = true;

        //Primeira vez que abre o app
        if(instance == null && hasFromLogin){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setMessage("Deseja receber as notificações")
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UpdateUserApns(1, MasterBakersNotificationManager.gcm);
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UpdateUserApns(0, MasterBakersNotificationManager.gcm);
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }

        checkComumPushSaveTracking();
        callGetVersion();

        instance = this;

        checkBundle(getIntent());

        int wantNotification = 1;
        if(prefs.containsKey(CONSTANTS.SHARED_PREFS_KEY_NEED_TO_UPDATE_APNS)){
            //ToDO - Ativar depois que tiver uma tela de ativar ou não notificação
            //wantNotification = Integer.parseInt(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_NEED_TO_UPDATE_APNS, "1"));
        }

        UpdateUserApns(wantNotification, MasterBakersNotificationManager.gcm);
        initAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBroadcast();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcast();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Push", "Request: " + requestCode + "resultCode: " + resultCode);
        if(requestCode == MAIN_MENU_PUSH_BUNDLE){
            checkBundle(data);
        }else{
            adapter.getList().get(mHolder.pages.getCurrentItem()).getFragment().onActivityResult(requestCode,resultCode,data);
        }

    }

    @Override
    public void onBackPressed() {
        onReloadFragments();
        if(adapter.getList().get(mHolder.pages.getCurrentItem()).getPosition() > 0){
            adapter.removeFragment(mHolder.pages.getCurrentItem());
        }else{
            initCloseAppMessage();
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void OnReloaded (){
        try {
            MainFragment fragment = (MainFragment)adapter.getCurrentFragment();
            fragment.onReload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initAction (){
        mHolder.refreshLayout.setEnabled(false);
        mHolder.refreshLayout.setSwipeableChildren(android.R.id.list, android.R.id.empty, R.id.scroll_home, R.id.swipe_view, R.id.pager_next_games, R.id.view_game_pager);
        mHolder.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                Calendar cal = Calendar.getInstance();
//                int actualHour = cal.get(Calendar.HOUR_OF_DAY);
//                Log.d("RefreshH", "actual: " + actualHour + "last: " + lastRefreshTime);
//                if((actualHour - lastRefreshTime) > 200){
//
//                    refrashed ++;
//                }

//                if(refrashed < 1){
//                    //lastRefreshTime = cal.get(Calendar.HOUR_OF_DAY);
//                    refrashed++;
//                    OnReloaded();
//                    mHolder.refreshLayout.setRefreshing(false);
//                }
            }
        });
    }

    //Quando o audio tiver sido preparado
    private BroadcastReceiver onPush = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkBundle(intent);
        }
    };

    void registerBroadcast (){
        if(!broadcastOnPush){
            IntentFilter iOnPrepared = new IntentFilter(ON_PUSH);
            registerReceiver(onPush, iOnPrepared);
            broadcastOnPush = true;
        }
    }

    void unregisterBroadcast (){
        if(broadcastOnPush){
            unregisterReceiver(onPush);
        }
    }


    void checkBundle (Intent _intent){
        Bundle b = _intent.getExtras();
        if((b != null) && (b.containsKey(LOADING_SCREEN_KEY)) ){
            screenToLoad = b.getInt(LOADING_SCREEN_KEY);
            Log.d("Push","Foi chamado");

            switch (screenToLoad) {
                //Quando o push leva para uma tela nova
                case SCREEN_REDIRECT_PUSH:
                    Log.d("Push","Foi chamado: " +  SCREEN_REDIRECT_PUSH);
                    pushBundle = b.getBundle("pushBundle");
                    break;

                //Quando o push é de tipo diferente
                case SCREEN_SEARCH:
                    Log.d("Push","Foi chamado: " +  SCREEN_SEARCH);
                    pushBundle = b;
                    break;
            }

            if(GetVersionOK){
                CallPush(pushBundle, false);
            }else{
                hasPushToCall = true;
            }
        }else{
            Log.d("Push","Não foi chamado");
        }
    }

    private void onReloadFragments(){
        try {
            MainFragment fragment = (MainFragment)adapter.getCurrentFragment();
            fragment.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Call get version
    void callGetVersion (){
        new AsyncOperation(this, AsyncOperation.TASK_ID_CHECK_VERSION, OP_CHECK_VERSION, this).execute();
    }

    //Faz update da Apns
    public void UpdateUserApns(final int allow, String gcmToken){
        if(isUpdatingApns){
            return;
        }
        isUpdatingApns = true;

        Hashtable<String, Object> params = new Hashtable<>();
        params.put("apns", gcmToken);
        params.put("allow", allow);
        new AsyncOperation(this, AsyncOperation.TASK_ID_SET_USER_APNS, 0, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                isUpdatingApns = false;
                needToUpdateAPNS = false;
                newApns = "";
                prefs.put(CONSTANTS.SHARED_PREFS_KEY_NEED_TO_UPDATE_APNS, String.valueOf(allow));
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {
                isUpdatingApns = false;
            }
        }).execute(params);
    }

    public void setPontos (int plus){
        MoedasController.showNewMoedas(activity, mHolder.showMoedas, plus);
    }

    //region View Pager Configurations
    //Inicia View Pager
    void initViewPager (){
        pageList.add(new FragmentHolder(new FragmentData(new HomeFragment(),"Home")));
        pageList.add(new FragmentHolder(new FragmentData(new GamesFragment(),"Interações")));
        pageList.add(new FragmentHolder(new FragmentData(new TimeFragment(), "Time")));
        pageList.add(new FragmentHolder(new FragmentData(new MenuLateralFragment(), "Menu")));

        adapter = new PageAdapter(getSupportFragmentManager(), pageList);
        mHolder.pages.setAdapter(adapter);
        mHolder.pages.setOffscreenPageLimit(pageList.size());

        mHolder.pages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mHolder.bottonNavigation.getMenu().getItem(position).setChecked(true);
                //changeToolbarName(adapter.getFragmentName(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //Inicia Botton View
    void initBottonView (){
        mHolder.bottonNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.home:
                        //mHolder.refreshLayout.setEnabled(true);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_HOME_SELECTHOME).execute();
                        if(mHolder.pages.getCurrentItem() == 0 && adapter.getFragmentName(0) == "Home"){
                            if(adapter.getList().get(mHolder.pages.getCurrentItem()).getPosition() > 0 ) {
                                adapter.removeFragment(mHolder.pages.getCurrentItem());
                            }else{
                               HomeFragment home = (HomeFragment) adapter.getItem(mHolder.pages.getCurrentItem());
                               home.GoTop();
                            }
                        }
                        mHolder.pages.setCurrentItem(0);
                        break;
                    case R.id.functions:
                        //mHolder.refreshLayout.setEnabled(true);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_JOGOS_SELECT).execute();
                        if(mHolder.pages.getCurrentItem() == 1 && adapter.getFragmentName(1) == "Interações"){
                            if(adapter.getList().get(mHolder.pages.getCurrentItem()).getPosition() > 0 ) {
                                adapter.removeFragment(mHolder.pages.getCurrentItem());
                            }
                        }
                        mHolder.pages.setCurrentItem(1);
                        break;
                    case R.id.time:
                        //mHolder.refreshLayout.setEnabled(true);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_TIME_SELECIONAR).execute();
                        if(mHolder.pages.getCurrentItem() == 2 && adapter.getFragmentName(2) == "Time"){
                            if(adapter.getList().get(mHolder.pages.getCurrentItem()).getPosition() > 0 ) {
                                adapter.removeFragment(mHolder.pages.getCurrentItem());
                            }
                        }
                        mHolder.pages.setCurrentItem(2);
                        break;
                    case R.id.perfil:
                        //mHolder.refreshLayout.setEnabled(true);
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_MENU_SELECIONAR).execute();
                        if(mHolder.pages.getCurrentItem() == 3 && adapter.getFragmentName(3) == "Menu"){
                            if(adapter.getList().get(mHolder.pages.getCurrentItem()).getPosition() > 0 ) {
                                adapter.removeFragment(mHolder.pages.getCurrentItem());
                            }

                        }
                        mHolder.pages.setCurrentItem(3);
                        break;
                }
                return false;
            }
        });
    }

    //Muda o nome do tolbar
    public void changeToolbarName (String name){
        setTitle(name);
    }
    //endregion

    //region Fragment Controllers
    //Adiciona Fragment a lista de fragmentos que podem ser abertos
    public void ChangeFragment (FragmentData fragment, int actualPosition){
        //Caso possa ser chamado de qualquer tela
        if(actualPosition < 0){
            adapter.addFragment(fragment, mHolder.pages.getCurrentItem());
        }else{
            adapter.addFragment(fragment, actualPosition);
        }
    }

    //Adiciona Fragment a lista de fragmentos que podem ser abertos
    public void ChangeFragment (FragmentData fragment, int actualPosition, String name){
        //Caso possa ser chamado de qualquer tela
        if(actualPosition < 0){
            adapter.addFragment(fragment, mHolder.pages.getCurrentItem());
        }else{
            adapter.addFragment(fragment, actualPosition);
        }
    }

    public void SetFragment (FragmentData fragment, int position){
        adapter.setFragment(fragment, position);
    }
    //endregion

    //region Push Notifcations
    public static void setAPNSToUpdate(String _newApns, boolean needToUpdate){
        needToUpdateAPNS = needToUpdate;
        newApns = _newApns;
    }

    //Save Tracking dos pushs
    void checkComumPushSaveTracking (){
        if(saveTrackingOfCommonPush){
            setSaveTrackingOfCommonPush(false);
            new AsyncOperation(this, AsyncOperation.TASK_ID_SAVE_TRACKING, OP_SAVE_TRACKING, new AsyncOperation.IAsyncOpCallback() {
                @Override
                public void CallHandler(int opId, JSONObject response, boolean success) {

                }

                @Override
                public void OnAsyncOperationSuccess(int opId, JSONObject response) {

                }

                @Override
                public void OnAsyncOperationError(int opId, JSONObject response) {

                }
            }, TRACKING.TRACKING_PUSH_COMUM).execute();
        }
    }

    //Chama o push expecifico
    void CallPush(Bundle pushBundle, boolean hasfromBroadcast){

        if(getIntent() != null && getIntent().getExtras() != null){
            getIntent().getExtras().clear();
        }
        //this check was already made, but... Just to be sure! :P
        if((pushBundle != null) && (pushBundle.containsKey("idPush") ) ){

            int type = pushBundle.getInt("t", -1);

            Hashtable<String, Object> params = new Hashtable<>();
            params.put("idPush", pushBundle.getInt("idPush"));
            new AsyncOperation(this, AsyncOperation.TASK_ID_SET_NOTIFICATION, OP_SAVE_TRACKING, emptyAsyncOpCallback).execute(params);

            switch (type){

                case MasterBakersNotificationManager.PUSH_ID_DEFAUT: {
                   //
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_NOTICIA: {
                    Intent intent = new Intent(activity, NoticiaActivity.class);
                    intent.putExtra("id", pushBundle.getInt("id"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_QUIZ: {
                    mHolder.pages.setCurrentItem(1);
                    if(hasfromBroadcast){
                        Intent broadcastIntent = new Intent(GamesFragment.ON_PUSH_GAMES);
                        broadcastIntent.putExtra("tipo", "quiz");
                        broadcastIntent.putExtra("id", pushBundle.getInt("idQuiz"));
                        sendBroadcast(broadcastIntent);
                    }else{
                        SetFragment(new FragmentData(new GamesFragment("quiz", pushBundle.getInt("idQuiz")), "Interações"), 1);
                    }

                }
                break;


                case MasterBakersNotificationManager.PUSH_ID_PESQUISA: {
                    mHolder.pages.setCurrentItem(1);
                    if(hasfromBroadcast){
                        Intent broadcastIntent = new Intent(GamesFragment.ON_PUSH_GAMES);
                        broadcastIntent.putExtra("tipo", "pesquisa");
                        sendBroadcast(broadcastIntent);
                    }else{
                        SetFragment(new FragmentData(new GamesFragment("pesquisa"), "Interações"), 1);
                    }
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PESQUISA_ESPECIFICA: {
                    mHolder.pages.setCurrentItem(1);
                    if(hasfromBroadcast){
                        Intent broadcastIntent = new Intent(GamesFragment.ON_PUSH_GAMES);
                        broadcastIntent.putExtra("tipo", "pesquisa");
                        broadcastIntent.putExtra("id", pushBundle.getInt("idPesquisa"));
                        sendBroadcast(broadcastIntent);
                    }else{
                        SetFragment(new FragmentData(new GamesFragment("pesquisa", pushBundle.getInt("idPesquisa")), "Interações"), 1);
                    }
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PALPITES: {
                    mHolder.pages.setCurrentItem(1);
                    if(hasfromBroadcast){
                        Intent broadcastIntent = new Intent(GamesFragment.ON_PUSH_GAMES);
                        broadcastIntent.putExtra("tipo", "palpites");
                        sendBroadcast(broadcastIntent);
                    }else{
                        SetFragment(new FragmentData(new GamesFragment("palpite"), "Interações"), 1);
                    }
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_PLAYERPROFILE: {
                    Intent intent = new Intent(this, PerfilAtletaActivity.class);
                    intent.putExtra("id", pushBundle.getInt("idPlayer"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_MEMBERSHIP: {
                    Intent intent = new Intent(this, SocioTorcedorActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_AUDIO: {
                    Intent intent = new Intent(activity, PlayerActivity.class);
                    intent.putExtra("push", true);
                    intent.putExtra("id", pushBundle.getInt("id"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_AGENDA: {
                    mHolder.pages.setCurrentItem(2);
                    ChangeFragment(new FragmentData(new AgendaFragment(), "Time"), 2);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_CAMPINHO: {
                    Intent intent = new Intent(this, EscalarTimeActivity.class);
                    intent.putExtra("esquema", pushBundle.getInt("escalacao"));
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_LOJA: {
                    Intent intent = new Intent(this, LojaActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_ALTERAR_DADOS: {
                    Intent intent = new Intent(this, ConfigurationActivity.class);
                    startActivity(intent);
                }
                break;

                case MasterBakersNotificationManager.PUSH_ID_RANK: {
                    Intent intent = new Intent(this, RankingActivity.class);
                    startActivity(intent);
                }
                break;

                default: finish();
            }
            hasPushToCall = false;
        }else{
            hasPushToCall = false;
        }
    }

    public void setPesquisaPage (){
        mHolder.pages.setCurrentItem(1);
        SetFragment(new FragmentData(new GamesFragment("pesquisa"), "Interações"), 1);
    }

    //Chama o fragmento de receitas
    void CallFromPushRecipes(int idReceita){
        MainMenuActivity.instance.ChangeFragment(new FragmentData(new ReceitaFragment(idReceita), "Receitas"), -1);
    }

    //Chama o fragmento de fale conosco
    void CallFromPushContactUs(int idPush){
        MainMenuActivity.instance.ChangeFragment(new FragmentData(new FaleConoscoFragment(), "Fale Conosco"), -1);
    }

    //endregion

    //region Async Operations
    AsyncOperation.IAsyncOpCallback emptyAsyncOpCallback = new AsyncOperation.IAsyncOpCallback() {
        @Override
        public void CallHandler(int opId, JSONObject response, boolean success) {

        }

        @Override
        public void OnAsyncOperationSuccess(int opId, JSONObject response) {

        }

        @Override
        public void OnAsyncOperationError(int opId, JSONObject response) {

        }
    };

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_CHECK_VERSION:{
                boolean success = false;
                String msg = "";
                int status = 0;
                try{
                    if( (response.has("Message")) && (response.get("Message") != JSONObject.NULL) ){
                        msg = response.getString("Message");
                    }
                    if( (response.has("Status")) && (response.get("Status") != JSONObject.NULL) ){
                        success = (response.getInt("Status") == HttpURLConnection.HTTP_OK);
                    }
                    if( (response.has("sId")) && (response.getInt("sId") > 0)){
                        UserInformation.setSessionId((int) response.get("sId"));
                    }

                    if( (response.has("idServidor")) && (response.get("idServidor") != JSONObject.NULL) ) {
                        int idServidor = response.getInt("idServidor");

                        if(UserInformation.getServerData().getServerId() != idServidor){
                            UserInformation.getServerData().setServerId(idServidor);
                            UTILS.salvarServidor(UserInformation.getServerData());
                            CONSTANTS.serverURL = CONSTANTS.serverAddress.replaceAll(CONSTANTS.serverAddress, UTILS.getApiServer(idServidor) + "api/");
                            CONSTANTS.serverURL = CONSTANTS.serverURL.concat(CONSTANTS.serverVersion);
                        }
                    }
//                    if( (response.has("rv")) && (response.get("rv") != JSONObject.NULL) ){
//                        double latestRegVer = response.getDouble("rv");
//                        double myRegVersion = Double.parseDouble(prefs.getString(CONSTANTS.SHARED_PREFS_KEY_REG_VERSION, "0.0"));
//
//                        UTILS.DebugLog(CONSTANTS.ASYNCDEBUG, "My RV: " + myRegVersion + ", Latest: " + latestRegVer);
//
//                        if(myRegVersion < latestRegVer){
//                            //callRegulamentoAcceptance(latestRegVer);
//                        }
//
//                    }
                } catch (JSONException e) {
                    UTILS.DebugLog(CONSTANTS.ASYNCDEBUG, e);
                }

                if(success){
                    GetVersionOK = true;
                    if(hasPushToCall){
                        CallPush(pushBundle, false);
                    }

                    TRACKING.buildHeader(BTN_ID, BTN_VAR);
                    initViewPager();
                    initBottonView();
                }else{
                    callGetVersion();
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        public ViewPager pages;
        public BottomNavigationView bottonNavigation;
        public Toolbar toolbar;
        public MultiSwipeRefreshLayout refreshLayout;
        public LinearLayout showMoedas;

        public ViewHolder (){
            pages = findViewById(R.id.framelayout);
            bottonNavigation = findViewById(R.id.navgation);
            toolbar = findViewById(R.id.topbar);
            refreshLayout = findViewById(R.id.refresh);
            showMoedas = findViewById(R.id.show_moedas);
        }
    }
}
