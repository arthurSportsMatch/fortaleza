package com.sportm.fortaleza;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import data.UserData;
import informations.UserInformation;
import models.ErrorModel;
import models.MessageModel;
import models.user.UserModel;
import models.utils.RegulamentoModel;
import okhttp3.internal.Util;
import repository.user.UserRepository;
import repository.utils.RegulamentoRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;
import vmodels.MainViewModel;
import vmodels.user.UserViewModel;
import vmodels.utils.RegulamentoViewModel;

import static utils.LoadEmbed.loadHtmlEmbed;

public class LoginActivity extends MainActivity{

    //Essentials
    private ViewHolder mHolder;
    private UserViewModel userViewModel;
    private RegulamentoViewModel regulamentoViewModel;

    Animation animSlideLeft, animSlideRight;
    public static String BUNDLE_KEY_TOKEN_MESSAGE = "";

    //Screen enumerator
    private enum Screen{
        whait,
        loginForm,
        createForm,
        forgotForm
    }
    Screen screenStates = Screen.createForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mHolder = new ViewHolder();

        initValues();
        initActions();
        initTerms();
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = ViewModelProviders.of(LoginActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);

        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                UTILS.setUserLoggedIn(true);
                callMainMenu();
            }
        });

        userViewModel.getMessage().observe(this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel message) {
                showMessage(message.getMsg());
            }
        });

        userViewModel.getError().observe(this, new Observer<ErrorModel>() {
            @Override
            public void onChanged(ErrorModel error) {
                showMessage(error.getMsg());
            }
        });

        RegulamentoRepository regulamentoRepository = new RegulamentoRepository(activity, prefs);
        regulamentoViewModel = ViewModelProviders.of(LoginActivity.this).get(RegulamentoViewModel.class);
        regulamentoViewModel.setmRepository(regulamentoRepository);

        regulamentoViewModel.getRegulamento().observe(this, new Observer<RegulamentoModel>() {
            @Override
            public void onChanged(RegulamentoModel regulamento) {
                showRegulamento(regulamento.getVersion(), regulamento.getDocumentacao());
            }
        });
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        userViewModel.getUserData().removeObservers(this);
        regulamentoViewModel.getRegulamento().removeObservers(this);
    }

    //Inicia valores
    void initValues (){
        animSlideLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        animSlideRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_righ);
    }

    //Inicia ações
    void initActions (){
        mHolder.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStateLogin();
            }
        });

        mHolder.txt_acont_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStateCreation();
            }
        });

        mHolder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenStates = Screen.whait;
                mHolder.layoutLogin.setVisibility(View.GONE);
                mHolder.chamada.setVisibility(View.GONE);
                mHolder.logo.setVisibility(View.VISIBLE);
                mHolder.txt_acont_action.setVisibility(View.VISIBLE);
                return;
            }
        });

        mHolder.txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(screenStates == Screen.loginForm){
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_LOGIN_FORGOT).execute();
                    activeForgotLayout();
                    mHolder.txt_acont_action.setText("Voltar");
                    screenStates = Screen.forgotForm;
                    return;
                }
            }
        });
    }

    //Ao clicar no botão de ação abaixo
    void changeStateLogin (){
        //primeira tela
        if(screenStates == Screen.whait){
            activeCreateAccontLayout();
            mHolder.enter.setText("Continuar");
            mHolder.chamada.setVisibility(View.VISIBLE);
            screenStates = Screen.createForm;
            return;
        }

        //Tela de login
        if(screenStates == Screen.loginForm){
            //Final da ação
            String email = mHolder.email.getText().toString();
            String password = mHolder.password.getText().toString();
            userViewModel.singIn(email, password, null);
            return;
        }
        //Tela de criar
        if(screenStates == Screen.createForm){
            String name = mHolder.name.getText().toString();
            String email = mHolder.email.getText().toString();
            String password = mHolder.password.getText().toString();
            String password2 = mHolder.confirmPassword.getText().toString();
            String userID = String.valueOf(UserInformation.getUserId());
            userViewModel.singUp(name, email, password, password2, userID);
            return;
        }
        //Tela de esqueceu a senha
        if(screenStates == Screen.forgotForm){
            //Final da ação
            String email = mHolder.email.getText().toString();
            userViewModel.forgot(email);
            return;
        }
    }

    //Ao clicar no botao abaixo
    void changeStateCreation (){
        //Tela inicial
        if(screenStates == Screen.whait || screenStates == Screen.createForm){
            activeLoginLayout();
            mHolder.chamada.setVisibility(View.VISIBLE);
            mHolder.txt_acont_action.setText("Não Possuo Conta");
            screenStates = Screen.loginForm;
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_CREATEACCOUNT_OPEN_LOGIN).execute();
            return;
        }

        //Tela de Login
        if(screenStates == Screen.loginForm){
            activeCreateAccontLayout();
            mHolder.txt_acont_action.setText("Já Possuo Conta");
            screenStates = Screen.createForm;
            new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_LOGIN_CREATEACCONT).execute();
            return;
        }

        //Tela de Login
        if(screenStates == Screen.forgotForm){
            activeLoginLayout();
            mHolder.chamada.setVisibility(View.VISIBLE);
            mHolder.txt_acont_action.setText("Não Possuo Conta");
            screenStates = Screen.loginForm;
            return;
        }
    }

    //Ativa tela de login
    void activeLoginLayout (){
        mHolder.logo.setVisibility(View.GONE);
        mHolder.layoutLogin.setVisibility(View.VISIBLE);
        mHolder.regulamentos.setVisibility(View.GONE);

        mHolder.enter.setText("Entrar");
        mHolder.txt_login_message.setText(R.string.para_entrar_no_app_insira_os_seus_dados_de_acesso);

        mHolder.name.setVisibility(View.GONE);
        mHolder.confirmPassword.setVisibility(View.GONE);
        mHolder.email.setVisibility(View.VISIBLE);
        mHolder.password.setVisibility(View.VISIBLE);

        mHolder.txt_forgot.setVisibility(View.VISIBLE);
    }

    //Ativa tela de criar a conta
    void activeCreateAccontLayout (){
        mHolder.logo.setVisibility(View.GONE);
        mHolder.layoutLogin.setVisibility(View.VISIBLE);
        mHolder.regulamentos.setVisibility(View.VISIBLE);

        mHolder.txt_login_message.setText(R.string.message_create_accont);

        mHolder.enter.setText("Comece Agora");
        mHolder.txt_acont_action.setText("Já Possuo Conta");

        mHolder.name.setVisibility(View.VISIBLE);
        mHolder.password.setVisibility(View.VISIBLE);
        mHolder.email.setVisibility(View.VISIBLE);
        mHolder.confirmPassword.setVisibility(View.VISIBLE);

        mHolder.txt_forgot.setVisibility(View.GONE);
        mHolder.txt_acont_action.setVisibility(View.VISIBLE);
    }

    //Ativa tela de Forgot
    void activeForgotLayout (){
        mHolder.logo.setVisibility(View.GONE);
        mHolder.layoutLogin.setVisibility(View.VISIBLE);
        mHolder.regulamentos.setVisibility(View.GONE);

        mHolder.txt_login_message.setText(R.string.forgot_password);

        mHolder.enter.setText("Enviar");
        mHolder.txt_acont_action.setText("Voltar");
        mHolder.name.setVisibility(View.GONE);
        mHolder.email.setVisibility(View.VISIBLE);
        mHolder.password.setVisibility(View.GONE);
        mHolder.confirmPassword.setVisibility(View.GONE);

        mHolder.txt_forgot.setVisibility(View.GONE);
        mHolder.txt_acont_action.setVisibility(View.VISIBLE);
    }

    //Inicia link dos termos
    void initTerms (){
        String text = mHolder.regulamentos.getText().toString();

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                getRegulamento(2);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_CREATEACCOUNT_TERMS).execute();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorRedSPFC7));
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                getRegulamento(3);
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync,TRACKING.TRACKING_CREATEACCOUNT_TERMS_POLITCS).execute();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorRedSPFC7));
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableSpan1, 42, 55, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 57, 81, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mHolder.regulamentos.setText(ss);
        mHolder.regulamentos.setMovementMethod(LinkMovementMethod.getInstance());
    }

    //Pega regulamento
    void getRegulamento (int id){
        regulamentoViewModel.getRegulamento(id);
    }

    //Chama o main menu
    void callMainMenu(){
        finish();
        Intent intent = new Intent(activity, StartMenuActivity.class);
        intent.putExtra("login", 1);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class ViewHolder {
        ImageView logo;
        View layoutLogin;
        EditText name;
        EditText email;
        EditText password;
        EditText confirmPassword;
        Button enter;
        TextView txt_forgot;

        TextView txt_acont_action;
        TextView txt_login_message;


        TextView regulamentos;
        View chamada;
        View back;

        public ViewHolder (){
            logo = findViewById(R.id.splash_logo);
            layoutLogin = findViewById(R.id.layout_login);
            txt_login_message = findViewById(R.id.txt_message_login);

            name = findViewById(R.id.login_nome);
            email = findViewById(R.id.login_email);
            password = findViewById(R.id.login_password);
            confirmPassword = findViewById(R.id.login_password_confirm);
            enter = findViewById(R.id.login_enter);

            txt_forgot = findViewById(R.id.txt_forgot);
            txt_acont_action = findViewById(R.id.create_account);
            regulamentos = findViewById(R.id.txt_regulamentos);

            chamada = findViewById(R.id.view_chamada_login);
            back = findViewById(R.id.txt_voltar_login);

        }
    }
}
