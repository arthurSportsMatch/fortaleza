package com.sportm.fortaleza;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.AdapterLojaItens;
import data.PositonData;
import data.ProdutoData;
import informations.UserInformation;
import models.user.UserModel;
import repository.user.UserRepository;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;
import vmodels.user.UserViewModel;

public class LojaActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    private ViewHolder mHolder;
    private UserViewModel userViewModel;

    private static final int OP_GET_PRODUTOS              = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loja);
        mHolder = new ViewHolder();

        getProdutos(null);
        initAction();
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = ViewModelProviders.of(LojaActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                uploadUser(user);
            }
        });
        userViewModel.getUser(false);
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        userViewModel.getUserData().removeObservers(this);
    }

    void initAction (){
        mHolder.editSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    String search = mHolder.editSearch.getText().toString();
                    search = search.replace(" ", "");

                    if(search.length() > 0){
                        getProdutos(search);
                    }else{
                        getProdutos(null);
                    }
                }
                return (keyCode == KeyEvent.KEYCODE_ENTER);
            }
        });

        mHolder.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = mHolder.editSearch.getText().toString();
                search = search.replace(" ", "");

                if(search.length() > 0){
                    getProdutos(search);
                }else{
                    getProdutos(null);
                }
            }
        });
    }

    void uploadUser (UserModel user){
        int coin = user.getMoedas();
        mHolder.moedas.setText(String.valueOf(coin));

        int moedas = Integer.parseInt(mHolder.moedas.getText().toString());

        if ( moedas <= 1 ) {
            mHolder.moedasDesc.setText(getResources().getString(R.string.moeda));
        }else {
            mHolder.moedasDesc.setText(getResources().getString(R.string.moedas));
        }
    }

    void getProdutos (@Nullable String search){
        Hashtable<String, Object> params = new Hashtable<>();
        if(search != null)
            params.put("search", search);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PRODUTOS, OP_GET_PRODUTOS, this).execute(params);
    }

    void separeteGrups (List<ProdutoData> produtos, List<PositonData> grupos){
        mHolder.produtos.removeAllViews();
        for(PositonData group: grupos){
            initGroup(group, produtos);
        }
    }

    void initGroup (PositonData group, List<ProdutoData> produtos){
        if(group.getId() > 0){
            List<ProdutoData> list = new ArrayList<>();

            for(ProdutoData data: produtos){
                if(data.getIdGrupo() == group.getId()){
                    list.add(data);
                }
            }

            if(list.size() > 0){
                View view = LayoutInflater.from(this).inflate(R.layout.include_loja_recycler, null, false);
                TextView text = view.findViewById(R.id.txt_tittle);
                RecyclerView recyclerView = view.findViewById(R.id.recycle_itens);

                text.setText(group.getTitulo());



                AdapterLojaItens adapter = new AdapterLojaItens(this, list, new AdapterLojaItens.Listener() {
                    @Override
                    public void OnClick(ProdutoData produtoData) {
                        new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_LOJA_SELECIONAR, produtoData.getId()).execute();
                        openLojaItem(produtoData);
                    }
                });
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

                mHolder.produtos.addView(view);
            }
        }else{
            List<ProdutoData> list = new ArrayList<>();

            for(ProdutoData data: produtos){
                if(data.getIdGrupo() == 0){
                    list.add(data);
                }
            }
            initMinhasCompras(list);
        }
    }

    void openLojaItem (ProdutoData data){
        Intent intent = new Intent(activity, ItemLojaActivity.class);
        intent.putExtra("id", data.getId());
        intent.putExtra("idCupom", data.getIdCupom());
        intent.putExtra("valor", data.getValor());
        intent.putExtra("valorST", data.getValorST());
        startActivity(intent);
    }

    void initMinhasCompras (List<ProdutoData> list){
        mHolder.minhasComprasView.setVisibility(View.VISIBLE);

        AdapterLojaItens adapter = new AdapterLojaItens(this, list, new AdapterLojaItens.Listener() {
            @Override
            public void OnClick(ProdutoData produtoData) {
                openLojaItem(produtoData);
            }
        });
        mHolder.minhasCompras.setAdapter(adapter);
        mHolder.minhasCompras.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PRODUTOS:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();

                            JSONObject obj = response.getJSONObject("Object");
                            ProdutoData[] data = gson.fromJson(obj.getString("itens"), ProdutoData[].class);
                            PositonData[] pos = gson.fromJson(obj.getString("grupos"), PositonData[].class);

                            List<ProdutoData> produtos = new ArrayList<>(Arrays.asList(data));
                            List<PositonData> grupos = new ArrayList<>(Arrays.asList(pos));

                            if(data != null){
                                separeteGrups(produtos,grupos );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        View minhasComprasView;
        RecyclerView minhasCompras;
        LinearLayout produtos;
        TextView moedas;
        TextView moedasDesc;

        EditText editSearch;
        ImageView imgSearch;

        public ViewHolder (){
            minhasComprasView = findViewById(R.id.view_minhas_compras);
            minhasCompras = findViewById(R.id.recycle_minhas_compras);
            moedas = findViewById(R.id.txt_moedas);
            moedasDesc = findViewById(R.id.txt_moedas_des);
            produtos = findViewById(R.id.view_loja);
            imgSearch = findViewById(R.id.img_search);
            editSearch = findViewById(R.id.edt_search);
        }
    }
}
