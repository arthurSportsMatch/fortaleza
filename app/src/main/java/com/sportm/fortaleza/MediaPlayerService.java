package com.sportm.fortaleza;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import data.Audio;

public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnInfoListener,
        MediaPlayer.OnBufferingUpdateListener,
        AudioManager.OnAudioFocusChangeListener {


    public static final String ON_PREPARED = "com.devarthur.spfcapp.ON_PREPARED";
    public static final String HAS_PLAYING = "com.devarthur.spfcapp.HAS_PLAYING";
    public static final String GET_PLAYER = "com.devarthur.spfcapp.GET_PLAYER";
    public static final String ON_SEEK_TO = "com.devarthur.spfcapp.ON_SEEKTO";
    public static final String ON_STATE_CHANGE = "com.devarthur.spfcapp.ON_STATE_CHANGE";
    public static final String CHANGE_CONFIGURATION = "com.devarthur.spfcapp.CHANGE_CONFIGURATION";

    public static final String ACTION_START = "com.devarthur.spfcapp.ACTION_START";
    public static final String ACTION_PLAY = "com.devarthur.spfcapp.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.devarthur.spfcapp.ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "com.devarthur.spfcapp.ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "com.devarthur.spfcapp.ACTION_NEXT";
    public static final String ACTION_STOP = "com.devarthur.spfcapp.ACTION_STOP";
    public static final String SEEK_TO = "com.devarthur.spfcapp.ACTION_SEEKTO";

    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;

    // Binder given to clients
    private final IBinder iBinder = new LocalBinder();

    //Handle incoming phone calls
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;

    //MediaSession
    private AudioManager audioManager;
    private MediaController.TransportControls transportControls;
    private MediaSessionManager mediaSessionManager;
    private MediaSession mediaSession;

    //Media PLayer
    private MediaPlayer mediaPlayer;
    private String mediaFile;
    private int resumePosition;

    //List of available Audio files
    private List<Audio> audioList;
    private Audio activeAudio; //Objeto do audio atual
    private int audioIndex = -1;

    boolean shufle = false;
    boolean repeat = false;
    boolean superRepeat = false;
    boolean audio = true;

    public enum PlaybackStatus {
        PLAYING,
        PAUSED
    }
    NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        // Perform one-time setup procedures

        // Manage incoming phone calls during playback.
        // Pause MediaPlayer on incoming call,
        // Resume on hangup.
        callStateListener();
        registerBroadCasts();
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Pegar audio atual
        try {
            //Load data from SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            audioList = storage.loadAudio();
            audioIndex = storage.loadAudioIndex();

            //Verificador do index (para caso inicie apertando next/previews)
            if(audioIndex < 0) audioIndex = audioList.size()-1;
            if(audioIndex > audioList.size() -1) audioIndex = 0;

            //index is in a valid range
            if (audioIndex != -1 && audioIndex < audioList.size()) {
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }
        } catch (NullPointerException e) {
            stopSelf();
        }

        //Request audio focus
        if (!requestAudioFocus()) {
            stopSelf();
        }

        if (mediaSessionManager == null) {
            try {
                initMediaSession();
                initMediaPlayer();
                buildNotification(PlaybackStatus.PLAYING);
            } catch (RemoteException e) {
                e.printStackTrace();
                stopSelf();
            }
        }

        //Handle Intent action from MediaSession.TransportControls
        handleIncomingActions(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        if(audioManager != null) {
            removeAudioFocus();
        }

        //Disabilita o listener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        //Remove as notificações
        removeNotification();

        //Tira os registros
        unRegisterBroadCasts();

        //clear cached playlist
        new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
    }

    //region MediaPlayer
    private void initMediaPlayer() {
//        if(mediaPlayer != null)
//            return;

        mediaPlayer = new MediaPlayer();
        //Set up MediaPlayer event listeners
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);
        mediaPlayer.setOnInfoListener(this);

        //Reset so that the MediaPlayer is not pointing to another data source
        mediaPlayer.reset();

        mediaPlayer.setAudioAttributes(new AudioAttributes
                .Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build());
        try {
            // Set the data source to the mediaFile location
            mediaPlayer.setDataSource(activeAudio.getData());
        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }
        mediaPlayer.prepareAsync();
    }

    private void playMedia() {
        if(mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                onStateChange(true);
            }else{
                onStateChange(true);
            }
        }
    }

    private void stopMedia() {
        if(mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                onStateChange(mediaPlayer.isPlaying());
            }
        }
    }

    private void pauseMedia() {
        if(mediaPlayer!= null){
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                resumePosition = mediaPlayer.getCurrentPosition();
                onStateChange(mediaPlayer.isPlaying());
            }
        }
    }

    private void resumeMedia() {
        if(mediaPlayer!= null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.seekTo(resumePosition);
                mediaPlayer.start();
                onStateChange(mediaPlayer.isPlaying());
            }
        }
    }

    private void seekTo (int position ){
        if(mediaPlayer!= null) {
            mediaPlayer.seekTo(position);
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                onStateChange(mediaPlayer.isPlaying());
            }
        }
    }

    public int getDuration (){
        return mediaPlayer.getDuration();
    }

    public void play (){
        if(mediaPlayer!= null) {
            if (!mediaPlayer.isPlaying()) {
                resumeMedia();
                buildNotification(PlaybackStatus.PLAYING);
            }
        }
    }

    public void pause (){
        if(mediaPlayer!= null) {
            if (mediaPlayer.isPlaying()) {
                pauseMedia();
                buildNotification(PlaybackStatus.PAUSED);
            }
        }
    }

    public void next (){
        skipToNext();
        updateMetaData();
        buildNotification(PlaybackStatus.PLAYING);
    }

    public void previews (){
        skipToPrevious();
        updateMetaData();
        buildNotification(PlaybackStatus.PLAYING);
    }

    public void onStateChange (boolean state){
        Intent broadcastIntent = new Intent(ON_STATE_CHANGE);
        broadcastIntent.putExtra("state", state);
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {


    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if(repeat){
            if(superRepeat){
                play();
                return;
            }

            next();
        }else{
            Intent broadcastIntent = new Intent(ACTION_STOP);
            sendBroadcast(broadcastIntent);
            //Invoked when playback of a media source has completed.
            stopMedia();
            //stop the service
            stopSelf();
            removeNotification();
        }
    }

    //Handle errors
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        //Invoked when there has been an error during an asynchronous operation.
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                break;
        }
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        //Invoked to communicate some info.
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //Invoked when the media source is ready for playback.
        Intent broadcastIntent = new Intent(ON_PREPARED);
        broadcastIntent.putExtra("duracao", mp.getDuration());
        broadcastIntent.putExtra("position", mp.getCurrentPosition());
        broadcastIntent.putExtra("audioID", audioIndex);
        broadcastIntent.putExtra("id", activeAudio.getId());
        sendBroadcast(broadcastIntent);
        playMedia();
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        //Invoked indicating the completion of a seek operation.
        Intent broadcastIntent = new Intent(ON_SEEK_TO);
        broadcastIntent.putExtra("position", mp.getCurrentPosition());
        sendBroadcast(broadcastIntent);
    }

    @Override//Todo mudar volume com codigo abaixo
    public void onAudioFocusChange(int focusState) {
        //Invoked when the audio focus of the system is updated.
        switch (focusState) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mediaPlayer == null) initMediaPlayer();
                else if (!mediaPlayer.isPlaying()) mediaPlayer.start();
                mediaPlayer.setVolume(1.0f, 1.0f);
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mediaPlayer.isPlaying()) mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mediaPlayer.isPlaying()) mediaPlayer.pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            result = audioManager.requestAudioFocus(new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN).build());
        }else{
            result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        //Could not gain focus
        return false;
    }

    private boolean removeAudioFocus() {

        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager.abandonAudioFocus(this);
    }
    //endregion

    //region Media Session
    private void initMediaSession() throws RemoteException {
        //Se já existir um media Sessions
        if (mediaSessionManager != null)
            return;

        mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
        // Create a new MediaSession
        mediaSession = new MediaSession(getApplicationContext(), "AudioPlayer");
        //Get MediaSessions transport controls
        transportControls = mediaSession.getController().getTransportControls();
        //set MediaSession -> ready to receive media commands
        mediaSession.setActive(true);
        //indicate that the MediaSession handles transport control commands
        // through its MediaSessionCompat.Callback.
        mediaSession.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);

        //Set mediaSession's MetaData
        updateMetaData();

        // Attach Callback to receive MediaSession updates
        mediaSession.setCallback(new MediaSession.Callback() {
            // Implement callbacks
            @Override
            public void onPlay() {
                super.onPlay();
                resumeMedia();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onPause() {
                super.onPause();
                pauseMedia();
                buildNotification(PlaybackStatus.PAUSED);
            }

            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
                skipToNext();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
                skipToPrevious();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onStop() {
                super.onStop();
                removeNotification();
                //Stop the service
                stopSelf();
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
                seekTo((int)position);
            }
        });
    }

    private void skipToNext() {
        if(shufle){
            audioIndex = changeShufle();
            activeAudio = audioList.get(audioIndex);
        }else{
            if (audioIndex == audioList.size() - 1) {
                //if last in playlist
                audioIndex = 0;
                activeAudio = audioList.get(audioIndex);
            } else {
                //get next in playlist
                activeAudio = audioList.get(++audioIndex);
            }
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        //reset mediaPlayer
        stopMedia();
        mediaPlayer.reset();
        initMediaPlayer();
    }

    private void playAudio (int index){
        if(index != -1 && index < audioList.size()){
            audioIndex = index;
            activeAudio = audioList.get(audioIndex);
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    private void skipToPrevious() {
        if(shufle){
            audioIndex = changeShufle();
            activeAudio = audioList.get(audioIndex);
        }else{
            if (audioIndex == 0) {
                //if first in playlist
                //set index to the last of audioList
                audioIndex = audioList.size() - 1;
                activeAudio = audioList.get(audioIndex);
            } else {
                //get previous in playlist
                activeAudio = audioList.get(--audioIndex);
            }
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    int changeShufle (){
        Random rand = new Random();
        int r = audioIndex;
        while(r == audioIndex ){
            r = rand.nextInt(audioList.size());
        }
        return  r;
    }

    private void updateMetaData() {
        Bitmap albumArt = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_correct); //replace with medias albumArt
        // Update the current metadata
        mediaSession.setMetadata(new MediaMetadata.Builder()
                .putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, albumArt)
                .putString(MediaMetadata.METADATA_KEY_ARTIST, activeAudio.getArtist())
                .putString(MediaMetadata.METADATA_KEY_ALBUM, activeAudio.getAlbum())
                .putString(MediaMetadata.METADATA_KEY_TITLE, activeAudio.getTitle())
                .build());
    }
    //endregion

    //region MediaPlayer

    //endregion

    //region BroadCast (Receivers)

    //Becoming noisy
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia();
            buildNotification(PlaybackStatus.PAUSED);
        }
    };

    //Pegar configuração
    private BroadcastReceiver getConfiguration = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            repeat = intent.getBooleanExtra("repeat", false);
            superRepeat = intent.getBooleanExtra("superRepeat", false);
            shufle = intent.getBooleanExtra("shufle", false);
            audio = intent.getBooleanExtra("audio", true);

            if(audio){
                mediaPlayer.setVolume(1.0f, 1.0f);
            }else{
                mediaPlayer.setVolume(0f, 0f);
            }
        }
    };
    //Dar play em audio diferente
    private BroadcastReceiver startAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra("audioID", audioIndex);
            if(id >= 0 && id <= audioList.size()-1){
                playAudio(id);
            }
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver seekTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra("position", 0);
            seekTo(position);
        }
    };

    //Quando o player activity quiser saber se esta tocando
    private BroadcastReceiver hasPlaying = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(mediaPlayer != null){
                Intent changeBroadcast = new Intent(MediaPlayerService.CHANGE_CONFIGURATION);
                changeBroadcast.putExtra("shufle",shufle);
                changeBroadcast.putExtra("repeat",repeat);
                changeBroadcast.putExtra("superRepeat",superRepeat);
                changeBroadcast.putExtra("audio",audio);
                sendBroadcast(changeBroadcast);

                if(intent.getIntExtra("audioID", audioIndex) == audioIndex){
                    onPrepared(mediaPlayer);
                }
                else{
                    Intent broadcastIntent = new Intent(GET_PLAYER);
                    broadcastIntent.putExtra("duracao", mediaPlayer.getDuration());
                    broadcastIntent.putExtra("position", mediaPlayer.getCurrentPosition());
                    broadcastIntent.putExtra("audioID", audioIndex);
                    broadcastIntent.putExtra("playing", mediaPlayer.isPlaying());
                    broadcastIntent.putExtra("id", activeAudio.getId());
                    sendBroadcast(broadcastIntent);
                }
            }
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver nextTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            next();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver previusTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            previews();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver pauseTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            pause();
        }
    };

    //Quando o audio tiver sido preparado
    private BroadcastReceiver playTo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            play();
        }
    };

    private BroadcastReceiver playNewAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Get the new media index form SharedPreferences
            audioIndex = new StorageUtil(getApplicationContext()).loadAudioIndex();

            if(audioIndex < 0) audioIndex = audioList.size()-1;

            if(audioIndex > audioList.size()-1) audioIndex = 0;

            if (audioIndex != -1 && audioIndex < audioList.size()) {
                //index is in a valid range
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }

            //A PLAY_NEW_AUDIO action received
            //reset mediaPlayer to play the new Audio
            stopMedia();
            mediaPlayer.reset();
            initMediaPlayer();
            updateMetaData();
            buildNotification(PlaybackStatus.PLAYING);
        }
    };

    void registerBroadCasts (){
        //Register playNewMedia receiver
        IntentFilter playAudio = new IntentFilter(MainActivity.Broadcast_PLAY_NEW_AUDIO);
        registerReceiver(playNewAudio, playAudio);

        //register after getting audio focus
        IntentFilter noisy = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, noisy);

        //Receber configuação
        IntentFilter iGetConfig = new IntentFilter(MediaPlayerService.CHANGE_CONFIGURATION);
        registerReceiver(getConfiguration, iGetConfig);

        //OnSeekTo
        IntentFilter iOnSeek = new IntentFilter(MediaPlayerService.SEEK_TO);
        registerReceiver(seekTo, iOnSeek);

        //Tocar novo audio
        IntentFilter iStartAudio = new IntentFilter(MediaPlayerService.ACTION_START);
        registerReceiver(startAudio, iStartAudio);

        //OnSeekTo
        IntentFilter iHasPlaying = new IntentFilter(MediaPlayerService.HAS_PLAYING);
        registerReceiver(hasPlaying, iHasPlaying);

        //Next
        IntentFilter iNext = new IntentFilter(MediaPlayerService.ACTION_NEXT);
        registerReceiver(nextTo, iNext);

        //Preview
        IntentFilter iPrevious = new IntentFilter(MediaPlayerService.ACTION_PREVIOUS);
        registerReceiver(previusTo, iPrevious);

        //play
        IntentFilter iPlay = new IntentFilter(MediaPlayerService.ACTION_PLAY);
        registerReceiver(playTo, iPlay);

        //Pause
        IntentFilter iPause = new IntentFilter(MediaPlayerService.ACTION_PAUSE);
        registerReceiver(pauseTo, iPause);
    }

    void unRegisterBroadCasts (){
        unregisterReceiver(playNewAudio);

        unregisterReceiver(becomingNoisyReceiver);

        unregisterReceiver(getConfiguration);

        unregisterReceiver(hasPlaying);

        unregisterReceiver(startAudio);

        unregisterReceiver(seekTo);

        unregisterReceiver(nextTo);

        unregisterReceiver(previusTo);

        unregisterReceiver(playTo);

        unregisterReceiver(pauseTo);
    }
    //endregion

    //region Notification
    private void buildNotification(PlaybackStatus playbackStatus) {

        if(mediaSession == null){
            try {
                initMediaSession();
                initMediaPlayer();
            } catch (RemoteException e) {
                e.printStackTrace();
                stopSelf();
            }
            buildNotification(PlaybackStatus.PLAYING);
            return;
        }

        int notificationAction = android.R.drawable.ic_media_pause;//needs to be initialized
        PendingIntent play_pauseAction = null;

        //Build a new notification according to the current state of the MediaPlayer
        if (playbackStatus == PlaybackStatus.PLAYING) {
            notificationAction = R.drawable.ic_pause_minor;
            //create the pause action
            play_pauseAction = playbackAction(1);
        } else if (playbackStatus == PlaybackStatus.PAUSED) {
            notificationAction = R.drawable.ic_play_minor;
            //create the play action
            play_pauseAction = playbackAction(0);
        }

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.logo_fortaleza);

        // Create a new Notification
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setShowWhen(false)
                // Set the Notification style
                .setStyle(new Notification.MediaStyle()
                        // Attach our MediaSession token
                        .setMediaSession(mediaSession.getSessionToken())
                        // Show our playback controls in the compact notification view.
                        .setShowActionsInCompactView(0, 1, 2))
                // Set the Notification color
                .setColor(getResources().getColor(R.color.color_back_player_2))
                // Set the large and small icons
                .setLargeIcon(largeIcon)
                .setSmallIcon(android.R.drawable.stat_sys_headset)
                // Set Notification content information
                .setContentText(activeAudio.getArtist())
                .setContentTitle(activeAudio.getAlbum())
                .setContentInfo(activeAudio.getTitle())
                // Add playback actions
                .addAction(R.drawable.ic_previous, "previous", playbackAction(3))
                .addAction(notificationAction, "pause", play_pauseAction)
                .addAction(R.drawable.ic_next, "next", playbackAction(2));


        SetNotificationChanel(notificationManager, notificationBuilder);
        notificationManager.notify("audioController", NOTIFICATION_ID, notificationBuilder.build());
    }

    public void removeNotification() {
        notificationManager.cancel("audioController", NOTIFICATION_ID);
    }

    private void SetNotificationChanel (NotificationManager notific, Notification.Builder builder){
        String channelId = "SPFC";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel followerChannel = new NotificationChannel(channelId, "notification", NotificationManager.IMPORTANCE_HIGH);
            notific.createNotificationChannel(followerChannel);
            builder.setChannelId(channelId);
        }
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MediaPlayerService.class);
        switch (actionNumber) {
            case 0:
                // Play
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Pause
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Next track
                playbackAction.setAction(ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                // Previous track
                playbackAction.setAction(ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 4:
                playbackAction.setAction(SEEK_TO);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            transportControls.play();
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
        } else if (actionString.equalsIgnoreCase(ACTION_NEXT)) {
            transportControls.skipToNext();
        } else if (actionString.equalsIgnoreCase(ACTION_PREVIOUS)) {
            transportControls.skipToPrevious();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
        }
    }
    //endregion

    //Handle incoming phone calls
    private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null) {
                            pauseMedia();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (mediaPlayer != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                resumeMedia();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    public class LocalBinder extends Binder {
        public MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

}