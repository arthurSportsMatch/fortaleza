package com.sportm.fortaleza;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdSize;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

import data.ReceitaData;
import utils.AdMobController;
import utils.AsyncOperation;
import utils.TRACKING;
import utils.UTILS;

public class NoticiaActivity extends MainActivity {

    ViewHolder mHolder;
    boolean isRecipe = true;
    int recipeID = 0;     // ID da receita

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticia);

        mHolder = new ViewHolder();
        initVars();
        initAction();

        Intent intent = getIntent();
        if(intent.hasExtra("url")){
            String url = intent.getStringExtra("url");
            if(!url.isEmpty()){
                setRecipe(url);
            }else{
                if(intent.hasExtra("id")){
                    recipeID = intent.getIntExtra("id", 0);
                    loadRecipe(recipeID);
                }
            }
        }else{
            if(intent.hasExtra("id")){
                recipeID = intent.getIntExtra("id", 0);
                loadRecipe(recipeID);
            }
        }

        LinearLayout mAdView = findViewById(R.id.adView);
        AdMobController.createBannerAd(activity, mAdView, AdSize.BANNER, AdMobController.NOTICIA_BOTTOM_BANNER_ID);
    }

    @Override
    public void onBackPressed() {
        if(mHolder.webviewContent.canGoBack()){
            mHolder.webviewContent.goBack();
        }else{
            super.onBackPressed();
        }
    }

    void initVars (){
        WebSettings webSetting = mHolder.webviewContent.getSettings();
        webSetting.setBuiltInZoomControls(true);
        webSetting.setJavaScriptEnabled(true);
        mHolder.webviewContent.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                if(url.endsWith(".pdf")){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(url), "application/pdf");
                    String googleDocs = "https://docs.google.com/viewer?url=";

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        mHolder.webviewContent.loadUrl(googleDocs + url);
                    }

                    return true;
                }

                else if (UTILS.hasImage(url)){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    return true;
                }

                else{
                    return super.shouldOverrideUrlLoading(webView, url);
                }
            }
        });
    }

    void initAction (){
        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHolder.webviewContent.canGoBack()){
                    mHolder.webviewContent.goBack();
                }else{
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_NOTICIA_FECHAR, recipeID).execute();
                    finish();
                }
            }
        });
    }

    //----------------------------------------------

    //Pega os valores da receita do servidor
    private void loadRecipe (int recipeID){

        Hashtable<String, Object> params = new Hashtable<String, Object>();
        params.put("id", recipeID);
        if(isRecipe){
            new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_NOTICIA, 0, new AsyncOperation.IAsyncOpCallback() {
                @Override
                public void CallHandler(int opId, JSONObject response, boolean success) {
                    if(success){
                        this.OnAsyncOperationSuccess(opId, response);
                    }else{
                        this.OnAsyncOperationError(opId, response);
                    }
                }

                @Override
                public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                    Log.d("Recipe-Menu", response.toString());
                    try {
                        if (response.getInt("Status") == 200){

                            if(response.has("Object")){
                                Gson gson = new Gson();
                                ReceitaData recipe = gson.fromJson(response.get("Object").toString(), ReceitaData.class);
                                if(recipe != null) {
                                    setRecipe(recipe);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnAsyncOperationError(int opId, JSONObject response) {

                }
            }).execute(params);
        }else{
            new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_TRAINING, 0, new AsyncOperation.IAsyncOpCallback() {
                @Override
                public void CallHandler(int opId, JSONObject response, boolean success) {
                    if(success){
                        this.OnAsyncOperationSuccess(opId, response);
                    }else{
                        this.OnAsyncOperationError(opId, response);
                    }
                }

                @Override
                public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                    Log.d("Recipe-Menu", response.toString());
                    try {
                        if (response.getInt("Status") == 200){

                            if(response.has("Object")){
                                Gson gson = new Gson();
                                ReceitaData recipe = gson.fromJson(response.get("Object").toString(), ReceitaData.class);

                                if(recipe != null)
                                    setRecipe(recipe);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnAsyncOperationError(int opId, JSONObject response) {

                }
            }).execute(params);
        }

    }

    //Seta receita
    void setRecipe (final ReceitaData recipe){

        //escreve html no embed
        if(recipe.getUrl() != null){
            mHolder.webviewContent.loadUrl(recipe.getUrl());
        }

        if(recipe.getShare() != null){
            mHolder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_NOTICIA_COMPARTILHAR, recipeID).execute();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    String tittle = " Baixe agora o app oficial do Fortaleza EC e fique por dentro de tudo que acontece!";
                    String body = recipe.getShare();
                    intent.putExtra(Intent.EXTRA_SUBJECT, tittle);
                    intent.putExtra(Intent.EXTRA_TEXT,  body + tittle);
                    startActivity(Intent.createChooser(intent, "Compartilhe com"));
                }
            });
        }
    }

    void setRecipe (final String link){
        //escreve html no embed
        if(link != null){
            mHolder.webviewContent.loadUrl(link);
        }

        if(link != null){
            mHolder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity,AsyncOperation.TASK_ID_SAVE_TRACKING,999, emptyAsync, TRACKING.TRACKING_NOTICIA_COMPARTILHAR, recipeID).execute();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    String tittle = "Baixe agora o app oficial do Fortaleza EC e fique por dentro de tudo que acontece!";
                    String body = link.replace("/app/", "/url/");
                    intent.putExtra(Intent.EXTRA_SUBJECT, tittle);
                    intent.putExtra(Intent.EXTRA_TEXT,  body + tittle);
                    startActivity(Intent.createChooser(intent, "Compartilhe com"));
                }
            });
        }
    }

    public class WebViewClient extends android.webkit.WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            mHolder.loading.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {

            // TODO Auto-generated method stub

            super.onPageFinished(view, url);
            mHolder.loading.setVisibility(View.GONE);
        }

    }


    //ViewHolder com valores da view
    class ViewHolder {
        TextView nome;
        WebView webviewContent;
        ImageView share;
        View voltar;
        View loading;

        public ViewHolder (){
            nome = findViewById(R.id.recipe_nome);
            webviewContent = findViewById(R.id.web_receita);
            share = findViewById(R.id.share_button);
            voltar = findViewById(R.id.txt_voltar);
            loading = findViewById(R.id.loading);
        }
    }
}
