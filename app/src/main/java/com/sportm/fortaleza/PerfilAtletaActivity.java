package com.sportm.fortaleza;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;

import adapter.PageAdapter;
import data.AtletaData;
import data.FragmentData;
import fragments.AtletaCasaDetailsFragment;
import fragments.AtletaChartFragment;
import fragments.AtletaDetalsFragment;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.FragmentHolder;
import utils.UTILS;

public class PerfilAtletaActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    private PagerAdapter adapter;
    private ArrayList<FragmentHolder> pageList = new ArrayList<>();

    int idAtleta;
    private static final int OP_ID_JOGADOR       = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_atleta);
        mHolder = new ViewHolder();

        Intent intent = getIntent();
        idAtleta = intent.getIntExtra("id",0);

        getPlayer(idAtleta);
    }

    void getPlayer (int id){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("id", id);
        new AsyncOperation(this, AsyncOperation.TASK_ID_GET_PLAYER, OP_ID_JOGADOR, this).execute(params);
    }

    //toDo tirar print da tela e compartilhar pelo lugar escolhido
    void printScreen (){

    }

    void initAtleta (AtletaData data){
        if(data != null ){

            UTILS.getImageAt(activity, data.getImg(), mHolder.atletaimg, new RequestOptions()
                    .placeholder(R.drawable.player_placeholder)
                    .error(R.drawable.player_placeholder));

            mHolder.atletaPos.setText(data.getPos());
            if(data.getCamisa() != null)
                mHolder.atletaNum.setText(String.valueOf(data.getCamisa()));

            mHolder.atletaName.setText(data.getNome());

            if(data.getMadeCotia() > 0){
                mHolder.madeInCotia.setVisibility(View.VISIBLE);
            }

            mHolder.atletaCompleteName.setText(data.getNomeCompleto());

            String nascimento = data.getDataNascimento()!= null? UTILS.formatDateFromDB(data.getDataNascimento()): "";
            if(!nascimento.isEmpty()){
                String dataContent = nascimento + " (" + UTILS.getBirthByDate(data.getDataNascimento()) + ")";
                mHolder.atletaNascimento.setText(dataContent);
            }


            mHolder.atletaNacionalidade.setText(data.getLocal());

            initFragments(data);
        }else{
            Toast.makeText(this, "Problemas em achar o atleta", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }

    }

    void initFragments (AtletaData data){
//        pageList.add(new FragmentHolder(new FragmentData(new AtletaChartFragment(idAtleta),"Data")));
//        pageList.add(new FragmentHolder(new FragmentData(new AtletaCasaDetailsFragment(data),"Data 3")));
        pageList.add(new FragmentHolder(new FragmentData(new AtletaDetalsFragment(data),"Data")));
        adapter = new PageAdapter(getSupportFragmentManager(), pageList);

        mHolder.dataPager.setAdapter(adapter);
        mHolder.dataPager.setPageMargin(20);
    }

    //region AsyncOperation
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_ID_JOGADOR:{
                int status = 0;
                boolean success = false;
                if(response.has("Status")) {
                    try {
                        status = (int) response.get("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(status == 200){
                    if(response.has("Object")){
                        try {
                            Gson gson = new Gson();
                            AtletaData data = gson.fromJson(response.getString("Object"), AtletaData.class);
                            if(data != null){
                                initAtleta(data);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        ImageView share;
        ImageView madeInCotia;

        ImageView atletaimg;
        TextView atletaPos;
        TextView atletaNum;
        TextView atletaName;

        TextView atletaCompleteName;
        TextView atletaNascimento;
        TextView atletaPeso;
        TextView atletaNacionalidade;

        ViewPager dataPager;

        public ViewHolder (){
            share = findViewById(R.id.img_share);
            madeInCotia = findViewById(R.id.img_madein_cotia);
            atletaimg = findViewById(R.id.img_atleta);
            atletaPos = findViewById(R.id.txt_posicao);
            atletaNum = findViewById(R.id.txt_numero_camisa);
            atletaName = findViewById(R.id.txt_nome_atleta);

            atletaCompleteName = findViewById(R.id.txt_nome_atleta_completo);
            atletaNascimento = findViewById(R.id.txt_atleta_nascimento);
            atletaPeso = findViewById(R.id.txt_atleta_peso);
            atletaNacionalidade = findViewById(R.id.txt_atleta_nacionalidade);

            dataPager = findViewById(R.id.view_dados_pager);
        }
    }
}
