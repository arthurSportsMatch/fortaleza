package com.sportm.fortaleza;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import adapter.FilterToggleAdapter;
import data.FilterData;
import utils.AsyncOperation;

public class SearchActivity extends MainActivity {

    ViewHolder holder;
    private static String toSearch = "";

    private FilterToggleAdapter segmentosAdapter;
    private FilterToggleAdapter produtosAdapter;

    public static String getToSearch() {
        return toSearch;
    }

    public static void setToSearch(String toSearch) {
        SearchActivity.toSearch = toSearch;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        holder = new ViewHolder();

        getSegmentos();
        getProdutos();
        initAction();

        setSupportActionBar(holder.tolbar);
        setTitle("Filtros");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_aplicar, menu);
        return true;//super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_aplicar:{
                apply();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initAction (){
        holder.limparSegmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(segmentosAdapter != null){
                    //limpar
                    if(holder.limparSegmentos.getText().toString().startsWith("L")){
                        segmentosAdapter.setToggle(false);
                        holder.limparSegmentos.setText("Selecionar Tudo");
                    }else{
                        segmentosAdapter.setToggle(true);
                        holder.limparSegmentos.setText("Limpar Tudo");
                    }
                }
            }
        });

        holder.limparProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(produtosAdapter != null){
                    //limpar
                    if(holder.limparProdutos.getText().toString().startsWith("L")){
                        produtosAdapter.setToggle(false);
                        holder.limparProdutos.setText("Selecionar Tudo");
                    }else{
                        produtosAdapter.setToggle(true);
                        holder.limparProdutos.setText("Limpar Tudo");
                    }
                }
            }
        });
    }

    //Recebe os segmentos de produtos para colocar de filtro
    void getSegmentos (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_SEGMENTS, 00, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    FilterData[] data = gson.fromJson(response.getString("Object"), FilterData[].class);
                    List<FilterData> segmentos = new ArrayList<>(Arrays.asList(data));
                    segmentosAdapter = new FilterToggleAdapter(segmentos, SearchActivity.this, R.layout.adapter_filter_toggle);
                    segmentosAdapter.setToggle(true);
                    holder.segmentos.setAdapter(segmentosAdapter);
                    holder.segmentos.setLayoutManager(new LinearLayoutManager(SearchActivity.this, RecyclerView.VERTICAL, false));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute();
    }

    //Recebe os produtos para colocar nos filtros
    void getProdutos (){
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PRODUCTS_FILTER, 01, new AsyncOperation.IAsyncOpCallback() {
            @Override
            public void CallHandler(int opId, JSONObject response, boolean success) {
                if(success){
                    this.OnAsyncOperationSuccess(opId, response);
                }else{
                    this.OnAsyncOperationError(opId, response);
                }
            }

            @Override
            public void OnAsyncOperationSuccess(int opId, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    FilterData[] data = gson.fromJson(response.getString("Object"), FilterData[].class);
                    List<FilterData> produtos = new ArrayList<>(Arrays.asList(data));
                    produtosAdapter = new FilterToggleAdapter(produtos, SearchActivity.this, R.layout.adapter_filter_toggle);

                    //Todo colocar valores recebidos no adapter
                    holder.produtos.setAdapter(produtosAdapter);
                    holder.produtos.setLayoutManager(new LinearLayoutManager(SearchActivity.this, RecyclerView.VERTICAL, false));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnAsyncOperationError(int opId, JSONObject response) {

            }
        }).execute();
    }


    void apply (){
        //Pego os filtros aplicados
        List<FilterData> segmentos = segmentosAdapter.getToggled();
        List<FilterData> produtos = produtosAdapter.getToggled();

        //Retiro os valores das listas e aplico em arrays
        int[] segmentosArr = copyListToArray(segmentos);
        int[] produtosArr = copyListToArray(produtos);

        //Envio os arrays de string para fazer filtragem no app
        Intent intent = getIntent();
        intent.putExtra("segmentos", segmentosArr);
        intent.putExtra("produtos", produtosArr);
        setResult(RESULT_OK, intent);
        Log.d("Recipe_Filters", "segmentos1: " + Arrays.toString(segmentosArr) + " produtos1: " + Arrays.toString(produtosArr));
        finish();
    }

    public int[] copyListToArray (List<FilterData> data){
        int[] newString = new int[data.size()];
        for(int i = 0; i < data.size(); i++){
            newString[i] = data.get(i).getId();
        }
        return newString;
    }

    class ViewHolder {
        Toolbar tolbar;
        TextView limparSegmentos;
        RecyclerView segmentos;

        TextView limparProdutos;
        RecyclerView produtos;

        public ViewHolder (){
            tolbar = findViewById(R.id.topbar);
            limparSegmentos = findViewById(R.id.txt_limpar_segmentos);
            segmentos = findViewById(R.id.recycler_segmentos);

            limparProdutos = findViewById(R.id.txt_limpar_produtos);
            produtos = findViewById(R.id.recycler_produtos);
        }
    }
}
