package com.sportm.fortaleza;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import adapter.PlaylistItemsAdapter;
import data.PlaylistItemData;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.TRACKING;
import utils.UTILS;

public class PlaylistActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback {

    ViewHolder mHolder;
    PlaylistItemsAdapter adapter;
    int actualPage = 1;
    boolean initFirtItem = false;
    boolean destaqueSelected = false;
    private static final int OP_GET_PLAYLIST              = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        Intent intent = getIntent();
        initFirtItem = intent.getBooleanExtra("initFirt", false);

        mHolder = new ViewHolder();
        getPlaylist(actualPage);
    }

    void getPlaylist (int page){
        Hashtable<String, Object> params = new Hashtable<>();
        params.put("page", page);
        new AsyncOperation(activity, AsyncOperation.TASK_ID_GET_PLAYLIST, OP_GET_PLAYLIST,this).execute(params);
    }

    void initPlaylist (List<PlaylistItemData> data){

        if(!destaqueSelected){
            initDestaque(data.get(0));
            destaqueSelected = true;
        }


        if(adapter == null){

            //data.remove(0);
            adapter = new PlaylistItemsAdapter(activity, data, new PlaylistItemsAdapter.Listener() {
                @Override
                public void LastItem() {
                    actualPage ++;
                    getPlaylist(actualPage);
                }

                @Override
                public void onClick(int id) {
                    Intent intent = new Intent(PlaylistActivity.this, PlayerActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onClick(int position, PlaylistItemData data){
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_AUDIOTECA_TOCAR_ITEM, data.getId()).execute();
                    Intent intent = new Intent(PlaylistActivity.this, PlayerActivity.class);
                    intent.putExtra("position", position);
                    intent.putExtra("duracao", data.getDuracao());
                    intent.putExtra("id", data.getId());
                    startActivity(intent);
                }
            });

            mHolder.players.setAdapter(adapter);
            mHolder.players.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
            mHolder.players.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
            mHolder.players.setNestedScrollingEnabled(false);
        }else{
            adapter.addPlaylists(data);
        }
    }

    void initDestaque (final PlaylistItemData data){

        //MainActivity.instance.addAudio(new Audio( data.getId(), CONSTANTS.serverContentCatalogs + data.getAudio(), "São Paulo FC", data.getTitulo(), UTILS.formatDateFromDB(data.getDataHora())));

        UTILS.getImageAt(activity, data.getImg(), mHolder.audioImage, new RequestOptions()
                .transforms(new RoundedCorners(100)));

        mHolder.audioTittle.setText(data.getTitulo());

        mHolder.destaqueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_AUDIOTECA_TOCAR_DESTAQUE, data.getId()).execute();
                Intent intent = new Intent(PlaylistActivity.this, PlayerActivity.class);
                intent.putExtra("position", 0);
                intent.putExtra("duracao", data.getDuracao());
                intent.putExtra("id", data.getId());
                startActivity(intent);
            }
        });

        if(initFirtItem){
            Intent intent = new Intent(PlaylistActivity.this, PlayerActivity.class);
            intent.putExtra("position", 0);
            intent.putExtra("duracao", data.getDuracao());
            intent.putExtra("id", data.getId());
            startActivity(intent);
        }
    }

    //region Async
    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Message message = new Message();
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        message.setData(b);
        handlerOp.sendMessage(message);
    }

    private Handler handlerOp = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int opId = -1;
            JSONObject response = null;
            boolean success = false;

            try{
                opId = message.getData().getInt("opId");
                response = new JSONObject(message.getData().getString("response"));
                success = message.getData().getBoolean("success");
            }
            catch (JSONException e){
                UTILS.DebugLog("Error", "Error getting handlers params.");
                return false;
            }

            if(success) {
                OnAsyncOperationSuccess(opId, response);
            }
            else{
                OnAsyncOperationError(opId, response);
            }
            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_PLAYLIST:{
                //pegar quiz
                int status = 0;
                //pegar categoria de quiz
                if(response.has("Status")) {
                    try {
                        status = response.getInt("Status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //pegar categoria de quiz
                if(response.has("Object")){
                    if(status == 200){
                        try {
                            Gson gson = new Gson();
                            JSONObject obj = response.getJSONObject("Object");
                            PlaylistItemData[] data = gson.fromJson(obj.getString("itens"), PlaylistItemData[].class);
                            List<PlaylistItemData> list = new ArrayList<>(Arrays.asList(data));

                            if(data != null){
                                initPlaylist(list);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {

    }
    //endregion

    class ViewHolder {
        ImageView audioImage;
        TextView audioTittle;
        Button destaqueBtn;

        RecyclerView players;

        public ViewHolder (){
            audioImage = findViewById(R.id.img_audio_image);
            audioTittle = findViewById(R.id.txt_audio_tittle);
            destaqueBtn = findViewById(R.id.btn_audio_play);

            players = findViewById(R.id.recycler);
        }
    }
}
