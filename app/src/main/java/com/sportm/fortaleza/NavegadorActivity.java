package com.sportm.fortaleza;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import utils.UTILS;

public class NavegadorActivity extends MainActivity {

    ViewHolder mHolder;
    String link = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador);

        Intent intent = getIntent();
        if(intent.hasExtra("link")){
            link = intent.getStringExtra("link");
        }

        mHolder = new ViewHolder();
        initVars();

        loadURL();
        initAction();
    }

    @Override
    public void onBackPressed() {
        if(mHolder.webviewContent.canGoBack()){
            mHolder.webviewContent.goBack();
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onBackPressed();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    void initVars (){
        WebSettings webSettings = mHolder.webviewContent.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setAllowFileAccess(true);

        mHolder.webviewContent.setWebViewClient(new WebViewClient());
        mHolder.webviewContent.setWebChromeClient(new ChromeClient());
    }

    void initAction (){
        mHolder.voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHolder.webviewContent.canGoBack()){
                    mHolder.webviewContent.goBack();
                }else{
                    finish();
                }
            }
        });
    }

    //Seta receita
    void loadURL (){
        if(!link.isEmpty()){
            if(link.endsWith(".pdf")){
                Log.d("Async-URL", "PDF");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(link), "application/pdf");
                String googleDocs = "https://docs.google.com/viewer?url=";

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                    finish();
                } else {
                    mHolder.webviewContent.loadUrl(googleDocs + link);
                }
            }

            else if (UTILS.hasImage(link)){
                Log.d("Async-URL", "IMAGE");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(intent);
                finish();
            }

            else{
                Log.d("Async-URL", "NORMAL");
                mHolder.webviewContent.loadUrl(link);
            }
        }
    }

    //ViewHolder com valores da view
    class ViewHolder {
        TextView nome;
        WebView webviewContent;
        ImageView share;
        View voltar;
        View loading;

        public ViewHolder (){
            nome = findViewById(R.id.recipe_nome);
            webviewContent = findViewById(R.id.web_receita);
            share = findViewById(R.id.share_button);
            voltar = findViewById(R.id.txt_voltar);
            loading = findViewById(R.id.loading);
        }
    }

    class WebViewClient extends android.webkit.WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mHolder.loading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mHolder.loading.setVisibility(View.GONE);
        }

    }

    class ChromeClient extends WebChromeClient {
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        ChromeClient() {}

        public Bitmap getDefaultVideoPoster()
        {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
        }

        public void onHideCustomView()
        {
            ((FrameLayout)getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            if (this.mCustomView != null)
            {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout)getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(3846 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }
}
