package com.sportm.fortaleza;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.Hashtable;
import java.util.List;

import data.TweetResultData;
import utils.AsyncOperation;
import utils.UTILS;

public class TwitterFeedCardExpandedActivity extends MainActivity implements AsyncOperation.IAsyncOpCallback{

    private static final int OP_GET_HTML_CODE_VIDEO = 0;
    private static final int OP_GET_HTML_CODE_IMG = 1;
    private static final int OP_SAVE_TRACKING  = 2;

    RelativeLayout btnCloseTweetCard;
    RelativeLayout bgTweetCard;
    ImageView imgPortraitTwitterUserCard;
    TextView txtUserCard;
    TextView txtMoreCard;
    TextView txtTagCard;
    TextView txtMsgCard;
    TextView txtDateCard;
    ImageView imgContentCard;
    WebView webViewCard;

    private static TweetResultData tweet = null;

    public static TweetResultData getTweet() {
        return tweet;
    }

    public static void setTweet(TweetResultData tweet) {
        TwitterFeedCardExpandedActivity.tweet = tweet;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_feed_card_expanded);

        btnCloseTweetCard = (RelativeLayout)findViewById(R.id.btnCloseTweetCard);
        bgTweetCard = (RelativeLayout)findViewById(R.id.bgTweetCard);
        imgPortraitTwitterUserCard = (ImageView)findViewById(R.id.imgPortraitTwitterUserCard);
        txtUserCard = (TextView)findViewById(R.id.txtUserCard);
        txtMoreCard = (TextView)findViewById(R.id.txtMoreCard);
        txtTagCard = (TextView)findViewById(R.id.txtTagCard);
        txtMsgCard = (TextView)findViewById(R.id.txtMsgCard);
        txtDateCard = (TextView)findViewById(R.id.txtDateCard);
        imgContentCard = (ImageView)findViewById(R.id.imgContentCard);
        webViewCard = (WebView)findViewById(R.id.webViewCard);

        webViewCard.getSettings().setJavaScriptEnabled(true);
        webViewCard.getSettings().setDefaultTextEncodingName("utf-8");
        webViewCard.setWebViewClient(new WebViewClient());
        webViewCard.setWebChromeClient(new WebChromeClient());

        webViewCard.getSettings().setDomStorageEnabled(true);
        webViewCard.getSettings().setAppCacheEnabled(true);
        webViewCard.getSettings().setAppCachePath(this.getFilesDir().getAbsolutePath() + "/cache");
        webViewCard.getSettings().setDatabaseEnabled(true);
        webViewCard.getSettings().setDatabasePath(this.getFilesDir().getAbsolutePath() + "/databases");

        btnCloseTweetCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtMoreCard.setVisibility(View.INVISIBLE);

        Bundle b = getIntent().getExtras();

        if(b != null){
            if(b.containsKey("tweet")){
                tweet = (TweetResultData) b.getSerializable("tweet");
            }
        }

        if(tweet == null){
            ConfigureTweetError();
        }

        ConfigureTweet(tweet);
    }

    void ConfigureTweetError(){
        Toast.makeText(getApplicationContext(), "Não foi possivel carregar", Toast.LENGTH_LONG).show();
        finish();
    }

    void ConfigureTweet(final TweetResultData tweet){
        if(!tweet.getImgUser().isEmpty()) {
            Glide.with(this).
                    load(tweet.getImgUser()).
                    apply(new RequestOptions().circleCrop()).
                    into(imgPortraitTwitterUserCard);
        }

        txtUserCard.setText(tweet.getNameUser());
        txtTagCard.setText(tweet.getTagUser());
        String text = tweet.getMsgContent();
        Log.d("Twitt_full", text);
        txtMsgCard.setText(text);
        //txtDateCard.setText(tweet.getDate().toTimeAgo(this));

        if(tweet.getDate() != null) {
            PrettyTime prettyTime = new PrettyTime(getResources().getConfiguration().locale);
            txtDateCard.setText(prettyTime.format(tweet.getDate()));

        }
        else {
            txtDateCard.setText("");
        }

        txtMoreCard.setVisibility(View.INVISIBLE);
        if(tweet.isImage()){

            webViewCard.setVisibility(View.GONE);
            imgContentCard.setVisibility(View.VISIBLE);

            imgContentCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putInt("contentType", MediaPlayActivity.MEDIA_PLAY_CONTENT_TYPE_IMAGE);
                    b.putString("content", tweet.getImgContent());
                    Intent newIntent = new Intent(TwitterFeedCardExpandedActivity.this, MediaPlayActivity.class);
                    newIntent.putExtras(b);
                    startActivity(newIntent);
                }
            });

            if(!tweet.getImgContent().isEmpty()) {
                Glide.with(this).
                        load(tweet.getImgContent()).
                        apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))
                                .error(R.drawable.tw__composer_logo_blue)).
                        into(imgContentCard);
            }
            else {

                if(tweet.isTriedToLoadImg()) {

                    if(!tweet.getImgContent().isEmpty()) {
                        Glide.with(this).
                                load(tweet.getImgContent()).
                                apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20))
                                        .error(R.drawable.tw__composer_logo_blue)).
                                into(imgContentCard);
                    }
                    else {
                        Glide.with(this).
                                load(R.drawable.tw__composer_logo_blue).
                                into(imgContentCard);
                    }
                }
                else {

                    Hashtable<String, Object> params = new Hashtable<>();
                    params.put("url", tweet.getImageHtmlToExtract());
                    params.put("id", tweet.getId());

                    new AsyncOperation(this, AsyncOperation.TASK_ID_GET_HTML_PAGE_CODE, OP_GET_HTML_CODE_IMG, this).execute(params);

                }

            }

        }
        else { //isVideo

            if(tweet.getVideoContent().isEmpty()){

                if(tweet.isTriedToLoadVideo()) {
                    webViewCard.setVisibility(View.GONE);
                    imgContentCard.setVisibility(View.VISIBLE);

                    if(!tweet.getImgContent().isEmpty()) {
                        Glide.with(this).
                                load(tweet.getImgContent()).
                                apply(new RequestOptions() .error(R.drawable.tw__composer_logo_blue)).
                                into(imgContentCard);
                    }
                    else {
                        Glide.with(this).
                                load(R.drawable.tw__composer_logo_blue).
                                into(imgContentCard);
                    }
                }
                else {

                    Hashtable<String, Object> params = new Hashtable<>();
                    params.put("url", tweet.getVideoHtmlToExtract());
                    params.put("id", tweet.getId());

                    new AsyncOperation(this, AsyncOperation.TASK_ID_GET_HTML_PAGE_CODE, OP_GET_HTML_CODE_VIDEO, this).execute(params);

                }
            }
            else {
                txtMoreCard.setVisibility(View.VISIBLE);
                webViewCard.setVisibility(View.VISIBLE);
                imgContentCard.setVisibility(View.INVISIBLE);

                webViewCard.loadUrl(tweet.getVideoContent());

                txtMoreCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Uri uri = Uri.parse(tweet.getVideoContent());

                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);

                    }
                });
            }

        }

    }

    @Override
    public void CallHandler(int opId, JSONObject response, boolean success) {
        Bundle b = new Bundle();
        b.putInt("opId", opId);
        b.putString("response", response.toString());
        b.putBoolean("success", success);
        Message msg = new Message();
        msg.setData(b);
        handler.sendMessage(msg);
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Bundle b = msg.getData();
            int opId = b.getInt("opId");
            boolean success = b.getBoolean("success");
            JSONObject response = new JSONObject();

            try {
                response = new JSONObject(b.getString("response"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(success){
                OnAsyncOperationSuccess(opId, response);
            }
            else {
                OnAsyncOperationError(opId, response);
            }

            return false;
        }
    });

    @Override
    public void OnAsyncOperationSuccess(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_HTML_CODE_VIDEO: {
                try {

                    if ((response.has("page")) && (response.get("page") != JSONObject.NULL)) {

                        String pattern1 = "<meta  property=\"og:video:url\" content=\"";
                        String pattern2 = "\">";

                        String page = (response.getString("page"));

                        List<String> strings = UTILS.getStringsBetweenStrings(page, pattern1, pattern2);

                        if(!strings.isEmpty()) {
                            tweet.setVideoContent(strings.get(0));
                        }
                        tweet.setTriedToLoadVideo(true);

                        webViewCard.setVisibility(View.VISIBLE);
                        imgContentCard.setVisibility(View.INVISIBLE);

                        webViewCard.loadUrl(tweet.getVideoContent());

                    }

                } catch (JSONException e) {
                    //UTILS.DebugLog(TAG, e);
                    ConfigureTweetError();
                }
            }

            case OP_GET_HTML_CODE_IMG: {
                try {

                    if ((response.has("page")) && (response.get("page") != JSONObject.NULL)) {

                        String pattern1 = "<meta  property=\"og:image\" content=\"";
                        String pattern2 = "\">";

                        String page = (response.getString("page"));

                        List<String> strings = UTILS.getStringsBetweenStrings(page, pattern1, pattern2);

                        if(!strings.isEmpty()) {
                            tweet.setImgContent(strings.get(0));
                        }
                        tweet.setTriedToLoadImg(true);

                        webViewCard.setVisibility(View.GONE);
                        imgContentCard.setVisibility(View.VISIBLE);

                        if(!tweet.getImgContent().isEmpty()) {
                            Glide.with(this).
                                    load(tweet.getImgContent()).
                                    apply(new RequestOptions() .error(R.drawable.tw__composer_logo_blue)).
                                    into(imgContentCard);
                        }
                    }

                } catch (JSONException e) {
                   // UTILS.DebugLog(TAG, e);

                    ConfigureTweetError();
                }
            }
        }
    }

    @Override
    public void OnAsyncOperationError(int opId, JSONObject response) {
        switch (opId){
            case OP_GET_HTML_CODE_IMG:
            case OP_GET_HTML_CODE_VIDEO:
                UTILS.DebugLog("OP_GET_AUTH_TOKEN", response.toString());
                break;
        }
    }

}
