package com.sportm.fortaleza;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import utils.camera.AutoFitTextureView;
import utils.camera.CustomCamera2;

public class CameraActivity extends MainActivity implements CustomCamera2.Listener{

    private CustomCamera2 camera;
    private boolean cameraFront = true;

    private ViewHolder mHolder;
    private Uri photoURI;

    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private Size mPreviewSize;

    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            initCamera();
            setUpCameraOutputs(width, height);
            //configureTransform(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            //configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }
    };

    private CaptureRequest mPreviewCaptureRequest;
    private CaptureRequest.Builder mPreviewCaptureRequestBuilder;
    private CameraCaptureSession mCameraCaptureSession;
    private CameraCaptureSession.CaptureCallback mCaptureSessionCallback = new
            CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                    super.onCaptureStarted(session, request, timestamp, frameNumber);
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        mHolder = new ViewHolder();

        Intent intent  = activity.getIntent();
        photoURI = intent.getData();

        initActions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if(mHolder.frame.isAvailable()){
            initCamera ();
        }else{
            mHolder.frame.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        camera.close();
        stopBackgroundThread();
    }

    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void initActions (){
        mHolder.swithCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseCamera();
            }
        });

        mHolder.takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(photoURI, mHolder.frame, mBackgroundHandler);
            }
        });
    }

    void initCamera (){
        getCamera();
    }

    void getCamera (){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            camera = new CustomCamera2(activity, this);
        } else {
            Toast.makeText(activity, "Sua camera não é aceita", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    void chooseCamera(){
        if(cameraFront){
            camera.changeCamera(0);
        }else{
            camera.changeCamera(1);
        }

        cameraFront = !cameraFront;
    }

    void setPreview (final CameraDevice cameraDevice){
        try {
            SurfaceTexture surfaceTexture = mHolder.frame.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            mPreviewCaptureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewCaptureRequestBuilder.addTarget(previewSurface);

            cameraDevice.createCaptureSession(Arrays.asList(previewSurface),
                new CameraCaptureSession.StateCallback() {
                    @Override
                    public void onConfigured(@NonNull CameraCaptureSession session) {
                        try {
                            mPreviewCaptureRequest = mPreviewCaptureRequestBuilder.build();
                            mCameraCaptureSession = session;
                            mCameraCaptureSession.setRepeatingRequest(
                                    mPreviewCaptureRequest,
                                    mCaptureSessionCallback,
                                    null
                            );
                        }catch (CameraAccessException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                        Toast.makeText(activity, "Erro ao abrir a camera", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, null);

        } catch (CameraAccessException e){
            e.printStackTrace();
        }
    }

    private void setUpCameraOutputs(int width, int height) {
        try {
            int id = 0;
            if(cameraFront){
                id = 0;
            }else{
                id = 1;
            }

            StreamConfigurationMap map = camera.getStreamConfig(id);
            if (map == null) {
                return;
            }

            Size largest = Collections.max(
                    Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                    new CompareSizesByArea());

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            int mSensorOrientation = camera.getOrientation(id);
            boolean swappedDimensions = false;
            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e("Camera2", "Display rotation is invalid: " + displayRotation);
            }

            Point displaySize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(displaySize);

            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

//            if (swappedDimensions) {
//                rotatedPreviewWidth = height;
//                rotatedPreviewHeight = width;
//                maxPreviewWidth = displaySize.y;
//                maxPreviewHeight = displaySize.x;
//            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            mPreviewSize =
//                    chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
//                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
//                    maxPreviewHeight, largest);

            getOptimalPreviewSize(map.getOutputSizes(SurfaceTexture.class), rotatedPreviewWidth, rotatedPreviewHeight);

        }catch (Exception e){

        }
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == mHolder.frame || null == mPreviewSize || null == activity) {
            return;
        }

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());

        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }

        mHolder.frame.setTransform(matrix);
    }

    private Size chooseOptimalSize(Size[] supportedPreviewSizes, int textureViewWidth, int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {
        List<Size> bigEnough = new ArrayList<>();
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : supportedPreviewSizes) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                bigEnough.add(option);
            }
//            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight && option.getHeight() == option.getWidth() * h / w) {
//                if (option.getWidth() >= textureViewWidth && option.getHeight() >= textureViewHeight) {
//                    bigEnough.add(option);
//                }
//            }
        }

        return Collections.min(bigEnough, new CompareSizesByArea());
    }

    private Size getOptimalPreviewSize(Size[] sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;
        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.getWidth() / size.getHeight();
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.getHeight() - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.getHeight() - targetHeight);
            }
        }
        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.getHeight() - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.getHeight() - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    @Override
    public void OnOpened(CameraDevice camera) {
        setPreview(camera);
    }

    @Override
    public void OnDisconnected(CameraDevice camera) {
        camera.close();
    }

    @Override
    public void OnError(CameraDevice camera, int error) {
        camera.close();
    }

    @Override
    public void TakePicture(File file) {
        try {
            //verifica se é nula
            if(file == null){
                return;
            }

            //verifica se existe
            if(!file.exists()){
                return;
            }

            //verifica se existe novamente
            if(file.exists()){
                if(file.length() > 0) {
                    Intent devolve = new Intent();
                    devolve.setData(Uri.fromFile(file));
                    if ( cameraFront ) {
                        devolve.putExtra("cam", 1);
                    }else {
                        devolve.putExtra("cam",2);
                    }
                    activity.setResult(RESULT_OK, devolve);
                    activity.finish();
                }else {
                    camera.takePicture(photoURI, mHolder.frame, mBackgroundHandler);
                }
            }

        } catch (SecurityException e) {
            Log.e("Camera-2", "Error accessing file: " + e.getMessage());
        }
    }

    class ViewHolder {

        private TextureView frame;
        private RelativeLayout swithCam;
        private RelativeLayout takePhoto;

        public ViewHolder (){
            frame = (TextureView) findViewById(R.id.cameraPreviewPerfil);
            swithCam = (RelativeLayout) findViewById(R.id.btnSwitchCamera_perfil);
            takePhoto = (RelativeLayout) findViewById(R.id.btntakePic_perfil);
        }
    }

    class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size o1, Size o2) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) o1.getHeight() * o1.getWidth() -
                    (long) o2.getHeight() * o2.getWidth());
        }
    }

    class ComparableByRatio implements Comparator<Size> {
        int ratio = 0;
        public ComparableByRatio(int ratio){
            this.ratio = ratio;
        }
        @Override
        public int compare(Size o1, Size o2) {
            return Long.signum((long) Math.abs((o2.getWidth() / o2.getHeight()) - ratio) - (long) Math.abs((o1.getWidth() / o1.getHeight()) - ratio));
        }
    }
}
