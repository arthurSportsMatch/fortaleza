package com.sportm.fortaleza;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import data.PositonData;
import informations.UserInformation;
import models.ErrorModel;
import models.MessageModel;
import models.user.UserModel;
import models.utils.RegulamentoModel;
import repository.user.UserRepository;
import repository.utils.RegulamentoRepository;
import utils.AsyncOperation;
import utils.CONSTANTS;
import utils.MaskEditUtil;
import utils.TRACKING;
import utils.UTILS;
import vmodels.user.UserViewModel;
import vmodels.utils.RegulamentoViewModel;

public class ConfigurationActivity extends MainActivity{

    ViewHolder mHolder;
    private UserViewModel userViewModel;
    private RegulamentoViewModel regulamentoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        mHolder = new ViewHolder();
        initActions();
    }

    @Override
    public void bindViewModel() {
        super.bindViewModel();
        UserRepository repository = new UserRepository(activity, prefs);
        userViewModel = ViewModelProviders.of(ConfigurationActivity.this).get(UserViewModel.class);
        userViewModel.setmRepository(repository);
        userViewModel.getUserData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel user) {
                uploadUser(user);
            }
        });
        userViewModel.getMessage().observe(this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel message) {
                showMessage(message.getMsg());
            }
        });
        userViewModel.getError().observe(this, new Observer<ErrorModel>() {
            @Override
            public void onChanged(ErrorModel error) {
                showMessage(error.getMsg());
            }
        });

        userViewModel.getUser(false);

        RegulamentoRepository  regulamentoRepository = new RegulamentoRepository(activity, prefs);
        regulamentoViewModel = ViewModelProviders.of(ConfigurationActivity.this).get(RegulamentoViewModel.class);
        regulamentoViewModel.setmRepository(regulamentoRepository);
        regulamentoViewModel.getRegulamento().observe(this, new Observer<RegulamentoModel>() {
            @Override
            public void onChanged(RegulamentoModel regulamento) {
                showRegulamento(regulamento.getVersion(), regulamento.getDocumentacao());
            }
        });
        regulamentoViewModel.getDocs().observe(this, new Observer<List<PositonData>>() {
           @Override
           public void onChanged(List<PositonData> docs) {
                initDocs(docs);
           }
        });
    }

    @Override
    public void unBindViewModel() {
        super.unBindViewModel();
        userViewModel.getUserData().removeObservers(this);
        userViewModel.getMessage().removeObservers(this);
        regulamentoViewModel.getRegulamento().removeObservers(this);
        regulamentoViewModel.getDocs().removeObservers(this);
    }

    void initActions (){
        mHolder.alterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_CONFIGURACAO_ALTERAR_DADOS).execute();
                openEditPerfil();
            }
        });

        mHolder.mudarSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPopupSenha();
            }
        });
    }

    void uploadUser (UserModel user){
        if(user.getNome() != null)
            mHolder.name.setHint(user.getNome());

        if(user.getEmail() != null)
            mHolder.email.setHint(user.getEmail());

        if(user.getCelular() != null && !user.getCelular().isEmpty()){
            String telefone = MaskEditUtil.unmask(user.getCelular());
            mHolder.telefone.setHint(MaskEditUtil.mask(telefone, MaskEditUtil.FORMAT_FONE));
        }

        if(user.getCpf() != null && !user.getCpf().isEmpty()){
            String cpf = MaskEditUtil.unmask(user.getCpf());
            String finalCpf = cpf.substring(0, 3) + ".***.***-**";
            mHolder.cpf.setHint(finalCpf);
        }

        mHolder.password.setHint("*******");

        if(user.getSexo() != null){
            String sex = user.getSexo();

            if(sex.startsWith("F")){
                mHolder.genero.setHint("Feminino");
            }else if(sex.startsWith("M")){
                mHolder.genero.setHint("Masculino");
            } else if(sex.startsWith("O")){
                mHolder.genero.setHint("Outro");
            }
        }

        if(user.getDn() != null){
            String nascimento = user.getDn();
            nascimento = UTILS.formatDate(nascimento);
            nascimento = nascimento.replace("-", "/");
            mHolder.data.setHint(nascimento);
        }
    }

    void getRegulamento (int id){
        regulamentoViewModel.getRegulamento(id);
    }

    void initDocs(List<PositonData> data){
        mHolder.termos.removeAllViews();

        for(PositonData d : data){
            View view = LayoutInflater.from(this).inflate(R.layout.include_term_item, null, false);
            TextView text = view.findViewById(R.id.txt_term_item);

            text.setText(d.getTipo());

            final int id = d.getId();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_CONFIGURACAO_DOCS_ABRIR, id).execute();
                    getRegulamento(id);
                }
            });

            mHolder.termos.addView(view);
        }
    }

    void initPopupSenha(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View customLayout = getLayoutInflater().inflate(R.layout.dialog_trocar_senha, null);
        final EditText edtsenha = customLayout.findViewById(R.id.edt_nova_senha);
        final EditText edtsenha2 = customLayout.findViewById(R.id.edt_repetir_senha);
        Button button = customLayout.findViewById(R.id.btn_trocar_senha);
        TextView msg = customLayout.findViewById(R.id.txt_senha);

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncOperation(activity, AsyncOperation.TASK_ID_SAVE_TRACKING, 999, emptyAsync, TRACKING.TRACKING_ALTERAR_SENHA_CONFIRMAR).execute();
                String senha = edtsenha.getText().toString();
                String senha2 = edtsenha2.getText().toString();
                userViewModel.changeSenha(senha, senha2);
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    void openEditPerfil (){
        Intent intent = new Intent(this, EditarPerfilActivity.class);
        startActivity(intent);
    }

    class ViewHolder {
        TextView name;
        TextView email;
        TextView password;
        TextView genero;
        TextView data;
        TextView telefone;
        TextView cpf;

        View alterar;
        View mudarSenha;

        LinearLayout termos;

        public ViewHolder (){
            name = findViewById(R.id.txt_conf_nome);
            email = findViewById(R.id.txt_conf_email);
            password = findViewById(R.id.txt_conf_senha);
            genero = findViewById(R.id.txt_conf_genero);
            data = findViewById(R.id.txt_conf_data);
            telefone = findViewById(R.id.txt_conf_telefone);
            cpf = findViewById(R.id.txt_conf_cpf);

            alterar = findViewById(R.id.view_alterar_dados);
            mudarSenha = findViewById(R.id.view_alterar_senha);

            termos = findViewById(R.id.view_terms);
        }
    }
}
