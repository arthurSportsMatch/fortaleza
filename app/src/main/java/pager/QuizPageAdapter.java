package pager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.sportm.fortaleza.R;

import java.util.List;

import data.QuizData;

public class QuizPageAdapter extends PagerAdapter {

    Context context;
    List<QuizData> data;
    QuizPageListener listener;
    boolean isQuiz;

    public QuizPageAdapter(Context context, List<QuizData> data) {
        this.context = context;
        this.data = data;
    }

    public QuizPageAdapter(Context context, List<QuizData> data, QuizPageListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public void changePage (){
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.include_card_quiz, container, false);
        ViewHolder holder = new ViewHolder(view);
        QuizData quiz = data.get(position);
        //isQuiz = quiz.isQuiz();

        //Mudar esqueleto
        holder.viewNext.setVisibility(View.VISIBLE);
        holder.viewAlignTop.setVisibility(View.VISIBLE);
        holder.viewAlign.setVisibility(View.VISIBLE);

        //Mudar valores de descrição
        holder.txtTitle.setText(quiz.getTitulo());
        holder.txtDescription.setText(quiz.getDescricao());

        //Se é uma Pesquisa
        if(!isQuiz){

            //Criar as opções

//            for(RespostasData r : quiz.getRespostas()){
//                View v = LayoutInflater.from(context).inflate(R.layout.adapter_answer, null, false);
//
//                ProgressBar progressBar = view.findViewById(R.id.progress_answer);
//                TextView tittle = view.findViewById(R.id.txt_answer);
//                TextView porc = view.findViewById(R.id.txt_porc);
//
//                tittle.setText(r.getTitle());
//
//                porc.setVisibility(View.VISIBLE);
//                porc.setText(r.getValue() + "%");
//
//                //progressBar.setVisibility(View.VISIBLE);
//                //progressBar.setProgress(r.getValue());
//
//                holder.viewResults.addView(v);
//            }

            //holder.viewResults.removeAllViews();

            //Clicar em next
            holder.viewNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            holder.txtNext.setText("Arraste para ir para próxima");
        }
        //Se é um Quiz
        else{
            //Botão para iniciar quiz
            holder.viewResults.setVisibility(View.GONE);
            holder.viewAction.setVisibility(View.VISIBLE);
            holder.txtCategoryButton.setVisibility(View.VISIBLE);

            holder.txtCategoryButton.setText("CATEGORIA: Time do São Paulo");

            //Botão de categorias
            holder.viewNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }


        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View)object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
    public interface QuizPageListener {
        void ClickOnItem (int id);  //Clica em um item
        void ClickInNext ();        //Clica em proximo
    }

    class ViewHolder {
        TextView txtTitle;
        TextView txtDescription;

        //resultados
        LinearLayout viewResults;

        //alinhamento (ultra necessario)
        LinearLayout viewAlign;
        LinearLayout viewAlignTop;

        //botão de ação
        LinearLayout viewAction;
        TextView txtCategoryButton;

        //arrastar para cima
        LinearLayout viewNext;
        TextView txtNext;


        public ViewHolder (View view){
            txtTitle = view.findViewById(R.id.txt_nome_quiz);
            txtDescription = view.findViewById(R.id.txt_description_quiz);
            viewResults = view.findViewById(R.id.view_results_quiz);

            viewAlign = view.findViewById(R.id.space_bottom_card_quiz);
            viewAlignTop = view.findViewById(R.id.space_bottom_card_quiz_0);

            viewNext = view.findViewById(R.id.view_next);
            txtNext = view.findViewById(R.id.txt_next);

            viewAction = view.findViewById(R.id.view_action_quiz);
            txtCategoryButton = view.findViewById(R.id.txt_categoria_2_quiz);
        }
    }
}
