package pager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.R;

import java.util.List;

import data.BannerData;
import utils.CONSTANTS;
import utils.UTILS;

public class BannerAdapter  extends PagerAdapter {

    Context context;
    List<BannerData> dataList;
    Listener listener;

    public BannerAdapter(Context context, List<BannerData> dataList, Listener listener) {
        this.context = context;
        this.dataList = dataList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_banner_news, container, false);


        final BannerData data = dataList.get(position);

        if(data.isHasLast()){
            view = LayoutInflater.from(context).inflate(R.layout.adapter_banner_last_news, container, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.Onclick(data);
                }
            });

        }else {
            ImageView img = view.findViewById(R.id.img_adapterBanner);
            UTILS.getImageAt(context,data.getImg(), img);

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.Onclick(data);
                }
            });
        }

        container.addView(view);

        return view;
    }

    public interface Listener{
        void Onclick(BannerData data);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View)object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
