package pager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.sportm.fortaleza.LojaFortalezaActivity;
import com.sportm.fortaleza.R;

import java.util.List;

import data.PartidaData;
import utils.CONSTANTS;
import utils.UTILS;

public class NextGamePageAdapter extends PagerAdapter {

    Context context;
    List<PartidaData> data;
    Listener listener;
    int layout;
    boolean hide;

    public NextGamePageAdapter(Context context, List<PartidaData> data, int layout) {
        this.context = context;
        this.data = data;
        this.layout = layout;
    }

    public NextGamePageAdapter(Context context, List<PartidaData> data, int layout, boolean hide) {
        this.context = context;
        this.data = data;
        this.layout = layout;
        this.hide = hide;
    }

    public NextGamePageAdapter(Context context, List<PartidaData> data, int layout, boolean hide, Listener listener) {
        this.context = context;
        this.data = data;
        this.layout = layout;
        this.hide = hide;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(layout, container, false);
        ViewHolder holder = new ViewHolder(view);
        final PartidaData partida = data.get(position);

        //Imagem primeiro time
        UTILS.getImageAt(context, partida.getClubeImg1(), holder.image1);

        //Imagem primeiro time
        UTILS.getImageAt(context, partida.getClubeImg2(), holder.image2);

        if(!hide) {
            holder.timer.setText(UTILS.formatHourFromDBEvent(partida.getDataHora()));
        }

        holder.local.setText(partida.getEstadio());

        if(partida.getTitle() != null)
            holder.txtCampeonato.setText(partida.getTitle());

        if(partida.getHeader() != null && !partida.getHeader().isEmpty()){
            holder.tipoPartida.setVisibility(View.VISIBLE);
            holder.txtTipoPartida.setText(partida.getHeader());
        }

        if(partida.getLinkCompra() != null){
            holder.comprar.setVisibility(View.VISIBLE);
            holder.comprar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.OnPurchase(partida);
                    }else{
                        String url = partida.getLinkCompra();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        context.startActivity(i);
                    }
                }
            });
        }else{
            holder.comprar.setVisibility(View.GONE);
        }

        if(partida.isPermiteCheckIn()){
            holder.checkIn.setVisibility(View.VISIBLE);
            holder.checkIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.OnCheckIn();
                    }else {
                        Intent i = new Intent(context, LojaFortalezaActivity.class);
                        context.startActivity(i);
                    }
                }
            });
        }else {
            holder.checkIn.setVisibility(View.GONE);
        }



        if(partida.getIdTransmissao() != null){
            String[] chanels = partida.getIdTransmissao().split(",");

            Log.d("Async", partida.getIdTransmissao() +"  :  "+ chanels.length);
            for(int i = 0; i < chanels.length; i ++){
                addTransmistion(holder.tvs, chanels[i]);
            }
        }

        container.addView(view);
        return view;
    }

    void addTransmistion (LinearLayout view, String id){
        ImageView image = new ImageView(context);
        image.setPadding(10,0,0,0);
        image.setAdjustViewBounds(true);
        int idChannel = UTILS.getChanelD(Integer.parseInt(id));
        if(idChannel == -1){
            UTILS.getImageAt(context, CONSTANTS.ICON_BROADCAST + id + ".png ", image);
        }else{
            image.setImageResource(idChannel);
        }
        view.addView(image);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View)object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public interface Listener {
        void OnPurchase (PartidaData data);
        void OnCheckIn ();
    }

    class ViewHolder {
        public View tipoPartida;
        public TextView txtTipoPartida;
        public ImageView imgTipoPartida;

        public TextView txtCampeonato;

        public ImageView image1;    //imagem do primeiro time
        public ImageView image2;    //imagem do segundo time

        public TextView timer;      //horario do jogo
        public TextView local;      //local do jogo

        public LinearLayout tvs;            //para colocar lista de imagens
        public Button comprar;
        public Button checkIn;

        public ViewHolder(View itemView) {
            tipoPartida = itemView.findViewById(R.id.view_ag_tipo);
            txtTipoPartida = itemView.findViewById(R.id.txt_age_tipo);
            imgTipoPartida = itemView.findViewById(R.id.img_age_tipo);

            txtCampeonato = itemView.findViewById(R.id.txt_tipo_jogo);

            image1 = itemView.findViewById(R.id.img_time_1);
            image2 = itemView.findViewById(R.id.img_time_2);

            timer = itemView.findViewById(R.id.txt_jogo_time);
            local = itemView.findViewById(R.id.txt_jogo_local);

            tvs = itemView.findViewById(R.id.view_where_watch);
            comprar = itemView.findViewById(R.id.btn_comprar);
            checkIn = itemView.findViewById(R.id.btn_check_in);
        }
    }
}
